﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Rremision
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbTipo = New System.Windows.Forms.Label()
        Me.B_WorkSystemDataSet1 = New SIGMA.B_WorkSystemDataSet()
        Me.dgvMaster = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpF = New System.Windows.Forms.DateTimePicker()
        Me.dtpI = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbFiltro = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txtFiltro = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnExpo = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtVehiculo = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtSupervisor = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtRemision = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.cbFiltro2 = New DevExpress.XtraEditors.ComboBoxEdit()
        CType(Me.B_WorkSystemDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbFiltro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDetalle.SuspendLayout()
        Me.pMain.SuspendLayout()
        CType(Me.cbFiltro2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(18, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(62, 13)
        Me.Label54.TabIndex = 111
        Me.Label54.Text = "REPORTE:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label3.Location = New System.Drawing.Point(-3, -32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(1338, 42)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "__________________________________________________________________"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbTipo
        '
        Me.lbTipo.AutoSize = True
        Me.lbTipo.Location = New System.Drawing.Point(86, 0)
        Me.lbTipo.Name = "lbTipo"
        Me.lbTipo.Size = New System.Drawing.Size(60, 13)
        Me.lbTipo.TabIndex = 112
        Me.lbTipo.Text = "REMISION"
        '
        'B_WorkSystemDataSet1
        '
        Me.B_WorkSystemDataSet1.DataSetName = "B_WorkSystemDataSet"
        Me.B_WorkSystemDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'dgvMaster
        '
        Me.dgvMaster.AllowUserToAddRows = False
        Me.dgvMaster.AllowUserToDeleteRows = False
        Me.dgvMaster.AllowUserToOrderColumns = True
        Me.dgvMaster.AllowUserToResizeColumns = False
        Me.dgvMaster.AllowUserToResizeRows = False
        Me.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMaster.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column8, Me.Column3, Me.Column2, Me.Column4, Me.Column5, Me.Column10, Me.Column6})
        Me.dgvMaster.Location = New System.Drawing.Point(3, 16)
        Me.dgvMaster.Name = "dgvMaster"
        Me.dgvMaster.ReadOnly = True
        Me.dgvMaster.RowHeadersVisible = False
        Me.dgvMaster.RowHeadersWidth = 4
        Me.dgvMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaster.Size = New System.Drawing.Size(970, 510)
        Me.dgvMaster.TabIndex = 113
        '
        'Column1
        '
        Me.Column1.HeaderText = "Cod. Remision"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column8
        '
        Me.Column8.HeaderText = "Precedencia"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Fecha"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 80
        '
        'Column2
        '
        Me.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column2.HeaderText = "Sucursal"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column4.HeaderText = "Supervisor"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Vehiculo"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 80
        '
        'Column10
        '
        Me.Column10.HeaderText = "C.Total"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 80
        '
        'Column6
        '
        Me.Column6.HeaderText = "Estado"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 120
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(987, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Range de Fecha:"
        '
        'dtpF
        '
        Me.dtpF.CustomFormat = ""
        Me.dtpF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF.Location = New System.Drawing.Point(1216, 27)
        Me.dtpF.Name = "dtpF"
        Me.dtpF.Size = New System.Drawing.Size(105, 20)
        Me.dtpF.TabIndex = 2
        '
        'dtpI
        '
        Me.dtpI.CustomFormat = ""
        Me.dtpI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpI.Location = New System.Drawing.Point(1105, 27)
        Me.dtpI.Name = "dtpI"
        Me.dtpI.Size = New System.Drawing.Size(105, 20)
        Me.dtpI.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(997, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "BUSQUEDA"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.Location = New System.Drawing.Point(979, -31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(358, 42)
        Me.Label4.TabIndex = 115
        Me.Label4.Text = "_________________"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbFiltro
        '
        Me.cbFiltro.Location = New System.Drawing.Point(990, 54)
        Me.cbFiltro.Name = "cbFiltro"
        Me.cbFiltro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFiltro.Properties.Items.AddRange(New Object() {"COD. REMISION", "SUPERVISOR", "VEHICULO", "ESTADO", "TODO"})
        Me.cbFiltro.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbFiltro.Size = New System.Drawing.Size(109, 20)
        Me.cbFiltro.TabIndex = 3
        '
        'txtFiltro
        '
        Me.txtFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFiltro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFiltro.Location = New System.Drawing.Point(1105, 53)
        Me.txtFiltro.Name = "txtFiltro"
        Me.txtFiltro.Size = New System.Drawing.Size(216, 22)
        Me.txtFiltro.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(997, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 133
        Me.Label5.Text = "DETALLE"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label6.Location = New System.Drawing.Point(976, 47)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(358, 42)
        Me.Label6.TabIndex = 132
        Me.Label6.Text = "_________________"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(997, 247)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 135
        Me.Label7.Text = "TOTAL"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label8.Location = New System.Drawing.Point(976, 215)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(358, 42)
        Me.Label8.TabIndex = 134
        Me.Label8.Text = "_________________"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Me.btnExpo)
        Me.Panel2.Controls.Add(Me.btnPrint)
        Me.Panel2.Location = New System.Drawing.Point(979, 476)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(377, 50)
        Me.Panel2.TabIndex = 115
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(201, 14)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(90, 23)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "Cancelar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'btnExpo
        '
        Me.btnExpo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnExpo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExpo.Location = New System.Drawing.Point(105, 14)
        Me.btnExpo.Name = "btnExpo"
        Me.btnExpo.Size = New System.Drawing.Size(90, 23)
        Me.btnExpo.TabIndex = 7
        Me.btnExpo.Text = "Exportar Excel"
        Me.btnExpo.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Location = New System.Drawing.Point(9, 14)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(90, 23)
        Me.btnPrint.TabIndex = 6
        Me.btnPrint.Text = "Imprimir"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToResizeColumns = False
        Me.dgvDetalle.AllowUserToResizeRows = False
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Column7, Me.DataGridViewTextBoxColumn2, Me.Column9, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5})
        Me.dgvDetalle.Location = New System.Drawing.Point(0, 37)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.RowHeadersWidth = 4
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(970, 473)
        Me.dgvDetalle.TabIndex = 136
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Cod. Detalle"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.HeaderText = "Cod. Producto"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.HeaderText = "Costo"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Venta"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 80
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.txtEstado)
        Me.pDetalle.Controls.Add(Me.Label14)
        Me.pDetalle.Controls.Add(Me.txtVehiculo)
        Me.pDetalle.Controls.Add(Me.Label13)
        Me.pDetalle.Controls.Add(Me.txtSupervisor)
        Me.pDetalle.Controls.Add(Me.Label12)
        Me.pDetalle.Controls.Add(Me.txtSucursal)
        Me.pDetalle.Controls.Add(Me.Label11)
        Me.pDetalle.Controls.Add(Me.dtpFecha)
        Me.pDetalle.Controls.Add(Me.Label10)
        Me.pDetalle.Controls.Add(Me.txtRemision)
        Me.pDetalle.Controls.Add(Me.Label9)
        Me.pDetalle.Controls.Add(Me.dgvDetalle)
        Me.pDetalle.Location = New System.Drawing.Point(3, 16)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(970, 510)
        Me.pDetalle.TabIndex = 137
        Me.pDetalle.Visible = False
        '
        'txtEstado
        '
        Me.txtEstado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtEstado.Location = New System.Drawing.Point(813, 11)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.ReadOnly = True
        Me.txtEstado.Size = New System.Drawing.Size(157, 20)
        Me.txtEstado.TabIndex = 147
        Me.txtEstado.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(764, 14)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(43, 13)
        Me.Label14.TabIndex = 146
        Me.Label14.Text = "Estado:"
        '
        'txtVehiculo
        '
        Me.txtVehiculo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtVehiculo.Location = New System.Drawing.Point(583, 11)
        Me.txtVehiculo.Name = "txtVehiculo"
        Me.txtVehiculo.ReadOnly = True
        Me.txtVehiculo.Size = New System.Drawing.Size(45, 20)
        Me.txtVehiculo.TabIndex = 145
        Me.txtVehiculo.TabStop = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(526, 14)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 13)
        Me.Label13.TabIndex = 144
        Me.Label13.Text = "Vehiculo:"
        '
        'txtSupervisor
        '
        Me.txtSupervisor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSupervisor.Location = New System.Drawing.Point(383, 11)
        Me.txtSupervisor.Name = "txtSupervisor"
        Me.txtSupervisor.ReadOnly = True
        Me.txtSupervisor.Size = New System.Drawing.Size(137, 20)
        Me.txtSupervisor.TabIndex = 143
        Me.txtSupervisor.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(317, 14)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 13)
        Me.Label12.TabIndex = 142
        Me.Label12.Text = "Supervisor:"
        '
        'txtSucursal
        '
        Me.txtSucursal.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSucursal.Location = New System.Drawing.Point(187, 11)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.ReadOnly = True
        Me.txtSucursal.Size = New System.Drawing.Size(123, 20)
        Me.txtSucursal.TabIndex = 141
        Me.txtSucursal.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(130, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 140
        Me.Label11.Text = "Sucursal:"
        '
        'dtpFecha
        '
        Me.dtpFecha.Enabled = False
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(683, 11)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(75, 20)
        Me.dtpFecha.TabIndex = 3
        Me.dtpFecha.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(637, 14)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 139
        Me.Label10.Text = "Fecha:"
        '
        'txtRemision
        '
        Me.txtRemision.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtRemision.Location = New System.Drawing.Point(67, 11)
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.ReadOnly = True
        Me.txtRemision.Size = New System.Drawing.Size(55, 20)
        Me.txtRemision.TabIndex = 138
        Me.txtRemision.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 13)
        Me.Label9.TabIndex = 137
        Me.Label9.Text = "Remision:"
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.Label1)
        Me.pMain.Controls.Add(Me.dtpF)
        Me.pMain.Controls.Add(Me.Panel2)
        Me.pMain.Controls.Add(Me.dtpI)
        Me.pMain.Controls.Add(Me.cbFiltro)
        Me.pMain.Controls.Add(Me.Label7)
        Me.pMain.Controls.Add(Me.lbTipo)
        Me.pMain.Controls.Add(Me.Label5)
        Me.pMain.Controls.Add(Me.Label54)
        Me.pMain.Controls.Add(Me.Label2)
        Me.pMain.Controls.Add(Me.Label3)
        Me.pMain.Controls.Add(Me.Label8)
        Me.pMain.Controls.Add(Me.Label4)
        Me.pMain.Controls.Add(Me.pDetalle)
        Me.pMain.Controls.Add(Me.dgvMaster)
        Me.pMain.Controls.Add(Me.cbFiltro2)
        Me.pMain.Controls.Add(Me.txtFiltro)
        Me.pMain.Controls.Add(Me.Label6)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1339, 532)
        Me.pMain.TabIndex = 138
        '
        'cbFiltro2
        '
        Me.cbFiltro2.Location = New System.Drawing.Point(1105, 54)
        Me.cbFiltro2.Name = "cbFiltro2"
        Me.cbFiltro2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFiltro2.Properties.Items.AddRange(New Object() {"---", "EMITIDA", "PREFACTURA", "CERRADA", "ANULADA"})
        Me.cbFiltro2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbFiltro2.Size = New System.Drawing.Size(216, 20)
        Me.cbFiltro2.TabIndex = 5
        Me.cbFiltro2.TabStop = False
        '
        'Rremision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1339, 532)
        Me.Controls.Add(Me.pMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Rremision"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.B_WorkSystemDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbFiltro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        Me.pMain.ResumeLayout(False)
        Me.pMain.PerformLayout()
        CType(Me.cbFiltro2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbTipo As System.Windows.Forms.Label
    Friend WithEvents B_WorkSystemDataSet1 As SIGMA.B_WorkSystemDataSet
    Friend WithEvents dgvMaster As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpF As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbFiltro As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtFiltro As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnExpo As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents txtEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtVehiculo As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtSupervisor As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtRemision As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents cbFiltro2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
