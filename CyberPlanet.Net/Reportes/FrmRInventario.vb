﻿Imports System.Data.SqlClient

Public Class FrmRInventario
    Dim sqlstring As String
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim adapter As SqlDataAdapter
    '-------------------------
    Dim bodega As Boolean = False
    Dim linea As Boolean = False
    Dim categoria As Boolean = False
    Dim proveedor As Boolean = False
    Private Sub FrmRInventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        lb_Bodega()
        lb_linea()
        lb_categoria()
        lb_Proveedor()
        clear()
    End Sub
    Private Sub FrmRInventario_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pmain.Location = New Point(Me.ClientSize.Width / 2 - Me.pmain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pmain.Size.Height / 2)
        Me.pmain.Anchor = AnchorStyles.None
    End Sub
    Private Sub lbBodega_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbBodega.SelectedIndexChanged
        If lbBodega.SelectedIndex = 0 Then
            lbBodega.SelectionMode = SelectionMode.One
            lbBodega.SelectedIndex = 0
            bodega = False
        Else
            lbBodega.SelectionMode = SelectionMode.MultiExtended
            bodega = True
        End If
        'Retrive()
    End Sub
    Private Sub lbLinea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbLinea.SelectedIndexChanged
        If lbLinea.SelectedIndex = 0 Then
            lbLinea.SelectionMode = SelectionMode.One
            lbLinea.SelectedIndex = 0
            linea = False
        Else
            lbLinea.SelectionMode = SelectionMode.MultiExtended
            linea = True
        End If
        'Retrive()
    End Sub
    Private Sub lbCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbCategoria.SelectedIndexChanged
        If lbCategoria.SelectedIndex = 0 Then
            lbCategoria.SelectionMode = SelectionMode.One
            lbCategoria.SelectedIndex = 0
            categoria = False
        Else
            lbCategoria.SelectionMode = SelectionMode.MultiExtended
            categoria = True
        End If
        'Retrive()
    End Sub
    Private Sub lbProveedor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbProveedor.SelectedIndexChanged
        If lbProveedor.SelectedIndex = 0 Then
            lbProveedor.SelectionMode = SelectionMode.One
            lbProveedor.SelectedIndex = 0
            proveedor = False
        Else
            lbProveedor.SelectionMode = SelectionMode.MultiExtended
            proveedor = True
        End If
        'Retrive()
    End Sub
    Sub clear()
        lbBodega.SelectedIndex = 0
        lbLinea.SelectedIndex = 0
        lbCategoria.SelectedIndex = 0
        lbProveedor.SelectedIndex = 0
        GridControl1.DataSource = vbNull
        GridView1.Columns.Clear()
    End Sub
    Sub Retrive()
        GridControl1.DataSource = vbNull
        GridView1.Columns.Clear()
        'GridView1.BestFitColumns()
        Dim Conta1 As Integer = 0 'where
        Dim Conta2 As Integer = 0 'and
        Dim Conta3 As Integer = 0 'uso or
        sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre', Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', Tbl_Proveedor.Nombre_Proveedor as 'Proveedor', Almacenes.Nombre_Bodega as 'Bodega' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
        Select Case cbTipo.SelectedIndex
            Case 0 ' LEVANTAMIENTO FISICO
                sqlstring = "select Productos.Codigo_Producto as 'Codigo', Nombre_Producto as 'Nombre', Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', ' ' as 'Conteo' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
            Case 1 'RESUMEN DE EXISTENCIA Y COSTO
                sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', Productos.Costo_Promedio as 'Costo',(Inventario.UndTotal_Existencia*Productos.Costo_Promedio) as 'Total' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
            Case 2 'RESUMEN DE EXISTENCIA
                sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Productos.Existencia_Minima as 'Existencia Minima', Inventario.UndTotal_Existencia as 'Existencias', Productos.Existencia_Maxima as 'Existencia Maxima' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
            Case 3 'PRECIO DE FACTURACION MENOR AL COSTO
                sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Productos.Costo_Promedio as 'Costo', isnull(Precios.Precio_Cordobas,'0') as 'Precio Facturacion', (Productos.Costo_Promedio-Precios.Precio_Cordobas) as 'Diferencia' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea left join Precios on Precios.Codigo_Prducto=Productos.Codigo_Producto and Precios.Codigo_Tipo=3 and Precios.Activo=1 where  Productos.Costo_Promedio > Precio_Cordobas"
                Conta1 = 1
                Conta2 = 1
            Case 4 'EXISTENCIA POR DEBAJO DEL MININO
                sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', Productos.Existencia_Minima as 'Existencia Minima', (Productos.Existencia_Minima-Inventario.UndTotal_Existencia) as 'Diferencia' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea where Productos.Existencia_Minima > Inventario.UndTotal_Existencia"
                Conta1 = 1
                Conta2 = 1
            Case 5 'ENTRADA SALIDA habilita fecha
        End Select
        If lbBodega.SelectedIndex > 0 Or lbLinea.SelectedIndex > 0 Or lbCategoria.SelectedIndex > 0 Or lbProveedor.SelectedIndex > 0 Then
            If lbBodega.SelectedIndex > 0 Then
                If Conta1 = 0 Then
                    sqlstring = sqlstring + " where "
                    Conta1 = 1
                End If
                If Conta2 = 1 Then
                    sqlstring = sqlstring + " AND "
                End If
                Conta3 = 0
                sqlstring = sqlstring + " ("
                For Each item In lbBodega.SelectedItems
                    If Conta3 > 0 Then
                        sqlstring = sqlstring + " or "
                    End If
                    sqlstring = sqlstring + " Nombre_Bodega= '" + item + "'"
                    Conta3 += 1
                Next
                sqlstring = sqlstring + " )"
                Conta2 = 1
            End If

            If lbLinea.SelectedIndex > 0 Then
                If Conta1 = 0 Then
                    sqlstring = sqlstring + " where "
                    Conta1 = 1
                End If
                If Conta2 = 1 Then
                    sqlstring = sqlstring + " AND "
                End If
                Conta3 = 0
                sqlstring = sqlstring + " ("
                For Each item In lbLinea.SelectedItems
                    If Conta3 > 0 Then
                        sqlstring = sqlstring + " or "
                    End If
                    sqlstring = sqlstring + " Linea_Producto.Nombre_Linea= '" + item + "'"
                    Conta3 += 1
                Next
                sqlstring = sqlstring + " )"
                Conta2 = 1
            End If

            If lbCategoria.SelectedIndex > 0 Then
                If Conta1 = 0 Then
                    sqlstring = sqlstring + " where "
                    Conta1 = 1
                End If
                If Conta2 = 1 Then
                    sqlstring = sqlstring + " AND "
                End If
                Conta3 = 0
                sqlstring = sqlstring + " ("
                For Each item In lbCategoria.SelectedItems
                    If Conta3 > 0 Then
                        sqlstring = sqlstring + " or "
                    End If
                    sqlstring = sqlstring + " nombre_categoria= '" + item + "'"
                    Conta3 += 1
                Next
                sqlstring = sqlstring + " )"
                Conta2 = 1
            End If
            If lbProveedor.SelectedIndex > 0 Then
                If Conta1 = 0 Then
                    sqlstring = sqlstring + " where "
                    Conta1 = 1
                End If
                If Conta2 = 1 Then
                    sqlstring = sqlstring + " AND "
                End If
                Conta3 = 0
                sqlstring = sqlstring + " ("
                For Each item In lbProveedor.SelectedItems
                    If Conta3 > 0 Then
                        sqlstring = sqlstring + " or "
                    End If
                    sqlstring = sqlstring + " Nombre_Proveedor= '" + item + "'"
                    Conta3 += 1
                Next
                sqlstring = sqlstring + " )"
                Conta2 = 1
            End If
        Else
            Select Case cbTipo.SelectedIndex
                Case 0 ' LEVANTAMIENTO FISICO
                    'sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre', Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', ' ' as 'Conteo' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
                    sqlstring = "select Productos.Codigo_Producto as 'Codigo', productos.Codigo_Alterno as 'Codigo Alterno',Nombre_Producto as 'Nombre', Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', ' ' as 'Conteo' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
                Case 1 'RESUMEN DE EXISTENCIA Y COSTO
                    sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', Productos.Costo_Promedio as 'Costo',(Inventario.UndTotal_Existencia*Productos.Costo_Promedio) as 'Total' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
                Case 2 'RESUMEN DE EXISTENCIA
                    sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Productos.Existencia_Minima as 'Existencia Minima', Inventario.UndTotal_Existencia as 'Existencias', Productos.Existencia_Maxima as 'Existencia Maxima' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea"
                Case 3 'PRECIO DE FACTURACION MENOR AL COSTO
                    sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Productos.Costo_Promedio as 'Costo', isnull(Precios.Precio_Cordobas,'0') as 'Precio Facturacion', (Productos.Costo_Promedio-Precios.Precio_Cordobas) as 'Diferencia' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea left join Precios on Precios.Codigo_Prducto=Productos.Codigo_Producto and Precios.Codigo_Tipo=3 and Precios.Activo=1 where  Productos.Costo_Promedio > Precio_Cordobas"
                Case 4 'EXISTENCIA POR DEBAJO DEL MININO
                    sqlstring = "select Productos.Codigo_Producto as 'Codigo',Nombre_Producto as 'Nombre',Unidades_Medida.Nombre_UnidadMed as 'Und. Medida', Inventario.UndTotal_Existencia as 'Existencias', Productos.Existencia_Minima as 'Existencia Minima', (Productos.Existencia_Minima-Inventario.UndTotal_Existencia) as 'Diferencia' from Productos inner join Unidades_Medida on Unidades_Medida.Codigo_UnidadMed=Productos.Codigo_UnidMedida inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto inner join Tbl_Proveedor on Tbl_Proveedor.Codigo_Proveedor=Productos.Codigo_Proveedor inner join Almacenes on Almacenes.Codigo_Bodega=Inventario.Codigo_Bodega inner join Categoria_Producto on Categoria_Producto.Codigo_Categoria=Productos.Codigo_Categoria inner join Linea_Producto on Linea_Producto.Codigo_Linea=Productos.Codigo_Linea and Linea_Producto.Codigo_Linea=Categoria_Producto.Codigo_Linea where Productos.Existencia_Minima > Inventario.UndTotal_Existencia"
            End Select
        End If
        'MsgBox(sqlstring)
        Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
        GridControl1.DataSource = TableDatos
        GridView1.BestFitColumns()
    End Sub
    Sub lb_Bodega()
        Dim dt As DataTable = New DataTable()
        lbBodega.Items.Clear()
        lbBodega.Items.Add("TODOS")
        sqlstring = "select Nombre_Bodega from Almacenes"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                lbBodega.Items.Add(row0(0))
            Next
            con.Close()
            dt.Rows.Clear()
            lbBodega.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub lb_linea()
        Dim dt As DataTable = New DataTable()
        lbLinea.Items.Clear()
        lbLinea.Items.Add("TODAS")
        sqlstring = "select Nombre_Linea from Linea_Producto"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                lbLinea.Items.Add(row0(0))
            Next
            con.Close()
            dt.Rows.Clear()
            lbLinea.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub lb_categoria()
        Dim dt As DataTable = New DataTable()
        lbCategoria.Items.Clear()
        lbCategoria.Items.Add("TODAS")
        sqlstring = "select Nombre_Categoria from Categoria_Producto"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                lbCategoria.Items.Add(row0(0))
            Next
            con.Close()
            dt.Rows.Clear()
            lbCategoria.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub lb_proveedor()
        Dim dt As DataTable = New DataTable()
        lbProveedor.Items.Clear()
        lbProveedor.Items.Add("TODOS")
        sqlstring = "Select Nombre_Proveedor from Tbl_Proveedor"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                lbProveedor.Items.Add(row0(0))
            Next
            con.Close()
            dt.Rows.Clear()
            lbProveedor.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        Retrive()
    End Sub
    Private Sub btnOcultar_Click(sender As Object, e As EventArgs) Handles btnOcultar.Click
        'size=1319, 381
        'location=16, 139

        If btnOcultar.Text = "Ocultar" Then
            btnOcultar.Text = "Mostrar"
            btnOcultar.BackColor = Color.Orange
            GridControl1.Location = New Point(16, 69)
            GridControl1.Height = 459
            lbBodega.Visible = False
            lbLinea.Visible = False
            lbCategoria.Visible = False
            lbProveedor.Visible = False
            Label2.Visible = False
            Label4.Visible = False
            Label5.Visible = False
            Label6.Visible = False
        Else
            btnOcultar.Text = "Ocultar"
            btnOcultar.BackColor = SystemColors.ButtonHighlight
            GridControl1.Location = New Point(16, 139)
            GridControl1.Height = 391
            lbBodega.Visible = True
            lbLinea.Visible = True
            lbCategoria.Visible = True
            lbProveedor.Visible = True
            Label2.Visible = True
            Label4.Visible = True
            Label5.Visible = True
            Label6.Visible = True
        End If
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.clear()
    End Sub
End Class