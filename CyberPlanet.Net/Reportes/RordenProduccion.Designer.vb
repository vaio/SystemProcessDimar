﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RordenProduccion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pmain = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpF = New System.Windows.Forms.DateTimePicker()
        Me.dtpI = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbTipo = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbFiltro = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbFiltro2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txtfiltro = New System.Windows.Forms.TextBox()
        Me.pDetalle2 = New System.Windows.Forms.Panel()
        Me.txtTotal2 = New System.Windows.Forms.TextBox()
        Me.txtComentario = New System.Windows.Forms.TextBox()
        Me.txtTipoMov = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDoc = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTipoDoc = New System.Windows.Forms.TextBox()
        Me.txtbodega = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpFecha2 = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtcodigo = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dgvDetalle2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtCodProducto = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtSupervisor = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtOrden = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvMaster = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.pmain.SuspendLayout()
        CType(Me.cbFiltro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbFiltro2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDetalle2.SuspendLayout()
        CType(Me.dgvDetalle2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDetalle.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'pmain
        '
        Me.pmain.Controls.Add(Me.Label1)
        Me.pmain.Controls.Add(Me.dtpF)
        Me.pmain.Controls.Add(Me.dtpI)
        Me.pmain.Controls.Add(Me.Label2)
        Me.pmain.Controls.Add(Me.lbTipo)
        Me.pmain.Controls.Add(Me.Label54)
        Me.pmain.Controls.Add(Me.Label3)
        Me.pmain.Controls.Add(Me.cbFiltro)
        Me.pmain.Controls.Add(Me.cbFiltro2)
        Me.pmain.Controls.Add(Me.txtfiltro)
        Me.pmain.Controls.Add(Me.pDetalle2)
        Me.pmain.Controls.Add(Me.pDetalle)
        Me.pmain.Controls.Add(Me.dgvMaster)
        Me.pmain.Controls.Add(Me.Panel2)
        Me.pmain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pmain.Location = New System.Drawing.Point(0, 0)
        Me.pmain.Name = "pmain"
        Me.pmain.Size = New System.Drawing.Size(1339, 532)
        Me.pmain.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(357, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 16)
        Me.Label1.TabIndex = 155
        Me.Label1.Text = "Range de Fecha:"
        '
        'dtpF
        '
        Me.dtpF.CustomFormat = ""
        Me.dtpF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF.Location = New System.Drawing.Point(586, 22)
        Me.dtpF.Name = "dtpF"
        Me.dtpF.Size = New System.Drawing.Size(105, 20)
        Me.dtpF.TabIndex = 4
        '
        'dtpI
        '
        Me.dtpI.CustomFormat = ""
        Me.dtpI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpI.Location = New System.Drawing.Point(475, 22)
        Me.dtpI.Name = "dtpI"
        Me.dtpI.Size = New System.Drawing.Size(105, 20)
        Me.dtpI.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 151
        Me.Label2.Text = "BUSQUEDA"
        '
        'lbTipo
        '
        Me.lbTipo.AutoSize = True
        Me.lbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTipo.Location = New System.Drawing.Point(1153, 2)
        Me.lbTipo.Name = "lbTipo"
        Me.lbTipo.Size = New System.Drawing.Size(172, 16)
        Me.lbTipo.TabIndex = 149
        Me.lbTipo.Text = "ORDEN DE PRODUCCION"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(1084, 3)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(62, 13)
        Me.Label54.TabIndex = 148
        Me.Label54.Text = "REPORTE:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label3.Location = New System.Drawing.Point(-2, -29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(1338, 42)
        Me.Label3.TabIndex = 147
        Me.Label3.Text = "__________________________________________________________________"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbFiltro
        '
        Me.cbFiltro.Location = New System.Drawing.Point(6, 25)
        Me.cbFiltro.Name = "cbFiltro"
        Me.cbFiltro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFiltro.Properties.Items.AddRange(New Object() {"COD. ORDEN", "SUCURSAL", "PRODUCTO", "ESTADO", "ACTIVO", "TODO"})
        Me.cbFiltro.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbFiltro.Size = New System.Drawing.Size(109, 20)
        Me.cbFiltro.TabIndex = 1
        '
        'cbFiltro2
        '
        Me.cbFiltro2.Location = New System.Drawing.Point(121, 25)
        Me.cbFiltro2.Name = "cbFiltro2"
        Me.cbFiltro2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFiltro2.Properties.DropDownRows = 14
        Me.cbFiltro2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbFiltro2.Size = New System.Drawing.Size(216, 20)
        Me.cbFiltro2.TabIndex = 2
        '
        'txtfiltro
        '
        Me.txtfiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtfiltro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfiltro.Location = New System.Drawing.Point(121, 24)
        Me.txtfiltro.Name = "txtfiltro"
        Me.txtfiltro.Size = New System.Drawing.Size(216, 22)
        Me.txtfiltro.TabIndex = 138
        '
        'pDetalle2
        '
        Me.pDetalle2.Controls.Add(Me.txtTotal2)
        Me.pDetalle2.Controls.Add(Me.txtComentario)
        Me.pDetalle2.Controls.Add(Me.txtTipoMov)
        Me.pDetalle2.Controls.Add(Me.Label4)
        Me.pDetalle2.Controls.Add(Me.txtDoc)
        Me.pDetalle2.Controls.Add(Me.Label5)
        Me.pDetalle2.Controls.Add(Me.txtTipoDoc)
        Me.pDetalle2.Controls.Add(Me.txtbodega)
        Me.pDetalle2.Controls.Add(Me.Label7)
        Me.pDetalle2.Controls.Add(Me.dtpFecha2)
        Me.pDetalle2.Controls.Add(Me.Label8)
        Me.pDetalle2.Controls.Add(Me.txtcodigo)
        Me.pDetalle2.Controls.Add(Me.Label15)
        Me.pDetalle2.Controls.Add(Me.dgvDetalle2)
        Me.pDetalle2.Location = New System.Drawing.Point(4, 56)
        Me.pDetalle2.Name = "pDetalle2"
        Me.pDetalle2.Size = New System.Drawing.Size(1332, 473)
        Me.pDetalle2.TabIndex = 153
        Me.pDetalle2.Visible = False
        '
        'txtTotal2
        '
        Me.txtTotal2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtTotal2.Location = New System.Drawing.Point(1210, 440)
        Me.txtTotal2.Name = "txtTotal2"
        Me.txtTotal2.ReadOnly = True
        Me.txtTotal2.Size = New System.Drawing.Size(122, 20)
        Me.txtTotal2.TabIndex = 149
        Me.txtTotal2.TabStop = False
        Me.txtTotal2.Text = "C$15.0000"
        '
        'txtComentario
        '
        Me.txtComentario.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtComentario.Location = New System.Drawing.Point(513, 11)
        Me.txtComentario.Name = "txtComentario"
        Me.txtComentario.ReadOnly = True
        Me.txtComentario.Size = New System.Drawing.Size(331, 20)
        Me.txtComentario.TabIndex = 148
        Me.txtComentario.TabStop = False
        Me.txtComentario.Text = "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS"
        '
        'txtTipoMov
        '
        Me.txtTipoMov.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtTipoMov.Location = New System.Drawing.Point(1201, 11)
        Me.txtTipoMov.Name = "txtTipoMov"
        Me.txtTipoMov.ReadOnly = True
        Me.txtTipoMov.Size = New System.Drawing.Size(120, 20)
        Me.txtTipoMov.TabIndex = 147
        Me.txtTipoMov.TabStop = False
        Me.txtTipoMov.Text = "ENTRADA"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(1164, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 146
        Me.Label4.Text = "Tipo:"
        '
        'txtDoc
        '
        Me.txtDoc.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtDoc.Location = New System.Drawing.Point(449, 11)
        Me.txtDoc.Name = "txtDoc"
        Me.txtDoc.ReadOnly = True
        Me.txtDoc.Size = New System.Drawing.Size(58, 20)
        Me.txtDoc.TabIndex = 145
        Me.txtDoc.TabStop = False
        Me.txtDoc.Text = "00004"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(378, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 13)
        Me.Label5.TabIndex = 144
        Me.Label5.Text = "Documento:"
        '
        'txtTipoDoc
        '
        Me.txtTipoDoc.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtTipoDoc.Location = New System.Drawing.Point(850, 11)
        Me.txtTipoDoc.Name = "txtTipoDoc"
        Me.txtTipoDoc.ReadOnly = True
        Me.txtTipoDoc.Size = New System.Drawing.Size(163, 20)
        Me.txtTipoDoc.TabIndex = 143
        Me.txtTipoDoc.TabStop = False
        Me.txtTipoDoc.Text = "Remision de Mercaderia"
        '
        'txtbodega
        '
        Me.txtbodega.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtbodega.Location = New System.Drawing.Point(175, 11)
        Me.txtbodega.Name = "txtbodega"
        Me.txtbodega.ReadOnly = True
        Me.txtbodega.Size = New System.Drawing.Size(188, 20)
        Me.txtbodega.TabIndex = 141
        Me.txtbodega.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(118, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 140
        Me.Label7.Text = "Bodega:"
        '
        'dtpFecha2
        '
        Me.dtpFecha2.Enabled = False
        Me.dtpFecha2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha2.Location = New System.Drawing.Point(1073, 11)
        Me.dtpFecha2.Name = "dtpFecha2"
        Me.dtpFecha2.Size = New System.Drawing.Size(75, 20)
        Me.dtpFecha2.TabIndex = 3
        Me.dtpFecha2.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(1027, 14)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 139
        Me.Label8.Text = "Fecha:"
        '
        'txtcodigo
        '
        Me.txtcodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtcodigo.Location = New System.Drawing.Point(53, 11)
        Me.txtcodigo.Name = "txtcodigo"
        Me.txtcodigo.ReadOnly = True
        Me.txtcodigo.Size = New System.Drawing.Size(55, 20)
        Me.txtcodigo.TabIndex = 138
        Me.txtcodigo.TabStop = False
        Me.txtcodigo.Text = "00000001"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 14)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 137
        Me.Label15.Text = "Codigo:"
        '
        'dgvDetalle2
        '
        Me.dgvDetalle2.AllowUserToAddRows = False
        Me.dgvDetalle2.AllowUserToDeleteRows = False
        Me.dgvDetalle2.AllowUserToResizeColumns = False
        Me.dgvDetalle2.AllowUserToResizeRows = False
        Me.dgvDetalle2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.dgvDetalle2.Location = New System.Drawing.Point(0, 37)
        Me.dgvDetalle2.Name = "dgvDetalle2"
        Me.dgvDetalle2.ReadOnly = True
        Me.dgvDetalle2.RowHeadersVisible = False
        Me.dgvDetalle2.RowHeadersWidth = 4
        Me.dgvDetalle2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle2.Size = New System.Drawing.Size(1332, 403)
        Me.dgvDetalle2.TabIndex = 136
        Me.dgvDetalle2.TabStop = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 110
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Costo Unitario"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Costo Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 120
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.txtProducto)
        Me.pDetalle.Controls.Add(Me.txtEstado)
        Me.pDetalle.Controls.Add(Me.Label14)
        Me.pDetalle.Controls.Add(Me.txtCodProducto)
        Me.pDetalle.Controls.Add(Me.Label13)
        Me.pDetalle.Controls.Add(Me.txtSupervisor)
        Me.pDetalle.Controls.Add(Me.Label12)
        Me.pDetalle.Controls.Add(Me.txtSucursal)
        Me.pDetalle.Controls.Add(Me.Label11)
        Me.pDetalle.Controls.Add(Me.dtpFecha)
        Me.pDetalle.Controls.Add(Me.Label10)
        Me.pDetalle.Controls.Add(Me.txtOrden)
        Me.pDetalle.Controls.Add(Me.Label9)
        Me.pDetalle.Controls.Add(Me.dgvDetalle)
        Me.pDetalle.Location = New System.Drawing.Point(4, 56)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(1332, 473)
        Me.pDetalle.TabIndex = 152
        Me.pDetalle.Visible = False
        '
        'txtProducto
        '
        Me.txtProducto.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtProducto.Location = New System.Drawing.Point(693, 11)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.ReadOnly = True
        Me.txtProducto.Size = New System.Drawing.Size(292, 20)
        Me.txtProducto.TabIndex = 148
        Me.txtProducto.TabStop = False
        '
        'txtEstado
        '
        Me.txtEstado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtEstado.Location = New System.Drawing.Point(1167, 11)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.ReadOnly = True
        Me.txtEstado.Size = New System.Drawing.Size(157, 20)
        Me.txtEstado.TabIndex = 147
        Me.txtEstado.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(1118, 14)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(43, 13)
        Me.Label14.TabIndex = 146
        Me.Label14.Text = "Estado:"
        '
        'txtCodProducto
        '
        Me.txtCodProducto.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodProducto.Location = New System.Drawing.Point(608, 11)
        Me.txtCodProducto.Name = "txtCodProducto"
        Me.txtCodProducto.ReadOnly = True
        Me.txtCodProducto.Size = New System.Drawing.Size(79, 20)
        Me.txtCodProducto.TabIndex = 145
        Me.txtCodProducto.TabStop = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(549, 14)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(53, 13)
        Me.Label13.TabIndex = 144
        Me.Label13.Text = "Producto:"
        '
        'txtSupervisor
        '
        Me.txtSupervisor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSupervisor.Location = New System.Drawing.Point(393, 11)
        Me.txtSupervisor.Name = "txtSupervisor"
        Me.txtSupervisor.ReadOnly = True
        Me.txtSupervisor.Size = New System.Drawing.Size(150, 20)
        Me.txtSupervisor.TabIndex = 143
        Me.txtSupervisor.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(327, 14)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 13)
        Me.Label12.TabIndex = 142
        Me.Label12.Text = "Supervisor:"
        '
        'txtSucursal
        '
        Me.txtSucursal.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSucursal.Location = New System.Drawing.Point(175, 11)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.ReadOnly = True
        Me.txtSucursal.Size = New System.Drawing.Size(141, 20)
        Me.txtSucursal.TabIndex = 141
        Me.txtSucursal.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(118, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 140
        Me.Label11.Text = "Sucursal:"
        '
        'dtpFecha
        '
        Me.dtpFecha.Enabled = False
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(1037, 11)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(75, 20)
        Me.dtpFecha.TabIndex = 3
        Me.dtpFecha.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(991, 14)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 139
        Me.Label10.Text = "Fecha:"
        '
        'txtOrden
        '
        Me.txtOrden.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtOrden.Location = New System.Drawing.Point(53, 11)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.ReadOnly = True
        Me.txtOrden.Size = New System.Drawing.Size(55, 20)
        Me.txtOrden.TabIndex = 138
        Me.txtOrden.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 13)
        Me.Label9.TabIndex = 137
        Me.Label9.Text = "Orden:"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToResizeColumns = False
        Me.dgvDetalle.AllowUserToResizeRows = False
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column9, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn5})
        Me.dgvDetalle.Location = New System.Drawing.Point(0, 37)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.RowHeadersWidth = 4
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(1332, 436)
        Me.dgvDetalle.TabIndex = 136
        Me.dgvDetalle.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Producto"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.HeaderText = "Cant Requerida"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 110
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "No utlizado"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Costo/ Unitario"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 120
        '
        'dgvMaster
        '
        Me.dgvMaster.AllowUserToAddRows = False
        Me.dgvMaster.AllowUserToDeleteRows = False
        Me.dgvMaster.AllowUserToOrderColumns = True
        Me.dgvMaster.AllowUserToResizeColumns = False
        Me.dgvMaster.AllowUserToResizeRows = False
        Me.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMaster.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column8, Me.Column10, Me.Column11, Me.Column12, Me.Column13, Me.Column14, Me.Column15})
        Me.dgvMaster.Location = New System.Drawing.Point(4, 56)
        Me.dgvMaster.Name = "dgvMaster"
        Me.dgvMaster.ReadOnly = True
        Me.dgvMaster.RowHeadersVisible = False
        Me.dgvMaster.RowHeadersWidth = 4
        Me.dgvMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaster.Size = New System.Drawing.Size(1332, 473)
        Me.dgvMaster.TabIndex = 150
        Me.dgvMaster.TabStop = False
        '
        'Column1
        '
        Me.Column1.HeaderText = "Orden"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 60
        '
        'Column2
        '
        Me.Column2.HeaderText = "Sucursal"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 140
        '
        'Column3
        '
        Me.Column3.HeaderText = "Cod_Producto"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column4.HeaderText = "Producto"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Supervisor"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "C/Req."
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 60
        '
        'Column8
        '
        Me.Column8.HeaderText = "Cant/Req."
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 60
        '
        'Column10
        '
        Me.Column10.HeaderText = "C/Elab."
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 60
        '
        'Column11
        '
        Me.Column11.HeaderText = "Cant/Elab."
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 60
        '
        'Column12
        '
        Me.Column12.HeaderText = "Registro"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Width = 60
        '
        'Column13
        '
        Me.Column13.HeaderText = "Cierre"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 60
        '
        'Column14
        '
        Me.Column14.HeaderText = "Estado"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Width = 120
        '
        'Column15
        '
        Me.Column15.HeaderText = "Activo"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column15.Width = 50
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Me.btnExportar)
        Me.Panel2.Controls.Add(Me.btnPrint)
        Me.Panel2.Location = New System.Drawing.Point(1034, 24)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(302, 50)
        Me.Panel2.TabIndex = 158
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(201, 5)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(90, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Cancelar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'btnExportar
        '
        Me.btnExportar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnExportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportar.Location = New System.Drawing.Point(105, 5)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(90, 23)
        Me.btnExportar.TabIndex = 6
        Me.btnExportar.Text = "Exportar Excel"
        Me.btnExportar.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Location = New System.Drawing.Point(9, 5)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(90, 23)
        Me.btnPrint.TabIndex = 5
        Me.btnPrint.Text = "Imprimir"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'RordenProduccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1339, 532)
        Me.Controls.Add(Me.pmain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "RordenProduccion"
        Me.pmain.ResumeLayout(False)
        Me.pmain.PerformLayout()
        CType(Me.cbFiltro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbFiltro2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDetalle2.ResumeLayout(False)
        Me.pDetalle2.PerformLayout()
        CType(Me.dgvDetalle2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pmain As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpF As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpI As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbFiltro2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbTipo As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvMaster As System.Windows.Forms.DataGridView
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents txtEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtCodProducto As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtSupervisor As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents cbFiltro As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnExportar As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents txtfiltro As System.Windows.Forms.TextBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pDetalle2 As System.Windows.Forms.Panel
    Friend WithEvents txtComentario As System.Windows.Forms.TextBox
    Friend WithEvents txtTipoMov As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTipoDoc As System.Windows.Forms.TextBox
    Friend WithEvents txtbodega As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtcodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dgvDetalle2 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotal2 As System.Windows.Forms.TextBox
End Class
