﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
Public Class RordenProduccion
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As SqlCommand
    Dim sqlstring As String
    '-----------print
    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    Dim misValue As Object = System.Reflection.Missing.Value
    Dim lectura As SqlDataReader
    Dim bandera As Boolean = False
    Public Tipo As Integer = 0
    'Calculo
    Dim CostoReq As Decimal = 0
    Dim CantReq As Decimal = 0
    Dim CostoElab As Decimal = 0
    Dim CantElab As Decimal = 0
    Dim expo As Boolean = False
    Private Sub RordenProduccion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbFiltro.Properties.Items.Clear()
        Select Case NumCatalogo
            Case 23 'ORDEN DE PRODUCCION
                lbTipo.Text = "ORDEN DE PRODUCCION"
                With cbFiltro.Properties.Items
                    .Add("COD. ORDEN")
                    .Add("SUCURSAL")
                    .Add("PRODUCTO")
                    .Add("ESTADO")
                    .Add("ACTIVO")
                    .Add("TODO")
                End With
                cbFiltro.SelectedIndex = 5
            Case 25 'ENTRADA
                lbTipo.Text = "ORDEN DE ENTRADA"
                Entrada_Salida()
                Tipo = 4
            Case 26 'SALIDA
                lbTipo.Text = "ORDEN DE SALIDA"
                Entrada_Salida()
                Tipo = 5
        End Select
        dtpI.Value = New Date(Now.Year, Now.Month, 1)
        dtpF.Value = New Date(Now.Year, Now.Month, Date.DaysInMonth(Now.Year, Now.Month))
        dgvMaster.BringToFront()
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
    End Sub
    Sub Entrada_Salida()
        With dgvMaster
            With cbFiltro.Properties.Items
                .Add("COD. MOV.")
                .Add("DOCUMENTO")
                .Add("TIPO")
                .Add("BODEGA")
                .Add("TODO")
            End With
            cbFiltro.SelectedIndex = 4
            .Columns(0).HeaderText = "Fecha"
            .Columns(0).Width = 100
            .Columns(1).HeaderText = "Bodega"
            .Columns(1).Width = 200
            .Columns(2).HeaderText = "Codigo"
            .Columns(3).HeaderText = "Documento"
            .Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet
            .Columns(4).HeaderText = "Tipo de Documento"
            .Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet
            .Columns(5).HeaderText = "Comentario"
            .Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(6).HeaderText = "Costo Total"
            .Columns(6).Width = 100
            .Columns(7).Visible = False
            .Columns(8).Visible = False
            .Columns(9).Visible = False
            .Columns(10).Visible = False
            .Columns(11).Visible = False
            .Columns(12).Visible = False
        End With
    End Sub
    Private Sub cbFiltro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFiltro.SelectedIndexChanged
        txtfiltro.Text = ""
        cbFiltro2.Properties.Items.Clear()
        cbFiltro2.SelectedIndex = 0
        If NumCatalogo = 23 Then
            Select Case cbFiltro.SelectedIndex
                Case 0 'codigo orden
                    txtfiltro.ReadOnly = False
                    txtfiltro.Focus()
                    cbFiltro2.Visible = False
                Case 1 'sucursal
                    txtfiltro.ReadOnly = False
                    cbFiltro2.Visible = True
                    cbFiltro2.Focus()
                    cb_Carga()
                Case 2 'producto
                    txtfiltro.ReadOnly = False
                    txtfiltro.Focus()
                    cbFiltro2.Visible = False
                Case 3 'estado
                    txtfiltro.ReadOnly = False
                    cbFiltro2.Visible = True
                    cbFiltro2.Focus()
                    cb_Carga()
                Case 4 'activo
                    txtfiltro.ReadOnly = False
                    cbFiltro2.Visible = True
                    cbFiltro2.Focus()
                    cb_Carga()
                Case 5 'todo
                    txtfiltro.ReadOnly = True
                    cbFiltro2.Visible = False
                    Retrive()
            End Select
        Else
            Select Case cbFiltro.SelectedIndex
                Case 0 'COD. MOV
                    txtfiltro.ReadOnly = False
                    txtfiltro.Focus()
                    cbFiltro2.Visible = False
                Case 1 'DOCUMENTO
                    txtfiltro.ReadOnly = False
                    txtfiltro.Focus()
                    cbFiltro2.Visible = False
                Case 2 'TIPO
                    txtfiltro.ReadOnly = False
                    cbFiltro2.Visible = True
                    cbFiltro2.Focus()
                    cb_Carga2()
                Case 3 'BODEGA
                    txtfiltro.ReadOnly = False
                    cbFiltro2.Visible = True
                    cbFiltro2.Focus()
                    cb_Carga2()
                Case 4 'TODO
                    txtfiltro.ReadOnly = True
                    cbFiltro2.Visible = False
                    Retrive()
            End Select
        End If
    End Sub
    Private Sub txtfiltro_TextChanged(sender As Object, e As EventArgs) Handles txtfiltro.TextChanged
        Retrive()
    End Sub
    Sub Retrive()
        dgvMaster.Visible = True
        pDetalle.Visible = False
        pDetalle2.Visible = False
        dgvMaster.Rows.Clear()
        dgvDetalle.Rows.Clear()
        dgv_Master()
        bandera = True
    End Sub
    Sub cb_Carga()
        Dim adapter As SqlDataAdapter
        Dim dt As DataTable = New DataTable()
        cbFiltro2.Properties.Items.Clear()
        Select Case cbFiltro.SelectedIndex
            Case 1 'sucursal
                sqlstring = "select Nombre from Configuracion.Sucursales"
                cmd = New SqlCommand(sqlstring, con)
                Try
                    con.Open()
                    adapter = New SqlDataAdapter(cmd)
                    adapter.Fill(dt)
                    For Each row0 In dt.Rows
                        cbFiltro2.Properties.Items.Add(row0(0))
                    Next
                    con.Close()
                    dt.Rows.Clear()
                Catch ex As Exception
                    MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
                    con.Close()
                End Try
            Case 3 'estado
                sqlstring = "select Estado from Categorias_Detalle where IdCategoria=1"
                cmd = New SqlCommand(sqlstring, con)
                Try
                    con.Open()
                    adapter = New SqlDataAdapter(cmd)
                    adapter.Fill(dt)
                    For Each row0 In dt.Rows
                        cbFiltro2.Properties.Items.Add(row0(0))
                    Next
                    con.Close()
                    dt.Rows.Clear()
                Catch ex As Exception
                    MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
                    con.Close()
                End Try
            Case 4 'activo
                cbFiltro2.Properties.Items.Add("SI")
                cbFiltro2.Properties.Items.Add("NO")
        End Select
        cbFiltro2.SelectedIndex = 0
    End Sub
    Sub cb_Carga2()
        Dim adapter As SqlDataAdapter
        Dim dt As DataTable = New DataTable()
        cbFiltro2.Properties.Items.Clear()
        cbFiltro2.SelectedIndex = 0
        Select Case cbFiltro.SelectedIndex
            Case 2 'Tipo
                sqlstring = "select Descripcion from Categorias_Detalle where Descripcion is not null"
            Case 3 'BODEGA
                sqlstring = "select Nombre_Bodega from Almacenes"
        End Select
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbFiltro2.Properties.Items.Add(row0(0))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub dgv_Master()
        If NumCatalogo = 23 Then
            Select Case cbFiltro.SelectedIndex
                Case 0 'cod orden
                    sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final and Codigo_Orden_Produc like'" + txtfiltro.Text + "%'"
                Case 1 'sucursal
                    If bandera = True Then
                        sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final and Nombre='" + cbFiltro2.Text + "'"
                    End If
                Case 2 'producto
                    sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final and Nombre_Producto like'%" + txtfiltro.Text + "%'"
                Case 3 'estado
                    If bandera = True Then
                        sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final and Estado='" + cbFiltro2.Text + "'"
                    End If
                Case 4 'activo
                    If bandera = True Then
                        If cbFiltro2.Text = "SI" Then
                            sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final and Activo='1'"
                        Else
                            sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final and Activo='0'"
                        End If
                    End If
                Case 5 'todo
                    sqlstring = "select * from vw_RordenMaster where Registrado >= @inicio and Registrado <=@final"
            End Select
            tryPopulateMaster()
        Else
            Select Case cbFiltro.SelectedIndex
                Case 0 'cod mov
                    sqlstring = "select * from vw_EntradaSalida where Fecha >=@inicio and Fecha <=@final and Tipo_Movimiento=@tipo and codigo like '" + txtfiltro.Text + "%'"
                Case 1 'documento
                    sqlstring = "select * from vw_EntradaSalida where Fecha >=@inicio and Fecha <=@final and Tipo_Movimiento=@tipo and [No.Documento] like '" + txtfiltro.Text + "%'"
                Case 2 'tipo
                    If bandera = True Then
                        sqlstring = "select * from vw_EntradaSalida where Fecha >=@inicio and Fecha <=@final and Tipo_Movimiento=@tipo and [Tipo Documento] = '" + cbFiltro2.Text + "'"
                    End If
                Case 3 'bodega
                    If bandera = True Then
                        sqlstring = "select * from vw_EntradaSalida where Fecha >=@inicio and Fecha <=@final and Tipo_Movimiento=@tipo and Bodega = '" + cbFiltro2.Text + "'"
                    End If
                Case 4 'todo
                    sqlstring = "select * from vw_EntradaSalida where Fecha >=@inicio and Fecha <=@final and Tipo_Movimiento=@tipo"
            End Select
            tryPopulateMaster2()
        End If
    End Sub
    Sub tryPopulateMaster()
        Dim adapter0 As SqlDataAdapter
        Dim dt0 As DataTable = New DataTable
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@inicio", dtpI.Value)
            cmd.Parameters.AddWithValue("@final", dtpF.Value)
            cmd.ExecuteNonQuery()

            adapter0 = New SqlDataAdapter(cmd)
            adapter0.Fill(dt0)
            For Each row0 In dt0.Rows
                populateM(row0(0), row0(1), row0(2), row0(3), row0(4), row0(5), row0(6), row0(7), row0(8), row0(9), row0(10), row0(11), row0(12))
            Next
            con.Close()
            dt0.Rows.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Sub
    Sub tryPopulateMaster2()
        Dim adapter0 As SqlDataAdapter
        Dim dt0 As DataTable = New DataTable
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@inicio", dtpI.Value)
            cmd.Parameters.AddWithValue("@final", dtpF.Value)
            cmd.Parameters.AddWithValue("@tipo", Tipo)
            cmd.ExecuteNonQuery()
            adapter0 = New SqlDataAdapter(cmd)
            adapter0.Fill(dt0)
            For Each row0 In dt0.Rows
                populateM2(row0(0), row0(1), row0(2), row0(3), row0(4), row0(5), row0(6))
            Next
            con.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try

    End Sub
    Sub tryPopulateDetalle()
        Dim adapter0 As SqlDataAdapter
        Dim dt0 As DataTable = New DataTable
        Try
            con.Open()
            adapter0 = New SqlDataAdapter(cmd)
            adapter0.Fill(dt0)
            For Each row0 In dt0.Rows
                populateD(row0(0), row0(1), row0(2), row0(3), row0(4))
            Next
            con.Close()
            dt0.Rows.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Sub
    Sub tryPopulateDetalle2()
        Dim adapater0 As SqlDataAdapter
        Dim dt0 As DataTable = New DataTable
        Try
            con.Open()
            adapater0 = New SqlDataAdapter(cmd)
            adapater0.Fill(dt0)
            For Each row0 In dt0.Rows
                populateD2(row0(0), row0(1), row0(2), row0(3), row0(4))
            Next
            con.Close()
            dt0.Rows.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Sub
    Sub populateM(id As String, sucursal As String, codigoP As String, producto As String, supervisor As String, cRequerido As Decimal, cantRequerida As Decimal, cElaborado As Decimal, cantElaborado As Decimal, Registro As Date, Cierre As String, Estado As String, activo As Boolean)
        Dim row0 As String() = {id, sucursal, codigoP, producto, supervisor, cRequerido, cantRequerida, cElaborado, cantElaborado, Registro, Cierre, Estado, activo}
        dgvMaster.Rows.Add(row0)
    End Sub
    Sub populateM2(fecha As Date, Bodega As String, Codigo As String, Documento As String, Tipo As String, Comentario As String, Total As Decimal)
        Dim row0 As String() = {fecha, Bodega, Codigo, Documento, Tipo, Comentario, Total}
        dgvMaster.Rows.Add(row0)
    End Sub
    Sub populateD(codigo As String, producto As String, cantRequerida As Decimal, cantNoutilizada As Decimal, Costo As Decimal)
        Dim row0 As String() = {codigo, producto, cantRequerida, cantNoutilizada, Costo}
        dgvDetalle.Rows.Add(row0)
    End Sub
    Sub populateD2(Codigo As String, producto As String, Cantidad As Decimal, costo As Decimal, costoTotal As Decimal)
        Dim row0 As String() = {Codigo, producto, Cantidad, costo, costoTotal}
        dgvDetalle2.Rows.Add(row0)
    End Sub
    Sub dgv_Detalle()
        Try
            If NumCatalogo = 23 Then
                dgvDetalle.Rows.Clear()
                txtOrden.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(0).Value
                txtSucursal.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(1).Value
                txtSupervisor.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(4).Value
                txtCodProducto.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(2).Value
                txtProducto.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(3).Value
                dtpFecha.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(9).Value
                txtEstado.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(11).Value
                sqlstring = "select Cod_Producto_Detalle, Productos.Nombre_Producto, Cantidad_Requerida, Cantidad_NoUtilizada,Costo_Unitario from Orden_Produccion_Detalle inner join Productos on Productos.Codigo_Producto=Orden_Produccion_Detalle.Cod_Producto_Detalle where Codigo_Orden_Produc='" + txtOrden.Text + "' and CodigoSucursal='" & My.Settings.Sucursal & "'"
                cmd = New SqlCommand(sqlstring, con)
                tryPopulateDetalle()
                pDetalle.Visible = True
            Else
                dgvDetalle2.Rows.Clear()
                txtcodigo.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(2).Value
                txtbodega.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(1).Value
                txtDoc.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(3).Value
                txtComentario.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(5).Value
                txtTipoDoc.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(4).Value
                dtpFecha2.Value = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(0).Value
                txtTotal2.Text = dgvMaster.Rows(dgvMaster.CurrentRow.Index).Cells(6).Value
                If Tipo = 4 Then
                    txtTipoMov.Text = "ENTRADA"
                Else
                    txtTipoMov.Text = "SALIDA"
                End If
                sqlstring = "select * from vw_EntradaSalida_Detalle where Codigo_movimiento='" + txtcodigo.Text + "' and bodega=(select Codigo_Bodega from Almacenes where Nombre_Bodega='" + txtbodega.Text + "') and Tipo_Movimiento ='" & Tipo & "'"
                cmd = New SqlCommand(sqlstring, con)
                tryPopulateDetalle2()
                pDetalle2.Visible = True
            End If
            dgvMaster.Visible = False
        Catch ex As Exception
        End Try

    End Sub

    Private Sub RordenProduccion_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pmain.Location = New Point(Me.ClientSize.Width / 2 - Me.pmain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pmain.Size.Height / 2)
        Me.pmain.Anchor = AnchorStyles.None
    End Sub

    Private Sub dtpI_ValueChanged(sender As Object, e As EventArgs) Handles dtpI.ValueChanged, dtpF.ValueChanged
        If bandera = True Then
            Retrive()
        End If
    End Sub

    Private Sub dgvMaster_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvMaster.CellMouseDoubleClick
        dgv_Detalle()
    End Sub
    Private Sub dgvDetalle_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvDetalle.CellMouseDoubleClick
        dgvMaster.Visible = True
        pDetalle.Visible = False
    End Sub
    Private Sub dgvDetalle2_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvDetalle2.CellMouseDoubleClick
        dgvMaster.Visible = True
        pDetalle2.Visible = False
    End Sub
    Private Sub cbFiltro2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFiltro2.SelectedIndexChanged
        Retrive()
    End Sub
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If NumCatalogo = 23 Then
            If pDetalle.Visible = True Then
                CalculoD()
                Iprint()
            Else
                CalculoM()
                Gprint()
            End If
        Else 'entrada/salida
            If pDetalle2.Visible = True Then
                CalculoD2()
                Iprint2()
            Else
                CalculoM2()
                Gprint2()
            End If
        End If
    End Sub
    Private Sub CalculoM()
        CostoReq = 0
        CantReq = 0
        CostoElab = 0
        CantElab = 0
        For Each row In dgvMaster.Rows
            CostoReq += dgvMaster.Rows(row.index).Cells(5).Value
            CantReq += dgvMaster.Rows(row.index).Cells(6).Value
            CostoElab += dgvMaster.Rows(row.index).Cells(7).Value
            CantElab += dgvMaster.Rows(row.index).Cells(8).Value
        Next
    End Sub
    Private Sub CalculoM2()
        CostoReq = 0
        For Each row In dgvMaster.Rows
            CostoReq += dgvMaster.Rows(row.index).Cells(6).Value
        Next
    End Sub
    Private Sub CalculoD()
        CantReq = 0
        CostoElab = 0
        CantElab = 0
        For Each row In dgvDetalle.Rows
            CantReq += dgvDetalle.Rows(row.index).Cells(2).Value
            CostoElab += dgvDetalle.Rows(row.index).Cells(3).Value
            CantElab += dgvDetalle.Rows(row.index).Cells(4).Value
        Next
    End Sub
    Private Sub CalculoD2()
        CostoReq = 0
        For Each row In dgvDetalle2.Rows
            CostoReq += dgvDetalle2.Rows(row.index).Cells(4).Value
        Next
    End Sub
    Sub Iprint()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reporte Orden.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reporte_Orden, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reporte Orden.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Detalle")

        xlWorkSheet.Cells(2, 1) = txtOrden.Text
        xlWorkSheet.Cells(4, 1) = txtCodProducto.Text + " " + txtProducto.Text
        xlWorkSheet.Cells(5, 1) = txtSupervisor.Text
        xlWorkSheet.Cells(6, 1) = dtpFecha.Text
        xlWorkSheet.Cells(4, 11) = txtSucursal.Text
        xlWorkSheet.Cells(5, 11) = txtEstado.Text

        Dim i As Integer = 1
        Dim f As Integer = 9
        Do While f <= 45
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 11) = ""
            f = f + 1
        Loop
        f = 9
        Do While i <= dgvDetalle.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvDetalle.Rows((i - 1)).Cells(0).Value 'detalle
            xlWorkSheet.Cells(f, 2) = dgvDetalle.Rows((i - 1)).Cells(1).Value 'codigo_p
            xlWorkSheet.Cells(f, 7) = dgvDetalle.Rows((i - 1)).Cells(2).Value 'Producto
            xlWorkSheet.Cells(f, 9) = dgvDetalle.Rows((i - 1)).Cells(3).Value 'costo
            xlWorkSheet.Cells(f, 11) = dgvDetalle.Rows((i - 1)).Cells(4).Value 'cantidad
            i = i + 1
            f = f + 1
        Loop
        xlWorkSheet.Cells(46, 7) = CantReq
        xlWorkSheet.Cells(46, 9) = CostoElab
        xlWorkSheet.Cells(46, 11) = CantElab
        xlWorkBook.Application.DisplayAlerts = False
        If expo = True Then
            Using sfd As New SaveFileDialog
                xlApp.DisplayAlerts = False
                sfd.Title = "Guardar archivo de excel"
                sfd.OverwritePrompt = True
                sfd.DefaultExt = ".xlsx"
                sfd.Filter = "Archivo de Excel(*xlsx)|"
                sfd.AddExtension = True
                If sfd.ShowDialog() = DialogResult.OK Then
                    xlWorkBook.SaveAs(sfd.FileName)
                Else
                End If
             xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)

                Dim res As MsgBoxResult
                res = MsgBox("Archivo guardado en : " + sfd.FileName + ", desea abrir el archivo?", MsgBoxStyle.YesNo)
                If (res = MsgBoxResult.Yes) Then
                    Process.Start(sfd.FileName)
                Else
                    Try
                        xlWorkBook.Save()
                    Catch ex As Exception
                        xlWorkBook.Close()
                        xlApp.Quit()
                        releaseObject(xlApp)
                        releaseObject(xlWorkBook)
                        releaseObject(xlWorkSheet)
                    End Try
                    '-----------------PRINT EXCEL--------------------------------
                    xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
                    'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
                    '------------------------------------------------------------
                    xlWorkBook.Close()
                    xlApp.Quit()
                    releaseObject(xlApp)
                    releaseObject(xlWorkBook)
                    releaseObject(xlWorkSheet)
                End If
            End Using
        Else
            Try
                xlWorkBook.Save()
            Catch ex As Exception
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)
            End Try
            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End If
    End Sub
    Sub Iprint2()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reportes.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reportes, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reportes.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("EntradaSalidaD")
        xlWorkSheet.Cells(1, 8) = lbTipo.Text
        xlWorkSheet.Cells(4, 1) = "DOC: " + txtDoc.Text + " " + txtComentario.Text
        xlWorkSheet.Cells(5, 1) = txtTipoDoc.Text
        xlWorkSheet.Cells(2, 8) = dtpFecha2.Text
        xlWorkSheet.Cells(3, 8) = "TIPO: " + txtTipoMov.Text
        xlWorkSheet.Cells(4, 8) = "BODEGA: " + txtbodega.Text
        xlWorkSheet.Cells(5, 8) = "CODIGO: " + txtcodigo.Text
        Dim i As Integer = 1
        Dim f As Integer = 8
        Do While f <= 45
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            f = f + 1
        Loop
        f = 8
        Do While i <= dgvDetalle2.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvDetalle2.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvDetalle2.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 6) = dgvDetalle2.Rows((i - 1)).Cells(2).Value
            xlWorkSheet.Cells(f, 7) = dgvDetalle2.Rows((i - 1)).Cells(3).Value
            xlWorkSheet.Cells(f, 8) = dgvDetalle2.Rows((i - 1)).Cells(4).Value
            i = i + 1
            f = f + 1
        Loop
        xlWorkSheet.Cells(46, 8) = CostoReq
        xlWorkBook.Application.DisplayAlerts = False
        If expo = True Then
            Using sfd As New SaveFileDialog
                xlApp.DisplayAlerts = False
                sfd.Title = "Guardar archivo de excel"
                sfd.OverwritePrompt = True
                sfd.DefaultExt = ".xlsx"
                sfd.Filter = "Archivo de Excel(*xlsx)|"
                sfd.AddExtension = True
                If sfd.ShowDialog() = DialogResult.OK Then
                    xlWorkBook.SaveAs(sfd.FileName)
                Else
                End If
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)

                Dim res As MsgBoxResult
                res = MsgBox("Archivo guardado en : " + sfd.FileName + ", desea abrir el archivo?", MsgBoxStyle.YesNo)
                If (res = MsgBoxResult.Yes) Then
                    Process.Start(sfd.FileName)
                Else
                    Try
                        xlWorkBook.Save()
                    Catch ex As Exception
                        xlWorkBook.Close()
                        xlApp.Quit()
                        releaseObject(xlApp)
                        releaseObject(xlWorkBook)
                        releaseObject(xlWorkSheet)
                    End Try
                    '-----------------PRINT EXCEL--------------------------------
                    xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
                    'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
                    '------------------------------------------------------------
                    xlWorkBook.Close()
                    xlApp.Quit()
                    releaseObject(xlApp)
                    releaseObject(xlWorkBook)
                    releaseObject(xlWorkSheet)
                End If
            End Using
        Else
            Try
                xlWorkBook.Save()
            Catch ex As Exception
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)
            End Try
            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End If
        'Try
        '    xlWorkBook.Save()
        'Catch ex As Exception
        '    xlWorkBook.Close()
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'End Try
        ''-----------------PRINT EXCEL--------------------------------
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        ''------------------------------------------------------------
        'xlWorkBook.Close()
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)
    End Sub
    Sub Gprint()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reporte Orden.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reporte_Orden, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reporte Orden.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("General")
        xlWorkSheet.Cells(3, 1) = "FECHA: " + dtpI.Text + "  a  " + dtpF.Text
        Dim i As Integer = 1
        Dim f As Integer = 6
        Do While f <= 33
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 4) = ""
            xlWorkSheet.Cells(f, 5) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            xlWorkSheet.Cells(f, 12) = ""
            xlWorkSheet.Cells(f, 13) = ""
            xlWorkSheet.Cells(f, 14) = ""
            f = f + 1
        Loop
        f = 6
        Do While i <= dgvMaster.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvMaster.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvMaster.Rows((i - 1)).Cells(3).Value + " " + dgvMaster.Rows((i - 1)).Cells(4).Value
            xlWorkSheet.Cells(f, 7) = dgvMaster.Rows((i - 1)).Cells(9).Value
            xlWorkSheet.Cells(f, 8) = dgvMaster.Rows((i - 1)).Cells(10).Value
            xlWorkSheet.Cells(f, 9) = dgvMaster.Rows((i - 1)).Cells(11).Value
            If dgvMaster.Rows((i - 1)).Cells(12).Value = True Then
                xlWorkSheet.Cells(f, 10) = "SI"
            Else
                xlWorkSheet.Cells(f, 10) = "NO"
            End If
            xlWorkSheet.Cells(f, 11) = dgvMaster.Rows((i - 1)).Cells(5).Value
            xlWorkSheet.Cells(f, 12) = dgvMaster.Rows((i - 1)).Cells(6).Value
            xlWorkSheet.Cells(f, 13) = dgvMaster.Rows((i - 1)).Cells(7).Value
            xlWorkSheet.Cells(f, 14) = dgvMaster.Rows((i - 1)).Cells(8).Value
            i = i + 1
            f = f + 1
        Loop
        xlWorkSheet.Cells(34, 11) = CostoReq
        xlWorkSheet.Cells(34, 12) = CantReq
        xlWorkSheet.Cells(34, 13) = CostoElab
        xlWorkSheet.Cells(34, 14) = CantElab
        xlWorkBook.Application.DisplayAlerts = False
        If expo = True Then
            Using sfd As New SaveFileDialog
                xlApp.DisplayAlerts = False
                sfd.Title = "Guardar archivo de excel"
                sfd.OverwritePrompt = True
                sfd.DefaultExt = ".xlsx"
                sfd.Filter = "Archivo de Excel(*xlsx)|"
                sfd.AddExtension = True
                If sfd.ShowDialog() = DialogResult.OK Then
                    xlWorkBook.SaveAs(sfd.FileName)
                Else
                End If
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)

                Dim res As MsgBoxResult
                res = MsgBox("Archivo guardado en : " + sfd.FileName + ", desea abrir el archivo?", MsgBoxStyle.YesNo)
                If (res = MsgBoxResult.Yes) Then
                    Process.Start(sfd.FileName)
                Else
                    Try
                        xlWorkBook.Save()
                    Catch ex As Exception
                        xlWorkBook.Close()
                        xlApp.Quit()
                        releaseObject(xlApp)
                        releaseObject(xlWorkBook)
                        releaseObject(xlWorkSheet)
                    End Try
                    '-----------------PRINT EXCEL--------------------------------
                    xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
                    'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
                    '------------------------------------------------------------
                    xlWorkBook.Close()
                    xlApp.Quit()
                    releaseObject(xlApp)
                    releaseObject(xlWorkBook)
                    releaseObject(xlWorkSheet)
                End If
            End Using
        Else
            Try
                xlWorkBook.Save()
            Catch ex As Exception
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)
            End Try
            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End If
        'Try
        '    xlWorkBook.Save()
        'Catch ex As Exception
        '    xlWorkBook.Close()
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'End Try
        ''-----------------PRINT EXCEL--------------------------------
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        ''------------------------------------------------------------
        'xlWorkBook.Close()
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)
    End Sub
    Private Sub Gprint2()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reportes.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reportes, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reportes.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("EntradaSalida")
        xlWorkSheet.Cells(1, 12) = lbTipo.Text
        xlWorkSheet.Cells(3, 12) = "FECHA INICIAL: " + dtpI.Text
        xlWorkSheet.Cells(4, 12) = "FECHA FINAL: " + dtpF.Text
        Dim i As Integer = 1
        Dim f As Integer = 7
        Do While f <= 33
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 4) = ""
            xlWorkSheet.Cells(f, 5) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            f = f + 1
        Loop
        f = 7
        Do While i <= dgvMaster.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvMaster.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvMaster.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 4) = dgvMaster.Rows((i - 1)).Cells(2).Value
            xlWorkSheet.Cells(f, 5) = dgvMaster.Rows((i - 1)).Cells(3).Value
            xlWorkSheet.Cells(f, 6) = dgvMaster.Rows((i - 1)).Cells(4).Value
            xlWorkSheet.Cells(f, 9) = dgvMaster.Rows((i - 1)).Cells(5).Value
            xlWorkSheet.Cells(f, 12) = dgvMaster.Rows((i - 1)).Cells(6).Value
            i = i + 1
            f = f + 1
        Loop
        xlWorkSheet.Cells(34, 12) = CostoReq
        xlWorkBook.Application.DisplayAlerts = False
        If expo = True Then
            Using sfd As New SaveFileDialog
                xlApp.DisplayAlerts = False
                sfd.Title = "Guardar archivo de excel"
                sfd.OverwritePrompt = True
                sfd.DefaultExt = ".xlsx"
                sfd.Filter = "Archivo de Excel(*xlsx)|"
                sfd.AddExtension = True
                If sfd.ShowDialog() = DialogResult.OK Then
                    xlWorkBook.SaveAs(sfd.FileName)
                Else
                End If
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)

                Dim res As MsgBoxResult
                res = MsgBox("Archivo guardado en : " + sfd.FileName + ", desea abrir el archivo?", MsgBoxStyle.YesNo)
                If (res = MsgBoxResult.Yes) Then
                    Process.Start(sfd.FileName)
                Else
                    Try
                        xlWorkBook.Save()
                    Catch ex As Exception
                        xlWorkBook.Close()
                        xlApp.Quit()
                        releaseObject(xlApp)
                        releaseObject(xlWorkBook)
                        releaseObject(xlWorkSheet)
                    End Try
                    '-----------------PRINT EXCEL--------------------------------
                    xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
                    'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
                    '------------------------------------------------------------
                    xlWorkBook.Close()
                    xlApp.Quit()
                    releaseObject(xlApp)
                    releaseObject(xlWorkBook)
                    releaseObject(xlWorkSheet)
                End If
            End Using
        Else
            Try
                xlWorkBook.Save()
            Catch ex As Exception
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)
            End Try
            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End If
        'Try
        '    xlWorkBook.Save()
        'Catch ex As Exception
        '    xlWorkBook.Close()
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'End Try
        ''-----------------PRINT EXCEL--------------------------------
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        ''------------------------------------------------------------
        'xlWorkBook.Close()
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        expo = True
        If NumCatalogo = 23 Then
            If pDetalle.Visible = True Then
                CalculoD()
                Iprint()
            Else
                CalculoM()
                Gprint()
            End If
        Else 'entrada/salida
            If pDetalle2.Visible = True Then
                CalculoD2()
                Iprint2()
            Else
                CalculoM2()
                Gprint2()
            End If
        End If
    End Sub
End Class