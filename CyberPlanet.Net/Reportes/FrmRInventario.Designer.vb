﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRInventario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pmain = New System.Windows.Forms.Panel()
        Me.lbBodega = New System.Windows.Forms.ListBox()
        Me.lbProveedor = New System.Windows.Forms.ListBox()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.lbCategoria = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbLinea = New System.Windows.Forms.ListBox()
        Me.lbTipo = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpF = New System.Windows.Forms.DateTimePicker()
        Me.dtpI = New System.Windows.Forms.DateTimePicker()
        Me.btnOcultar = New System.Windows.Forms.Button()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.pmain.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pmain
        '
        Me.pmain.Controls.Add(Me.Label6)
        Me.pmain.Controls.Add(Me.Label5)
        Me.pmain.Controls.Add(Me.Label4)
        Me.pmain.Controls.Add(Me.lbBodega)
        Me.pmain.Controls.Add(Me.lbProveedor)
        Me.pmain.Controls.Add(Me.GridControl1)
        Me.pmain.Controls.Add(Me.lbCategoria)
        Me.pmain.Controls.Add(Me.Label2)
        Me.pmain.Controls.Add(Me.lbLinea)
        Me.pmain.Controls.Add(Me.lbTipo)
        Me.pmain.Controls.Add(Me.Label54)
        Me.pmain.Controls.Add(Me.Label3)
        Me.pmain.Controls.Add(Me.Panel2)
        Me.pmain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pmain.Location = New System.Drawing.Point(0, 0)
        Me.pmain.Name = "pmain"
        Me.pmain.Size = New System.Drawing.Size(1339, 532)
        Me.pmain.TabIndex = 0
        '
        'lbBodega
        '
        Me.lbBodega.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbBodega.FormattingEnabled = True
        Me.lbBodega.Location = New System.Drawing.Point(17, 25)
        Me.lbBodega.Name = "lbBodega"
        Me.lbBodega.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbBodega.Size = New System.Drawing.Size(196, 108)
        Me.lbBodega.TabIndex = 165
        '
        'lbProveedor
        '
        Me.lbProveedor.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbProveedor.FormattingEnabled = True
        Me.lbProveedor.Location = New System.Drawing.Point(623, 25)
        Me.lbProveedor.Name = "lbProveedor"
        Me.lbProveedor.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbProveedor.Size = New System.Drawing.Size(196, 108)
        Me.lbProveedor.TabIndex = 168
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(16, 139)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1319, 381)
        Me.GridControl1.TabIndex = 164
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.PaintStyleName = "UltraFlat"
        '
        'lbCategoria
        '
        Me.lbCategoria.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbCategoria.FormattingEnabled = True
        Me.lbCategoria.Location = New System.Drawing.Point(421, 25)
        Me.lbCategoria.Name = "lbCategoria"
        Me.lbCategoria.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbCategoria.Size = New System.Drawing.Size(196, 108)
        Me.lbCategoria.TabIndex = 167
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 161
        Me.Label2.Text = "Bodega"
        '
        'lbLinea
        '
        Me.lbLinea.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lbLinea.FormattingEnabled = True
        Me.lbLinea.Location = New System.Drawing.Point(219, 25)
        Me.lbLinea.Name = "lbLinea"
        Me.lbLinea.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbLinea.Size = New System.Drawing.Size(196, 108)
        Me.lbLinea.TabIndex = 166
        '
        'lbTipo
        '
        Me.lbTipo.AutoSize = True
        Me.lbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTipo.Location = New System.Drawing.Point(1156, 2)
        Me.lbTipo.Name = "lbTipo"
        Me.lbTipo.Size = New System.Drawing.Size(90, 16)
        Me.lbTipo.TabIndex = 160
        Me.lbTipo.Text = "INVENTARIO"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(1087, 3)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(62, 13)
        Me.Label54.TabIndex = 159
        Me.Label54.Text = "REPORTE:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label3.Location = New System.Drawing.Point(1, -29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(1338, 42)
        Me.Label3.TabIndex = 158
        Me.Label3.Text = "__________________________________________________________________"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.dtpF)
        Me.Panel2.Controls.Add(Me.dtpI)
        Me.Panel2.Controls.Add(Me.btnOcultar)
        Me.Panel2.Controls.Add(Me.btnGenerar)
        Me.Panel2.Controls.Add(Me.cbTipo)
        Me.Panel2.Controls.Add(Me.Label53)
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Me.btnExportar)
        Me.Panel2.Controls.Add(Me.btnPrint)
        Me.Panel2.Location = New System.Drawing.Point(825, 25)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(510, 108)
        Me.Panel2.TabIndex = 169
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(42, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 16)
        Me.Label1.TabIndex = 176
        Me.Label1.Text = "Range de Fecha:"
        '
        'dtpF
        '
        Me.dtpF.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpF.CustomFormat = ""
        Me.dtpF.Enabled = False
        Me.dtpF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF.Location = New System.Drawing.Point(271, 18)
        Me.dtpF.Name = "dtpF"
        Me.dtpF.Size = New System.Drawing.Size(105, 20)
        Me.dtpF.TabIndex = 175
        '
        'dtpI
        '
        Me.dtpI.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpI.CustomFormat = ""
        Me.dtpI.Enabled = False
        Me.dtpI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpI.Location = New System.Drawing.Point(160, 18)
        Me.dtpI.Name = "dtpI"
        Me.dtpI.Size = New System.Drawing.Size(105, 20)
        Me.dtpI.TabIndex = 174
        '
        'btnOcultar
        '
        Me.btnOcultar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOcultar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnOcultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOcultar.Location = New System.Drawing.Point(382, 17)
        Me.btnOcultar.Name = "btnOcultar"
        Me.btnOcultar.Size = New System.Drawing.Size(90, 23)
        Me.btnOcultar.TabIndex = 173
        Me.btnOcultar.Text = "Ocultar"
        Me.btnOcultar.UseVisualStyleBackColor = False
        '
        'btnGenerar
        '
        Me.btnGenerar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGenerar.BackColor = System.Drawing.Color.Orange
        Me.btnGenerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerar.Location = New System.Drawing.Point(160, 75)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(105, 23)
        Me.btnGenerar.TabIndex = 172
        Me.btnGenerar.Text = "Generar Informe"
        Me.btnGenerar.UseVisualStyleBackColor = False
        '
        'cbTipo
        '
        Me.cbTipo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbTipo.EditValue = "LEVANTAMIENTO FISICO"
        Me.cbTipo.Location = New System.Drawing.Point(160, 48)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.cbTipo.Properties.Appearance.Options.UseForeColor = True
        Me.cbTipo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTipo.Properties.Items.AddRange(New Object() {"LEVANTAMIENTO FISICO", "RESUMEN DE EXISTENCIA Y COSTO", "RESUMEN DE EXISTENCIA", "PRECIO DE FACTURACION MENOR AL COSTO", "EXISTENCIAS POR DEBAJO DEL MINIMO", "ENTRADA SALIDA"})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(216, 20)
        Me.cbTipo.TabIndex = 171
        Me.cbTipo.TabStop = False
        '
        'Label53
        '
        Me.Label53.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(67, 51)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(87, 13)
        Me.Label53.TabIndex = 170
        Me.Label53.Text = "Tipo de Reporte:"
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(271, 75)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(105, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Cancelar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'btnExportar
        '
        Me.btnExportar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExportar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnExportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportar.Location = New System.Drawing.Point(382, 75)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(90, 23)
        Me.btnExportar.TabIndex = 6
        Me.btnExportar.Text = "Exportar Excel"
        Me.btnExportar.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Location = New System.Drawing.Point(382, 46)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(90, 23)
        Me.btnPrint.TabIndex = 5
        Me.btnPrint.Text = "Imprimir"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(216, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 170
        Me.Label4.Text = "Linea"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(418, 4)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 171
        Me.Label5.Text = "Categoria"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(620, 4)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 172
        Me.Label6.Text = "Proveedor"
        '
        'FrmRInventario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1339, 532)
        Me.Controls.Add(Me.pmain)
        Me.Name = "FrmRInventario"
        Me.pmain.ResumeLayout(False)
        Me.pmain.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pmain As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbTipo As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lbBodega As System.Windows.Forms.ListBox
    Friend WithEvents lbCategoria As System.Windows.Forms.ListBox
    Friend WithEvents lbLinea As System.Windows.Forms.ListBox
    Friend WithEvents lbProveedor As System.Windows.Forms.ListBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnExportar As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents btnOcultar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpF As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpI As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
