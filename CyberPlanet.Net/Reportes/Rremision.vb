﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
Public Class Rremision
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As SqlCommand
    Dim sqlstring As String
    '----Print-----
    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    Dim misValue As Object = System.Reflection.Missing.Value
    Dim lectura As SqlDataReader
    Dim bandera As Boolean = False
    Dim expo As Boolean = False
    '-------CALCULO----------
    Private Sub Rremision_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpI.Value = New Date(Now.Year, Now.Month, 1)
        dtpF.Value = New Date(Now.Year, Now.Month, Date.DaysInMonth(Now.Year, Now.Month))
        'cbFiltro2.SelectedIndex = 0
        cbFiltro.SelectedIndex = 4
        dgvMaster.BringToFront()
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
    End Sub

    Private Sub cbFiltro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFiltro.SelectedIndexChanged
        txtFiltro.Text = ""
        Select Case cbFiltro.SelectedIndex
            Case 0 'Cod remision
                txtFiltro.ReadOnly = False
                txtFiltro.Focus()
                cbFiltro2.Visible = False
            Case 1 'Supervisor
                txtFiltro.ReadOnly = False
                txtFiltro.Focus()
                cbFiltro2.Visible = False
            Case 2 'Vehiculo
                txtFiltro.ReadOnly = False
                txtFiltro.Focus()
                cbFiltro2.Visible = False
            Case 3 ' Estado
                txtFiltro.ReadOnly = False
                txtFiltro.Focus()
                cbFiltro2.Visible = True
            Case 4 'Todo
                txtFiltro.ReadOnly = True
                txtFiltro.Text = ""
                cbFiltro2.Visible = False
        End Select
        Retrive()
    End Sub
    Private Sub txtFiltro_TextChanged(sender As Object, e As EventArgs) Handles txtFiltro.TextChanged
        Retrive()
    End Sub
    Sub Retrive()
        dgvMaster.Rows.Clear()
        dgvDetalle.Rows.Clear()
        dgv_Master()
        bandera = True
        'dgv_Detalle()
        '-----Caluclos------------
    End Sub
    Sub dgv_Master()
        Select Case cbFiltro.SelectedIndex
            Case 0 ' cod. Remsion
                sqlstring = "select distinct Tbl_Remision.Id_Remision, Id_Remision2, Tbl_Remision.Fecha, Configuracion.Sucursales.Nombre, Tbl_Empleados.Nombres, Tbl_Vehiculo.Nombre_Ruta, (select sum(([Tbl_Remision.Detalle].costo)*Cantidad) from [Tbl_Remision.Detalle] where [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision) as 'Costo Total', (case when Tbl_Remision.Estado=0 then 'EMITIDA' when Tbl_Remision.Estado=1 then 'PREFACTURADA' when Tbl_Remision.Estado=2 then 'CERRADA' when Tbl_Remision.Estado=3 then 'ANULADA' end) as 'ESTADO' from Tbl_Remision inner join [Tbl_Remision.Detalle] on [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Tbl_Empleados on Tbl_Empleados.Id_Empleado=Tbl_Remision.Id_Supervisor inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo where Tbl_Remision.Fecha >= @inicio and Tbl_Remision.Fecha <=@final and Tbl_Remision.Id_Remision LIKE '%" + txtFiltro.Text + "%'"
            Case 1 ' Supervisor
                sqlstring = "select distinct Tbl_Remision.Id_Remision, Id_Remision2, Tbl_Remision.Fecha, Configuracion.Sucursales.Nombre, Tbl_Empleados.Nombres, Tbl_Vehiculo.Nombre_Ruta, (select sum(([Tbl_Remision.Detalle].costo)*Cantidad) from [Tbl_Remision.Detalle] where [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision) as 'Costo Total', (case when Tbl_Remision.Estado=0 then 'EMITIDA' when Tbl_Remision.Estado=1 then 'PREFACTURADA' when Tbl_Remision.Estado=2 then 'CERRADA' when Tbl_Remision.Estado=3 then 'ANULADA' end) as 'ESTADO' from Tbl_Remision inner join [Tbl_Remision.Detalle] on [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Tbl_Empleados on Tbl_Empleados.Id_Empleado=Tbl_Remision.Id_Supervisor inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo where Tbl_Remision.Fecha >= @inicio and Tbl_Remision.Fecha <=@final and Id_Supervisor=( select Codigo_Supervisor where Nombre_Supervisor LIKE '%" + txtFiltro.Text + "%')"
            Case 2 ' Vehiculo
                sqlstring = "select distinct Tbl_Remision.Id_Remision, Id_Remision2, Tbl_Remision.Fecha, Configuracion.Sucursales.Nombre, Tbl_Empleados.Nombres, Tbl_Vehiculo.Nombre_Ruta, (select sum(([Tbl_Remision.Detalle].costo)*Cantidad) from [Tbl_Remision.Detalle] where [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision) as 'Costo Total', (case when Tbl_Remision.Estado=0 then 'EMITIDA' when Tbl_Remision.Estado=1 then 'PREFACTURADA' when Tbl_Remision.Estado=2 then 'CERRADA' when Tbl_Remision.Estado=3 then 'ANULADA' end) as 'ESTADO' from Tbl_Remision inner join [Tbl_Remision.Detalle] on [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Tbl_Empleados on Tbl_Empleados.Id_Empleado=Tbl_Remision.Id_Supervisor inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo where Tbl_Remision.Fecha >= @inicio and Tbl_Remision.Fecha <=@final and Tbl_Remision.Id_Vehiculo ='" + txtFiltro.Text + "'"
            Case 3 ' Estado
                'cb aparte
            Case 4 ' Todo
                sqlstring = "select distinct Tbl_Remision.Id_Remision, Id_Remision2, Tbl_Remision.Fecha, Configuracion.Sucursales.Nombre, Tbl_Empleados.Nombres, Tbl_Vehiculo.Nombre_Ruta, (select sum(([Tbl_Remision.Detalle].costo)*Cantidad) from [Tbl_Remision.Detalle] where [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision) as 'Costo Total', (case when Tbl_Remision.Estado=0 then 'EMITIDA' when Tbl_Remision.Estado=1 then 'PREFACTURADA' when Tbl_Remision.Estado=2 then 'CERRADA' when Tbl_Remision.Estado=3 then 'ANULADA' end) as 'ESTADO' from Tbl_Remision inner join [Tbl_Remision.Detalle] on [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Tbl_Empleados on Tbl_Empleados.Id_Empleado=Tbl_Remision.Id_Supervisor inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo where Tbl_Remision.Fecha >= @inicio and Tbl_Remision.Fecha <=@final"
        End Select
        tryPopulateMaster()
    End Sub
    Sub tryPopulateMaster()
        Dim adapter0 As SqlDataAdapter
        Dim dt0 As DataTable = New DataTable
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@inicio", dtpI.Value)
            cmd.Parameters.AddWithValue("@final", dtpF.Value)
            cmd.ExecuteNonQuery()

            adapter0 = New SqlDataAdapter(cmd)
            adapter0.Fill(dt0)
            For Each row0 In dt0.Rows
                populateM(row0(0), row0(1), row0(2), row0(3), row0(4), row0(5), row0(6), row0(7))
            Next
            con.Close()
            dt0.Rows.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Sub
    Sub dgv_Detalle()
        dgvDetalle.Rows.Clear()
        pDetalle.Visible = True
        dgvMaster.Visible = False
        Try
            txtRemision.Text = dgvMaster.CurrentRow.Cells(0).Value
            txtSucursal.Text = dgvMaster.CurrentRow.Cells(3).Value
            txtSupervisor.Text = dgvMaster.CurrentRow.Cells(4).Value
            txtVehiculo.Text = dgvMaster.CurrentRow.Cells(5).Value
            dtpFecha.Value = dgvMaster.CurrentRow.Cells(2).Value
            txtEstado.Text = dgvMaster.CurrentRow.Cells(7).Value
            sqlstring = "select Id_Remsion_Detalle, Id_Producto, Productos.Nombre_Producto, Costo, Cantidad,Cant_vendida from [Tbl_Remision.Detalle] inner join Productos on Productos.Codigo_Producto=[Tbl_Remision.Detalle].Id_Producto where [Tbl_Remision.Detalle].Id_Remision='" + txtRemision.Text + "'"
            cmd = New SqlCommand(sqlstring, con)
            tryPopulateDetalle()
        Catch ex As Exception
        End Try
    End Sub
    Sub tryPopulateDetalle()
        Dim adapter0 As SqlDataAdapter
        Dim dt0 As DataTable = New DataTable
        Try
            con.Open()
            adapter0 = New SqlDataAdapter(cmd)
            adapter0.Fill(dt0)
            For Each row0 In dt0.Rows
                populateD(row0(0), row0(1), row0(2), row0(3), row0(4), row0(5))
            Next
            con.Close()
            dt0.Rows.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try
    End Sub
    Sub populateM(Id As String, pre As String, Fecha As Date, Sucursal As String, Supervisor As String, Vehiculo As String, Costo As Decimal, Estado As String)
        Dim row0 As String() = {Id, pre, Fecha, Sucursal, Supervisor, Vehiculo, Costo, Estado}
        dgvMaster.Rows.Add(row0)
    End Sub
    Sub populateD(id As String, Id_Producto As String, Nombre_Producto As String, Costo As Decimal, Cantidad As Decimal, Vendido As Decimal)
        Dim row0 As String() = {Id, Id_Producto, Nombre_Producto, Costo, Cantidad, Vendido}
        dgvDetalle.Rows.Add(row0)
    End Sub

    Private Sub Rremision_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None
    End Sub
    Private Sub cbFiltro2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFiltro2.SelectedIndexChanged
        dgvMaster.Rows.Clear()
        Try
            sqlstring = "select distinct Tbl_Remision.Id_Remision, Id_Remision2, Tbl_Remision.Fecha, Configuracion.Sucursales.Nombre, Supervisores.Nombre_Supervisor, Tbl_Vehiculo.Nombre_Ruta, (select sum(([Tbl_Remision.Detalle].costo)*Cantidad) from [Tbl_Remision.Detalle] where [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision) as 'Costo Total', Estado from Tbl_Remision inner join [Tbl_Remision.Detalle] on [Tbl_Remision.Detalle].Id_Remision=Tbl_Remision.Id_Remision inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Supervisores on Supervisores.Codigo_Supervisor=Tbl_Remision.Id_Supervisor inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo where Tbl_Remision.Fecha >= @inicio and Tbl_Remision.Fecha <=@final and Estado='" & cbFiltro2.SelectedIndex & "' "
            cmd = New SqlCommand(sqlstring, con)
            tryPopulateMaster()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvDetalle_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvDetalle.CellMouseDoubleClick
        dgvMaster.Visible = True
        pDetalle.Visible = False
    End Sub

    Private Sub dgvMaster_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvMaster.CellMouseDoubleClick
        dgv_Detalle()
    End Sub
    Sub Iprint()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reporte Remision.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reporte_Remision, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reporte Remision.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Detalle")
        xlWorkSheet.Cells(5, 2) = txtSucursal.Text
        xlWorkSheet.Cells(6, 2) = txtRemision.Text
        xlWorkSheet.Cells(5, 6) = txtSupervisor.Text
        xlWorkSheet.Cells(6, 6) = txtVehiculo.Text
        xlWorkSheet.Cells(5, 10) = dtpFecha.Text
        xlWorkSheet.Cells(6, 10) = txtEstado.Text
        Dim i As Integer = 1
        Dim f As Integer = 9
        Do While f <= 47
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 4) = ""
            xlWorkSheet.Cells(f, 5) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            f = f + 1
        Loop
        f = 9
        Do While i <= dgvDetalle.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvDetalle.Rows((i - 1)).Cells(0).Value 'detalle
            xlWorkSheet.Cells(f, 2) = dgvDetalle.Rows((i - 1)).Cells(1).Value 'codigo_p
            xlWorkSheet.Cells(f, 3) = dgvDetalle.Rows((i - 1)).Cells(2).Value 'Producto
            xlWorkSheet.Cells(f, 9) = dgvDetalle.Rows((i - 1)).Cells(3).Value 'costo
            xlWorkSheet.Cells(f, 10) = dgvDetalle.Rows((i - 1)).Cells(4).Value 'cantidad
            xlWorkSheet.Cells(f, 11) = dgvDetalle.Rows((i - 1)).Cells(5).Value 'vendido
            i = i + 1
            f = f + 1
        Loop
        xlWorkBook.Application.DisplayAlerts = False
        If expo = True Then
            Using sfd As New SaveFileDialog
                xlApp.DisplayAlerts = False
                sfd.Title = "Guardar archivo de excel"
                sfd.OverwritePrompt = True
                sfd.DefaultExt = ".xlsx"
                sfd.Filter = "Archivo de Excel(*xlsx)|"
                sfd.AddExtension = True
                If sfd.ShowDialog() = DialogResult.OK Then
                    xlWorkBook.SaveAs(sfd.FileName)
                Else
                End If
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)

                Dim res As MsgBoxResult
                res = MsgBox("Archivo guardado en : " + sfd.FileName + ", desea abrir el archivo?", MsgBoxStyle.YesNo)
                If (res = MsgBoxResult.Yes) Then
                    Process.Start(sfd.FileName)
                End If
            End Using
        Else
            Try
                xlWorkBook.Save()
            Catch ex As Exception
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)
            End Try
            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End If
        'Try
        '    xlWorkBook.Save()
        'Catch ex As Exception
        '    xlWorkBook.Close()
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'End Try
        ''-----------------PRINT EXCEL--------------------------------
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        ''------------------------------------------------------------
        'xlWorkBook.Close()
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)
    End Sub
    Sub Gprint()
       Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reporte Remision.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reporte_Remision, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reporte Remision.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("General")
        'xlWorkBook = xlApp.Workbooks.Open("C:\Dimarsa\Reporte Remision.xlsx")
        'xlWorkSheet = xlWorkBook.Sheets("General")
        xlWorkSheet.Cells(3, 1) = "FECHA: " + dtpI.Text + "  a  " + dtpF.Text
        Dim i As Integer = 1
        Dim f As Integer = 6
        Do While f <= 47
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 4) = ""
            xlWorkSheet.Cells(f, 5) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            f = f + 1
        Loop
        f = 6
        Do While i <= dgvMaster.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvMaster.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvMaster.Rows((i - 1)).Cells(2).Value
            xlWorkSheet.Cells(f, 3) = dgvMaster.Rows((i - 1)).Cells(3).Value
            xlWorkSheet.Cells(f, 6) = dgvMaster.Rows((i - 1)).Cells(4).Value
            xlWorkSheet.Cells(f, 9) = dgvMaster.Rows((i - 1)).Cells(5).Value
            xlWorkSheet.Cells(f, 10) = dgvMaster.Rows((i - 1)).Cells(6).Value
            xlWorkSheet.Cells(f, 11) = dgvMaster.Rows((i - 1)).Cells(7).Value
            i = i + 1
            f = f + 1
        Loop
        xlWorkBook.Application.DisplayAlerts = False
        If expo = True Then
            Using sfd As New SaveFileDialog
                xlApp.DisplayAlerts = False
                sfd.Title = "Guardar archivo de excel"
                sfd.OverwritePrompt = True
                sfd.DefaultExt = ".xlsx"
                sfd.Filter = "Archivo de Excel(*xlsx)|"
                sfd.AddExtension = True
                If sfd.ShowDialog() = DialogResult.OK Then
                    xlWorkBook.SaveAs(sfd.FileName)
                Else
                End If
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)

                Dim res As MsgBoxResult
                res = MsgBox("Archivo guardado en : " + sfd.FileName + ", desea abrir el archivo?", MsgBoxStyle.YesNo)
                If (res = MsgBoxResult.Yes) Then
                    Process.Start(sfd.FileName)
                End If
            End Using
        Else
            Try
                xlWorkBook.Save()
            Catch ex As Exception
                xlWorkBook.Close()
                xlApp.Quit()
                releaseObject(xlApp)
                releaseObject(xlWorkBook)
                releaseObject(xlWorkSheet)
            End Try
            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End If
        'Try
        '    xlWorkBook.Save()
        'Catch ex As Exception
        '    xlWorkBook.Close()
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'End Try
        ''-----------------PRINT EXCEL--------------------------------
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        ''------------------------------------------------------------
        'xlWorkBook.Close()
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If pDetalle.Visible = True Then
            Iprint()
        Else
            Gprint()
        End If
    End Sub

    Private Sub dtpI_ValueChanged(sender As Object, e As EventArgs) Handles dtpI.ValueChanged, dtpF.ValueChanged
        If bandera = True Then
            Retrive()
        End If
    End Sub

    Private Sub btnExpo_Click(sender As Object, e As EventArgs) Handles btnExpo.Click
        expo = True
        If pDetalle.Visible = True Then
            Iprint()
        Else
            Gprint()
        End If
    End Sub
End Class