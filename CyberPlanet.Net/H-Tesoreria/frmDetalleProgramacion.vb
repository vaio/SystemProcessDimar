﻿Public Class frmDetalleProgramacion

  Private Sub frmDetalleProgramacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
      CargarProveedores()
      CargarRubros()
  End Sub

    Private Sub CargarProveedores()
        TblProvBindingSource.DataSource = SQL("select Codigo_Proveedor,Nombre_Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedores", My.Settings.SolIndustrialCNX).Tables(0)
        luProveedor.Properties.DataSource = TblProvBindingSource
        luProveedor.Properties.DisplayMember = "Nombre_Proveedor"
        luProveedor.Properties.ValueMember = "Codigo_Proveedor"
        luProveedor.ItemIndex = 0
    End Sub

    Private Sub CargarRubros()
        TblRubrosBindingSource.DataSource = SQL("select Id_RubroPago,Nombre_RubroProg from Tbl_RubrosProgram order by Id_RubroPago", "tblRubros", My.Settings.SolIndustrialCNX).Tables(0)
        luRubros.Properties.DataSource = TblRubrosBindingSource
        luRubros.Properties.DisplayMember = "Nombre_RubroProg"
        luRubros.Properties.ValueMember = "Id_RubroPago"
        luRubros.ItemIndex = 0
    End Sub

End Class