﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProgramPagos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.TblProgramacionesBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.PanelMaster = New System.Windows.Forms.Panel()
    Me.txtProgram = New DevExpress.XtraEditors.TextEdit()
    Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
    Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
    Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
    Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
    Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
    Me.txtDescripPagos = New DevExpress.XtraEditors.TextEdit()
    Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
    Me.txtSemanaProg = New DevExpress.XtraEditors.TextEdit()
    Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
    Me.deFechaProg = New DevExpress.XtraEditors.DateEdit()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
    Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
    Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
    Me.lblCodModelo = New System.Windows.Forms.Label()
    Me.luRubros = New DevExpress.XtraEditors.LookUpEdit()
    Me.lblMarcaAsoc = New System.Windows.Forms.Label()
    Me.txtDescripcion = New System.Windows.Forms.TextBox()
    Me.lblNomModelo = New System.Windows.Forms.Label()
    CType(Me.TblProgramacionesBS, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.PanelMaster.SuspendLayout()
    CType(Me.txtProgram.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.txtDescripPagos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.txtSemanaProg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaProg.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaProg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.luRubros.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'PanelMaster
    '
    Me.PanelMaster.Controls.Add(Me.txtProgram)
    Me.PanelMaster.Controls.Add(Me.LabelControl11)
    Me.PanelMaster.Controls.Add(Me.TextEdit2)
    Me.PanelMaster.Controls.Add(Me.LabelControl10)
    Me.PanelMaster.Controls.Add(Me.TextEdit1)
    Me.PanelMaster.Controls.Add(Me.LabelControl9)
    Me.PanelMaster.Controls.Add(Me.TextBox1)
    Me.PanelMaster.Controls.Add(Me.LabelControl4)
    Me.PanelMaster.Controls.Add(Me.txtDescripPagos)
    Me.PanelMaster.Controls.Add(Me.LabelControl3)
    Me.PanelMaster.Controls.Add(Me.txtSemanaProg)
    Me.PanelMaster.Controls.Add(Me.LabelControl2)
    Me.PanelMaster.Controls.Add(Me.LabelControl1)
    Me.PanelMaster.Controls.Add(Me.deFechaProg)
    Me.PanelMaster.Dock = System.Windows.Forms.DockStyle.Top
    Me.PanelMaster.Enabled = False
    Me.PanelMaster.Location = New System.Drawing.Point(0, 0)
    Me.PanelMaster.Name = "PanelMaster"
    Me.PanelMaster.Size = New System.Drawing.Size(1354, 10)
    Me.PanelMaster.TabIndex = 0
    '
    'txtProgram
    '
    Me.txtProgram.Location = New System.Drawing.Point(99, 12)
    Me.txtProgram.Name = "txtProgram"
    Me.txtProgram.Size = New System.Drawing.Size(86, 20)
    Me.txtProgram.TabIndex = 24
    '
    'LabelControl11
    '
    Me.LabelControl11.Location = New System.Drawing.Point(24, 15)
    Me.LabelControl11.Name = "LabelControl11"
    Me.LabelControl11.Size = New System.Drawing.Size(69, 13)
    Me.LabelControl11.TabIndex = 25
    Me.LabelControl11.Text = "Programación:"
    '
    'TextEdit2
    '
    Me.TextEdit2.Location = New System.Drawing.Point(241, 38)
    Me.TextEdit2.Name = "TextEdit2"
    Me.TextEdit2.Size = New System.Drawing.Size(42, 20)
    Me.TextEdit2.TabIndex = 22
    '
    'LabelControl10
    '
    Me.LabelControl10.Location = New System.Drawing.Point(214, 41)
    Me.LabelControl10.Name = "LabelControl10"
    Me.LabelControl10.Size = New System.Drawing.Size(23, 13)
    Me.LabelControl10.TabIndex = 23
    Me.LabelControl10.Text = "Año:"
    '
    'TextEdit1
    '
    Me.TextEdit1.Location = New System.Drawing.Point(157, 38)
    Me.TextEdit1.Name = "TextEdit1"
    Me.TextEdit1.Size = New System.Drawing.Size(42, 20)
    Me.TextEdit1.TabIndex = 20
    '
    'LabelControl9
    '
    Me.LabelControl9.Location = New System.Drawing.Point(130, 41)
    Me.LabelControl9.Name = "LabelControl9"
    Me.LabelControl9.Size = New System.Drawing.Size(23, 13)
    Me.LabelControl9.TabIndex = 21
    Me.LabelControl9.Text = "Mes:"
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(442, 12)
    Me.TextBox1.Multiline = True
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Size = New System.Drawing.Size(349, 48)
    Me.TextBox1.TabIndex = 2
    '
    'LabelControl4
    '
    Me.LabelControl4.Location = New System.Drawing.Point(361, 15)
    Me.LabelControl4.Name = "LabelControl4"
    Me.LabelControl4.Size = New System.Drawing.Size(75, 13)
    Me.LabelControl4.TabIndex = 6
    Me.LabelControl4.Text = "Observaciones:"
    '
    'txtDescripPagos
    '
    Me.txtDescripPagos.Location = New System.Drawing.Point(131, 66)
    Me.txtDescripPagos.Name = "txtDescripPagos"
    Me.txtDescripPagos.Size = New System.Drawing.Size(660, 20)
    Me.txtDescripPagos.TabIndex = 3
    '
    'LabelControl3
    '
    Me.LabelControl3.Location = New System.Drawing.Point(24, 69)
    Me.LabelControl3.Name = "LabelControl3"
    Me.LabelControl3.Size = New System.Drawing.Size(90, 13)
    Me.LabelControl3.TabIndex = 4
    Me.LabelControl3.Text = "Descripcion Pagos:"
    '
    'txtSemanaProg
    '
    Me.txtSemanaProg.Location = New System.Drawing.Point(72, 38)
    Me.txtSemanaProg.Name = "txtSemanaProg"
    Me.txtSemanaProg.Size = New System.Drawing.Size(42, 20)
    Me.txtSemanaProg.TabIndex = 1
    '
    'LabelControl2
    '
    Me.LabelControl2.Location = New System.Drawing.Point(24, 41)
    Me.LabelControl2.Name = "LabelControl2"
    Me.LabelControl2.Size = New System.Drawing.Size(42, 13)
    Me.LabelControl2.TabIndex = 2
    Me.LabelControl2.Text = "Semana:"
    '
    'LabelControl1
    '
    Me.LabelControl1.Location = New System.Drawing.Point(205, 15)
    Me.LabelControl1.Name = "LabelControl1"
    Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
    Me.LabelControl1.TabIndex = 1
    Me.LabelControl1.Text = "Fecha:"
    '
    'deFechaProg
    '
    Me.deFechaProg.EditValue = Nothing
    Me.deFechaProg.Location = New System.Drawing.Point(241, 12)
    Me.deFechaProg.Name = "deFechaProg"
    Me.deFechaProg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.deFechaProg.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.deFechaProg.Size = New System.Drawing.Size(94, 20)
    Me.deFechaProg.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.txtDescripcion)
    Me.Panel1.Controls.Add(Me.lblNomModelo)
    Me.Panel1.Controls.Add(Me.luRubros)
    Me.Panel1.Controls.Add(Me.lblMarcaAsoc)
    Me.Panel1.Controls.Add(Me.DateEdit1)
    Me.Panel1.Controls.Add(Me.lblCodModelo)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
    Me.Panel1.Location = New System.Drawing.Point(1154, 10)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(200, 723)
    Me.Panel1.TabIndex = 5
    '
    'GridControl1
    '
    Me.GridControl1.DataSource = Me.TblProgramacionesBS
    Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GridControl1.Location = New System.Drawing.Point(0, 10)
    Me.GridControl1.MainView = Me.GridView1
    Me.GridControl1.Name = "GridControl1"
    Me.GridControl1.Size = New System.Drawing.Size(1154, 723)
    Me.GridControl1.TabIndex = 6
    Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
    '
    'GridView1
    '
    Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
    Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
    Me.GridView1.GridControl = Me.GridControl1
    Me.GridView1.Name = "GridView1"
    Me.GridView1.OptionsBehavior.Editable = False
    Me.GridView1.OptionsBehavior.ReadOnly = True
    Me.GridView1.OptionsCustomization.AllowColumnMoving = False
    Me.GridView1.OptionsCustomization.AllowColumnResizing = False
    Me.GridView1.OptionsFind.AlwaysVisible = True
    Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
    Me.GridView1.OptionsView.ShowAutoFilterRow = True
    Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
    Me.GridView1.OptionsView.ShowGroupPanel = False
    '
    'DateEdit1
    '
    Me.DateEdit1.EditValue = Nothing
    Me.DateEdit1.Location = New System.Drawing.Point(18, 100)
    Me.DateEdit1.Name = "DateEdit1"
    Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.DateEdit1.Size = New System.Drawing.Size(161, 20)
    Me.DateEdit1.TabIndex = 18
    '
    'lblCodModelo
    '
    Me.lblCodModelo.Location = New System.Drawing.Point(15, 81)
    Me.lblCodModelo.Name = "lblCodModelo"
    Me.lblCodModelo.Size = New System.Drawing.Size(108, 20)
    Me.lblCodModelo.TabIndex = 17
    Me.lblCodModelo.Text = "Fecha Programación:"
    '
    'luRubros
    '
    Me.luRubros.Location = New System.Drawing.Point(18, 36)
    Me.luRubros.Name = "luRubros"
    Me.luRubros.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luRubros.Size = New System.Drawing.Size(161, 20)
    Me.luRubros.TabIndex = 20
    '
    'lblMarcaAsoc
    '
    Me.lblMarcaAsoc.Location = New System.Drawing.Point(15, 16)
    Me.lblMarcaAsoc.Name = "lblMarcaAsoc"
    Me.lblMarcaAsoc.Size = New System.Drawing.Size(45, 17)
    Me.lblMarcaAsoc.TabIndex = 19
    Me.lblMarcaAsoc.Text = "Rubro:"
    '
    'txtDescripcion
    '
    Me.txtDescripcion.Location = New System.Drawing.Point(16, 162)
    Me.txtDescripcion.Multiline = True
    Me.txtDescripcion.Name = "txtDescripcion"
    Me.txtDescripcion.Size = New System.Drawing.Size(163, 108)
    Me.txtDescripcion.TabIndex = 21
    '
    'lblNomModelo
    '
    Me.lblNomModelo.Location = New System.Drawing.Point(15, 144)
    Me.lblNomModelo.Name = "lblNomModelo"
    Me.lblNomModelo.Size = New System.Drawing.Size(78, 20)
    Me.lblNomModelo.TabIndex = 22
    Me.lblNomModelo.Text = "Descripción:"
    '
    'frmProgramPagos
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1354, 733)
    Me.Controls.Add(Me.GridControl1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.PanelMaster)
    Me.Name = "frmProgramPagos"
    Me.Text = "Programación de Pagos"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    CType(Me.TblProgramacionesBS, System.ComponentModel.ISupportInitialize).EndInit()
    Me.PanelMaster.ResumeLayout(False)
    Me.PanelMaster.PerformLayout()
    CType(Me.txtProgram.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.txtDescripPagos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.txtSemanaProg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaProg.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaProg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.luRubros.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents TblProgramacionesBS As System.Windows.Forms.BindingSource
    Friend WithEvents PanelMaster As System.Windows.Forms.Panel
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDescripPagos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSemanaProg As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaProg As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtProgram As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCodModelo As System.Windows.Forms.Label
    Friend WithEvents luRubros As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblMarcaAsoc As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents lblNomModelo As System.Windows.Forms.Label
End Class
