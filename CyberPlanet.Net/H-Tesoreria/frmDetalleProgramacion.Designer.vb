﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDetalleProgramacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDetalleProgramacion))
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.lblCodModelo = New System.Windows.Forms.Label()
    Me.lblNomModelo = New System.Windows.Forms.Label()
    Me.lblMarcaAsoc = New System.Windows.Forms.Label()
    Me.luRubros = New DevExpress.XtraEditors.LookUpEdit()
    Me.cmdAddMarca = New DevExpress.XtraEditors.SimpleButton()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.luProveedor = New DevExpress.XtraEditors.LookUpEdit()
    Me.txtDescripcion = New System.Windows.Forms.TextBox()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.deFechaVenc = New DevExpress.XtraEditors.DateEdit()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.txtMontoCuota = New System.Windows.Forms.TextBox()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.txtMontoDoc = New System.Windows.Forms.TextBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.txtNumDoc = New System.Windows.Forms.TextBox()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.deFechaProg = New DevExpress.XtraEditors.DateEdit()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.TextBox3 = New System.Windows.Forms.TextBox()
    Me.Label10 = New System.Windows.Forms.Label()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.Label8 = New System.Windows.Forms.Label()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.Label9 = New System.Windows.Forms.Label()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.TblProvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
    Me.TblRubrosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
    Me.Panel1.SuspendLayout()
    CType(Me.luRubros.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.luProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox1.SuspendLayout()
    CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaVenc.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaVenc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaProg.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaProg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox2.SuspendLayout()
    Me.Panel2.SuspendLayout()
    CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TblRubrosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 222)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(661, 49)
    Me.Panel1.TabIndex = 6
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(566, 8)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(482, 8)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'lblCodModelo
    '
    Me.lblCodModelo.Location = New System.Drawing.Point(21, 18)
    Me.lblCodModelo.Name = "lblCodModelo"
    Me.lblCodModelo.Size = New System.Drawing.Size(108, 20)
    Me.lblCodModelo.TabIndex = 1
    Me.lblCodModelo.Text = "Fecha Programación:"
    '
    'lblNomModelo
    '
    Me.lblNomModelo.Location = New System.Drawing.Point(21, 68)
    Me.lblNomModelo.Name = "lblNomModelo"
    Me.lblNomModelo.Size = New System.Drawing.Size(78, 20)
    Me.lblNomModelo.TabIndex = 3
    Me.lblNomModelo.Text = "Descripción:"
    '
    'lblMarcaAsoc
    '
    Me.lblMarcaAsoc.Location = New System.Drawing.Point(266, 43)
    Me.lblMarcaAsoc.Name = "lblMarcaAsoc"
    Me.lblMarcaAsoc.Size = New System.Drawing.Size(45, 17)
    Me.lblMarcaAsoc.TabIndex = 5
    Me.lblMarcaAsoc.Text = "Rubro:"
    '
    'luRubros
    '
    Me.luRubros.Location = New System.Drawing.Point(328, 40)
    Me.luRubros.Name = "luRubros"
    Me.luRubros.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luRubros.Size = New System.Drawing.Size(289, 20)
    Me.luRubros.TabIndex = 6
    '
    'cmdAddMarca
    '
    Me.cmdAddMarca.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.cmdAddMarca.Appearance.ForeColor = System.Drawing.Color.Blue
    Me.cmdAddMarca.Appearance.Options.UseFont = True
    Me.cmdAddMarca.Appearance.Options.UseForeColor = True
    Me.cmdAddMarca.Location = New System.Drawing.Point(618, 40)
    Me.cmdAddMarca.Name = "cmdAddMarca"
    Me.cmdAddMarca.Size = New System.Drawing.Size(20, 19)
    Me.cmdAddMarca.TabIndex = 10
    Me.cmdAddMarca.Text = "+"
    Me.cmdAddMarca.ToolTip = "Agregar una nueva Marca"
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(266, 15)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(60, 17)
    Me.Label1.TabIndex = 12
    Me.Label1.Text = "Proveedor:"
    '
    'luProveedor
    '
    Me.luProveedor.Location = New System.Drawing.Point(328, 12)
    Me.luProveedor.Name = "luProveedor"
    Me.luProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luProveedor.Size = New System.Drawing.Size(289, 20)
    Me.luProveedor.TabIndex = 13
    '
    'txtDescripcion
    '
    Me.txtDescripcion.Location = New System.Drawing.Point(89, 65)
    Me.txtDescripcion.Name = "txtDescripcion"
    Me.txtDescripcion.Size = New System.Drawing.Size(528, 20)
    Me.txtDescripcion.TabIndex = 2
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.DateEdit1)
    Me.GroupBox1.Controls.Add(Me.Label5)
    Me.GroupBox1.Controls.Add(Me.deFechaVenc)
    Me.GroupBox1.Controls.Add(Me.Label6)
    Me.GroupBox1.Controls.Add(Me.txtMontoCuota)
    Me.GroupBox1.Controls.Add(Me.Label4)
    Me.GroupBox1.Controls.Add(Me.txtMontoDoc)
    Me.GroupBox1.Controls.Add(Me.Label3)
    Me.GroupBox1.Controls.Add(Me.txtNumDoc)
    Me.GroupBox1.Controls.Add(Me.Label2)
    Me.GroupBox1.Location = New System.Drawing.Point(24, 101)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(412, 112)
    Me.GroupBox1.TabIndex = 15
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Datos del Documento"
    '
    'DateEdit1
    '
    Me.DateEdit1.EditValue = Nothing
    Me.DateEdit1.Location = New System.Drawing.Point(291, 28)
    Me.DateEdit1.Name = "DateEdit1"
    Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.DateEdit1.Size = New System.Drawing.Size(103, 20)
    Me.DateEdit1.TabIndex = 23
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(218, 31)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(84, 17)
    Me.Label5.TabIndex = 24
    Me.Label5.Text = "Fecha Pago :"
    '
    'deFechaVenc
    '
    Me.deFechaVenc.EditValue = Nothing
    Me.deFechaVenc.Location = New System.Drawing.Point(291, 54)
    Me.deFechaVenc.Name = "deFechaVenc"
    Me.deFechaVenc.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.deFechaVenc.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.deFechaVenc.Size = New System.Drawing.Size(103, 20)
    Me.deFechaVenc.TabIndex = 11
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(218, 57)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(84, 17)
    Me.Label6.TabIndex = 22
    Me.Label6.Text = "Vencimiento:"
    '
    'txtMontoCuota
    '
    Me.txtMontoCuota.Location = New System.Drawing.Point(117, 80)
    Me.txtMontoCuota.Name = "txtMontoCuota"
    Me.txtMontoCuota.Size = New System.Drawing.Size(83, 20)
    Me.txtMontoCuota.TabIndex = 17
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(15, 83)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(84, 17)
    Me.Label4.TabIndex = 18
    Me.Label4.Text = "Monto Cuota:"
    '
    'txtMontoDoc
    '
    Me.txtMontoDoc.Location = New System.Drawing.Point(117, 54)
    Me.txtMontoDoc.Name = "txtMontoDoc"
    Me.txtMontoDoc.Size = New System.Drawing.Size(83, 20)
    Me.txtMontoDoc.TabIndex = 15
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(15, 57)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(100, 17)
    Me.Label3.TabIndex = 16
    Me.Label3.Text = "Monto Documento:"
    '
    'txtNumDoc
    '
    Me.txtNumDoc.Location = New System.Drawing.Point(117, 28)
    Me.txtNumDoc.Name = "txtNumDoc"
    Me.txtNumDoc.Size = New System.Drawing.Size(83, 20)
    Me.txtNumDoc.TabIndex = 0
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(15, 31)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(105, 17)
    Me.Label2.TabIndex = 14
    Me.Label2.Text = "Número Documento:"
    '
    'deFechaProg
    '
    Me.deFechaProg.EditValue = Nothing
    Me.deFechaProg.Location = New System.Drawing.Point(131, 15)
    Me.deFechaProg.Name = "deFechaProg"
    Me.deFechaProg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.deFechaProg.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.deFechaProg.Size = New System.Drawing.Size(103, 20)
    Me.deFechaProg.TabIndex = 16
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.TextBox3)
    Me.GroupBox2.Controls.Add(Me.Label10)
    Me.GroupBox2.Controls.Add(Me.TextBox1)
    Me.GroupBox2.Controls.Add(Me.Label8)
    Me.GroupBox2.Controls.Add(Me.TextBox2)
    Me.GroupBox2.Controls.Add(Me.Label9)
    Me.GroupBox2.Location = New System.Drawing.Point(442, 101)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(196, 112)
    Me.GroupBox2.TabIndex = 17
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Pagos Recurrentes"
    '
    'TextBox3
    '
    Me.TextBox3.Location = New System.Drawing.Point(100, 80)
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.Size = New System.Drawing.Size(84, 20)
    Me.TextBox3.TabIndex = 23
    '
    'Label10
    '
    Me.Label10.Location = New System.Drawing.Point(17, 83)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(84, 17)
    Me.Label10.TabIndex = 24
    Me.Label10.Text = "N° Pagos:"
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(99, 28)
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Size = New System.Drawing.Size(84, 20)
    Me.TextBox1.TabIndex = 19
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(16, 31)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(84, 17)
    Me.Label8.TabIndex = 20
    Me.Label8.Text = "Fecha Abono:"
    '
    'TextBox2
    '
    Me.TextBox2.Location = New System.Drawing.Point(100, 54)
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.Size = New System.Drawing.Size(83, 20)
    Me.TextBox2.TabIndex = 17
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(16, 57)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(84, 17)
    Me.Label9.TabIndex = 18
    Me.Label9.Text = "Monto Cuota:"
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.GroupBox2)
    Me.Panel2.Controls.Add(Me.deFechaProg)
    Me.Panel2.Controls.Add(Me.GroupBox1)
    Me.Panel2.Controls.Add(Me.txtDescripcion)
    Me.Panel2.Controls.Add(Me.luProveedor)
    Me.Panel2.Controls.Add(Me.Label1)
    Me.Panel2.Controls.Add(Me.cmdAddMarca)
    Me.Panel2.Controls.Add(Me.luRubros)
    Me.Panel2.Controls.Add(Me.lblMarcaAsoc)
    Me.Panel2.Controls.Add(Me.lblNomModelo)
    Me.Panel2.Controls.Add(Me.lblCodModelo)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel2.Location = New System.Drawing.Point(0, 0)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(661, 222)
    Me.Panel2.TabIndex = 7
    '
    'frmDetalleProgramacion
    '
    Me.AcceptButton = Me.cmdAceptar
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(661, 271)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Panel1)
    Me.Name = "frmDetalleProgramacion"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Detalle Programacion de Pagos"
    Me.Panel1.ResumeLayout(False)
    CType(Me.luRubros.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.luProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaVenc.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaVenc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaProg.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaProg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TblRubrosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents lblCodModelo As System.Windows.Forms.Label
    Friend WithEvents lblNomModelo As System.Windows.Forms.Label
    Friend WithEvents lblMarcaAsoc As System.Windows.Forms.Label
    Friend WithEvents luRubros As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmdAddMarca As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents luProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents deFechaVenc As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMontoCuota As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMontoDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNumDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents deFechaProg As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TblProvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblRubrosBindingSource As System.Windows.Forms.BindingSource
End Class
