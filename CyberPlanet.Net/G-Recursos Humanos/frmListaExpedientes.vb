﻿Public Class frmListaExpedientes
    Dim strlListaColab As String

    Private Sub frmListaExpedientes_Leave(sender As Object, e As EventArgs) Handles Me.Leave
        NumCatalogo = 0
    End Sub

    Private Sub frmListaExpedientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarDatos()
    End Sub

    Sub CargarDatos()
        strlListaColab = "Select * from vw_Colaboradores"
        'Dim tblDatosCatalogo As DataTable
        Me.GridView1.Columns.Clear()
        Me.TblListaColabBS.DataSource = SQL(strlListaColab, "tblListaColab", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub


    'Function Nuevo()
    '    PermitirBotonesEdicion(False)
    '    txtNumDoc.Text = Microsoft.VisualBasic.Right("0000000000" & SigNumDoc(), 10)
    '    txtNumDoc.Focus()
    'End Function

    'Function Modificar()
    '    'Panel2.Enabled = True
    '    PermitirBotonesEdicion(False)
    '    txtNumDoc.Focus()
    'End Function

    'Function Eliminar()
    '    DesactivarCampos(True)
    '    PermitirBotonesEdicion(False)
    'End Function

    'Function Guardar()
    '    'Panel2.Enabled = False
    '    'Panel1.Enabled = False
    '    PermitirBotonesEdicion(True)
    '    DesactivarCampos(True)
    '    GuardarMasterDoc()
    'End Function

    'Sub GuardarMasterDoc()
    '    Dim Resum As String = ""
    '    Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)

    '    Try
    '        Resum = MasterDocument.EditarMasterCompraLocal(1, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue, 1, deFechaDoc.Value, deFechaVenc.Value, CInt(txtPlazo.Text), txtObservacion.Text.ToUpper, (cmbTipExoneracion.SelectedIndex), nTipoEdic)
    '        If Resum = "OK" Then
    '            If nTipoEdic = 3 Then
    '                LimpiarCampos()
    '            End If
    '            'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
    '            CargarDetalleDoc(1, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
    '        Else
    '            MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
    '        End If

    '    Catch ex As Exception
    '        Call MsgBox("Error: " + ex.Message)
    '    Finally
    '        '    Cerrar()
    '        If NumCatalogo_Ant = 0 Then
    '        End If
    '    End Try
    'End Sub

    Function Cancelar()
        'LimpiarCampos()
        PermitirBotonesEdicion(True)
        'Panel2.Enabled = False
        Panel1.Enabled = False
    End Function

    Function Buscar()
        frmBusquedaDoc.showdialog()
    End Function

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub



End Class