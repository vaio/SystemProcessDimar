﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
Imports DevExpress.XtraEditors.Controls

Public Class FrmRemision
    '----Print-----
    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    Dim misValue As Object = System.Reflection.Missing.Value
    '------------------
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Dim tabla As Integer = 0
    Dim Tab As Integer = 0
    Dim check As Boolean = True
    'Dim CodBodega As Integer = 0
    Dim ds As New DataSet
    '------datos producto
    Public codigo As String = ""
    Public Producto As String = ""
    Public Cantidad As Decimal = 0
    Public Costo_Promedio As Decimal = 0
    Public Existencia As Decimal = 0
    Public Producto_Nuevo As Boolean = False
    Dim codigoMovimiento As String = ""
    Dim codigoMovimiento2 As String = ""
    Dim cod_Movimiento As String = ""
    'PA-------------------------------
    Dim ResumEntrada As String = ""
    Dim CodMaxEntrada As String = ""
    Dim ResumEntradaDeta As String = ""
    Public codEntrada As String = ""
    Public codSalida As String = ""
    '-----------------ESTADO REMISION----------
    Public edicion As Integer
    Dim Usuario As String
    Dim Estado As Integer = 0
    Public Solicitud As String
    Public Garantia As String
    Dim placa As String
    Public OdoF As Decimal
    '---------------VARIABLES------------------
    Dim Dt_Ruta As DataTable
    Dim DR_Ruta() As DataRow
    Public Id_Supervisor As Integer
    Public Id_Coordinador As Integer
    Public Id_Vehiculo As Integer
    Public Id_Bodega As Integer
    Dim Color_enable As Color = Color.DarkOrange
    Dim Color_Disable As Color = SystemColors.ButtonHighlight

    Private Sub FrmRemision_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pmaster.Enabled = False
        pDetalle.Enabled = False
        pEntrada.Enabled = False
        pPrefacturacion.Enabled = False
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None

    End Sub

    Sub Nuevo()
        '-----ojo-----
        pmaster.Enabled = False
        pDetalle.Enabled = False
        pEntrada.Enabled = False
        pPrefacturacion.Enabled = False
        dgvRemision.Rows.Clear()
        '-----ojo-----
        Estado = 1
        dtpFecha.Value = Today
        dgvRemision.Rows.Clear()
        dgvEntrada.Rows.Clear()
        dgvVenta.Rows.Clear()
        cbEstado.SelectedItem = "EMITIDA"
        pmaster.Enabled = True
        txtCantidad.Text = 0
        cbTipo.SelectedIndex = 0
        frmPrincipal.bbiGuardar.Enabled = True
        frmPrincipal.bbiCancelar.Enabled = True

        dgv_Remisiones()

        cb_Ruta()

    End Sub
    Sub CargaF()

        With frmBusquedaDoc.GridView1

            Dt_Ruta = (.DataSource).table

            luRuta.Properties.Columns.Clear()
            luRuta.Properties.DataSource = Dt_Ruta
            luRuta.Properties.DisplayMember = "Ruta"
            luRuta.Properties.ValueMember = "Id_Ruta"
            luRuta.Properties.Columns.Add(New LookUpColumnInfo("Id_Ruta", "Id", 20))
            luRuta.Properties.Columns.Add(New LookUpColumnInfo("Ruta", "Nombre", 100))
            luRuta.Properties.Columns(0).Visible = False
            luRuta.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            lbRemision.Text = .GetRowCellValue(.GetSelectedRows(0), "Codigo").ToString()
            lbremision2.Text = .GetRowCellValue(.GetSelectedRows(0), "Pre").ToString()
            cbTipo.SelectedItem = .GetRowCellValue(.GetSelectedRows(0), "Tipo").ToString()
            cbEstado.SelectedItem = .GetRowCellValue(.GetSelectedRows(0), "Estado").ToString()
            dtpFecha.Value = .GetRowCellValue(.GetSelectedRows(0), "Fecha")
            luRuta.EditValue = .GetRowCellValue(.GetSelectedRows(0), "Id_Ruta")
            txtOdoF.Text = .GetRowCellValue(.GetSelectedRows(0), "OdoF")
            Remision_Cargar2()

        End With
    End Sub

    Sub dgv_Remisiones()
        lbRemision.Text = ""
        Dim dt As DataTable = New DataTable()
        sqlstring = "select isnull(MAX (cast(Id_Remision as int))+1,1) from Tbl_Remision"
        Dim tblRegMax As DataTable = dt
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            con.Close()
            lbRemision.Text = Microsoft.VisualBasic.Right("00000" & tblRegMax.Rows(0).Item(0), 5)
        Catch ex As Exception
            con.Close()
        End Try
    End Sub
    Private Sub cb_Ruta()

        Try

            Dt_Ruta = SQL("select * from vw_Remision_Ruta", "tbl_Ruta", My.Settings.SolIndustrialCNX).Tables(0)

            luRuta.Properties.Columns.Clear()
            luRuta.Properties.DataSource = Dt_Ruta
            luRuta.Properties.DisplayMember = "Ruta"
            luRuta.Properties.ValueMember = "Id_Ruta"
            luRuta.Properties.Columns.Add(New LookUpColumnInfo("Id_Ruta", "Id", 20))
            luRuta.Properties.Columns.Add(New LookUpColumnInfo("Ruta", "Nombre", 100))
            luRuta.Properties.Columns(0).Visible = False
            luRuta.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            luRuta.ItemIndex = 0

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try

    End Sub

    Sub dgv_Remisiones2()

        lbremision2.Text = ""

        Dim dt As DataTable = New DataTable()

        sqlstring = "select isnull(MAX(Id_Remision),'') from Tbl_Remision where Id_Vehiculo=" & Id_Vehiculo & ""

        Dim tblRegMax As DataTable = dt

        cmd = New SqlCommand(sqlstring, con)

        Try

            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            con.Close()
            lbremision2.Text = Microsoft.VisualBasic.Right("00000" & tblRegMax.Rows(0).Item(0), 5)

        Catch ex As Exception

            con.Close()

        End Try
    End Sub

    Private Sub cb_Vendedor()

        Try
            luVendedor.Properties.DataSource = SQL("select Codigo, Empleado from vw_Ruta_Vendedor where Id_Ruta=" & luRuta.EditValue & "", "tbl_Vendedor", My.Settings.SolIndustrialCNX).Tables(0)
            luVendedor.Properties.DisplayMember = "Empleado"
            luVendedor.Properties.ValueMember = "Codigo"
            luVendedor.Properties.Columns.Add(New LookUpColumnInfo("Codigo", "Id", 20))
            luVendedor.Properties.Columns.Add(New LookUpColumnInfo("Empleado", "Nombre", 100))
            luVendedor.Properties.Columns(0).Visible = False
            luVendedor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            luVendedor.ItemIndex = 0

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try
    End Sub

    Private Sub Vehiculo_dgvRemision()
        dgvRemision.Rows.Clear()
        sqlstring = "select Inventario.Codigo_Producto as Codigo, Inventario.UndTotal_Existencia as Cantidad, Productos.Nombre_Producto as Producto, Inventario.Codigo_Bodega from Inventario inner join Productos on Productos.Codigo_Producto=Inventario.Codigo_Producto where Inventario.Codigo_Bodega=(Select Codigo_Bodega from Almacenes where Nombre_Bodega=(select Nombre_Ruta from Tbl_Vehiculo where Placa='" + txtVehiculo.Text + "'))"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                dgvRemision.Rows.Add(lectura("Codigo"), lectura("Cantidad"), lectura("Producto"))
            End While
            con.Close()
        Catch ex As Exception
            MsgBox("Error al mostrar items existentes en vehiculo" + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Guardar()

        Try
            con.Open()
            Dim cmd As New SqlCommand("SP_Remision", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@edicion", edicion)
                .AddWithValue("@remision1", lbRemision.Text)
                .AddWithValue("@remision2", lbremision2.Text)
                .AddWithValue("@sucursal", My.Settings.Sucursal)
                .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Value))
                .AddWithValue("@coordinador", Id_Coordinador)
                .AddWithValue("@supervisor", Id_Supervisor)
                .AddWithValue("@vehiculo", Id_Vehiculo)
                .AddWithValue("@estado", cbEstado.SelectedIndex + 1)
                .AddWithValue("@tipo", cbTipo.SelectedIndex + 1)
                .AddWithValue("@odo1", txtOdoI.Text)
                .AddWithValue("@odo2", txtOdoF.Text)
                .AddWithValue("@ruta", luRuta.EditValue)
                .AddWithValue("@usuario", UserID)
            End With
            cmd.ExecuteReader()
            con.Close()
            Select Case edicion
                Case 1
                    MsgBox("Remision Guardada")
                Case 2
                    MsgBox("Remision Actualizada")
                Case 3
                    MsgBox("Remision Eliminada")
            End Select
            pDetalle.Enabled = True
            pmaster.Enabled = False
            If lbremision2.Text <> "00000" Then
                Remision_Cargar()
            End If
            Update_Odo()
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Remision_Guardar_Detalle(ByVal edicion As Integer)
        If edicion = 1 Then
            For i = 0 To dgvRemision.Rows.Count - 1
                Try
                    con.Open()
                    Dim cmd As New SqlCommand("SP_Remision_Detalle", con)
                    cmd.CommandType = CommandType.StoredProcedure
                    With cmd.Parameters
                        .AddWithValue("@edicion", edicion)
                        .AddWithValue("@master", lbRemision.Text)
                        .AddWithValue("@sucursal", My.Settings.Sucursal)
                        .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Value))
                        .AddWithValue("@producto", dgvRemision.Rows(i).Cells(0).Value)
                        .AddWithValue("@costo", dgvRemision.Rows(i).Cells(3).Value)
                        .AddWithValue("@cantidad", dgvRemision.Rows(i).Cells(1).Value)
                        '.AddWithValue("@ruta", luRuta.EditValue)
                    End With
                    cmd.ExecuteReader()
                    con.Close()
                Catch ex As Exception
                    MsgBox("Error: " + ex.Message)
                    con.Close()
                End Try
            Next
        Else 'borrar
            Try
                con.Open()
                Dim cmd As New SqlCommand("SP_Remision_Detalle", con)
                cmd.CommandType = CommandType.StoredProcedure
                With cmd.Parameters
                    .AddWithValue("@master", lbRemision.Text)
                    .AddWithValue("@sucursal", My.Settings.Sucursal)
                    .AddWithValue("@producto", dgvRemision.Rows(dgvRemision.CurrentRow.Index).Cells(0).Value)
                    .AddWithValue("@costo", dgvRemision.Rows(dgvRemision.CurrentRow.Index).Cells(3).Value)
                    .AddWithValue("@cantidad", dgvRemision.Rows(dgvRemision.CurrentRow.Index).Cells(1).Value)
                    '.AddWithValue("@ruta", cbDestino.Text)
                    .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Value))
                    .AddWithValue("@edicion", edicion)
                End With
                cmd.ExecuteReader()
                con.Close()
            Catch ex As Exception
                MsgBox("Error: " + ex.Message)
                con.Close()
            End Try
        End If
    End Sub
    Private Sub Movimiento_Master(sucursal As Integer, bodega As String, tipo As Integer, cod_movimiento As String, cod_Documento As String, tipo_documento As Integer, comentario As String, fecha As Date, edicion As Integer)
        Try
            con.Open()
            Dim cmd As New SqlCommand("MOV_Master", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@Sucursal", sucursal)
                .AddWithValue("@bodega", bodega)
                .AddWithValue("@tipo", tipo)
                .AddWithValue("@cod_movimiento", cod_movimiento)
                .AddWithValue("@cod_documento", cod_Documento)
                .AddWithValue("@tipo_documento", tipo_documento)
                .AddWithValue("@comentario", comentario)
                .AddWithValue("@fecha", Convert.ToDateTime(fecha))
                .AddWithValue("@edicion", edicion)
            End With
            cmd.ExecuteReader()
            con.Close()
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Movimiento_Detalle(sucursal As Integer, bodega As String, tipo As Integer, codigo As String, producto As String, cantidad As Decimal, costo As Decimal, factura As String, edicion As Integer, remision As String)

        Try
            con.Open()
            Dim cmd As New SqlCommand("MOV_Detalle", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@sucursal", sucursal)
                .AddWithValue("@bodega", bodega)
                .AddWithValue("@tipo", tipo)
                .AddWithValue("@codigo_movimiento", codigo)
                .AddWithValue("@producto", producto)
                .AddWithValue("@cantidad", cantidad)
                .AddWithValue("@costo", costo)
                .AddWithValue("@factura", factura)
                .AddWithValue("@remision", remision)
                .AddWithValue("@edicion", edicion)
            End With
            cmd.ExecuteReader()
            con.Close()
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try

    End Sub

    Sub Update_Odo()
        sqlstring = "update Tbl_Vehiculo set [Odo_Final]= '" & OdoF & "' where Id_Vehiculo='" & Id_Vehiculo & "'"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub Update_RemiOdo()
        sqlstring = "update Tbl_Remision set [OdoF]='" & OdoF & "' where Id_Remision='" + lbRemision.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "'"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub RemisionBorrar()
        Select Case MsgBox("Remision No.:" + lbRemision.Text + "" & vbNewLine & "Esta seguro que desea BORRAR la remision?", MsgBoxStyle.YesNo, "")
            Case MsgBoxResult.Yes
                edicion = 3
                Guardar()
        End Select
        Nuevo()
    End Sub
    Sub Remision_Cargar()
        Dim dt As DataTable = New DataTable()
        dgvRemision.Rows.Clear()
        sqlstring = "select Id_Producto as codigo, Productos.Nombre_Producto as producto, (Cantidad-Cant_Vendida) as cantidad, Costo as costo from [Tbl_Remision.Detalle] inner join Productos on Productos.Codigo_Producto=[Tbl_Remision.Detalle].Id_Producto where Id_Remision='" + lbremision2.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "' and Cantidad > Cant_vendida"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvRemision.Rows.Add(row0(0), row0(2), row0(1), row0(3), 1)
            Next

            con.Close()
            dt.Rows.Clear()

            If dgvRemision.Rows.Count > 0 Then
                Select Case MsgBox("Existen articulos en la RUTA: " + txtVehiculo.Text + "" & vbNewLine & "Desea cargar articulos a la nueva REMISION?", MsgBoxStyle.YesNo, "")

                    Case MsgBoxResult.Yes

                        Remision_Guardar_Detalle(1)

                    Case MsgBoxResult.No

                        dgvRemision.Rows.Clear()

                End Select

            End If

        Catch ex As Exception
            MsgBox("Error al cargar datos." & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Remision_Cargar2()
        Dim dt As DataTable = New DataTable()
        pmaster.Enabled = False
        pDetalle.Enabled = False
        pEntrada.Enabled = False
        pPrefacturacion.Enabled = False
        dgvRemision.Rows.Clear()

        sqlstring = "select Id_Producto as codigo, Productos.Nombre_Producto as producto, Cantidad as Cantidad, Costo as costo from [Tbl_Remision.Detalle] inner join Productos on Productos.Codigo_Producto=[Tbl_Remision.Detalle].Id_Producto where Id_Remision='" + lbRemision.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "'"

        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvRemision.Rows.Add(row0(0), row0(2), row0(1), row0(3), 0)
            Next
            con.Close()
            dt.Rows.Clear()
            dgvRemision.ClearSelection()
        Catch ex As Exception
            MsgBox("Error al cargar datos." & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try

        If cbEstado.SelectedIndex <> 0 Then

            sqlstring = "select Id_Producto as codigo, Productos.Nombre_Producto as producto, (Cantidad-Cant_Vendida) as cantidad, Costo as costo from [Tbl_Remision.Detalle] inner join Productos on Productos.Codigo_Producto=[Tbl_Remision.Detalle].Id_Producto where Id_Remision='" + lbRemision.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "'"

            cmd = New SqlCommand(sqlstring, con)

            Try
                con.Open()
                adapter = New SqlDataAdapter(cmd)
                adapter.Fill(dt)
                For Each row0 In dt.Rows
                    dgvEntrada.Rows.Add(row0(0), row0(2), row0(1), row0(3))
                Next
                con.Close()
                dt.Rows.Clear()
                dgvEntrada.ClearSelection()
            Catch ex As Exception
                MsgBox("Error al cargar datos." & vbNewLine & "Error: " + ex.Message)
                con.Close()
            End Try
            prueba()
        End If
        RemiOdo()
    End Sub
    Sub RemiOdo()
        Dim dt As DataTable = New DataTable()
        sqlstring = "select OdoI, OdoF From Tbl_Remision where Id_Remision='" + lbRemision.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtOdoI.Text = row0(0)
                txtOdoF.Text = row0(1)
            Next
            con.Close()
        Catch ex As Exception
            MsgBox("Error al cargar datos." & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub prueba()
        Dim dt As DataTable = New DataTable
        dgvVenta.Rows.Clear()
        sqlstring = "SELECT [Tbl_Factura_Solicitud.Detalle].Id_Producto, [Tbl_Factura_Solicitud.Detalle].Cantidad, Productos.Nombre_Producto, [Tbl_Factura_Solicitud.Detalle].Id_Solicitud, Tbl_Factura_Solicitud.Cliente, (Tbl_Empleados.Nombres+' '+Tbl_Empleados.Apellidos) as 'Vendedor', Tbl_Factura_Solicitud.Cedula, Tbl_Factura_Solicitud.Recibo, Tbl_Factura_Solicitud.Const, Tbl_Factura_Solicitud.Fiador, isnull([Tbl_Factura_Solicitud.Detalle].Id_Factura,'') as 'Id_Factura', DREM.Costo as 'Costo'from [Tbl_Factura_Solicitud.Detalle] inner join Productos on Productos.Codigo_Producto=[Tbl_Factura_Solicitud.Detalle].Id_Producto inner join Tbl_Factura_Solicitud on Tbl_Factura_Solicitud.Id_Factura_Solicitud=[Tbl_Factura_Solicitud.Detalle].Id_Factura_Solicitud inner join Tbl_Empleados on Tbl_Empleados.Id_Empleado=Tbl_Factura_Solicitud.Id_Vendedor inner join [Tbl_Remision.Detalle] DREM on DREM.Id_Sucursal=Tbl_Factura_Solicitud.Id_Remision and DREM.Id_Sucursal=Tbl_Factura_Solicitud.Id_Sucursal where Tbl_Factura_Solicitud.Id_Remision='" + lbRemision.Text + "' and Tbl_Factura_Solicitud.Id_Sucursal=" & My.Settings.Sucursal & ""
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvVenta.Rows.Add(row0(0), row0(1), row0(2), row0(3), row0(4), "", row0(10), row0(5), row0(6), row0(7), row0(8), row0(9), row0(11), 0, 0, 0, 0, 1)
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub FrmRemision_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None
    End Sub
    Private Sub dgvRemision_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRemision.CellMouseClick
        If dgvRemision.RowCount > 0 Then
            Try
                btnRmenos.BackColor = Color.MistyRose
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub btnRmas_Click(sender As Object, e As EventArgs) Handles btnRmas.Click
        Dim bandera As Boolean = False
        If codigo <> "" Then
            If Cantidad <= (Existencia) And (Cantidad > 0) Then
                If dgvRemision.Rows.Count = 0 Then
                    dgvRemision.Rows.Add(codigo, txtCantidad.Text, Producto, Costo_Promedio, 1)
                Else
                    For i = 0 To dgvRemision.Rows.Count - 1
                        If dgvRemision.Rows(i).Cells(0).Value.ToString = codigo Then 'And dgvRemision.Rows(i).Cells(4).Value = 1 Then

                            dgvRemision.Rows(i).Cells(1).Value += Cantidad
                            dgvRemision.Rows(i).Cells(3).Value = Costo_Promedio
                            bandera = True
                            Exit For

                        End If

                    Next

                    If bandera = False Then dgvRemision.Rows.Add(codigo, txtCantidad.Text, Producto, Costo_Promedio, 1)


                    'Remision_Guardar_Detalle(1)
                End If
                Remision_Guardar_Detalle(1)
                btnRmas.BackColor = SystemColors.ButtonHighlight
                btnRmenos.BackColor = SystemColors.ButtonHighlight
                btnRmenos.Text = "Remover"
                codigo = ""
                txtCantidad.Text = 0
                txtCantidad.Enabled = False
                txtCantidad.Text = 0
                codigo = ""
                Cantidad = 0
                btnPrefactura.Text = "Aplicar"
            Else
                If Cantidad = 0 Then MsgBox("La cantidad debe ser mayor a cero") Else txtCantidad.Text = Existencia : MsgBox("Existencias insuficientes")

            End If
        Else
            frmBusquedaProd.Show()
        End If
    End Sub
    Private Sub btnRmenos_Click(sender As Object, e As EventArgs) Handles btnRmenos.Click
        Try
            If dgvRemision.RowCount > 0 And txtCantidad.Enabled = False Then
                'Remision_Borrar_Detalle()
                Remision_Guardar_Detalle(2)
                dgvRemision.Rows.RemoveAt(dgvRemision.CurrentRow.Index)
                dgvRemision.ClearSelection()
                'If dgvRemision.Rows(dgvRemision.CurrentRow.Index).Cells(4).Value = 1 Then
                '    'Remision_Borrar_Detalle()
                '    dgvRemision.Rows.RemoveAt(dgvRemision.CurrentRow.Index)
                '    dgvRemision.ClearSelection()
                'Else
                '    MsgBox("Existen un movimiento registrado para este articulo" & vbNewLine & "Desea regresar articulo a inventario?")
                'End If
                btnPrefactura.Text = "Aplicar"
            Else
                btnRmenos.Text = "Remover"
            End If
            btnRmas.BackColor = SystemColors.ButtonHighlight
            btnRmenos.BackColor = SystemColors.ButtonHighlight
            txtCantidad.Text = 0
            codigo = ""
            txtCantidad.Enabled = False
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Remision_Borrar_Detalle()
        sqlstring = "delete from [Tbl_Remision.Detalle] where Id_Remision='" + lbRemision.Text + "' and  Id_Producto='" + dgvRemision.Rows(dgvRemision.CurrentRow.Index).Cells(0).Value.ToString + "'"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error al guardar Detalle de remision: " + ex.Message)
        End Try
    End Sub
    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged
        Try
            Cantidad = txtCantidad.Text
        Catch ex As Exception
            MsgBox("Cantidad no valida")
        End Try
    End Sub
    Private Sub txtvehiculo_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dt As DataTable = New DataTable()
        sqlstring = "select Nombre_Bodega, Tbl_Vehiculo.Odo_Final as Odo from Almacenes inner join Tbl_Vehiculo on Tbl_Vehiculo.Nombre_Ruta=Almacenes.Nombre_Bodega where Nombre_Bodega=(select Nombre_Ruta from Tbl_Vehiculo where Placa='" + txtVehiculo.Text + "')"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row1 In dt.Rows
                'cbDestino.SelectedItem = row1(0)
                txtOdoI.Text = row1(1)
            Next
            con.Close()
            dt.Rows.Clear()
            dgv_Remisiones2()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub btnVagregar_Click(sender As Object, e As EventArgs) Handles btnPrefactura.Click
        If dgvRemision.RowCount = 0 Then MsgBox("No ha ingresado Articulos a la REMISION") : Exit Sub
        If btnPrefactura.Text = "Aplicar" Then
            txtCantidad.Text = 0
            codigo = ""
            Cantidad = 0
            Costo_Promedio = 0
            btnPrefactura.Text = "Prefactura"
            '------PA-------------------
            Select Case MsgBox("Desea imprimir REMISION?", MsgBoxStyle.YesNo, "")
                Case MsgBoxResult.Yes
                    Print()
            End Select
        Else

            'If UserPerfil = "0003" Then MsgBox("Acceso Deneado") : Exit Sub

            Select Case MsgBox("Cambiar estado de Remision a prefacturacion?" & vbNewLine & "Al cambiar estado, no permitira Agregar o Remover articulos.", MsgBoxStyle.YesNo, "")
                Case MsgBoxResult.Yes

                    'Dim Salida As String
                    'Dim Entrada As String
                    Dim resul As String = ""
                    Dim div As Double = 0
                    Dim inventario As New clsInventario(My.Settings.SolIndustrialCNX)

                    pPrefacturacion.Enabled = True
                    pEntrada.Enabled = True
                    cbEstado.SelectedIndex = 1
                    Estado = 2
                    Remision_Estado()
                    dgvRemision.ClearSelection()
                    pDetalle.Enabled = False

                    cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(4, 5), 8)
                    ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, lbRemision.Text, 3040, "SALIDA DE PRODUCTOS ", 0, dtpFecha.Value, 5, 0, 1, 4)

                    For i = 0 To dgvRemision.Rows.Count - 1

                        ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, dgvRemision.Rows(i).Cells(0).Value, dgvRemision.Rows(i).Cells(1).Value, dgvRemision.Rows(i).Cells(3).Value, "", 4, False, 1, 5)

                    Next

                    cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(Id_Bodega, 4), 8)
                    ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, lbRemision.Text, 3040, "ENTRADA A: " + luRuta.Text + "", 0, dtpFecha.Value, 4, 0, 1, Id_Bodega)

                    For i = 0 To dgvRemision.Rows.Count - 1

                        ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, dgvRemision.Rows(i).Cells(0).Value, dgvRemision.Rows(i).Cells(1).Value, dgvRemision.Rows(i).Cells(3).Value, "", Id_Bodega, False, 1, 4)

                        dgvEntrada.Rows.Add(dgvRemision.Rows(i).Cells(0).Value, dgvRemision.Rows(i).Cells(1).Value, dgvRemision.Rows(i).Cells(2).Value, dgvRemision.Rows(i).Cells(3).Value)

                    Next

                    'Movimiento_Master(My.Settings.Sucursal, cbOrigen.Text, 5, Nothing, lbRemision.Text, 3040, "SALIDA DE PRODUCTOS TERMINADOS", dtpFecha.Value, 1) 'salida

                    'Movimiento_Master(My.Settings.Sucursal, cbDestino.Text, 4, Nothing, lbRemision.Text, 3040, "ENTRADA A: " + cbDestino.Text + "", dtpFecha.Value, 1) 'entrada

                    'For i = 0 To dgvRemision.Rows.Count - 1
                    '    Dim producto As String = (dgvRemision.Rows(i).Cells(0).Value.ToString)
                    '    Dim cantidad As Decimal = (dgvRemision.Rows(i).Cells(1).Value)
                    '    Dim costo As Decimal = (dgvRemision.Rows(i).Cells(3).Value)

                    '    Movimiento_Detalle(My.Settings.Sucursal, cbOrigen.Text, 5, Nothing, producto, cantidad, costo, Nothing, 1, lbRemision.Text) 'SALIDA

                    '    Movimiento_Detalle(My.Settings.Sucursal, cbDestino.Text, 4, Nothing, producto, cantidad, costo, Nothing, 1, lbRemision.Text) 'ENTRADA

                    'Next
            End Select
        End If
    End Sub
    Sub Remision_Estado()
        sqlstring = " Update Tbl_Remision set [Estado]='" & Estado & "' where Id_Remision ='" + lbRemision.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "'"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            MsgBox("Error al actualizar Estado")
        End Try
    End Sub
    Sub Remision_Total()
        For i = 0 To dgvVenta.Rows.Count - 1
            If dgvVenta.Rows(i).Cells(17).Value = 0 Then
                sqlstring = "Update [Tbl_Remision.Detalle] set Cant_vendida+='" & dgvVenta.Rows(i).Cells(1).Value & "' where Id_Sucursal='" & My.Settings.Sucursal & "' and Id_Remision='" + lbRemision.Text + "' and Id_Producto='" + dgvVenta.Rows(i).Cells(0).Value + "' "
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                Try
                    con.Open()
                    cmd.ExecuteNonQuery()
                    con.Close()
                Catch ex As Exception
                    MsgBox("Error: " + ex.Message)
                End Try
            End If
        Next
    End Sub
    Private Sub dgvVenta_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvVenta.RowsAdded
        btnCerrar.BackColor = Color.DarkOrange
    End Sub
    Private Sub pmaster_EnabledChanged(sender As Object, e As EventArgs) Handles pmaster.EnabledChanged
        If pmaster.Enabled = True Then
            pmasterDeco.ForeColor = Color.DodgerBlue
            frmPrincipal.bbiGuardar.Enabled = True
        Else
            pmasterDeco.ForeColor = SystemColors.AppWorkspace
            frmPrincipal.bbiGuardar.Enabled = False
        End If
    End Sub
    Private Sub pDetalle_EnabledChanged(sender As Object, e As EventArgs) Handles pDetalle.EnabledChanged
        If pDetalle.Enabled = True Then
            pdetalleDeco.ForeColor = Color.DodgerBlue
            btnPrefactura.BackColor = Color.DarkOrange

            If UserPerfil = "0003" And btnPrefactura.Text = "Prefacturar" Then btnPrefactura.Enabled = False : btnPrefactura.BackColor = Color_Disable
            If UserPerfil = "0011" And btnPrefactura.Text = "Aplicar" Then btnPrefactura.Enabled = False : btnPrefactura.BackColor = Color_Disable

            If UserPerfil = "0011" Then txtCantidad.Enabled = False : btnRmas.Enabled = False : btnRmas.BackColor = Color_Disable : btnRmenos.Enabled = False : btnRmenos.BackColor = Color_Disable

        Else
            pdetalleDeco.ForeColor = SystemColors.AppWorkspace
            btnPrefactura.BackColor = SystemColors.ButtonHighlight
        End If
    End Sub
    Private Sub pPrefacturacion_EnabledChanged(sender As Object, e As EventArgs) Handles pPrefacturacion.EnabledChanged
        If pPrefacturacion.Enabled = True Then
            pprefacturacionDeco.ForeColor = Color.DodgerBlue
            btnVagregar.BackColor = Color.DarkOrange
            btnCerrar.BackColor = Color.DarkOrange
            btnAplicar.BackColor = Color.DarkOrange

            If UserPerfil = "0003" Then btnAplicar.Enabled = False : btnAplicar.BackColor = SystemColors.ButtonHighlight : btnVagregar.Enabled = False : btnVagregar.BackColor = SystemColors.ButtonHighlight
            If UserPerfil = "0011" Then btnCerrar.Enabled = False : btnCerrar.BackColor = SystemColors.ButtonHighlight

            Clientes()
            cb_Vendedor()
        Else
            pprefacturacionDeco.ForeColor = SystemColors.AppWorkspace
            btnVagregar.BackColor = SystemColors.ButtonHighlight
            btnCerrar.BackColor = SystemColors.ButtonHighlight
            btnAplicar.BackColor = SystemColors.ButtonHighlight
        End If
    End Sub
    Private Sub pEntrada_EnabledChanged(sender As Object, e As EventArgs) Handles pEntrada.EnabledChanged
        If pEntrada.Enabled = True Then
            pEntradadeco.ForeColor = Color.DodgerBlue
        Else
            pEntradadeco.ForeColor = SystemColors.AppWorkspace
        End If
    End Sub
    Private Sub dgvEntrada_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvEntrada.CellMouseClick
        btnVagregar.Text = "Agregar"
        dgvVenta.ClearSelection()
        Try
            Existencia = dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value
            luCliente.Focus()
            txtvCantidad.Enabled = True
            If cbTipo.SelectedIndex = 1 Then
                txtvCantidad.Text = dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value
                txtvCantidad.Enabled = False
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub Borrar_Venta()
        luCliente.EditValue = -1
        txtvCantidad.Text = 0
        txtvSolicitud.Text = ""
        txtvGarantia.Text = ""
        ckbCedula.Checked = False
        ckbRecibo.Checked = False
        ckbConstancia.Checked = False
        ckbFiador.Checked = False
        cbvTipo.SelectedIndex = 0
        Cantidad = 0
        Existencia = 0
    End Sub
    Private Sub btnVagregar_Click_1(sender As Object, e As EventArgs) Handles btnVagregar.Click
        Venta_Entrada()
    End Sub
    Sub Venta_Entrada()

        Try
            Cantidad = txtvCantidad.Text
        Catch ex As Exception
            txtvCantidad.Text = 0
            Cantidad = 0
        End Try

        Try
            If btnVagregar.Text = "Agregar" Then
                If dgvEntrada.SelectedRows.Count > 0 And Cantidad > 0 Then

                    Existencia = dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value

                    Select Case cbTipo.SelectedIndex

                        Case 0 'detalle

                            If cbvTipo.SelectedIndex = 0 Then
                                If luCliente.Text = "" Then MsgBox("No existen clientes verificados") : Exit Sub
                            End If

                        Case 1 'mayorista y/o Contado

                    End Select

                    If Cantidad <= Existencia Then
                        If cbTipo.SelectedIndex = 0 And cbvTipo.SelectedIndex = 0 Then 'DETALLE AL CREDITO
                            If Cantidad > 1 Then
                                For i = 1 To Cantidad
                                    FrmSolicitud_Garantia.ShowDialog()
                                    FrmSolicitud_Garantia.lbNum.Text = Microsoft.VisualBasic.Right("00" & i + 1, 2)
                                    dgvVenta.Rows.Add(dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(0).Value.ToString, 1, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(2).Value.ToString, Solicitud, luCliente.Text, "", "", luVendedor.Text, ckbCedula.Checked, ckbRecibo.Checked, ckbConstancia.Checked, ckbFiador.Checked, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(3).Value, cbvTipo.SelectedIndex + 1, Garantia, luCliente.EditValue, luVendedor.EditValue, 0)
                                Next
                                dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value -= Cantidad
                                dgvVenta.Sort(dgvVenta.Columns(4), System.ComponentModel.ListSortDirection.Descending)
                            Else
                                dgvVenta.Rows.Add(dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(0).Value.ToString, 1, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(2).Value.ToString, txtvSolicitud.Text, luCliente.Text, "", "", luVendedor.Text, ckbCedula.Checked, ckbRecibo.Checked, ckbConstancia.Checked, ckbFiador.Checked, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(3).Value, cbvTipo.SelectedIndex + 1, txtvGarantia.Text, luCliente.EditValue, luVendedor.EditValue, 0)
                                dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value -= Cantidad
                                dgvVenta.Sort(dgvVenta.Columns(4), System.ComponentModel.ListSortDirection.Descending)
                            End If
                        Else 'MAYORISTA O DE CONTADO
                            If cbTipo.SelectedIndex = 0 Then  'no puede ser asi
                                If Cantidad > 1 Then
                                    For i = 1 To Cantidad
                                        FrmSolicitud_Garantia.ShowDialog()
                                        FrmSolicitud_Garantia.lbNum.Text = Microsoft.VisualBasic.Right("00" & i + 1, 2)
                                        dgvVenta.Rows.Add(dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(0).Value.ToString, 1, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(2).Value.ToString, txtvSolicitud.Text, luCliente.Text, "", "", luVendedor.Text, ckbCedula.Checked, ckbRecibo.Checked, ckbConstancia.Checked, ckbFiador.Checked, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(3).Value, cbvTipo.SelectedIndex + 1, Garantia)
                                    Next
                                    dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value -= Cantidad
                                    dgvVenta.Sort(dgvVenta.Columns(4), System.ComponentModel.ListSortDirection.Descending)
                                Else
                                    dgvVenta.Rows.Add(dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(0).Value.ToString, 1, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(2).Value.ToString, txtvSolicitud.Text, luCliente.Text, "", "", luVendedor.Text, ckbCedula.Checked, ckbRecibo.Checked, ckbConstancia.Checked, ckbFiador.Checked, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(3).Value, cbvTipo.SelectedIndex + 1, txtvGarantia.Text, luCliente.EditValue, luVendedor.EditValue, 0)
                                    dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value -= Cantidad
                                    dgvVenta.Sort(dgvVenta.Columns(4), System.ComponentModel.ListSortDirection.Descending)
                                End If

                            Else
                                dgvVenta.Rows.Add(dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(0).Value.ToString, Cantidad, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(2).Value.ToString, txtvSolicitud.Text, luCliente.Text, "", "", luVendedor.Text, ckbCedula.Checked, ckbRecibo.Checked, ckbConstancia.Checked, ckbFiador.Checked, dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(3).Value, cbvTipo.SelectedIndex + 1, txtvGarantia.Text, luCliente.EditValue, luVendedor.EditValue, 0)
                                dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value -= Cantidad
                                dgvVenta.Sort(dgvVenta.Columns(4), System.ComponentModel.ListSortDirection.Descending)
                            End If

                        End If
                    Else
                        MsgBox("Existencias insuficientes." & vbNewLine & "La cantidad a factuar no puede ser mayor que: " & Existencia & "")
                        txtvCantidad.Text = Existencia
                        txtvCantidad.Focus()
                        txtvCantidad.SelectAll()
                        Exit Sub
                    End If
                Else
                    txtvCantidad.Text = Existencia
                    Exit Sub
                End If
            ElseIf btnVagregar.Text = "Remover" Then
                If dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(17).Value = 1 Then MsgBox("No puede eliminar una venta aplicada") : Exit Sub
                For i = 0 To dgvEntrada.Rows.Count - 1
                    If dgvEntrada.Rows(i).Cells(0).Value = dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(0).Value Then
                        Cantidad = dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(1).Value
                        dgvEntrada.Rows(i).Cells(1).Value += Cantidad
                        dgvVenta.Rows.RemoveAt(dgvVenta.CurrentRow.Index)
                    End If
                Next
            End If
            dgvEntrada.ClearSelection()
            dgvVenta.ClearSelection()
            Borrar_Venta()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub Print()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Remision.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Remision, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Remision.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Remision")
        xlWorkSheet.Cells(4, 2) = txtSupervisor.Text
        xlWorkSheet.Cells(5, 2) = txtVehiculo.Text
        'xlWorkSheet.Cells(5, 5) = cbDestino.Text
        'xlWorkSheet.Cells(5, 5) = cbDestino.Text
        'xlWorkSheet.Cells(5, 7) = 4
        'xlWorkSheet.Cells(5, 9) = cbDestino.Text
        xlWorkSheet.Cells(5, 11) = dtpFecha.Text
        xlWorkSheet.Cells(3, 8) = "No. " + lbRemision.Text
        xlWorkSheet.Cells(3, 7) = "No. " + lbremision2.Text
        Dim i As Integer = 1
        Dim f As Integer = 10
        Do While f <= 15
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""

            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            f = f + 1
        Loop
        f = 20
        Do While f <= 25
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            xlWorkSheet.Cells(f, 12) = ""
            xlWorkSheet.Cells(f, 13) = ""
            f = f + 1
        Loop
        f = 10
        Do While i <= dgvRemision.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvRemision.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvRemision.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 3) = dgvRemision.Rows((i - 1)).Cells(2).Value
            i = i + 1
            f = f + 1
        Loop
        xlWorkBook.Application.DisplayAlerts = False
        Try
            xlWorkBook.Save()
        Catch ex As Exception
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End Try
        '-----------------PRINT EXCEL--------------------------------
        xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        '------------------------------------------------------------
        xlWorkBook.Close()
        xlApp.Quit()
        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

    End Sub
    Sub Print2()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Remision.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Remision, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Remision.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Remision")
        xlWorkSheet.Cells(4, 2) = txtSupervisor.Text
        xlWorkSheet.Cells(5, 2) = txtVehiculo.Text
        'xlWorkSheet.Cells(5, 5) = cbDestino.Text
        'xlWorkSheet.Cells(5, 5) = cbDestino.Text
        'xlWorkSheet.Cells(5, 7) = 4
        'xlWorkSheet.Cells(5, 9) = cbDestino.Text
        xlWorkSheet.Cells(5, 11) = dtpFecha.Text
        xlWorkSheet.Cells(3, 8) = "No. " + lbRemision.Text
        xlWorkSheet.Cells(3, 7) = "No. " + lbremision2.Text
        Dim i As Integer = 1
        Dim f As Integer = 10
        Do While f <= 15
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""

            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            f = f + 1
        Loop
        f = 10
        Do While i <= dgvRemision.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvRemision.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvRemision.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 3) = dgvRemision.Rows((i - 1)).Cells(2).Value
            i = i + 1
            f = f + 1
        Loop
        f = 10
        i = 1

        Do While i <= dgvEntrada.Rows.Count
            xlWorkSheet.Cells(f, 8) = dgvEntrada.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 9) = dgvEntrada.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 10) = dgvEntrada.Rows((i - 1)).Cells(2).Value
            i = i + 1
            f = f + 1
        Loop
        f = 20
        i = 1
        Do While f <= 25
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""

            xlWorkSheet.Cells(f, 6) = ""

            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            xlWorkSheet.Cells(f, 12) = ""
            xlWorkSheet.Cells(f, 13) = ""

            f = f + 1
        Loop
        f = 20
        i = 1
        Do While i <= dgvVenta.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvVenta.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 2) = dgvVenta.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 3) = dgvVenta.Rows((i - 1)).Cells(2).Value

            xlWorkSheet.Cells(f, 6) = dgvVenta.Rows((i - 1)).Cells(4).Value
            xlWorkSheet.Cells(f, 8) = dgvVenta.Rows((i - 1)).Cells(5).Value
            xlWorkSheet.Cells(f, 9) = dgvVenta.Rows((i - 1)).Cells(6).Value
            'xlWorkSheet.Cells(f, 3) = dgvVenta.Rows((i - 1)).Cells(2).Value vendedor

            If dgvVenta.Rows((i - 1)).Cells(8).Value = True Then
                xlWorkSheet.Cells(f, 10) = "SI"
            End If
            If dgvVenta.Rows((i - 1)).Cells(9).Value = True Then
                xlWorkSheet.Cells(f, 11) = "SI"
            End If
            If dgvVenta.Rows((i - 1)).Cells(10).Value = True Then
                xlWorkSheet.Cells(f, 12) = "SI"
            End If
            If dgvVenta.Rows((i - 1)).Cells(11).Value = True Then
                xlWorkSheet.Cells(f, 13) = "SI"
            End If
            'xlWorkSheet.Cells(f, 10) = dgvVenta.Rows((i - 1)).Cells(8).Value
            'xlWorkSheet.Cells(f, 11) = dgvVenta.Rows((i - 1)).Cells(9).Value
            'xlWorkSheet.Cells(f, 12) = dgvVenta.Rows((i - 1)).Cells(10).Value
            'xlWorkSheet.Cells(f, 13) = dgvVenta.Rows((i - 1)).Cells(11).Value

            i = i + 1
            f = f + 1
        Loop
        xlWorkBook.Application.DisplayAlerts = False
        Try
            xlWorkBook.Save()

            '-----------------PRINT EXCEL--------------------------------
            xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
            'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
            '------------------------------------------------------------
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        Catch ex As Exception
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End Try

    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub
    Private Sub dgvVenta_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVenta.CellMouseClick
        btnVagregar.Text = "Remover"
        dgvEntrada.ClearSelection()
    End Sub

    Private Sub txtvCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtvCantidad.TextChanged
        If pPrefacturacion.Enabled = True Then
            Try
                Cantidad = txtvCantidad.Text
            Catch ex As Exception
            End Try
            If Cantidad > 1 Then
                txtvSolicitud.Enabled = False
                txtvGarantia.Enabled = False
            Else
                txtvSolicitud.Enabled = True
                txtvGarantia.Enabled = True
            End If
        End If
    End Sub

    Private Sub dgvRemision_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvRemision.RowsAdded
        btnPrefactura.Text = "Prefactura"
        For i = 0 To dgvRemision.Rows.Count - 1
            If dgvRemision.Rows(i).Cells(4).Value = 1 Then
                btnPrefactura.Text = "Aplicar"
            Else
                btnPrefactura.Text = "Prefactura"
            End If
        Next
    End Sub
    Sub Busqueda()
        Select Case cbEstado.SelectedIndex
            Case 0 'EMITIDA
                pDetalle.Enabled = True
            Case 1 'PREFACTURAR
                'MsgBox("La remision en estado " + cbEstado.Text + " no puede ser editada")
                'If dgvEntrada.RowCount - 1 > 0 Then
                pPrefacturacion.Enabled = True
                pEntrada.Enabled = True
                'End If
            Case 2 To 3 'CERRADA O ANULADA
                MsgBox("La remision en estado " + cbEstado.Text + " no puede ser editada")
        End Select
    End Sub
    Private Sub cbvTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbvTipo.SelectedIndexChanged
        If cbTipo.SelectedIndex = 1 Or cbvTipo.SelectedIndex = 1 Then
            txtvSolicitud.Enabled = False
        Else
            txtvSolicitud.Enabled = True
        End If
        Clientes()
    End Sub
    Private Sub cbDestino_SelectedIndexChanged(sender As Object, e As EventArgs)
        Clientes()
    End Sub
    Sub Clientes()
        Try

            luCliente.Properties.DataSource = SQL("select  Id_Cliente as 'Codigo',Nombre + ' '+ Apellido as Nombre from Tbl_TransUnion", "tbl_Cliente", My.Settings.SolIndustrialCNX).Tables(0)
            luCliente.Properties.DisplayMember = "Nombre"
            luCliente.Properties.ValueMember = "Codigo"
            luCliente.Properties.Columns.Add(New LookUpColumnInfo("Codigo", "Id", 20))
            luCliente.Properties.Columns.Add(New LookUpColumnInfo("Nombre", "Nombre", 100))
            luCliente.Properties.Columns(0).Visible = False
            luCliente.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            luCliente.ItemIndex = 0

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try

    End Sub
    Private Sub cbTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipo.SelectedIndexChanged
        If cbTipo.SelectedIndex = 0 Then
            cbvTipo.Enabled = True
        Else
            cbvTipo.SelectedIndex = 1
            cbvTipo.Enabled = False
        End If
    End Sub

    Private Sub txtOdoI_TextChanged(sender As Object, e As EventArgs) Handles txtOdoI.TextChanged
        txtOdoF.Text = txtOdoI.Text
    End Sub

    Private Sub lbRemision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtvSolicitud.KeyPress, txtvGarantia.KeyPress, txtvCantidad.KeyPress, lbRemision.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub

    Private Sub luRuta_EditValueChanged(sender As Object, e As EventArgs) Handles luRuta.EditValueChanged
        Dim expression As String = "Id_Ruta =" & luRuta.EditValue & ""
        DR_Ruta = Dt_Ruta.Select(expression)

        Id_Coordinador = DR_Ruta(0)("id_Coordinador")
        Id_Supervisor = DR_Ruta(0)("Id_Supervisor")
        Id_Vehiculo = DR_Ruta(0)("Id_Vehiculo")
        Id_Bodega = DR_Ruta(0)("Id_Bodega")

        txtCoordinador.Text = DR_Ruta(0)("Coordinador")
        txtSupervisor.Text = DR_Ruta(0)("Supervisor")
        txtVehiculo.Text = DR_Ruta(0)("Placa")
        txtOdoI.Text = DR_Ruta(0)("Odo")
        txtOdoF.Text = DR_Ruta(0)("Odo")

    End Sub

    Private Sub btnasda_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click

        Dim count As Integer = 0

        For i = 0 To dgvVenta.Rows.Count - 1
            count = 0
            For j = i To dgvVenta.Rows.Count - 1
                If dgvVenta.Rows(i).Cells(4).Value.ToString = dgvVenta.Rows(j).Cells(4).Value.ToString Then
                    count = count + 1
                    If count = 1 Then
                        Try

                            If dgvVenta.Rows(j).Cells(17).Value = 0 Then

                                sqlstring = "insert into Tbl_Factura_Solicitud (Id_Sucursal, Id_Remision, Id_Cliente, Id_CR, Cliente, Fecha, Id_Vendedor, Cedula, Recibo, Const, Fiador, Facturado, Tipo) values (@sucursal, @remision, @id_cliente, @id_cr, @cliente, @fecha, @id_vendedor, @cedula, @recibo, @const, @fiador, @facturado, @tipo)"

                                con.Open()
                                cmd = New SqlCommand(sqlstring, con)
                                cmd.Parameters.AddWithValue("@sucursal", My.Settings.Sucursal)
                                cmd.Parameters.AddWithValue("@remision", lbRemision.Text)
                                cmd.Parameters.AddWithValue("@id_cliente", dgvVenta.Rows(i).Cells(5).Value) 'asjdhaskdaskhdkjsa
                                cmd.Parameters.AddWithValue("@id_cr", dgvVenta.Rows(i).Cells(15).Value)
                                cmd.Parameters.AddWithValue("@cliente", dgvVenta.Rows(i).Cells(4).Value.ToString)
                                cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Text))
                                cmd.Parameters.AddWithValue("@id_vendedor", dgvVenta.Rows(i).Cells(16).Value)
                                cmd.Parameters.AddWithValue("@cedula", dgvVenta.Rows(i).Cells(8).Value)
                                cmd.Parameters.AddWithValue("@recibo", dgvVenta.Rows(i).Cells(9).Value)
                                cmd.Parameters.AddWithValue("@const", dgvVenta.Rows(i).Cells(10).Value)
                                cmd.Parameters.AddWithValue("@fiador", dgvVenta.Rows(i).Cells(11).Value)
                                cmd.Parameters.AddWithValue("@facturado", 0)
                                cmd.Parameters.AddWithValue("@tipo", dgvVenta.Rows(i).Cells(13).Value)
                                cmd.ExecuteNonQuery()
                                con.Close()

                                '=========================
                                For x = 0 To dgvVenta.Rows.Count - 1    'REVISAR MAYORISTA AL MOSTRAR LOS RESULTADOS EN EL DGVVENTAS
                                    If dgvVenta.Rows(i).Cells(4).Value.ToString = dgvVenta.Rows(x).Cells(4).Value.ToString Then

                                        Try
                                            sqlstring = "Insert into [Tbl_Factura_Solicitud.Detalle] (Id_Factura_Solicitud, Id_Remision, Id_Solicitud, Id_Sucursal, Id_Producto, Cantidad, Garantia, Costo) values ((select isnull(max(Id_Factura_Solicitud),0) from Tbl_Factura_Solicitud where Id_Remision='" + lbRemision.Text + "' and Id_Sucursal='" & My.Settings.Sucursal & "'),'" + lbRemision.Text + "','" + dgvVenta.Rows(x).Cells(3).Value.ToString + "','" & My.Settings.Sucursal & "','" + dgvVenta.Rows(x).Cells(0).Value.ToString + "','" & dgvVenta.Rows(x).Cells(1).Value & "', '" + dgvVenta.Rows(x).Cells(14).Value + "', '" & dgvVenta.Rows(x).Cells(12).Value & "')"
                                            cmd = New SqlCommand(sqlstring, con)
                                            cmd.CommandType = CommandType.Text
                                            con.Open()
                                            cmd.ExecuteNonQuery()
                                            con.Close()

                                        Catch ex As Exception
                                            MsgBox("Error: detalle" + ex.Message)
                                            con.Close()
                                        End Try




                                        Try
                                            sqlstring = "Update [Tbl_Remision.Detalle] set Cant_vendida+='" & dgvVenta.Rows(x).Cells(1).Value & "' where Id_Sucursal='" & My.Settings.Sucursal & "' and Id_Remision='" + lbRemision.Text + "' and Id_Producto='" + dgvVenta.Rows(x).Cells(0).Value + "' "

                                            cmd = New SqlCommand(sqlstring, con)
                                            cmd.CommandType = CommandType.Text

                                            con.Open()
                                            cmd.ExecuteNonQuery()
                                            con.Close()

                                        Catch ex As Exception
                                            MsgBox("Error: " + ex.Message)
                                        End Try

                                        dgvVenta.Rows(j).Cells(17).Value = 1



                                    End If




                                Next
                                ' ===============

                            End If

                        Catch ex As Exception
                            MsgBox("Error: master" + ex.Message)
                            con.Close()
                        End Try
                    End If
                    i = j
                End If
            Next
        Next
        MsgBox("Ventas ingresada")
        'Remision_Total()

    End Sub
    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Select Case MsgBox("Desea Cerrar la REMISION?" & vbNewLine & "Una REMISION CERRADA no permitira registrar mas compras.", MsgBoxStyle.YesNo, "")

            Case MsgBoxResult.No

                Exit Sub

            Case MsgBoxResult.Yes

                MsgBox("Remision Cerrada")
                cbEstado.SelectedIndex = 2
                Estado = 3
                Remision_Estado()
                pPrefacturacion.Enabled = False
                pEntrada.Enabled = False
                FrmOdometro.ShowDialog()
                Update_Odo()
                Update_RemiOdo()

                '------pa--------------
                Dim resul As String = ""
                Dim div As Double = 0
                Dim inventario As New clsInventario(My.Settings.SolIndustrialCNX)
                Try

                    cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(Id_Bodega, 5), 8)
                    ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, lbRemision.Text, 3040, "SALIDA DE PRODUCTOS " + luRuta.Text + "", 0, dtpFecha.Value, 5, 0, 1, Id_Bodega)
                    For i = 0 To dgvEntrada.Rows.Count - 1
                        If dgvEntrada.Rows(i).Cells(1).Value > 0 Then
                            ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, dgvEntrada.Rows(i).Cells(0).Value, dgvEntrada.Rows(i).Cells(1).Value, dgvEntrada.Rows(i).Cells(3).Value, "", Id_Bodega, False, 1, 5)
                        End If
                    Next
                Catch ex As Exception
                    MsgBox("error: master salida")
                End Try

                If dgvVenta.Rows.Count > 0 Then

                    Try
                        Dim Fcodigo As String
                        Dim tcantidad As Decimal = 0
                        Dim Fcosto As Decimal = 0

                        cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(Id_Bodega, 5), 8)

                        ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, lbRemision.Text, 3040, "SALIDA CLIENTE DE " + luRuta.Text + "", 0, dtpFecha.Value, 5, 0, 1, Id_Bodega)

                        For i = 0 To dgvVenta.RowCount - 1


                            If dgvVenta.Rows(i).Cells(1).Value > 0 Then
                                For j = i To dgvVenta.RowCount - 1
                                    If dgvVenta.Rows(i).Cells(0).Value = dgvVenta.Rows(j).Cells(0).Value Then
                                        Fcodigo = dgvVenta.Rows(j).Cells(0).Value
                                        tcantidad += dgvVenta.Rows(j).Cells(1).Value
                                        Fcosto = dgvVenta.Rows(j).Cells(12).Value
                                        i = j
                                    End If
                                Next
                                ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, Fcodigo, tcantidad, Fcosto, "", Id_Bodega, False, 1, 5)

                                tcantidad = 0
                                Fcosto = 0
                            End If
                        Next

                    Catch ex As Exception
                        MsgBox("error: master salida")
                    End Try

                End If

                '-------------------------------------
                'entrada

                Dim Flag As Boolean = False
                For i = 0 To dgvEntrada.Rows.Count - 1
                    If dgvEntrada.Rows(i).Cells(1).Value > 0 Then
                        Flag = True
                    End If
                Next
                If Flag = True Then

                    Try

                        cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(4, 4), 8)
                        ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, lbRemision.Text, 3040, "ENTRADA A PRODUCTOS TERMINADOS", 0, dtpFecha.Value, 4, 0, 1, 4)
                        For i = 0 To dgvEntrada.Rows.Count - 1
                            If dgvEntrada.Rows(i).Cells(1).Value > 0 Then
                                ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, dgvEntrada.Rows(i).Cells(0).Value, dgvEntrada.Rows(i).Cells(1).Value, dgvEntrada.Rows(i).Cells(3).Value, "", 4, False, 1, 4)
                            End If
                        Next
                        txtCantidad.Text = 0
                        codigo = ""
                        Cantidad = 0
                        Costo_Promedio = 0
                        btnPrefactura.Text = "prefactura"
                    Catch ex As Exception
                        MsgBox("error: master entrada2")
                    End Try
                End If

                Select Case MsgBox("Desea imprimir REMISION?", MsgBoxStyle.YesNo, "")
                    Case MsgBoxResult.Yes
                        Print2()
                End Select


        End Select
    End Sub

    Private Sub btnPrefactura_TextChanged(sender As Object, e As EventArgs) Handles btnPrefactura.TextChanged

        If btnPrefactura.Text = "Prefactura" And UserPerfil = "0003" Then btnPrefactura.Enabled = False : btnPrefactura.BackColor = SystemColors.ButtonHighlight
        If btnPrefactura.Text = "Aplicar" And UserPerfil = "0011" Then btnPrefactura.Enabled = False : btnPrefactura.BackColor = SystemColors.ButtonHighlight
    End Sub

    Private Sub txtVehiculo_TextChanged(sender As Object, e As EventArgs) Handles txtVehiculo.TextChanged
        dgv_Remisiones2()
    End Sub
End Class
