﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRemision
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRemision))
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtvCantidad = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnPrefactura = New System.Windows.Forms.Button()
        Me.dgvVenta = New System.Windows.Forms.DataGridView()
        Me.btnRmenos = New System.Windows.Forms.Button()
        Me.btnRmas = New System.Windows.Forms.Button()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.dgvRemision = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.pEntrada = New System.Windows.Forms.Panel()
        Me.dgvEntrada = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pPrefacturacion = New System.Windows.Forms.Panel()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.luVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.luCliente = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtvGarantia = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cbvTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnVagregar = New System.Windows.Forms.Button()
        Me.ckbFiador = New System.Windows.Forms.CheckBox()
        Me.ckbConstancia = New System.Windows.Forms.CheckBox()
        Me.ckbRecibo = New System.Windows.Forms.CheckBox()
        Me.ckbCedula = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtvSolicitud = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.pmaster = New System.Windows.Forms.Panel()
        Me.txtVehiculo = New System.Windows.Forms.TextBox()
        Me.txtSupervisor = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCoordinador = New System.Windows.Forms.TextBox()
        Me.luRuta = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtOdoF = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtOdoI = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lbremision2 = New System.Windows.Forms.TextBox()
        Me.lbRemision = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cbEstado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pprefacturacionDeco = New System.Windows.Forms.Label()
        Me.pEntradadeco = New System.Windows.Forms.Label()
        Me.pdetalleDeco = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TblDepartBindingSource = New System.Windows.Forms.BindingSource()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMain.SuspendLayout()
        Me.pEntrada.SuspendLayout()
        CType(Me.dgvEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pPrefacturacion.SuspendLayout()
        CType(Me.luVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbvTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDetalle.SuspendLayout()
        Me.pmaster.SuspendLayout()
        CType(Me.luRuta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(45, 40)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 41
        Me.Label18.Text = "Cantidad:"
        '
        'txtvCantidad
        '
        Me.txtvCantidad.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtvCantidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtvCantidad.Location = New System.Drawing.Point(103, 37)
        Me.txtvCantidad.MaxLength = 10
        Me.txtvCantidad.Name = "txtvCantidad"
        Me.txtvCantidad.Size = New System.Drawing.Size(81, 20)
        Me.txtvCantidad.TabIndex = 3
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(230, 13)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 13)
        Me.Label19.TabIndex = 39
        Me.Label19.Text = "Cliente:"
        '
        'btnCerrar
        '
        Me.btnCerrar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnCerrar.Location = New System.Drawing.Point(969, 254)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(131, 23)
        Me.btnCerrar.TabIndex = 18
        Me.btnCerrar.Text = "&Cerrar Remision"
        Me.btnCerrar.UseVisualStyleBackColor = False
        '
        'btnPrefactura
        '
        Me.btnPrefactura.BackColor = System.Drawing.Color.DarkOrange
        Me.btnPrefactura.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnPrefactura.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnPrefactura.Location = New System.Drawing.Point(507, 101)
        Me.btnPrefactura.Name = "btnPrefactura"
        Me.btnPrefactura.Size = New System.Drawing.Size(86, 25)
        Me.btnPrefactura.TabIndex = 10
        Me.btnPrefactura.TabStop = False
        Me.btnPrefactura.Text = "Prefactura"
        Me.btnPrefactura.UseVisualStyleBackColor = False
        '
        'dgvVenta
        '
        Me.dgvVenta.AllowUserToAddRows = False
        Me.dgvVenta.AllowUserToDeleteRows = False
        Me.dgvVenta.AllowUserToResizeColumns = False
        Me.dgvVenta.AllowUserToResizeRows = False
        Me.dgvVenta.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVenta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column12, Me.Column19, Me.Column20, Me.Column21, Me.Column13, Me.Column14, Me.Column15})
        Me.dgvVenta.Location = New System.Drawing.Point(4, 70)
        Me.dgvVenta.MultiSelect = False
        Me.dgvVenta.Name = "dgvVenta"
        Me.dgvVenta.ReadOnly = True
        Me.dgvVenta.RowHeadersVisible = False
        Me.dgvVenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVenta.Size = New System.Drawing.Size(1096, 178)
        Me.dgvVenta.TabIndex = 23
        Me.dgvVenta.TabStop = False
        '
        'btnRmenos
        '
        Me.btnRmenos.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnRmenos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRmenos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnRmenos.FlatAppearance.BorderSize = 0
        Me.btnRmenos.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRmenos.Location = New System.Drawing.Point(507, 70)
        Me.btnRmenos.Name = "btnRmenos"
        Me.btnRmenos.Size = New System.Drawing.Size(86, 25)
        Me.btnRmenos.TabIndex = 7
        Me.btnRmenos.Text = "Remover"
        Me.btnRmenos.UseVisualStyleBackColor = False
        '
        'btnRmas
        '
        Me.btnRmas.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnRmas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRmas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnRmas.FlatAppearance.BorderSize = 0
        Me.btnRmas.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRmas.Location = New System.Drawing.Point(507, 39)
        Me.btnRmas.Name = "btnRmas"
        Me.btnRmas.Size = New System.Drawing.Size(86, 25)
        Me.btnRmas.TabIndex = 6
        Me.btnRmas.Text = "Agregar"
        Me.btnRmas.UseVisualStyleBackColor = False
        '
        'txtCantidad
        '
        Me.txtCantidad.Enabled = False
        Me.txtCantidad.Location = New System.Drawing.Point(507, 13)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(86, 20)
        Me.txtCantidad.TabIndex = 5
        '
        'dgvRemision
        '
        Me.dgvRemision.AllowUserToAddRows = False
        Me.dgvRemision.AllowUserToDeleteRows = False
        Me.dgvRemision.AllowUserToResizeColumns = False
        Me.dgvRemision.AllowUserToResizeRows = False
        Me.dgvRemision.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvRemision.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRemision.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column16, Me.Column17})
        Me.dgvRemision.Location = New System.Drawing.Point(55, 13)
        Me.dgvRemision.MultiSelect = False
        Me.dgvRemision.Name = "dgvRemision"
        Me.dgvRemision.ReadOnly = True
        Me.dgvRemision.RowHeadersVisible = False
        Me.dgvRemision.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRemision.Size = New System.Drawing.Size(434, 119)
        Me.dgvRemision.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "Codigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 80
        '
        'Column2
        '
        Me.Column2.HeaderText = "Cant"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 80
        '
        'Column3
        '
        Me.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column3.HeaderText = "Descripcion"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column16
        '
        Me.Column16.HeaderText = "Costo"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        Me.Column16.Visible = False
        '
        'Column17
        '
        Me.Column17.HeaderText = "Nuevo"
        Me.Column17.Name = "Column17"
        Me.Column17.ReadOnly = True
        Me.Column17.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(920, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Vehiculo:"
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.txtSucursal)
        Me.pMain.Controls.Add(Me.pEntrada)
        Me.pMain.Controls.Add(Me.Label3)
        Me.pMain.Controls.Add(Me.pPrefacturacion)
        Me.pMain.Controls.Add(Me.pDetalle)
        Me.pMain.Controls.Add(Me.pmaster)
        Me.pMain.Controls.Add(Me.Label54)
        Me.pMain.Controls.Add(Me.Label55)
        Me.pMain.Controls.Add(Me.Label10)
        Me.pMain.Controls.Add(Me.pprefacturacionDeco)
        Me.pMain.Controls.Add(Me.pEntradadeco)
        Me.pMain.Controls.Add(Me.pdetalleDeco)
        Me.pMain.Controls.Add(Me.pmasterDeco)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1118, 581)
        Me.pMain.TabIndex = 13
        '
        'txtSucursal
        '
        Me.txtSucursal.BackColor = System.Drawing.SystemColors.Control
        Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSucursal.Location = New System.Drawing.Point(904, 7)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.ReadOnly = True
        Me.txtSucursal.Size = New System.Drawing.Size(195, 15)
        Me.txtSucursal.TabIndex = 127
        Me.txtSucursal.TabStop = False
        Me.txtSucursal.Text = "DIMARSA CENTRAL S.A."
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pEntrada
        '
        Me.pEntrada.Controls.Add(Me.dgvEntrada)
        Me.pEntrada.Enabled = False
        Me.pEntrada.Location = New System.Drawing.Point(605, 123)
        Me.pEntrada.Name = "pEntrada"
        Me.pEntrada.Size = New System.Drawing.Size(506, 138)
        Me.pEntrada.TabIndex = 124
        '
        'dgvEntrada
        '
        Me.dgvEntrada.AllowUserToAddRows = False
        Me.dgvEntrada.AllowUserToDeleteRows = False
        Me.dgvEntrada.AllowUserToResizeColumns = False
        Me.dgvEntrada.AllowUserToResizeRows = False
        Me.dgvEntrada.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvEntrada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEntrada.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn24, Me.Column18})
        Me.dgvEntrada.Location = New System.Drawing.Point(3, 13)
        Me.dgvEntrada.MultiSelect = False
        Me.dgvEntrada.Name = "dgvEntrada"
        Me.dgvEntrada.ReadOnly = True
        Me.dgvEntrada.RowHeadersVisible = False
        Me.dgvEntrada.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEntrada.Size = New System.Drawing.Size(434, 119)
        Me.dgvEntrada.TabIndex = 122
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Cant"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 80
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn24.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        '
        'Column18
        '
        Me.Column18.HeaderText = "Costo"
        Me.Column18.Name = "Column18"
        Me.Column18.ReadOnly = True
        Me.Column18.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(621, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 13)
        Me.Label3.TabIndex = 126
        Me.Label3.Text = "ENTRADA A BODEGA"
        '
        'pPrefacturacion
        '
        Me.pPrefacturacion.Controls.Add(Me.btnAplicar)
        Me.pPrefacturacion.Controls.Add(Me.luVendedor)
        Me.pPrefacturacion.Controls.Add(Me.luCliente)
        Me.pPrefacturacion.Controls.Add(Me.txtvGarantia)
        Me.pPrefacturacion.Controls.Add(Me.Label17)
        Me.pPrefacturacion.Controls.Add(Me.cbvTipo)
        Me.pPrefacturacion.Controls.Add(Me.Label15)
        Me.pPrefacturacion.Controls.Add(Me.btnVagregar)
        Me.pPrefacturacion.Controls.Add(Me.btnCerrar)
        Me.pPrefacturacion.Controls.Add(Me.ckbFiador)
        Me.pPrefacturacion.Controls.Add(Me.ckbConstancia)
        Me.pPrefacturacion.Controls.Add(Me.ckbRecibo)
        Me.pPrefacturacion.Controls.Add(Me.ckbCedula)
        Me.pPrefacturacion.Controls.Add(Me.Label6)
        Me.pPrefacturacion.Controls.Add(Me.txtvSolicitud)
        Me.pPrefacturacion.Controls.Add(Me.Label4)
        Me.pPrefacturacion.Controls.Add(Me.Label18)
        Me.pPrefacturacion.Controls.Add(Me.Label19)
        Me.pPrefacturacion.Controls.Add(Me.txtvCantidad)
        Me.pPrefacturacion.Controls.Add(Me.dgvVenta)
        Me.pPrefacturacion.Location = New System.Drawing.Point(0, 279)
        Me.pPrefacturacion.Name = "pPrefacturacion"
        Me.pPrefacturacion.Size = New System.Drawing.Size(1111, 290)
        Me.pPrefacturacion.TabIndex = 125
        '
        'btnAplicar
        '
        Me.btnAplicar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnAplicar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAplicar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnAplicar.Location = New System.Drawing.Point(844, 254)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(119, 23)
        Me.btnAplicar.TabIndex = 164
        Me.btnAplicar.Text = "&Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = False
        '
        'luVendedor
        '
        Me.luVendedor.Location = New System.Drawing.Point(278, 37)
        Me.luVendedor.Name = "luVendedor"
        Me.luVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luVendedor.Size = New System.Drawing.Size(315, 20)
        Me.luVendedor.TabIndex = 4
        '
        'luCliente
        '
        Me.luCliente.Location = New System.Drawing.Point(278, 10)
        Me.luCliente.Name = "luCliente"
        Me.luCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luCliente.Size = New System.Drawing.Size(315, 20)
        Me.luCliente.TabIndex = 2
        '
        'txtvGarantia
        '
        Me.txtvGarantia.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtvGarantia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtvGarantia.Location = New System.Drawing.Point(706, 10)
        Me.txtvGarantia.Name = "txtvGarantia"
        Me.txtvGarantia.Size = New System.Drawing.Size(72, 20)
        Me.txtvGarantia.TabIndex = 5
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(622, 13)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(78, 13)
        Me.Label17.TabIndex = 50
        Me.Label17.Text = "Num. Garantia:"
        '
        'cbvTipo
        '
        Me.cbvTipo.EditValue = "CREDITO"
        Me.cbvTipo.Location = New System.Drawing.Point(103, 10)
        Me.cbvTipo.Name = "cbvTipo"
        Me.cbvTipo.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbvTipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbvTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbvTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbvTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbvTipo.Properties.Items.AddRange(New Object() {"CREDITO", "CONTADO", "CREDITO/CONTADO"})
        Me.cbvTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbvTipo.Size = New System.Drawing.Size(81, 20)
        Me.cbvTipo.TabIndex = 1
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(12, 13)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(85, 13)
        Me.Label15.TabIndex = 47
        Me.Label15.Text = "Tipo de Compra:"
        '
        'btnVagregar
        '
        Me.btnVagregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnVagregar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnVagregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVagregar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnVagregar.Location = New System.Drawing.Point(981, 34)
        Me.btnVagregar.Name = "btnVagregar"
        Me.btnVagregar.Size = New System.Drawing.Size(119, 23)
        Me.btnVagregar.TabIndex = 11
        Me.btnVagregar.Text = "Agregar"
        Me.btnVagregar.UseVisualStyleBackColor = False
        '
        'ckbFiador
        '
        Me.ckbFiador.BackColor = System.Drawing.SystemColors.Control
        Me.ckbFiador.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.ckbFiador.Location = New System.Drawing.Point(885, 36)
        Me.ckbFiador.Name = "ckbFiador"
        Me.ckbFiador.Size = New System.Drawing.Size(88, 23)
        Me.ckbFiador.TabIndex = 10
        Me.ckbFiador.Text = "Fiador"
        Me.ckbFiador.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ckbFiador.UseVisualStyleBackColor = False
        '
        'ckbConstancia
        '
        Me.ckbConstancia.BackColor = System.Drawing.SystemColors.Control
        Me.ckbConstancia.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.ckbConstancia.Location = New System.Drawing.Point(885, 9)
        Me.ckbConstancia.Name = "ckbConstancia"
        Me.ckbConstancia.Size = New System.Drawing.Size(88, 23)
        Me.ckbConstancia.TabIndex = 8
        Me.ckbConstancia.Text = "Const."
        Me.ckbConstancia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ckbConstancia.UseVisualStyleBackColor = False
        '
        'ckbRecibo
        '
        Me.ckbRecibo.BackColor = System.Drawing.SystemColors.Control
        Me.ckbRecibo.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.ckbRecibo.Location = New System.Drawing.Point(791, 36)
        Me.ckbRecibo.Name = "ckbRecibo"
        Me.ckbRecibo.Size = New System.Drawing.Size(88, 23)
        Me.ckbRecibo.TabIndex = 9
        Me.ckbRecibo.Text = "Recibo"
        Me.ckbRecibo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ckbRecibo.UseVisualStyleBackColor = False
        '
        'ckbCedula
        '
        Me.ckbCedula.BackColor = System.Drawing.SystemColors.Control
        Me.ckbCedula.FlatAppearance.CheckedBackColor = System.Drawing.Color.DodgerBlue
        Me.ckbCedula.Location = New System.Drawing.Point(791, 9)
        Me.ckbCedula.Name = "ckbCedula"
        Me.ckbCedula.Size = New System.Drawing.Size(88, 23)
        Me.ckbCedula.TabIndex = 7
        Me.ckbCedula.Text = "Cedula"
        Me.ckbCedula.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ckbCedula.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(216, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "Vendedor:"
        '
        'txtvSolicitud
        '
        Me.txtvSolicitud.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtvSolicitud.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtvSolicitud.Location = New System.Drawing.Point(706, 37)
        Me.txtvSolicitud.Name = "txtvSolicitud"
        Me.txtvSolicitud.Size = New System.Drawing.Size(72, 20)
        Me.txtvSolicitud.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(628, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 13)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "Num. Pagare:"
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.dgvRemision)
        Me.pDetalle.Controls.Add(Me.txtCantidad)
        Me.pDetalle.Controls.Add(Me.btnRmenos)
        Me.pDetalle.Controls.Add(Me.btnRmas)
        Me.pDetalle.Controls.Add(Me.btnPrefactura)
        Me.pDetalle.Enabled = False
        Me.pDetalle.Location = New System.Drawing.Point(0, 123)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(605, 138)
        Me.pDetalle.TabIndex = 124
        '
        'pmaster
        '
        Me.pmaster.Controls.Add(Me.txtVehiculo)
        Me.pmaster.Controls.Add(Me.txtSupervisor)
        Me.pmaster.Controls.Add(Me.Label9)
        Me.pmaster.Controls.Add(Me.txtCoordinador)
        Me.pmaster.Controls.Add(Me.luRuta)
        Me.pmaster.Controls.Add(Me.txtOdoF)
        Me.pmaster.Controls.Add(Me.Label8)
        Me.pmaster.Controls.Add(Me.txtOdoI)
        Me.pmaster.Controls.Add(Me.Label5)
        Me.pmaster.Controls.Add(Me.Label14)
        Me.pmaster.Controls.Add(Me.cbTipo)
        Me.pmaster.Controls.Add(Me.Label13)
        Me.pmaster.Controls.Add(Me.lbremision2)
        Me.pmaster.Controls.Add(Me.lbRemision)
        Me.pmaster.Controls.Add(Me.Label12)
        Me.pmaster.Controls.Add(Me.cbEstado)
        Me.pmaster.Controls.Add(Me.Label2)
        Me.pmaster.Controls.Add(Me.Label21)
        Me.pmaster.Controls.Add(Me.Label1)
        Me.pmaster.Controls.Add(Me.Label16)
        Me.pmaster.Controls.Add(Me.dtpFecha)
        Me.pmaster.Controls.Add(Me.Label7)
        Me.pmaster.Enabled = False
        Me.pmaster.Location = New System.Drawing.Point(0, 38)
        Me.pmaster.Name = "pmaster"
        Me.pmaster.Size = New System.Drawing.Size(1111, 66)
        Me.pmaster.TabIndex = 123
        '
        'txtVehiculo
        '
        Me.txtVehiculo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtVehiculo.Location = New System.Drawing.Point(977, 31)
        Me.txtVehiculo.MaxLength = 5
        Me.txtVehiculo.Name = "txtVehiculo"
        Me.txtVehiculo.ReadOnly = True
        Me.txtVehiculo.Size = New System.Drawing.Size(122, 20)
        Me.txtVehiculo.TabIndex = 161
        '
        'txtSupervisor
        '
        Me.txtSupervisor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSupervisor.Location = New System.Drawing.Point(527, 31)
        Me.txtSupervisor.MaxLength = 5
        Me.txtSupervisor.Name = "txtSupervisor"
        Me.txtSupervisor.ReadOnly = True
        Me.txtSupervisor.Size = New System.Drawing.Size(214, 20)
        Me.txtSupervisor.TabIndex = 160
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(461, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 13)
        Me.Label9.TabIndex = 159
        Me.Label9.Text = "Supervisor:"
        '
        'txtCoordinador
        '
        Me.txtCoordinador.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCoordinador.Location = New System.Drawing.Point(527, 5)
        Me.txtCoordinador.MaxLength = 5
        Me.txtCoordinador.Name = "txtCoordinador"
        Me.txtCoordinador.ReadOnly = True
        Me.txtCoordinador.Size = New System.Drawing.Size(214, 20)
        Me.txtCoordinador.TabIndex = 158
        '
        'luRuta
        '
        Me.luRuta.Location = New System.Drawing.Point(305, 31)
        Me.luRuta.Name = "luRuta"
        Me.luRuta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luRuta.Size = New System.Drawing.Size(122, 20)
        Me.luRuta.TabIndex = 154
        '
        'txtOdoF
        '
        Me.txtOdoF.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtOdoF.Location = New System.Drawing.Point(827, 31)
        Me.txtOdoF.Name = "txtOdoF"
        Me.txtOdoF.ReadOnly = True
        Me.txtOdoF.Size = New System.Drawing.Size(72, 20)
        Me.txtOdoF.TabIndex = 139
        Me.txtOdoF.TabStop = False
        Me.txtOdoF.Text = "0.0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(771, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 138
        Me.Label8.Text = "Km Final:"
        '
        'txtOdoI
        '
        Me.txtOdoI.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtOdoI.Location = New System.Drawing.Point(827, 5)
        Me.txtOdoI.Name = "txtOdoI"
        Me.txtOdoI.ReadOnly = True
        Me.txtOdoI.Size = New System.Drawing.Size(72, 20)
        Me.txtOdoI.TabIndex = 137
        Me.txtOdoI.TabStop = False
        Me.txtOdoI.Text = "0.0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(766, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 136
        Me.Label5.Text = "Km Inicial:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(268, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(31, 13)
        Me.Label14.TabIndex = 135
        Me.Label14.Text = "Tipo:"
        '
        'cbTipo
        '
        Me.cbTipo.Location = New System.Drawing.Point(305, 5)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.Items.AddRange(New Object() {"DETALLE", "MAYORISTA"})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(122, 20)
        Me.cbTipo.TabIndex = 3
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(169, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(18, 13)
        Me.Label13.TabIndex = 133
        Me.Label13.Text = "←"
        '
        'lbremision2
        '
        Me.lbremision2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbremision2.Location = New System.Drawing.Point(193, 5)
        Me.lbremision2.Name = "lbremision2"
        Me.lbremision2.ReadOnly = True
        Me.lbremision2.Size = New System.Drawing.Size(43, 20)
        Me.lbremision2.TabIndex = 2
        Me.lbremision2.Text = "00000"
        '
        'lbRemision
        '
        Me.lbRemision.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbRemision.Location = New System.Drawing.Point(87, 5)
        Me.lbRemision.MaxLength = 5
        Me.lbRemision.Name = "lbRemision"
        Me.lbRemision.Size = New System.Drawing.Size(72, 20)
        Me.lbRemision.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(28, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 13)
        Me.Label12.TabIndex = 121
        Me.Label12.Text = "Remision:"
        '
        'cbEstado
        '
        Me.cbEstado.Enabled = False
        Me.cbEstado.Location = New System.Drawing.Point(977, 5)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbEstado.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbEstado.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEstado.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbEstado.Properties.Items.AddRange(New Object() {"EMITIDA", "PREFACTURADA", "CERRADA", "ANULADA"})
        Me.cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbEstado.Size = New System.Drawing.Size(122, 20)
        Me.cbEstado.TabIndex = 128
        Me.cbEstado.TabStop = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(266, 34)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(33, 13)
        Me.Label21.TabIndex = 123
        Me.Label21.Text = "Ruta:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(454, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Coordinador:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(928, 8)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(43, 13)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "Estado:"
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "dd/MMMM/yyyy"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFecha.Location = New System.Drawing.Point(87, 31)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(149, 20)
        Me.dtpFecha.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(41, 34)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Fecha:"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(18, 22)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(100, 13)
        Me.Label54.TabIndex = 111
        Me.Label54.Text = "DATOS REMISION"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(17, 107)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(111, 13)
        Me.Label55.TabIndex = 116
        Me.Label55.Text = "DETALLE REMISION"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(18, 263)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(105, 13)
        Me.Label10.TabIndex = 119
        Me.Label10.Text = "PREFACTURACION"
        '
        'pprefacturacionDeco
        '
        Me.pprefacturacionDeco.AutoSize = True
        Me.pprefacturacionDeco.BackColor = System.Drawing.Color.Transparent
        Me.pprefacturacionDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pprefacturacionDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pprefacturacionDeco.Location = New System.Drawing.Point(-3, 231)
        Me.pprefacturacionDeco.Name = "pprefacturacionDeco"
        Me.pprefacturacionDeco.Size = New System.Drawing.Size(1118, 42)
        Me.pprefacturacionDeco.TabIndex = 118
        Me.pprefacturacionDeco.Text = "_______________________________________________________"
        Me.pprefacturacionDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pEntradadeco
        '
        Me.pEntradadeco.AutoSize = True
        Me.pEntradadeco.BackColor = System.Drawing.Color.Transparent
        Me.pEntradadeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pEntradadeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pEntradadeco.Location = New System.Drawing.Point(601, 76)
        Me.pEntradadeco.Margin = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.pEntradadeco.Name = "pEntradadeco"
        Me.pEntradadeco.Size = New System.Drawing.Size(518, 42)
        Me.pEntradadeco.TabIndex = 127
        Me.pEntradadeco.Text = "_________________________"
        Me.pEntradadeco.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pdetalleDeco
        '
        Me.pdetalleDeco.AutoSize = True
        Me.pdetalleDeco.BackColor = System.Drawing.Color.Transparent
        Me.pdetalleDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdetalleDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pdetalleDeco.Location = New System.Drawing.Point(-4, 76)
        Me.pdetalleDeco.Name = "pdetalleDeco"
        Me.pdetalleDeco.Size = New System.Drawing.Size(738, 42)
        Me.pdetalleDeco.TabIndex = 115
        Me.pdetalleDeco.Text = "____________________________________" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.pdetalleDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(-3, -10)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(1258, 42)
        Me.pmasterDeco.TabIndex = 110
        Me.pmasterDeco.Text = "______________________________________________________________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Location = New System.Drawing.Point(270, 10)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(72, 20)
        Me.TextBox1.TabIndex = 49
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Width = 80
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "Cant"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Width = 60
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Width = 200
        '
        'Column4
        '
        Me.Column4.HeaderText = "Pagare"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Column5
        '
        Me.Column5.HeaderText = "Nombre Cliente"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 200
        '
        'Column6
        '
        Me.Column6.HeaderText = "Cod."
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        Me.Column6.Width = 60
        '
        'Column7
        '
        Me.Column7.HeaderText = "Factura"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 60
        '
        'Column8
        '
        Me.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column8.HeaderText = "Nombre Vendedor"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.HeaderText = "Cedula"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column9.Width = 50
        '
        'Column10
        '
        Me.Column10.HeaderText = "Recibo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column10.Width = 50
        '
        'Column11
        '
        Me.Column11.HeaderText = "Const."
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column11.Width = 50
        '
        'Column12
        '
        Me.Column12.HeaderText = "Fiador"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column12.Width = 50
        '
        'Column19
        '
        Me.Column19.HeaderText = "Costo"
        Me.Column19.Name = "Column19"
        Me.Column19.ReadOnly = True
        '
        'Column20
        '
        Me.Column20.HeaderText = "Tipo"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        Me.Column20.Visible = False
        '
        'Column21
        '
        Me.Column21.HeaderText = "Garantia"
        Me.Column21.Name = "Column21"
        Me.Column21.ReadOnly = True
        Me.Column21.Visible = False
        '
        'Column13
        '
        Me.Column13.HeaderText = "Id_Cr"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Visible = False
        '
        'Column14
        '
        Me.Column14.HeaderText = "Id_Vendedor"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Visible = False
        '
        'Column15
        '
        Me.Column15.HeaderText = "Aplicado"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Visible = False
        '
        'FrmRemision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1118, 581)
        Me.Controls.Add(Me.pMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmRemision"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.dgvVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRemision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMain.ResumeLayout(False)
        Me.pMain.PerformLayout()
        Me.pEntrada.ResumeLayout(False)
        CType(Me.dgvEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pPrefacturacion.ResumeLayout(False)
        Me.pPrefacturacion.PerformLayout()
        CType(Me.luVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbvTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        Me.pmaster.ResumeLayout(False)
        Me.pmaster.PerformLayout()
        CType(Me.luRuta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvRemision As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnPrefactura As System.Windows.Forms.Button
    Friend WithEvents dgvVenta As System.Windows.Forms.DataGridView
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtvCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents pdetalleDeco As System.Windows.Forms.Label
    Friend WithEvents btnRmenos As System.Windows.Forms.Button
    Friend WithEvents btnRmas As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pprefacturacionDeco As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents pmaster As System.Windows.Forms.Panel
    Friend WithEvents dgvEntrada As System.Windows.Forms.DataGridView
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents pPrefacturacion As System.Windows.Forms.Panel
    Friend WithEvents cbEstado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ckbCedula As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtvSolicitud As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ckbFiador As System.Windows.Forms.CheckBox
    Friend WithEvents ckbConstancia As System.Windows.Forms.CheckBox
    Friend WithEvents ckbRecibo As System.Windows.Forms.CheckBox
    Friend WithEvents pEntrada As System.Windows.Forms.Panel
    Friend WithEvents pEntradadeco As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TblDepartBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtvGarantia As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cbvTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnVagregar As System.Windows.Forms.Button
    Friend WithEvents txtOdoF As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtOdoI As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lbremision2 As System.Windows.Forms.TextBox
    Friend WithEvents lbRemision As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtVehiculo As System.Windows.Forms.TextBox
    Friend WithEvents txtSupervisor As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCoordinador As System.Windows.Forms.TextBox
    Friend WithEvents luRuta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luCliente As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewCheckBoxColumn
    Friend WithEvents Column10 As DataGridViewCheckBoxColumn
    Friend WithEvents Column11 As DataGridViewCheckBoxColumn
    Friend WithEvents Column12 As DataGridViewCheckBoxColumn
    Friend WithEvents Column19 As DataGridViewTextBoxColumn
    Friend WithEvents Column20 As DataGridViewTextBoxColumn
    Friend WithEvents Column21 As DataGridViewTextBoxColumn
    Friend WithEvents Column13 As DataGridViewTextBoxColumn
    Friend WithEvents Column14 As DataGridViewTextBoxColumn
    Friend WithEvents Column15 As DataGridViewTextBoxColumn
End Class
