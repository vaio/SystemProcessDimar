﻿
Imports System.Data.SqlClient

Public Class clsAddTransaccion

	Implements IDisposable

	Dim mconexion As String
	Dim strSql As String
	Dim isWebService As Boolean = False

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
          If disposing Then
            ' TODO: free other state (managed objects).
          End If

          ' TODO: free your own state (unmanaged objects).
          ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

	Public Sub New(ByVal ConextionString As String)
		mconexion = ConextionString
	End Sub

	Public Function Sql(ByVal strSql As String) As DataSet
		Dim cn As New SqlConnection
		cn.ConnectionString = mconexion
		Try
			cn.Open()
			Dim sqlda As New SqlDataAdapter(strSql, cn)
			Dim ds As New DataSet
			sqlda.Fill(ds, "tblTransacciones")
			Sql = ds
		Catch ex As Exception
			Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
			Sql = Nothing
		Finally
			cn.Close()
		End Try
	End Function

  Public Function AgregarTransaccion(ByVal IdTransaccion As Integer, ByVal TipoTransaccion As Integer, ByVal Fecha As Date,
                                                                          ByVal Concepto As String, ByVal Monto As Double, ByVal Debito As Double, ByVal Credito As Double, ByVal IdProveedor As Integer, ByVal IdCliente As Integer, ByVal nSigno As Integer, ByVal TipoEdicion As Integer)
      Dim ID As String = ""
      Dim cnConec As New SqlConnection

      With cnConec
          .ConnectionString = mconexion
          .Open()
      End With

      Try
          Dim cmdComisiones As New SqlCommand("SP_AME_Transacciones", cnConec)
          cmdComisiones.CommandType = CommandType.StoredProcedure
          cmdComisiones.Parameters.Add("@IdTransaccion", SqlDbType.Int).Value = IdTransaccion
          cmdComisiones.Parameters.Add("@TipoTransaccion", SqlDbType.Int).Value = TipoTransaccion
          cmdComisiones.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha
          cmdComisiones.Parameters.Add("@Concepto", SqlDbType.NVarChar).Value = Concepto
          cmdComisiones.Parameters.Add("@Monto", SqlDbType.Money).Value = Monto
          cmdComisiones.Parameters.Add("@Debito", SqlDbType.Money).Value = Debito
          cmdComisiones.Parameters.Add("@Credito", SqlDbType.Money).Value = Credito
          cmdComisiones.Parameters.Add("@IdProveedor", SqlDbType.Int).Value = IdProveedor
          cmdComisiones.Parameters.Add("@IdCliente", SqlDbType.Int).Value = IdCliente
          cmdComisiones.Parameters.Add("@Signo", SqlDbType.Int).Value = nSigno
          cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
          cmdComisiones.ExecuteReader()

          AgregarTransaccion = "OK"

      Catch ex As SqlException
          MsgBox("Error: " + ex.Message)
          AgregarTransaccion = "ERROR"
      Finally
          cnConec.Close()
          cnConec.Dispose()
      End Try

  End Function

	Public Function AsignarTiempo(ByVal Id_Terminal As Integer, ByVal TiempoProg As Integer, ByVal Total As Double, ByVal TimeIni As DateTime, ByVal nVueltacero As Integer, ByVal isProgram As Integer, ByVal isFree As Integer, ByVal isPend As Integer)
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		Try
			Dim cmdComisiones As New SqlCommand("SP_AsignarTiempo", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@Id_Terminal", SqlDbType.Int).Value = Id_Terminal
			cmdComisiones.Parameters.Add("@TiempoProg", SqlDbType.Int).Value = TiempoProg
			cmdComisiones.Parameters.Add("@Total", SqlDbType.Money).Value = Total
			cmdComisiones.Parameters.Add("@TimeIni", SqlDbType.DateTime).Value = TimeIni
			cmdComisiones.Parameters.Add("@isVueltaCero", SqlDbType.Int).Value = nVueltacero
			cmdComisiones.Parameters.Add("@isProgram", SqlDbType.Int).Value = isProgram
			cmdComisiones.Parameters.Add("@isFree", SqlDbType.Int).Value = isFree
			cmdComisiones.Parameters.Add("@isPend", SqlDbType.Int).Value = isPend
			cmdComisiones.ExecuteReader()

			AsignarTiempo = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			AsignarTiempo = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try

	End Function

  Public Function TimeNacCall(ByVal TimeIni As DateTime)  'Para llamadas nacionales.
      Dim cnConec As New SqlConnection

      With cnConec
          .ConnectionString = mconexion
          .Open()
      End With

      Try
          Dim cmdNacional As New SqlCommand("SP_AsignarTiempoNacCall", cnConec)
          cmdNacional.CommandType = CommandType.StoredProcedure
          cmdNacional.Parameters.Add("@TimeIni", SqlDbType.DateTime).Value = TimeIni
          cmdNacional.ExecuteReader()

          TimeNacCall = "OK"

      Catch ex As SqlException
          MsgBox("Error: " + ex.Message)
          TimeNacCall = "ERROR"
      Finally
          cnConec.Close()
          cnConec.Dispose()
      End Try

  End Function

  Public Function TimeInternacCall(ByVal TimeIni As DateTime)  'Para llamadas Internacionales.
      Dim cnConec As New SqlConnection

      With cnConec
          .ConnectionString = mconexion
          .Open()
      End With

      Try
          Dim cmdInternacional As New SqlCommand("SP_AsignarTiempoInternacCall", cnConec)
          cmdInternacional.CommandType = CommandType.StoredProcedure
          cmdInternacional.Parameters.Add("@TimeIni", SqlDbType.DateTime).Value = TimeIni
          cmdInternacional.ExecuteReader()

          TimeInternacCall = "OK"

      Catch ex As SqlException
          MsgBox("Error: " + ex.Message)
          TimeInternacCall = "ERROR"
      Finally
          cnConec.Close()
          cnConec.Dispose()
      End Try

  End Function

	Public Function EditarRecargas(ByVal IdDetalle As Integer, ByVal IdOperadora As Integer, ByVal TipoOperacion As Integer, ByVal Fecha As Date, ByVal Debito As Double, ByVal Credito As Double, ByVal Pendiente As Integer, ByVal TipoEdicion As Integer, ByVal nCodEntidad As Integer)
		Dim ID As String = ""
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		'En este Procedimiento se agregaran las compras y ventas de recargas para ayudar a mantener un control del saldo.
		Try
			Dim cmdComisiones As New SqlCommand("SP_EditarRecargas", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@IdDetalle", SqlDbType.Int).Value = IdDetalle
			cmdComisiones.Parameters.Add("@IdOperadora", SqlDbType.Int).Value = IdOperadora
			cmdComisiones.Parameters.Add("@TipoOperacion", SqlDbType.Int).Value = TipoOperacion
			cmdComisiones.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
			cmdComisiones.Parameters.Add("@Debito", SqlDbType.Money).Value = Debito
			cmdComisiones.Parameters.Add("@Credito", SqlDbType.Money).Value = Credito
			cmdComisiones.Parameters.Add("@Pendiente", SqlDbType.Int).Value = Pendiente
			cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
			cmdComisiones.Parameters.Add("@CodEntidad", SqlDbType.Int).Value = nCodEntidad
			cmdComisiones.ExecuteReader()

			EditarRecargas = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			EditarRecargas = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try

	End Function

	Public Function CerrarTransacciones(ByVal Fecha As DateTime, ByVal CantTrans As Integer, ByVal ConsumEnerg As Integer, ByVal MontoCierre As Double, ByVal VentCont As Double, ByVal VentCred As Double, ByVal Recup As Double, ByVal CompCont As Double, ByVal CompCred As Double, ByVal PagoDeuda As Double, ByVal GastOper As Double, ByVal SaldoCaja As Double, ByVal Diff As Double)

		Dim ID As String = ""
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		Try
			Dim cmdComisiones As New SqlCommand("SP_AME_CierreTransacciones", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
			cmdComisiones.Parameters.Add("@CantTrans", SqlDbType.Int).Value = CantTrans
			cmdComisiones.Parameters.Add("@ConsumEnerg", SqlDbType.Int).Value = ConsumEnerg
			cmdComisiones.Parameters.Add("@MontoCierre", SqlDbType.Money).Value = MontoCierre
			cmdComisiones.Parameters.Add("@VentCont", SqlDbType.Money).Value = VentCont
			cmdComisiones.Parameters.Add("@VentCred", SqlDbType.Money).Value = VentCred
			cmdComisiones.Parameters.Add("@Recup", SqlDbType.Money).Value = Recup
			cmdComisiones.Parameters.Add("@CompCont", SqlDbType.Money).Value = CompCont
			cmdComisiones.Parameters.Add("@CompCred", SqlDbType.Money).Value = CompCred
			cmdComisiones.Parameters.Add("@PagoDeuda", SqlDbType.Money).Value = PagoDeuda
			cmdComisiones.Parameters.Add("@GastOper", SqlDbType.Money).Value = GastOper
			cmdComisiones.Parameters.Add("@SaldoCaja", SqlDbType.Money).Value = SaldoCaja
			cmdComisiones.Parameters.Add("@Diff", SqlDbType.Money).Value = Diff

			cmdComisiones.ExecuteReader()

			CerrarTransacciones = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			CerrarTransacciones = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try
	End Function

	Public Function AgregarMasterDiario(ByVal IdTransaccion As Integer, ByVal Fecha As Date, ByVal Concepto As String, ByVal Monto As Double, ByVal TipoEdicion As Integer)
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		Try
			Dim cmdComisiones As New SqlCommand("SP_Add_MasterDiario", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@IdTransaccion", SqlDbType.Int).Value = IdTransaccion
			cmdComisiones.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha
			cmdComisiones.Parameters.Add("@Concepto", SqlDbType.NVarChar).Value = Concepto
			cmdComisiones.Parameters.Add("@Monto", SqlDbType.Money).Value = Monto
			cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
			cmdComisiones.ExecuteReader()

			AgregarMasterDiario = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			AgregarMasterDiario = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try

	End Function

	Public Function AgregarDetalleDiario(ByVal IdTransaccion As Integer, ByVal CodCuenta As String, ByVal Debito As Double, ByVal Credito As Double, ByVal TipoEdicion As Integer)
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		Try
			Dim cmdComisiones As New SqlCommand("SP_Add_DetalleDiario", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@IdTransaccion", SqlDbType.Int).Value = IdTransaccion
			cmdComisiones.Parameters.Add("@CodCuenta", SqlDbType.NVarChar).Value = CodCuenta
			cmdComisiones.Parameters.Add("@Debito", SqlDbType.Money).Value = Debito
			cmdComisiones.Parameters.Add("@Credito", SqlDbType.Money).Value = Credito
			cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
			cmdComisiones.ExecuteReader()

			AgregarDetalleDiario = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			AgregarDetalleDiario = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try

	End Function

	Public Function ActualizarCatalogo(ByVal CodCuenta As String, ByVal Monto As Double, ByVal IsDebito As Double)
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		Try
			Dim cmdComisiones As New SqlCommand("SP_ActualizarCatalogo", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@CodCuenta", SqlDbType.NVarChar).Value = CodCuenta
			cmdComisiones.Parameters.Add("@Monto", SqlDbType.Money).Value = Monto
			cmdComisiones.Parameters.Add("@IsDebito", SqlDbType.Int).Value = IsDebito
			cmdComisiones.ExecuteReader()

			ActualizarCatalogo = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			ActualizarCatalogo = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try

	End Function

	Public Function TiempoGuardado(ByVal Nombre As String, ByVal TiempoMin As Integer, ByVal CodeTime As String, ByVal isGuardar As Integer)
		Dim cnConec As New SqlConnection

		With cnConec
			.ConnectionString = mconexion
			.Open()
		End With

		Try
			Dim cmdComisiones As New SqlCommand("SP_TiempoGuardado", cnConec)
			cmdComisiones.CommandType = CommandType.StoredProcedure
			cmdComisiones.Parameters.Add("@Nombre", SqlDbType.NVarChar).Value = Nombre
			cmdComisiones.Parameters.Add("@TiempoMin", SqlDbType.Int).Value = TiempoMin
			cmdComisiones.Parameters.Add("@CodeTime", SqlDbType.NVarChar).Value = CodeTime
			cmdComisiones.Parameters.Add("@Guardar", SqlDbType.Int).Value = isGuardar
			cmdComisiones.ExecuteReader()

			TiempoGuardado = "OK"

		Catch ex As SqlException
			MsgBox("Error: " + ex.Message)
			TiempoGuardado = "ERROR"
		Finally
			cnConec.Close()
			cnConec.Dispose()
		End Try
  End Function

  Public Function TipoShutdown(ByVal IdTerminal As Integer, ByVal TipoAccion As Integer)
    Dim cnConec As New SqlConnection

    With cnConec
      .ConnectionString = mconexion
      .Open()
    End With

    Try
      Dim cmdComisiones As New SqlCommand("SP_ShutdownPC", cnConec)
      cmdComisiones.CommandType = CommandType.StoredProcedure
      cmdComisiones.Parameters.Add("@Id_Terminal", SqlDbType.Int).Value = IdTerminal
      cmdComisiones.Parameters.Add("@TipoAccion", SqlDbType.Int).Value = TipoAccion
      cmdComisiones.ExecuteReader()

      TipoShutdown = "OK"

    Catch ex As SqlException
      MsgBox("Error: " + ex.Message)
      TipoShutdown = "ERROR"
    Finally
      cnConec.Close()
      cnConec.Dispose()
    End Try
  End Function

End Class
