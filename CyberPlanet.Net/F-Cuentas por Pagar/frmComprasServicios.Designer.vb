﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComprasServicios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComprasServicios))
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.ckIsIMI = New DevExpress.XtraEditors.CheckEdit()
    Me.ckIsIR = New DevExpress.XtraEditors.CheckEdit()
    Me.ckIsIVA = New DevExpress.XtraEditors.CheckEdit()
    Me.txtNomProv = New System.Windows.Forms.TextBox()
    Me.deFechaBaja = New DevExpress.XtraEditors.DateEdit()
    Me.deFechaIng = New DevExpress.XtraEditors.DateEdit()
    Me.lblFechaBaja = New System.Windows.Forms.Label()
    Me.lblFechaIngreso = New System.Windows.Forms.Label()
    Me.lblNomContacto = New System.Windows.Forms.Label()
    Me.lblNomProv = New System.Windows.Forms.Label()
    Me.lblCodProv = New System.Windows.Forms.Label()
    Me.txtCodProv = New System.Windows.Forms.TextBox()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.Label13 = New System.Windows.Forms.Label()
    Me.TextBox3 = New System.Windows.Forms.TextBox()
    Me.TextBox4 = New System.Windows.Forms.TextBox()
    Me.TextBox5 = New System.Windows.Forms.TextBox()
    Me.TextBox6 = New System.Windows.Forms.TextBox()
    Me.Panel2.SuspendLayout()
    CType(Me.ckIsIMI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.ckIsIR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.ckIsIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.TextBox6)
    Me.Panel2.Controls.Add(Me.TextBox5)
    Me.Panel2.Controls.Add(Me.TextBox4)
    Me.Panel2.Controls.Add(Me.TextBox3)
    Me.Panel2.Controls.Add(Me.TextBox2)
    Me.Panel2.Controls.Add(Me.Label13)
    Me.Panel2.Controls.Add(Me.TextBox1)
    Me.Panel2.Controls.Add(Me.Label3)
    Me.Panel2.Controls.Add(Me.txtCodProv)
    Me.Panel2.Controls.Add(Me.ckIsIMI)
    Me.Panel2.Controls.Add(Me.ckIsIR)
    Me.Panel2.Controls.Add(Me.ckIsIVA)
    Me.Panel2.Controls.Add(Me.txtNomProv)
    Me.Panel2.Controls.Add(Me.deFechaBaja)
    Me.Panel2.Controls.Add(Me.deFechaIng)
    Me.Panel2.Controls.Add(Me.lblFechaBaja)
    Me.Panel2.Controls.Add(Me.lblFechaIngreso)
    Me.Panel2.Controls.Add(Me.lblNomContacto)
    Me.Panel2.Controls.Add(Me.lblNomProv)
    Me.Panel2.Controls.Add(Me.lblCodProv)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel2.Location = New System.Drawing.Point(0, 0)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(760, 381)
    Me.Panel2.TabIndex = 2
    '
    'ckIsIMI
    '
    Me.ckIsIMI.Location = New System.Drawing.Point(604, 62)
    Me.ckIsIMI.Name = "ckIsIMI"
    Me.ckIsIMI.Properties.Caption = "Es IMI"
    Me.ckIsIMI.Size = New System.Drawing.Size(75, 18)
    Me.ckIsIMI.TabIndex = 20
    '
    'ckIsIR
    '
    Me.ckIsIR.Location = New System.Drawing.Point(604, 38)
    Me.ckIsIR.Name = "ckIsIR"
    Me.ckIsIR.Properties.Caption = "Es IR"
    Me.ckIsIR.Size = New System.Drawing.Size(75, 18)
    Me.ckIsIR.TabIndex = 19
    '
    'ckIsIVA
    '
    Me.ckIsIVA.Location = New System.Drawing.Point(604, 86)
    Me.ckIsIVA.Name = "ckIsIVA"
    Me.ckIsIVA.Properties.Caption = "Es IVA"
    Me.ckIsIVA.Size = New System.Drawing.Size(75, 18)
    Me.ckIsIVA.TabIndex = 21
    '
    'txtNomProv
    '
    Me.txtNomProv.Location = New System.Drawing.Point(312, 38)
    Me.txtNomProv.Name = "txtNomProv"
    Me.txtNomProv.Size = New System.Drawing.Size(280, 20)
    Me.txtNomProv.TabIndex = 2
    '
    'deFechaBaja
    '
    Me.deFechaBaja.EditValue = Nothing
    Me.deFechaBaja.Location = New System.Drawing.Point(482, 12)
    Me.deFechaBaja.Name = "deFechaBaja"
    Me.deFechaBaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.deFechaBaja.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.deFechaBaja.Size = New System.Drawing.Size(110, 20)
    Me.deFechaBaja.TabIndex = 10
    '
    'deFechaIng
    '
    Me.deFechaIng.EditValue = Nothing
    Me.deFechaIng.Location = New System.Drawing.Point(277, 12)
    Me.deFechaIng.Name = "deFechaIng"
    Me.deFechaIng.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.deFechaIng.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
    Me.deFechaIng.Size = New System.Drawing.Size(110, 20)
    Me.deFechaIng.TabIndex = 9
    '
    'lblFechaBaja
    '
    Me.lblFechaBaja.Location = New System.Drawing.Point(412, 15)
    Me.lblFechaBaja.Name = "lblFechaBaja"
    Me.lblFechaBaja.Size = New System.Drawing.Size(76, 17)
    Me.lblFechaBaja.TabIndex = 30
    Me.lblFechaBaja.Text = "Fecha Venc.:"
    '
    'lblFechaIngreso
    '
    Me.lblFechaIngreso.Location = New System.Drawing.Point(210, 15)
    Me.lblFechaIngreso.Name = "lblFechaIngreso"
    Me.lblFechaIngreso.Size = New System.Drawing.Size(70, 17)
    Me.lblFechaIngreso.TabIndex = 29
    Me.lblFechaIngreso.Text = "Fecha Doc.:"
    '
    'lblNomContacto
    '
    Me.lblNomContacto.Location = New System.Drawing.Point(21, 71)
    Me.lblNomContacto.Name = "lblNomContacto"
    Me.lblNomContacto.Size = New System.Drawing.Size(117, 20)
    Me.lblNomContacto.TabIndex = 5
    Me.lblNomContacto.Text = "Observaciones:"
    '
    'lblNomProv
    '
    Me.lblNomProv.Location = New System.Drawing.Point(200, 41)
    Me.lblNomProv.Name = "lblNomProv"
    Me.lblNomProv.Size = New System.Drawing.Size(117, 20)
    Me.lblNomProv.TabIndex = 3
    Me.lblNomProv.Text = "Nombre de Proveedor:"
    '
    'lblCodProv
    '
    Me.lblCodProv.Location = New System.Drawing.Point(21, 41)
    Me.lblCodProv.Name = "lblCodProv"
    Me.lblCodProv.Size = New System.Drawing.Size(54, 20)
    Me.lblCodProv.TabIndex = 1
    Me.lblCodProv.Text = "Codigo:"
    '
    'txtCodProv
    '
    Me.txtCodProv.Location = New System.Drawing.Point(111, 38)
    Me.txtCodProv.Name = "txtCodProv"
    Me.txtCodProv.Size = New System.Drawing.Size(83, 20)
    Me.txtCodProv.TabIndex = 0
    Me.txtCodProv.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 381)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(760, 49)
    Me.Panel1.TabIndex = 3
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.TopLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(660, 6)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.TopLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(576, 6)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(111, 12)
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Size = New System.Drawing.Size(83, 20)
    Me.TextBox1.TabIndex = 59
    Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(21, 15)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(93, 20)
    Me.Label3.TabIndex = 60
    Me.Label3.Text = "Num Documento:"
    '
    'TextBox2
    '
    Me.TextBox2.Location = New System.Drawing.Point(658, 12)
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.Size = New System.Drawing.Size(83, 20)
    Me.TextBox2.TabIndex = 61
    '
    'Label13
    '
    Me.Label13.Location = New System.Drawing.Point(603, 15)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(42, 20)
    Me.Label13.TabIndex = 62
    Me.Label13.Text = "Monto:"
    '
    'TextBox3
    '
    Me.TextBox3.Location = New System.Drawing.Point(658, 38)
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.Size = New System.Drawing.Size(83, 20)
    Me.TextBox3.TabIndex = 63
    '
    'TextBox4
    '
    Me.TextBox4.Location = New System.Drawing.Point(658, 62)
    Me.TextBox4.Name = "TextBox4"
    Me.TextBox4.Size = New System.Drawing.Size(83, 20)
    Me.TextBox4.TabIndex = 64
    '
    'TextBox5
    '
    Me.TextBox5.Location = New System.Drawing.Point(658, 86)
    Me.TextBox5.Name = "TextBox5"
    Me.TextBox5.Size = New System.Drawing.Size(83, 20)
    Me.TextBox5.TabIndex = 65
    '
    'TextBox6
    '
    Me.TextBox6.Location = New System.Drawing.Point(111, 68)
    Me.TextBox6.Multiline = True
    Me.TextBox6.Name = "TextBox6"
    Me.TextBox6.Size = New System.Drawing.Size(481, 38)
    Me.TextBox6.TabIndex = 66
    '
    'frmComprasServicios
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(760, 430)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Panel1)
    Me.Name = "frmComprasServicios"
    Me.Text = "frmComprasServicios"
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.ckIsIMI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.ckIsIR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.ckIsIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ckIsIMI As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckIsIR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckIsIVA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtNomProv As System.Windows.Forms.TextBox
    Friend WithEvents deFechaBaja As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFechaIng As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaBaja As System.Windows.Forms.Label
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents lblNomContacto As System.Windows.Forms.Label
    Friend WithEvents lblNomProv As System.Windows.Forms.Label
    Friend WithEvents lblCodProv As System.Windows.Forms.Label
    Friend WithEvents txtCodProv As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
