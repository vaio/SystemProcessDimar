﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompraRecargas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompraRecargas))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.OptInternacional = New System.Windows.Forms.RadioButton()
		Me.lblInter = New System.Windows.Forms.Label()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.ChkTransferencia = New System.Windows.Forms.CheckBox()
		Me.LblTransferencia = New System.Windows.Forms.Label()
		Me.OptTUC = New System.Windows.Forms.RadioButton()
		Me.OptClaro = New System.Windows.Forms.RadioButton()
		Me.OptMovistar = New System.Windows.Forms.RadioButton()
		Me.txtMonto = New System.Windows.Forms.TextBox()
		Me.lblSaldoRecargas = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.lblTUC = New System.Windows.Forms.Label()
		Me.lblClaro = New System.Windows.Forms.Label()
		Me.lblMovistar = New System.Windows.Forms.Label()
		Me.lblDescripcion = New System.Windows.Forms.Label()
		Me.lblCodDepart = New System.Windows.Forms.Label()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.chkFormaPago = New System.Windows.Forms.CheckBox()
		Me.lblFormaPago = New System.Windows.Forms.Label()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptarCont = New System.Windows.Forms.Button()
		Me.Panel2.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.OptInternacional)
		Me.Panel2.Controls.Add(Me.lblInter)
		Me.Panel2.Controls.Add(Me.GroupBox1)
		Me.Panel2.Controls.Add(Me.OptTUC)
		Me.Panel2.Controls.Add(Me.OptClaro)
		Me.Panel2.Controls.Add(Me.OptMovistar)
		Me.Panel2.Controls.Add(Me.txtMonto)
		Me.Panel2.Controls.Add(Me.lblSaldoRecargas)
		Me.Panel2.Controls.Add(Me.Label2)
		Me.Panel2.Controls.Add(Me.lblTUC)
		Me.Panel2.Controls.Add(Me.lblClaro)
		Me.Panel2.Controls.Add(Me.lblMovistar)
		Me.Panel2.Controls.Add(Me.lblDescripcion)
		Me.Panel2.Controls.Add(Me.lblCodDepart)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(515, 222)
		Me.Panel2.TabIndex = 0
		'
		'OptInternacional
		'
		Me.OptInternacional.Appearance = System.Windows.Forms.Appearance.Button
		Me.OptInternacional.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.OptInternacional.Location = New System.Drawing.Point(392, 51)
		Me.OptInternacional.Name = "OptInternacional"
		Me.OptInternacional.Size = New System.Drawing.Size(107, 73)
		Me.OptInternacional.TabIndex = 11
		Me.OptInternacional.Text = "Llamadas Internacionales"
		Me.OptInternacional.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.OptInternacional.UseVisualStyleBackColor = True
		'
		'lblInter
		'
		Me.lblInter.BackColor = System.Drawing.Color.Gainsboro
		Me.lblInter.Location = New System.Drawing.Point(387, 46)
		Me.lblInter.Name = "lblInter"
		Me.lblInter.Size = New System.Drawing.Size(117, 81)
		Me.lblInter.TabIndex = 12
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.ChkTransferencia)
		Me.GroupBox1.Controls.Add(Me.LblTransferencia)
		Me.GroupBox1.Location = New System.Drawing.Point(392, 134)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(109, 80)
		Me.GroupBox1.TabIndex = 10
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Transferencia a:"
		'
		'ChkTransferencia
		'
		Me.ChkTransferencia.Appearance = System.Windows.Forms.Appearance.Button
		Me.ChkTransferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.ChkTransferencia.Location = New System.Drawing.Point(13, 22)
		Me.ChkTransferencia.Name = "ChkTransferencia"
		Me.ChkTransferencia.Size = New System.Drawing.Size(82, 46)
		Me.ChkTransferencia.TabIndex = 10
		Me.ChkTransferencia.Text = "Llamadas Nacionales"
		Me.ChkTransferencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.ChkTransferencia.UseVisualStyleBackColor = True
		'
		'LblTransferencia
		'
		Me.LblTransferencia.BackColor = System.Drawing.Color.Gainsboro
		Me.LblTransferencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.LblTransferencia.Location = New System.Drawing.Point(6, 18)
		Me.LblTransferencia.Name = "LblTransferencia"
		Me.LblTransferencia.Size = New System.Drawing.Size(97, 53)
		Me.LblTransferencia.TabIndex = 11
		Me.LblTransferencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'OptTUC
		'
		Me.OptTUC.Appearance = System.Windows.Forms.Appearance.Button
		Me.OptTUC.Image = CType(resources.GetObject("OptTUC.Image"), System.Drawing.Image)
		Me.OptTUC.Location = New System.Drawing.Point(268, 51)
		Me.OptTUC.Name = "OptTUC"
		Me.OptTUC.Size = New System.Drawing.Size(107, 73)
		Me.OptTUC.TabIndex = 3
		Me.OptTUC.UseVisualStyleBackColor = True
		'
		'OptClaro
		'
		Me.OptClaro.Appearance = System.Windows.Forms.Appearance.Button
		Me.OptClaro.Image = CType(resources.GetObject("OptClaro.Image"), System.Drawing.Image)
		Me.OptClaro.Location = New System.Drawing.Point(144, 51)
		Me.OptClaro.Name = "OptClaro"
		Me.OptClaro.Size = New System.Drawing.Size(107, 73)
		Me.OptClaro.TabIndex = 2
		Me.OptClaro.UseVisualStyleBackColor = True
		'
		'OptMovistar
		'
		Me.OptMovistar.Appearance = System.Windows.Forms.Appearance.Button
		Me.OptMovistar.Checked = True
		Me.OptMovistar.Image = CType(resources.GetObject("OptMovistar.Image"), System.Drawing.Image)
		Me.OptMovistar.Location = New System.Drawing.Point(20, 51)
		Me.OptMovistar.Name = "OptMovistar"
		Me.OptMovistar.Size = New System.Drawing.Size(107, 73)
		Me.OptMovistar.TabIndex = 1
		Me.OptMovistar.TabStop = True
		Me.OptMovistar.UseVisualStyleBackColor = True
		'
		'txtMonto
		'
		Me.txtMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtMonto.HideSelection = False
		Me.txtMonto.Location = New System.Drawing.Point(234, 177)
		Me.txtMonto.Name = "txtMonto"
		Me.txtMonto.Size = New System.Drawing.Size(100, 31)
		Me.txtMonto.TabIndex = 5
		Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'lblSaldoRecargas
		'
		Me.lblSaldoRecargas.BackColor = System.Drawing.Color.Gainsboro
		Me.lblSaldoRecargas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblSaldoRecargas.Location = New System.Drawing.Point(234, 140)
		Me.lblSaldoRecargas.Name = "lblSaldoRecargas"
		Me.lblSaldoRecargas.Size = New System.Drawing.Size(100, 25)
		Me.lblSaldoRecargas.TabIndex = 4
		Me.lblSaldoRecargas.Text = "0.00"
		Me.lblSaldoRecargas.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label2
		'
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(52, 140)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(139, 25)
		Me.Label2.TabIndex = 9
		Me.Label2.Text = "Saldo Actual:"
		'
		'lblTUC
		'
		Me.lblTUC.BackColor = System.Drawing.Color.Gainsboro
		Me.lblTUC.Location = New System.Drawing.Point(263, 46)
		Me.lblTUC.Name = "lblTUC"
		Me.lblTUC.Size = New System.Drawing.Size(117, 81)
		Me.lblTUC.TabIndex = 8
		'
		'lblClaro
		'
		Me.lblClaro.BackColor = System.Drawing.Color.Gainsboro
		Me.lblClaro.Location = New System.Drawing.Point(138, 46)
		Me.lblClaro.Name = "lblClaro"
		Me.lblClaro.Size = New System.Drawing.Size(117, 81)
		Me.lblClaro.TabIndex = 6
		'
		'lblMovistar
		'
		Me.lblMovistar.BackColor = System.Drawing.Color.LightSkyBlue
		Me.lblMovistar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.lblMovistar.Location = New System.Drawing.Point(15, 46)
		Me.lblMovistar.Name = "lblMovistar"
		Me.lblMovistar.Size = New System.Drawing.Size(117, 81)
		Me.lblMovistar.TabIndex = 5
		'
		'lblDescripcion
		'
		Me.lblDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblDescripcion.ForeColor = System.Drawing.Color.Red
		Me.lblDescripcion.Location = New System.Drawing.Point(12, 13)
		Me.lblDescripcion.Name = "lblDescripcion"
		Me.lblDescripcion.Size = New System.Drawing.Size(489, 22)
		Me.lblDescripcion.TabIndex = 0
		Me.lblDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'lblCodDepart
		'
		Me.lblCodDepart.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblCodDepart.Location = New System.Drawing.Point(52, 180)
		Me.lblCodDepart.Name = "lblCodDepart"
		Me.lblCodDepart.Size = New System.Drawing.Size(107, 25)
		Me.lblCodDepart.TabIndex = 0
		Me.lblCodDepart.Text = "Monto:"
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.chkFormaPago)
		Me.Panel1.Controls.Add(Me.lblFormaPago)
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptarCont)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 222)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(515, 49)
		Me.Panel1.TabIndex = 1
		'
		'chkFormaPago
		'
		Me.chkFormaPago.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkFormaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chkFormaPago.Location = New System.Drawing.Point(12, 9)
		Me.chkFormaPago.Name = "chkFormaPago"
		Me.chkFormaPago.Size = New System.Drawing.Size(71, 31)
		Me.chkFormaPago.TabIndex = 0
		Me.chkFormaPago.Text = "Credito"
		Me.chkFormaPago.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.chkFormaPago.UseVisualStyleBackColor = True
		'
		'lblFormaPago
		'
		Me.lblFormaPago.BackColor = System.Drawing.Color.Gainsboro
		Me.lblFormaPago.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.lblFormaPago.Location = New System.Drawing.Point(10, 6)
		Me.lblFormaPago.Name = "lblFormaPago"
		Me.lblFormaPago.Size = New System.Drawing.Size(75, 38)
		Me.lblFormaPago.TabIndex = 9
		Me.lblFormaPago.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(423, 7)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 2
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptarCont
		'
		Me.cmdAceptarCont.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptarCont.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdAceptarCont.Image = CType(resources.GetObject("cmdAceptarCont.Image"), System.Drawing.Image)
		Me.cmdAceptarCont.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptarCont.Location = New System.Drawing.Point(339, 7)
		Me.cmdAceptarCont.Name = "cmdAceptarCont"
		Me.cmdAceptarCont.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptarCont.TabIndex = 1
		Me.cmdAceptarCont.Text = "&Aceptar"
		Me.cmdAceptarCont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptarCont.UseVisualStyleBackColor = True
		'
		'frmCompraRecargas
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(515, 271)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmCompraRecargas"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Compra de Recargas"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents OptTUC As System.Windows.Forms.RadioButton
	Friend WithEvents OptClaro As System.Windows.Forms.RadioButton
	Friend WithEvents OptMovistar As System.Windows.Forms.RadioButton
	Friend WithEvents txtMonto As System.Windows.Forms.TextBox
	Friend WithEvents lblSaldoRecargas As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents lblTUC As System.Windows.Forms.Label
	Friend WithEvents lblClaro As System.Windows.Forms.Label
	Friend WithEvents lblMovistar As System.Windows.Forms.Label
	Friend WithEvents lblDescripcion As System.Windows.Forms.Label
	Friend WithEvents lblCodDepart As System.Windows.Forms.Label
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cmdCancelar As System.Windows.Forms.Button
	Friend WithEvents cmdAceptarCont As System.Windows.Forms.Button
	Friend WithEvents chkFormaPago As System.Windows.Forms.CheckBox
	Friend WithEvents lblFormaPago As System.Windows.Forms.Label
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
 Friend WithEvents ChkTransferencia As System.Windows.Forms.CheckBox
 Friend WithEvents LblTransferencia As System.Windows.Forms.Label
 Friend WithEvents OptInternacional As System.Windows.Forms.RadioButton
 Friend WithEvents lblInter As System.Windows.Forms.Label
End Class
