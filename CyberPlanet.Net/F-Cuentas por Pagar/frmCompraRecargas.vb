﻿Public Class frmCompraRecargas
    Dim nOperadora As Integer
	Dim nNextDiario As Integer

    Private Sub frmCompraRecargas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtMonto.Text = Format(0, "###,###,###.00")
		ObtenerSaldoAtual()
	End Sub

    Sub ObtenerSaldoAtual()
        Dim strSaldo As String, mSaldo As Double = 0
				Dim drReg As DataTable

				If OptMovistar.Checked = True Then
					nOperadora = 1
				ElseIf OptClaro.Checked = True Then
					nOperadora = 2
				ElseIf OptTUC.Checked = True Then
					nOperadora = 3
				End If

				strSaldo = "SELECT isnull(SUM(Debito),0) AS Debito, isnull(SUM(Credito),0) AS Credito, isnull(SUM(Debito)-SUM(Credito),0) as Saldo FROM Tbl_DetalleOperadoraRecargas where (Id_Operadora = " & nOperadora & ")"
        drReg = SQL(strSaldo, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)

        If drReg.Rows.Count > 0 Then
            lblSaldoRecargas.Text = Format(drReg.Rows(0).Item(2), "###,###,###.00")
        Else
            lblSaldoRecargas.Text = Format(0, "###,###,###.00")
        End If
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub cmdAceptarCont_Click(sender As Object, e As EventArgs) Handles cmdAceptarCont.Click
        Dim nMonto As Double

        If txtMonto.Text = ".00" Or txtMonto.Text = "" Then
            MsgBox("Debe ingresar un monto de compra de recarga valido.", MsgBoxStyle.Critical, "CyberPlanet.Net")
            Exit Sub
        End If

        Dim StrMasterDiario As String = ""
        Dim strPadres As String
        Dim dtPadre As DataTable
        Dim sCodPadre As String
        Dim bIsHijo As Boolean = True
        Dim sNumCuenta As String = ""

        'If CDbl(txtMonto.Text) < CDbl(lblSaldoRecargas.Text) Then
        Dim StrTrans As String = ""
        Dim StrCompraRecarga As String = ""
        Dim Transacciones As New clsAddTransaccion(My.Settings.SolIndustrialCNX)
        Dim Recargas As New clsAddTransaccion(My.Settings.SolIndustrialCNX)
        Dim sConcept As String = lblDescripcion.Text
        Dim IsPendiente As Integer

        Try
            If chkFormaPago.Checked = True Then
                StrTrans = Transacciones.AgregarTransaccion(0, 5, Now, lblDescripcion.Text, CDbl(txtMonto.Text), CDbl(0), CDbl(txtMonto.Text), 1, 1, -1, 1)
            Else
                StrTrans = Transacciones.AgregarTransaccion(0, 4, Now, lblDescripcion.Text, CDbl(txtMonto.Text), CDbl(0), CDbl(txtMonto.Text), 1, 1, -1, 1)
            End If

            If StrTrans = "OK" Then
                If OptMovistar.Checked = True Then
                    nOperadora = 1
                ElseIf OptClaro.Checked = True Then
                    nOperadora = 2
                ElseIf OptTUC.Checked = True Then
                    nOperadora = 3
                End If

                If chkFormaPago.Checked Then
                    IsPendiente = 1
                End If

                StrCompraRecarga = Recargas.EditarRecargas(0, nOperadora, 1, Now, CDbl(txtMonto.Text), CDbl(0), IIf(IsPendiente = 1, 1, 0), 1, nCodProveedor)

                If StrTrans = "OK" Then
                    'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
                    'frmTransacciones.CargarTransacciones()
                    nNextDiario = SiguienteTransaccionDiario()
                Else
                    MsgBox("Ha ocurrido un error al actualizar el monto de recarga, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            '*********************************CONTABILIZAR ESTA TRANSACCION*************************************************

            'Debito
            nMonto = CDbl(txtMonto.Text)
            If OptInternacional.Checked = True Then
                sNumCuenta = 713            'GASTO POR LLAMADAS INTERNACIONALES PARA VENTAS
            ElseIf OptInternacional.Checked = False Then
                If ChkTransferencia.Checked = True Then
                    sNumCuenta = 714        'GASTO POR LLAMADAS NACIONALES PARA VENTAS
                Else
                    sNumCuenta = 10302      'INVENTARIO DE RECARGAS
                    nMonto = (CDbl(txtMonto.Text) - (CDbl(txtMonto.Text) * 0.04))
                End If
            End If

            'Master Comprobante
            StrMasterDiario = Transacciones.AgregarMasterDiario(nNextDiario, Now, lblDescripcion.Text, nMonto, 1)    'nTipoEdic
            If StrMasterDiario <> "OK" Then
                MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            bIsHijo = True
            StrMasterDiario = Transacciones.AgregarDetalleDiario(nNextDiario, sNumCuenta, nMonto, 0, 1)         'nTipoEdic
            If StrMasterDiario <> "OK" Then
                MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            '**************APLICAR UN DEBITO A PADRES E HIJOS
            Do While bIsHijo = True
                strPadres = "SELECT Cuenta_Padre FROM Tbl_CatalogoCuentas where (Codigo_Cuenta = '" & sNumCuenta & "')"
                dtPadre = SQL(strPadres, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)
                sCodPadre = dtPadre.Rows(0).Item(0)
                If sCodPadre <> "0" Then
                    StrMasterDiario = Transacciones.ActualizarCatalogo(sCodPadre, nMonto, 1)
                    If StrMasterDiario <> "OK" Then
                        MsgBox("Ha ocurrido un error al actualizar la cuenta en el catalogo.", MsgBoxStyle.Information, "SIGCA")
                    Else
                        bIsHijo = True
                        sNumCuenta = sCodPadre
                    End If
                Else
                    bIsHijo = False
                End If
            Loop
            '************

            'Credito
            sNumCuenta = 10102 'Caja
            StrMasterDiario = Transacciones.AgregarDetalleDiario(nNextDiario, sNumCuenta, 0, nMonto, 1)         'nTipoEdic
            If StrMasterDiario <> "OK" Then
                MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            '**************APLICAR UN CREDITO A PADRES E HIJOS
            Do While bIsHijo = True
                strPadres = "SELECT Cuenta_Padre FROM Tbl_CatalogoCuentas where (Codigo_Cuenta = '" & sNumCuenta & "')"
                dtPadre = SQL(strPadres, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)
                sCodPadre = dtPadre.Rows(0).Item(0)
                If sCodPadre <> "0" Then
                    StrMasterDiario = Transacciones.ActualizarCatalogo(sCodPadre, nMonto, 0)             'nTipoEdic
                    If StrMasterDiario <> "OK" Then
                        MsgBox("Ha ocurrido un error al actualizar la cuenta en el catalogo.", MsgBoxStyle.Information, "SIGCA")
                    Else
                        bIsHijo = True
                        sNumCuenta = sCodPadre
                    End If
                Else
                    bIsHijo = False
                End If
            Loop
            '************

            '*************************************FIN DE CONTABILIZAR TRANSACCION

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
        End Try
        'Else
        '		MsgBox("El saldo es insuficiente para realizar la transacción de forma satisfactoria.", MsgBoxStyle.Critical, "SIGCA")
        'End If
    End Sub

    Private Sub chkFormaPago_CheckedChanged(sender As Object, e As EventArgs) Handles chkFormaPago.CheckedChanged
				CambioDescripcion()
				If chkFormaPago.Checked = True Then
						lblFormaPago.BackColor = Color.LightSkyBlue
						chkFormaPago.Text = "Contado"
						nIsCredito = 1
						frmSelProveedor.ShowDialog()
				Else
						lblFormaPago.BackColor = Color.Gainsboro
						chkFormaPago.Text = "Credito"
						nIsCredito = 0
				End If
		End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Sub CambioDescripcion()
        If OptMovistar.Checked = True Then
			lblDescripcion.Text = IIf(ChkTransferencia.Checked = True, "Transferencia de Saldo de recarga Movistar para Llamadas Nacionales", "Compra " & IIf(chkFormaPago.Checked = True, "de credito", "de contado") & " de recarga Movistar") & " por un monto de " & Format(CInt(IIf(txtMonto.Text = "", 0, txtMonto.Text)), "###,###,###.00")
        ElseIf OptClaro.Checked = True Then
			lblDescripcion.Text = IIf(ChkTransferencia.Checked = True, "Transferencia de Saldo de recarga Claro para Llamadas Nacionales", "Compra " & IIf(chkFormaPago.Checked = True, "de credito", "de contado") & " de recarga Claro") & " por un monto de " & Format(CInt(IIf(txtMonto.Text = "", 0, txtMonto.Text)), "###,###,###.00")
        ElseIf OptTUC.Checked = True Then
			lblDescripcion.Text = "Compra " & IIf(chkFormaPago.Checked = True, "de credito", "de contado") & " de recarga TUC por un monto de " & Format(CInt(IIf(txtMonto.Text = "", 0, txtMonto.Text)), "###,###,###.00")
		ElseIf OptInternacional.Checked = True Then
			lblDescripcion.Text = "Compra " & IIf(chkFormaPago.Checked = True, "de credito", "de contado") & " de recarga para Llamadas Internacionales por un monto de " & Format(CInt(IIf(txtMonto.Text = "", 0, txtMonto.Text)), "###,###,###.00")
		End If
    End Sub

    Private Sub txtMonto_TextChanged(sender As Object, e As EventArgs) Handles txtMonto.TextChanged
        CambioDescripcion()
    End Sub

    Private Sub txtMonto_Leave(sender As Object, e As EventArgs) Handles txtMonto.Leave
        txtMonto.Text = Format(CInt(IIf(txtMonto.Text = "", 0, txtMonto.Text)), "###,###,###.00")
    End Sub

		Private Sub OptMovistar_KeyUp(sender As Object, e As KeyEventArgs) Handles OptMovistar.KeyUp
			ObtenerSaldoAtual()
		End Sub

		Private Sub OptClaro_KeyUp(sender As Object, e As KeyEventArgs) Handles OptClaro.KeyUp
			ObtenerSaldoAtual()
		End Sub

		Private Sub OptTUC_KeyUp(sender As Object, e As KeyEventArgs) Handles OptTUC.KeyUp
			ObtenerSaldoAtual()
		End Sub

		Private Sub OptMovistar_CheckedChanged(sender As Object, e As EventArgs) Handles OptMovistar.CheckedChanged
				lblMovistar.BackColor = Color.LightSkyBlue
				lblClaro.BackColor = Color.Gainsboro
				lblTUC.BackColor = Color.Gainsboro
				lblInter.BackColor = Color.Gainsboro
				CambioDescripcion()
				ObtenerSaldoAtual()
		End Sub

		Private Sub OptClaro_CheckedChanged(sender As Object, e As EventArgs) Handles OptClaro.CheckedChanged
				lblMovistar.BackColor = Color.Gainsboro
				lblClaro.BackColor = Color.LightSkyBlue
				lblTUC.BackColor = Color.Gainsboro
				lblInter.BackColor = Color.Gainsboro
				CambioDescripcion()
				ObtenerSaldoAtual()
		End Sub

		Private Sub OptTUC_CheckedChanged(sender As Object, e As EventArgs) Handles OptTUC.CheckedChanged
				lblMovistar.BackColor = Color.Gainsboro
				lblClaro.BackColor = Color.Gainsboro
				lblTUC.BackColor = Color.LightSkyBlue
				lblInter.BackColor = Color.Gainsboro
				CambioDescripcion()
				ObtenerSaldoAtual()
		End Sub

		Private Sub OptInternacional_CheckedChanged(sender As Object, e As EventArgs) Handles OptInternacional.CheckedChanged
				lblMovistar.BackColor = Color.Gainsboro
				lblClaro.BackColor = Color.Gainsboro
				lblTUC.BackColor = Color.Gainsboro
				lblInter.BackColor = Color.LightSkyBlue
				CambioDescripcion()
		End Sub

		Private Sub ChkLlamadasInter_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTransferencia.CheckedChanged
				CambioDescripcion()
				chkFormaPago.Checked = False
				chkFormaPago.Text = "Credito"
				nIsCredito = 0
				If ChkTransferencia.Checked = True Then
						LblTransferencia.BackColor = Color.LightSkyBlue
						chkFormaPago.Enabled = False
						OptClaro.Checked = True
						OptTUC.Enabled = False
						OptInternacional.Enabled = False
				Else
						LblTransferencia.BackColor = Color.Gainsboro
						chkFormaPago.Enabled = True
						OptClaro.Checked = True
						OptTUC.Enabled = True
						OptInternacional.Enabled = True
				End If
		End Sub

Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
		If InStr("0123456789" & Chr(8), e.KeyChar) Then
			e.Handled = False
		Else
			e.Handled = True
		End If
End Sub
End Class