﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompraActivoFijo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompraActivoFijo))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.txtObservaciones = New System.Windows.Forms.TextBox()
		Me.LblObservaciones = New System.Windows.Forms.Label()
		Me.cmbActivoFijo = New DevExpress.XtraEditors.ComboBoxEdit()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.lblCodDepart = New System.Windows.Forms.Label()
		Me.txtMonto = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.Panel2.SuspendLayout()
		CType(Me.cmbActivoFijo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.txtObservaciones)
		Me.Panel2.Controls.Add(Me.LblObservaciones)
		Me.Panel2.Controls.Add(Me.cmbActivoFijo)
		Me.Panel2.Controls.Add(Me.Label1)
		Me.Panel2.Controls.Add(Me.lblCodDepart)
		Me.Panel2.Controls.Add(Me.txtMonto)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(402, 160)
		Me.Panel2.TabIndex = 4
		'
		'txtObservaciones
		'
		Me.txtObservaciones.Location = New System.Drawing.Point(103, 62)
		Me.txtObservaciones.Name = "txtObservaciones"
		Me.txtObservaciones.Size = New System.Drawing.Size(232, 20)
		Me.txtObservaciones.TabIndex = 2
		'
		'LblObservaciones
		'
		Me.LblObservaciones.AutoSize = True
		Me.LblObservaciones.Location = New System.Drawing.Point(15, 62)
		Me.LblObservaciones.Name = "LblObservaciones"
		Me.LblObservaciones.Size = New System.Drawing.Size(81, 13)
		Me.LblObservaciones.TabIndex = 5
		Me.LblObservaciones.Text = "Observaciones:"
		'
		'cmbActivoFijo
		'
		Me.cmbActivoFijo.Location = New System.Drawing.Point(15, 35)
		Me.cmbActivoFijo.Name = "cmbActivoFijo"
		Me.cmbActivoFijo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.cmbActivoFijo.Size = New System.Drawing.Size(320, 20)
		Me.cmbActivoFijo.TabIndex = 1
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(12, 19)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(134, 13)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Seleccion de Activos Fijos:"
		'
		'lblCodDepart
		'
		Me.lblCodDepart.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblCodDepart.Location = New System.Drawing.Point(70, 96)
		Me.lblCodDepart.Name = "lblCodDepart"
		Me.lblCodDepart.Size = New System.Drawing.Size(107, 38)
		Me.lblCodDepart.TabIndex = 0
		Me.lblCodDepart.Text = "Monto :"
		'
		'txtMonto
		'
		Me.txtMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtMonto.Location = New System.Drawing.Point(171, 96)
		Me.txtMonto.Name = "txtMonto"
		Me.txtMonto.Size = New System.Drawing.Size(100, 38)
		Me.txtMonto.TabIndex = 3
		Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 160)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(402, 49)
		Me.Panel1.TabIndex = 5
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(312, 6)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(228, 6)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'frmCompraActivoFijo
		'
		Me.AcceptButton = Me.cmdAceptar
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.CancelButton = Me.cmdCancelar
		Me.ClientSize = New System.Drawing.Size(402, 209)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmCompraActivoFijo"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "frmCompraActivoFijo"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.cmbActivoFijo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents LblObservaciones As System.Windows.Forms.Label
    Friend WithEvents cmbActivoFijo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCodDepart As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
End Class
