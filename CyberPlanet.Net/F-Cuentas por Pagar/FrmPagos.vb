﻿Public Class FrmPagos

    Public Sub cargarTipoDocumento()
        cmbTipDocumento.DataSource = SQL("SELECT CatDet.IdCategoriaDetalle, CatDet.Estado FROM Categorias Cat INNER JOIN Categorias_Detalle CatDet ON Cat.IdCategoria = CatDet.IdCategoria where CatDet.IdCategoria = 7 and CatDet.Activo = 1 and Cat.Activo = 1", "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)
        cmbTipDocumento.DisplayMember = "Estado"
        cmbTipDocumento.ValueMember = "IdCategoriaDetalle"

        If NumCatalogo = 22 Then
            ' cmbTipDocumento.SelectedValue = frmMasterCompraForanea.TipoPago

            'If frmMasterCompraForanea.TipoPago = 1020 Then
            '    txtNumDoc.Enabled = False
            '    txtNumDoc.Text = frmMasterCompraForanea.txtNumDocumento.Text
            'Else
            '    txtNumDoc.Enabled = True
            '    txtNumDoc.Text = ""
            'End If

            cmbTipDocumento.Enabled = False
        Else
            cmbTipDocumento.SelectedIndex = 0
            txtNumDoc.Enabled = True
            txtNumDoc.Text = ""
            cmbTipDocumento.Enabled = True
        End If

    End Sub

    Public Sub cargarTipoReferecia()
        cmbReferencia.DataSource = SQL("SELECT CatDet.IdCategoriaDetalle, CatDet.Estado FROM Categorias Cat INNER JOIN Categorias_Detalle CatDet ON Cat.IdCategoria = CatDet.IdCategoria where CatDet.IdCategoria = 6 and CatDet.Activo = 1 and Cat.Activo = 1", "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)
        cmbReferencia.DisplayMember = "Estado"
        cmbReferencia.ValueMember = "IdCategoriaDetalle"

        If NumCatalogo = 22 Then
            txtNumRefe.Text = frmMasterCompraForanea.txtNumDocumento.Text
            txtNumRefe.Enabled = False
            cmbReferencia.SelectedValue = 20
            cmbReferencia.Enabled = False
        Else
            txtNumRefe.Enabled = True
            cmbReferencia.SelectedIndex = 0
            cmbReferencia.Enabled = True
        End If
    End Sub

    Sub CargarProveedor()
        cmbProveedor.DataSource = SQL("Select Codigo_Proveedor As Codigo, Nombre_Proveedor As Nombre from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
        cmbProveedor.DisplayMember = "Nombre"
        cmbProveedor.ValueMember = "Codigo"
        cmbProveedor.SelectedIndex = 0
    End Sub

    Public Sub CargarDatosProveedor(ByVal CodigoProv As String)
        Dim strsqlDatProv As String = ""
        Dim Plazo As Integer = 0

        strsqlDatProv = String.Format("SELECT Plazo,Is_IR,Is_IMI,Is_IVA,TasaIR,TasaIMI FROM Tbl_Proveedor WHERE Codigo_Proveedor='{0}'", CodigoProv)
        Dim tblDatosProdServ As DataTable = SQL(strsqlDatProv, "tblDatosProv", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosProdServ.Rows.Count <> 0 Then

            If NumCatalogo = 22 Then
                Plazo = frmMasterCompraForanea.txtPlazo.Text
            End If

            deFechaVenc.Value = DateAdd(DateInterval.Day, CInt(Plazo), deFechaDoc.Value)
            If (IIf(IsDBNull(tblDatosProdServ.Rows(0).Item(1)), 0, tblDatosProdServ.Rows(0).Item(1))) <> 0 Then
                CkIR.Checked = True
                txtPorcIR.Text = tblDatosProdServ.Rows(0).Item(4)
            Else
                CkIR.Checked = False
                txtPorcIR.Text = 0
            End If
            If (IIf(IsDBNull(tblDatosProdServ.Rows(0).Item(2)), 0, tblDatosProdServ.Rows(0).Item(2))) <> 0 Then
                CkIMI.Checked = True
                txtPorcIMI.Text = tblDatosProdServ.Rows(0).Item(5)
            Else
                CkIMI.Checked = False
                txtPorcIMI.Text = 0
            End If
        End If
    End Sub

    Sub Recalcular()
        If Not txtSubTotal.Text = "" Then
            If CDbl(txtSubTotal.Text) > 0 Then
                txtIR.Text = ((CDbl(txtPorcIR.Text) / 100) * CDbl(txtSubTotal.Text))
                txtIMI.Text = ((CDbl(txtPorcIMI.Text) / 100) * CDbl(txtSubTotal.Text))
                txtIVA.Text = ((0.15) * CDbl(txtSubTotal.Text))
                txtTotalNeto.Text = ((CDbl(txtSubTotal.Text) - (CDbl(txtIR.Text) + CDbl(txtIMI.Text))) + CDbl(txtIVA.Text))
                txtNeto.Text = (CDbl(txtTotalNeto.Text) + CDbl(txtOtrosGas.Text) + CDbl(txtMultas.Text))
            ElseIf CDbl(txtSubTotal.Text) = 0 Then
                txtIR.Text = "0.0000"
                txtIMI.Text = "0.0000"
                txtIVA.Text = "0.0000"
                txtTotalNeto.Text = "0.0000"
                txtNeto.Text = "0.0000"
            End If
        End If
    End Sub

    Public Sub Limpiar()
        txtNumRefe.Text = ""
        deFechaDoc.Text = Date.Now.Date
        deFechaVenc.Text = Date.Now.Date
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub FrmPagos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            cargarTipoDocumento()
            CargarProveedor()
            cargarTipoReferecia()

            txtSubTotal.Text = "0.0000"
            txtOtrosGas.Text = "0.0000"
            txtMultas.Text = "0.0000"

            If NumCatalogo = 28 Then
                If nTipoEdic = 1 Then
                    Limpiar()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CkIR_CheckedChanged(sender As Object, e As EventArgs) Handles CkIR.CheckedChanged
        If CkIR.Checked = True Then
            txtPorcIR.ReadOnly = False
            txtPorcIR.Focus()
            txtPorcIR.SelectionStart = 0
            txtPorcIR.SelectionLength = txtPorcIR.TextLength
        Else
            txtPorcIR.Text = 0
            txtPorcIR.ReadOnly = True
        End If
    End Sub

    Private Sub CkIMI_CheckedChanged(sender As Object, e As EventArgs) Handles CkIMI.CheckedChanged
        If CkIMI.Checked = True Then
            txtPorcIMI.ReadOnly = False
            txtPorcIMI.Focus()
            txtPorcIMI.SelectionStart = 0
            txtPorcIMI.SelectionLength = txtPorcIMI.TextLength
        Else
            txtPorcIMI.Text = 0
            txtPorcIMI.ReadOnly = True
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim pagos As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim CodigoProv As String = ""

        Try
            Dim result As Integer = MessageBox.Show("Desea registrar el pago?", "Información", MessageBoxButtons.YesNo)

            CodigoProv = (DirectCast(cmbProveedor.SelectedItem, DataRowView).Item("Codigo")).ToString
            If result = DialogResult.Yes Then
                Resum = pagos.EditarMasterPagos(nSucursal, CodigoProv, cmbTipDocumento.SelectedValue, txtNumDoc.Text, deFechaDoc.Value, deFechaVenc.Value, txtNumRefe.Text, cmbReferencia.SelectedValue, String.Empty, txtSubTotal.Text, txtIR.Text, txtIMI.Text, txtIVA.Text, 0, txtOtrosGas.Text, txtMultas.Text, Date.Now, True, 1)
                MsgBox("Pago registrado correctamente.", MsgBoxStyle.Information, "Información")

                If NumCatalogo = 22 Then
                    '   frmMasterCompraForanea.CargarDetalleDoc(My.Settings.Sucursal, frmMasterCompraForanea.txtNumDocumento.Text, IIf(frmMasterCompraForanea.rbContado.Checked = True, 1, 2), frmMasterCompraForanea.luProveedor.EditValue)

                ElseIf NumCatalogo = 28 Then
                    frmPrincipal.CargarDatos()
                End If

                Me.Close()
            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub txtSubTotal_TextChanged(sender As Object, e As EventArgs) Handles txtSubTotal.TextChanged
        Recalcular()
    End Sub

    Private Sub txtPorcIMI_TextChanged(sender As Object, e As EventArgs) Handles txtPorcIMI.TextChanged
        Recalcular()
    End Sub

    Private Sub txtPorcIR_TextChanged(sender As Object, e As EventArgs) Handles txtPorcIR.TextChanged
        Recalcular()
    End Sub

    Private Sub txtMultas_TextChanged(sender As Object, e As EventArgs) Handles txtMultas.TextChanged
        Recalcular()
    End Sub

    Private Sub txtOtrosGas_TextChanged(sender As Object, e As EventArgs) Handles txtOtrosGas.TextChanged
        Recalcular()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub cmbProveedor2_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbProveedor.SelectedValueChanged
        Dim CodigoProv As String = ""
        CodigoProv = (DirectCast(cmbProveedor.SelectedItem, DataRowView).Item("Codigo")).ToString
        CargarDatosProveedor(CodigoProv)
    End Sub
End Class