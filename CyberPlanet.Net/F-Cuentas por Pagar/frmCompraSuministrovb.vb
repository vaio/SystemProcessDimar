﻿Public Class frmCompraSuministrovb
	Dim nNextDiario As Integer

    Private Sub frmCompraSuministrovb_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarSuministros()
    End Sub

    Sub CargarSuministros()
        cmbSuministros.Properties.Items.Clear()
        For Each dr As DataRow In SQL("Select Nombre_Producto from Tbl_Producto where TipoArticulo=2 and Activo=1  ", "tblSuministros", My.Settings.SolIndustrialCNX).Tables(0).Rows
            cmbSuministros.Properties.Items.Add(dr.Item("Nombre_Producto"))
        Next
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim StrTrans As String = ""
        Dim Transacciones As New clsAddTransaccion(My.Settings.SolIndustrialCNX)
        Dim sComentario As String

        Dim StrMasterDiario As String = ""
        Dim strPadres As String
        Dim dtPadre As DataTable
        Dim sCodPadre As String
        Dim bIsHijo As Boolean = True
        Dim sNumCuenta As String

        Try
            sComentario = "Compra de " & cmbSuministros.Text & " - " & txtObservaciones.Text
            StrTrans = Transacciones.AgregarTransaccion(0, 4, Now, sComentario, CDbl(txtMonto.Text), CDbl(0), CDbl(txtMonto.Text), 1, 1, -1, 1)
            If StrTrans = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
                'frmTransacciones.CargarTransacciones()
                'frmPrincipal.CargarContabilidad()
                nNextDiario = SiguienteTransaccionDiario()
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            '*********************************CONTABILIZAR ESTA TRANSACCION*************************************************
            'Master Comprobante
            StrMasterDiario = Transacciones.AgregarMasterDiario(nNextDiario, Now, sComentario, CDbl(txtMonto.Text), 1)   'nTipoEdic
            If StrMasterDiario <> "OK" Then
                MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            'Debito
            sNumCuenta = 105
            StrMasterDiario = Transacciones.AgregarDetalleDiario(nNextDiario, sNumCuenta, CDbl(txtMonto.Text), 0, 1)            'nTipoEdic
            If StrMasterDiario <> "OK" Then
                MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            '**************APLICAR UN DEBITO A PADRES E HIJOS
            Do While bIsHijo = True
                strPadres = "SELECT Cuenta_Padre FROM Tbl_CatalogoCuentas where (Codigo_Cuenta = '" & sNumCuenta & "')"
                dtPadre = SQL(strPadres, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)
                sCodPadre = dtPadre.Rows(0).Item(0)
                If sCodPadre <> "0" Then
                    StrMasterDiario = Transacciones.ActualizarCatalogo(sCodPadre, CDbl(txtMonto.Text), 1)            'nTipoEdic
                    If StrMasterDiario <> "OK" Then
                        MsgBox("Ha ocurrido un error al actualizar la cuenta en el catalogo.", MsgBoxStyle.Information, "SIGCA")
                    Else
                        bIsHijo = True
                        sNumCuenta = sCodPadre
                    End If
                Else
                    bIsHijo = False
                End If
            Loop
            '************

            'Credito
            sNumCuenta = 10102
            bIsHijo = True
            StrMasterDiario = Transacciones.AgregarDetalleDiario(nNextDiario, sNumCuenta, 0, CDbl(txtMonto.Text), 1)            'nTipoEdic
            If StrMasterDiario <> "OK" Then
                MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            '**************APLICAR UN CREDITO A PADRES E HIJOS
            Do While bIsHijo = True
                strPadres = "SELECT Cuenta_Padre FROM Tbl_CatalogoCuentas where (Codigo_Cuenta = '" & sNumCuenta & "')"
                dtPadre = SQL(strPadres, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)
                sCodPadre = dtPadre.Rows(0).Item(0)
                If sCodPadre <> "0" Then
                    StrMasterDiario = Transacciones.ActualizarCatalogo(sCodPadre, CDbl(txtMonto.Text), 0)
                    If StrMasterDiario <> "OK" Then
                        MsgBox("Ha ocurrido un error al actualizar la cuenta en el catalogo.", MsgBoxStyle.Information, "SIGCA")
                    Else
                        bIsHijo = True
                        sNumCuenta = sCodPadre
                    End If
                Else
                    bIsHijo = False
                End If
            Loop
            '************
            '*************************************FIN DE CONTABILIZAR TRANSACCION

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
		If InStr("0123456789" & Chr(8), e.KeyChar) Then
			e.Handled = False
		Else
			e.Handled = True
		End If
End Sub

End Class