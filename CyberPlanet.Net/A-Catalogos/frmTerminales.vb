﻿Public Class frmTerminales

	Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
		Dim strsql As String = ""

		strsql = "SELECT Id_Terminal,Nombre_Terminal FROM Tbl_Terminal WHERE Id_Terminal='" & CodigoEntidad & "'"
        Dim tblDatosTerminal As DataTable = SQL(strsql, "tblDatosTerminal", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosTerminal.Rows.Count <> 0 Then
            txtCodTerminal.Text = tblDatosTerminal.Rows(0).Item(0)
            txtNomTerminal.Text = tblDatosTerminal.Rows(0).Item(1)
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmTerminales_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodTerminal.Visible = True
            txtCodTerminal.Visible = True
            txtCodTerminal.Text = RegistroMaximo()
            txtNomTerminal.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodTerminal.Visible = True
            txtCodTerminal.Enabled = False
            txtCodTerminal.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoDepartPais As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoDepartPais.EditarTerminales(CInt(IIf(txtCodTerminal.Text = "", 0, txtCodTerminal.Text)), txtNomTerminal.Text.ToUpper, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

	Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
		Cerrar()
	End Sub

End Class