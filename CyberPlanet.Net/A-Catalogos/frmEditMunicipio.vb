﻿Public Class frmEditMunicipio

  Private Sub frmEditMunicipio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    cargarDepartPais()
    If nTipoEdic = 1 Then
      lblCodMunicipio.Visible = True
      txtCodMunicipio.Visible = True
      txtCodMunicipio.Text = RegistroMaximo()
    ElseIf nTipoEdic = 2 Then
      lblCodMunicipio.Visible = True
      txtCodMunicipio.Enabled = False
      txtCodMunicipio.Visible = True
      CargarDatos(CodigoEntidad)
    ElseIf nTipoEdic = 3 Then
      Panel2.Enabled = False
      CargarDatos(CodigoEntidad)
    End If
  End Sub

  Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
    Dim Resum As String = ""
        Dim CatalogoCateg As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoCateg.EditarMunicipio(CInt(IIf(txtCodMunicipio.Text = "", 0, txtCodMunicipio.Text)), txtNomMunicipio.Text.ToUpper, luDepartPais.EditValue, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo,Municipio,Departamento FROM vwMunicipios WHERE Codigo='" & CodigoEntidad & "'"
        Dim tblDatosMunic As DataTable = SQL(strsql, "tblDatosMunic", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosMunic.Rows.Count <> 0 Then
            txtCodMunicipio.Text = tblDatosMunic.Rows(0).Item(0)
            txtNomMunicipio.Text = tblDatosMunic.Rows(0).Item(1)
            luDepartPais.Text = tblDatosMunic.Rows(0).Item(2)
        End If
    End Sub

    Private Sub cargarDepartPais()
        TblDepartPaisBindingSource.DataSource = SQL("select Id_Departamento,Nombre_Departamento from Tbl_DepartamentoPais order by Id_Departamento", "tblDepartPais", My.Settings.SolIndustrialCNX).Tables(0)
        luDepartPais.Properties.DataSource = TblDepartPaisBindingSource
        luDepartPais.Properties.DisplayMember = "Nombre_Departamento"
        luDepartPais.Properties.ValueMember = "Id_Departamento"
        luDepartPais.ItemIndex = 0
    End Sub

  Public Sub Cerrar()
    Me.Dispose()
    Close()
  End Sub

  Private Sub cmdAddDeparPais_Click(sender As Object, e As EventArgs) Handles cmdAddDeparPais.Click
    nTipoEdic_Ant = nTipoEdic
    NumCatalogo_Ant = NumCatalogo
    nTipoEdic = 1
		NumCatalogo = 8
    frmCatalogo.AbrirEditorCatalogos()
    nTipoEdic = nTipoEdic_Ant
    NumCatalogo = NumCatalogo_Ant
    nTipoEdic_Ant = 0
    NumCatalogo_Ant = 0
		cargarDepartPais()
  End Sub

End Class