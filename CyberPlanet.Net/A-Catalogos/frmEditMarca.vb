﻿Public Class frmEditMarca

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo_Marca,Nombre_Marca FROM Tbl_Marca WHERE Codigo_Marca='" & CodigoEntidad & "'"
        Dim tblDatosMarca As DataTable = SQL(strsql, "tblDatosMarca", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosMarca.Rows.Count > 0 Then
            txtCodMarca.Text = tblDatosMarca.Rows(0).Item("Codigo_Marca")
            txtNomMarca.Text = tblDatosMarca.Rows(0).Item("Nombre_Marca")
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub frmEditMarca_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodMarca.Visible = True
            txtCodMarca.Visible = True
            txtCodMarca.Text = RegistroMaximo()
            txtNomMarca.Text = ""
            txtNomMarca.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodMarca.Visible = True
            txtCodMarca.Enabled = False
            txtCodMarca.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoMarcas As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoMarcas.EditarMarcas(CInt(IIf(txtCodMarca.Text = "", 0, txtCodMarca.Text)), txtNomMarca.Text, nTipoEdic)
            If Not Resum = "OK" Then
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

    Private Sub frmEditMarca_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Cerrar()
    End Sub
End Class