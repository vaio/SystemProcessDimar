﻿Public Class frmEditActivosFijos

	Private Sub frmEditActivosFijos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		cargarProveedores()
		cargarDepartamentos()
		cargarCategorias()
		cargarMarcas()
		cargarModelos()
		If nTipoEdic = 1 Then
			txtCodActivoFijo.Enabled = False
			txtCodActivoFijo.Text = RegistroMaximo()
			deFechaIng.DateTime = Now
			deFechaBaja.DateTime = Now
			ckIsActivo.Checked = 1
		ElseIf nTipoEdic = 2 Then
			txtCodActivoFijo.Enabled = False
			CargarDatos(CodigoEntidad)
		ElseIf nTipoEdic = 3 Then
			Panel2.Enabled = False
			CargarDatos(CodigoEntidad)
		End If
	End Sub


	Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
		Dim Resum As String = ""
        Dim CatalogoProductos As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            'Resum = CatalogoProductos.EditarProducto(CInt(IIf(txtCodActivoFijo.Text = "", 0, txtCodActivoFijo.Text)), txtNomActivoFijo.Text.ToUpper, 0, CDbl(IIf(txtCostoCompra.Text = "", 0, txtCostoCompra.Text)), CDbl(IIf(txtPrecioMercado.Text = "", 0, txtPrecioMercado.Text)), luDepart.EditValue, luCateg.EditValue, luProv.EditValue, luMarca.EditValue, luModelo.EditValue, 3, CInt(IIf(txtPeriodoDepre.Text = "", 0, txtPeriodoDepre.Text)), nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            frmPrincipal.CargarDatos()
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo,Nombre_ActivoFijo,Costo,Precio_Venta,Nombre_Proveedor,Nombre_Departamento,Nombre_Categoria,Nombre_Marca,Nombre_Modelo,Activo,Fecha_Ingreso,Fecha_Baja,PeriodoDepre  FROM vwActivosFijos WHERE Codigo='" & CodigoEntidad & "'"
        Dim tblDatosProdServ As DataTable = SQL(strsql, "tblDatosProdServ", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosProdServ.Rows.Count <> 0 Then
            txtCodActivoFijo.Text = tblDatosProdServ.Rows(0).Item(0)
            txtNomActivoFijo.Text = tblDatosProdServ.Rows(0).Item(1)
            ckIsActivo.Checked = tblDatosProdServ.Rows(0).Item(9)
            txtCostoCompra.Text = Format(tblDatosProdServ.Rows(0).Item(2), "###,###,###.00")
            txtPrecioMercado.Text = Format(tblDatosProdServ.Rows(0).Item(3), "###,###,###.00")
            luProv.Text = tblDatosProdServ.Rows(0).Item(4)
            luDepart.Text = tblDatosProdServ.Rows(0).Item(5)
            luCateg.Text = tblDatosProdServ.Rows(0).Item(6)
            luMarca.Text = tblDatosProdServ.Rows(0).Item(7)
            luModelo.Text = tblDatosProdServ.Rows(0).Item(8)
            txtPeriodoDepre.Text = tblDatosProdServ.Rows(0).Item(12)
            deFechaIng.DateTime = tblDatosProdServ.Rows(0).Item(10)
            deFechaBaja.DateTime = tblDatosProdServ.Rows(0).Item(11)
        End If

    End Sub

    Private Sub cargarProveedores()
        TblProvBindingSource.DataSource = SQL("select Codigo_Proveedor,Nombre_Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
        luProv.Properties.DataSource = TblProvBindingSource
        luProv.Properties.DisplayMember = "Nombre_Proveedor"
        luProv.Properties.ValueMember = "Codigo_Proveedor"
        luProv.ItemIndex = 0
    End Sub

    Private Sub cargarDepartamentos()
        TblDeparBindingSource.DataSource = SQL("select Codigo_Linea,Nombre_Linea from Linea_Producto order by Codigo_Linea", "tblLineas", My.Settings.SolIndustrialCNX).Tables(0)
        luDepart.Properties.DataSource = TblDeparBindingSource
        luDepart.Properties.DisplayMember = "Nombre_Linea"
        luDepart.Properties.ValueMember = "Codigo_Linea"
        luDepart.ItemIndex = 0
    End Sub

    Private Sub cargarCategorias()
        Dim strsql As String = "SELECT Codigo_Categoria, Nombre_Categoria FROM Categoria_Producto INNER JOIN Linea_Producto ON Categoria_Producto.Codigo_Linea = Linea_Producto.Codigo_Linea"
        strsql = strsql & " WHERE (Linea_Producto.Nombre_Linea = N'" & luDepart.Text & "') ORDER BY Categoria_Producto.Codigo_Categoria"
        TblCategBindingSource.DataSource = SQL(strsql, "tblCategorias", My.Settings.SolIndustrialCNX).Tables(0)
        luCateg.Properties.DataSource = TblCategBindingSource
        luCateg.Properties.DisplayMember = "Nombre_Categoria"
        luCateg.Properties.ValueMember = "Codigo_Categoria"
        luCateg.ItemIndex = 0
    End Sub

    Private Sub cargarMarcas()
        TblMarcaBindingSource.DataSource = SQL("select Codigo_Marca,Nombre_Marca from Tbl_Marca order by Codigo_Marca", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        luMarca.Properties.DataSource = TblMarcaBindingSource
        luMarca.Properties.DisplayMember = "Nombre_Marca"
        luMarca.Properties.ValueMember = "Codigo_Marca"
        luMarca.ItemIndex = 0
    End Sub

    Private Sub cargarModelos()
        Dim strsql As String = "SELECT Id_Modelo, Nombre_Modelo FROM Tbl_Modelo INNER JOIN Tbl_Marca ON Tbl_Modelo.Codigo_Marca = Tbl_Marca.Codigo_Marca"
        strsql = strsql & " WHERE (Tbl_Marca.Nombre_Marca = N'" & luMarca.Text & "') ORDER BY Tbl_Modelo.Id_Modelo"
        TblModeloBindingSource.DataSource = SQL(strsql, "tblModelos", My.Settings.SolIndustrialCNX).Tables(0)
        luModelo.Properties.DataSource = TblModeloBindingSource
        luModelo.Properties.DisplayMember = "Nombre_Modelo"
        luModelo.Properties.ValueMember = "Id_Modelo"
        luModelo.ItemIndex = 0
    End Sub

	Public Sub Cerrar()
		Me.Dispose()
		Close()
	End Sub

	Private Sub luDepart_EditValueChanged(sender As Object, e As EventArgs) Handles luDepart.EditValueChanged
		cargarCategorias()
	End Sub

	Private Sub luMarca_EditValueChanged(sender As Object, e As EventArgs) Handles luMarca.EditValueChanged
		cargarModelos()
	End Sub

	Private Sub cmdAddProv_Click(sender As Object, e As EventArgs) Handles cmdAddProv.Click
		nTipoEdic_Ant = nTipoEdic
		NumCatalogo_Ant = NumCatalogo
		nTipoEdic = 1
		NumCatalogo = 6
		frmCatalogo.AbrirEditorCatalogos()
		nTipoEdic = nTipoEdic_Ant
		NumCatalogo = NumCatalogo_Ant
		nTipoEdic_Ant = 0
		NumCatalogo_Ant = 0
		cargarProveedores()
	End Sub

	Private Sub cmdAddDepar_Click(sender As Object, e As EventArgs) Handles cmdAddDepar.Click
		nTipoEdic_Ant = nTipoEdic
		NumCatalogo_Ant = NumCatalogo
		nTipoEdic = 1
		NumCatalogo = 4
		frmCatalogo.AbrirEditorCatalogos()
		nTipoEdic = nTipoEdic_Ant
		NumCatalogo = NumCatalogo_Ant
		nTipoEdic_Ant = 0
		NumCatalogo_Ant = 0
		cargarDepartamentos()
	End Sub

	Private Sub cmdAddCateg_Click(sender As Object, e As EventArgs) Handles cmdAddCateg.Click
		nTipoEdic_Ant = nTipoEdic
		NumCatalogo_Ant = NumCatalogo
		nTipoEdic = 1
		NumCatalogo = 5
		frmCatalogo.AbrirEditorCatalogos()
		nTipoEdic = nTipoEdic_Ant
		NumCatalogo = NumCatalogo_Ant
		nTipoEdic_Ant = 0
		NumCatalogo_Ant = 0
		cargarCategorias()
	End Sub

	Private Sub cmdAddMarca_Click(sender As Object, e As EventArgs) Handles cmdAddMarca.Click
		nTipoEdic_Ant = nTipoEdic
		NumCatalogo_Ant = NumCatalogo
		nTipoEdic = 1
		NumCatalogo = 2
		frmCatalogo.AbrirEditorCatalogos()
		nTipoEdic = nTipoEdic_Ant
		NumCatalogo = NumCatalogo_Ant
		nTipoEdic_Ant = 0
		NumCatalogo_Ant = 0
		cargarMarcas()
	End Sub

	Private Sub cmdAddModelo_Click(sender As Object, e As EventArgs) Handles cmdAddModelo.Click
		nTipoEdic_Ant = nTipoEdic
		NumCatalogo_Ant = NumCatalogo
		nTipoEdic = 1
		NumCatalogo = 3
		frmCatalogo.AbrirEditorCatalogos()
		nTipoEdic = nTipoEdic_Ant
		NumCatalogo = NumCatalogo_Ant
		nTipoEdic_Ant = 0
		NumCatalogo_Ant = 0
		cargarModelos()
	End Sub

End Class