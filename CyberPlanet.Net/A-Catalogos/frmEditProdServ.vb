﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmEditProdServ
    Private Const INT_txtPrecioText As Integer = 0
    'CONEXION ALTERNA
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter

    'GENERAR CODIGO
    Dim linea As String
    Dim categoria As String
    Dim marca As String
    Dim modelo As String
    Dim consecutivo As Integer
    Dim codigo As String

    Public Sub CargarDatos(ByVal CodigoEntidad As String)
        Dim strsql As String = ""

        Try
            strsql = "SELECT TOP 1 Codigo, Nombre_Producto,Codigo_Alterno,Nombre_Generico,Tipo_Articulo,Nombre_Proveedor,Nombre_Linea,Nombre_Categoria,Nombre_Marca,Nombre_Modelo,Nombre_UnidadMed," & _
                      " Descripcion,Existencia_Minima,Existencia_Maxima,EnPaquetes,Unid_por_Paquetes,EsExcento,Costo_Crd,Costo_USS,Fecha_Ingreso,Fecha_Baja,Descontinuado, Id_ProductosTipo, SeFactura FROM vwVerProductos WHERE Codigo='" & CodigoEntidad & "'"

            Dim tblDatosProdServ As DataTable = SQL(strsql, "tblDatosProdServ", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatosProdServ.Rows.Count <> 0 Then
                txtNomProd.Text = tblDatosProdServ.Rows(0).Item(1)
                txtCodAlterno.Text = tblDatosProdServ.Rows(0).Item(2)
                txtNomGeneric.Text = tblDatosProdServ.Rows(0).Item(3)
                cmbTipArticulo.SelectedIndex = tblDatosProdServ.Rows(0).Item(4)
                cmbProve.Text = tblDatosProdServ.Rows(0).Item(5)
                cmbLineas.Text = tblDatosProdServ.Rows(0).Item(6)
                cmbCatego.Text = tblDatosProdServ.Rows(0).Item(7)
                cmbMarcas.Text = tblDatosProdServ.Rows(0).Item(8)
                cmbModelos.Text = tblDatosProdServ.Rows(0).Item(9)
                cmbUnidMedi.Text = tblDatosProdServ.Rows(0).Item(10)

                txtCodProd.Text = tblDatosProdServ.Rows(0).Item(0)

                luPresenta.Text = tblDatosProdServ.Rows(0).Item(11)
                txtExistMin.Text = tblDatosProdServ.Rows(0).Item(12)
                txtExistMax.Text = tblDatosProdServ.Rows(0).Item(13)
                ckIsPaquetes.Checked = tblDatosProdServ.Rows(0).Item(14)
                txtUnidperPakage.Text = tblDatosProdServ.Rows(0).Item(15)
                ckIsExcento.Checked = tblDatosProdServ.Rows(0).Item(16)
                txtCostoCS.Text = Format(tblDatosProdServ.Rows(0).Item(17), "###,###,###.00")
                txtCostoUS.Text = Format(tblDatosProdServ.Rows(0).Item(18), "###,###,###.00")
                deFechaIng.EditValue = IIf(tblDatosProdServ.Rows(0).Item(19) Is DBNull.Value, "", tblDatosProdServ.Rows(0).Item(19))
                ckIsDescontinuado.Checked = tblDatosProdServ.Rows(0).Item(21)
                deFechaBaja.EditValue = IIf(tblDatosProdServ.Rows(0).Item(20) Is DBNull.Value, "", tblDatosProdServ.Rows(0).Item(20))
                CmbTipoProducto.SelectedIndex = IIf(tblDatosProdServ.Rows(0).Item(22) Is DBNull.Value, 0, tblDatosProdServ.Rows(0).Item(22))
                ckIsFactura.Checked = tblDatosProdServ.Rows(0).Item(23)

            End If
        Catch ex As Exception
            MsgBox("")

        End Try
    End Sub
    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf (e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",")) Or (e.KeyChar = "." And Not CajaTexto.Text.IndexOf(".")) Then
            e.Handled = True
        ElseIf e.KeyChar = "," Or e.KeyChar = "." Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub frmEditProdServ_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Tbl_ProveedorTableAdapter.Fill(Me.WorkSystemDataSet.Tbl_Proveedor)
        cargarProveedores()
        cargarDepartamentos()
        cargarCategorias()
        cargarMarcas()
        cargarModelos()
        cargarUnidMed()
        cargarPresent()
        cargarTipoProductos()
        txtCostoCS.Text = 0
        txtCostoUS.Text = 0
        txtExistMin.Text = 0
        txtExistMax.Text = 0
        ' txtPrecio.Text = 0
        cmbTipArticulo.SelectedIndex = 0

        txtCostoUS.Enabled = False

        cargar()
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoProductos As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoProductos.EditarProducto(IIf(txtCodProd.Text = "", "000000", txtCodProd.Text), txtNomProd.Text.ToUpper, txtCodAlterno.Text, txtNomGeneric.Text.ToUpper, cmbTipArticulo.SelectedValue, cmbProve.SelectedValue, cmbLineas.SelectedValue, cmbCatego.SelectedValue, cmbMarcas.SelectedValue, cmbModelos.SelectedValue, cmbUnidMedi.SelectedValue, luPresenta.SelectedValue, CInt(IIf(txtExistMin.Text = "", 0, txtExistMin.Text)), CInt(IIf(txtExistMax.Text = "", 0, txtExistMax.Text)), IIf(ckIsPaquetes.Checked, 1, 0), CInt(IIf(txtUnidperPakage.Text = "", 0, txtUnidperPakage.Text)), IIf(ckIsExcento.Checked, 1, 0), CDbl(IIf(txtCostoCS.Text = "", 0, txtCostoCS.Text)), CDbl(IIf(txtCostoUS.Text = "", 0, txtCostoUS.Text)), nTipoEdic, CmbTipoProducto.SelectedValue, ckIsDescontinuado.Checked, ckIsFactura.Checked, deFechaIng.DateTime, deFechaBaja.DateTime)
            If Resum = "OK" Then
                MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            CodigoEntidad = txtCodProd.Text
            nTipoEdic = 2
            cargar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
            Me.Dispose()
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub cargarProveedores()
        cmbProve.Text = ""
        cmbProve.DataSource = SQL("Select Codigo_Proveedor as Codigo,Nombre_Proveedor AS Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedores", My.Settings.SolIndustrialCNX).Tables(0)
        cmbProve.DisplayMember = "Proveedor"
        cmbProve.ValueMember = "Codigo"

        If cmbProve.Items.Count > 0 Then
            cmbProve.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0

    End Sub

    Public Sub cargarDepartamentos()
        cmbLineas.DataSource = SQL("select Codigo_Linea AS Codigo,Nombre_Linea AS Linea from Linea_Producto order by Codigo_Linea", "tblLinea", My.Settings.SolIndustrialCNX).Tables(0)
        cmbLineas.DisplayMember = "Linea"
        cmbLineas.ValueMember = "Codigo"

        If cmbLineas.Items.Count > 0 Then
            cmbLineas.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0
    End Sub

    Public Sub cargarTipoProductos()
        CmbTipoProducto.DataSource = SQL("SELECT Id_ProductosTipo as Codigo, Descripion AS Tipo FROM Productos_Tipo order by Id_ProductosTipo", "tblTipoProductos", My.Settings.SolIndustrialCNX).Tables(0)
        CmbTipoProducto.DisplayMember = "Tipo"
        CmbTipoProducto.ValueMember = "Codigo"
        CmbTipoProducto.SelectedIndex = 0
    End Sub

    Public Sub cargarCategorias()
        Dim strsql As String = "SELECT Codigo_Categoria AS Codigo, Nombre_Categoria AS Categoria FROM Categoria_Producto INNER JOIN Linea_Producto ON Categoria_Producto.Codigo_Linea = Linea_Producto.Codigo_Linea"
        strsql = strsql & " WHERE (Linea_Producto.Nombre_Linea = N'" & cmbLineas.Text & "') order by Codigo_Categoria"

        cmbCatego.Text = ""
        cmbCatego.DataSource = SQL(strsql, "tblCategorias", My.Settings.SolIndustrialCNX).Tables(0)
        cmbCatego.DisplayMember = "Categoria"
        cmbCatego.ValueMember = "Codigo"

        If cmbCatego.Items.Count > 0 Then
            cmbCatego.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0
    End Sub

    Public Sub cargarMarcas()
        cmbMarcas.DataSource = SQL("select Codigo_Marca AS Codigo,Nombre_Marca AS Marca from Tbl_Marca order by Codigo_Marca", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        cmbMarcas.DisplayMember = "Marca"
        cmbMarcas.ValueMember = "Codigo"

        If cmbMarcas.Items.Count > 0 Then
            cmbMarcas.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0
    End Sub

    Public Sub cargarModelos()
        Dim strsql As String = "SELECT Id_Modelo AS Codigo, Nombre_Modelo AS Modelo FROM Tbl_Modelo INNER JOIN Tbl_Marca ON Tbl_Modelo.Id_Marca = Tbl_Marca.Codigo_Marca"
        strsql = strsql & " WHERE (Tbl_Marca.Nombre_Marca = N'" & cmbMarcas.Text & "') order by Id_Modelo"

        cmbModelos.DataSource = SQL(strsql, "tblModelos", My.Settings.SolIndustrialCNX).Tables(0)
        cmbModelos.DisplayMember = "Modelo"
        cmbModelos.ValueMember = "Codigo"

        If cmbModelos.Items.Count > 0 Then
            cmbModelos.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0
    End Sub

    Public Sub cargarUnidMed()
        cmbUnidMedi.DataSource = SQL("select Codigo_UnidadMed AS Codigo,Nombre_UnidadMed AS Unidad from Unidades_Medida order by Codigo_UnidadMed", "tblUnidMed", My.Settings.SolIndustrialCNX).Tables(0)
        cmbUnidMedi.DisplayMember = "Unidad"
        cmbUnidMedi.ValueMember = "Codigo"

        If cmbUnidMedi.Items.Count > 0 Then
            cmbUnidMedi.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0
    End Sub

    Public Sub cargarPresent()
        luPresenta.DataSource = SQL("select Codigo_Presentacion AS Codigo,Descripcion AS Presentacion from Presentacion order by Codigo_Presentacion", "tblPresent", My.Settings.SolIndustrialCNX).Tables(0)
        luPresenta.DisplayMember = "Presentacion"
        luPresenta.ValueMember = "Codigo"

        If luPresenta.Items.Count > 0 Then
            luPresenta.SelectedIndex = itemAdd_Ant
        End If

        itemAdd_Ant = 0
    End Sub
    Public Sub cargar()
        If nTipoEdic = 1 Then
            txtCodAlterno.Text = "0000"
            txtNomProd.Text = ""
            txtNomGeneric.Text = ""
            btnAceptar.Text = "Agregar"
            txtCodProd.Enabled = False
            'pnlBodegaPrecio.Enabled = False
            deFechaIng.DateTime = Now
            ckIsDescontinuado.Checked = 0
        ElseIf nTipoEdic = 2 Then
            txtCodProd.Enabled = False
            ' pnlBodegaPrecio.Enabled = True
            btnAceptar.Text = "Actualizar"
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAddProv_Click(sender As Object, e As EventArgs) Handles cmdAddProv.Click
        itemAdd_Ant = cmbProve.SelectedIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 6
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarProveedores()
    End Sub

    Private Sub cmdAddDepar_Click(sender As Object, e As EventArgs) Handles cmdAddDepar.Click
        itemAdd_Ant = cmbLineas.SelectedIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 4
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarDepartamentos()
    End Sub

    Private Sub cmdAddCateg_Click(sender As Object, e As EventArgs) Handles cmdAddCateg.Click
        itemAdd_Ant = cmbCatego.SelectedIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 5
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarCategorias()
    End Sub

    Private Sub cmdAddMarca_Click(sender As Object, e As EventArgs) Handles cmdAddMarca.Click
        itemAdd_Ant = cmbMarcas.SelectedIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 2
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarMarcas()
    End Sub

    Private Sub cmdAddModelo_Click(sender As Object, e As EventArgs) Handles cmdAddModelo.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 3
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarModelos()
    End Sub

    Private Sub cmdAddUnidMed_Click(sender As Object, e As EventArgs) Handles cmdAddUnidMed.Click
        itemAdd_Ant = cmbUnidMedi.SelectedIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 7
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarUnidMed()
    End Sub

    Private Sub cmdAddPresent_Click(sender As Object, e As EventArgs) Handles cmdAddPresent.Click
        itemAdd_Ant = luPresenta.SelectedIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 8
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarPresent()
    End Sub

    Function GetNumBodega(ByVal sNomBodega As String)
        Dim strsql As String = "SELECT Codigo_Bodega FROM Almacenes where Nombre_Bodega='" & sNomBodega & "'"
        GetNumBodega = SQL(strsql, "tblNumBodega", My.Settings.SolIndustrialCNX).Tables(0).Rows(0).Item(0)
    End Function

    Private Sub ckIsPaquetes_CheckedChanged(sender As Object, e As EventArgs) Handles ckIsPaquetes.CheckedChanged
        If ckIsPaquetes.Checked = True Then
            txtUnidperPakage.Enabled = True
            txtUnidperPakage.Focus()
        Else
            txtUnidperPakage.Enabled = False
            txtUnidperPakage.Text = 1
        End If
    End Sub


    Private Sub ckIsDescontinuado_CheckedChanged(sender As Object, e As EventArgs) Handles ckIsDescontinuado.CheckedChanged
        deFechaBaja.DateTime = Nothing
        deFechaBaja.Text = ""
        If ckIsDescontinuado.Checked Then
            deFechaBaja.Enabled = True
        Else
            deFechaBaja.Enabled = False
        End If
    End Sub

    Private Sub txtCostoCS_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoCS.KeyPress
        NumerosyDecimal(txtCostoCS, e)
    End Sub

    Private Sub txtExistMin_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtExistMin.KeyPress
        NumerosyDecimal(txtExistMin, e)
    End Sub

    Private Sub txtExistMax_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtExistMax.KeyPress
        NumerosyDecimal(txtExistMax, e)
    End Sub

    Private Sub cmbLineas_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbLineas.SelectedValueChanged
        cargarCategorias()
    End Sub

    Private Sub luMarcas_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbMarcas.SelectedValueChanged
        cargarModelos()
    End Sub
    Private Sub Generar_Codigo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbLineas.SelectedIndexChanged, cmbCatego.SelectedIndexChanged, cmbMarcas.SelectedIndexChanged, cmbModelos.SelectedIndexChanged
        If cmbLineas.Text <> "" And cmbCatego.Text <> "" And cmbMarcas.Text <> "" And cmbModelos.Text <> "" Then
            If nTipoEdic = 1 Then
                Generacion_Codigo()
            End If
        End If
    End Sub

    Private Sub Generacion_Codigo()
        linea = ""
        categoria = ""
        marca = ""
        modelo = ""
        consecutivo = 0
        codigo = ""
        sqlstring = "select (select Prefijo from Linea_Producto where Nombre_Linea='" + cmbLineas.Text + "') as linea, (select Codigo_Categoria from Categoria_Producto where Nombre_Categoria='" + cmbCatego.Text + "' and Codigo_Linea=(Select Codigo_Linea From Linea_Producto where Nombre_Linea='" + cmbLineas.Text + "' )) as Categoria, (select Codigo_Marca from Tbl_Marca where Nombre_Marca='" + cmbMarcas.Text + "') as Marca, (select Id_Modelo from Tbl_Modelo where Nombre_Modelo='" + cmbModelos.Text + "' and Id_Marca=(select Codigo_Marca from Tbl_Marca where Nombre_Marca='" + cmbMarcas.Text + "')) as Modelo"
        Dim cmd As New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                linea = lectura("Linea")
                categoria = lectura("Categoria")
                marca = lectura("Marca")
                modelo = lectura("Modelo")
            End While
            lectura.Close()
            con.Close()
        Catch ex As Exception
            con.Close()
        End Try
        If categoria.Length = 1 Then
            categoria = "0" + categoria
        End If
        If marca.Length = 1 Then
            marca = "0" + marca
        End If
        If modelo.Length = 1 Then
            modelo = "0" + modelo
        End If
        codigo = linea + categoria + marca + modelo
        Generacion_Codigo2()
        consecutivo = consecutivo + 1
        If consecutivo.ToString.Length = 1 Then
            codigo = codigo + "0" + consecutivo.ToString
        Else
            codigo = codigo + consecutivo.ToString
        End If
        txtCodProd.Text = codigo
    End Sub
    Private Sub Generacion_Codigo2()
        sqlstring = "select MAX(Codigo_Producto) as Codigo from Productos where Codigo_Producto like'" + codigo + "%'"
        Dim cmd As New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                Dim sdata = lectura("Codigo")
                consecutivo = (sdata.substring(sdata.length - 2))
            End While
            lectura.Close()
            con.Close()
        Catch ex As Exception
            con.Close()
        End Try
    End Sub
End Class