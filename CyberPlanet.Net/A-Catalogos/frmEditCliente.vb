﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmEditCliente
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Public edicion As Integer = 0
    Dim c_nulo As Color = Color.Black
    Dim c_C As Color = Color.Coral
    Dim c_B As Color = Color.Gold
    Dim c_A As Color = Color.LightGreen

    Private Sub frmEditCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Height = 413
        GridControl1.Visible = False
        cbTipo.Width = 254
        Select Case edicion
            Case 1 'nuevo
                btnAceptar.Text = "Ingresar"
                CargarCodigo()
                lbnuevo.Visible = True
            Case 2 'update
                lbnuevo.Visible = False
                btnAceptar.Text = "Actualizar"
                CargaF()
            Case 3 'delete
                btnAceptar.Text = "Eliminar"
                lbnuevo.Visible = False
                CargaF()
        End Select
    End Sub
    Private Sub CargaF()
        Dim dt As DataTable = New DataTable()
        sqlstring = "Select * from vwVerClientes where codigo='" + txtcodigo.Text + "' "
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtcodigo.Text = row0("Codigo")
                txtCedula.Text = row0("Cedula")
                txtnombre.Text = row0("Nombre")
                txtapellido.Text = row0("Apellido")
                txtDireccion1.Text = row0("Direccion Domiciliar 1")
                txtDireccion2.Text = row0("Direccion Domiciliar 2")
                txtTdireccion.Text = row0("Direccion Trabajo")
                txttelefono1.Text = row0("Tel Personal 1")
                txttelefono2.Text = row0("Tel Personal 2")
                txtTtelefono.Text = row0("Tel Trabajo") '--
                txtcorreo1.Text = row0("Correo Personal 1")
                txtcorreo2.Text = row0("Correo Personal 2")
                txtTcorreo.Text = row0("Correo Trabajo")
                txtTingreso.Text = row0("Ingresos")
                txtLimiteCredito.Text = row0("Limite de Credito")
                txtSolicitudPendiete.Text = row0("Solicitudes Pendientes")
                txtcantCredito.Text = row0("Cant. Creditos")
                txtSaldo.Text = row0("Saldo")
                cbTipo.SelectedIndex = row0("Id_Tipo") - 1
                If row0("Id_Tipo") = 3 Then
                    txtEcodigo.Text = row0("Codigo Empleado")
                Else
                    txtEcodigo.Text = ""
                End If
                'If row0(19) <> "--" Then
                lbClasificacion.Text = row0("Clasificacion")
                'lbClasificacion.Visible = True
                'End If
                txtDepartamento.Text = row0("Departamento")
                txtMunicipio.Text = row0("Municipio")
                txtDistrito.Text = row0("Distrito")
                txtBarrio.Text = row0("Barrio")
                dtpFechaI.Value = row0("Fecha Ingreso")
                ckbActivo.Checked = row0("Activo")
                If row0("Activo") = False Then
                    dtpFechaF = row0("Fecha Baja")
                End If
                'txtmontoDebito. = row0(27)
                'txtMontoCredito = row0(28)
                txtRnombre1.Text = row0("Nombre Referencia 1") '-----
                'txtRapellido1.Text=
                txtRdireccion1.Text = row0("Direccion Refenrencia 1")
                txtRtel1.Text = row0("Tel Refencia 1")
                txtRnombre2.Text = row0("Nombre Referencia 2")
                txtRdireccion2.Text = row0("Direccion Refenrencia 2")
                txtRtel2.Text = row0("Tel Refencia 2")
            Next
            dt.Rows.Clear()
            con.Close()
            Me.Height = 413
            GridControl1.Visible = False
        Catch ex As Exception
            MsgBox("Error al cargar datos de cliente" & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Try
            con.Open()
            Dim cmd As New SqlCommand("SP_AME_Cliente", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@edicion", edicion)
                .AddWithValue("@codigo", txtcodigo.Text)
                .AddWithValue("@cedula", txtCedula.Text)
                .AddWithValue("@nombre", txtnombre.Text)
                .AddWithValue("@apellido", txtapellido.Text) '---
                .AddWithValue("@direccion1", txtDireccion1.Text) '---
                .AddWithValue("@direccion2", txtDireccion2.Text) '---
                .AddWithValue("@direccion3", txtTdireccion.Text) '---
                .AddWithValue("@tel1", txttelefono1.Text) '---
                .AddWithValue("@tel2", txttelefono2.Text) '---
                .AddWithValue("@tel3", txtTtelefono.Text) '---
                .AddWithValue("@correo1", txtcorreo1.Text) '---
                .AddWithValue("@correo2", txtcorreo2.Text) '---
                .AddWithValue("@correo3", txtTcorreo.Text) '---
                .AddWithValue("@ingreso", txtTingreso.Text)
                .AddWithValue("@limite", txtLimiteCredito.Text) '---
                .AddWithValue("@tipo", cbTipo.SelectedIndex + 1)
                .AddWithValue("@departamento", txtDepartamento.Text) '---
                .AddWithValue("@municipio", txtMunicipio.Text) '---
                .AddWithValue("@distrito", txtDistrito.Text) '---
                .AddWithValue("@barrio", txtBarrio.Text) '---
                .AddWithValue("@fechaI", Convert.ToDateTime(dtpFechaI.Value))
                .AddWithValue("@fechaF", Convert.ToDateTime(dtpFechaF.Value)) '---
                .AddWithValue("@activo", ckbActivo.Checked)
                .AddWithValue("@id_empleado", txtEcodigo.Text) '---
                .AddWithValue("@rnombre1", txtRnombre1.Text) '---   OJO
                .AddWithValue("@rnombre2", txtRnombre2.Text) '---   OJO
                .AddWithValue("@rdireccion1", txtRdireccion1.Text) '---
                .AddWithValue("@rdireccion2", txtRdireccion2.Text) '---
                .AddWithValue("@rtel1", txtRtel1.Text) '---
                .AddWithValue("@rtel2", txtRtel2.Text) '---

            End With
            cmd.ExecuteReader()
            con.Close()
            Select Case edicion
                Case 1
                    MsgBox("Cliente Ingresado")
                Case 2
                    MsgBox("Cliente Actualizado")
                Case 3
                    MsgBox("Cliente Eliminado")
            End Select
            Select Case NumCatalogo
                Case 14 'catalogo cliente
                    frmPrincipal.CargarDatos()
                Case 33 'factura
                    With FrmFactura
                        .txtCcodigo.Text = ""
                        .txtCcodigo.Text = txtcodigo.Text
                        .cbtipo.SelectedIndex = cbTipo.SelectedIndex
                        .txtCnombre.Text = txtnombre.Text
                        .txtCapellido.Text = txtapellido.Text
                        .txtCcartera.Text = cbColector.Text
                        .Guardar()
                    End With
            End Select
            'With FrmFactura
            '    .txtCcodigo.Text = txtcodigo.Text
            '    .cbtipo.SelectedIndex = cbTipo.SelectedIndex
            '    .txtCnombre.Text = txtnombre.Text
            '    .txtCapellido.Text = txtapellido.Text
            '    .txtCcolector.Text = cbColector.Text
            'End With
            'frmPrincipal.BringToFront()
            Me.Close()
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Function UpdateFactura_Cliente(ByVal tipo As Integer, codigo As String, nombre As String, apellido As String, cedula As String, factura As String, serie As String, colector As String)
        con.Open()
        Try
            Dim cmdSP As New SqlCommand("SP_FACTURA_UPDATE", con)
            cmdSP.CommandType = CommandType.StoredProcedure
            With cmdSP.Parameters
                .AddWithValue("@tipo", SqlDbType.Int).Value = tipo
                .AddWithValue("@codigo", SqlDbType.VarChar).Value = codigo
                .AddWithValue("@nombre", SqlDbType.VarChar).Value = nombre
                .AddWithValue("@apellido", SqlDbType.VarChar).Value = apellido
                .AddWithValue("@cedula", SqlDbType.VarChar).Value = cedula
                .AddWithValue("@factura", SqlDbType.VarChar).Value = factura
                .AddWithValue("@serie", SqlDbType.VarChar).Value = serie
                .AddWithValue("@colector", SqlDbType.VarChar).Value = colector
            End With
            cmdSP.ExecuteReader()
            UpdateFactura_Cliente = "OK"
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            UpdateFactura_Cliente = "ERROR"
            con.Close()
            con.Dispose()
        End Try
        con.Close()
        con.Dispose()
    End Function
    Private Sub CargarCodigo()
        Dim dt As DataTable = New DataTable()
        sqlstring = "select isnull(max(cast(Codigo as int))+1,1) as 'Codigo' from Clientes"
        Dim tblRegMax As DataTable = dt
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            con.Close()
            txtcodigo.Text = Microsoft.VisualBasic.Right("00000000" & tblRegMax.Rows(0).Item(0), 8)
        Catch ex As Exception
            con.Close()
        End Try
    End Sub
    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Cerrar()
    End Sub
    Public Sub CargarDatos(ByVal CodigoEntidad As String)
        Dim strsql As String = ""
        strsql = "Select Codigo,Nombre_Cliente,Cedula_Cliente,Direccion1_Cliente,Telefono1_Cliente,Telefono2_Cliente,TelTrabajo_Cliente "
        strsql = strsql & " ,Email1_Cliente, Nombre,Nombre_Municipio,Nombre_Zona_Distrito,Nombre_Barrio,Activo, Fecha_Ingreso,Fecha_Baja from "
        strsql = strsql & " vwVerClientes WHERE Codigo ='" & CodigoEntidad & "'"
        Dim tblDatosClientes As DataTable = SQL(strsql, "tblDatosClientes", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosClientes.Rows.Count <> 0 Then
            txtcodigo.Text = tblDatosClientes.Rows(0).Item(0)
            txtnombre.Text = tblDatosClientes.Rows(0).Item(1)
            txtDireccion1.Text = IIf(IsDBNull(tblDatosClientes.Rows(0).Item(3)), "", tblDatosClientes.Rows(0).Item(3))
            txttelefono1.Text = IIf(IsDBNull(tblDatosClientes.Rows(0).Item(4)), "", tblDatosClientes.Rows(0).Item(4))
            txttelefono2.Text = IIf(IsDBNull(tblDatosClientes.Rows(0).Item(5)), "", tblDatosClientes.Rows(0).Item(5))
        End If

    End Sub
    Private Sub CargarColector()
        Dim dt As DataTable = New DataTable()
        cbColector.Properties.Items.Clear()
        sqlstring = "select distinct Tbl_Asignacion_CobradorSerie.Id_Serie, Tbl_Empleados.Nombres+' '+Tbl_Empleados.Apellidos as 'Nombre' from Tbl_Empleados inner join Tbl_Asignacion_CobradorSerie on Tbl_Asignacion_CobradorSerie.Id_Colector=Tbl_Empleados.Id_Empleado inner join Tbl_Asignacion_BarrioSerie on Tbl_Asignacion_BarrioSerie.Id_Serie=Tbl_Asignacion_CobradorSerie.Id_Serie inner join Tbl_Barrios on Tbl_Asignacion_BarrioSerie.Id_Barrio=Tbl_Barrios.Id_Barrio where Tbl_Barrios.Nombre_Barrio=@barrio"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@barrio", txtBarrio.Text)
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbColector.Properties.Items.Add(row0(1))
            Next
            cbColector.Properties.Items.Add("N/D")
            con.Close()
            cbColector.SelectedIndex = 0
        Catch ex As Exception
            con.Close()
        End Try
    End Sub
    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub
    Private Sub cmdAddDepar_Click(sender As Object, e As EventArgs) Handles btnDepartamento.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 8
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
    End Sub

    Private Sub cmdAddMunic_Click(sender As Object, e As EventArgs) Handles btnMunicipio.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 9
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
    End Sub

    Private Sub cmdAddZonaDist_Click(sender As Object, e As EventArgs) Handles btnDistrito.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 10
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
    End Sub

    Private Sub cmdAddBarrio_Click(sender As Object, e As EventArgs) Handles btnBarrio.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 11
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
    End Sub
    Private Sub ckbActivo_CheckedChanged(sender As Object, e As EventArgs) Handles ckbActivo.CheckedChanged
        If ckbActivo.Checked = True Then
            dtpFechaF.Enabled = False
        Else
            dtpFechaF.Enabled = True
        End If
    End Sub
    Private Sub cbBarrio_TextChanged(sender As Object, e As EventArgs)
        CargarColector()
    End Sub
    Private Sub txtMunicipio_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles txtMunicipio.MouseDoubleClick, txtDistrito.MouseDoubleClick, txtDepartamento.MouseDoubleClick, txtBarrio.MouseDoubleClick
        If GridControl1.Visible = True Then
            Me.Height = 413
            GridControl1.Visible = False
        Else
            Me.Height = 704
            GridControl1.Visible = True
            GridControl1.DataSource = vbNull
            GridView1.Columns.Clear()
            sqlstring = "select Tbl_Barrios.Nombre_Barrio as 'Barrio', Tbl_Zonas_Distritos.Nombre_Zona_Distrito as 'Distrito', Tbl_Municipio.Nombre_Municipio as 'Municipio',catalogo.Departamento.Nombre as 'Departamento' from Catalogo.Departamento right join Tbl_Municipio on Tbl_Municipio.Id_Departamento=Catalogo.Departamento.Id_Departamento  right join Tbl_Zonas_Distritos on Tbl_Zonas_Distritos.Id_Municipio=Tbl_Municipio.Id_Municipio right join Tbl_Barrios on Tbl_Barrios.Id_Zona_Distrito=Tbl_Zonas_Distritos.Id_Zona_Distrito"
            Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
            GridControl1.DataSource = TableDatos
            GridView1.BestFitColumns()
        End If
    End Sub
    Private Sub GridControl1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles GridControl1.MouseDoubleClick
        Try
            With GridView1
                txtDepartamento.Text = .GetRowCellValue(.GetSelectedRows(0), "Departamento").ToString()
                txtMunicipio.Text = .GetRowCellValue(.GetSelectedRows(0), "Municipio").ToString()
                txtDistrito.Text = .GetRowCellValue(.GetSelectedRows(0), "Distrito").ToString()
                txtBarrio.Text = .GetRowCellValue(.GetSelectedRows(0), "Barrio").ToString()
            End With
            Me.Height = 413
            GridControl1.Visible = False
        Catch ex As Exception
        End Try
        Try
            With GridView1
                txtEcodigo.Text = .GetRowCellValue(.GetSelectedRows(0), "Codigo").ToString()
                txtCedula.Text = .GetRowCellValue(.GetSelectedRows(0), "Cedula").ToString()
                txtnombre.Text = .GetRowCellValue(.GetSelectedRows(0), "Nombre").ToString()
                txtapellido.Text = .GetRowCellValue(.GetSelectedRows(0), "Apellido").ToString()
                txtTingreso.Text = String.Format("{0:n2}", .GetRowCellValue(.GetSelectedRows(0), "Salario").ToString())
                txttelefono1.Text = .GetRowCellValue(.GetSelectedRows(0), "Telefono 1").ToString()
                txttelefono2.Text = .GetRowCellValue(.GetSelectedRows(0), "Telefono 2").ToString()

            End With
            Me.Height = 413
            GridControl1.Visible = False
        Catch ex As Exception

        End Try
    End Sub
    Private Sub cbClasificacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipo.SelectedIndexChanged
        If cbTipo.SelectedIndex = 2 Then
            cbTipo.Width = 102
            If GridControl1.Visible = True Then
                Me.Height = 413
                GridControl1.Visible = False
            Else
                Me.Height = 704
                GridControl1.Visible = True
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Id_Empleado as 'Codigo', Num_Identificacion as 'Cedula', Nombres as 'Nombre', Apellidos as 'Apellido', Salario_MensualCS as 'Salario', Telefono_Domiciliar as 'Telefono 1', Telefono_Personal1 as 'Telefono 2' from Tbl_Empleados where Activo=1"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
            End If
        Else
            cbTipo.Width = 254
        End If
    End Sub
    Private Sub TabPrincipal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabPrincipal.SelectedIndexChanged
        If TabPrincipal.SelectedIndex = 1 Then
            Me.Height = 502
        Else
            Me.Height = 413
        End If
    End Sub
    Private Sub frmEditCliente_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Select Case Me.Height
            Case 413 To 502
                GridControl1.Visible = False
            Case 704
                GridControl1.Visible = True
        End Select
    End Sub

    Private Sub txtCedula_TextChanged(sender As Object, e As EventArgs) Handles txtCedula.TextChanged
        If txtCedula.TextLength = 14 Then
            Dim Codigo As String = ""
            Dim Cedula As String = ""
            Dim Nombre As String = ""
            Dim bandera As Boolean = False
            Dim dt As DataTable = New DataTable()
            sqlstring = "select Codigo,Cedula, Nombre+' '+Apellido from Clientes where Cedula='" + txtCedula.Text + "'"
            Try
                con.Open()
                cmd = New SqlCommand(sqlstring, con)
                cmd.ExecuteNonQuery()
                adapter = New SqlDataAdapter(cmd)
                adapter.Fill(dt)
                For Each row0 In dt.Rows
                    If row0(1) <> "" Then
                        Codigo = row0(0)
                        Cedula = row0(1)
                        Nombre = row0(2)
                        bandera = True
                    End If
                Next
                con.Close()
                If bandera = True Then
                    Select Case MsgBox("Cliente EXISTENTE con el mismo numero de CEDULA" & vbNewLine & "" & vbNewLine & "Codigo: " + Codigo + "" & vbNewLine & "Nombre: " + Nombre + "" & vbNewLine & "Cedula: " + Cedula + "" & vbNewLine & "" & vbNewLine & "Desea CARGAR los datos del CLIENTE EXISTENTE?", MsgBoxStyle.YesNo, "")
                        Case MsgBoxResult.Yes
                            edicion = 2
                            txtcodigo.Text = Codigo
                            lbnuevo.Visible = False
                            btnAceptar.Text = "Actualizar"
                            CargaF()
                        Case MsgBoxResult.No
                            txtCedula.SelectAll()
                    End Select
                End If
            Catch ex As Exception
                con.Close()
            End Try
        End If
    End Sub

    Private Sub lbClasificacion_TextChanged(sender As Object, e As EventArgs) Handles lbClasificacion.TextChanged
        With lbClasificacion

            Select Case lbClasificacion.Text

                Case "-"
                    .ForeColor = c_nulo
                Case "A"
                    .ForeColor = c_A
                Case "B"
                    .ForeColor = c_B
                Case "C"
                    .ForeColor = c_C
            End Select

        End With

    End Sub

    Private Sub frmEditCliente_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Cerrar()
    End Sub
End Class