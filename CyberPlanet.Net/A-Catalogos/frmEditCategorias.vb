﻿Imports System.Xml
Public Class frmEditCategorias

    Private Sub frmEditCategorias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarLinea()
        If nTipoEdic = 1 Then
            lblCodCategoria.Visible = True
            txtCodCategoria.Visible = True
            txtNomCategoria.Text = ""
            txtNomCategoria.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodCategoria.Visible = True
            txtCodCategoria.Enabled = False
            txtCodCategoria.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoCateg As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoCateg.EditarCategoria(IIf(txtCodCategoria.Text = "", 0, txtCodCategoria.Text), txtNomCategoria.Text, luLinea.EditValue, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub CargarDatos(ByVal CodigoEntidad As String)
        Dim strsql As String = ""
        Dim Linea As String = ""

        Try
            Dim registro As DataRowView = frmCatalogo.TblCatalogosBS.Current
            Linea = registro.Item("Nombre_Linea")

            strsql = "SELECT Codigo,Nombre_Categoria,Nombre_Linea FROM vwVerCategorias WHERE Codigo='" & CodigoEntidad & "' and Nombre_Linea = '" & Linea & "'"
            Dim tblDatosCateg As DataTable = SQL(strsql, "tblDatosCateg", My.Settings.SolIndustrialCNX).Tables(0)
            If tblDatosCateg.Rows.Count > 0 Then
                luLinea.Text = tblDatosCateg.Rows(0).Item("Nombre_Linea")
                txtCodCategoria.Text = tblDatosCateg.Rows(0).Item("Codigo")
                txtNomCategoria.Text = tblDatosCateg.Rows(0).Item("Nombre_Categoria")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub cargarLinea()
        'TblDepartBindingSource.DataSource = SQL("select Codigo_Linea,Nombre_Linea from Linea_Producto order by Codigo_Linea", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        'luLinea.Properties.DataSource = TblDepartBindingSource
        luLinea.Properties.DataSource = SQL("select Codigo_Linea,Nombre_Linea from Linea_Producto order by Codigo_Linea", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        luLinea.Properties.DisplayMember = "Nombre_Linea"
        luLinea.Properties.ValueMember = "Codigo_Linea"
        luLinea.ItemIndex = 0
    End Sub

    Public Sub Cerrar()
        'Me.Dispose()
        Close()
    End Sub

    Private Sub cmdAddDepar_Click(sender As Object, e As EventArgs) Handles cmdAddDepar.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 4
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarLinea()
    End Sub

    Private Sub luLinea_EditValueChanged(sender As Object, e As EventArgs) Handles luLinea.EditValueChanged
        valTemp = luLinea.EditValue
        txtCodCategoria.Text = Microsoft.VisualBasic.Right("00" & RegistroMaximo(), 2)
    End Sub
End Class