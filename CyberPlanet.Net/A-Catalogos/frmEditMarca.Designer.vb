﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditMarca
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditMarca))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblNomMarca = New System.Windows.Forms.Label()
        Me.txtNomMarca = New System.Windows.Forms.TextBox()
        Me.lblCodMarca = New System.Windows.Forms.Label()
        Me.txtCodMarca = New System.Windows.Forms.TextBox()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblNomMarca)
        Me.Panel2.Controls.Add(Me.txtNomMarca)
        Me.Panel2.Controls.Add(Me.lblCodMarca)
        Me.Panel2.Controls.Add(Me.txtCodMarca)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(323, 99)
        Me.Panel2.TabIndex = 5
        '
        'lblNomMarca
        '
        Me.lblNomMarca.Location = New System.Drawing.Point(26, 57)
        Me.lblNomMarca.Name = "lblNomMarca"
        Me.lblNomMarca.Size = New System.Drawing.Size(103, 20)
        Me.lblNomMarca.TabIndex = 3
        Me.lblNomMarca.Text = "Nombre de Marca:"
        '
        'txtNomMarca
        '
        Me.txtNomMarca.Location = New System.Drawing.Point(135, 57)
        Me.txtNomMarca.Name = "txtNomMarca"
        Me.txtNomMarca.Size = New System.Drawing.Size(173, 20)
        Me.txtNomMarca.TabIndex = 2
        '
        'lblCodMarca
        '
        Me.lblCodMarca.Location = New System.Drawing.Point(26, 24)
        Me.lblCodMarca.Name = "lblCodMarca"
        Me.lblCodMarca.Size = New System.Drawing.Size(80, 20)
        Me.lblCodMarca.TabIndex = 1
        Me.lblCodMarca.Text = "Codigo Marca:"
        '
        'txtCodMarca
        '
        Me.txtCodMarca.Location = New System.Drawing.Point(135, 21)
        Me.txtCodMarca.Name = "txtCodMarca"
        Me.txtCodMarca.ReadOnly = True
        Me.txtCodMarca.Size = New System.Drawing.Size(100, 20)
        Me.txtCodMarca.TabIndex = 0
        Me.txtCodMarca.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(146, 10)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Guardar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 99)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(323, 53)
        Me.Panel1.TabIndex = 4
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(230, 10)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'frmEditMarca
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(323, 152)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditMarca"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblNomMarca As System.Windows.Forms.Label
    Friend WithEvents txtNomMarca As System.Windows.Forms.TextBox
    Friend WithEvents lblCodMarca As System.Windows.Forms.Label
    Friend WithEvents txtCodMarca As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
End Class
