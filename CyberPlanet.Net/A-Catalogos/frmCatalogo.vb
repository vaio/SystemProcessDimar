﻿Public Class frmCatalogo

    Sub AbrirEditorCatalogos()

        If NumCatalogo = 1 Then

            frmEditBodega.ShowDialog()

        ElseIf NumCatalogo = 2 Then

            frmEditMarca.ShowDialog()

        ElseIf NumCatalogo = 3 Then

            frmEditModelo.ShowDialog()

        ElseIf NumCatalogo = 4 Then

            frmEditDepartamento.ShowDialog()

        ElseIf NumCatalogo = 5 Then

            frmEditCategorias.ShowDialog()

        ElseIf NumCatalogo = 6 Then

            frmEditProveedor.ShowDialog()

        ElseIf NumCatalogo = 7 Then

            frmUnidMed.ShowDialog()

        ElseIf NumCatalogo = 8 Then

            frmPresentacion.ShowDialog()

        ElseIf NumCatalogo = 9 Then
            Select Case nTipoEdic
                Case 1

                    frmEditProdServ.ShowDialog()

                Case 2

                    frmEleccion.Show()

                Case 3

                    frmEditProdServ.ShowDialog()

            End Select

        ElseIf NumCatalogo = 10 Then

            frmDepartPais.ShowDialog()

        ElseIf NumCatalogo = 11 Then
            frmEditMunicipio.ShowDialog()
        ElseIf NumCatalogo = 12 Then
            frmEditDistricZona.ShowDialog()
        ElseIf NumCatalogo = 13 Then
            frmEditBarrios.ShowDialog()
        ElseIf NumCatalogo = 14 Then
            frmEditCliente.edicion = nTipoEdic
            Select Case nTipoEdic
                Case 1
                Case 2 To 3
                    frmEditCliente.txtcodigo.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
            End Select
            frmEditCliente.ShowDialog()
        ElseIf NumCatalogo = 15 Then
            frmEditSuministros.ShowDialog()
        ElseIf NumCatalogo = 16 Then
            frmEditActivosFijos.ShowDialog()
        ElseIf NumCatalogo = 17 Then
            frmColaboradores.ShowDialog()
        ElseIf NumCatalogo = 18 Then
            frmEditTipoOperaciones.ShowDialog()
        ElseIf NumCatalogo = 19 Then
            frmTerminales.ShowDialog()
        ElseIf NumCatalogo = 20 Then
            frmRecetas.ShowDialog()
        ElseIf NumCatalogo = 28 Then
            FrmPagos.ShowDialog()
        End If
    End Sub

    Private Sub TblCatalogosBS_CurrentChanged(sender As Object, e As EventArgs) Handles TblCatalogosBS.CurrentChanged
        Try
            If TblCatalogosBS.Count > 0 Then
                frmPrincipal.bbiModificar.Enabled = True
                frmPrincipal.bbiEliminar.Enabled = True
                'btnAnular.Enabled = True
                Dim registro As DataRowView = TblCatalogosBS.Current
                CodigoEntidad = registro.Item("Codigo")
            Else
                frmPrincipal.bbiModificar.Enabled = False
                frmPrincipal.bbiEliminar.Enabled = False
                'btnAnular.Enabled = False
                CodigoEntidad = 0
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frmCatalogo_Leave(sender As Object, e As EventArgs) Handles Me.Leave
        CatalogoVisible = False
        NumCatalogo = 0
        frmPrincipal.bbiExportar.Enabled = False
    End Sub

    Private Sub frmCatalogo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CatalogoVisible = True
        'AcomodarTitulo()
    End Sub

    'Sub AcomodarTitulo()
    '    LblTitulo.Left = (Me.Width / 2) - (LblTitulo.Width / 2)  '(frmPrincipal.bbiNuevo.Width * 4) + 30
    '    LblTitulo.Width = Me.Width - 400 '(frmPrincipal.bbiNuevo.Width * 4) - 220
    '    PictureBox1.Width = Me.Width + 100
    'End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Sub encabezadoProducto()
        GridView1.Columns("Alterno").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        GridView1.Columns("Alterno").DisplayFormat.FormatString = "{0:0000.0000}"

        GridView1.Columns("Costo Promedio").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        GridView1.Columns("Costo Promedio").DisplayFormat.FormatString = "{0:#,###,##0.0000}"

        GridView1.Columns("Costo USS").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        GridView1.Columns("Costo USS").DisplayFormat.FormatString = "{0:#,###,##0.0000}"

        GridView1.BestFitColumns()
    End Sub

    Sub EncabezadosDetDoc()
        If NumCatalogo = 16 Then
            GridView1.Columns.Clear()
            GridView1.OptionsView.ShowFooter = True

            Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Codigo", .Caption = "Codigo", .FieldName = "Codigo", .Width = 6, .Visible = True, .VisibleIndex = 0}
            GridView1.Columns.Add(gc0)
            GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

            Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Descripcion", .Caption = "Descripcion", .FieldName = "Descripcion", .Width = 60, .Visible = True, .VisibleIndex = 1}
            GridView1.Columns.Add(gc1)

            Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Cantidad", .Caption = "Cantidad", .FieldName = "Cantidad", .Width = 10, .Visible = True, .VisibleIndex = 2}
            GridView1.Columns.Add(gc2)

            Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Precio_Compra", .Caption = "Costo_Unitario", .FieldName = "Precio_Compra", .Width = 10, .Visible = True, .VisibleIndex = 3}
            GridView1.Columns.Add(gc3)
            gc3.DisplayFormat.FormatString = "{0:$0.00}"
            gc3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric

            Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Descuento", .Caption = "Descuento", .FieldName = "Descuento", .Width = 10, .Visible = True, .VisibleIndex = 4}
            GridView1.Columns.Add(gc4)
            gc4.DisplayFormat.FormatString = "{0:$0.00}"
            gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            GridView1.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"

            Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Subtotales", .Caption = "Subtotales", .FieldName = "Subtotales", .Width = 10, .Visible = True, .VisibleIndex = 5}
            GridView1.Columns.Add(gc5)
            gc5.DisplayFormat.FormatString = "{0:$0.00}"
            gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns(5).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            GridView1.Columns(5).SummaryItem.DisplayFormat = "{0:$0.00}"

            Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn() With {.Name = "Subtotal_Neto", .Caption = "Subtotal_Neto", .FieldName = "Subtotal_Neto", .Width = 10, .Visible = True, .VisibleIndex = 6}
            GridView1.Columns.Add(gc6)
            gc6.DisplayFormat.FormatString = "{0:$0.00}"
            gc6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns(6).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            GridView1.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"
        End If
    End Sub

End Class