﻿Partial Public Class FrmKit
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub


#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim TileItemElement7 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement6 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement8 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement9 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement10 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.tileBar = New DevExpress.XtraBars.Navigation.TileBar()
        Me.tileBarGroupTables = New DevExpress.XtraBars.Navigation.TileBarGroup()
        Me.ntbNuevo = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.ntbCreados = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.ntbEntrada = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.nFRAME = New DevExpress.XtraBars.Navigation.NavigationFrame()
        Me.customersNavigationPage = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.customersLabelControl = New DevExpress.XtraEditors.LabelControl()
        Me.NPnuevo = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.lbNuevo = New DevExpress.XtraEditors.LabelControl()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.txtM_nombre = New DevExpress.XtraEditors.TextEdit()
        Me.txtM_producto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMmaximo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMcosto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMAexistencia = New DevExpress.XtraEditors.TextEdit()
        Me.txtMAcosto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMcantidad = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Pcontrol = New DevExpress.XtraBars.Navigation.TileNavPane()
        Me.nbAplicar = New DevExpress.XtraBars.Navigation.NavButton()
        Me.nbBorrar = New DevExpress.XtraBars.Navigation.NavButton()
        Me.btnAplicar = New DevExpress.XtraBars.Navigation.NavButton()
        Me.BtnCancel = New DevExpress.XtraBars.Navigation.NavButton()
        Me.btnSalir = New DevExpress.XtraBars.Navigation.NavButton()
        Me.pDato = New DevExpress.XtraEditors.PanelControl()
        Me.txtCosto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtModelo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMarca = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCategoria = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtAlterno = New DevExpress.XtraEditors.TextEdit()
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCodigo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.luProducto = New DevExpress.XtraEditors.LookUpEdit()
        Me.btnCrear = New System.Windows.Forms.Button()
        Me.pBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.btnGuardar = New DevExpress.XtraBars.Navigation.NavButton()
        Me.btnCancelar = New DevExpress.XtraBars.Navigation.NavButton()
        Me.Salir = New DevExpress.XtraBars.Navigation.NavButton()
        Me.TileNavCategory2 = New DevExpress.XtraBars.Navigation.TileNavCategory()
        Me.TileBarItem1 = New DevExpress.XtraBars.Navigation.TileBarItem()
        Me.pMovimiento = New DevExpress.XtraEditors.PanelControl()
        Me.txtTotal = New DevExpress.XtraEditors.TextEdit()
        Me.txtBodega = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCodigoDocumento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTipoMovimiento = New DevExpress.XtraEditors.TextEdit()
        Me.txtTipoDocumento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCodigo_Movimiento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMcostoTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.nFRAME, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.nFRAME.SuspendLayout()
        Me.customersNavigationPage.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NPnuevo.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPage1.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.txtM_nombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtM_producto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMmaximo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMcosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMAexistencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMAcosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMcantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pDato, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDato.SuspendLayout()
        CType(Me.txtCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtModelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCategoria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAlterno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luProducto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBuscar.SuspendLayout()
        CType(Me.pMovimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMovimiento.SuspendLayout()
        CType(Me.txtTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTipoMovimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTipoDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo_Movimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMcostoTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tileBar
        '
        Me.tileBar.AllowDrag = False
        Me.tileBar.AllowGlyphSkinning = True
        Me.tileBar.AllowSelectedItem = True
        Me.tileBar.AppearanceGroupText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(140, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(140, Byte), Integer))
        Me.tileBar.AppearanceGroupText.Options.UseForeColor = True
        Me.tileBar.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.tileBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tileBar.Dock = System.Windows.Forms.DockStyle.Top
        Me.tileBar.DropDownButtonWidth = 30
        Me.tileBar.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.tileBar.Groups.Add(Me.tileBarGroupTables)
        Me.tileBar.IndentBetweenGroups = 10
        Me.tileBar.IndentBetweenItems = 10
        Me.tileBar.ItemPadding = New System.Windows.Forms.Padding(8, 6, 12, 6)
        Me.tileBar.Location = New System.Drawing.Point(0, 0)
        Me.tileBar.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tileBar.MaxId = 5
        Me.tileBar.MaximumSize = New System.Drawing.Size(0, 110)
        Me.tileBar.MinimumSize = New System.Drawing.Size(100, 110)
        Me.tileBar.Name = "tileBar"
        Me.tileBar.Padding = New System.Windows.Forms.Padding(29, 11, 29, 11)
        Me.tileBar.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.None
        Me.tileBar.SelectedItem = Me.ntbCreados
        Me.tileBar.SelectionBorderWidth = 2
        Me.tileBar.SelectionColorMode = DevExpress.XtraBars.Navigation.SelectionColorMode.UseItemBackColor
        Me.tileBar.ShowGroupText = False
        Me.tileBar.Size = New System.Drawing.Size(1341, 110)
        Me.tileBar.TabIndex = 1
        Me.tileBar.Text = "tileBar"
        Me.tileBar.WideTileWidth = 150
        '
        'tileBarGroupTables
        '
        Me.tileBarGroupTables.Items.Add(Me.ntbNuevo)
        Me.tileBarGroupTables.Items.Add(Me.ntbCreados)
        Me.tileBarGroupTables.Items.Add(Me.ntbEntrada)
        Me.tileBarGroupTables.Name = "tileBarGroupTables"
        Me.tileBarGroupTables.Text = "TABLES"
        '
        'ntbNuevo
        '
        Me.ntbNuevo.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(115, Byte), Integer), CType(CType(196, Byte), Integer))
        Me.ntbNuevo.AppearanceItem.Normal.Options.UseBackColor = True
        Me.ntbNuevo.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement7.ImageUri.Uri = "Add;Size32x32;GrayScaled"
        TileItemElement7.Text = "Crear / Modificar Kits"
        Me.ntbNuevo.Elements.Add(TileItemElement7)
        Me.ntbNuevo.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.[Default]
        Me.ntbNuevo.Name = "ntbNuevo"
        '
        'ntbCreados
        '
        Me.ntbCreados.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement6.ImageUri.Uri = "Cube;Size32x32;GrayScaled"
        TileItemElement6.Text = "Kits Creados"
        Me.ntbCreados.Elements.Add(TileItemElement6)
        Me.ntbCreados.Id = 2
        Me.ntbCreados.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.[Default]
        Me.ntbCreados.Name = "ntbCreados"
        '
        'ntbEntrada
        '
        Me.ntbEntrada.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement8.ImageUri.Uri = "Recurrence;Size32x32;GrayScaled"
        TileItemElement8.Text = "Entrada / Salida"
        Me.ntbEntrada.Elements.Add(TileItemElement8)
        Me.ntbEntrada.Id = 4
        Me.ntbEntrada.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.[Default]
        Me.ntbEntrada.Name = "ntbEntrada"
        '
        'nFRAME
        '
        Me.nFRAME.Controls.Add(Me.customersNavigationPage)
        Me.nFRAME.Controls.Add(Me.NPnuevo)
        Me.nFRAME.Controls.Add(Me.NavigationPage1)
        Me.nFRAME.Dock = System.Windows.Forms.DockStyle.Fill
        Me.nFRAME.Location = New System.Drawing.Point(0, 110)
        Me.nFRAME.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.nFRAME.Name = "nFRAME"
        Me.nFRAME.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NPnuevo, Me.customersNavigationPage, Me.NavigationPage1})
        Me.nFRAME.SelectedPage = Me.NPnuevo
        Me.nFRAME.Size = New System.Drawing.Size(1341, 499)
        Me.nFRAME.TabIndex = 0
        Me.nFRAME.Text = "navigationFrame"
        '
        'customersNavigationPage
        '
        Me.customersNavigationPage.Caption = "customersNavigationPage"
        Me.customersNavigationPage.Controls.Add(Me.GridControl2)
        Me.customersNavigationPage.Controls.Add(Me.customersLabelControl)
        Me.customersNavigationPage.Name = "customersNavigationPage"
        Me.customersNavigationPage.Size = New System.Drawing.Size(1341, 499)
        '
        'GridControl2
        '
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.Location = New System.Drawing.Point(0, 0)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(1341, 499)
        Me.GridControl2.TabIndex = 4
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        '
        'customersLabelControl
        '
        Me.customersLabelControl.Appearance.Font = New System.Drawing.Font("Tahoma", 25.25!)
        Me.customersLabelControl.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.customersLabelControl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.customersLabelControl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.customersLabelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.customersLabelControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.customersLabelControl.Location = New System.Drawing.Point(0, 0)
        Me.customersLabelControl.Name = "customersLabelControl"
        Me.customersLabelControl.Size = New System.Drawing.Size(1341, 499)
        Me.customersLabelControl.TabIndex = 2
        Me.customersLabelControl.Text = "Customers"
        '
        'NPnuevo
        '
        Me.NPnuevo.Caption = "NPnuevo"
        Me.NPnuevo.Controls.Add(Me.GridControl3)
        Me.NPnuevo.Controls.Add(Me.GridControl1)
        Me.NPnuevo.Controls.Add(Me.lbNuevo)
        Me.NPnuevo.Name = "NPnuevo"
        Me.NPnuevo.Size = New System.Drawing.Size(1341, 499)
        '
        'GridControl3
        '
        Me.GridControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl3.Location = New System.Drawing.Point(703, 18)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(494, 413)
        Me.GridControl3.TabIndex = 179
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.GroupPanelText = "Productos Disponibles"
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.Editable = False
        Me.GridView3.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView3.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView3.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView3.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView3.OptionsView.ShowFooter = True
        Me.GridView3.PaintStyleName = "UltraFlat"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(143, 18)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(496, 413)
        Me.GridControl1.TabIndex = 178
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.GroupPanelText = "Productos Disponibles"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.PaintStyleName = "UltraFlat"
        '
        'lbNuevo
        '
        Me.lbNuevo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbNuevo.Appearance.Font = New System.Drawing.Font("Tahoma", 25.25!)
        Me.lbNuevo.Appearance.ForeColor = System.Drawing.Color.Gray
        Me.lbNuevo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbNuevo.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.lbNuevo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbNuevo.Location = New System.Drawing.Point(566, 56)
        Me.lbNuevo.Name = "lbNuevo"
        Me.lbNuevo.Size = New System.Drawing.Size(265, 232)
        Me.lbNuevo.TabIndex = 2
        Me.lbNuevo.Text = "NUEVO KIT"
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Controls.Add(Me.PanelControl1)
        Me.NavigationPage1.Controls.Add(Me.GridControl5)
        Me.NavigationPage1.Controls.Add(Me.GridControl4)
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.Size = New System.Drawing.Size(1341, 499)
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.Controls.Add(Me.txtMcostoTotal)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.txtM_nombre)
        Me.PanelControl1.Controls.Add(Me.txtM_producto)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.txtMmaximo)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Controls.Add(Me.txtMcosto)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.txtMAexistencia)
        Me.PanelControl1.Controls.Add(Me.txtMAcosto)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.txtMcantidad)
        Me.PanelControl1.Controls.Add(Me.LabelControl20)
        Me.PanelControl1.Location = New System.Drawing.Point(703, 317)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(496, 124)
        Me.PanelControl1.TabIndex = 181
        '
        'txtM_nombre
        '
        Me.txtM_nombre.Location = New System.Drawing.Point(152, 18)
        Me.txtM_nombre.Name = "txtM_nombre"
        Me.txtM_nombre.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtM_nombre.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtM_nombre.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtM_nombre.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtM_nombre.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtM_nombre.Properties.ReadOnly = True
        Me.txtM_nombre.Size = New System.Drawing.Size(309, 20)
        Me.txtM_nombre.TabIndex = 15
        '
        'txtM_producto
        '
        Me.txtM_producto.Location = New System.Drawing.Point(83, 18)
        Me.txtM_producto.Name = "txtM_producto"
        Me.txtM_producto.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtM_producto.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtM_producto.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtM_producto.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtM_producto.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtM_producto.Properties.ReadOnly = True
        Me.txtM_producto.Size = New System.Drawing.Size(63, 20)
        Me.txtM_producto.TabIndex = 14
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(37, 21)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl9.TabIndex = 13
        Me.LabelControl9.Text = "Articulo:"
        '
        'txtMmaximo
        '
        Me.txtMmaximo.Location = New System.Drawing.Point(152, 44)
        Me.txtMmaximo.Name = "txtMmaximo"
        Me.txtMmaximo.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMmaximo.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMmaximo.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMmaximo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMmaximo.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMmaximo.Properties.ReadOnly = True
        Me.txtMmaximo.Size = New System.Drawing.Size(51, 20)
        Me.txtMmaximo.TabIndex = 12
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(248, 47)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl17.TabIndex = 0
        Me.LabelControl17.Text = "Cantidad Actual:"
        '
        'txtMcosto
        '
        Me.txtMcosto.Location = New System.Drawing.Point(83, 70)
        Me.txtMcosto.Name = "txtMcosto"
        Me.txtMcosto.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMcosto.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMcosto.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMcosto.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMcosto.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMcosto.Properties.ReadOnly = True
        Me.txtMcosto.Size = New System.Drawing.Size(120, 20)
        Me.txtMcosto.TabIndex = 11
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(45, 73)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl18.TabIndex = 10
        Me.LabelControl18.Text = "Costo:"
        '
        'txtMAexistencia
        '
        Me.txtMAexistencia.Location = New System.Drawing.Point(335, 44)
        Me.txtMAexistencia.Name = "txtMAexistencia"
        Me.txtMAexistencia.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMAexistencia.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMAexistencia.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMAexistencia.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMAexistencia.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMAexistencia.Properties.ReadOnly = True
        Me.txtMAexistencia.Size = New System.Drawing.Size(126, 20)
        Me.txtMAexistencia.TabIndex = 1
        '
        'txtMAcosto
        '
        Me.txtMAcosto.Location = New System.Drawing.Point(335, 70)
        Me.txtMAcosto.Name = "txtMAcosto"
        Me.txtMAcosto.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMAcosto.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMAcosto.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMAcosto.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMAcosto.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMAcosto.Properties.ReadOnly = True
        Me.txtMAcosto.Size = New System.Drawing.Size(126, 20)
        Me.txtMAcosto.TabIndex = 9
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(263, 73)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl19.TabIndex = 8
        Me.LabelControl19.Text = "Costo Actual:"
        '
        'txtMcantidad
        '
        Me.txtMcantidad.Location = New System.Drawing.Point(83, 44)
        Me.txtMcantidad.Name = "txtMcantidad"
        Me.txtMcantidad.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMcantidad.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMcantidad.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMcantidad.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMcantidad.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMcantidad.Properties.MaxLength = 8
        Me.txtMcantidad.Size = New System.Drawing.Size(63, 20)
        Me.txtMcantidad.TabIndex = 5
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(30, 47)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl20.TabIndex = 4
        Me.LabelControl20.Text = "Cantidad:"
        '
        'GridControl5
        '
        Me.GridControl5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl5.Location = New System.Drawing.Point(703, 36)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(496, 244)
        Me.GridControl5.TabIndex = 180
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.GroupPanelText = "Productos Disponibles"
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView5.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView5.OptionsBehavior.Editable = False
        Me.GridView5.OptionsCustomization.AllowFilter = False
        Me.GridView5.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView5.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView5.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView5.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden
        Me.GridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView5.OptionsView.ShowFooter = True
        Me.GridView5.PaintStyleName = "UltraFlat"
        '
        'GridControl4
        '
        Me.GridControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl4.Location = New System.Drawing.Point(143, 36)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(496, 413)
        Me.GridControl4.TabIndex = 179
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.GroupPanelText = "Productos Disponibles"
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView4.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView4.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView4.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden
        Me.GridView4.OptionsView.ShowAutoFilterRow = True
        Me.GridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView4.OptionsView.ShowFooter = True
        Me.GridView4.PaintStyleName = "UltraFlat"
        '
        'Pcontrol
        '
        Me.Pcontrol.ButtonPadding = New System.Windows.Forms.Padding(12)
        Me.Pcontrol.Buttons.Add(Me.nbAplicar)
        Me.Pcontrol.Buttons.Add(Me.nbBorrar)
        '
        'TileNavCategory1
        '
        Me.Pcontrol.DefaultCategory.Name = "TileNavCategory1"
        Me.Pcontrol.DefaultCategory.OptionsDropDown.BackColor = System.Drawing.Color.Empty
        Me.Pcontrol.DefaultCategory.OwnerCollection = Nothing
        '
        '
        '
        Me.Pcontrol.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        Me.Pcontrol.DefaultCategory.Tile.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.[Default]
        Me.Pcontrol.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Pcontrol.Location = New System.Drawing.Point(0, 569)
        Me.Pcontrol.Name = "Pcontrol"
        Me.Pcontrol.OptionsPrimaryDropDown.BackColor = System.Drawing.Color.Empty
        Me.Pcontrol.OptionsSecondaryDropDown.BackColor = System.Drawing.Color.Empty
        Me.Pcontrol.Size = New System.Drawing.Size(1341, 40)
        Me.Pcontrol.TabIndex = 180
        Me.Pcontrol.Text = "TileNavPane1"
        '
        'nbAplicar
        '
        Me.nbAplicar.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.nbAplicar.Caption = "Aplicar"
        Me.nbAplicar.Name = "nbAplicar"
        '
        'nbBorrar
        '
        Me.nbBorrar.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.nbBorrar.Caption = "Borrar"
        Me.nbBorrar.Name = "nbBorrar"
        '
        'btnAplicar
        '
        Me.btnAplicar.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.btnAplicar.Caption = "Aplicar"
        Me.btnAplicar.Name = "btnAplicar"
        '
        'BtnCancel
        '
        Me.BtnCancel.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.BtnCancel.Caption = "Cancelar"
        Me.BtnCancel.Name = "BtnCancel"
        '
        'btnSalir
        '
        Me.btnSalir.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.btnSalir.Caption = "Salir"
        Me.btnSalir.Name = "btnSalir"
        '
        'pDato
        '
        Me.pDato.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pDato.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.pDato.Appearance.Options.UseBackColor = True
        Me.pDato.Controls.Add(Me.txtCosto)
        Me.pDato.Controls.Add(Me.LabelControl6)
        Me.pDato.Controls.Add(Me.txtModelo)
        Me.pDato.Controls.Add(Me.LabelControl7)
        Me.pDato.Controls.Add(Me.txtMarca)
        Me.pDato.Controls.Add(Me.LabelControl5)
        Me.pDato.Controls.Add(Me.txtCategoria)
        Me.pDato.Controls.Add(Me.LabelControl2)
        Me.pDato.Controls.Add(Me.txtAlterno)
        Me.pDato.Controls.Add(Me.txtNombre)
        Me.pDato.Controls.Add(Me.LabelControl4)
        Me.pDato.Controls.Add(Me.LabelControl1)
        Me.pDato.Controls.Add(Me.txtCodigo)
        Me.pDato.Controls.Add(Me.LabelControl3)
        Me.pDato.Location = New System.Drawing.Point(509, 10)
        Me.pDato.Name = "pDato"
        Me.pDato.Size = New System.Drawing.Size(820, 92)
        Me.pDato.TabIndex = 4
        Me.pDato.Visible = False
        '
        'txtCosto
        '
        Me.txtCosto.Location = New System.Drawing.Point(666, 47)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtCosto.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtCosto.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtCosto.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtCosto.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtCosto.Properties.ReadOnly = True
        Me.txtCosto.Size = New System.Drawing.Size(126, 20)
        Me.txtCosto.TabIndex = 15
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(623, 50)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl6.TabIndex = 14
        Me.LabelControl6.Text = "Costo:"
        '
        'txtModelo
        '
        Me.txtModelo.Location = New System.Drawing.Point(666, 21)
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtModelo.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtModelo.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtModelo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtModelo.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtModelo.Properties.ReadOnly = True
        Me.txtModelo.Size = New System.Drawing.Size(126, 20)
        Me.txtModelo.TabIndex = 13
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(623, 24)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Modelo:"
        '
        'txtMarca
        '
        Me.txtMarca.Location = New System.Drawing.Point(465, 47)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMarca.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMarca.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMarca.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMarca.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMarca.Properties.ReadOnly = True
        Me.txtMarca.Size = New System.Drawing.Size(126, 20)
        Me.txtMarca.TabIndex = 11
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(426, 50)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Marca:"
        '
        'txtCategoria
        '
        Me.txtCategoria.Location = New System.Drawing.Point(465, 21)
        Me.txtCategoria.Name = "txtCategoria"
        Me.txtCategoria.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtCategoria.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtCategoria.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtCategoria.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtCategoria.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtCategoria.Properties.ReadOnly = True
        Me.txtCategoria.Size = New System.Drawing.Size(126, 20)
        Me.txtCategoria.TabIndex = 9
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(408, 24)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl2.TabIndex = 8
        Me.LabelControl2.Text = "Categoria:"
        '
        'txtAlterno
        '
        Me.txtAlterno.Location = New System.Drawing.Point(254, 47)
        Me.txtAlterno.Name = "txtAlterno"
        Me.txtAlterno.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtAlterno.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtAlterno.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtAlterno.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtAlterno.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtAlterno.Properties.ReadOnly = True
        Me.txtAlterno.Size = New System.Drawing.Size(126, 20)
        Me.txtAlterno.TabIndex = 7
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(66, 21)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtNombre.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtNombre.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtNombre.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtNombre.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtNombre.Properties.ReadOnly = True
        Me.txtNombre.Size = New System.Drawing.Size(314, 20)
        Me.txtNombre.TabIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(209, 50)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Alterno:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(20, 24)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Articulo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(66, 47)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtCodigo.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtCodigo.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtCodigo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtCodigo.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtCodigo.Properties.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(126, 20)
        Me.txtCodigo.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(23, 50)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Codigo:"
        '
        'luProducto
        '
        Me.luProducto.Location = New System.Drawing.Point(73, 21)
        Me.luProducto.Name = "luProducto"
        Me.luProducto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luProducto.Size = New System.Drawing.Size(459, 20)
        Me.luProducto.TabIndex = 4
        '
        'btnCrear
        '
        Me.btnCrear.Location = New System.Drawing.Point(73, 47)
        Me.btnCrear.Name = "btnCrear"
        Me.btnCrear.Size = New System.Drawing.Size(136, 23)
        Me.btnCrear.TabIndex = 5
        Me.btnCrear.Text = "CREAR"
        Me.btnCrear.UseVisualStyleBackColor = True
        '
        'pBuscar
        '
        Me.pBuscar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pBuscar.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.pBuscar.Appearance.Options.UseBackColor = True
        Me.pBuscar.Controls.Add(Me.LabelControl14)
        Me.pBuscar.Controls.Add(Me.btnCrear)
        Me.pBuscar.Controls.Add(Me.luProducto)
        Me.pBuscar.Location = New System.Drawing.Point(509, 10)
        Me.pBuscar.Name = "pBuscar"
        Me.pBuscar.Size = New System.Drawing.Size(820, 92)
        Me.pBuscar.TabIndex = 16
        Me.pBuscar.Visible = False
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(27, 24)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl14.TabIndex = 0
        Me.LabelControl14.Text = "Articulo:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.btnGuardar.Caption = "Aplicar"
        Me.btnGuardar.Name = "btnGuardar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.btnCancelar.Caption = "Cancelar"
        Me.btnCancelar.Name = "btnCancelar"
        '
        'Salir
        '
        Me.Salir.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right
        Me.Salir.Caption = "Salir"
        Me.Salir.Name = "Salir"
        '
        'TileNavCategory2
        '
        Me.TileNavCategory2.Caption = "TileNavCategory3"
        Me.TileNavCategory2.Name = "TileNavCategory2"
        Me.TileNavCategory2.OptionsDropDown.BackColor = System.Drawing.Color.Empty
        Me.TileNavCategory2.OwnerCollection = Nothing
        '
        '
        '
        Me.TileNavCategory2.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement9.Text = "TileNavCategory3"
        Me.TileNavCategory2.Tile.Elements.Add(TileItemElement9)
        Me.TileNavCategory2.Tile.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.[Default]
        Me.TileNavCategory2.Tile.Name = "TileBarItem2"
        '
        'TileBarItem1
        '
        Me.TileBarItem1.DropDownOptions.BeakColor = System.Drawing.Color.Empty
        TileItemElement10.Text = "TileNavCategory3"
        Me.TileBarItem1.Elements.Add(TileItemElement10)
        Me.TileBarItem1.ItemSize = DevExpress.XtraBars.Navigation.TileBarItemSize.[Default]
        Me.TileBarItem1.Name = "TileBarItem1"
        '
        'pMovimiento
        '
        Me.pMovimiento.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pMovimiento.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.pMovimiento.Appearance.Options.UseBackColor = True
        Me.pMovimiento.Controls.Add(Me.txtTotal)
        Me.pMovimiento.Controls.Add(Me.txtBodega)
        Me.pMovimiento.Controls.Add(Me.LabelControl12)
        Me.pMovimiento.Controls.Add(Me.LabelControl8)
        Me.pMovimiento.Controls.Add(Me.LabelControl13)
        Me.pMovimiento.Controls.Add(Me.txtCodigoDocumento)
        Me.pMovimiento.Controls.Add(Me.LabelControl10)
        Me.pMovimiento.Controls.Add(Me.txtTipoMovimiento)
        Me.pMovimiento.Controls.Add(Me.txtTipoDocumento)
        Me.pMovimiento.Controls.Add(Me.LabelControl11)
        Me.pMovimiento.Controls.Add(Me.txtCodigo_Movimiento)
        Me.pMovimiento.Controls.Add(Me.LabelControl15)
        Me.pMovimiento.Location = New System.Drawing.Point(509, 10)
        Me.pMovimiento.Name = "pMovimiento"
        Me.pMovimiento.Size = New System.Drawing.Size(820, 92)
        Me.pMovimiento.TabIndex = 5
        Me.pMovimiento.Visible = False
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(554, 47)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtTotal.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtTotal.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtTotal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtTotal.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtTotal.Properties.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(125, 20)
        Me.txtTotal.TabIndex = 15
        '
        'txtBodega
        '
        Me.txtBodega.Location = New System.Drawing.Point(554, 21)
        Me.txtBodega.Name = "txtBodega"
        Me.txtBodega.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtBodega.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtBodega.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtBodega.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtBodega.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtBodega.Properties.ReadOnly = True
        Me.txtBodega.Size = New System.Drawing.Size(125, 20)
        Me.txtBodega.TabIndex = 7
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(508, 24)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl12.TabIndex = 6
        Me.LabelControl12.Text = "Bodega:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(489, 50)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl8.TabIndex = 14
        Me.LabelControl8.Text = "Costo Total:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(238, 24)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl13.TabIndex = 0
        Me.LabelControl13.Text = "Tipo Movimiento:"
        '
        'txtCodigoDocumento
        '
        Me.txtCodigoDocumento.Location = New System.Drawing.Point(126, 47)
        Me.txtCodigoDocumento.Name = "txtCodigoDocumento"
        Me.txtCodigoDocumento.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtCodigoDocumento.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtCodigoDocumento.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtCodigoDocumento.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtCodigoDocumento.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtCodigoDocumento.Properties.ReadOnly = True
        Me.txtCodigoDocumento.Size = New System.Drawing.Size(73, 20)
        Me.txtCodigoDocumento.TabIndex = 11
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(26, 50)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl10.TabIndex = 10
        Me.LabelControl10.Text = "Codigo Documento:"
        '
        'txtTipoMovimiento
        '
        Me.txtTipoMovimiento.Location = New System.Drawing.Point(325, 21)
        Me.txtTipoMovimiento.Name = "txtTipoMovimiento"
        Me.txtTipoMovimiento.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtTipoMovimiento.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtTipoMovimiento.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtTipoMovimiento.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtTipoMovimiento.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtTipoMovimiento.Properties.ReadOnly = True
        Me.txtTipoMovimiento.Size = New System.Drawing.Size(126, 20)
        Me.txtTipoMovimiento.TabIndex = 1
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.Location = New System.Drawing.Point(325, 47)
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtTipoDocumento.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtTipoDocumento.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtTipoDocumento.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtTipoDocumento.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtTipoDocumento.Properties.ReadOnly = True
        Me.txtTipoDocumento.Size = New System.Drawing.Size(126, 20)
        Me.txtTipoDocumento.TabIndex = 9
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(238, 50)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl11.TabIndex = 8
        Me.LabelControl11.Text = "Tipo Documento:"
        '
        'txtCodigo_Movimiento
        '
        Me.txtCodigo_Movimiento.Location = New System.Drawing.Point(126, 21)
        Me.txtCodigo_Movimiento.Name = "txtCodigo_Movimiento"
        Me.txtCodigo_Movimiento.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtCodigo_Movimiento.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtCodigo_Movimiento.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtCodigo_Movimiento.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtCodigo_Movimiento.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtCodigo_Movimiento.Properties.ReadOnly = True
        Me.txtCodigo_Movimiento.Size = New System.Drawing.Size(73, 20)
        Me.txtCodigo_Movimiento.TabIndex = 5
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(26, 24)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl15.TabIndex = 4
        Me.LabelControl15.Text = "Codigo Movimiento:"
        '
        'txtMcostoTotal
        '
        Me.txtMcostoTotal.Location = New System.Drawing.Point(335, 96)
        Me.txtMcostoTotal.Name = "txtMcostoTotal"
        Me.txtMcostoTotal.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txtMcostoTotal.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White
        Me.txtMcostoTotal.Properties.AppearanceReadOnly.BorderColor = System.Drawing.Color.White
        Me.txtMcostoTotal.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txtMcostoTotal.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.txtMcostoTotal.Properties.ReadOnly = True
        Me.txtMcostoTotal.Size = New System.Drawing.Size(126, 20)
        Me.txtMcostoTotal.TabIndex = 17
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(270, 99)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl16.TabIndex = 16
        Me.LabelControl16.Text = "Costo Total:"
        '
        'FrmKit
        '
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1341, 609)
        Me.Controls.Add(Me.Pcontrol)
        Me.Controls.Add(Me.nFRAME)
        Me.Controls.Add(Me.pBuscar)
        Me.Controls.Add(Me.tileBar)
        Me.Controls.Add(Me.pMovimiento)
        Me.Controls.Add(Me.pDato)
        Me.Name = "FrmKit"
        CType(Me.nFRAME, System.ComponentModel.ISupportInitialize).EndInit()
        Me.nFRAME.ResumeLayout(False)
        Me.customersNavigationPage.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NPnuevo.ResumeLayout(False)
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPage1.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.txtM_nombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtM_producto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMmaximo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMcosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMAexistencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMAcosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMcantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pDato, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDato.ResumeLayout(False)
        Me.pDato.PerformLayout()
        CType(Me.txtCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtModelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCategoria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAlterno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luProducto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBuscar.ResumeLayout(False)
        Me.pBuscar.PerformLayout()
        CType(Me.pMovimiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMovimiento.ResumeLayout(False)
        Me.pMovimiento.PerformLayout()
        CType(Me.txtTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTipoMovimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTipoDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo_Movimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMcostoTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents tileBar As DevExpress.XtraBars.Navigation.TileBar
    Private nFRAME As DevExpress.XtraBars.Navigation.NavigationFrame
    Private tileBarGroupTables As DevExpress.XtraBars.Navigation.TileBarGroup
    Private ntbNuevo As DevExpress.XtraBars.Navigation.TileBarItem
    Private ntbCreados As DevExpress.XtraBars.Navigation.TileBarItem
    Private NPnuevo As DevExpress.XtraBars.Navigation.NavigationPage
    Private customersNavigationPage As DevExpress.XtraBars.Navigation.NavigationPage
    Private lbNuevo As DevExpress.XtraEditors.LabelControl
    Private customersLabelControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents pDato As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtAlterno As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtModelo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMarca As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCategoria As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents luProducto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btnCrear As Button
    Friend WithEvents pBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnGuardar As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents btnCancelar As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents Salir As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents TileNavCategory2 As DevExpress.XtraBars.Navigation.TileNavCategory
    Friend WithEvents TileBarItem1 As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents btnAplicar As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents BtnCancel As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents btnSalir As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Private WithEvents ntbEntrada As DevExpress.XtraBars.Navigation.TileBarItem
    Friend WithEvents Pcontrol As DevExpress.XtraBars.Navigation.TileNavPane
    Friend WithEvents nbAplicar As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents nbBorrar As DevExpress.XtraBars.Navigation.NavButton
    Friend WithEvents pMovimiento As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBodega As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCodigoDocumento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTipoMovimiento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtTipoDocumento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCodigo_Movimiento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMcosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMAexistencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtMAcosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMcantidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtMmaximo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtM_nombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtM_producto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMcostoTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
End Class
