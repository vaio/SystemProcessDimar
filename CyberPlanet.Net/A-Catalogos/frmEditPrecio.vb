﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmEditPrecio
    Dim cmd As New SqlCommand
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Dim D1 As Decimal = 0
    Dim D2 As Decimal = 0
    Dim M1 As Decimal = 0
    Dim M2 As Decimal = 0
    Dim F1 As Decimal = 0
    Dim F2 As Decimal = 0
    Dim Costo As Decimal = 0
    Dim Porcentaje As Decimal = 0
    Dim Tasa As Decimal = 0
    Dim edicion As Integer
    Dim edicion2 As Integer
    Dim Usuario As String
    Private Sub frmEditPrecio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Carga()
        Carga_Detalle()
    End Sub
    Private Sub Carga()
        Try
            sqlstring = "SELECT Codigo,Codigo_Alterno, Nombre_Producto,Nombre_Generico,Tipo_Articulo, Costo_Promedio FROM vwVerProductos where Codigo='" + CodigoEntidad + "'"
            Dim tblDatosProdServ As DataTable = SQL(sqlstring, "tblDatosProdServ", My.Settings.SolIndustrialCNX).Tables(0)
            If tblDatosProdServ.Rows.Count <> 0 Then
                txtCodigo.Text = tblDatosProdServ.Rows(0).Item(0)
                txtAlterno.Text = tblDatosProdServ.Rows(0).Item(1)
                txtProducto.Text = tblDatosProdServ.Rows(0).Item(2)
                txtGenerico.Text = tblDatosProdServ.Rows(0).Item(3)
                cbTipo.SelectedIndex = tblDatosProdServ.Rows(0).Item(4)
                txtCostoCS.Text = String.Format("{0:n2}", tblDatosProdServ.Rows(0).Item(5))
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub txtCodigo_TextChanged(sender As Object, e As EventArgs) Handles txtCodigo.TextChanged
    End Sub
    Sub Carga_Detalle()
        Dim dt As DataTable = New DataTable()
        'sqlstring = "select * from vw_Precio_Detalle where Producto='" + txtCodigo.Text + "' order by Fecha desc"
        sqlstring = "select distinct P.Fecha, p.Id_Precio, p.Id_Producto as 'Producto', p1.Precio as 'Detalle', p1.Id_Detalle as 'D1', (p2.Precio / tc.Tasa_Oficial) as 'Mayorista', p2.Id_Detalle as 'D2', p3.Precio as 'Facturacion', p3.Id_Detalle as 'D3' from Tbl_Precio_Detalle PD inner join Tbl_Precio P on P.Id_Precio=PD.Id_Precio inner join Tbl_Precio_Detalle P1 on P1.Id_Precio=P.Id_Precio and p1.Tipo=1 inner join Tbl_Precio_Detalle P2 on P2.Id_Precio=P.Id_Precio and p2.Tipo=2 inner join Tbl_Precio_Detalle P3 on P3.Id_Precio=P.Id_Precio and p3.Tipo=3 inner join Tbl_TasasCambio TC on TC.Fecha=P.Fecha where P.Id_Producto='" + txtCodigo.Text + "' order by Fecha desc"


        Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
        GridControl1.DataSource = TableDatos
        GridView1.BestFitColumns()
        GridView1.Columns("Id_Precio").Visible = False
        GridView1.Columns("Producto").Visible = False
        GridView1.Columns("D1").Visible = False
        GridView1.Columns("D2").Visible = False
        GridView1.Columns("D3").Visible = False

    End Sub
    Private Sub GridControl1_MouseClick(sender As Object, e As MouseEventArgs) Handles GridControl1.MouseClick
        Try
            If GridView1.RowCount > 0 Then
                With GridView1
                    lbCodigo.Text = .GetRowCellValue(.GetSelectedRows(0), "Id_Precio").ToString()
                    txtDetelle1.Text = String.Format("{0:n2}", .GetRowCellValue(.GetSelectedRows(0), "Detalle").ToString)
                    lbD1.Text = .GetRowCellValue(.GetSelectedRows(0), "D1").ToString
                    txtMayorista2.Text = String.Format("{0:n2}", .GetRowCellValue(.GetSelectedRows(0), "Mayorista").ToString)
                    lbD2.Text = .GetRowCellValue(.GetSelectedRows(0), "D2").ToString
                    txtFacturacion1.Text = String.Format("{0:n2}", .GetRowCellValue(.GetSelectedRows(0), "Facturacion").ToString)
                    lbD3.Text = .GetRowCellValue(.GetSelectedRows(0), "D3").ToString
                    dtpFecha.Text = .GetRowCellValue(.GetSelectedRows(0), "Fecha").ToString
                    'txtOficial.Text = .GetRowCellValue(.GetSelectedRows(0), "Tasa Oficial").ToString
                    'txtParalela.Text = .GetRowCellValue(.GetSelectedRows(0), "Tasa Paralela").ToString
                End With
                ckbIVA1.Checked = False
                ckbIVA2.Checked = False
                ckbIVA3.Checked = False
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If btnEditar.Text = "&Editar" Then
            If GridView1.SelectedRowsCount > 0 Then
                pDetalle.Enabled = True
                txtDetelle1.Focus()
                btnNuevo.Enabled = False
                btnEditar.Text = "&Aceptar"
                btnCancelar.Text = "&Cancelar"
                Usuario = frmPrincipal.LblNombreUsuario.Text
                edicion = 1
                edicion2 = 1
            Else
                MsgBox("No ha seleccionado registro")
            End If
        ElseIf btnEditar.Text = "&Cancelar" Then
            clear()
        Else
            If txtDetelle1.Text > 0 Then
                'Try
                '    clear()
                'Catch ex As Exception
                '    MsgBox("Error: " + ex.Message)
                'End Try
            End If
            'edicion = 2
            Guardar()
            clear()
            btnNuevo.Enabled = True
        End If
    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        If btnNuevo.Text = "&Aceptar" Then
            edicion = 0
            edicion2 = 0
            Guardar()
        Else
            clear()
            CargaI()
            Usuario = frmPrincipal.LblNombreUsuario.Text
            pDetalle.Enabled = True
            With GridView1
                txtDetelle1.Focus()
                ckbActivo.Checked = True
                dtpFecha.Enabled = True
                btnNuevo.Text = "&Aceptar"
                btnEditar.Text = "&Cancelar"
            End With
        End If
    End Sub
    Sub CargaI()
        Dim dt As DataTable = New DataTable()
        sqlstring = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(Id_Precio as int))+1,1))),8)as 'Codigo' from Tbl_Precio"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                lbCodigo.Text = row0(0)
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Guardar()
        Try
            con.Open()
            Dim cmd As New SqlCommand("SP_Precio", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@edicion", Edicion)
                .AddWithValue("@codigo", lbCodigo.Text)
                .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Value))
                .AddWithValue("@producto", txtCodigo.Text)
                .AddWithValue("@activo", ckbActivo.Checked)
                .AddWithValue("@usuario", Usuario)
            End With
            cmd.ExecuteReader()
            con.Close()
            Guardar_Detalle()
            MsgBox("Precios Guardados")
            Carga_Detalle()
            clear()
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Guardar_Detalle()
        Try
            For i = 1 To 3
                con.Open()
                Dim cmd As New SqlCommand("SP_Precio_Detalle", con)
                cmd.CommandType = CommandType.StoredProcedure
                With cmd.Parameters
                    .AddWithValue("@edicion", edicion2)
                    .AddWithValue("@master", lbCodigo.Text)
                    Select Case i
                        Case 1
                            .AddWithValue("@codigo", lbD1.Text)
                            .AddWithValue("@tipo", 1)
                            .AddWithValue("@precio", D1)
                        Case 2
                            .AddWithValue("@codigo", lbD2.Text)
                            .AddWithValue("@tipo", 2)
                            .AddWithValue("@precio", (ChangeToDouble(M1) * ChangeToDouble(txtTC.Text)))
                        Case 3
                            .AddWithValue("@codigo", lbD3.Text)
                            .AddWithValue("@tipo", 3)
                            .AddWithValue("@precio", F1)
                    End Select

                End With
                cmd.ExecuteReader()
                con.Close()
            Next
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Sub clear()
        Carga()
        dtpFecha.Enabled = False
        txtDetelle1.Text = 0
        txtDetalle2.Text = 0
        txtMayorista1.Text = 0
        txtMayorista2.Text = 0
        txtFacturacion1.Text = 0
        txtFacturacion2.Text = 0
        dtpFecha.Value = Today
        btnEditar.Text = "&Editar"
        btnNuevo.Text = "&Agregar"
        pDetalle.Enabled = False
        GridView1.ClearSelection()
        btnCancelar.Text = "&Salir"
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If btnCancelar.Text = "&Salir" Then
            Me.Close()
            Me.Dispose()
        Else
            clear()
            btnNuevo.Enabled = True
        End If
    End Sub

    Private Sub txtDetelle1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMayorista2.KeyPress, txtMayorista1.KeyPress, txtFacturacion2.KeyPress, txtFacturacion1.KeyPress, txtDetelle1.KeyPress, txtDetalle2.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub
    Private Sub txtDetelle1_TextChanged(sender As Object, e As EventArgs) Handles txtMayorista2.TextChanged, txtFacturacion1.TextChanged, txtDetelle1.TextChanged, ckbIVA3.CheckedChanged, ckbIVA2.CheckedChanged, ckbIVA1.CheckedChanged, dtpFecha.ValueChanged
        Calculo()
    End Sub
    Sub Calculo()
        Try
            D1 = txtDetelle1.Text
            D2 = 0
            M1 = txtMayorista2.Text
            M2 = 0
            F1 = txtFacturacion1.Text
            F2 = 0
            Porcentaje = 0
            Costo = txtCostoCS.Text
            '----
            If ckbIVA1.Checked = True Then
                D1 = D1 - (D1 * 0.15)
            End If
            If ckbIVA2.Checked = True Then
                M1 = M1 - (M1 * 0.15)
            End If
            If ckbIVA3.Checked = True Then
                F1 = F1 - (F1 * 0.15)
            End If

            '---------
            Porcentaje = ((D1 - Costo) * 100) / Costo
            txtDetalle2.Text = String.Format("{0:n2}", Porcentaje) + " %"
            If Porcentaje > 0 Then
                txtDetalle2.BackColor = Color.PaleGreen
            Else
                txtDetalle2.BackColor = Color.MistyRose
            End If
            '----------------
            Porcentaje = (((M1 * Tasa) - Costo) * 100) / Costo
            txtMayorista1.Text = String.Format("{0:n2}", Porcentaje) + " %"
            If Porcentaje > 0 Then
                txtMayorista1.BackColor = Color.PaleGreen
            Else
                txtMayorista1.BackColor = Color.MistyRose
            End If
            '----------------
            Porcentaje = ((F1 - Costo) * 100) / Costo
            txtFacturacion2.Text = String.Format("{0:n2}", Porcentaje) + " %"
            If Porcentaje > 0 Then
                txtFacturacion2.BackColor = Color.PaleGreen
            Else
                txtFacturacion2.BackColor = Color.MistyRose
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        Dim dt As DataTable = New DataTable()
        sqlstring = "select Tasa_Oficial from Tbl_TasasCambio where Fecha=@fecha"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Text))
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                Tasa = row0(0)
                txtTC.Text = Tasa
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
End Class