﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditModelo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditModelo))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdAddMarca = New DevExpress.XtraEditors.SimpleButton()
        Me.luMarcas = New DevExpress.XtraEditors.LookUpEdit()
        Me.tblMarcasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblMarcaAsoc = New System.Windows.Forms.Label()
        Me.lblNomModelo = New System.Windows.Forms.Label()
        Me.txtNomModelo = New System.Windows.Forms.TextBox()
        Me.lblCodModelo = New System.Windows.Forms.Label()
        Me.txtCodModelo = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        CType(Me.luMarcas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tblMarcasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdAddMarca)
        Me.Panel2.Controls.Add(Me.luMarcas)
        Me.Panel2.Controls.Add(Me.lblMarcaAsoc)
        Me.Panel2.Controls.Add(Me.lblNomModelo)
        Me.Panel2.Controls.Add(Me.txtNomModelo)
        Me.Panel2.Controls.Add(Me.lblCodModelo)
        Me.Panel2.Controls.Add(Me.txtCodModelo)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(485, 160)
        Me.Panel2.TabIndex = 5
        '
        'cmdAddMarca
        '
        Me.cmdAddMarca.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddMarca.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddMarca.Appearance.Options.UseFont = True
        Me.cmdAddMarca.Appearance.Options.UseForeColor = True
        Me.cmdAddMarca.Location = New System.Drawing.Point(436, 26)
        Me.cmdAddMarca.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdAddMarca.Name = "cmdAddMarca"
        Me.cmdAddMarca.Size = New System.Drawing.Size(27, 23)
        Me.cmdAddMarca.TabIndex = 10
        Me.cmdAddMarca.Text = "+"
        Me.cmdAddMarca.ToolTip = "Agregar una nueva Marca"
        '
        'luMarcas
        '
        Me.luMarcas.Location = New System.Drawing.Point(180, 26)
        Me.luMarcas.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.luMarcas.Name = "luMarcas"
        Me.luMarcas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luMarcas.Properties.DataSource = Me.tblMarcasBindingSource
        Me.luMarcas.Size = New System.Drawing.Size(253, 23)
        Me.luMarcas.TabIndex = 6
        '
        'lblMarcaAsoc
        '
        Me.lblMarcaAsoc.Location = New System.Drawing.Point(16, 30)
        Me.lblMarcaAsoc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMarcaAsoc.Name = "lblMarcaAsoc"
        Me.lblMarcaAsoc.Size = New System.Drawing.Size(156, 25)
        Me.lblMarcaAsoc.TabIndex = 5
        Me.lblMarcaAsoc.Text = "Marca Asociada:"
        '
        'lblNomModelo
        '
        Me.lblNomModelo.Location = New System.Drawing.Point(16, 119)
        Me.lblNomModelo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNomModelo.Name = "lblNomModelo"
        Me.lblNomModelo.Size = New System.Drawing.Size(156, 25)
        Me.lblNomModelo.TabIndex = 3
        Me.lblNomModelo.Text = "Nombre de Modelo:"
        '
        'txtNomModelo
        '
        Me.txtNomModelo.Location = New System.Drawing.Point(180, 116)
        Me.txtNomModelo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtNomModelo.Name = "txtNomModelo"
        Me.txtNomModelo.Size = New System.Drawing.Size(281, 22)
        Me.txtNomModelo.TabIndex = 2
        '
        'lblCodModelo
        '
        Me.lblCodModelo.Location = New System.Drawing.Point(16, 74)
        Me.lblCodModelo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCodModelo.Name = "lblCodModelo"
        Me.lblCodModelo.Size = New System.Drawing.Size(100, 25)
        Me.lblCodModelo.TabIndex = 1
        Me.lblCodModelo.Text = "Id_Modelo:"
        '
        'txtCodModelo
        '
        Me.txtCodModelo.Location = New System.Drawing.Point(180, 70)
        Me.txtCodModelo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCodModelo.Name = "txtCodModelo"
        Me.txtCodModelo.ReadOnly = True
        Me.txtCodModelo.Size = New System.Drawing.Size(132, 22)
        Me.txtCodModelo.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 160)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(485, 60)
        Me.Panel1.TabIndex = 4
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(359, 10)
        Me.cmdCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(104, 43)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(247, 10)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(104, 43)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'frmEditModelo
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(485, 220)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditModelo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Modelos"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.luMarcas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tblMarcasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblMarcaAsoc As System.Windows.Forms.Label
    Friend WithEvents lblNomModelo As System.Windows.Forms.Label
    Friend WithEvents txtNomModelo As System.Windows.Forms.TextBox
    Friend WithEvents lblCodModelo As System.Windows.Forms.Label
    Friend WithEvents txtCodModelo As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents luMarcas As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tblMarcasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents cmdAddMarca As DevExpress.XtraEditors.SimpleButton
End Class
