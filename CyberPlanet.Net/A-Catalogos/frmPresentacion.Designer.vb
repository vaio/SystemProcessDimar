﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPresentacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPresentacion))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtNomPresent = New System.Windows.Forms.TextBox()
        Me.txtCodPresent = New System.Windows.Forms.TextBox()
        Me.lblNomPresent = New System.Windows.Forms.Label()
        Me.lblCodPresent = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtNomPresent)
        Me.Panel2.Controls.Add(Me.txtCodPresent)
        Me.Panel2.Controls.Add(Me.lblNomPresent)
        Me.Panel2.Controls.Add(Me.lblCodPresent)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(351, 92)
        Me.Panel2.TabIndex = 0
        '
        'txtNomPresent
        '
        Me.txtNomPresent.Location = New System.Drawing.Point(155, 54)
        Me.txtNomPresent.Name = "txtNomPresent"
        Me.txtNomPresent.Size = New System.Drawing.Size(173, 20)
        Me.txtNomPresent.TabIndex = 1
        '
        'txtCodPresent
        '
        Me.txtCodPresent.Location = New System.Drawing.Point(155, 21)
        Me.txtCodPresent.Name = "txtCodPresent"
        Me.txtCodPresent.Size = New System.Drawing.Size(100, 20)
        Me.txtCodPresent.TabIndex = 0
        Me.txtCodPresent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblNomPresent
        '
        Me.lblNomPresent.Location = New System.Drawing.Point(26, 57)
        Me.lblNomPresent.Name = "lblNomPresent"
        Me.lblNomPresent.Size = New System.Drawing.Size(123, 20)
        Me.lblNomPresent.TabIndex = 3
        Me.lblNomPresent.Text = "Nombre Presentacion"
        '
        'lblCodPresent
        '
        Me.lblCodPresent.Location = New System.Drawing.Point(26, 24)
        Me.lblCodPresent.Name = "lblCodPresent"
        Me.lblCodPresent.Size = New System.Drawing.Size(123, 20)
        Me.lblCodPresent.TabIndex = 1
        Me.lblCodPresent.Text = "Codigo Presentacion"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 92)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(351, 49)
        Me.Panel1.TabIndex = 1
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(253, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(169, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'frmPresentacion
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(351, 141)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmPresentacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Presentacion"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtNomPresent As System.Windows.Forms.TextBox
    Friend WithEvents txtCodPresent As System.Windows.Forms.TextBox
    Friend WithEvents lblNomPresent As System.Windows.Forms.Label
    Friend WithEvents lblCodPresent As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
End Class
