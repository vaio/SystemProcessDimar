﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditMunicipio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditMunicipio))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.cmdAddDeparPais = New DevExpress.XtraEditors.SimpleButton()
		Me.luDepartPais = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblDepartPaisAsoc = New System.Windows.Forms.Label()
		Me.lblNomMunicipio = New System.Windows.Forms.Label()
		Me.txtNomMunicipio = New System.Windows.Forms.TextBox()
		Me.lblCodMunicipio = New System.Windows.Forms.Label()
		Me.txtCodMunicipio = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.TblDepartPaisBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.Panel2.SuspendLayout()
		CType(Me.luDepartPais.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.TblDepartPaisBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.cmdAddDeparPais)
		Me.Panel2.Controls.Add(Me.luDepartPais)
		Me.Panel2.Controls.Add(Me.lblDepartPaisAsoc)
		Me.Panel2.Controls.Add(Me.lblNomMunicipio)
		Me.Panel2.Controls.Add(Me.txtNomMunicipio)
		Me.Panel2.Controls.Add(Me.lblCodMunicipio)
		Me.Panel2.Controls.Add(Me.txtCodMunicipio)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(364, 130)
		Me.Panel2.TabIndex = 7
		'
		'cmdAddDeparPais
		'
		Me.cmdAddDeparPais.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddDeparPais.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddDeparPais.Appearance.Options.UseFont = True
		Me.cmdAddDeparPais.Appearance.Options.UseForeColor = True
		Me.cmdAddDeparPais.Location = New System.Drawing.Point(330, 94)
		Me.cmdAddDeparPais.Name = "cmdAddDeparPais"
		Me.cmdAddDeparPais.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddDeparPais.TabIndex = 9
		Me.cmdAddDeparPais.Text = "+"
		Me.cmdAddDeparPais.ToolTip = "Agregar un nuevo Departamento"
		'
		'luDepartPais
		'
		Me.luDepartPais.Location = New System.Drawing.Point(135, 94)
		Me.luDepartPais.Name = "luDepartPais"
		Me.luDepartPais.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luDepartPais.Size = New System.Drawing.Size(194, 20)
		Me.luDepartPais.TabIndex = 7
		'
		'lblDepartPaisAsoc
		'
		Me.lblDepartPaisAsoc.Location = New System.Drawing.Point(12, 97)
		Me.lblDepartPaisAsoc.Name = "lblDepartPaisAsoc"
		Me.lblDepartPaisAsoc.Size = New System.Drawing.Size(129, 20)
		Me.lblDepartPaisAsoc.TabIndex = 5
		Me.lblDepartPaisAsoc.Text = "Departamento Asociado:"
		'
		'lblNomMunicipio
		'
		Me.lblNomMunicipio.Location = New System.Drawing.Point(12, 60)
		Me.lblNomMunicipio.Name = "lblNomMunicipio"
		Me.lblNomMunicipio.Size = New System.Drawing.Size(117, 20)
		Me.lblNomMunicipio.TabIndex = 3
		Me.lblNomMunicipio.Text = "Nombre de Municipio:"
		'
		'txtNomMunicipio
		'
		Me.txtNomMunicipio.Location = New System.Drawing.Point(135, 57)
		Me.txtNomMunicipio.Name = "txtNomMunicipio"
		Me.txtNomMunicipio.Size = New System.Drawing.Size(212, 20)
		Me.txtNomMunicipio.TabIndex = 2
		'
		'lblCodMunicipio
		'
		Me.lblCodMunicipio.Location = New System.Drawing.Point(12, 24)
		Me.lblCodMunicipio.Name = "lblCodMunicipio"
		Me.lblCodMunicipio.Size = New System.Drawing.Size(75, 20)
		Me.lblCodMunicipio.TabIndex = 1
		Me.lblCodMunicipio.Text = "Id_Municipio:"
		'
		'txtCodMunicipio
		'
		Me.txtCodMunicipio.Location = New System.Drawing.Point(135, 21)
		Me.txtCodMunicipio.Name = "txtCodMunicipio"
		Me.txtCodMunicipio.Size = New System.Drawing.Size(100, 20)
		Me.txtCodMunicipio.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 130)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(364, 49)
		Me.Panel1.TabIndex = 6
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(269, 8)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(185, 8)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'frmEditMunicipio
		'
		Me.AcceptButton = Me.cmdAceptar
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.CancelButton = Me.cmdCancelar
		Me.ClientSize = New System.Drawing.Size(364, 179)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmEditMunicipio"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Edición de Municipio"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.luDepartPais.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		CType(Me.TblDepartPaisBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents cmdAddDeparPais As DevExpress.XtraEditors.SimpleButton
  Friend WithEvents luDepartPais As DevExpress.XtraEditors.LookUpEdit
  Friend WithEvents lblDepartPaisAsoc As System.Windows.Forms.Label
  Friend WithEvents lblNomMunicipio As System.Windows.Forms.Label
  Friend WithEvents txtNomMunicipio As System.Windows.Forms.TextBox
  Friend WithEvents lblCodMunicipio As System.Windows.Forms.Label
  Friend WithEvents txtCodMunicipio As System.Windows.Forms.TextBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents cmdCancelar As System.Windows.Forms.Button
  Friend WithEvents cmdAceptar As System.Windows.Forms.Button
  Friend WithEvents TblDepartPaisBindingSource As System.Windows.Forms.BindingSource
End Class
