﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditTipoOperaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditTipoOperaciones))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.ckIsIngreso = New DevExpress.XtraEditors.CheckEdit()
		Me.txtNomTipOper = New System.Windows.Forms.TextBox()
		Me.lblNomTipOper = New System.Windows.Forms.Label()
		Me.lblCodTipOper = New System.Windows.Forms.Label()
		Me.txtCodTipOper = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.txtCuentaContable = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Panel2.SuspendLayout()
		CType(Me.ckIsIngreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.txtCuentaContable)
		Me.Panel2.Controls.Add(Me.Label1)
		Me.Panel2.Controls.Add(Me.ckIsIngreso)
		Me.Panel2.Controls.Add(Me.txtNomTipOper)
		Me.Panel2.Controls.Add(Me.lblNomTipOper)
		Me.Panel2.Controls.Add(Me.lblCodTipOper)
		Me.Panel2.Controls.Add(Me.txtCodTipOper)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(386, 117)
		Me.Panel2.TabIndex = 7
		'
		'ckIsIngreso
		'
		Me.ckIsIngreso.Enabled = False
		Me.ckIsIngreso.Location = New System.Drawing.Point(280, 20)
		Me.ckIsIngreso.Name = "ckIsIngreso"
		Me.ckIsIngreso.Properties.Caption = "Es Ingreso"
		Me.ckIsIngreso.Size = New System.Drawing.Size(75, 18)
		Me.ckIsIngreso.TabIndex = 4
		'
		'txtNomTipOper
		'
		Me.txtNomTipOper.Location = New System.Drawing.Point(143, 51)
		Me.txtNomTipOper.Name = "txtNomTipOper"
		Me.txtNomTipOper.Size = New System.Drawing.Size(212, 20)
		Me.txtNomTipOper.TabIndex = 2
		'
		'lblNomTipOper
		'
		Me.lblNomTipOper.Location = New System.Drawing.Point(25, 54)
		Me.lblNomTipOper.Name = "lblNomTipOper"
		Me.lblNomTipOper.Size = New System.Drawing.Size(99, 20)
		Me.lblNomTipOper.TabIndex = 3
		Me.lblNomTipOper.Text = "Tipo de Operación:"
		'
		'lblCodTipOper
		'
		Me.lblCodTipOper.Location = New System.Drawing.Point(25, 20)
		Me.lblCodTipOper.Name = "lblCodTipOper"
		Me.lblCodTipOper.Size = New System.Drawing.Size(99, 20)
		Me.lblCodTipOper.TabIndex = 1
		Me.lblCodTipOper.Text = "Id_Tipo Operacion:"
		'
		'txtCodTipOper
		'
		Me.txtCodTipOper.Location = New System.Drawing.Point(143, 20)
		Me.txtCodTipOper.Name = "txtCodTipOper"
		Me.txtCodTipOper.Size = New System.Drawing.Size(85, 20)
		Me.txtCodTipOper.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 117)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(386, 49)
		Me.Panel1.TabIndex = 6
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(288, 8)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(204, 8)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'txtCuentaContable
		'
		Me.txtCuentaContable.Location = New System.Drawing.Point(143, 80)
		Me.txtCuentaContable.Name = "txtCuentaContable"
		Me.txtCuentaContable.Size = New System.Drawing.Size(112, 20)
		Me.txtCuentaContable.TabIndex = 5
		Me.txtCuentaContable.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(25, 83)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(99, 20)
		Me.Label1.TabIndex = 6
		Me.Label1.Text = "Cuenta Contable:"
		'
		'frmEditTipoOperaciones
		'
		Me.AcceptButton = Me.cmdAceptar
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.CancelButton = Me.cmdCancelar
		Me.ClientSize = New System.Drawing.Size(386, 166)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmEditTipoOperaciones"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "frmEditTipoOperaciones"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.ckIsIngreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents txtNomTipOper As System.Windows.Forms.TextBox
	Friend WithEvents lblNomTipOper As System.Windows.Forms.Label
	Friend WithEvents lblCodTipOper As System.Windows.Forms.Label
	Friend WithEvents txtCodTipOper As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cmdCancelar As System.Windows.Forms.Button
	Friend WithEvents cmdAceptar As System.Windows.Forms.Button
	Friend WithEvents ckIsIngreso As DevExpress.XtraEditors.CheckEdit
 Friend WithEvents txtCuentaContable As System.Windows.Forms.TextBox
 Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
