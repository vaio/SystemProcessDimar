﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrecios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrecios))
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.Panel4 = New System.Windows.Forms.Panel()
    Me.Button1 = New System.Windows.Forms.Button()
    Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
    Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
    Me.txtCosto = New System.Windows.Forms.TextBox()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.Label10 = New System.Windows.Forms.Label()
    Me.Label9 = New System.Windows.Forms.Label()
    Me.lblCostoProm = New System.Windows.Forms.Label()
    Me.txtNomProd = New System.Windows.Forms.TextBox()
    Me.Panel1.SuspendLayout()
    Me.Panel4.SuspendLayout()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 201)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(414, 49)
    Me.Panel1.TabIndex = 53
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(316, 8)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(232, 8)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'Panel4
    '
    Me.Panel4.Controls.Add(Me.txtNomProd)
    Me.Panel4.Controls.Add(Me.txtCosto)
    Me.Panel4.Controls.Add(Me.TextBox1)
    Me.Panel4.Controls.Add(Me.Label10)
    Me.Panel4.Controls.Add(Me.Label9)
    Me.Panel4.Controls.Add(Me.lblCostoProm)
    Me.Panel4.Controls.Add(Me.Button1)
    Me.Panel4.Controls.Add(Me.GridControl1)
    Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel4.Location = New System.Drawing.Point(0, 0)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(414, 250)
    Me.Panel4.TabIndex = 54
    '
    'Button1
    '
    Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.Button1.Enabled = False
    Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
    Me.Button1.Location = New System.Drawing.Point(362, 66)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(36, 39)
    Me.Button1.TabIndex = 52
    Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Button1.UseVisualStyleBackColor = True
    '
    'GridControl1
    '
    Me.GridControl1.Location = New System.Drawing.Point(21, 66)
    Me.GridControl1.MainView = Me.GridView1
    Me.GridControl1.Name = "GridControl1"
    Me.GridControl1.Size = New System.Drawing.Size(335, 124)
    Me.GridControl1.TabIndex = 51
    Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
    '
    'GridView1
    '
    Me.GridView1.GridControl = Me.GridControl1
    Me.GridView1.Name = "GridView1"
    Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
    Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
    Me.GridView1.OptionsBehavior.Editable = False
    Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
    Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
    Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
    Me.GridView1.OptionsView.ShowFooter = True
    Me.GridView1.OptionsView.ShowGroupPanel = False
    Me.GridView1.PaintStyleName = "UltraFlat"
    '
    'txtCosto
    '
    Me.txtCosto.Location = New System.Drawing.Point(202, 37)
    Me.txtCosto.Name = "txtCosto"
    Me.txtCosto.Size = New System.Drawing.Size(75, 20)
    Me.txtCosto.TabIndex = 57
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(320, 37)
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Size = New System.Drawing.Size(78, 20)
    Me.TextBox1.TabIndex = 58
    '
    'Label10
    '
    Me.Label10.Location = New System.Drawing.Point(283, 40)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(35, 20)
    Me.Label10.TabIndex = 60
    Me.Label10.Text = "US$:"
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(179, 40)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(24, 20)
    Me.Label9.TabIndex = 59
    Me.Label9.Text = "C$:"
    '
    'lblCostoProm
    '
    Me.lblCostoProm.Location = New System.Drawing.Point(18, 12)
    Me.lblCostoProm.Name = "lblCostoProm"
    Me.lblCostoProm.Size = New System.Drawing.Size(66, 17)
    Me.lblCostoProm.TabIndex = 56
    Me.lblCostoProm.Text = "Descripción:"
    '
    'txtNomProd
    '
    Me.txtNomProd.Location = New System.Drawing.Point(84, 9)
    Me.txtNomProd.Name = "txtNomProd"
    Me.txtNomProd.Size = New System.Drawing.Size(314, 20)
    Me.txtNomProd.TabIndex = 61
    '
    'frmPrecios
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(414, 250)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Panel4)
    Me.Name = "frmPrecios"
    Me.Text = "Edición de Precios"
    Me.Panel1.ResumeLayout(False)
    Me.Panel4.ResumeLayout(False)
    Me.Panel4.PerformLayout()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblCostoProm As System.Windows.Forms.Label
    Friend WithEvents txtNomProd As System.Windows.Forms.TextBox
End Class
