﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditDepartamento
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditDepartamento))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtNomLinea = New System.Windows.Forms.TextBox()
        Me.lblNomDepart = New System.Windows.Forms.Label()
        Me.lblCodDepart = New System.Windows.Forms.Label()
        Me.txtCodLinea = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtNomLinea)
        Me.Panel2.Controls.Add(Me.lblNomDepart)
        Me.Panel2.Controls.Add(Me.lblCodDepart)
        Me.Panel2.Controls.Add(Me.txtCodLinea)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(386, 89)
        Me.Panel2.TabIndex = 5
        '
        'txtNomLinea
        '
        Me.txtNomLinea.Location = New System.Drawing.Point(155, 57)
        Me.txtNomLinea.Name = "txtNomLinea"
        Me.txtNomLinea.Size = New System.Drawing.Size(212, 20)
        Me.txtNomLinea.TabIndex = 2
        '
        'lblNomDepart
        '
        Me.lblNomDepart.Location = New System.Drawing.Point(24, 60)
        Me.lblNomDepart.Name = "lblNomDepart"
        Me.lblNomDepart.Size = New System.Drawing.Size(136, 20)
        Me.lblNomDepart.TabIndex = 3
        Me.lblNomDepart.Text = "Nombre de Linea:"
        '
        'lblCodDepart
        '
        Me.lblCodDepart.Location = New System.Drawing.Point(24, 24)
        Me.lblCodDepart.Name = "lblCodDepart"
        Me.lblCodDepart.Size = New System.Drawing.Size(92, 20)
        Me.lblCodDepart.TabIndex = 1
        Me.lblCodDepart.Text = "Codigo_Linea:"
        '
        'txtCodLinea
        '
        Me.txtCodLinea.Location = New System.Drawing.Point(155, 21)
        Me.txtCodLinea.Name = "txtCodLinea"
        Me.txtCodLinea.ReadOnly = True
        Me.txtCodLinea.Size = New System.Drawing.Size(100, 20)
        Me.txtCodLinea.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 89)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(386, 49)
        Me.Panel1.TabIndex = 4
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(288, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(204, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'frmEditDepartamento
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(386, 138)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditDepartamento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Lineas de Productos"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblNomDepart As System.Windows.Forms.Label
    Friend WithEvents txtNomLinea As System.Windows.Forms.TextBox
    Friend WithEvents lblCodDepart As System.Windows.Forms.Label
    Friend WithEvents txtCodLinea As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
End Class
