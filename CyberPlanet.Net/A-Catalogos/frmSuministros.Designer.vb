﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditSuministros
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditSuministros))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.cmdAddMarca = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddDepar = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddModelo = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddCateg = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddProv = New DevExpress.XtraEditors.SimpleButton()
		Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
		Me.deFechaBaja = New DevExpress.XtraEditors.DateEdit()
		Me.deFechaIng = New DevExpress.XtraEditors.DateEdit()
		Me.lblFechaBaja = New System.Windows.Forms.Label()
		Me.lblFechaIngreso = New System.Windows.Forms.Label()
		Me.luProv = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblProveedor = New System.Windows.Forms.Label()
		Me.luMarca = New DevExpress.XtraEditors.LookUpEdit()
		Me.luModelo = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblModelo = New System.Windows.Forms.Label()
		Me.lblMarca = New System.Windows.Forms.Label()
		Me.txtCosto = New System.Windows.Forms.TextBox()
		Me.luDepart = New DevExpress.XtraEditors.LookUpEdit()
		Me.luCateg = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblCateg = New System.Windows.Forms.Label()
		Me.lblDepart = New System.Windows.Forms.Label()
		Me.lblCostoProm = New System.Windows.Forms.Label()
		Me.lblNomSuministro = New System.Windows.Forms.Label()
		Me.txtNomSuministro = New System.Windows.Forms.TextBox()
		Me.lblCodSuministro = New System.Windows.Forms.Label()
		Me.txtCodSuministro = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.TblProvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblDeparBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblCategBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblMarcaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblModeloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.Panel2.SuspendLayout()
		CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luProv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luModelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luDepart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luCateg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblDeparBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblCategBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblMarcaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblModeloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.cmdAddMarca)
		Me.Panel2.Controls.Add(Me.cmdAddDepar)
		Me.Panel2.Controls.Add(Me.cmdAddModelo)
		Me.Panel2.Controls.Add(Me.cmdAddCateg)
		Me.Panel2.Controls.Add(Me.cmdAddProv)
		Me.Panel2.Controls.Add(Me.ckIsActivo)
		Me.Panel2.Controls.Add(Me.deFechaBaja)
		Me.Panel2.Controls.Add(Me.deFechaIng)
		Me.Panel2.Controls.Add(Me.lblFechaBaja)
		Me.Panel2.Controls.Add(Me.lblFechaIngreso)
		Me.Panel2.Controls.Add(Me.luProv)
		Me.Panel2.Controls.Add(Me.lblProveedor)
		Me.Panel2.Controls.Add(Me.luMarca)
		Me.Panel2.Controls.Add(Me.luModelo)
		Me.Panel2.Controls.Add(Me.lblModelo)
		Me.Panel2.Controls.Add(Me.lblMarca)
		Me.Panel2.Controls.Add(Me.txtCosto)
		Me.Panel2.Controls.Add(Me.luDepart)
		Me.Panel2.Controls.Add(Me.luCateg)
		Me.Panel2.Controls.Add(Me.lblCateg)
		Me.Panel2.Controls.Add(Me.lblDepart)
		Me.Panel2.Controls.Add(Me.lblCostoProm)
		Me.Panel2.Controls.Add(Me.lblNomSuministro)
		Me.Panel2.Controls.Add(Me.txtNomSuministro)
		Me.Panel2.Controls.Add(Me.lblCodSuministro)
		Me.Panel2.Controls.Add(Me.txtCodSuministro)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(564, 234)
		Me.Panel2.TabIndex = 2
		'
		'cmdAddMarca
		'
		Me.cmdAddMarca.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddMarca.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddMarca.Appearance.Options.UseFont = True
		Me.cmdAddMarca.Appearance.Options.UseForeColor = True
		Me.cmdAddMarca.Location = New System.Drawing.Point(264, 163)
		Me.cmdAddMarca.Name = "cmdAddMarca"
		Me.cmdAddMarca.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddMarca.TabIndex = 28
		Me.cmdAddMarca.Text = "+"
		Me.cmdAddMarca.ToolTip = "Agregar una nueva Marca"
		'
		'cmdAddDepar
		'
		Me.cmdAddDepar.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddDepar.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddDepar.Appearance.Options.UseFont = True
		Me.cmdAddDepar.Appearance.Options.UseForeColor = True
		Me.cmdAddDepar.Location = New System.Drawing.Point(264, 131)
		Me.cmdAddDepar.Name = "cmdAddDepar"
		Me.cmdAddDepar.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddDepar.TabIndex = 27
		Me.cmdAddDepar.Text = "+"
		Me.cmdAddDepar.ToolTip = "Agregar un nuevo Departamento"
		'
		'cmdAddModelo
		'
		Me.cmdAddModelo.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddModelo.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddModelo.Appearance.Options.UseFont = True
		Me.cmdAddModelo.Appearance.Options.UseForeColor = True
		Me.cmdAddModelo.Location = New System.Drawing.Point(524, 165)
		Me.cmdAddModelo.Name = "cmdAddModelo"
		Me.cmdAddModelo.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddModelo.TabIndex = 26
		Me.cmdAddModelo.Text = "+"
		Me.cmdAddModelo.ToolTip = "Agregar un nuevo Modelo"
		'
		'cmdAddCateg
		'
		Me.cmdAddCateg.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddCateg.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddCateg.Appearance.Options.UseFont = True
		Me.cmdAddCateg.Appearance.Options.UseForeColor = True
		Me.cmdAddCateg.Location = New System.Drawing.Point(524, 131)
		Me.cmdAddCateg.Name = "cmdAddCateg"
		Me.cmdAddCateg.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddCateg.TabIndex = 25
		Me.cmdAddCateg.Text = "+"
		Me.cmdAddCateg.ToolTip = "Agregar una nueva Categoría"
		'
		'cmdAddProv
		'
		Me.cmdAddProv.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddProv.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddProv.Appearance.Options.UseFont = True
		Me.cmdAddProv.Appearance.Options.UseForeColor = True
		Me.cmdAddProv.Location = New System.Drawing.Point(524, 91)
		Me.cmdAddProv.Name = "cmdAddProv"
		Me.cmdAddProv.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddProv.TabIndex = 24
		Me.cmdAddProv.Text = "+"
		Me.cmdAddProv.ToolTip = "Agregar un nuevo Proveedor"
		'
		'ckIsActivo
		'
		Me.ckIsActivo.Enabled = False
		Me.ckIsActivo.Location = New System.Drawing.Point(22, 92)
		Me.ckIsActivo.Name = "ckIsActivo"
		Me.ckIsActivo.Properties.Caption = "Esta Activo"
		Me.ckIsActivo.Size = New System.Drawing.Size(75, 19)
		Me.ckIsActivo.TabIndex = 4
		'
		'deFechaBaja
		'
		Me.deFechaBaja.EditValue = Nothing
		Me.deFechaBaja.Location = New System.Drawing.Point(365, 197)
		Me.deFechaBaja.Name = "deFechaBaja"
		Me.deFechaBaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaBaja.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaBaja.Size = New System.Drawing.Size(180, 20)
		Me.deFechaBaja.TabIndex = 13
		'
		'deFechaIng
		'
		Me.deFechaIng.EditValue = Nothing
		Me.deFechaIng.Location = New System.Drawing.Point(104, 197)
		Me.deFechaIng.Name = "deFechaIng"
		Me.deFechaIng.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaIng.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaIng.Size = New System.Drawing.Size(180, 20)
		Me.deFechaIng.TabIndex = 12
		'
		'lblFechaBaja
		'
		Me.lblFechaBaja.Location = New System.Drawing.Point(303, 200)
		Me.lblFechaBaja.Name = "lblFechaBaja"
		Me.lblFechaBaja.Size = New System.Drawing.Size(61, 17)
		Me.lblFechaBaja.TabIndex = 23
		Me.lblFechaBaja.Text = "F. Baja:"
		'
		'lblFechaIngreso
		'
		Me.lblFechaIngreso.Location = New System.Drawing.Point(21, 200)
		Me.lblFechaIngreso.Name = "lblFechaIngreso"
		Me.lblFechaIngreso.Size = New System.Drawing.Size(61, 17)
		Me.lblFechaIngreso.TabIndex = 22
		Me.lblFechaIngreso.Text = "F. Ingreso:"
		'
		'luProv
		'
		Me.luProv.Location = New System.Drawing.Point(365, 91)
		Me.luProv.Name = "luProv"
		Me.luProv.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luProv.Size = New System.Drawing.Size(159, 20)
		Me.luProv.TabIndex = 7
		'
		'lblProveedor
		'
		Me.lblProveedor.Location = New System.Drawing.Point(303, 92)
		Me.lblProveedor.Name = "lblProveedor"
		Me.lblProveedor.Size = New System.Drawing.Size(61, 17)
		Me.lblProveedor.TabIndex = 20
		Me.lblProveedor.Text = "Proveedor:"
		'
		'luMarca
		'
		Me.luMarca.Location = New System.Drawing.Point(104, 163)
		Me.luMarca.Name = "luMarca"
		Me.luMarca.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luMarca.Size = New System.Drawing.Size(159, 20)
		Me.luMarca.TabIndex = 10
		'
		'luModelo
		'
		Me.luModelo.Location = New System.Drawing.Point(365, 166)
		Me.luModelo.Name = "luModelo"
		Me.luModelo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luModelo.Size = New System.Drawing.Size(159, 20)
		Me.luModelo.TabIndex = 11
		'
		'lblModelo
		'
		Me.lblModelo.Location = New System.Drawing.Point(303, 169)
		Me.lblModelo.Name = "lblModelo"
		Me.lblModelo.Size = New System.Drawing.Size(61, 17)
		Me.lblModelo.TabIndex = 18
		Me.lblModelo.Text = "Modelo:"
		'
		'lblMarca
		'
		Me.lblMarca.Location = New System.Drawing.Point(21, 166)
		Me.lblMarca.Name = "lblMarca"
		Me.lblMarca.Size = New System.Drawing.Size(77, 20)
		Me.lblMarca.TabIndex = 17
		Me.lblMarca.Text = "Marca:"
		'
		'txtCosto
		'
		Me.txtCosto.Location = New System.Drawing.Point(210, 91)
		Me.txtCosto.Name = "txtCosto"
		Me.txtCosto.Size = New System.Drawing.Size(74, 20)
		Me.txtCosto.TabIndex = 5
		'
		'luDepart
		'
		Me.luDepart.Location = New System.Drawing.Point(104, 131)
		Me.luDepart.Name = "luDepart"
		Me.luDepart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luDepart.Size = New System.Drawing.Size(159, 20)
		Me.luDepart.TabIndex = 8
		'
		'luCateg
		'
		Me.luCateg.Location = New System.Drawing.Point(365, 131)
		Me.luCateg.Name = "luCateg"
		Me.luCateg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luCateg.Size = New System.Drawing.Size(159, 20)
		Me.luCateg.TabIndex = 9
		'
		'lblCateg
		'
		Me.lblCateg.Location = New System.Drawing.Point(303, 134)
		Me.lblCateg.Name = "lblCateg"
		Me.lblCateg.Size = New System.Drawing.Size(61, 20)
		Me.lblCateg.TabIndex = 11
		Me.lblCateg.Text = "Categoría:"
		'
		'lblDepart
		'
		Me.lblDepart.Location = New System.Drawing.Point(21, 134)
		Me.lblDepart.Name = "lblDepart"
		Me.lblDepart.Size = New System.Drawing.Size(77, 20)
		Me.lblDepart.TabIndex = 10
		Me.lblDepart.Text = "Departamento:"
		'
		'lblCostoProm
		'
		Me.lblCostoProm.Location = New System.Drawing.Point(167, 91)
		Me.lblCostoProm.Name = "lblCostoProm"
		Me.lblCostoProm.Size = New System.Drawing.Size(44, 20)
		Me.lblCostoProm.TabIndex = 8
		Me.lblCostoProm.Text = "Costo:"
		'
		'lblNomSuministro
		'
		Me.lblNomSuministro.Location = New System.Drawing.Point(21, 57)
		Me.lblNomSuministro.Name = "lblNomSuministro"
		Me.lblNomSuministro.Size = New System.Drawing.Size(117, 20)
		Me.lblNomSuministro.TabIndex = 3
		Me.lblNomSuministro.Text = "Nombre del Suminitro:"
		'
		'txtNomSuministro
		'
		Me.txtNomSuministro.Location = New System.Drawing.Point(144, 57)
		Me.txtNomSuministro.Name = "txtNomSuministro"
		Me.txtNomSuministro.Size = New System.Drawing.Size(401, 20)
		Me.txtNomSuministro.TabIndex = 2
		'
		'lblCodSuministro
		'
		Me.lblCodSuministro.Location = New System.Drawing.Point(21, 24)
		Me.lblCodSuministro.Name = "lblCodSuministro"
		Me.lblCodSuministro.Size = New System.Drawing.Size(117, 20)
		Me.lblCodSuministro.TabIndex = 1
		Me.lblCodSuministro.Text = "Id_Suministro:"
		'
		'txtCodSuministro
		'
		Me.txtCodSuministro.Location = New System.Drawing.Point(144, 21)
		Me.txtCodSuministro.Name = "txtCodSuministro"
		Me.txtCodSuministro.Size = New System.Drawing.Size(100, 20)
		Me.txtCodSuministro.TabIndex = 1
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 234)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(564, 49)
		Me.Panel1.TabIndex = 3
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(466, 8)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(382, 8)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'frmEditSuministros
		'
		Me.AcceptButton = Me.cmdAceptar
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.CancelButton = Me.cmdCancelar
		Me.ClientSize = New System.Drawing.Size(564, 283)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmEditSuministros"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Edición de Suministros"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luProv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luModelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luDepart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luCateg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblDeparBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblCategBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblMarcaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblModeloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents cmdAddMarca As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddDepar As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddModelo As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddCateg As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddProv As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
	Friend WithEvents deFechaBaja As DevExpress.XtraEditors.DateEdit
	Friend WithEvents deFechaIng As DevExpress.XtraEditors.DateEdit
	Friend WithEvents lblFechaBaja As System.Windows.Forms.Label
	Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
	Friend WithEvents luProv As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblProveedor As System.Windows.Forms.Label
	Friend WithEvents luMarca As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents luModelo As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblModelo As System.Windows.Forms.Label
	Friend WithEvents lblMarca As System.Windows.Forms.Label
	Friend WithEvents txtCosto As System.Windows.Forms.TextBox
	Friend WithEvents luDepart As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents luCateg As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblCateg As System.Windows.Forms.Label
	Friend WithEvents lblDepart As System.Windows.Forms.Label
	Friend WithEvents lblCostoProm As System.Windows.Forms.Label
	Friend WithEvents lblNomSuministro As System.Windows.Forms.Label
	Friend WithEvents txtNomSuministro As System.Windows.Forms.TextBox
	Friend WithEvents lblCodSuministro As System.Windows.Forms.Label
	Friend WithEvents txtCodSuministro As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cmdCancelar As System.Windows.Forms.Button
	Friend WithEvents cmdAceptar As System.Windows.Forms.Button
	Friend WithEvents TblProvBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblDeparBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblCategBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblMarcaBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblModeloBindingSource As System.Windows.Forms.BindingSource
End Class
