﻿Imports DevExpress.XtraNavBar
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmVehiculo
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Public Categoria As Integer = 0
    Public SubCat As Integer = 0
    Public Edicion As Integer = 0

    Private Sub frmVehiculo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        nbgVehiculo.Expanded = True
        SubCat = 1
        retriveV()
    End Sub
    Sub retriveV()
        Expand(0)
        nbgVehiculo.Expanded = True
        Categoria = 1
        GridControl2.Visible = False
        Select Case SubCat
            Case 1 'Mostrar todos

                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Id_Vehiculo as 'Codigo', (case when Tbl_Vehiculo.Tipo=1 then 'OPERATIVO' when Tbl_Vehiculo.Tipo=2 then 'ADMINISTRATIVO' END) as 'Tipo', Placa, Marca, Modelo, Fecha as 'Adquisicion', Odo_Inicial as 'KM inicial', Odo_Final as 'KM Actual', (case when Estado=1 then 'ACTIVO' when Estado=2 then 'BAJA' when Estado=3 then 'REPARACION' when Estado=4 then 'MANTENIMIENTO' END) as 'Estado' from Tbl_Vehiculo"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Todos los Vehiculos"
                GridControl1.Dock = DockStyle.Fill


            Case 2 'Vehiculos Operativos
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Id_Vehiculo as 'Codigo', (case when Tbl_Vehiculo.Tipo=1 then 'OPERATIVO' when Tbl_Vehiculo.Tipo=2 then 'ADMINISTRATIVO' END) as 'Tipo', Placa, Marca, Modelo, Fecha as 'Adquisicion', Odo_Inicial as 'KM inicial', Odo_Final as 'KM Actual', (case when Estado=1 then 'ACTIVO' when Estado=2 then 'BAJA' when Estado=3 then 'REPARACION' when Estado=4 then 'MANTENIMIENTO' END) as 'Estado'from Tbl_Vehiculo where Tipo=1"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Vehiculos Operativos"
                GridControl1.Dock = DockStyle.Fill
            Case 3 'Veheculos Administrativos
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Id_Vehiculo as 'Codigo', (case when Tbl_Vehiculo.Tipo=1 then 'OPERATIVO' when Tbl_Vehiculo.Tipo=2 then 'ADMINISTRATIVO' END) as 'Tipo', Placa, Marca, Modelo, Fecha as 'Adquisicion', Odo_Inicial as 'KM inicial', Odo_Final as 'KM Actual', (case when Estado=1 then 'ACTIVO' when Estado=2 then 'BAJA' when Estado=3 then 'REPARACION' when Estado=4 then 'MANTENIMIENTO' END) as 'Estado'from Tbl_Vehiculo where Tipo=2"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Vehiculos Administrativos"
                GridControl1.Dock = DockStyle.Fill
            Case 4 'Historial A
                GridControl2.DataSource = vbNull
                GridView2.Columns.Clear()
                'Consulta
                GridControl1.Dock = DockStyle.Top
                GridControl2.Dock = DockStyle.Fill
                GridControl2.Visible = True
        End Select
        frmPrincipal.bbiNuevo.Enabled = True
    End Sub
    Sub retriveA()
        Expand(0)
        nbgAsignacion.Expanded = True
        Categoria = 2
        GridControl2.Visible = False
        Select Case SubCat
            Case 1 'mostrar todo
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select * from vw_Ruta"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Mostras todos los Vehiculos Operativos"
                GridControl1.Dock = DockStyle.Fill
            Case 2 'vehiculos sin asignar
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select * from vw_Ruta where Pendiente =1 or Activo='NO'"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Vehiculos Operativos sin Asignacion"
                GridControl1.Dock = DockStyle.Fill
            Case 3 'Rutas asignadas
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select * from vw_Ruta where Pendiente is null"
                Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = TableDatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Vehiculos Operativos Asignados"
                GridControl1.Dock = DockStyle.Fill
            Case 4 'Historial B
                GridControl2.DataSource = vbNull
                GridView2.Columns.Clear()

                GridControl2.DataSource = SQL("SELECT * FROM vw_Ruta_Historial ORDER BY Fecha ASC", "tbl_Historial", My.Settings.SolIndustrialCNX).Tables(0)
                GridView2.BestFitColumns()
                GridView2.GroupPanelText = "Ruta Historial"
                GridControl1.Dock = DockStyle.Top
                GridControl2.Dock = DockStyle.Fill
                GridControl2.Visible = True
        End Select
        Try
            GridView1.Columns("Pendiente").Visible = False
            GridView1.Columns("Vendedores").Visible = False
        Catch ex As Exception
        End Try
        frmPrincipal.bbiNuevo.Enabled = False
    End Sub
    Sub retriveVen()
        Expand(0)
        nbgVendedor.Expanded = True
        Categoria = 3
        GridControl2.Visible = False

        Select Case SubCat

            Case 1 'Todo
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                GridControl1.DataSource = SQL("select * from vw_Ruta", "tbl_todo", My.Settings.SolIndustrialCNX).Tables(0)
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Rutas"
                GridControl1.Dock = DockStyle.Fill

            Case 2 'No asignado

                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                GridControl1.DataSource = SQL("select * from vw_Ruta", "tbl_todo", My.Settings.SolIndustrialCNX).Tables(0)
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Rutas"
                '------------------------------------------
                GridControl2.DataSource = vbNull
                GridView2.Columns.Clear()

                GridControl2.DataSource = SQL("select * from vw_Ruta_Vendedor where Ruta ='-'", "tbl_vendedor", My.Settings.SolIndustrialCNX).Tables(0)
                GridView2.BestFitColumns()
                GridView2.GroupPanelText = "Vendedores no asignados"

                GridControl1.Dock = DockStyle.Top
                GridControl2.Dock = DockStyle.Fill
                GridControl2.Visible = True

            Case 3 'Asignado

                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                GridControl1.DataSource = SQL("select * from vw_Ruta", "tbl_todo", My.Settings.SolIndustrialCNX).Tables(0)
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Rutas"
                '------------------------------------------
                GridControl2.DataSource = vbNull
                GridView2.Columns.Clear()

                GridControl2.DataSource = SQL("select * from vw_Ruta_Vendedor where Ruta <> '-'", "tbl_vendedor", My.Settings.SolIndustrialCNX).Tables(0)
                GridView2.BestFitColumns()
                GridView2.GroupPanelText = "Vendedores Asignados a ruta"

                GridControl1.Dock = DockStyle.Top
                GridControl2.Dock = DockStyle.Fill
                GridControl2.Visible = True

            Case 4 'Historial

                GridControl2.DataSource = vbNull
                GridView2.Columns.Clear()

                GridControl2.DataSource = SQL("select * from vw_Vendedor_Historial", "tbl_Historial", My.Settings.SolIndustrialCNX).Tables(0)
                GridView2.BestFitColumns()
                GridView2.GroupPanelText = "Historial de Vendedores"
                GridControl1.Dock = DockStyle.Top
                GridControl2.Dock = DockStyle.Fill
                GridControl2.Visible = True

        End Select
        Try
            GridView1.Columns("Pendiente").Visible = False
        Catch ex As Exception
        End Try
        frmPrincipal.bbiNuevo.Enabled = False
    End Sub

    Sub Vnuevo()
        Select Case Categoria
            Case 1
                Edicion = 1
                frmVehiculo_Edicion.ShowDialog()
                retriveV()
        End Select
    End Sub
    Sub Veditar()
        If GridView1.RowCount > 0 Then
            Select Case Categoria
                Case 1
                    Edicion = 2
                    frmVehiculo_Edicion.ShowDialog()
                    retriveV()

                Case 2
                    Edicion = 2
                    FrmVasignacion.ShowDialog()
                    retriveA()

                Case 3
                    Edicion = 2
                    Asignacion_Vendedor.ShowDialog()
                    retriveVen()
            End Select

        Else
            MsgBox("No ha seleccionado registro")
        End If
    End Sub
    Sub Vborrar()
        Select Case MsgBox("Borrar registro de vehiculo?", MsgBoxStyle.YesNo, "")
            Case MsgBoxResult.Yes
                Edicion = 3
                Dim codigo As String = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                Dim resum = EditarVehiculo(Edicion, codigo, "", "", "", Today, 0, 0, 0, 0)
                If resum = "OK" Then
                    MsgBox("Registro Borrado")
                    retriveV()
                End If
            Case MsgBoxResult.No
        End Select
    End Sub
    Public Function EditarVehiculo(ByVal edicion As Integer, codigo As String, placa As String, marca As String, modelo As String, fecha As Date, odoI As Decimal,
                                   odoF As Decimal, estado As Integer, tipo As Integer)
        Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
        con.Open()
        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_VEHICULO", con)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            With cmdComisiones.Parameters
                .AddWithValue("@edicion", SqlDbType.Int).Value = edicion
                .AddWithValue("@id_vehiculo", SqlDbType.VarChar).Value = codigo
                .AddWithValue("@placa", SqlDbType.VarChar).Value = placa
                .AddWithValue("@marca", SqlDbType.VarChar).Value = marca
                .AddWithValue("@modelo", SqlDbType.VarChar).Value = modelo
                .AddWithValue("@fecha", SqlDbType.Date).Value = fecha
                .AddWithValue("@odoI", SqlDbType.VarChar).Value = odoI
                .AddWithValue("@odoF", SqlDbType.VarChar).Value = odoF
                .AddWithValue("@estado", SqlDbType.VarChar).Value = estado
                .AddWithValue("@tipo", SqlDbType.Int).Value = tipo
                .AddWithValue("@sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
            End With
            cmdComisiones.ExecuteReader()
            EditarVehiculo = "OK"
        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarVehiculo = "ERROR"
            con.Close()
            con.Dispose()
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Function
    Public Function EditarAsignacion(ByVal edicion As Integer, codigo As String, Coordinador As String, Supervisor As String, Ruta As String, Nombre As String, Activo As Boolean)
        Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
        con.Open()
        Try
            Dim cmdcomisiones As New SqlCommand("SP_AME_Vasignacion", con)
            cmdcomisiones.CommandType = CommandType.StoredProcedure
            With cmdcomisiones.Parameters
                .AddWithValue("@edicion", SqlDbType.Int).Value = edicion
                .AddWithValue("@codigo", SqlDbType.VarChar).Value = codigo
                .AddWithValue("@coordinador", SqlDbType.VarChar).Value = Coordinador
                .AddWithValue("@supervisor", SqlDbType.VarChar).Value = Supervisor
                .AddWithValue("@ruta", SqlDbType.VarChar).Value = Ruta
                .AddWithValue("@sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
                .AddWithValue("@nombre", SqlDbType.VarChar).Value = Nombre
                .AddWithValue("@activo", SqlDbType.Bit).Value = Activo
            End With
            cmdcomisiones.ExecuteReader()
            EditarAsignacion = "OK"
        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarAsignacion = "ERROR"
            con.Close()
            con.Dispose()
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Function

    Private Sub Expand(ByVal activo As Boolean)
        nbgVehiculo.Expanded = activo
        nbgAsignacion.Expanded = activo
        nbgVendedor.Expanded = activo
        nbgMantenimiento.Expanded = activo

    End Sub

    Private Sub frmVehiculo_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        frmPrincipal.bbiNuevo.Enabled = True
    End Sub
    Private Sub nbiVtodo_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVtodo.LinkPressed
        SubCat = 1
        retriveV()
    End Sub

    Private Sub nbiVoperativo_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVoperativo.LinkPressed
        SubCat = 2
        retriveV()
    End Sub
    Private Sub nbiVadministrativo_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVadministrativo.LinkPressed
        SubCat = 3
        retriveV()
    End Sub
    Private Sub nbiVhistorial_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVhistorial.LinkPressed
        SubCat = 4
        retriveV()
    End Sub
    Private Sub nbiAtodo_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiAtodo.LinkPressed
        SubCat = 1
        retriveA()
    End Sub
    Private Sub nviAsinAsignar_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nviAsinAsignar.LinkPressed
        SubCat = 2
        retriveA()
    End Sub
    Private Sub nbiAsignado_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiAsignado.LinkPressed
        SubCat = 3
        retriveA()
    End Sub
    Private Sub nbiAhistorial_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiAhistorial.LinkPressed
        SubCat = 4
        retriveA()
    End Sub
   
    Private Sub nbiVeTodo_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVeTodo.LinkPressed
        SubCat = 1
        retriveVen()
    End Sub

    Private Sub nbiVesinAsignar_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVesinAsignar.LinkPressed
        SubCat = 2
        retriveVen()
    End Sub

    Private Sub nbiVeasignados_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVeasignados.LinkPressed
        SubCat = 3
        retriveVen()
    End Sub

    Private Sub nbiVehistorial_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiVehistorial.LinkPressed
        SubCat = 4
        retriveVen()
    End Sub
End Class