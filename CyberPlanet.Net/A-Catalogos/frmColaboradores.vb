﻿Imports DevExpress.XtraEditors.Controls

Public Class frmColaboradores

    Private vsalario As New Validar With {.CantDecimal = 2}

    Private Sub frmColaboradores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarProfesiones()
        CargarAreas()
        'CargarDependencias()
        'CargarCargos()
        CargarPlanillas()
        If nTipoEdic = 1 Then
            lblCodColaborador.Visible = True
            txtCodColab.Visible = True
            txtCodColab.Text = RegistroMaximo()
            txtNomColab.Focus()
            deFechaIng.DateTime = Now
            deFechaBaja.DateTime = Now
            ckIsActivo.Checked = 1
        ElseIf nTipoEdic = 2 Then
            lblCodColaborador.Visible = True
            ' txtCodColab.Enabled = False
            txtCodColab.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 4 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If

        AddHandler luArea.EditValueChanged, AddressOf CargarCargos

        vsalario.agregar_control(New List(Of Control)({txtSalarioMensCS, txtSalarioMensUS})).ActivarEventosNumericos()

    End Sub

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        'strsql = " SELECT dbo.Tbl_Empleados.id_empleado, dbo.Tbl_Empleados.Num_Identificacion, dbo.Tbl_Empleados.Num_SeguroSocial, dbo.Tbl_Empleados.Nombres, dbo.Tbl_Empleados.Apellidos, dbo.Tbl_Empleados.Telefono_Domiciliar, dbo.Tbl_Empleados.Telefono_Personal1, dbo.Tbl_Empleados.Telefono_Personal2, dbo.Tbl_Empleados.Direccion_Domiciliar, "
        'strsql = strsql & " dbo.Tbl_Profesiones.Id_Profesion, dbo.Tbl_AreasEmpresa.Id_Area, dbo.Tbl_Cargos.NombreCargo, dbo.Tbl_TiposPlanillas.Id_TipoPlanilla, dbo.Tbl_Empleados.Salario_MensualCS, dbo.Tbl_Empleados.EsDolarizado, dbo.Tbl_Empleados.Salario_MensualUSS, dbo.Tbl_Empleados.Activo, dbo.Tbl_Empleados.Fecha_Ingreso, dbo.Tbl_Empleados.Fecha_Baja, dbo.Tbl_Empleados.Fotografia "
        'strsql = strsql & " FROM dbo.Tbl_Empleados INNER JOIN dbo.Tbl_Profesiones ON dbo.Tbl_Empleados.Id_Profesion = dbo.Tbl_Profesiones.Id_Profesion INNER JOIN dbo.Tbl_AreasEmpresa ON dbo.Tbl_Empleados.Id_Area = dbo.Tbl_AreasEmpresa.Id_Area INNER JOIN dbo.Tbl_Cargos ON dbo.Tbl_Empleados.Id_Cargo = dbo.Tbl_Cargos.Id_Cargo INNER JOIN dbo.Tbl_TiposPlanillas ON dbo.Tbl_Empleados.Id_TipoPlanilla = dbo.Tbl_TiposPlanillas.Id_TipoPlanilla WHERE dbo.Tbl_Empleados.id_empleado =" & CodigoEntidad & ""

        Dim JustAnotherFuckSql = String.Format("SELECT dbo.Tbl_Empleados.id_empleado, 
	                                           dbo.Tbl_Empleados.Num_Identificacion, 
	                                           dbo.Tbl_Empleados.Num_SeguroSocial,
	                                           dbo.Tbl_Empleados.Nombres, 
	                                           dbo.Tbl_Empleados.Apellidos, 
	                                           dbo.Tbl_Empleados.Telefono_Domiciliar, 
	                                           dbo.Tbl_Empleados.Telefono_Personal1, 
	                                           dbo.Tbl_Empleados.Telefono_Personal2, 
	                                           dbo.Tbl_Empleados.Direccion_Domiciliar,
	                                           Id_Profesion, 
	                                           Id_Area, 
	                                           Id_Cargo, 
	                                           Id_TipoPlanilla, 
	                                           dbo.Tbl_Empleados.Salario_MensualCS, 
	                                           dbo.Tbl_Empleados.EsDolarizado, 
	                                           dbo.Tbl_Empleados.Salario_MensualUSS, 
	                                           dbo.Tbl_Empleados.Activo, 
	                                           dbo.Tbl_Empleados.Fecha_Ingreso, 
	                                           dbo.Tbl_Empleados.Fecha_Baja, 
	                                           dbo.Tbl_Empleados.Fotografia 
	                                           FROM dbo.Tbl_Empleados 
	                                           WHERE dbo.Tbl_Empleados.id_empleado = {0}", CodigoEntidad)

        Dim tblDatosColaborador As DataTable = SQL(JustAnotherFuckSql, "tblDatosColaborador", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosColaborador.Rows.Count <> 0 Then
            txtCodColab.Text = tblDatosColaborador.Rows(0).Item(0)
            txtNomColab.Text = tblDatosColaborador.Rows(0).Item(3)
            txtApellidoColab.Text = tblDatosColaborador.Rows(0).Item(4)
            txtTelefonoDom.Text = tblDatosColaborador.Rows(0).Item(5)
            txtSalarioMensCS.Text = tblDatosColaborador.Rows(0).Item(13)
            ckIsActivo.Checked = tblDatosColaborador.Rows(0).Item("Activo")
            deFechaIng.DateTime = IIf(IsDBNull(tblDatosColaborador.Rows(0).Item("Fecha_Ingreso")), Now, tblDatosColaborador.Rows(0).Item("Fecha_Ingreso"))

            txtNumID.Text = tblDatosColaborador.Rows(0).Item("Num_Identificacion")

            txtNumINSS.Text = tblDatosColaborador.Rows(0).Item("Num_SeguroSocial")

            txtDireccion.Text = tblDatosColaborador.Rows(0).Item("Direccion_Domiciliar")

            txtTelefonoDom.Text = IIf(IsDBNull(tblDatosColaborador.Rows(0).Item("Telefono_Domiciliar")), Nothing, tblDatosColaborador.Rows(0).Item("Telefono_Domiciliar"))

            txtTelefonoPers1.Text = IIf(IsDBNull(tblDatosColaborador.Rows(0).Item("Telefono_Personal1")), Nothing, tblDatosColaborador.Rows(0).Item("Telefono_Personal1"))

            txtTelefonoPers2.Text = IIf(IsDBNull(tblDatosColaborador.Rows(0).Item("Telefono_Personal2")), Nothing, tblDatosColaborador.Rows(0).Item("Telefono_Personal2"))

            luProfesion.EditValue = tblDatosColaborador.Rows(0).Item("Id_Profesion")

            luArea.EditValue = tblDatosColaborador.Rows(0).Item("Id_Area")

            luCargo.EditValue = tblDatosColaborador.Rows(0).Item("Id_Cargo")

            luPlanillaCalculo.EditValue = tblDatosColaborador.Rows(0).Item("Id_TipoPlanilla")

            If IsDBNull(tblDatosColaborador.Rows(0).Item("Fecha_Baja")) = False Then
                deFechaBaja.DateTime = IIf(IsDBNull(tblDatosColaborador.Rows(0).Item("Fecha_Baja")), Now, tblDatosColaborador.Rows(0).Item("Fecha_Baja"))
            End If

            'deFechaBaja.DateTime = IIf(tblDatosColaborador.Rows(0).Item(6), Now, tblDatosColaborador.Rows(0).Item(6))
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        If txtNumID.Text <> "" And txtNumINSS.Text <> "" And txtNomColab.Text <> "" And txtApellidoColab.Text <> "" And txtSalarioMensCS.Text <> "" Then
            Dim Resum As String = ""
            Dim CatalogoProv As New clsCatalogos(My.Settings.SolIndustrialCNX)
            Dim dependencia As Integer
            If luDependencia.ItemIndex > -1 Then
                dependencia = luDependencia.EditValue
            Else
                dependencia = vbNull
            End If
            Try
                Resum = CatalogoProv.EditarColaboradores(CInt(IIf(txtCodColab.Text = "", 0, txtCodColab.Text)), txtNomColab.Text.ToUpper, txtApellidoColab.Text.ToUpper, txtTelefonoDom.Text, txtSalarioMensCS.Text, nTipoEdic, txtNumID.Text,
                                                         txtNumINSS.Text, luProfesion.EditValue, luArea.EditValue, luCargo.EditValue, ckIsActivo.Checked, dependencia, txtDireccion.Text, deFechaIng.Text)
                If Resum = "OK" Then
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            Catch ex As Exception
                Call MsgBox("Error: " + ex.Message)
            Finally
                Cerrar()
                If NumCatalogo_Ant = 0 Then
                    frmPrincipal.CargarDatos()
                End If
            End Try
        Else
            MsgBox("Campos faltantes")
        End If
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub browseButton_Click(sender As Object, e As EventArgs) Handles browseButton.Click
        Dim files As New OpenFileDialog()
        files.Filter = "Archivo JPG|*.jpg"
        If files.ShowDialog() = DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(files.FileName)
        End If
    End Sub

    Private Sub CargarProfesiones()
        TblProfesionesBS.DataSource = SQL("select Id_Profesion,Nombre_Profesion from Tbl_Profesiones order by Id_Profesion", "tblProfesiones", My.Settings.SolIndustrialCNX).Tables(0)
        luProfesion.Properties.DataSource = TblProfesionesBS
        luProfesion.Properties.DisplayMember = "Nombre_Profesion"
        luProfesion.Properties.ValueMember = "Id_Profesion"

        luProfesion.Properties.Columns.Add(New LookUpColumnInfo("Id_Profesion", "id", 20))

        luProfesion.Properties.Columns.Add(New LookUpColumnInfo("Nombre_Profesion", "Descripcion", 100))

        luProfesion.Properties.Columns(0).Visible = False

        luProfesion.EditValue = 0

        luProfesion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub

    Private Sub CargarAreas()
        TblAreasBS.DataSource = SQL("select Id_Area,Nombre_Area from Tbl_AreasEmpresa order by Id_Area", "tblAreas", My.Settings.SolIndustrialCNX).Tables(0)
        luArea.Properties.DataSource = TblAreasBS
        luArea.Properties.DisplayMember = "Nombre_Area"
        luArea.Properties.ValueMember = "Id_Area"

        luArea.Properties.Columns.Add(New LookUpColumnInfo("Id_Area", "id", 20))

        luArea.Properties.Columns.Add(New LookUpColumnInfo("Nombre_Area", "Descripcion", 100))

        luArea.Properties.Columns(0).Visible = False

        luArea.EditValue = 0

        luArea.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub
    Private Sub CargarDependencias()
        TblDependenciasBS.DataSource = SQL("select Id_Empleado,Nombres from Tbl_Empleados where Id_Cargo=(select Dependencia from Tbl_Cargos where Id_Cargo='" & luCargo.EditValue & "')", "Dependencias", My.Settings.SolIndustrialCNX).Tables(0)
        With luDependencia.Properties
            .DataSource = TblDependenciasBS
            .DisplayMember = "Nombres"
            .ValueMember = "Id_Empleado"
            luDependencia.ItemIndex = 0
        End With
    End Sub
    Private Sub CargarCargos()

        TblCargosBS.DataSource = SQL(String.Format("select Id_Cargo,NombreCargo from Tbl_Cargos where Id_Area={0}", luArea.EditValue), "tblDepartamentos", My.Settings.SolIndustrialCNX).Tables(0)

        luCargo.Properties.DataSource = TblCargosBS
        luCargo.Properties.DisplayMember = "NombreCargo"
        luCargo.Properties.ValueMember = "Id_Cargo"

        luCargo.Properties.PopulateViewColumns()

        luCargo.Properties.View.Columns("Id_Cargo").Visible = False

        luCargo.Properties.View.OptionsView.ShowColumnHeaders = False

        luCargo.Properties.View.BorderStyle = BorderStyles.NoBorder

        'cargo_empleado.Properties.Columns.Add(New LookUpColumnInfo("Id_Cargo", "id", 20))

        'cargo_empleado.Properties.Columns.Add(New LookUpColumnInfo("NombreCargo", "Descripcion", 100))

        'cargo_empleado.Properties.Columns(0).Visible = False

        luCargo.EditValue = 0

        luCargo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub

    Private Sub CargarPlanillas()
        TblPlanillasBS.DataSource = SQL("select Id_TipoPlanilla,Nombre_TipoPlanilla from Tbl_TiposPlanillas order by Id_TipoPlanilla", "tblDepartamentos", My.Settings.SolIndustrialCNX).Tables(0)
        luPlanillaCalculo.Properties.DataSource = TblPlanillasBS
        luPlanillaCalculo.Properties.DisplayMember = "Nombre_TipoPlanilla"
        luPlanillaCalculo.Properties.ValueMember = "Id_TipoPlanilla"

        luPlanillaCalculo.Properties.Columns.Add(New LookUpColumnInfo("Id_TipoPlanilla", "id", 20))

        luPlanillaCalculo.Properties.Columns.Add(New LookUpColumnInfo("Nombre_TipoPlanilla", "Descripcion", 100))

        luPlanillaCalculo.Properties.Columns(0).Visible = False

        luPlanillaCalculo.EditValue = 1

        luPlanillaCalculo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub
    Private Sub luArea_EditValueChanged(sender As Object, e As EventArgs) Handles luArea.EditValueChanged
        CargarCargos()
    End Sub

    Private Sub ckIsActivo_CheckedChanged(sender As Object, e As EventArgs) Handles ckIsActivo.CheckedChanged
        If ckIsActivo.Checked = True Then
            deFechaBaja.Enabled = False
        Else
            deFechaBaja.Enabled = True
        End If
    End Sub

    Private Sub luCargo_EditValueChanged(sender As Object, e As EventArgs)
        CargarDependencias()
    End Sub

    Private Sub txtSalarioMensCS_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSalarioMensCS.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub

    Private Sub txtTelefonoDom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTelefonoPers2.KeyPress, txtTelefonoPers1.KeyPress, txtTelefonoDom.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar)
    End Sub
End Class