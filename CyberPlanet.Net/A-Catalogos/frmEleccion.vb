﻿Public Class frmEleccion
    Private Sub frmEleccion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If UserPerfil = "0002" Then btnSel2.Enabled = False
    End Sub
    Private Sub btnSel1_Click(sender As Object, e As EventArgs) Handles btnSel1.Click
        Me.Close()
        frmEditProdServ.ShowDialog()
    End Sub
    Private Sub btnSel2_Click(sender As Object, e As EventArgs) Handles btnSel2.Click
        Me.Close()
        frmEditPrecio.ShowDialog()
    End Sub
    Private Sub btnSel3_Click(sender As Object, e As EventArgs) Handles btnSel3.Click
        Me.Close()
    End Sub
    Private Sub btnSel1_MouseHover(sender As Object, e As EventArgs) Handles btnSel1.MouseHover, btnSel1.GotFocus
        pbLogo.BackgroundImage = My.Resources.Product_icon
        pbLogo.BackgroundImageLayout = ImageLayout.Center
    End Sub
    Private Sub btnSel2_MouseHover(sender As Object, e As EventArgs) Handles btnSel2.MouseHover, btnSel2.GotFocus
        pbLogo.BackgroundImage = My.Resources.Price_icon
        pbLogo.BackgroundImageLayout = ImageLayout.Center
    End Sub
    Private Sub frmEleccion_MouseHover(sender As Object, e As EventArgs) Handles MyBase.MouseHover, btnSel3.GotFocus
        pbLogo.BackgroundImage = My.Resources.logo
        pbLogo.BackgroundImageLayout = ImageLayout.Zoom
    End Sub
End Class