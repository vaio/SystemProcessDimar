﻿Public Class frmPresentacion

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo_Presentacion,Descripcion FROM Presentacion WHERE Codigo_Presentacion='" & CodigoEntidad & "'"
        Dim tblDatosPresent As DataTable = SQL(strsql, "tblDatosPresent", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosPresent.Rows.Count <> 0 Then
            txtCodPresent.Text = tblDatosPresent.Rows(0).Item(0)
            txtNomPresent.Text = tblDatosPresent.Rows(0).Item(1)
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub frmPresentacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodPresent.Visible = True
            txtCodPresent.Visible = True
            txtCodPresent.Text = RegistroMaximo()
            txtNomPresent.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodPresent.Visible = True
            txtCodPresent.Enabled = False
            txtCodPresent.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoPresent As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoPresent.EditarPresent(CInt(IIf(txtCodPresent.Text = "", 0, txtCodPresent.Text)), txtNomPresent.Text.ToUpper, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub
End Class