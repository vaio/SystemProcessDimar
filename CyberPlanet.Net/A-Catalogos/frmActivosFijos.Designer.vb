﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditActivosFijos
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditActivosFijos))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.cmdAddDepar = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddCateg = New DevExpress.XtraEditors.SimpleButton()
		Me.luDepart = New DevExpress.XtraEditors.LookUpEdit()
		Me.luCateg = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblCateg = New System.Windows.Forms.Label()
		Me.lblDepart = New System.Windows.Forms.Label()
		Me.txtPrecioMercado = New System.Windows.Forms.TextBox()
		Me.lblPrecioMercado = New System.Windows.Forms.Label()
		Me.cmdAddMarca = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddModelo = New DevExpress.XtraEditors.SimpleButton()
		Me.cmdAddProv = New DevExpress.XtraEditors.SimpleButton()
		Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
		Me.deFechaBaja = New DevExpress.XtraEditors.DateEdit()
		Me.deFechaIng = New DevExpress.XtraEditors.DateEdit()
		Me.lblFechaBaja = New System.Windows.Forms.Label()
		Me.lblFechaIngreso = New System.Windows.Forms.Label()
		Me.luProv = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblProveedor = New System.Windows.Forms.Label()
		Me.luMarca = New DevExpress.XtraEditors.LookUpEdit()
		Me.luModelo = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblModelo = New System.Windows.Forms.Label()
		Me.lblMarca = New System.Windows.Forms.Label()
		Me.txtPeriodoDepre = New System.Windows.Forms.TextBox()
		Me.txtCostoCompra = New System.Windows.Forms.TextBox()
		Me.lblPeriodoDepre = New System.Windows.Forms.Label()
		Me.lblCostoCompra = New System.Windows.Forms.Label()
		Me.lblNomActivoFijo = New System.Windows.Forms.Label()
		Me.txtNomActivoFijo = New System.Windows.Forms.TextBox()
		Me.lblCodActivoFijo = New System.Windows.Forms.Label()
		Me.txtCodActivoFijo = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.TblProvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblDeparBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblCategBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblMarcaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblModeloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.ckIsMobiliarioComputo = New DevExpress.XtraEditors.CheckEdit()
		Me.Panel2.SuspendLayout()
		CType(Me.luDepart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luCateg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luProv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luModelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblDeparBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblCategBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblMarcaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblModeloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ckIsMobiliarioComputo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.ckIsMobiliarioComputo)
		Me.Panel2.Controls.Add(Me.cmdAddDepar)
		Me.Panel2.Controls.Add(Me.cmdAddCateg)
		Me.Panel2.Controls.Add(Me.luDepart)
		Me.Panel2.Controls.Add(Me.luCateg)
		Me.Panel2.Controls.Add(Me.lblCateg)
		Me.Panel2.Controls.Add(Me.lblDepart)
		Me.Panel2.Controls.Add(Me.txtPrecioMercado)
		Me.Panel2.Controls.Add(Me.lblPrecioMercado)
		Me.Panel2.Controls.Add(Me.cmdAddMarca)
		Me.Panel2.Controls.Add(Me.cmdAddModelo)
		Me.Panel2.Controls.Add(Me.cmdAddProv)
		Me.Panel2.Controls.Add(Me.ckIsActivo)
		Me.Panel2.Controls.Add(Me.deFechaBaja)
		Me.Panel2.Controls.Add(Me.deFechaIng)
		Me.Panel2.Controls.Add(Me.lblFechaBaja)
		Me.Panel2.Controls.Add(Me.lblFechaIngreso)
		Me.Panel2.Controls.Add(Me.luProv)
		Me.Panel2.Controls.Add(Me.lblProveedor)
		Me.Panel2.Controls.Add(Me.luMarca)
		Me.Panel2.Controls.Add(Me.luModelo)
		Me.Panel2.Controls.Add(Me.lblModelo)
		Me.Panel2.Controls.Add(Me.lblMarca)
		Me.Panel2.Controls.Add(Me.txtPeriodoDepre)
		Me.Panel2.Controls.Add(Me.txtCostoCompra)
		Me.Panel2.Controls.Add(Me.lblPeriodoDepre)
		Me.Panel2.Controls.Add(Me.lblCostoCompra)
		Me.Panel2.Controls.Add(Me.lblNomActivoFijo)
		Me.Panel2.Controls.Add(Me.txtNomActivoFijo)
		Me.Panel2.Controls.Add(Me.lblCodActivoFijo)
		Me.Panel2.Controls.Add(Me.txtCodActivoFijo)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(569, 234)
		Me.Panel2.TabIndex = 4
		'
		'cmdAddDepar
		'
		Me.cmdAddDepar.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddDepar.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddDepar.Appearance.Options.UseFont = True
		Me.cmdAddDepar.Appearance.Options.UseForeColor = True
		Me.cmdAddDepar.Location = New System.Drawing.Point(259, 127)
		Me.cmdAddDepar.Name = "cmdAddDepar"
		Me.cmdAddDepar.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddDepar.TabIndex = 36
		Me.cmdAddDepar.Text = "+"
		Me.cmdAddDepar.ToolTip = "Agregar un nuevo Departamento"
		'
		'cmdAddCateg
		'
		Me.cmdAddCateg.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddCateg.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddCateg.Appearance.Options.UseFont = True
		Me.cmdAddCateg.Appearance.Options.UseForeColor = True
		Me.cmdAddCateg.Location = New System.Drawing.Point(525, 127)
		Me.cmdAddCateg.Name = "cmdAddCateg"
		Me.cmdAddCateg.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddCateg.TabIndex = 35
		Me.cmdAddCateg.Text = "+"
		Me.cmdAddCateg.ToolTip = "Agregar una nueva Categoría"
		'
		'luDepart
		'
		Me.luDepart.Location = New System.Drawing.Point(99, 127)
		Me.luDepart.Name = "luDepart"
		Me.luDepart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luDepart.Size = New System.Drawing.Size(159, 20)
		Me.luDepart.TabIndex = 31
		'
		'luCateg
		'
		Me.luCateg.Location = New System.Drawing.Point(366, 127)
		Me.luCateg.Name = "luCateg"
		Me.luCateg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luCateg.Size = New System.Drawing.Size(159, 20)
		Me.luCateg.TabIndex = 32
		'
		'lblCateg
		'
		Me.lblCateg.Location = New System.Drawing.Point(304, 130)
		Me.lblCateg.Name = "lblCateg"
		Me.lblCateg.Size = New System.Drawing.Size(61, 20)
		Me.lblCateg.TabIndex = 34
		Me.lblCateg.Text = "Categoría:"
		'
		'lblDepart
		'
		Me.lblDepart.Location = New System.Drawing.Point(21, 130)
		Me.lblDepart.Name = "lblDepart"
		Me.lblDepart.Size = New System.Drawing.Size(77, 20)
		Me.lblDepart.TabIndex = 33
		Me.lblDepart.Text = "Departamento:"
		'
		'txtPrecioMercado
		'
		Me.txtPrecioMercado.Location = New System.Drawing.Point(264, 88)
		Me.txtPrecioMercado.Name = "txtPrecioMercado"
		Me.txtPrecioMercado.Size = New System.Drawing.Size(68, 20)
		Me.txtPrecioMercado.TabIndex = 29
		'
		'lblPrecioMercado
		'
		Me.lblPrecioMercado.Location = New System.Drawing.Point(184, 91)
		Me.lblPrecioMercado.Name = "lblPrecioMercado"
		Me.lblPrecioMercado.Size = New System.Drawing.Size(85, 20)
		Me.lblPrecioMercado.TabIndex = 30
		Me.lblPrecioMercado.Text = "Precio Mercado :"
		'
		'cmdAddMarca
		'
		Me.cmdAddMarca.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddMarca.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddMarca.Appearance.Options.UseFont = True
		Me.cmdAddMarca.Appearance.Options.UseForeColor = True
		Me.cmdAddMarca.Location = New System.Drawing.Point(259, 163)
		Me.cmdAddMarca.Name = "cmdAddMarca"
		Me.cmdAddMarca.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddMarca.TabIndex = 28
		Me.cmdAddMarca.Text = "+"
		Me.cmdAddMarca.ToolTip = "Agregar una nueva Marca"
		'
		'cmdAddModelo
		'
		Me.cmdAddModelo.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddModelo.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddModelo.Appearance.Options.UseFont = True
		Me.cmdAddModelo.Appearance.Options.UseForeColor = True
		Me.cmdAddModelo.Location = New System.Drawing.Point(524, 165)
		Me.cmdAddModelo.Name = "cmdAddModelo"
		Me.cmdAddModelo.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddModelo.TabIndex = 26
		Me.cmdAddModelo.Text = "+"
		Me.cmdAddModelo.ToolTip = "Agregar un nuevo Modelo"
		'
		'cmdAddProv
		'
		Me.cmdAddProv.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddProv.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddProv.Appearance.Options.UseFont = True
		Me.cmdAddProv.Appearance.Options.UseForeColor = True
		Me.cmdAddProv.Location = New System.Drawing.Point(525, 88)
		Me.cmdAddProv.Name = "cmdAddProv"
		Me.cmdAddProv.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddProv.TabIndex = 24
		Me.cmdAddProv.Text = "+"
		Me.cmdAddProv.ToolTip = "Agregar un nuevo Proveedor"
		'
		'ckIsActivo
		'
		Me.ckIsActivo.Enabled = False
		Me.ckIsActivo.Location = New System.Drawing.Point(257, 21)
		Me.ckIsActivo.Name = "ckIsActivo"
		Me.ckIsActivo.Properties.Caption = "Esta Activo"
		Me.ckIsActivo.Size = New System.Drawing.Size(75, 19)
		Me.ckIsActivo.TabIndex = 4
		'
		'deFechaBaja
		'
		Me.deFechaBaja.EditValue = Nothing
		Me.deFechaBaja.Location = New System.Drawing.Point(365, 197)
		Me.deFechaBaja.Name = "deFechaBaja"
		Me.deFechaBaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaBaja.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaBaja.Size = New System.Drawing.Size(180, 20)
		Me.deFechaBaja.TabIndex = 13
		'
		'deFechaIng
		'
		Me.deFechaIng.EditValue = Nothing
		Me.deFechaIng.Location = New System.Drawing.Point(99, 197)
		Me.deFechaIng.Name = "deFechaIng"
		Me.deFechaIng.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaIng.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaIng.Size = New System.Drawing.Size(180, 20)
		Me.deFechaIng.TabIndex = 12
		'
		'lblFechaBaja
		'
		Me.lblFechaBaja.Location = New System.Drawing.Point(303, 200)
		Me.lblFechaBaja.Name = "lblFechaBaja"
		Me.lblFechaBaja.Size = New System.Drawing.Size(61, 17)
		Me.lblFechaBaja.TabIndex = 23
		Me.lblFechaBaja.Text = "F. Baja:"
		'
		'lblFechaIngreso
		'
		Me.lblFechaIngreso.Location = New System.Drawing.Point(21, 200)
		Me.lblFechaIngreso.Name = "lblFechaIngreso"
		Me.lblFechaIngreso.Size = New System.Drawing.Size(61, 17)
		Me.lblFechaIngreso.TabIndex = 22
		Me.lblFechaIngreso.Text = "F. Ingreso:"
		'
		'luProv
		'
		Me.luProv.Location = New System.Drawing.Point(400, 88)
		Me.luProv.Name = "luProv"
		Me.luProv.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luProv.Size = New System.Drawing.Size(125, 20)
		Me.luProv.TabIndex = 7
		'
		'lblProveedor
		'
		Me.lblProveedor.Location = New System.Drawing.Point(344, 90)
		Me.lblProveedor.Name = "lblProveedor"
		Me.lblProveedor.Size = New System.Drawing.Size(61, 17)
		Me.lblProveedor.TabIndex = 20
		Me.lblProveedor.Text = "Proveedor:"
		'
		'luMarca
		'
		Me.luMarca.Location = New System.Drawing.Point(99, 162)
		Me.luMarca.Name = "luMarca"
		Me.luMarca.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luMarca.Size = New System.Drawing.Size(159, 20)
		Me.luMarca.TabIndex = 10
		'
		'luModelo
		'
		Me.luModelo.Location = New System.Drawing.Point(365, 166)
		Me.luModelo.Name = "luModelo"
		Me.luModelo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luModelo.Size = New System.Drawing.Size(159, 20)
		Me.luModelo.TabIndex = 11
		'
		'lblModelo
		'
		Me.lblModelo.Location = New System.Drawing.Point(303, 169)
		Me.lblModelo.Name = "lblModelo"
		Me.lblModelo.Size = New System.Drawing.Size(61, 17)
		Me.lblModelo.TabIndex = 18
		Me.lblModelo.Text = "Modelo:"
		'
		'lblMarca
		'
		Me.lblMarca.Location = New System.Drawing.Point(21, 162)
		Me.lblMarca.Name = "lblMarca"
		Me.lblMarca.Size = New System.Drawing.Size(77, 20)
		Me.lblMarca.TabIndex = 17
		Me.lblMarca.Text = "Marca:"
		'
		'txtPeriodoDepre
		'
		Me.txtPeriodoDepre.Location = New System.Drawing.Point(503, 21)
		Me.txtPeriodoDepre.Name = "txtPeriodoDepre"
		Me.txtPeriodoDepre.Size = New System.Drawing.Size(42, 20)
		Me.txtPeriodoDepre.TabIndex = 6
		Me.txtPeriodoDepre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'txtCostoCompra
		'
		Me.txtCostoCompra.Location = New System.Drawing.Point(99, 90)
		Me.txtCostoCompra.Name = "txtCostoCompra"
		Me.txtCostoCompra.Size = New System.Drawing.Size(68, 20)
		Me.txtCostoCompra.TabIndex = 5
		'
		'lblPeriodoDepre
		'
		Me.lblPeriodoDepre.Location = New System.Drawing.Point(373, 23)
		Me.lblPeriodoDepre.Name = "lblPeriodoDepre"
		Me.lblPeriodoDepre.Size = New System.Drawing.Size(130, 19)
		Me.lblPeriodoDepre.TabIndex = 9
		Me.lblPeriodoDepre.Text = "Periodo de Depreciacion:"
		'
		'lblCostoCompra
		'
		Me.lblCostoCompra.Location = New System.Drawing.Point(21, 91)
		Me.lblCostoCompra.Name = "lblCostoCompra"
		Me.lblCostoCompra.Size = New System.Drawing.Size(89, 20)
		Me.lblCostoCompra.TabIndex = 8
		Me.lblCostoCompra.Text = "Costo Compra:"
		'
		'lblNomActivoFijo
		'
		Me.lblNomActivoFijo.Location = New System.Drawing.Point(21, 57)
		Me.lblNomActivoFijo.Name = "lblNomActivoFijo"
		Me.lblNomActivoFijo.Size = New System.Drawing.Size(117, 20)
		Me.lblNomActivoFijo.TabIndex = 3
		Me.lblNomActivoFijo.Text = "Nombre del Activo Fijo:"
		'
		'txtNomActivoFijo
		'
		Me.txtNomActivoFijo.Location = New System.Drawing.Point(144, 57)
		Me.txtNomActivoFijo.Name = "txtNomActivoFijo"
		Me.txtNomActivoFijo.Size = New System.Drawing.Size(226, 20)
		Me.txtNomActivoFijo.TabIndex = 2
		'
		'lblCodActivoFijo
		'
		Me.lblCodActivoFijo.Location = New System.Drawing.Point(21, 24)
		Me.lblCodActivoFijo.Name = "lblCodActivoFijo"
		Me.lblCodActivoFijo.Size = New System.Drawing.Size(117, 20)
		Me.lblCodActivoFijo.TabIndex = 1
		Me.lblCodActivoFijo.Text = "Id_ActivoFijo:"
		'
		'txtCodActivoFijo
		'
		Me.txtCodActivoFijo.Location = New System.Drawing.Point(144, 21)
		Me.txtCodActivoFijo.Name = "txtCodActivoFijo"
		Me.txtCodActivoFijo.Size = New System.Drawing.Size(79, 20)
		Me.txtCodActivoFijo.TabIndex = 1
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 234)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(569, 49)
		Me.Panel1.TabIndex = 5
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(471, 8)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(387, 8)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'ckIsMobiliarioComputo
		'
		Me.ckIsMobiliarioComputo.Enabled = False
		Me.ckIsMobiliarioComputo.Location = New System.Drawing.Point(376, 58)
		Me.ckIsMobiliarioComputo.Name = "ckIsMobiliarioComputo"
		Me.ckIsMobiliarioComputo.Properties.Caption = "Es Mobiliario / Equipo Computo"
		Me.ckIsMobiliarioComputo.Size = New System.Drawing.Size(173, 19)
		Me.ckIsMobiliarioComputo.TabIndex = 37
		'
		'frmEditActivosFijos
		'
		Me.AcceptButton = Me.cmdAceptar
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.CancelButton = Me.cmdCancelar
		Me.ClientSize = New System.Drawing.Size(569, 283)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmEditActivosFijos"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Edición de Activos Fijos"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.luDepart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luCateg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luProv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luModelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblDeparBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblCategBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblMarcaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblModeloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ckIsMobiliarioComputo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents cmdAddMarca As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddModelo As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddProv As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
	Friend WithEvents deFechaBaja As DevExpress.XtraEditors.DateEdit
	Friend WithEvents deFechaIng As DevExpress.XtraEditors.DateEdit
	Friend WithEvents lblFechaBaja As System.Windows.Forms.Label
	Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
	Friend WithEvents luProv As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblProveedor As System.Windows.Forms.Label
	Friend WithEvents luMarca As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents luModelo As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblModelo As System.Windows.Forms.Label
	Friend WithEvents lblMarca As System.Windows.Forms.Label
	Friend WithEvents txtPeriodoDepre As System.Windows.Forms.TextBox
	Friend WithEvents txtCostoCompra As System.Windows.Forms.TextBox
	Friend WithEvents lblPeriodoDepre As System.Windows.Forms.Label
	Friend WithEvents lblCostoCompra As System.Windows.Forms.Label
	Friend WithEvents lblNomActivoFijo As System.Windows.Forms.Label
	Friend WithEvents txtNomActivoFijo As System.Windows.Forms.TextBox
	Friend WithEvents lblCodActivoFijo As System.Windows.Forms.Label
	Friend WithEvents txtCodActivoFijo As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cmdCancelar As System.Windows.Forms.Button
	Friend WithEvents cmdAceptar As System.Windows.Forms.Button
	Friend WithEvents txtPrecioMercado As System.Windows.Forms.TextBox
	Friend WithEvents lblPrecioMercado As System.Windows.Forms.Label
	Friend WithEvents cmdAddDepar As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents cmdAddCateg As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents luDepart As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents luCateg As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblCateg As System.Windows.Forms.Label
	Friend WithEvents lblDepart As System.Windows.Forms.Label
	Friend WithEvents TblProvBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblDeparBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblCategBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblMarcaBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblModeloBindingSource As System.Windows.Forms.BindingSource
 Friend WithEvents ckIsMobiliarioComputo As DevExpress.XtraEditors.CheckEdit
End Class
