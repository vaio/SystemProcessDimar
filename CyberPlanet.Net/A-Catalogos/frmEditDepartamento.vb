﻿Public Class frmEditDepartamento

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo_Linea,Nombre_Linea FROM Linea_Producto WHERE Codigo_Linea='" & CodigoEntidad & "'"
        Dim tblDatosDepart As DataTable = SQL(strsql, "tblDatosDepart", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosDepart.Rows.Count <> 0 Then
            txtCodLinea.Text = tblDatosDepart.Rows(0).Item(0)
            txtNomLinea.Text = tblDatosDepart.Rows(0).Item(1)
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmEditDepartamento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodDepart.Visible = True
            txtCodLinea.Visible = True
            txtCodLinea.Text = RegistroMaximo()
            txtNomLinea.Text = ""
            txtNomLinea.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodDepart.Visible = True
            txtCodLinea.Enabled = False
            txtCodLinea.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoLinea As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoLinea.EditarLinea(CInt(IIf(txtCodLinea.Text = "", 0, txtCodLinea.Text)), txtNomLinea.Text.ToUpper, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

End Class