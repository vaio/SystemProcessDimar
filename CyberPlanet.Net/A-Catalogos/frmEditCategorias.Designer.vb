﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditCategorias
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditCategorias))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdAddDepar = New DevExpress.XtraEditors.SimpleButton()
        Me.luLinea = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblDepartAsoc = New System.Windows.Forms.Label()
        Me.lblNomCategoria = New System.Windows.Forms.Label()
        Me.txtNomCategoria = New System.Windows.Forms.TextBox()
        Me.lblCodCategoria = New System.Windows.Forms.Label()
        Me.txtCodCategoria = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.TblDepartBindingSource = New System.Windows.Forms.BindingSource()
        Me.Panel2.SuspendLayout()
        CType(Me.luLinea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.TblDepartBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdAddDepar)
        Me.Panel2.Controls.Add(Me.luLinea)
        Me.Panel2.Controls.Add(Me.lblDepartAsoc)
        Me.Panel2.Controls.Add(Me.lblNomCategoria)
        Me.Panel2.Controls.Add(Me.txtNomCategoria)
        Me.Panel2.Controls.Add(Me.lblCodCategoria)
        Me.Panel2.Controls.Add(Me.txtCodCategoria)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(364, 130)
        Me.Panel2.TabIndex = 5
        '
        'cmdAddDepar
        '
        Me.cmdAddDepar.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddDepar.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddDepar.Appearance.Options.UseFont = True
        Me.cmdAddDepar.Appearance.Options.UseForeColor = True
        Me.cmdAddDepar.Location = New System.Drawing.Point(330, 17)
        Me.cmdAddDepar.Name = "cmdAddDepar"
        Me.cmdAddDepar.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddDepar.TabIndex = 9
        Me.cmdAddDepar.Text = "+"
        Me.cmdAddDepar.ToolTip = "Agregar un nuevo Departamento"
        '
        'luLinea
        '
        Me.luLinea.Location = New System.Drawing.Point(135, 17)
        Me.luLinea.Name = "luLinea"
        Me.luLinea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luLinea.Size = New System.Drawing.Size(194, 20)
        Me.luLinea.TabIndex = 7
        '
        'lblDepartAsoc
        '
        Me.lblDepartAsoc.Location = New System.Drawing.Point(12, 20)
        Me.lblDepartAsoc.Name = "lblDepartAsoc"
        Me.lblDepartAsoc.Size = New System.Drawing.Size(129, 20)
        Me.lblDepartAsoc.TabIndex = 5
        Me.lblDepartAsoc.Text = "Linea Asociada:"
        '
        'lblNomCategoria
        '
        Me.lblNomCategoria.Location = New System.Drawing.Point(12, 91)
        Me.lblNomCategoria.Name = "lblNomCategoria"
        Me.lblNomCategoria.Size = New System.Drawing.Size(117, 20)
        Me.lblNomCategoria.TabIndex = 3
        Me.lblNomCategoria.Text = "Nombre de Categoria:"
        '
        'txtNomCategoria
        '
        Me.txtNomCategoria.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomCategoria.Location = New System.Drawing.Point(135, 88)
        Me.txtNomCategoria.Name = "txtNomCategoria"
        Me.txtNomCategoria.Size = New System.Drawing.Size(212, 20)
        Me.txtNomCategoria.TabIndex = 2
        '
        'lblCodCategoria
        '
        Me.lblCodCategoria.Location = New System.Drawing.Point(12, 56)
        Me.lblCodCategoria.Name = "lblCodCategoria"
        Me.lblCodCategoria.Size = New System.Drawing.Size(75, 20)
        Me.lblCodCategoria.TabIndex = 1
        Me.lblCodCategoria.Text = "Codigo_Categoria:"
        '
        'txtCodCategoria
        '
        Me.txtCodCategoria.Location = New System.Drawing.Point(135, 53)
        Me.txtCodCategoria.Name = "txtCodCategoria"
        Me.txtCodCategoria.ReadOnly = True
        Me.txtCodCategoria.Size = New System.Drawing.Size(100, 20)
        Me.txtCodCategoria.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 130)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(364, 49)
        Me.Panel1.TabIndex = 4
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(269, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(185, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'frmEditCategorias
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(364, 179)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditCategorias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Categorias"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.luLinea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.TblDepartBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblDepartAsoc As System.Windows.Forms.Label
    Friend WithEvents lblNomCategoria As System.Windows.Forms.Label
    Friend WithEvents txtNomCategoria As System.Windows.Forms.TextBox
    Friend WithEvents lblCodCategoria As System.Windows.Forms.Label
    Friend WithEvents txtCodCategoria As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents luLinea As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblDepartBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents cmdAddDepar As DevExpress.XtraEditors.SimpleButton
End Class
