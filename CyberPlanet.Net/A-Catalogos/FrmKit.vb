﻿Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Partial Public Class FrmKit

    Inherits XtraForm
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim Dt_Catalogo As DataTable
    Dim Dt_Producto As DataTable
    Dim Dt_Kit_Detalle As DataTable
    Dim DR_Kit_Detalle() As DataRow
    Dim Dt_Movimiento As DataTable
    Dim DR_Producto() As DataRow
    Dim Codigo, Alterno, Producto, Id_Cat, Categoria, Id_Mc, Marca, Id_Mo, Modelo, Codigo_Master, Id_Movimiento As String
    Dim Costo As Decimal
    Dim inventario As New clsInventario(My.Settings.SolIndustrialCNX)
    Dim bien As Color = Color.Transparent
    Dim mal As Color = Color.Salmon
    '---
    Private vtxtMcantidad As New Validar With {.CantDecimal = 0}        'funcion roberto


    Private Sub GridControl1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles GridControl1.MouseDoubleClick
        AddRow(GridView1, GridControl1, GridView3, GridControl3)
    End Sub

    Private Sub GridControl3_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles GridControl3.MouseDoubleClick
        AddRow(GridView3, GridControl2, GridView1, GridControl1)
    End Sub
    Public Sub AddRow(ByVal view1 As DevExpress.XtraGrid.Views.Grid.GridView, ByVal grid1 As DevExpress.XtraGrid.GridControl, view2 As DevExpress.XtraGrid.Views.Grid.GridView, ByVal grid2 As DevExpress.XtraGrid.GridControl)

        Dim currentRow = view1.FocusedRowHandle

        Dim newRow As DataRow = (TryCast(grid2.DataSource, DataTable)).NewRow()

        newRow("Codigo") = view1.GetRowCellValue(currentRow, "Codigo")
        newRow("Nombre") = view1.GetRowCellValue(currentRow, "Nombre")

        view1.DeleteRow(currentRow)

        TryCast(grid2.DataSource, DataTable).Rows.Add(newRow)

        grid2.RefreshDataSource()

    End Sub

    Private Sub nbAplicar_ElementClick(sender As Object, e As DevExpress.XtraBars.Navigation.NavElementEventArgs) Handles nbAplicar.ElementClick
        Select Case nFRAME.SelectedPageIndex
            Case 0  'Creacion de kits

                For i = 0 To GridView3.RowCount - 1
                    Detalle(1, GridView3.GetRowCellValue(i, "Codigo"), i)
                Next

                tileBar.SelectedItem = ntbCreados

            Case 2 'Movimiento

                If txtMcantidad.Text <> "" And txtMcantidad.Text <> "0" And txtMmaximo.Properties.AppearanceReadOnly.BackColor = bien Then

                    '----
                    Dim inventario As New clsInventario(My.Settings.SolIndustrialCNX)
                    Try

                        'salida
                        Dim cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(3, 5), 8)
                        Dim ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, "", 17, "SALIDA DE PRODUCTOS KIT", txtMcosto.Text, Date.Now, 5, 0, 1, 3)
                        Dim ResumEntradaDeta

                        For i = 0 To Dt_Kit_Detalle.Rows.Count - 1

                            With Dt_Kit_Detalle

                                Dim M_Producto As String = .Rows(i)("Codigo")
                                Dim M_Cantidad As Decimal = (.Rows(i)("Cantidad") * txtMcantidad.Text)
                                Dim M_Costo As Decimal = .Rows(i)("Costo")

                                ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, M_Producto, M_Cantidad, M_Costo, "", 3, False, 1, 5)
                            End With

                        Next
                        'Entrada
                        cod_Movimiento = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(4, 4), 8)
                        ResumEntrada = inventario.EditarMovimientoAlmacen(cod_Movimiento, My.Settings.Sucursal, "", 17, "ENTRADA DE PRODUCTOS TERMINADOS", txtMcosto.Text, Date.Now, 4, 0, 1, 4)
                        ResumEntradaDeta = inventario.EditarMovimientoAlmacenDetalle(cod_Movimiento, My.Settings.Sucursal, txtM_producto.Text, txtMcantidad.Text, txtMcosto.Text, "", 4, False, 1, 4)

                        tileBar.SelectedItem = ntbCreados
                        'Movimiento_clear(Nothing)
                    Catch ex As Exception
                        MsgBox("error: master salida")
                    End Try
                    '---

                End If

        End Select
    End Sub
    'Sub Movimiento_clear(ByVal value As String)



    'End Sub
    Private Sub nbBorrar_ElementClick(sender As Object, e As DevExpress.XtraBars.Navigation.NavElementEventArgs) Handles nbBorrar.ElementClick

        Detalle(3, Producto, 0)

        nFRAME.SelectedPageIndex = 1
        Seleccion(1)

    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub FrmKit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        nFRAME.SelectedPageIndex = 1
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Seleccion(1)

        vtxtMcantidad.agregar_control(New List(Of Control)({txtMcantidad})).SoloEnteros()   'funcion roberto

    End Sub

    Private Sub GridControl2_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles GridControl2.MouseDoubleClick

        tileBar.SelectedItem = ntbNuevo

        With GridView2
            luProducto.EditValue = .GetRowCellValue(.GetSelectedRows(0), "Producto").ToString()
        End With
        btnCrear.PerformClick()

    End Sub
    Private Sub tileBar_SelectedItemChanged(ByVal sender As Object, ByVal e As TileItemEventArgs) Handles tileBar.SelectedItemChanged
        nFRAME.SelectedPageIndex = tileBarGroupTables.Items.IndexOf(e.Item)
        Seleccion(tileBarGroupTables.Items.IndexOf(e.Item))

    End Sub

    Private Sub GridControl4_MouseClick(sender As Object, e As MouseEventArgs) Handles GridControl4.MouseClick

        With GridView4

            Dt_Kit_Detalle = SQL("select * from vw_Kit_Detalle where Kit='" + .GetRowCellValue(.GetSelectedRows(0), "Kit").ToString() + "'", "tbl_Producto", My.Settings.SolIndustrialCNX).Tables(0)

        End With

        GridControl5.DataSource = vbNull
        GridView5.Columns.Clear()
        GridControl5.DataSource = Dt_Kit_Detalle
        GridView5.Columns("Kit").Visible = False
        GridView5.Columns("Sucursal").Visible = False
        GridView5.BestFitColumns()

        Dim Cantidad_Actual As Decimal
        Dim Costo_Actual As Decimal

        '--------
        With GridView4

            txtM_producto.Text = .GetRowCellValue(.FocusedRowHandle, "Codigo")
            txtM_nombre.Text = .GetRowCellValue(.FocusedRowHandle, "Nombre")
            Cantidad_Actual = .GetRowCellValue(.FocusedRowHandle, "Existencias")
            Costo_Actual = .GetRowCellValue(.FocusedRowHandle, "Costo")
        End With

        txtMAexistencia.Text = Cantidad_Actual
        txtMAcosto.Text = Costo_Actual

        txtMcantidad.Text = 0
        txtMcantidad.Text = 1

    End Sub

    Sub M_Calculo()
        Dim cantidad As Decimal
        Dim Costo As Decimal
        Dim Costo_Total As Decimal
        Dim Cantidad_maxima As Integer = 100000000
        txtMcantidad.BackColor = bien
        txtMmaximo.Properties.AppearanceReadOnly.BackColor = bien

        Try
            cantidad = txtMcantidad.Text
        Catch ex As Exception
            cantidad = 0
        End Try

        For i = 0 To Dt_Kit_Detalle.Rows.Count - 1

            Costo_Total += (Dt_Kit_Detalle.Rows(i)("Costo") * (Dt_Kit_Detalle.Rows(i)("Cantidad")) * cantidad)
            Costo += Dt_Kit_Detalle.Rows(i)("Costo") * (Dt_Kit_Detalle.Rows(i)("Cantidad"))

            If Cantidad_maxima > (Dt_Kit_Detalle.Rows(i)("Existencia") / Dt_Kit_Detalle.Rows(i)("Cantidad")) Then Cantidad_maxima = (Dt_Kit_Detalle.Rows(i)("Existencia") / Dt_Kit_Detalle.Rows(i)("Cantidad"))

        Next

        If cantidad > Cantidad_maxima Then txtMcantidad.BackColor = mal : txtMmaximo.Properties.AppearanceReadOnly.BackColor = mal

        txtMcosto.Text = Costo
        txtMmaximo.Text = Cantidad_maxima
        txtMcostoTotal.Text = Costo_Total


    End Sub
    Private Sub txtMcantidad_EditValueChanged(sender As Object, e As EventArgs) Handles txtMcantidad.EditValueChanged

        M_Calculo()


    End Sub

    Sub Seleccion(index As Integer)

        pBuscar.Visible = False
        pDato.Visible = False
        Pcontrol.Visible = False
        pMovimiento.Visible = False

        Select Case index

            Case 0      'crear/mod. kits
                Carga_Nuevo()
                pBuscar.Visible = True
                Pcontrol.Visible = True
                pBuscar.BringToFront()
                lbNuevo.Visible = True
                GridControl1.Visible = False
                GridControl3.Visible = False

            Case 1      'Catalogo
                Carga_Catalogo()



            Case 2      'Movimiento Inventario

                txtCodigo_Movimiento.Text = Microsoft.VisualBasic.Right("00000000" & inventario.RegMaxMovimiento(3, 5), 8) 'Id_Movimiento
                Carga_Kit_Movimiento()
                pMovimiento.Visible = True
                Pcontrol.Visible = True
                pMovimiento.BringToFront()

        End Select

    End Sub

    Sub Carga_Nuevo()
        Try

            Dt_Producto = SQL("select * from vw_Kit_Buscar", "tbl_Buscar", My.Settings.SolIndustrialCNX).Tables(0)

            luProducto.Properties.Columns.Clear()
            luProducto.Properties.DataSource = Dt_Producto
            luProducto.Properties.DisplayMember = "Nombre"
            luProducto.Properties.ValueMember = "Codigo"
            luProducto.Properties.Columns.Add(New LookUpColumnInfo("Kit", "Kit", 40))
            luProducto.Properties.Columns.Add(New LookUpColumnInfo("Codigo", "Codigo", 50))
            luProducto.Properties.Columns.Add(New LookUpColumnInfo("Nombre", "Nombre", 200))
            'luProducto.Properties.Columns(0).Visible = False
            luProducto.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            'luProducto.ItemIndex = -1

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try
    End Sub

    Sub Carga_Catalogo()

        GridControl2.DataSource = vbNull
        GridView2.Columns.Clear()
        GridControl2.DataSource = SQL("select * from vw_Kit_Catalogo", "tbl_Catalogo", My.Settings.SolIndustrialCNX).Tables(0)
        GridView2.BestFitColumns()
        GridView2.GroupPanelText = "Catalogo"

    End Sub

    Sub Carga_Producto_Disponibles()

        GridControl1.DataSource = vbNull
        GridView1.Columns.Clear()
        GridControl1.DataSource = SQL("select * from vw_Kit_Producto_Inicial where not Kit='" + Codigo_Master + "'", "tbl_Producto", My.Settings.SolIndustrialCNX).Tables(0)
        GridView1.BestFitColumns()
        GridView1.GroupPanelText = "Productos Disponibles"

    End Sub

    Sub Carga_Kit_Movimiento()

        GridControl4.DataSource = vbNull
        GridView4.Columns.Clear()
        GridControl4.DataSource = SQL("select * from vw_Kit_Movimiento_Inicial", "tbl_Producto", My.Settings.SolIndustrialCNX).Tables(0)
        GridView4.Columns("Kit").Visible = False
        GridView4.Columns("Nombre").BestFit()
        GridView4.GroupPanelText = "Kits Creados"

    End Sub
    Sub Carga_Producto_Kit()

        GridControl3.DataSource = vbNull
        GridView3.Columns.Clear()
        'GridControl3.DataSource = SQL("select * from vw_Kit_Producto_Inicial where Kit='" + Codigo_Master + "'" & My.Settings.Sucursal & " and DKIT.Id_Receta='" + Codigo_Master + "'", "tbl_Producto", My.Settings.SolIndustrialCNX).Tables(0)
        GridControl3.DataSource = SQL("select DKIT.id_producto as 'Codigo', PRD.Nombre_Producto as 'Nombre' from tbl_Kit_Detalle DKIT inner join Productos PRD on PRD.Codigo_Producto=DKIT.Id_Producto where DKIT.Id_Sucursal=" & My.Settings.Sucursal & " and DKIT.Id_Receta='" + Codigo_Master + "'", "tbl_Producto", My.Settings.SolIndustrialCNX).Tables(0)
        GridView3.BestFitColumns()
        GridView3.GroupPanelText = "Producto: " + Producto + "                                           " + Codigo_Master + ""

    End Sub
    Private Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        pBuscar.Visible = False
        pDato.Visible = True
        lbNuevo.Visible = False
        GridControl1.Visible = True
        GridControl3.Visible = True


        Dim expression As String = "Codigo ='" + luProducto.EditValue + "'"
        DR_Producto = Dt_Producto.Select(expression)

        Codigo = DR_Producto(0)("Codigo")
        Alterno = DR_Producto(0)("Alterno")
        Producto = DR_Producto(0)("Nombre")
        Id_Cat = DR_Producto(0)("Id_Cat")
        Categoria = DR_Producto(0)("Categoria")
        Id_Mc = DR_Producto(0)("Id_mc")
        Marca = DR_Producto(0)("Marca")
        Id_Mo = DR_Producto(0)("Id_Mo")
        Modelo = DR_Producto(0)("Modelo")
        Costo = DR_Producto(0)("Costo")

        txtCodigo.Text = Codigo
        txtAlterno.Text = Alterno
        txtNombre.Text = Producto
        txtCategoria.Text = Categoria
        txtMarca.Text = Marca
        txtModelo.Text = Modelo
        txtCosto.Text = Costo

        pDato.BringToFront()

        Master(1, Codigo_Master, Codigo, UserID, 1)

        Carga_Producto_Disponibles()

        Carga_Producto_Kit()
    End Sub

    Private Sub Master(ByVal edicion As Integer, codigo As String, producto As String, usuario As String, activo As Boolean)

        Try
            con.Open()

            Dim cmd As New SqlCommand("SP_AME_KIT", con)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd.Parameters

                .AddWithValue("@edicion", SqlDbType.Int).Value = edicion
                .AddWithValue("@sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
                .AddWithValue("@codigo", codigo)
                .AddWithValue("@producto", producto)
                .AddWithValue("@usuario", usuario)
                .AddWithValue("@activo", activo)

                Codigo_Master = cmd.ExecuteScalar

            End With
            cmd.ExecuteReader()
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub

    Private Sub Detalle(ByVal edicion As Integer, prod As String, primer As Integer)

        Try
            con.Open()

            Dim cmd As New SqlCommand("SP_AME_KIT_DETALLE", con)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd.Parameters
                .AddWithValue("@edicion", edicion)
                .AddWithValue("@sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
                .AddWithValue("@codigo", Codigo_Master)
                .AddWithValue("@producto", prod)
                .AddWithValue("@primer", primer)

            End With
            cmd.ExecuteReader()
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub

End Class
