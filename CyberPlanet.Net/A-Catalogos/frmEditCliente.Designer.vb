﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditCliente))
        Me.pMain = New System.Windows.Forms.Panel()
        Me.TabPrincipal = New System.Windows.Forms.TabControl()
        Me.TabPersonal = New System.Windows.Forms.TabPage()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lbClasificacion = New System.Windows.Forms.Label()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.txtMunicipio = New System.Windows.Forms.TextBox()
        Me.txtDistrito = New System.Windows.Forms.TextBox()
        Me.txtBarrio = New System.Windows.Forms.TextBox()
        Me.txtDepartamento = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.dtpFechaF = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.cbColector = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.ckbActivo = New System.Windows.Forms.CheckBox()
        Me.txtDireccion1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtcodigo = New System.Windows.Forms.TextBox()
        Me.txtnombre = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtapellido = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDireccion2 = New System.Windows.Forms.TextBox()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txttelefono2 = New System.Windows.Forms.TextBox()
        Me.txttelefono1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtcorreo1 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtcorreo2 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnBarrio = New DevExpress.XtraEditors.SimpleButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnDepartamento = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDistrito = New DevExpress.XtraEditors.SimpleButton()
        Me.btnMunicipio = New DevExpress.XtraEditors.SimpleButton()
        Me.txtEcodigo = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.lbnuevo = New System.Windows.Forms.Label()
        Me.TabLaboral = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtRapellido2 = New System.Windows.Forms.TextBox()
        Me.txtRtel2 = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtRnombre2 = New System.Windows.Forms.TextBox()
        Me.txtRdireccion2 = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtRapellido1 = New System.Windows.Forms.TextBox()
        Me.txtRtel1 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtRnombre1 = New System.Windows.Forms.TextBox()
        Me.txtRdireccion1 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtcantCredito = New System.Windows.Forms.TextBox()
        Me.txtLimiteCredito = New System.Windows.Forms.TextBox()
        Me.txtSaldo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtSolicitudPendiete = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtTingreso = New System.Windows.Forms.TextBox()
        Me.txtTtelefono = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtTdireccion = New System.Windows.Forms.TextBox()
        Me.txtTcorreo = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TabFiador = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.ComboBoxEdit5 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.TblBarrioBindingSource = New System.Windows.Forms.BindingSource()
        Me.TblZonaDistBindingSource = New System.Windows.Forms.BindingSource()
        Me.TblMunicipioBindingSource = New System.Windows.Forms.BindingSource()
        Me.TblDepartBindingSource = New System.Windows.Forms.BindingSource()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.pMain.SuspendLayout()
        Me.TabPrincipal.SuspendLayout()
        Me.TabPersonal.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbColector.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabLaboral.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabFiador.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBarrioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblZonaDistBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMunicipioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.TabPrincipal)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(782, 621)
        Me.pMain.TabIndex = 0
        '
        'TabPrincipal
        '
        Me.TabPrincipal.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TabPrincipal.Controls.Add(Me.TabPersonal)
        Me.TabPrincipal.Controls.Add(Me.TabLaboral)
        Me.TabPrincipal.Controls.Add(Me.TabFiador)
        Me.TabPrincipal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.TabPrincipal.Name = "TabPrincipal"
        Me.TabPrincipal.SelectedIndex = 0
        Me.TabPrincipal.Size = New System.Drawing.Size(782, 621)
        Me.TabPrincipal.TabIndex = 68
        Me.TabPrincipal.TabStop = False
        '
        'TabPersonal
        '
        Me.TabPersonal.BackColor = System.Drawing.SystemColors.Control
        Me.TabPersonal.Controls.Add(Me.Label18)
        Me.TabPersonal.Controls.Add(Me.lbClasificacion)
        Me.TabPersonal.Controls.Add(Me.GridControl1)
        Me.TabPersonal.Controls.Add(Me.txtMunicipio)
        Me.TabPersonal.Controls.Add(Me.txtDistrito)
        Me.TabPersonal.Controls.Add(Me.txtBarrio)
        Me.TabPersonal.Controls.Add(Me.txtDepartamento)
        Me.TabPersonal.Controls.Add(Me.Label31)
        Me.TabPersonal.Controls.Add(Me.dtpFechaF)
        Me.TabPersonal.Controls.Add(Me.dtpFechaI)
        Me.TabPersonal.Controls.Add(Me.cbColector)
        Me.TabPersonal.Controls.Add(Me.Label26)
        Me.TabPersonal.Controls.Add(Me.Label27)
        Me.TabPersonal.Controls.Add(Me.ckbActivo)
        Me.TabPersonal.Controls.Add(Me.txtDireccion1)
        Me.TabPersonal.Controls.Add(Me.Label15)
        Me.TabPersonal.Controls.Add(Me.Label16)
        Me.TabPersonal.Controls.Add(Me.cbTipo)
        Me.TabPersonal.Controls.Add(Me.Label25)
        Me.TabPersonal.Controls.Add(Me.txtcodigo)
        Me.TabPersonal.Controls.Add(Me.txtnombre)
        Me.TabPersonal.Controls.Add(Me.Label1)
        Me.TabPersonal.Controls.Add(Me.Label2)
        Me.TabPersonal.Controls.Add(Me.txtapellido)
        Me.TabPersonal.Controls.Add(Me.Label28)
        Me.TabPersonal.Controls.Add(Me.Label3)
        Me.TabPersonal.Controls.Add(Me.Label9)
        Me.TabPersonal.Controls.Add(Me.txtDireccion2)
        Me.TabPersonal.Controls.Add(Me.txtCedula)
        Me.TabPersonal.Controls.Add(Me.Label4)
        Me.TabPersonal.Controls.Add(Me.Label12)
        Me.TabPersonal.Controls.Add(Me.Label29)
        Me.TabPersonal.Controls.Add(Me.Label10)
        Me.TabPersonal.Controls.Add(Me.txttelefono2)
        Me.TabPersonal.Controls.Add(Me.txttelefono1)
        Me.TabPersonal.Controls.Add(Me.Label5)
        Me.TabPersonal.Controls.Add(Me.txtcorreo1)
        Me.TabPersonal.Controls.Add(Me.Label7)
        Me.TabPersonal.Controls.Add(Me.Label8)
        Me.TabPersonal.Controls.Add(Me.Label13)
        Me.TabPersonal.Controls.Add(Me.Label30)
        Me.TabPersonal.Controls.Add(Me.txtcorreo2)
        Me.TabPersonal.Controls.Add(Me.Label14)
        Me.TabPersonal.Controls.Add(Me.btnBarrio)
        Me.TabPersonal.Controls.Add(Me.Label6)
        Me.TabPersonal.Controls.Add(Me.btnDepartamento)
        Me.TabPersonal.Controls.Add(Me.btnDistrito)
        Me.TabPersonal.Controls.Add(Me.btnMunicipio)
        Me.TabPersonal.Controls.Add(Me.txtEcodigo)
        Me.TabPersonal.Controls.Add(Me.Label32)
        Me.TabPersonal.Controls.Add(Me.lbnuevo)
        Me.TabPersonal.Location = New System.Drawing.Point(4, 25)
        Me.TabPersonal.Name = "TabPersonal"
        Me.TabPersonal.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPersonal.Size = New System.Drawing.Size(774, 592)
        Me.TabPersonal.TabIndex = 0
        Me.TabPersonal.Text = "Datos Personales"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(380, 211)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(69, 13)
        Me.Label18.TabIndex = 181
        Me.Label18.Text = "Clasificacion:"
        '
        'lbClasificacion
        '
        Me.lbClasificacion.AutoSize = True
        Me.lbClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbClasificacion.Location = New System.Drawing.Point(549, 202)
        Me.lbClasificacion.Name = "lbClasificacion"
        Me.lbClasificacion.Size = New System.Drawing.Size(53, 73)
        Me.lbClasificacion.TabIndex = 180
        Me.lbClasificacion.Text = "-"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GridControl1.Location = New System.Drawing.Point(3, 286)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(768, 303)
        Me.GridControl1.TabIndex = 176
        Me.GridControl1.TabStop = False
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsCustomization.AllowColumnMoving = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsFind.AllowFindPanel = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'txtMunicipio
        '
        Me.txtMunicipio.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtMunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMunicipio.Location = New System.Drawing.Point(455, 100)
        Me.txtMunicipio.Name = "txtMunicipio"
        Me.txtMunicipio.ReadOnly = True
        Me.txtMunicipio.Size = New System.Drawing.Size(228, 20)
        Me.txtMunicipio.TabIndex = 15
        '
        'txtDistrito
        '
        Me.txtDistrito.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtDistrito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDistrito.Location = New System.Drawing.Point(455, 127)
        Me.txtDistrito.Name = "txtDistrito"
        Me.txtDistrito.ReadOnly = True
        Me.txtDistrito.Size = New System.Drawing.Size(228, 20)
        Me.txtDistrito.TabIndex = 16
        '
        'txtBarrio
        '
        Me.txtBarrio.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtBarrio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBarrio.Location = New System.Drawing.Point(455, 153)
        Me.txtBarrio.Name = "txtBarrio"
        Me.txtBarrio.ReadOnly = True
        Me.txtBarrio.Size = New System.Drawing.Size(228, 20)
        Me.txtBarrio.TabIndex = 17
        '
        'txtDepartamento
        '
        Me.txtDepartamento.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtDepartamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartamento.Location = New System.Drawing.Point(455, 74)
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.ReadOnly = True
        Me.txtDepartamento.Size = New System.Drawing.Size(228, 20)
        Me.txtDepartamento.TabIndex = 14
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(400, 182)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(49, 13)
        Me.Label31.TabIndex = 157
        Me.Label31.Text = "Colector:"
        '
        'dtpFechaF
        '
        Me.dtpFechaF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaF.Location = New System.Drawing.Point(607, 23)
        Me.dtpFechaF.Name = "dtpFechaF"
        Me.dtpFechaF.Size = New System.Drawing.Size(102, 20)
        Me.dtpFechaF.TabIndex = 11
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaI.Location = New System.Drawing.Point(455, 23)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(102, 20)
        Me.dtpFechaI.TabIndex = 10
        '
        'cbColector
        '
        Me.cbColector.EditValue = ""
        Me.cbColector.Enabled = False
        Me.cbColector.Location = New System.Drawing.Point(455, 179)
        Me.cbColector.Name = "cbColector"
        Me.cbColector.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbColector.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbColector.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbColector.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbColector.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbColector.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbColector.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbColector.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbColector.Size = New System.Drawing.Size(254, 20)
        Me.cbColector.TabIndex = 18
        Me.cbColector.TabStop = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(648, 6)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(40, 13)
        Me.Label26.TabIndex = 161
        Me.Label26.Text = "Activo:"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(412, 156)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(37, 13)
        Me.Label27.TabIndex = 165
        Me.Label27.Text = "Barrio:"
        '
        'ckbActivo
        '
        Me.ckbActivo.AutoSize = True
        Me.ckbActivo.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbActivo.Checked = True
        Me.ckbActivo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbActivo.Location = New System.Drawing.Point(694, 6)
        Me.ckbActivo.Name = "ckbActivo"
        Me.ckbActivo.Size = New System.Drawing.Size(15, 14)
        Me.ckbActivo.TabIndex = 158
        Me.ckbActivo.TabStop = False
        Me.ckbActivo.UseVisualStyleBackColor = True
        '
        'txtDireccion1
        '
        Me.txtDireccion1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion1.Location = New System.Drawing.Point(94, 179)
        Me.txtDireccion1.MaxLength = 250
        Me.txtDireccion1.Multiline = True
        Me.txtDireccion1.Name = "txtDireccion1"
        Me.txtDireccion1.Size = New System.Drawing.Size(254, 45)
        Me.txtDireccion1.TabIndex = 8
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(418, 52)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(31, 13)
        Me.Label15.TabIndex = 155
        Me.Label15.Text = "Tipo:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(371, 26)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(78, 13)
        Me.Label16.TabIndex = 159
        Me.Label16.Text = "Fecha Ingreso:"
        '
        'cbTipo
        '
        Me.cbTipo.EditValue = "DETALLE"
        Me.cbTipo.Location = New System.Drawing.Point(455, 49)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTipo.Properties.Items.AddRange(New Object() {"DETALLE", "MAYORISTA", "EMPLEADO"})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(102, 20)
        Me.cbTipo.TabIndex = 12
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(570, 26)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(31, 13)
        Me.Label25.TabIndex = 160
        Me.Label25.Text = "Baja:"
        '
        'txtcodigo
        '
        Me.txtcodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtcodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcodigo.Location = New System.Drawing.Point(94, 23)
        Me.txtcodigo.Name = "txtcodigo"
        Me.txtcodigo.ReadOnly = True
        Me.txtcodigo.Size = New System.Drawing.Size(67, 20)
        Me.txtcodigo.TabIndex = 1
        Me.txtcodigo.TabStop = False
        '
        'txtnombre
        '
        Me.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnombre.Location = New System.Drawing.Point(94, 49)
        Me.txtnombre.MaxLength = 50
        Me.txtnombre.Name = "txtnombre"
        Me.txtnombre.Size = New System.Drawing.Size(254, 20)
        Me.txtnombre.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(45, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Codigo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Nombres:"
        '
        'txtapellido
        '
        Me.txtapellido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtapellido.Location = New System.Drawing.Point(94, 75)
        Me.txtapellido.MaxLength = 50
        Me.txtapellido.Name = "txtapellido"
        Me.txtapellido.Size = New System.Drawing.Size(254, 20)
        Me.txtapellido.TabIndex = 3
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(377, 130)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(72, 13)
        Me.Label28.TabIndex = 164
        Me.Label28.Text = "Zona/Distrito:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(41, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 50
        Me.Label3.Text = "Apellido:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(83, 230)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(10, 12)
        Me.Label9.TabIndex = 90
        Me.Label9.Text = "2"
        '
        'txtDireccion2
        '
        Me.txtDireccion2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion2.Location = New System.Drawing.Point(94, 230)
        Me.txtDireccion2.MaxLength = 250
        Me.txtDireccion2.Multiline = True
        Me.txtDireccion2.Name = "txtDireccion2"
        Me.txtDireccion2.Size = New System.Drawing.Size(254, 45)
        Me.txtDireccion2.TabIndex = 9
        '
        'txtCedula
        '
        Me.txtCedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCedula.Location = New System.Drawing.Point(236, 23)
        Me.txtCedula.MaxLength = 14
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(112, 20)
        Me.txtCedula.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(187, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 52
        Me.Label4.Text = "Cedula:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(250, 101)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(10, 12)
        Me.Label12.TabIndex = 89
        Me.Label12.Text = "2"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(394, 104)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(55, 13)
        Me.Label29.TabIndex = 163
        Me.Label29.Text = "Municipio:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(83, 153)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(10, 12)
        Me.Label10.TabIndex = 87
        Me.Label10.Text = "2"
        '
        'txttelefono2
        '
        Me.txttelefono2.Location = New System.Drawing.Point(261, 101)
        Me.txttelefono2.MaxLength = 10
        Me.txttelefono2.Name = "txttelefono2"
        Me.txttelefono2.Size = New System.Drawing.Size(87, 20)
        Me.txttelefono2.TabIndex = 5
        '
        'txttelefono1
        '
        Me.txttelefono1.Location = New System.Drawing.Point(94, 101)
        Me.txttelefono1.MaxLength = 10
        Me.txttelefono1.Name = "txttelefono1"
        Me.txttelefono1.Size = New System.Drawing.Size(87, 20)
        Me.txttelefono1.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 182)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Direccion:"
        '
        'txtcorreo1
        '
        Me.txtcorreo1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtcorreo1.Location = New System.Drawing.Point(94, 127)
        Me.txtcorreo1.MaxLength = 50
        Me.txtcorreo1.Name = "txtcorreo1"
        Me.txtcorreo1.Size = New System.Drawing.Size(254, 20)
        Me.txtcorreo1.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(36, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Telefono:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(203, 104)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 13)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "Telefono:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(47, 130)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 13)
        Me.Label13.TabIndex = 63
        Me.Label13.Text = "Correo:"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(372, 78)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(77, 13)
        Me.Label30.TabIndex = 162
        Me.Label30.Text = "Departamento:"
        '
        'txtcorreo2
        '
        Me.txtcorreo2.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtcorreo2.Location = New System.Drawing.Point(94, 153)
        Me.txtcorreo2.MaxLength = 50
        Me.txtcorreo2.Name = "txtcorreo2"
        Me.txtcorreo2.Size = New System.Drawing.Size(254, 20)
        Me.txtcorreo2.TabIndex = 7
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(47, 156)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(41, 13)
        Me.Label14.TabIndex = 65
        Me.Label14.Text = "Correo:"
        '
        'btnBarrio
        '
        Me.btnBarrio.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBarrio.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnBarrio.Appearance.Options.UseFont = True
        Me.btnBarrio.Appearance.Options.UseForeColor = True
        Me.btnBarrio.Enabled = False
        Me.btnBarrio.Location = New System.Drawing.Point(689, 153)
        Me.btnBarrio.Name = "btnBarrio"
        Me.btnBarrio.Size = New System.Drawing.Size(20, 19)
        Me.btnBarrio.TabIndex = 17
        Me.btnBarrio.Text = "+"
        Me.btnBarrio.ToolTip = "Agregar un nuevo Departamento"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(33, 233)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 55
        Me.Label6.Text = "Direccion:"
        '
        'btnDepartamento
        '
        Me.btnDepartamento.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDepartamento.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnDepartamento.Appearance.Options.UseFont = True
        Me.btnDepartamento.Appearance.Options.UseForeColor = True
        Me.btnDepartamento.Enabled = False
        Me.btnDepartamento.Location = New System.Drawing.Point(689, 75)
        Me.btnDepartamento.Name = "btnDepartamento"
        Me.btnDepartamento.Size = New System.Drawing.Size(20, 19)
        Me.btnDepartamento.TabIndex = 11
        Me.btnDepartamento.Text = "+"
        Me.btnDepartamento.ToolTip = "Agregar un nuevo Departamento"
        '
        'btnDistrito
        '
        Me.btnDistrito.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDistrito.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnDistrito.Appearance.Options.UseFont = True
        Me.btnDistrito.Appearance.Options.UseForeColor = True
        Me.btnDistrito.Enabled = False
        Me.btnDistrito.Location = New System.Drawing.Point(689, 127)
        Me.btnDistrito.Name = "btnDistrito"
        Me.btnDistrito.Size = New System.Drawing.Size(20, 19)
        Me.btnDistrito.TabIndex = 15
        Me.btnDistrito.Text = "+"
        Me.btnDistrito.ToolTip = "Agregar un nuevo Departamento"
        '
        'btnMunicipio
        '
        Me.btnMunicipio.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMunicipio.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnMunicipio.Appearance.Options.UseFont = True
        Me.btnMunicipio.Appearance.Options.UseForeColor = True
        Me.btnMunicipio.Enabled = False
        Me.btnMunicipio.Location = New System.Drawing.Point(689, 101)
        Me.btnMunicipio.Name = "btnMunicipio"
        Me.btnMunicipio.Size = New System.Drawing.Size(20, 19)
        Me.btnMunicipio.TabIndex = 13
        Me.btnMunicipio.Text = "+"
        Me.btnMunicipio.ToolTip = "Agregar un nuevo Departamento"
        '
        'txtEcodigo
        '
        Me.txtEcodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtEcodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEcodigo.Location = New System.Drawing.Point(630, 49)
        Me.txtEcodigo.Name = "txtEcodigo"
        Me.txtEcodigo.ReadOnly = True
        Me.txtEcodigo.Size = New System.Drawing.Size(79, 20)
        Me.txtEcodigo.TabIndex = 13
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(581, 52)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(43, 13)
        Me.Label32.TabIndex = 178
        Me.Label32.Text = "Codigo:"
        '
        'lbnuevo
        '
        Me.lbnuevo.AutoSize = True
        Me.lbnuevo.ForeColor = System.Drawing.Color.CadetBlue
        Me.lbnuevo.Location = New System.Drawing.Point(160, 26)
        Me.lbnuevo.Name = "lbnuevo"
        Me.lbnuevo.Size = New System.Drawing.Size(15, 13)
        Me.lbnuevo.TabIndex = 179
        Me.lbnuevo.Text = "N"
        Me.lbnuevo.Visible = False
        '
        'TabLaboral
        '
        Me.TabLaboral.AutoScroll = True
        Me.TabLaboral.Controls.Add(Me.GroupBox6)
        Me.TabLaboral.Controls.Add(Me.GroupBox5)
        Me.TabLaboral.Controls.Add(Me.GroupBox2)
        Me.TabLaboral.Controls.Add(Me.GroupBox1)
        Me.TabLaboral.Location = New System.Drawing.Point(4, 25)
        Me.TabLaboral.Name = "TabLaboral"
        Me.TabLaboral.Padding = New System.Windows.Forms.Padding(3)
        Me.TabLaboral.Size = New System.Drawing.Size(774, 592)
        Me.TabLaboral.TabIndex = 1
        Me.TabLaboral.Text = "Datos Laborales/Credito"
        Me.TabLaboral.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label38)
        Me.GroupBox6.Controls.Add(Me.Label41)
        Me.GroupBox6.Controls.Add(Me.txtRapellido2)
        Me.GroupBox6.Controls.Add(Me.txtRtel2)
        Me.GroupBox6.Controls.Add(Me.Label46)
        Me.GroupBox6.Controls.Add(Me.txtRnombre2)
        Me.GroupBox6.Controls.Add(Me.txtRdireccion2)
        Me.GroupBox6.Controls.Add(Me.Label52)
        Me.GroupBox6.Location = New System.Drawing.Point(3, 249)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(752, 103)
        Me.GroupBox6.TabIndex = 245
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Referencia #2"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(421, 22)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(55, 13)
        Me.Label38.TabIndex = 81
        Me.Label38.Text = "Direccion:"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(30, 74)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(52, 13)
        Me.Label41.TabIndex = 242
        Me.Label41.Text = "Telefono:"
        '
        'txtRapellido2
        '
        Me.txtRapellido2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRapellido2.Location = New System.Drawing.Point(88, 45)
        Me.txtRapellido2.Name = "txtRapellido2"
        Me.txtRapellido2.Size = New System.Drawing.Size(254, 20)
        Me.txtRapellido2.TabIndex = 11
        '
        'txtRtel2
        '
        Me.txtRtel2.Location = New System.Drawing.Point(88, 71)
        Me.txtRtel2.Name = "txtRtel2"
        Me.txtRtel2.Size = New System.Drawing.Size(87, 20)
        Me.txtRtel2.TabIndex = 12
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(35, 48)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(47, 13)
        Me.Label46.TabIndex = 244
        Me.Label46.Text = "Apellido:"
        '
        'txtRnombre2
        '
        Me.txtRnombre2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRnombre2.Location = New System.Drawing.Point(88, 19)
        Me.txtRnombre2.Name = "txtRnombre2"
        Me.txtRnombre2.Size = New System.Drawing.Size(254, 20)
        Me.txtRnombre2.TabIndex = 10
        '
        'txtRdireccion2
        '
        Me.txtRdireccion2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRdireccion2.Location = New System.Drawing.Point(482, 19)
        Me.txtRdireccion2.Multiline = True
        Me.txtRdireccion2.Name = "txtRdireccion2"
        Me.txtRdireccion2.Size = New System.Drawing.Size(254, 44)
        Me.txtRdireccion2.TabIndex = 13
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(30, 22)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(52, 13)
        Me.Label52.TabIndex = 50
        Me.Label52.Text = "Nombres:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label37)
        Me.GroupBox5.Controls.Add(Me.Label35)
        Me.GroupBox5.Controls.Add(Me.txtRapellido1)
        Me.GroupBox5.Controls.Add(Me.txtRtel1)
        Me.GroupBox5.Controls.Add(Me.Label36)
        Me.GroupBox5.Controls.Add(Me.txtRnombre1)
        Me.GroupBox5.Controls.Add(Me.txtRdireccion1)
        Me.GroupBox5.Controls.Add(Me.Label33)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 140)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(752, 103)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Referencia #1"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(421, 22)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(55, 13)
        Me.Label37.TabIndex = 81
        Me.Label37.Text = "Direccion:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(30, 74)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(52, 13)
        Me.Label35.TabIndex = 242
        Me.Label35.Text = "Telefono:"
        '
        'txtRapellido1
        '
        Me.txtRapellido1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRapellido1.Location = New System.Drawing.Point(88, 45)
        Me.txtRapellido1.Name = "txtRapellido1"
        Me.txtRapellido1.Size = New System.Drawing.Size(254, 20)
        Me.txtRapellido1.TabIndex = 7
        '
        'txtRtel1
        '
        Me.txtRtel1.Location = New System.Drawing.Point(88, 67)
        Me.txtRtel1.Name = "txtRtel1"
        Me.txtRtel1.Size = New System.Drawing.Size(87, 20)
        Me.txtRtel1.TabIndex = 8
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(35, 48)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(47, 13)
        Me.Label36.TabIndex = 244
        Me.Label36.Text = "Apellido:"
        '
        'txtRnombre1
        '
        Me.txtRnombre1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRnombre1.Location = New System.Drawing.Point(88, 19)
        Me.txtRnombre1.Name = "txtRnombre1"
        Me.txtRnombre1.Size = New System.Drawing.Size(254, 20)
        Me.txtRnombre1.TabIndex = 6
        '
        'txtRdireccion1
        '
        Me.txtRdireccion1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRdireccion1.Location = New System.Drawing.Point(482, 19)
        Me.txtRdireccion1.Multiline = True
        Me.txtRdireccion1.Name = "txtRdireccion1"
        Me.txtRdireccion1.Size = New System.Drawing.Size(254, 44)
        Me.txtRdireccion1.TabIndex = 9
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(30, 22)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(52, 13)
        Me.Label33.TabIndex = 50
        Me.Label33.Text = "Nombres:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtcantCredito)
        Me.GroupBox2.Controls.Add(Me.txtLimiteCredito)
        Me.GroupBox2.Controls.Add(Me.txtSaldo)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.txtSolicitudPendiete)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Location = New System.Drawing.Point(500, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(258, 128)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Credito"
        '
        'txtcantCredito
        '
        Me.txtcantCredito.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtcantCredito.Location = New System.Drawing.Point(155, 17)
        Me.txtcantCredito.Name = "txtcantCredito"
        Me.txtcantCredito.ReadOnly = True
        Me.txtcantCredito.Size = New System.Drawing.Size(87, 20)
        Me.txtcantCredito.TabIndex = 20
        Me.txtcantCredito.TabStop = False
        Me.txtcantCredito.Text = "0"
        '
        'txtLimiteCredito
        '
        Me.txtLimiteCredito.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtLimiteCredito.Location = New System.Drawing.Point(155, 43)
        Me.txtLimiteCredito.Name = "txtLimiteCredito"
        Me.txtLimiteCredito.ReadOnly = True
        Me.txtLimiteCredito.Size = New System.Drawing.Size(87, 20)
        Me.txtLimiteCredito.TabIndex = 21
        Me.txtLimiteCredito.TabStop = False
        Me.txtLimiteCredito.Text = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSaldo.Location = New System.Drawing.Point(155, 69)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.ReadOnly = True
        Me.txtSaldo.Size = New System.Drawing.Size(87, 20)
        Me.txtSaldo.TabIndex = 22
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.Text = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(112, 72)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(37, 13)
        Me.Label11.TabIndex = 92
        Me.Label11.Text = "Saldo:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(33, 98)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(116, 13)
        Me.Label24.TabIndex = 83
        Me.Label24.Text = "Solicitudes pendientes:"
        '
        'txtSolicitudPendiete
        '
        Me.txtSolicitudPendiete.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSolicitudPendiete.Location = New System.Drawing.Point(155, 95)
        Me.txtSolicitudPendiete.Name = "txtSolicitudPendiete"
        Me.txtSolicitudPendiete.ReadOnly = True
        Me.txtSolicitudPendiete.Size = New System.Drawing.Size(87, 20)
        Me.txtSolicitudPendiete.TabIndex = 23
        Me.txtSolicitudPendiete.TabStop = False
        Me.txtSolicitudPendiete.Text = "0"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(73, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(76, 13)
        Me.Label23.TabIndex = 85
        Me.Label23.Text = "Cant. Creditos:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(61, 46)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(88, 13)
        Me.Label22.TabIndex = 81
        Me.Label22.Text = "Limite de Credito:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtTingreso)
        Me.GroupBox1.Controls.Add(Me.txtTtelefono)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtTdireccion)
        Me.GroupBox1.Controls.Add(Me.txtTcorreo)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(485, 128)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Trabajo"
        '
        'txtTingreso
        '
        Me.txtTingreso.Location = New System.Drawing.Point(88, 46)
        Me.txtTingreso.Name = "txtTingreso"
        Me.txtTingreso.Size = New System.Drawing.Size(87, 20)
        Me.txtTingreso.TabIndex = 2
        Me.txtTingreso.Text = "0"
        '
        'txtTtelefono
        '
        Me.txtTtelefono.Location = New System.Drawing.Point(255, 46)
        Me.txtTtelefono.Name = "txtTtelefono"
        Me.txtTtelefono.Size = New System.Drawing.Size(87, 20)
        Me.txtTtelefono.TabIndex = 4
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(197, 49)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(52, 13)
        Me.Label19.TabIndex = 72
        Me.Label19.Text = "Telefono:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(27, 75)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(55, 13)
        Me.Label20.TabIndex = 70
        Me.Label20.Text = "Direccion:"
        '
        'txtTdireccion
        '
        Me.txtTdireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTdireccion.Location = New System.Drawing.Point(88, 72)
        Me.txtTdireccion.Multiline = True
        Me.txtTdireccion.Name = "txtTdireccion"
        Me.txtTdireccion.Size = New System.Drawing.Size(254, 44)
        Me.txtTdireccion.TabIndex = 5
        '
        'txtTcorreo
        '
        Me.txtTcorreo.Location = New System.Drawing.Point(88, 17)
        Me.txtTcorreo.MaxLength = 50
        Me.txtTcorreo.Name = "txtTcorreo"
        Me.txtTcorreo.Size = New System.Drawing.Size(254, 20)
        Me.txtTcorreo.TabIndex = 1
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(32, 49)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(50, 13)
        Me.Label21.TabIndex = 79
        Me.Label21.Text = "Ingresos:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(41, 20)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(41, 13)
        Me.Label17.TabIndex = 75
        Me.Label17.Text = "Correo:"
        '
        'TabFiador
        '
        Me.TabFiador.Controls.Add(Me.GroupBox4)
        Me.TabFiador.Controls.Add(Me.GroupBox3)
        Me.TabFiador.Location = New System.Drawing.Point(4, 25)
        Me.TabFiador.Name = "TabFiador"
        Me.TabFiador.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFiador.Size = New System.Drawing.Size(774, 592)
        Me.TabFiador.TabIndex = 2
        Me.TabFiador.Text = "Datos de Fiador"
        Me.TabFiador.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.TextBox18)
        Me.GroupBox4.Controls.Add(Me.Label54)
        Me.GroupBox4.Controls.Add(Me.TextBox17)
        Me.GroupBox4.Controls.Add(Me.Label53)
        Me.GroupBox4.Controls.Add(Me.TextBox16)
        Me.GroupBox4.Controls.Add(Me.Label49)
        Me.GroupBox4.Controls.Add(Me.TextBox12)
        Me.GroupBox4.Controls.Add(Me.Label44)
        Me.GroupBox4.Controls.Add(Me.TextBox8)
        Me.GroupBox4.Controls.Add(Me.Label40)
        Me.GroupBox4.Controls.Add(Me.TextBox7)
        Me.GroupBox4.Controls.Add(Me.TextBox10)
        Me.GroupBox4.Controls.Add(Me.Label34)
        Me.GroupBox4.Controls.Add(Me.Label42)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 144)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(750, 134)
        Me.GroupBox4.TabIndex = 233
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Datos Laborales"
        '
        'TextBox18
        '
        Me.TextBox18.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox18.Location = New System.Drawing.Point(86, 71)
        Me.TextBox18.Multiline = True
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(254, 45)
        Me.TextBox18.TabIndex = 11
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(25, 74)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(55, 13)
        Me.Label54.TabIndex = 246
        Me.Label54.Text = "Direccion:"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(588, 96)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(116, 20)
        Me.TextBox17.TabIndex = 253
        Me.TextBox17.TabStop = False
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(532, 99)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(50, 13)
        Me.Label53.TabIndex = 254
        Me.Label53.Text = "Total C$:"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(588, 70)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(116, 20)
        Me.TextBox16.TabIndex = 14
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(488, 73)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(94, 13)
        Me.Label49.TabIndex = 252
        Me.Label49.Text = "Otros Ingresos C$:"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(588, 44)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(116, 20)
        Me.TextBox12.TabIndex = 13
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(478, 47)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(104, 13)
        Me.Label44.TabIndex = 250
        Me.Label44.Text = "Ingreso Mensual C$:"
        '
        'TextBox8
        '
        Me.TextBox8.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox8.Location = New System.Drawing.Point(86, 45)
        Me.TextBox8.MaxLength = 14
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(254, 20)
        Me.TextBox8.TabIndex = 10
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(43, 48)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(37, 13)
        Me.Label40.TabIndex = 248
        Me.Label40.Text = "Lugar:"
        '
        'TextBox7
        '
        Me.TextBox7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox7.Location = New System.Drawing.Point(86, 19)
        Me.TextBox7.MaxLength = 14
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(254, 20)
        Me.TextBox7.TabIndex = 9
        '
        'TextBox10
        '
        Me.TextBox10.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox10.Location = New System.Drawing.Point(588, 18)
        Me.TextBox10.MaxLength = 14
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(116, 20)
        Me.TextBox10.TabIndex = 12
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(42, 22)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(38, 13)
        Me.Label34.TabIndex = 246
        Me.Label34.Text = "Cargo:"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(483, 21)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(99, 13)
        Me.Label42.TabIndex = 246
        Me.Label42.Text = "Tiempo de Laborar:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBox6)
        Me.GroupBox3.Controls.Add(Me.Label56)
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Controls.Add(Me.ComboBoxEdit4)
        Me.GroupBox3.Controls.Add(Me.Label39)
        Me.GroupBox3.Controls.Add(Me.Label51)
        Me.GroupBox3.Controls.Add(Me.Label43)
        Me.GroupBox3.Controls.Add(Me.TextBox14)
        Me.GroupBox3.Controls.Add(Me.ComboBoxEdit5)
        Me.GroupBox3.Controls.Add(Me.Label50)
        Me.GroupBox3.Controls.Add(Me.TextBox9)
        Me.GroupBox3.Controls.Add(Me.Label47)
        Me.GroupBox3.Controls.Add(Me.Label45)
        Me.GroupBox3.Controls.Add(Me.TextBox11)
        Me.GroupBox3.Controls.Add(Me.TextBox5)
        Me.GroupBox3.Controls.Add(Me.Label48)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(750, 132)
        Me.GroupBox3.TabIndex = 232
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos Personales"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(271, 95)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(69, 20)
        Me.TextBox6.TabIndex = 6
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(410, 72)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(34, 13)
        Me.Label56.TabIndex = 229
        Me.Label56.Text = "Casa:"
        '
        'TextBox13
        '
        Me.TextBox13.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox13.Location = New System.Drawing.Point(86, 17)
        Me.TextBox13.MaxLength = 14
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(112, 20)
        Me.TextBox13.TabIndex = 1
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.EditValue = "ALQUILADA"
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(450, 69)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.ComboBoxEdit4.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.ComboBoxEdit4.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.ComboBoxEdit4.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ComboBoxEdit4.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ComboBoxEdit4.Properties.Items.AddRange(New Object() {"PROPIA", "ALQUILADA"})
        Me.ComboBoxEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(254, 20)
        Me.ComboBoxEdit4.TabIndex = 8
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(209, 98)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(56, 13)
        Me.Label39.TabIndex = 244
        Me.Label39.Text = "Familiares:"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(213, 20)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(52, 13)
        Me.Label51.TabIndex = 240
        Me.Label51.Text = "Telefono:"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(15, 98)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(65, 13)
        Me.Label43.TabIndex = 242
        Me.Label43.Text = "Estado Civil:"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(271, 17)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(69, 20)
        Me.TextBox14.TabIndex = 2
        '
        'ComboBoxEdit5
        '
        Me.ComboBoxEdit5.EditValue = "SOLTERO"
        Me.ComboBoxEdit5.Location = New System.Drawing.Point(86, 95)
        Me.ComboBoxEdit5.Name = "ComboBoxEdit5"
        Me.ComboBoxEdit5.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.ComboBoxEdit5.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.ComboBoxEdit5.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.ComboBoxEdit5.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ComboBoxEdit5.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.ComboBoxEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ComboBoxEdit5.Properties.Items.AddRange(New Object() {"SOLTERO", "CASADO", "AJUNTADO", "VIUDO", "SEPARADO", "OTROS"})
        Me.ComboBoxEdit5.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit5.Size = New System.Drawing.Size(112, 20)
        Me.ComboBoxEdit5.TabIndex = 5
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(37, 20)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(43, 13)
        Me.Label50.TabIndex = 239
        Me.Label50.Text = "Cedula:"
        '
        'TextBox9
        '
        Me.TextBox9.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox9.Location = New System.Drawing.Point(86, 43)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(254, 20)
        Me.TextBox9.TabIndex = 3
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(33, 72)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(47, 13)
        Me.Label47.TabIndex = 238
        Me.Label47.Text = "Apellido:"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(28, 46)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(52, 13)
        Me.Label45.TabIndex = 237
        Me.Label45.Text = "Nombres:"
        '
        'TextBox11
        '
        Me.TextBox11.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox11.Location = New System.Drawing.Point(86, 69)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(254, 20)
        Me.TextBox11.TabIndex = 4
        '
        'TextBox5
        '
        Me.TextBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox5.Location = New System.Drawing.Point(450, 17)
        Me.TextBox5.Multiline = True
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(254, 45)
        Me.TextBox5.TabIndex = 7
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(389, 20)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(55, 13)
        Me.Label48.TabIndex = 205
        Me.Label48.Text = "Direccion:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 621)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(782, 49)
        Me.Panel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Image = CType(resources.GetObject("btnCancelar.Image"), System.Drawing.Image)
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(684, 8)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(78, 35)
        Me.btnCancelar.TabIndex = 25
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAceptar.Location = New System.Drawing.Point(600, 8)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(78, 35)
        Me.btnAceptar.TabIndex = 24
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'frmEditCliente
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(782, 670)
        Me.Controls.Add(Me.pMain)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Clientes"
        Me.pMain.ResumeLayout(False)
        Me.TabPrincipal.ResumeLayout(False)
        Me.TabPersonal.ResumeLayout(False)
        Me.TabPersonal.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbColector.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabLaboral.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabFiador.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBarrioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblZonaDistBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMunicipioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents txtcodigo As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txttelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents txtDireccion1 As System.Windows.Forms.TextBox
    Friend WithEvents txtcorreo1 As System.Windows.Forms.TextBox
    Friend WithEvents txttelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents btnBarrio As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDistrito As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnMunicipio As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDepartamento As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TblDepartBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMunicipioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblZonaDistBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBarrioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TabPrincipal As System.Windows.Forms.TabControl
    Friend WithEvents TabPersonal As System.Windows.Forms.TabPage
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtcorreo2 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCedula As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtapellido As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents ckbActivo As System.Windows.Forms.CheckBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TabLaboral As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtcantCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtLimiteCredito As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSolicitudPendiete As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTingreso As System.Windows.Forms.TextBox
    Friend WithEvents txtTtelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtTdireccion As System.Windows.Forms.TextBox
    Friend WithEvents txtTcorreo As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TabFiador As System.Windows.Forms.TabPage
    Friend WithEvents dtpFechaF As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents cbColector As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtMunicipio As System.Windows.Forms.TextBox
    Friend WithEvents txtDistrito As System.Windows.Forms.TextBox
    Friend WithEvents txtBarrio As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartamento As System.Windows.Forms.TextBox
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtEcodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxEdit5 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRapellido1 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtRtel1 As System.Windows.Forms.TextBox
    Friend WithEvents txtRnombre1 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtRapellido2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRtel2 As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtRnombre2 As System.Windows.Forms.TextBox
    Friend WithEvents txtRdireccion2 As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtRdireccion1 As System.Windows.Forms.TextBox
    Friend WithEvents lbnuevo As System.Windows.Forms.Label
    Friend WithEvents lbClasificacion As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
End Class
