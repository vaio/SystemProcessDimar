﻿Public Class frmEditBodega

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes WHERE Codigo_Bodega='" & CodigoEntidad & "'"
        Dim tblDatosBodega As DataTable = SQL(strsql, "tblDatosBodega", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosBodega.Rows.Count <> 0 Then
            txtCodBodega.Text = tblDatosBodega.Rows(0).Item("Codigo_Bodega")
            txtNomBodega.Text = tblDatosBodega.Rows(0).Item("Nombre_Bodega")
        End If
    End Sub

    Public Sub Cerrar()
        Close()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoBodegas As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoBodegas.EditarBodegas(nSucursal, CInt(IIf(txtCodBodega.Text = "", 0, txtCodBodega.Text)), txtNomBodega.Text, nTipoEdic)
            If Not Resum = "OK" Then
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            frmPrincipal.CargarDatos()
        End Try
    End Sub

    Private Sub frmEditBodega_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            LblCod.Visible = True
            txtCodBodega.Visible = True
            txtCodBodega.Text = RegistroMaximo()
            txtNomBodega.Text = ""
            txtNomBodega.Focus()
        ElseIf nTipoEdic = 2 Then
            LblCod.Visible = True
            txtCodBodega.Enabled = False
            txtCodBodega.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub
End Class