﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports SIGMA.Servicio
Imports System.Xml
Public Class FrmTasaCambio
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Private Sub FrmTasaCambio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
    End Sub
    Sub Cargar()
        Dim dt As DataTable = New DataTable()
        sqlstring = "select Id_Tasa as 'Codigo', Fecha, Tasa_Oficial as 'Tasa Oficial', Tasa_Paralela as 'Tasa Paralela' from Tbl_TasasCambio order by Fecha desc"
        Dim TableDatos As DataTable = SQL(sqlstring, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
        GridControl1.DataSource = TableDatos
        GridView1.BestFitColumns()
    End Sub
    Private Sub GridControl1_MouseClick(sender As Object, e As MouseEventArgs) Handles GridControl1.MouseClick
        If GridView1.RowCount > 0 Then
            With GridView1
                txtCodigo.Text = .GetRowCellValue(.GetSelectedRows(0), "Codigo").ToString()
                dtpFecha.Value = .GetRowCellValue(.GetSelectedRows(0), "Fecha").ToString
                txtOficial.Text = .GetRowCellValue(.GetSelectedRows(0), "Tasa Oficial").ToString
                txtParalela.Text = .GetRowCellValue(.GetSelectedRows(0), "Tasa Paralela").ToString
            End With
        End If
    End Sub
    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If btnEditar.Text = "&Editar" Then
            If GridView1.SelectedRowsCount > 0 Then
                gbDato.Enabled = True
                txtOficial.Focus()
                btnNuevo.Enabled = False
                btnEditar.Text = "&Aceptar"
                btnCancelar.Text = "&Cancelar"
            Else
                MsgBox("No ha seleccionado registro")
            End If
        ElseIf btnEditar.Text = "&Cancelar" Then
            clear()
        Else
            If txtOficial.Text > 0 Then
                sqlstring = "update Tbl_TasasCambio set [Fecha]=@fecha, [Tasa_Oficial]=@oficial,[Tasa_Paralela]=@paralela where Id_Tasa=@codigo"
                Try
                    con.Open()
                    cmd = New SqlCommand(sqlstring, con)
                    With cmd.Parameters
                        .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Text))
                        .AddWithValue("@oficial", txtOficial.Text)
                        .AddWithValue("@paralela", txtParalela.Text)
                        .AddWithValue("@codigo", txtCodigo.Text)
                    End With
                    cmd.ExecuteNonQuery()
                    con.Close()
                    MsgBox("Registro Actulizado")
                    clear()
                Catch ex As Exception
                    MsgBox("Error: " + ex.Message)
                    con.Close()
                End Try
            End If
        End If
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        If btnNuevo.Text = "&Aceptar" Then
            If txtOficial.Text > 0 Then
                sqlstring = "insert into Tbl_TasasCambio (Fecha, Tasa_Oficial,Tasa_Paralela)values (@fecha, @oficial, @paralela)"
                Try
                    con.Open()
                    cmd = New SqlCommand(sqlstring, con)
                    With cmd.Parameters
                        .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Text))
                        .AddWithValue("@oficial", txtOficial.Text)
                        .AddWithValue("@paralela", txtParalela.Text)
                    End With
                    cmd.ExecuteNonQuery()
                    con.Close()
                    MsgBox("Registro Guardado")
                    clear()
                Catch ex As Exception
                    MsgBox("Error: " + ex.Message)
                    con.Close()
                End Try
            End If
        Else
            clear()
            gbDato.Enabled = True
            With GridView1
                txtOficial.Focus()
                dtpFecha.Enabled = True
                btnNuevo.Text = "&Aceptar"
                btnEditar.Text = "&Cancelar"
            End With
        End If
    End Sub
    Sub clear()
        Cargar()
        dtpFecha.Enabled = False
        txtCodigo.Text = ""
        txtOficial.Text = "0"
        dtpFecha.Value = Today
        txtParalela.Text = "0"
        btnEditar.Text = "&Editar"
        btnNuevo.Text = "&Agregar"
        gbDato.Enabled = False
        btnNuevo.Enabled = True
        GridView1.ClearSelection()
        btnCancelar.Text = "&Salir"
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If btnCancelar.Text = "&Salir" Then
            Me.Close()
            Me.Dispose()
        Else
            clear()
        End If
    End Sub
    Private Sub txtOficial_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtParalela.KeyPress, txtOficial.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub
End Class