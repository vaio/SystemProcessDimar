﻿Public Class frmDepartPais

  Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
    Dim strsql As String = ""

        strsql = "SELECT Id_Departamento,Nombre FROM Catalogo.Departamento WHERE Id_Departamento='" & CodigoEntidad & "'"
        Dim tblDatosDepartPais As DataTable = SQL(strsql, "tblDatosDepartPais", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosDepartPais.Rows.Count <> 0 Then
            txtCodDepartPais.Text = tblDatosDepartPais.Rows(0).Item(0)
            txtNomDepartPais.Text = tblDatosDepartPais.Rows(0).Item(1)
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmDepartPais_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodDepartPais.Visible = True
            txtCodDepartPais.Visible = True
            txtCodDepartPais.Text = RegistroMaximo()
            txtNomDepartPais.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodDepartPais.Visible = True
            txtCodDepartPais.Enabled = False
            txtCodDepartPais.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoDepartPais As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoDepartPais.EditarDepartPais(CInt(IIf(txtCodDepartPais.Text = "", 0, txtCodDepartPais.Text)), txtNomDepartPais.Text.ToUpper, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

  Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
    Cerrar()
  End Sub
End Class