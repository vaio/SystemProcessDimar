﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditDistricZona
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditDistricZona))
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.cmdAddMunicipio = New DevExpress.XtraEditors.SimpleButton()
    Me.luMunic = New DevExpress.XtraEditors.LookUpEdit()
    Me.lblMunicAsociado = New System.Windows.Forms.Label()
    Me.cmdAddDeparPais = New DevExpress.XtraEditors.SimpleButton()
    Me.luDepartPais = New DevExpress.XtraEditors.LookUpEdit()
    Me.lblDepartPaisAsoc = New System.Windows.Forms.Label()
    Me.lblNomDistZona = New System.Windows.Forms.Label()
    Me.txtNomDistZona = New System.Windows.Forms.TextBox()
    Me.lblCodDistZona = New System.Windows.Forms.Label()
    Me.txtCodDistZona = New System.Windows.Forms.TextBox()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.TblDepartPaisBindingSource = New System.Windows.Forms.BindingSource(Me.components)
    Me.TblMunicipioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
    Me.Panel2.SuspendLayout()
    CType(Me.luMunic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.luDepartPais.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    CType(Me.TblDepartPaisBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TblMunicipioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.cmdAddMunicipio)
    Me.Panel2.Controls.Add(Me.luMunic)
    Me.Panel2.Controls.Add(Me.lblMunicAsociado)
    Me.Panel2.Controls.Add(Me.cmdAddDeparPais)
    Me.Panel2.Controls.Add(Me.luDepartPais)
    Me.Panel2.Controls.Add(Me.lblDepartPaisAsoc)
    Me.Panel2.Controls.Add(Me.lblNomDistZona)
    Me.Panel2.Controls.Add(Me.txtNomDistZona)
    Me.Panel2.Controls.Add(Me.lblCodDistZona)
    Me.Panel2.Controls.Add(Me.txtCodDistZona)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel2.Location = New System.Drawing.Point(0, 0)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(387, 171)
    Me.Panel2.TabIndex = 9
    '
    'cmdAddMunicipio
    '
    Me.cmdAddMunicipio.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.cmdAddMunicipio.Appearance.ForeColor = System.Drawing.Color.Blue
    Me.cmdAddMunicipio.Appearance.Options.UseFont = True
    Me.cmdAddMunicipio.Appearance.Options.UseForeColor = True
    Me.cmdAddMunicipio.Location = New System.Drawing.Point(342, 130)
    Me.cmdAddMunicipio.Name = "cmdAddMunicipio"
    Me.cmdAddMunicipio.Size = New System.Drawing.Size(20, 19)
    Me.cmdAddMunicipio.TabIndex = 12
    Me.cmdAddMunicipio.Text = "+"
    Me.cmdAddMunicipio.ToolTip = "Agregar un nuevo Departamento"
    '
    'luMunic
    '
    Me.luMunic.Location = New System.Drawing.Point(147, 129)
    Me.luMunic.Name = "luMunic"
    Me.luMunic.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luMunic.Size = New System.Drawing.Size(194, 20)
    Me.luMunic.TabIndex = 11
    '
    'lblMunicAsociado
    '
    Me.lblMunicAsociado.Location = New System.Drawing.Point(12, 132)
    Me.lblMunicAsociado.Name = "lblMunicAsociado"
    Me.lblMunicAsociado.Size = New System.Drawing.Size(129, 20)
    Me.lblMunicAsociado.TabIndex = 10
    Me.lblMunicAsociado.Text = "Municipio Asociado:"
    '
    'cmdAddDeparPais
    '
    Me.cmdAddDeparPais.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.cmdAddDeparPais.Appearance.ForeColor = System.Drawing.Color.Blue
    Me.cmdAddDeparPais.Appearance.Options.UseFont = True
    Me.cmdAddDeparPais.Appearance.Options.UseForeColor = True
    Me.cmdAddDeparPais.Location = New System.Drawing.Point(342, 95)
    Me.cmdAddDeparPais.Name = "cmdAddDeparPais"
    Me.cmdAddDeparPais.Size = New System.Drawing.Size(20, 19)
    Me.cmdAddDeparPais.TabIndex = 9
    Me.cmdAddDeparPais.Text = "+"
    Me.cmdAddDeparPais.ToolTip = "Agregar un nuevo Departamento"
    '
    'luDepartPais
    '
    Me.luDepartPais.Location = New System.Drawing.Point(147, 94)
    Me.luDepartPais.Name = "luDepartPais"
    Me.luDepartPais.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luDepartPais.Size = New System.Drawing.Size(194, 20)
    Me.luDepartPais.TabIndex = 7
    '
    'lblDepartPaisAsoc
    '
    Me.lblDepartPaisAsoc.Location = New System.Drawing.Point(12, 97)
    Me.lblDepartPaisAsoc.Name = "lblDepartPaisAsoc"
    Me.lblDepartPaisAsoc.Size = New System.Drawing.Size(129, 20)
    Me.lblDepartPaisAsoc.TabIndex = 5
    Me.lblDepartPaisAsoc.Text = "Departamento Asociado:"
    '
    'lblNomDistZona
    '
    Me.lblNomDistZona.Location = New System.Drawing.Point(12, 60)
    Me.lblNomDistZona.Name = "lblNomDistZona"
    Me.lblNomDistZona.Size = New System.Drawing.Size(129, 20)
    Me.lblNomDistZona.TabIndex = 3
    Me.lblNomDistZona.Text = "Nombre de Distrito/Zona:"
    '
    'txtNomDistZona
    '
    Me.txtNomDistZona.Location = New System.Drawing.Point(147, 57)
    Me.txtNomDistZona.Name = "txtNomDistZona"
    Me.txtNomDistZona.Size = New System.Drawing.Size(200, 20)
    Me.txtNomDistZona.TabIndex = 2
    '
    'lblCodDistZona
    '
    Me.lblCodDistZona.Location = New System.Drawing.Point(12, 24)
    Me.lblCodDistZona.Name = "lblCodDistZona"
    Me.lblCodDistZona.Size = New System.Drawing.Size(117, 20)
    Me.lblCodDistZona.TabIndex = 1
    Me.lblCodDistZona.Text = "Id Distrito/ Zona:"
    '
    'txtCodDistZona
    '
    Me.txtCodDistZona.Location = New System.Drawing.Point(147, 21)
    Me.txtCodDistZona.Name = "txtCodDistZona"
    Me.txtCodDistZona.Size = New System.Drawing.Size(88, 20)
    Me.txtCodDistZona.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 171)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(387, 49)
    Me.Panel1.TabIndex = 8
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(292, 8)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(208, 8)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'frmEditDistricZona
    '
    Me.AcceptButton = Me.cmdAceptar
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.cmdCancelar
    Me.ClientSize = New System.Drawing.Size(387, 220)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Panel1)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmEditDistricZona"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Edición de Distritos /Zonas"
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.luMunic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.luDepartPais.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    CType(Me.TblDepartPaisBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TblMunicipioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents cmdAddDeparPais As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents luDepartPais As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblDepartPaisAsoc As System.Windows.Forms.Label
	Friend WithEvents lblNomDistZona As System.Windows.Forms.Label
	Friend WithEvents txtNomDistZona As System.Windows.Forms.TextBox
	Friend WithEvents lblCodDistZona As System.Windows.Forms.Label
	Friend WithEvents txtCodDistZona As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cmdCancelar As System.Windows.Forms.Button
	Friend WithEvents cmdAceptar As System.Windows.Forms.Button
	Friend WithEvents cmdAddMunicipio As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents luMunic As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblMunicAsociado As System.Windows.Forms.Label
	Friend WithEvents TblDepartPaisBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblMunicipioBindingSource As System.Windows.Forms.BindingSource
End Class
