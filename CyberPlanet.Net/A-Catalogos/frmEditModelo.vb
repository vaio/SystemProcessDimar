﻿Public Class frmEditModelo
    Dim pre_marca As Integer
    Dim Pre_modelo As Integer
    Private Sub frmEditModelo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarMarcas()
        If nTipoEdic = 1 Then
            lblCodModelo.Visible = True
            txtCodModelo.Visible = True
            txtNomModelo.Text = ""
            txtNomModelo.Focus()
        ElseIf nTipoEdic = 2 Then
            'pre_marca = luMarcas.Properties.ValueMember
            lblCodModelo.Visible = True
            txtCodModelo.Enabled = False
            txtCodModelo.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoModelos As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoModelos.EditarModelos(CInt(IIf(txtCodModelo.Text = "", 0, txtCodModelo.Text)), txtNomModelo.Text, luMarcas.EditValue, nTipoEdic, pre_marca, Pre_modelo)
            If Not Resum = "OK" Then
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""
        Dim Marca As String = ""

        Try
            Dim registro As DataRowView = frmCatalogo.TblCatalogosBS.Current
            Marca = registro.Item("Nombre_Marca")

            strsql = "SELECT Codigo,Nombre_Modelo,Nombre_Marca FROM vwVerModelos WHERE Codigo='" & CodigoEntidad & "' and Nombre_Marca = '" & Marca & "'"
            Dim tblDatosModelo As DataTable = SQL(strsql, "tblDatosModelo", My.Settings.SolIndustrialCNX).Tables(0)
            If tblDatosModelo.Rows.Count <> 0 Then
                luMarcas.Text = tblDatosModelo.Rows(0).Item("Nombre_Marca")

                txtCodModelo.Text = tblDatosModelo.Rows(0).Item("Codigo")
                Pre_modelo = txtCodModelo.Text

                txtNomModelo.Text = tblDatosModelo.Rows(0).Item("Nombre_Modelo")
                pre_marca = valTemp
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub cargarMarcas()
        'tblMarcasBindingSource.DataSource = SQL("select Codigo_Marca,Nombre_Marca from Tbl_Marca order by Codigo_Marca", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        'luMarcas.Properties.DataSource = tblMarcasBindingSource
        luMarcas.Properties.DataSource = SQL("select Codigo_Marca,Nombre_Marca from Tbl_Marca order by Codigo_Marca", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        luMarcas.Properties.DisplayMember = "Nombre_Marca"
        luMarcas.Properties.ValueMember = "Codigo_Marca"
        'pre_marca = luMarcas.Properties.ValueMember
        luMarcas.ItemIndex = 0
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub cmdAddMarca_Click(sender As Object, e As EventArgs) Handles cmdAddMarca.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 2
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarMarcas()
    End Sub

    Private Sub luMarcas_EditValueChanged(sender As Object, e As EventArgs) Handles luMarcas.EditValueChanged
        valTemp = luMarcas.EditValue
        txtCodModelo.Text = RegistroMaximo()
    End Sub
End Class