﻿Public Class frmEditBarrios

	Private Sub frmEditBarrios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		cargarDepartPais()
		cargarMunicipios()
		cargarDistZona()
		If nTipoEdic = 1 Then
			lblCodBarrio.Visible = True
			txtCodBarrio.Visible = True
			txtCodBarrio.Text = RegistroMaximo()
		ElseIf nTipoEdic = 2 Then
			lblCodBarrio.Visible = True
			txtCodBarrio.Enabled = False
			txtCodBarrio.Visible = True
			CargarDatos(CodigoEntidad)
		ElseIf nTipoEdic = 3 Then
			Panel2.Enabled = False
			CargarDatos(CodigoEntidad)
		End If
	End Sub

	Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
		Dim Resum As String = ""
        Dim CatalogoCateg As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoCateg.EditarBarrio(CInt(IIf(txtCodBarrio.Text = "", 0, txtCodBarrio.Text)), txtNomBarrio.Text.ToUpper, luZonaDist.EditValue, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub luDepartPais_EditValueChanged(sender As Object, e As EventArgs) Handles luDepartPais.EditValueChanged
        cargarMunicipios()
    End Sub

    Private Sub luMunic_EditValueChanged(sender As Object, e As EventArgs) Handles luMunic.EditValueChanged
        cargarDistZona()
    End Sub

    Private Sub cmdAddDeparPais_Click(sender As Object, e As EventArgs) Handles cmdAddDeparPais.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 8
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarDepartPais()
    End Sub

    Private Sub cmdAddMunicipio_Click(sender As Object, e As EventArgs) Handles cmdAddMunicipio.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 9
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarMunicipios()
    End Sub

    Private Sub cmdAddDistZona_Click(sender As Object, e As EventArgs) Handles cmdAddDistZona.Click
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 10
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarDistZona()
    End Sub

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo,Barrio,[Zona/Distrito],Municipio,Departamento FROM [vwBarrios] WHERE Codigo='" & CodigoEntidad & "'"
        Dim tblDatosBarrio As DataTable = SQL(strsql, "tblDatosBarrio", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosBarrio.Rows.Count <> 0 Then
            txtCodBarrio.Text = tblDatosBarrio.Rows(0).Item(0)
            txtNomBarrio.Text = tblDatosBarrio.Rows(0).Item(1)
            luZonaDist.Text = tblDatosBarrio.Rows(0).Item(2)
            luMunic.Text = tblDatosBarrio.Rows(0).Item(3)
            luDepartPais.Text = tblDatosBarrio.Rows(0).Item(4)
        End If
    End Sub

    Private Sub cargarDepartPais()
        TblDepartPaisBindingSource.DataSource = SQL("select Id_Departamento,Nombre_Departamento from Tbl_DepartamentoPais order by Id_Departamento", "tblDepartPais", My.Settings.SolIndustrialCNX).Tables(0)
        luDepartPais.Properties.DataSource = TblDepartPaisBindingSource
        luDepartPais.Properties.DisplayMember = "Nombre_Departamento"
        luDepartPais.Properties.ValueMember = "Id_Departamento"
        luDepartPais.ItemIndex = 0
    End Sub

    Private Sub cargarMunicipios()
        Dim strsql As String = "SELECT Id_Municipio, Nombre_Municipio FROM Tbl_Municipio INNER JOIN Tbl_DepartamentoPais ON Tbl_Municipio.Id_Departamento = Tbl_DepartamentoPais.Id_Departamento"
        strsql = strsql & " WHERE (Tbl_DepartamentoPais.Nombre_Departamento = '" & luDepartPais.Text & "') ORDER BY Tbl_Municipio.Id_Municipio"
        TblMunicipioBindingSource.DataSource = SQL(strsql, "tblMunic", My.Settings.SolIndustrialCNX).Tables(0)
        luMunic.Properties.DataSource = TblMunicipioBindingSource
        luMunic.Properties.DisplayMember = "Nombre_Municipio"
        luMunic.Properties.ValueMember = "Id_Municipio"
        luMunic.ItemIndex = 0
    End Sub

    Private Sub cargarDistZona()
        Dim strsql As String = " SELECT Id_Zona_Distrito, Nombre_Zona_Distrito FROM Tbl_Zonas_Distritos INNER JOIN Tbl_Municipio ON Tbl_Zonas_Distritos.Id_Municipio = Tbl_Municipio.Id_Municipio "
        strsql = strsql & " WHERE (Tbl_Municipio.Nombre_Municipio = N'" & luMunic.Text & "') ORDER BY Tbl_Zonas_Distritos.Id_Zona_Distrito"
        TblZonaDistBindingSource.DataSource = SQL(strsql, "tblDistZona", My.Settings.SolIndustrialCNX).Tables(0)
        luZonaDist.Properties.DataSource = TblZonaDistBindingSource
        luZonaDist.Properties.DisplayMember = "Nombre_Zona_Distrito"
        luZonaDist.Properties.ValueMember = "Id_Zona_Distrito"
        luZonaDist.ItemIndex = 0
    End Sub

	Public Sub Cerrar()
		Me.Dispose()
		Close()
	End Sub

End Class