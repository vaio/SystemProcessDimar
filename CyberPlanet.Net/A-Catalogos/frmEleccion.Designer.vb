﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEleccion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSel1 = New System.Windows.Forms.Button()
        Me.btnSel2 = New System.Windows.Forms.Button()
        Me.btnSel3 = New System.Windows.Forms.Button()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSel1
        '
        Me.btnSel1.Location = New System.Drawing.Point(101, 35)
        Me.btnSel1.Name = "btnSel1"
        Me.btnSel1.Size = New System.Drawing.Size(144, 23)
        Me.btnSel1.TabIndex = 0
        Me.btnSel1.Text = "Producto"
        Me.btnSel1.UseVisualStyleBackColor = True
        '
        'btnSel2
        '
        Me.btnSel2.Location = New System.Drawing.Point(101, 64)
        Me.btnSel2.Name = "btnSel2"
        Me.btnSel2.Size = New System.Drawing.Size(144, 23)
        Me.btnSel2.TabIndex = 1
        Me.btnSel2.Text = "Precio"
        Me.btnSel2.UseVisualStyleBackColor = True
        '
        'btnSel3
        '
        Me.btnSel3.Location = New System.Drawing.Point(101, 93)
        Me.btnSel3.Name = "btnSel3"
        Me.btnSel3.Size = New System.Drawing.Size(144, 23)
        Me.btnSel3.TabIndex = 2
        Me.btnSel3.Text = "Cancelar"
        Me.btnSel3.UseVisualStyleBackColor = True
        '
        'pbLogo
        '
        Me.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pbLogo.Location = New System.Drawing.Point(12, 35)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(83, 64)
        Me.pbLogo.TabIndex = 3
        Me.pbLogo.TabStop = False
        '
        'frmEleccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(263, 150)
        Me.Controls.Add(Me.pbLogo)
        Me.Controls.Add(Me.btnSel3)
        Me.Controls.Add(Me.btnSel2)
        Me.Controls.Add(Me.btnSel1)
        Me.Name = "frmEleccion"
        Me.Text = "Modificar"
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSel1 As System.Windows.Forms.Button
    Friend WithEvents btnSel2 As System.Windows.Forms.Button
    Friend WithEvents btnSel3 As System.Windows.Forms.Button
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
End Class
