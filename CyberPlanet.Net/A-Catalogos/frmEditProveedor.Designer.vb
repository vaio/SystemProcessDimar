﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditProveedor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditProveedor))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtTasaIVA = New System.Windows.Forms.TextBox()
        Me.txtTasaIMI = New System.Windows.Forms.TextBox()
        Me.txtTasaIR = New System.Windows.Forms.TextBox()
        Me.ckIsIMI = New DevExpress.XtraEditors.CheckEdit()
        Me.ckIsIR = New DevExpress.XtraEditors.CheckEdit()
        Me.ckIsIVA = New DevExpress.XtraEditors.CheckEdit()
        Me.txtCuentaContable = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.luTipoProveedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblTipoProveedorBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtDireccion2 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtDireccion1 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTelefono2 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtLimiteCredito = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPlazo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbTipoCompra = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNumRuc = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbTipLiquidacion = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdAddMarca = New DevExpress.XtraEditors.SimpleButton()
        Me.luProcedencia = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblProcedenciaBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNomProv = New System.Windows.Forms.TextBox()
        Me.deFechaBaja = New DevExpress.XtraEditors.DateEdit()
        Me.deFechaIng = New DevExpress.XtraEditors.DateEdit()
        Me.lblFechaBaja = New System.Windows.Forms.Label()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.txtTelefono1 = New System.Windows.Forms.TextBox()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.txtNomContacto = New System.Windows.Forms.TextBox()
        Me.lblNomContacto = New System.Windows.Forms.Label()
        Me.lblNomProv = New System.Windows.Forms.Label()
        Me.lblCodProv = New System.Windows.Forms.Label()
        Me.txtCodProv = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        CType(Me.ckIsIMI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsIR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luTipoProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblTipoProveedorBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luProcedencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblProcedenciaBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtTasaIVA)
        Me.Panel2.Controls.Add(Me.txtTasaIMI)
        Me.Panel2.Controls.Add(Me.txtTasaIR)
        Me.Panel2.Controls.Add(Me.ckIsIMI)
        Me.Panel2.Controls.Add(Me.ckIsIR)
        Me.Panel2.Controls.Add(Me.ckIsIVA)
        Me.Panel2.Controls.Add(Me.txtCuentaContable)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.SimpleButton1)
        Me.Panel2.Controls.Add(Me.luTipoProveedor)
        Me.Panel2.Controls.Add(Me.txtEmail)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.txtDireccion2)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.txtDireccion1)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.txtTelefono2)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.txtLimiteCredito)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.txtPlazo)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.cmbTipoCompra)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txtNumRuc)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.cmbTipLiquidacion)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.cmdAddMarca)
        Me.Panel2.Controls.Add(Me.luProcedencia)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txtNomProv)
        Me.Panel2.Controls.Add(Me.deFechaBaja)
        Me.Panel2.Controls.Add(Me.deFechaIng)
        Me.Panel2.Controls.Add(Me.lblFechaBaja)
        Me.Panel2.Controls.Add(Me.lblFechaIngreso)
        Me.Panel2.Controls.Add(Me.ckIsActivo)
        Me.Panel2.Controls.Add(Me.txtTelefono1)
        Me.Panel2.Controls.Add(Me.lblTelefono)
        Me.Panel2.Controls.Add(Me.txtNomContacto)
        Me.Panel2.Controls.Add(Me.lblNomContacto)
        Me.Panel2.Controls.Add(Me.lblNomProv)
        Me.Panel2.Controls.Add(Me.lblCodProv)
        Me.Panel2.Controls.Add(Me.txtCodProv)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(735, 260)
        Me.Panel2.TabIndex = 0
        '
        'txtTasaIVA
        '
        Me.txtTasaIVA.Location = New System.Drawing.Point(677, 171)
        Me.txtTasaIVA.Name = "txtTasaIVA"
        Me.txtTasaIVA.Size = New System.Drawing.Size(40, 20)
        Me.txtTasaIVA.TabIndex = 24
        Me.txtTasaIVA.Text = "0"
        Me.txtTasaIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTasaIMI
        '
        Me.txtTasaIMI.Location = New System.Drawing.Point(677, 146)
        Me.txtTasaIMI.Name = "txtTasaIMI"
        Me.txtTasaIMI.Size = New System.Drawing.Size(40, 20)
        Me.txtTasaIMI.TabIndex = 22
        Me.txtTasaIMI.Text = "0"
        Me.txtTasaIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTasaIR
        '
        Me.txtTasaIR.Location = New System.Drawing.Point(677, 122)
        Me.txtTasaIR.Name = "txtTasaIR"
        Me.txtTasaIR.Size = New System.Drawing.Size(40, 20)
        Me.txtTasaIR.TabIndex = 20
        Me.txtTasaIR.Text = "0"
        Me.txtTasaIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ckIsIMI
        '
        Me.ckIsIMI.Location = New System.Drawing.Point(638, 147)
        Me.ckIsIMI.Name = "ckIsIMI"
        Me.ckIsIMI.Properties.Caption = "IMI"
        Me.ckIsIMI.Size = New System.Drawing.Size(39, 18)
        Me.ckIsIMI.TabIndex = 21
        '
        'ckIsIR
        '
        Me.ckIsIR.Location = New System.Drawing.Point(638, 122)
        Me.ckIsIR.Name = "ckIsIR"
        Me.ckIsIR.Properties.Caption = "IR"
        Me.ckIsIR.Size = New System.Drawing.Size(39, 18)
        Me.ckIsIR.TabIndex = 19
        '
        'ckIsIVA
        '
        Me.ckIsIVA.Location = New System.Drawing.Point(638, 171)
        Me.ckIsIVA.Name = "ckIsIVA"
        Me.ckIsIVA.Properties.Caption = "IVA"
        Me.ckIsIVA.Size = New System.Drawing.Size(39, 18)
        Me.ckIsIVA.TabIndex = 23
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.Location = New System.Drawing.Point(529, 224)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Size = New System.Drawing.Size(188, 20)
        Me.txtCuentaContable.TabIndex = 26
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(436, 227)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(108, 17)
        Me.Label12.TabIndex = 58
        Me.Label12.Text = "Cuenta Contable:"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Appearance.Options.UseForeColor = True
        Me.SimpleButton1.Location = New System.Drawing.Point(697, 15)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(20, 19)
        Me.SimpleButton1.TabIndex = 12
        Me.SimpleButton1.Text = "+"
        Me.SimpleButton1.ToolTip = "Agregar una nueva Marca"
        '
        'luTipoProveedor
        '
        Me.luTipoProveedor.Location = New System.Drawing.Point(546, 16)
        Me.luTipoProveedor.Name = "luTipoProveedor"
        Me.luTipoProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luTipoProveedor.Properties.DataSource = Me.TblTipoProveedorBS
        Me.luTipoProveedor.Size = New System.Drawing.Size(151, 20)
        Me.luTipoProveedor.TabIndex = 11
        '
        'txtEmail
        '
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtEmail.Location = New System.Drawing.Point(188, 137)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(232, 20)
        Me.txtEmail.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(21, 140)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(197, 20)
        Me.Label11.TabIndex = 54
        Me.Label11.Text = "Correo Electronico de Contacto :"
        '
        'txtDireccion2
        '
        Me.txtDireccion2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion2.Location = New System.Drawing.Point(88, 193)
        Me.txtDireccion2.Name = "txtDireccion2"
        Me.txtDireccion2.Size = New System.Drawing.Size(332, 20)
        Me.txtDireccion2.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(21, 196)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 20)
        Me.Label10.TabIndex = 52
        Me.Label10.Text = "Dirección 2:"
        '
        'txtDireccion1
        '
        Me.txtDireccion1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion1.Location = New System.Drawing.Point(88, 163)
        Me.txtDireccion1.Name = "txtDireccion1"
        Me.txtDireccion1.Size = New System.Drawing.Size(332, 20)
        Me.txtDireccion1.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(21, 168)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 20)
        Me.Label9.TabIndex = 50
        Me.Label9.Text = "Dirección 1:"
        '
        'txtTelefono2
        '
        Me.txtTelefono2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono2.Location = New System.Drawing.Point(337, 109)
        Me.txtTelefono2.Name = "txtTelefono2"
        Me.txtTelefono2.Size = New System.Drawing.Size(83, 20)
        Me.txtTelefono2.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(240, 112)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(108, 20)
        Me.Label8.TabIndex = 48
        Me.Label8.Text = "Telefono Contacto:"
        '
        'txtLimiteCredito
        '
        Me.txtLimiteCredito.Location = New System.Drawing.Point(546, 147)
        Me.txtLimiteCredito.Name = "txtLimiteCredito"
        Me.txtLimiteCredito.Size = New System.Drawing.Size(80, 20)
        Me.txtLimiteCredito.TabIndex = 18
        Me.txtLimiteCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(440, 150)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 20)
        Me.Label7.TabIndex = 46
        Me.Label7.Text = "Limite de Crédito:"
        '
        'txtPlazo
        '
        Me.txtPlazo.Location = New System.Drawing.Point(546, 121)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(81, 20)
        Me.txtPlazo.TabIndex = 17
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(438, 124)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Plazo de Crédito:"
        '
        'cmbTipoCompra
        '
        Me.cmbTipoCompra.FormattingEnabled = True
        Me.cmbTipoCompra.Items.AddRange(New Object() {"CREDITO", "CONTADO"})
        Me.cmbTipoCompra.Location = New System.Drawing.Point(546, 94)
        Me.cmbTipoCompra.Name = "cmbTipoCompra"
        Me.cmbTipoCompra.Size = New System.Drawing.Size(171, 21)
        Me.cmbTipoCompra.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(438, 97)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 20)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Tipo de Compras:"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(438, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 20)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Tipo de Proveedor:"
        '
        'txtNumRuc
        '
        Me.txtNumRuc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumRuc.Location = New System.Drawing.Point(300, 16)
        Me.txtNumRuc.Name = "txtNumRuc"
        Me.txtNumRuc.Size = New System.Drawing.Size(120, 20)
        Me.txtNumRuc.TabIndex = 1
        Me.txtNumRuc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(240, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 18)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Num RUC:"
        '
        'cmbTipLiquidacion
        '
        Me.cmbTipLiquidacion.FormattingEnabled = True
        Me.cmbTipLiquidacion.Location = New System.Drawing.Point(546, 67)
        Me.cmbTipLiquidacion.Name = "cmbTipLiquidacion"
        Me.cmbTipLiquidacion.Size = New System.Drawing.Size(171, 21)
        Me.cmbTipLiquidacion.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(438, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 20)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Tipo de Liquidación:"
        '
        'cmdAddMarca
        '
        Me.cmdAddMarca.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddMarca.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddMarca.Appearance.Options.UseFont = True
        Me.cmdAddMarca.Appearance.Options.UseForeColor = True
        Me.cmdAddMarca.Location = New System.Drawing.Point(697, 40)
        Me.cmdAddMarca.Name = "cmdAddMarca"
        Me.cmdAddMarca.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddMarca.TabIndex = 14
        Me.cmdAddMarca.Text = "+"
        Me.cmdAddMarca.ToolTip = "Agregar una nueva Marca"
        '
        'luProcedencia
        '
        Me.luProcedencia.Location = New System.Drawing.Point(546, 41)
        Me.luProcedencia.Name = "luProcedencia"
        Me.luProcedencia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luProcedencia.Properties.DataSource = Me.TblProcedenciaBS
        Me.luProcedencia.Size = New System.Drawing.Size(151, 20)
        Me.luProcedencia.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(438, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 20)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Procedencia:"
        '
        'txtNomProv
        '
        Me.txtNomProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomProv.Location = New System.Drawing.Point(135, 47)
        Me.txtNomProv.Name = "txtNomProv"
        Me.txtNomProv.Size = New System.Drawing.Size(285, 20)
        Me.txtNomProv.TabIndex = 2
        '
        'deFechaBaja
        '
        Me.deFechaBaja.EditValue = Nothing
        Me.deFechaBaja.Location = New System.Drawing.Point(290, 224)
        Me.deFechaBaja.Name = "deFechaBaja"
        Me.deFechaBaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaBaja.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaBaja.Size = New System.Drawing.Size(130, 20)
        Me.deFechaBaja.TabIndex = 10
        '
        'deFechaIng
        '
        Me.deFechaIng.EditValue = Nothing
        Me.deFechaIng.Location = New System.Drawing.Point(88, 224)
        Me.deFechaIng.Name = "deFechaIng"
        Me.deFechaIng.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaIng.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaIng.Size = New System.Drawing.Size(130, 20)
        Me.deFechaIng.TabIndex = 9
        '
        'lblFechaBaja
        '
        Me.lblFechaBaja.Location = New System.Drawing.Point(240, 227)
        Me.lblFechaBaja.Name = "lblFechaBaja"
        Me.lblFechaBaja.Size = New System.Drawing.Size(61, 17)
        Me.lblFechaBaja.TabIndex = 30
        Me.lblFechaBaja.Text = "F. Baja:"
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(21, 227)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(61, 17)
        Me.lblFechaIngreso.TabIndex = 29
        Me.lblFechaIngreso.Text = "F. Ingreso:"
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Enabled = False
        Me.ckIsActivo.Location = New System.Drawing.Point(638, 197)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Es Activo"
        Me.ckIsActivo.Size = New System.Drawing.Size(75, 18)
        Me.ckIsActivo.TabIndex = 25
        '
        'txtTelefono1
        '
        Me.txtTelefono1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefono1.Location = New System.Drawing.Point(135, 109)
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.Size = New System.Drawing.Size(83, 20)
        Me.txtTelefono1.TabIndex = 4
        '
        'lblTelefono
        '
        Me.lblTelefono.Location = New System.Drawing.Point(21, 112)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(108, 20)
        Me.lblTelefono.TabIndex = 7
        Me.lblTelefono.Text = "Telefono Proveedor:"
        '
        'txtNomContacto
        '
        Me.txtNomContacto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomContacto.Location = New System.Drawing.Point(135, 79)
        Me.txtNomContacto.Name = "txtNomContacto"
        Me.txtNomContacto.Size = New System.Drawing.Size(285, 20)
        Me.txtNomContacto.TabIndex = 3
        '
        'lblNomContacto
        '
        Me.lblNomContacto.Location = New System.Drawing.Point(21, 83)
        Me.lblNomContacto.Name = "lblNomContacto"
        Me.lblNomContacto.Size = New System.Drawing.Size(117, 20)
        Me.lblNomContacto.TabIndex = 5
        Me.lblNomContacto.Text = "Nombre de Contacto:"
        '
        'lblNomProv
        '
        Me.lblNomProv.Location = New System.Drawing.Point(21, 50)
        Me.lblNomProv.Name = "lblNomProv"
        Me.lblNomProv.Size = New System.Drawing.Size(117, 20)
        Me.lblNomProv.TabIndex = 3
        Me.lblNomProv.Text = "Nombre de Proveedor:"
        '
        'lblCodProv
        '
        Me.lblCodProv.Location = New System.Drawing.Point(21, 19)
        Me.lblCodProv.Name = "lblCodProv"
        Me.lblCodProv.Size = New System.Drawing.Size(108, 20)
        Me.lblCodProv.TabIndex = 1
        Me.lblCodProv.Text = "Codigo_Proveedor:"
        '
        'txtCodProv
        '
        Me.txtCodProv.BackColor = System.Drawing.SystemColors.Window
        Me.txtCodProv.Location = New System.Drawing.Point(135, 16)
        Me.txtCodProv.Name = "txtCodProv"
        Me.txtCodProv.ReadOnly = True
        Me.txtCodProv.Size = New System.Drawing.Size(83, 20)
        Me.txtCodProv.TabIndex = 0
        Me.txtCodProv.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 260)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(735, 49)
        Me.Panel1.TabIndex = 1
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(635, 6)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(551, 6)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'frmEditProveedor
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(735, 309)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditProveedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Proveedores"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.ckIsIMI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsIR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luTipoProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblTipoProveedorBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luProcedencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblProcedenciaBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblNomContacto As System.Windows.Forms.Label
    Friend WithEvents lblNomProv As System.Windows.Forms.Label
    Friend WithEvents txtNomProv As System.Windows.Forms.TextBox
    Friend WithEvents lblCodProv As System.Windows.Forms.Label
    Friend WithEvents txtCodProv As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents txtTelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtNomContacto As System.Windows.Forms.TextBox
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents deFechaBaja As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFechaIng As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaBaja As System.Windows.Forms.Label
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdAddMarca As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents luProcedencia As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmbTipLiquidacion As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TblProcedenciaBS As System.Windows.Forms.BindingSource
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNumRuc As System.Windows.Forms.TextBox
    Friend WithEvents txtLimiteCredito As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPlazo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoCompra As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion2 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion1 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents luTipoProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblTipoProveedorBS As System.Windows.Forms.BindingSource
    Friend WithEvents txtCuentaContable As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ckIsIMI As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckIsIR As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ckIsIVA As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtTasaIVA As System.Windows.Forms.TextBox
    Friend WithEvents txtTasaIMI As System.Windows.Forms.TextBox
    Friend WithEvents txtTasaIR As System.Windows.Forms.TextBox
End Class
