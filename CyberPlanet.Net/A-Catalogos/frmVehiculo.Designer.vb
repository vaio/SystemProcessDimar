﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVehiculo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVehiculo))
        Me.pmain = New System.Windows.Forms.Panel()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.nbgVehiculo = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nbiVtodo = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiVoperativo = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiVadministrativo = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiVhistorial = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbgAsignacion = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nbiAtodo = New DevExpress.XtraNavBar.NavBarItem()
        Me.nviAsinAsignar = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiAsignado = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiAhistorial = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbgVendedor = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nbiVeTodo = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiVesinAsignar = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiVeasignados = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiVehistorial = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbgMantenimiento = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarItem1 = New DevExpress.XtraNavBar.NavBarItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pmain.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pmain
        '
        Me.pmain.Controls.Add(Me.GridControl2)
        Me.pmain.Controls.Add(Me.GridControl1)
        Me.pmain.Controls.Add(Me.NavBarControl1)
        Me.pmain.Controls.Add(Me.PictureBox1)
        Me.pmain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pmain.Location = New System.Drawing.Point(0, 0)
        Me.pmain.Name = "pmain"
        Me.pmain.Size = New System.Drawing.Size(776, 624)
        Me.pmain.TabIndex = 0
        '
        'GridControl2
        '
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.Location = New System.Drawing.Point(179, 186)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(597, 438)
        Me.GridControl2.TabIndex = 175
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.GroupPanelText = "Detalles"
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView2.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView2.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView2.OptionsView.ShowFooter = True
        Me.GridView2.PaintStyleName = "UltraFlat"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GridControl1.Location = New System.Drawing.Point(179, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(597, 186)
        Me.GridControl1.TabIndex = 174
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.GroupPanelText = "Master"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.PaintStyleName = "UltraFlat"
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.nbgVehiculo
        Me.NavBarControl1.Appearance.Background.BackColor = System.Drawing.Color.LightSlateGray
        Me.NavBarControl1.Appearance.Background.Options.UseBackColor = True
        Me.NavBarControl1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.NavBarControl1.ContentButtonHint = Nothing
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.GroupBackgroundImage = CType(resources.GetObject("NavBarControl1.GroupBackgroundImage"), System.Drawing.Image)
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.nbgVehiculo, Me.nbgAsignacion, Me.nbgVendedor, Me.nbgMantenimiento})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NavBarItem1, Me.nbiAtodo, Me.nbiVtodo, Me.nbiVadministrativo, Me.nbiVoperativo, Me.nbiVhistorial, Me.nviAsinAsignar, Me.nbiAsignado, Me.nbiAhistorial, Me.nbiVeTodo, Me.nbiVesinAsignar, Me.nbiVeasignados, Me.nbiVehistorial})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 0)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.OptionsNavPane.ExpandedWidth = 179
        Me.NavBarControl1.Size = New System.Drawing.Size(179, 624)
        Me.NavBarControl1.TabIndex = 166
        Me.NavBarControl1.Text = "NavBarControl1"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.AdvExplorerBarViewInfoRegistrator()
        '
        'nbgVehiculo
        '
        Me.nbgVehiculo.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.nbgVehiculo.Appearance.BackColor2 = System.Drawing.SystemColors.ButtonFace
        Me.nbgVehiculo.Appearance.Options.UseBackColor = True
        Me.nbgVehiculo.AppearanceHotTracked.BackColor = System.Drawing.Color.White
        Me.nbgVehiculo.AppearanceHotTracked.Options.UseBackColor = True
        Me.nbgVehiculo.Caption = "Vehiculos"
        Me.nbgVehiculo.Expanded = True
        Me.nbgVehiculo.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText
        Me.nbgVehiculo.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVtodo), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVoperativo), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVadministrativo), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVhistorial)})
        Me.nbgVehiculo.Name = "nbgVehiculo"
        '
        'nbiVtodo
        '
        Me.nbiVtodo.Caption = "Mostrar Todo"
        Me.nbiVtodo.Name = "nbiVtodo"
        '
        'nbiVoperativo
        '
        Me.nbiVoperativo.Caption = "Vehiculos Operativos"
        Me.nbiVoperativo.Name = "nbiVoperativo"
        '
        'nbiVadministrativo
        '
        Me.nbiVadministrativo.Caption = "Vehiculos Administrativos"
        Me.nbiVadministrativo.Name = "nbiVadministrativo"
        '
        'nbiVhistorial
        '
        Me.nbiVhistorial.Caption = "Historial"
        Me.nbiVhistorial.Name = "nbiVhistorial"
        '
        'nbgAsignacion
        '
        Me.nbgAsignacion.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.nbgAsignacion.Appearance.BackColor2 = System.Drawing.SystemColors.ButtonFace
        Me.nbgAsignacion.Appearance.Options.UseBackColor = True
        Me.nbgAsignacion.Caption = "Rutas"
        Me.nbgAsignacion.Expanded = True
        Me.nbgAsignacion.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText
        Me.nbgAsignacion.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiAtodo), New DevExpress.XtraNavBar.NavBarItemLink(Me.nviAsinAsignar), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiAsignado), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiAhistorial)})
        Me.nbgAsignacion.Name = "nbgAsignacion"
        '
        'nbiAtodo
        '
        Me.nbiAtodo.Caption = "Mostrar todos"
        Me.nbiAtodo.Name = "nbiAtodo"
        '
        'nviAsinAsignar
        '
        Me.nviAsinAsignar.Caption = "Vehiculos Sin Asignar"
        Me.nviAsinAsignar.Name = "nviAsinAsignar"
        '
        'nbiAsignado
        '
        Me.nbiAsignado.Caption = "Vehiculos Asignados"
        Me.nbiAsignado.Name = "nbiAsignado"
        '
        'nbiAhistorial
        '
        Me.nbiAhistorial.Caption = "Historial"
        Me.nbiAhistorial.Name = "nbiAhistorial"
        '
        'nbgVendedor
        '
        Me.nbgVendedor.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.nbgVendedor.Appearance.BackColor2 = System.Drawing.SystemColors.ButtonFace
        Me.nbgVendedor.Appearance.Options.UseBackColor = True
        Me.nbgVendedor.Caption = "Vendedores"
        Me.nbgVendedor.Expanded = True
        Me.nbgVendedor.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText
        Me.nbgVendedor.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVeTodo), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVesinAsignar), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVeasignados), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiVehistorial)})
        Me.nbgVendedor.Name = "nbgVendedor"
        '
        'nbiVeTodo
        '
        Me.nbiVeTodo.Caption = "Mostrar Todos"
        Me.nbiVeTodo.Name = "nbiVeTodo"
        '
        'nbiVesinAsignar
        '
        Me.nbiVesinAsignar.Caption = "Vendedores no Asignados"
        Me.nbiVesinAsignar.Name = "nbiVesinAsignar"
        '
        'nbiVeasignados
        '
        Me.nbiVeasignados.Caption = "Vendedores Asignados"
        Me.nbiVeasignados.Name = "nbiVeasignados"
        '
        'nbiVehistorial
        '
        Me.nbiVehistorial.Caption = "Historial"
        Me.nbiVehistorial.Name = "nbiVehistorial"
        '
        'nbgMantenimiento
        '
        Me.nbgMantenimiento.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.nbgMantenimiento.Appearance.BackColor2 = System.Drawing.SystemColors.ButtonFace
        Me.nbgMantenimiento.Appearance.Options.UseBackColor = True
        Me.nbgMantenimiento.Caption = "Mantenimiento"
        Me.nbgMantenimiento.Name = "nbgMantenimiento"
        '
        'NavBarItem1
        '
        Me.NavBarItem1.Caption = "NavBarItem1"
        Me.NavBarItem1.Name = "NavBarItem1"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.Color.LightSlateGray
        Me.PictureBox1.BackgroundImage = Global.SIGMA.My.Resources.Resources.Imagen2
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(13, 493)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(149, 120)
        Me.PictureBox1.TabIndex = 168
        Me.PictureBox1.TabStop = False
        '
        'frmVehiculo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 624)
        Me.Controls.Add(Me.pmain)
        Me.Name = "frmVehiculo"
        Me.Text = "frmVehiculo"
        Me.pmain.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pmain As System.Windows.Forms.Panel
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents nbgVehiculo As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nbgAsignacion As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nbgMantenimiento As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarItem1 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiAtodo As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents nbgVendedor As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nbiVtodo As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiVadministrativo As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiVoperativo As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiVhistorial As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nviAsinAsignar As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiAsignado As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiAhistorial As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents nbiVeTodo As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiVesinAsignar As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiVeasignados As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiVehistorial As DevExpress.XtraNavBar.NavBarItem
End Class
