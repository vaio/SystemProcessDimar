﻿Public Class frmEditProveedor

    Private Sub frmEditProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarTipoProveedor()
        CargarProcedencia()
        CargarTipoLiquidacion()
        CargarTipoCompra()

        If nTipoEdic = 1 Then
            lblCodProv.Visible = True
            txtCodProv.Visible = True
            txtCodProv.Text = Microsoft.VisualBasic.Right("000" & RegistroMaximo(), 3)
            txtNomProv.Focus()
            deFechaIng.DateTime = Now
            deFechaBaja.DateTime = Now
            cmbTipLiquidacion.SelectedIndex = 0
            cmbTipoCompra.SelectedIndex = 0
            ckIsActivo.Checked = 1
        ElseIf nTipoEdic = 2 Then
            lblCodProv.Visible = True
            txtCodProv.Enabled = False
            txtCodProv.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 4 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Sub CargarTipoProveedor()
        Dim strsqlProv As String = ""
        strsqlProv = "Select Id_TipoProveedor, Tipo_Proveedor from [Tbl_TipoProveedor] order by Id_TipoProveedor"
        TblTipoProveedorBS.DataSource = SQL(strsqlProv, "tblDatosTipoProv", My.Settings.SolIndustrialCNX).Tables(0)
        luTipoProveedor.Properties.DataSource = TblTipoProveedorBS
        luTipoProveedor.Properties.DisplayMember = "Tipo_Proveedor"
        luTipoProveedor.Properties.ValueMember = "Id_TipoProveedor"
        luTipoProveedor.ItemIndex = 0
    End Sub

    Sub CargarProcedencia()
        Dim strsql As String = ""
        strsql = "Select Id_Procedencia, Nom_Procedencia from [Tbl_Procedencia] order by Id_Procedencia"
        TblProcedenciaBS.DataSource = SQL(strsql, "tblDatosProced", My.Settings.SolIndustrialCNX).Tables(0)
        luProcedencia.Properties.DataSource = TblProcedenciaBS
        luProcedencia.Properties.DisplayMember = "Nom_Procedencia"
        luProcedencia.Properties.ValueMember = "Id_Procedencia"
        luProcedencia.ItemIndex = 0
    End Sub

    Sub CargarTipoLiquidacion()
        Dim strsql As String = ""
        strsql = "Select Id_TipoLiquidacion, TipoLiquidacion from [Catalogo].[Tipo_Liquidacion] order by Id_TipoLiquidacion"
        cmbTipLiquidacion.DataSource = SQL(strsql, "tblTipoLiquidacion", My.Settings.SolIndustrialCNX).Tables(0)
        cmbTipLiquidacion.DisplayMember = "TipoLiquidacion"
        cmbTipLiquidacion.ValueMember = "Id_TipoLiquidacion"
        cmbTipLiquidacion.SelectedIndex = 0
    End Sub

    Sub CargarTipoCompra()
        Dim strsql As String = ""
        strsql = "Select Id_TipoCompra, TipoCompra from [Catalogo].[Tipo_Compra] order by Id_TipoCompra"
        cmbTipoCompra.DataSource = SQL(strsql, "tblTipoCompra", My.Settings.SolIndustrialCNX).Tables(0)
        cmbTipoCompra.DisplayMember = "TipoCompra"
        cmbTipoCompra.ValueMember = "Id_TipoCompra"
        cmbTipoCompra.SelectedIndex = 0
    End Sub

  Public Sub CargarDatos(ByVal CodigoEntidad As String)
    Dim strsql As String = ""

    strsql = "SELECT Codigo_Proveedor,Num_RUC,Nombre_Proveedor,Nombre_Contacto,Tel_Prov,Tel_Contacto,Email,Direccion_Oficina_1,Direccion_Oficina_2,Fecha_Ingreso,Fecha_Baja,Id_TipoProveedor,Id_Procedencia,Id_TipoLiquidacion,Tipo_de_Compra,Plazo,Limite_Credito,Cuenta_Contable,Is_IR,Is_IMI,Is_IVA,TasaIR,TasaIMI,TasaIVA,Activo FROM Tbl_Proveedor WHERE Codigo_Proveedor='" & CodigoEntidad & "'"
        Dim tblDatosProv As DataTable = SQL(strsql, "tblDatosProv", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosProv.Rows.Count <> 0 Then
            txtCodProv.Text = tblDatosProv.Rows(0).Item(0)
            txtNumRuc.Text = tblDatosProv.Rows(0).Item(1)
            txtNomProv.Text = tblDatosProv.Rows(0).Item(2)
            txtNomContacto.Text = IIf(IsDBNull(tblDatosProv.Rows(0).Item(3)), 0, tblDatosProv.Rows(0).Item(3))
            txtTelefono1.Text = tblDatosProv.Rows(0).Item(4)
            txtTelefono2.Text = tblDatosProv.Rows(0).Item(5)
            txtEmail.Text = txtTelefono1.Text = tblDatosProv.Rows(0).Item(6)
            txtDireccion1.Text = tblDatosProv.Rows(0).Item(7)
            txtDireccion2.Text = tblDatosProv.Rows(0).Item(8)
            deFechaIng.EditValue = tblDatosProv.Rows(0).Item(9)
            deFechaBaja.EditValue = tblDatosProv.Rows(0).Item(10)
            luTipoProveedor.EditValue = tblDatosProv.Rows(0).Item(11)
            luProcedencia.EditValue = tblDatosProv.Rows(0).Item(12)
            cmbTipLiquidacion.SelectedValue = tblDatosProv.Rows(0).Item(13)
            cmbTipoCompra.SelectedValue = tblDatosProv.Rows(0).Item(14)
            txtPlazo.Text = tblDatosProv.Rows(0).Item(15)
            txtLimiteCredito.Text = tblDatosProv.Rows(0).Item(16)
            txtCuentaContable.Text = IIf(IsDBNull(tblDatosProv.Rows(0).Item(17)), "", tblDatosProv.Rows(0).Item(17))
            ckIsIR.Checked = IIf(IsDBNull(tblDatosProv.Rows(0).Item(18)), 0, tblDatosProv.Rows(0).Item(18))
            ckIsIMI.Checked = IIf(IsDBNull(tblDatosProv.Rows(0).Item(19)), 0, tblDatosProv.Rows(0).Item(19))
            ckIsIVA.Checked = IIf(IsDBNull(tblDatosProv.Rows(0).Item(20)), 0, tblDatosProv.Rows(0).Item(20))
            txtTasaIR.Text = IIf(IsDBNull(tblDatosProv.Rows(0).Item(21)), 0, tblDatosProv.Rows(0).Item(21))
            txtTasaIMI.Text = IIf(IsDBNull(tblDatosProv.Rows(0).Item(22)), 0, tblDatosProv.Rows(0).Item(22))
            txtTasaIVA.Text = IIf(IsDBNull(tblDatosProv.Rows(0).Item(23)), 0, tblDatosProv.Rows(0).Item(23))
            ckIsActivo.Checked = IIf(IsDBNull(tblDatosProv.Rows(0).Item(21)), 0, tblDatosProv.Rows(0).Item(21))
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoProv As New clsCatalogos(My.Settings.SolIndustrialCNX)

      If txtNumRuc.Text = "" Then
        MsgBox("Debe ingresar el número RUC o de Identificación del proveedor.", MsgBoxStyle.Information, "WorkSystem")
        Exit Sub
      End If

        Try
            Resum = CatalogoProv.EditarProv(IIf(txtCodProv.Text = "", "000", txtCodProv.Text), txtNumRuc.Text.ToUpper, txtNomProv.Text.ToUpper,
                                                     txtNomContacto.Text.ToUpper, txtTelefono1.Text, txtTelefono2.Text, txtEmail.Text, txtDireccion1.Text.ToUpper,
                                                     txtDireccion2.Text.ToUpper, CInt(luTipoProveedor.EditValue), CInt(luProcedencia.EditValue), cmbTipLiquidacion.SelectedValue,
                                                     cmbTipoCompra.SelectedValue, CInt(IIf(txtPlazo.Text = "", "0", txtPlazo.Text)), CDbl(IIf(txtLimiteCredito.Text = "", "0", txtLimiteCredito.Text)), ckIsIR.Checked, ckIsIMI.Checked, ckIsIVA.Checked, CInt(txtTasaIR.Text), CInt(txtTasaIMI.Text), CInt(txtTasaIVA.Text),
                                                     txtCuentaContable.Text, nTipoEdic)
            If Resum = "OK" Then
                MsgBox("La operación solicitada se realizó de forma exitosa.", MsgBoxStyle.Information, "Información")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

	Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
		Cerrar()
	End Sub

	Public Sub Cerrar()
		Me.Dispose()
		Close()
	End Sub

Private Sub ckIsIR_CheckedChanged(sender As Object, e As EventArgs) Handles ckIsIR.CheckedChanged
  If ckIsIR.Checked = False Then
      txtTasaIR.Text = 0
      txtTasaIR.ReadOnly = True
  Else
      txtTasaIR.ReadOnly = False
      txtTasaIR.Focus()
      txtTasaIR.Text = 2
      txtTasaIR.SelectionStart = 0
      txtTasaIR.SelectionLength = txtTasaIR.TextLength
  End If
End Sub

Private Sub ckIsIMI_CheckedChanged(sender As Object, e As EventArgs) Handles ckIsIMI.CheckedChanged
  If ckIsIMI.Checked = False Then
      txtTasaIMI.Text = 0
      txtTasaIMI.ReadOnly = True
  Else
      txtTasaIMI.ReadOnly = False
      txtTasaIMI.Focus()
      txtTasaIMI.Text = 1
      txtTasaIMI.SelectionStart = 0
      txtTasaIMI.SelectionLength = txtTasaIMI.TextLength
  End If
End Sub

Private Sub ckIsIVA_CheckedChanged(sender As Object, e As EventArgs) Handles ckIsIVA.CheckedChanged
  If ckIsIVA.Checked = False Then
      txtTasaIVA.Text = 0
      txtTasaIVA.ReadOnly = True
  Else
      txtTasaIVA.ReadOnly = False
      txtTasaIVA.Focus()
      txtTasaIVA.Text = 15
      txtTasaIVA.SelectionStart = 0
      txtTasaIVA.SelectionLength = txtTasaIVA.TextLength
  End If
End Sub
End Class