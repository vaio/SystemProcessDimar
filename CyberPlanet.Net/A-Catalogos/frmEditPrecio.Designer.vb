﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditPrecio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditPrecio))
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.lblCodProdServ = New System.Windows.Forms.Label()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblNomProdServ = New System.Windows.Forms.Label()
        Me.txtAlterno = New System.Windows.Forms.TextBox()
        Me.lblCostoProm = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtGenerico = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCostoCS = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.txtTC = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lbCodigo = New System.Windows.Forms.Label()
        Me.lbDeco = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lbD3 = New System.Windows.Forms.Label()
        Me.ckbIVA3 = New System.Windows.Forms.CheckBox()
        Me.txtFacturacion2 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtFacturacion1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbD2 = New System.Windows.Forms.Label()
        Me.ckbIVA2 = New System.Windows.Forms.CheckBox()
        Me.txtMayorista2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMayorista1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ckbActivo = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ckbIVA1 = New System.Windows.Forms.CheckBox()
        Me.txtDetalle2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtDetelle1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lbD1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.pComando = New System.Windows.Forms.Panel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.pMaster.SuspendLayout()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDetalle.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pComando.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.cbTipo)
        Me.pMaster.Controls.Add(Me.txtCodigo)
        Me.pMaster.Controls.Add(Me.lblCodProdServ)
        Me.pMaster.Controls.Add(Me.txtProducto)
        Me.pMaster.Controls.Add(Me.Label4)
        Me.pMaster.Controls.Add(Me.lblNomProdServ)
        Me.pMaster.Controls.Add(Me.txtAlterno)
        Me.pMaster.Controls.Add(Me.lblCostoProm)
        Me.pMaster.Controls.Add(Me.Label3)
        Me.pMaster.Controls.Add(Me.txtGenerico)
        Me.pMaster.Controls.Add(Me.Label1)
        Me.pMaster.Controls.Add(Me.txtCostoCS)
        Me.pMaster.Controls.Add(Me.Label9)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Top
        Me.pMaster.Enabled = False
        Me.pMaster.Location = New System.Drawing.Point(0, 0)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(725, 100)
        Me.pMaster.TabIndex = 0
        '
        'cbTipo
        '
        Me.cbTipo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbTipo.EditValue = "PRODUCTO"
        Me.cbTipo.Enabled = False
        Me.cbTipo.Location = New System.Drawing.Point(371, 64)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.cbTipo.Properties.Appearance.Options.UseForeColor = True
        Me.cbTipo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTipo.Properties.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(339, 20)
        Me.cbTipo.TabIndex = 142
        Me.cbTipo.TabStop = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.White
        Me.txtCodigo.Location = New System.Drawing.Point(119, 13)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(119, 20)
        Me.txtCodigo.TabIndex = 56
        '
        'lblCodProdServ
        '
        Me.lblCodProdServ.Location = New System.Drawing.Point(30, 13)
        Me.lblCodProdServ.Name = "lblCodProdServ"
        Me.lblCodProdServ.Size = New System.Drawing.Size(83, 20)
        Me.lblCodProdServ.TabIndex = 57
        Me.lblCodProdServ.Text = "Codigo Articulo:"
        Me.lblCodProdServ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtProducto
        '
        Me.txtProducto.BackColor = System.Drawing.Color.White
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Location = New System.Drawing.Point(371, 12)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(339, 20)
        Me.txtProducto.TabIndex = 58
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(287, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 20)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Tipo Articulo:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNomProdServ
        '
        Me.lblNomProdServ.Location = New System.Drawing.Point(263, 12)
        Me.lblNomProdServ.Name = "lblNomProdServ"
        Me.lblNomProdServ.Size = New System.Drawing.Size(102, 20)
        Me.lblNomProdServ.TabIndex = 60
        Me.lblNomProdServ.Text = "Nombre del Articulo:"
        Me.lblNomProdServ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAlterno
        '
        Me.txtAlterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAlterno.Location = New System.Drawing.Point(119, 39)
        Me.txtAlterno.Name = "txtAlterno"
        Me.txtAlterno.Size = New System.Drawing.Size(119, 20)
        Me.txtAlterno.TabIndex = 59
        '
        'lblCostoProm
        '
        Me.lblCostoProm.Location = New System.Drawing.Point(50, 64)
        Me.lblCostoProm.Name = "lblCostoProm"
        Me.lblCostoProm.Size = New System.Drawing.Size(84, 20)
        Me.lblCostoProm.TabIndex = 63
        Me.lblCostoProm.Text = "Costo Promedio:"
        Me.lblCostoProm.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Label3.Location = New System.Drawing.Point(263, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 20)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Nombre Generico:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtGenerico
        '
        Me.txtGenerico.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGenerico.Enabled = False
        Me.txtGenerico.Location = New System.Drawing.Point(371, 38)
        Me.txtGenerico.Name = "txtGenerico"
        Me.txtGenerico.Size = New System.Drawing.Size(339, 20)
        Me.txtGenerico.TabIndex = 61
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(31, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 20)
        Me.Label1.TabIndex = 65
        Me.Label1.Text = "Codigo Alterno:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCostoCS
        '
        Me.txtCostoCS.Location = New System.Drawing.Point(163, 65)
        Me.txtCostoCS.Name = "txtCostoCS"
        Me.txtCostoCS.Size = New System.Drawing.Size(75, 20)
        Me.txtCostoCS.TabIndex = 64
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(133, 65)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(24, 20)
        Me.Label9.TabIndex = 69
        Me.Label9.Text = "C$:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.txtTC)
        Me.pDetalle.Controls.Add(Me.Label10)
        Me.pDetalle.Controls.Add(Me.lbCodigo)
        Me.pDetalle.Controls.Add(Me.lbDeco)
        Me.pDetalle.Controls.Add(Me.GroupBox3)
        Me.pDetalle.Controls.Add(Me.GroupBox2)
        Me.pDetalle.Controls.Add(Me.ckbActivo)
        Me.pDetalle.Controls.Add(Me.GroupBox1)
        Me.pDetalle.Controls.Add(Me.Label8)
        Me.pDetalle.Controls.Add(Me.dtpFecha)
        Me.pDetalle.Dock = System.Windows.Forms.DockStyle.Right
        Me.pDetalle.Enabled = False
        Me.pDetalle.Location = New System.Drawing.Point(424, 100)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(301, 259)
        Me.pDetalle.TabIndex = 1
        '
        'txtTC
        '
        Me.txtTC.BackColor = System.Drawing.Color.White
        Me.txtTC.Enabled = False
        Me.txtTC.Location = New System.Drawing.Point(58, 8)
        Me.txtTC.Name = "txtTC"
        Me.txtTC.Size = New System.Drawing.Size(89, 20)
        Me.txtTC.TabIndex = 79
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(19, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 20)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "T/C:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbCodigo
        '
        Me.lbCodigo.Location = New System.Drawing.Point(260, 4)
        Me.lbCodigo.Name = "lbCodigo"
        Me.lbCodigo.Size = New System.Drawing.Size(34, 20)
        Me.lbCodigo.TabIndex = 86
        Me.lbCodigo.Text = "0"
        Me.lbCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbCodigo.Visible = False
        '
        'lbDeco
        '
        Me.lbDeco.AutoSize = True
        Me.lbDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.lbDeco.Location = New System.Drawing.Point(3, -38)
        Me.lbDeco.Name = "lbDeco"
        Me.lbDeco.Size = New System.Drawing.Size(298, 42)
        Me.lbDeco.TabIndex = 87
        Me.lbDeco.Text = "______________"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lbD3)
        Me.GroupBox3.Controls.Add(Me.ckbIVA3)
        Me.GroupBox3.Controls.Add(Me.txtFacturacion2)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.txtFacturacion1)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Location = New System.Drawing.Point(5, 167)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(290, 49)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Facturacion"
        '
        'lbD3
        '
        Me.lbD3.Location = New System.Drawing.Point(255, 30)
        Me.lbD3.Name = "lbD3"
        Me.lbD3.Size = New System.Drawing.Size(34, 20)
        Me.lbD3.TabIndex = 87
        Me.lbD3.Text = "0"
        Me.lbD3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbD3.Visible = False
        '
        'ckbIVA3
        '
        Me.ckbIVA3.AutoSize = True
        Me.ckbIVA3.Checked = True
        Me.ckbIVA3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbIVA3.Location = New System.Drawing.Point(246, 16)
        Me.ckbIVA3.Name = "ckbIVA3"
        Me.ckbIVA3.Size = New System.Drawing.Size(43, 17)
        Me.ckbIVA3.TabIndex = 6
        Me.ckbIVA3.Text = "IVA"
        Me.ckbIVA3.UseVisualStyleBackColor = True
        '
        'txtFacturacion2
        '
        Me.txtFacturacion2.BackColor = System.Drawing.Color.White
        Me.txtFacturacion2.Enabled = False
        Me.txtFacturacion2.ForeColor = System.Drawing.SystemColors.MenuText
        Me.txtFacturacion2.Location = New System.Drawing.Point(175, 14)
        Me.txtFacturacion2.Name = "txtFacturacion2"
        Me.txtFacturacion2.ReadOnly = True
        Me.txtFacturacion2.Size = New System.Drawing.Size(65, 20)
        Me.txtFacturacion2.TabIndex = 74
        Me.txtFacturacion2.TabStop = False
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(157, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(12, 20)
        Me.Label7.TabIndex = 75
        Me.Label7.Text = "="
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFacturacion1
        '
        Me.txtFacturacion1.BackColor = System.Drawing.Color.White
        Me.txtFacturacion1.Location = New System.Drawing.Point(53, 14)
        Me.txtFacturacion1.Name = "txtFacturacion1"
        Me.txtFacturacion1.Size = New System.Drawing.Size(89, 20)
        Me.txtFacturacion1.TabIndex = 5
        Me.txtFacturacion1.Text = "0.00"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(13, 14)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(34, 20)
        Me.Label12.TabIndex = 73
        Me.Label12.Text = "C$:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lbD2)
        Me.GroupBox2.Controls.Add(Me.ckbIVA2)
        Me.GroupBox2.Controls.Add(Me.txtMayorista2)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtMayorista1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 112)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(290, 49)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Mayorista"
        '
        'lbD2
        '
        Me.lbD2.Location = New System.Drawing.Point(255, 30)
        Me.lbD2.Name = "lbD2"
        Me.lbD2.Size = New System.Drawing.Size(34, 20)
        Me.lbD2.TabIndex = 86
        Me.lbD2.Text = "0"
        Me.lbD2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbD2.Visible = False
        '
        'ckbIVA2
        '
        Me.ckbIVA2.AutoSize = True
        Me.ckbIVA2.Checked = True
        Me.ckbIVA2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbIVA2.Location = New System.Drawing.Point(246, 16)
        Me.ckbIVA2.Name = "ckbIVA2"
        Me.ckbIVA2.Size = New System.Drawing.Size(43, 17)
        Me.ckbIVA2.TabIndex = 4
        Me.ckbIVA2.Text = "IVA"
        Me.ckbIVA2.UseVisualStyleBackColor = True
        '
        'txtMayorista2
        '
        Me.txtMayorista2.BackColor = System.Drawing.Color.White
        Me.txtMayorista2.Location = New System.Drawing.Point(53, 14)
        Me.txtMayorista2.Name = "txtMayorista2"
        Me.txtMayorista2.Size = New System.Drawing.Size(89, 20)
        Me.txtMayorista2.TabIndex = 3
        Me.txtMayorista2.Text = "0.00"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(1, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 20)
        Me.Label2.TabIndex = 75
        Me.Label2.Text = "US$:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMayorista1
        '
        Me.txtMayorista1.BackColor = System.Drawing.Color.White
        Me.txtMayorista1.Enabled = False
        Me.txtMayorista1.ForeColor = System.Drawing.SystemColors.MenuText
        Me.txtMayorista1.Location = New System.Drawing.Point(175, 14)
        Me.txtMayorista1.Name = "txtMayorista1"
        Me.txtMayorista1.ReadOnly = True
        Me.txtMayorista1.Size = New System.Drawing.Size(65, 20)
        Me.txtMayorista1.TabIndex = 72
        Me.txtMayorista1.TabStop = False
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(157, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(12, 20)
        Me.Label6.TabIndex = 73
        Me.Label6.Text = "="
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ckbActivo
        '
        Me.ckbActivo.AutoSize = True
        Me.ckbActivo.Location = New System.Drawing.Point(169, 8)
        Me.ckbActivo.Name = "ckbActivo"
        Me.ckbActivo.Size = New System.Drawing.Size(56, 17)
        Me.ckbActivo.TabIndex = 84
        Me.ckbActivo.TabStop = False
        Me.ckbActivo.Text = "Activo"
        Me.ckbActivo.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ckbIVA1)
        Me.GroupBox1.Controls.Add(Me.txtDetalle2)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtDetelle1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lbD1)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 57)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(290, 49)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Detalle"
        '
        'ckbIVA1
        '
        Me.ckbIVA1.AutoSize = True
        Me.ckbIVA1.Checked = True
        Me.ckbIVA1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbIVA1.Location = New System.Drawing.Point(246, 16)
        Me.ckbIVA1.Name = "ckbIVA1"
        Me.ckbIVA1.Size = New System.Drawing.Size(43, 17)
        Me.ckbIVA1.TabIndex = 2
        Me.ckbIVA1.Text = "IVA"
        Me.ckbIVA1.UseVisualStyleBackColor = True
        '
        'txtDetalle2
        '
        Me.txtDetalle2.BackColor = System.Drawing.Color.White
        Me.txtDetalle2.Enabled = False
        Me.txtDetalle2.ForeColor = System.Drawing.SystemColors.MenuText
        Me.txtDetalle2.Location = New System.Drawing.Point(175, 14)
        Me.txtDetalle2.Name = "txtDetalle2"
        Me.txtDetalle2.Size = New System.Drawing.Size(65, 20)
        Me.txtDetalle2.TabIndex = 74
        Me.txtDetalle2.TabStop = False
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(154, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(15, 20)
        Me.Label11.TabIndex = 75
        Me.Label11.Text = "="
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDetelle1
        '
        Me.txtDetelle1.BackColor = System.Drawing.Color.White
        Me.txtDetelle1.Location = New System.Drawing.Point(53, 14)
        Me.txtDetelle1.Name = "txtDetelle1"
        Me.txtDetelle1.Size = New System.Drawing.Size(89, 20)
        Me.txtDetelle1.TabIndex = 1
        Me.txtDetelle1.Text = "0.00"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(13, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 20)
        Me.Label5.TabIndex = 73
        Me.Label5.Text = "C$:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbD1
        '
        Me.lbD1.Location = New System.Drawing.Point(255, 29)
        Me.lbD1.Name = "lbD1"
        Me.lbD1.Size = New System.Drawing.Size(34, 20)
        Me.lbD1.TabIndex = 78
        Me.lbD1.Text = "0"
        Me.lbD1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbD1.Visible = False
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(3, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 20)
        Me.Label8.TabIndex = 80
        Me.Label8.Text = "Fecha:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(58, 34)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(227, 20)
        Me.dtpFecha.TabIndex = 79
        Me.dtpFecha.TabStop = False
        '
        'pComando
        '
        Me.pComando.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pComando.Controls.Add(Me.btnNuevo)
        Me.pComando.Controls.Add(Me.btnCancelar)
        Me.pComando.Controls.Add(Me.btnEditar)
        Me.pComando.Location = New System.Drawing.Point(430, 323)
        Me.pComando.Name = "pComando"
        Me.pComando.Size = New System.Drawing.Size(295, 49)
        Me.pComando.TabIndex = 210
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(44, 9)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(78, 30)
        Me.btnNuevo.TabIndex = 6
        Me.btnNuevo.Text = "&Agregar"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.Image = CType(resources.GetObject("btnCancelar.Image"), System.Drawing.Image)
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(212, 9)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(78, 30)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "&Salir"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEditar.Image = CType(resources.GetObject("btnEditar.Image"), System.Drawing.Image)
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.Location = New System.Drawing.Point(128, 9)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(78, 30)
        Me.btnEditar.TabIndex = 7
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 359)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(725, 14)
        Me.Panel1.TabIndex = 211
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(12, 100)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(412, 259)
        Me.GridControl1.TabIndex = 212
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.PaintStyleName = "UltraFlat"
        '
        'frmEditPrecio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 373)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.pComando)
        Me.Controls.Add(Me.pDetalle)
        Me.Controls.Add(Me.pMaster)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmEditPrecio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEditPrecio"
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pComando.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents lblCodProdServ As System.Windows.Forms.Label
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblNomProdServ As System.Windows.Forms.Label
    Friend WithEvents txtAlterno As System.Windows.Forms.TextBox
    Friend WithEvents lblCostoProm As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtGenerico As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCostoCS As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents ckbActivo As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDetalle2 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtDetelle1 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbD1 As System.Windows.Forms.Label
    Friend WithEvents ckbIVA1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ckbIVA3 As System.Windows.Forms.CheckBox
    Friend WithEvents txtFacturacion2 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacion1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ckbIVA2 As System.Windows.Forms.CheckBox
    Friend WithEvents txtMayorista2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pComando As System.Windows.Forms.Panel
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lbDeco As System.Windows.Forms.Label
    Friend WithEvents lbD3 As System.Windows.Forms.Label
    Friend WithEvents lbD2 As System.Windows.Forms.Label
    Friend WithEvents lbCodigo As System.Windows.Forms.Label
    Friend WithEvents txtMayorista1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTC As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class
