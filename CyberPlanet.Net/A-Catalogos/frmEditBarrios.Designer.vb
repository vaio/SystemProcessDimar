﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditBarrios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditBarrios))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.cmdAddDistZona = New DevExpress.XtraEditors.SimpleButton()
		Me.luZonaDist = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblDistZonaAsociada = New System.Windows.Forms.Label()
		Me.cmdAddMunicipio = New DevExpress.XtraEditors.SimpleButton()
		Me.luMunic = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblMunicAsociado = New System.Windows.Forms.Label()
		Me.cmdAddDeparPais = New DevExpress.XtraEditors.SimpleButton()
		Me.luDepartPais = New DevExpress.XtraEditors.LookUpEdit()
		Me.lblDepartPaisAsoc = New System.Windows.Forms.Label()
		Me.lblNomBarrio = New System.Windows.Forms.Label()
		Me.txtNomBarrio = New System.Windows.Forms.TextBox()
		Me.lblCodBarrio = New System.Windows.Forms.Label()
		Me.txtCodBarrio = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.TblDepartPaisBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblMunicipioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.TblZonaDistBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.Panel2.SuspendLayout()
		CType(Me.luZonaDist.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luMunic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.luDepartPais.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.TblDepartPaisBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblMunicipioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TblZonaDistBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.cmdAddDistZona)
		Me.Panel2.Controls.Add(Me.luZonaDist)
		Me.Panel2.Controls.Add(Me.lblDistZonaAsociada)
		Me.Panel2.Controls.Add(Me.cmdAddMunicipio)
		Me.Panel2.Controls.Add(Me.luMunic)
		Me.Panel2.Controls.Add(Me.lblMunicAsociado)
		Me.Panel2.Controls.Add(Me.cmdAddDeparPais)
		Me.Panel2.Controls.Add(Me.luDepartPais)
		Me.Panel2.Controls.Add(Me.lblDepartPaisAsoc)
		Me.Panel2.Controls.Add(Me.lblNomBarrio)
		Me.Panel2.Controls.Add(Me.txtNomBarrio)
		Me.Panel2.Controls.Add(Me.lblCodBarrio)
		Me.Panel2.Controls.Add(Me.txtCodBarrio)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(387, 202)
		Me.Panel2.TabIndex = 11
		'
		'cmdAddDistZona
		'
		Me.cmdAddDistZona.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddDistZona.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddDistZona.Appearance.Options.UseFont = True
		Me.cmdAddDistZona.Appearance.Options.UseForeColor = True
		Me.cmdAddDistZona.Location = New System.Drawing.Point(342, 165)
		Me.cmdAddDistZona.Name = "cmdAddDistZona"
		Me.cmdAddDistZona.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddDistZona.TabIndex = 15
		Me.cmdAddDistZona.Text = "+"
		Me.cmdAddDistZona.ToolTip = "Agregar un nuevo Departamento"
		'
		'luZonaDist
		'
		Me.luZonaDist.Location = New System.Drawing.Point(147, 164)
		Me.luZonaDist.Name = "luZonaDist"
		Me.luZonaDist.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luZonaDist.Size = New System.Drawing.Size(194, 20)
		Me.luZonaDist.TabIndex = 14
		'
		'lblDistZonaAsociada
		'
		Me.lblDistZonaAsociada.Location = New System.Drawing.Point(21, 167)
		Me.lblDistZonaAsociada.Name = "lblDistZonaAsociada"
		Me.lblDistZonaAsociada.Size = New System.Drawing.Size(129, 20)
		Me.lblDistZonaAsociada.TabIndex = 13
		Me.lblDistZonaAsociada.Text = "Ditrito/Zona Asociada:"
		'
		'cmdAddMunicipio
		'
		Me.cmdAddMunicipio.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddMunicipio.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddMunicipio.Appearance.Options.UseFont = True
		Me.cmdAddMunicipio.Appearance.Options.UseForeColor = True
		Me.cmdAddMunicipio.Location = New System.Drawing.Point(342, 130)
		Me.cmdAddMunicipio.Name = "cmdAddMunicipio"
		Me.cmdAddMunicipio.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddMunicipio.TabIndex = 12
		Me.cmdAddMunicipio.Text = "+"
		Me.cmdAddMunicipio.ToolTip = "Agregar un nuevo Departamento"
		'
		'luMunic
		'
		Me.luMunic.Location = New System.Drawing.Point(147, 129)
		Me.luMunic.Name = "luMunic"
		Me.luMunic.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luMunic.Size = New System.Drawing.Size(194, 20)
		Me.luMunic.TabIndex = 11
		'
		'lblMunicAsociado
		'
		Me.lblMunicAsociado.Location = New System.Drawing.Point(21, 132)
		Me.lblMunicAsociado.Name = "lblMunicAsociado"
		Me.lblMunicAsociado.Size = New System.Drawing.Size(129, 20)
		Me.lblMunicAsociado.TabIndex = 10
		Me.lblMunicAsociado.Text = "Municipio Asociado:"
		'
		'cmdAddDeparPais
		'
		Me.cmdAddDeparPais.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdAddDeparPais.Appearance.ForeColor = System.Drawing.Color.Blue
		Me.cmdAddDeparPais.Appearance.Options.UseFont = True
		Me.cmdAddDeparPais.Appearance.Options.UseForeColor = True
		Me.cmdAddDeparPais.Location = New System.Drawing.Point(342, 95)
		Me.cmdAddDeparPais.Name = "cmdAddDeparPais"
		Me.cmdAddDeparPais.Size = New System.Drawing.Size(20, 19)
		Me.cmdAddDeparPais.TabIndex = 9
		Me.cmdAddDeparPais.Text = "+"
		Me.cmdAddDeparPais.ToolTip = "Agregar un nuevo Departamento"
		'
		'luDepartPais
		'
		Me.luDepartPais.Location = New System.Drawing.Point(147, 94)
		Me.luDepartPais.Name = "luDepartPais"
		Me.luDepartPais.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luDepartPais.Size = New System.Drawing.Size(194, 20)
		Me.luDepartPais.TabIndex = 7
		'
		'lblDepartPaisAsoc
		'
		Me.lblDepartPaisAsoc.Location = New System.Drawing.Point(21, 97)
		Me.lblDepartPaisAsoc.Name = "lblDepartPaisAsoc"
		Me.lblDepartPaisAsoc.Size = New System.Drawing.Size(129, 20)
		Me.lblDepartPaisAsoc.TabIndex = 5
		Me.lblDepartPaisAsoc.Text = "Departamento Asociado:"
		'
		'lblNomBarrio
		'
		Me.lblNomBarrio.Location = New System.Drawing.Point(21, 60)
		Me.lblNomBarrio.Name = "lblNomBarrio"
		Me.lblNomBarrio.Size = New System.Drawing.Size(129, 20)
		Me.lblNomBarrio.TabIndex = 3
		Me.lblNomBarrio.Text = "Nombre de Barrio:"
		'
		'txtNomBarrio
		'
		Me.txtNomBarrio.Location = New System.Drawing.Point(147, 57)
		Me.txtNomBarrio.Name = "txtNomBarrio"
		Me.txtNomBarrio.Size = New System.Drawing.Size(200, 20)
		Me.txtNomBarrio.TabIndex = 2
		'
		'lblCodBarrio
		'
		Me.lblCodBarrio.Location = New System.Drawing.Point(21, 24)
		Me.lblCodBarrio.Name = "lblCodBarrio"
		Me.lblCodBarrio.Size = New System.Drawing.Size(117, 20)
		Me.lblCodBarrio.TabIndex = 1
		Me.lblCodBarrio.Text = "Id Barrio:"
		'
		'txtCodBarrio
		'
		Me.txtCodBarrio.Location = New System.Drawing.Point(147, 21)
		Me.txtCodBarrio.Name = "txtCodBarrio"
		Me.txtCodBarrio.Size = New System.Drawing.Size(88, 20)
		Me.txtCodBarrio.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 202)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(387, 49)
		Me.Panel1.TabIndex = 10
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(292, 8)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(208, 8)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'frmEditBarrios
		'
		Me.AcceptButton = Me.cmdAceptar
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.CancelButton = Me.cmdCancelar
		Me.ClientSize = New System.Drawing.Size(387, 251)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmEditBarrios"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Edicion de Barrios"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.luZonaDist.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luMunic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.luDepartPais.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		CType(Me.TblDepartPaisBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblMunicipioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TblZonaDistBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents cmdAddMunicipio As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents luMunic As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblMunicAsociado As System.Windows.Forms.Label
	Friend WithEvents cmdAddDeparPais As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents luDepartPais As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblDepartPaisAsoc As System.Windows.Forms.Label
	Friend WithEvents lblNomBarrio As System.Windows.Forms.Label
	Friend WithEvents txtNomBarrio As System.Windows.Forms.TextBox
	Friend WithEvents lblCodBarrio As System.Windows.Forms.Label
	Friend WithEvents txtCodBarrio As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents cmdCancelar As System.Windows.Forms.Button
	Friend WithEvents cmdAceptar As System.Windows.Forms.Button
	Friend WithEvents cmdAddDistZona As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents luZonaDist As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents lblDistZonaAsociada As System.Windows.Forms.Label
	Friend WithEvents TblDepartPaisBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblMunicipioBindingSource As System.Windows.Forms.BindingSource
	Friend WithEvents TblZonaDistBindingSource As System.Windows.Forms.BindingSource
End Class
