﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmColaboradores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmColaboradores))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.luDependencia = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblPlanillasBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCodColab = New System.Windows.Forms.TextBox()
        Me.luPlanillaCalculo = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblCargosBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.luArea = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblAreasBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.luProfesion = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblProfesionesBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.browseButton = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txtSalarioMensUS = New System.Windows.Forms.TextBox()
        Me.ckIsDolarizado = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTelefonoPers2 = New System.Windows.Forms.TextBox()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtApellidoColab = New System.Windows.Forms.TextBox()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNomColab = New System.Windows.Forms.TextBox()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTelefonoPers1 = New System.Windows.Forms.TextBox()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNumINSS = New System.Windows.Forms.TextBox()
        Me.txtNumID = New System.Windows.Forms.TextBox()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaBaja = New DevExpress.XtraEditors.DateEdit()
        Me.deFechaIng = New DevExpress.XtraEditors.DateEdit()
        Me.lblFechaBaja = New System.Windows.Forms.Label()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.txtTelefonoDom = New System.Windows.Forms.TextBox()
        Me.txtSalarioMensCS = New System.Windows.Forms.TextBox()
        Me.lblSalarioMens = New System.Windows.Forms.Label()
        Me.lblNomColaborador = New System.Windows.Forms.Label()
        Me.lblCodColaborador = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.TblDependenciasBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.luCargo = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Panel2.SuspendLayout()
        CType(Me.luDependencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblPlanillasBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luPlanillaCalculo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCargosBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblAreasBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luProfesion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblProfesionesBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsDolarizado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaBaja.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIng.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.TblDependenciasBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luCargo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.luCargo)
        Me.Panel2.Controls.Add(Me.luDependencia)
        Me.Panel2.Controls.Add(Me.LabelControl12)
        Me.Panel2.Controls.Add(Me.txtCodColab)
        Me.Panel2.Controls.Add(Me.luPlanillaCalculo)
        Me.Panel2.Controls.Add(Me.luArea)
        Me.Panel2.Controls.Add(Me.luProfesion)
        Me.Panel2.Controls.Add(Me.browseButton)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.LabelControl11)
        Me.Panel2.Controls.Add(Me.LabelControl10)
        Me.Panel2.Controls.Add(Me.txtSalarioMensUS)
        Me.Panel2.Controls.Add(Me.ckIsDolarizado)
        Me.Panel2.Controls.Add(Me.LabelControl6)
        Me.Panel2.Controls.Add(Me.LabelControl5)
        Me.Panel2.Controls.Add(Me.txtTelefonoPers2)
        Me.Panel2.Controls.Add(Me.LabelControl1)
        Me.Panel2.Controls.Add(Me.txtApellidoColab)
        Me.Panel2.Controls.Add(Me.LabelControl4)
        Me.Panel2.Controls.Add(Me.txtNomColab)
        Me.Panel2.Controls.Add(Me.txtDireccion)
        Me.Panel2.Controls.Add(Me.LabelControl9)
        Me.Panel2.Controls.Add(Me.txtTelefonoPers1)
        Me.Panel2.Controls.Add(Me.LabelControl7)
        Me.Panel2.Controls.Add(Me.LabelControl8)
        Me.Panel2.Controls.Add(Me.txtNumINSS)
        Me.Panel2.Controls.Add(Me.txtNumID)
        Me.Panel2.Controls.Add(Me.LabelControl3)
        Me.Panel2.Controls.Add(Me.LabelControl2)
        Me.Panel2.Controls.Add(Me.deFechaBaja)
        Me.Panel2.Controls.Add(Me.deFechaIng)
        Me.Panel2.Controls.Add(Me.lblFechaBaja)
        Me.Panel2.Controls.Add(Me.lblFechaIngreso)
        Me.Panel2.Controls.Add(Me.ckIsActivo)
        Me.Panel2.Controls.Add(Me.txtTelefonoDom)
        Me.Panel2.Controls.Add(Me.txtSalarioMensCS)
        Me.Panel2.Controls.Add(Me.lblSalarioMens)
        Me.Panel2.Controls.Add(Me.lblNomColaborador)
        Me.Panel2.Controls.Add(Me.lblCodColaborador)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(865, 363)
        Me.Panel2.TabIndex = 0
        '
        'luDependencia
        '
        Me.luDependencia.Location = New System.Drawing.Point(149, 289)
        Me.luDependencia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.luDependencia.Name = "luDependencia"
        Me.luDependencia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luDependencia.Properties.DataSource = Me.TblPlanillasBS
        Me.luDependencia.Size = New System.Drawing.Size(301, 23)
        Me.luDependencia.TabIndex = 56
        Me.luDependencia.Visible = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(17, 293)
        Me.LabelControl12.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(78, 16)
        Me.LabelControl12.TabIndex = 57
        Me.LabelControl12.Text = "Dependencia:"
        Me.LabelControl12.Visible = False
        '
        'txtCodColab
        '
        Me.txtCodColab.Enabled = False
        Me.txtCodColab.Location = New System.Drawing.Point(149, 26)
        Me.txtCodColab.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCodColab.Name = "txtCodColab"
        Me.txtCodColab.Size = New System.Drawing.Size(136, 22)
        Me.txtCodColab.TabIndex = 0
        Me.txtCodColab.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'luPlanillaCalculo
        '
        Me.luPlanillaCalculo.Location = New System.Drawing.Point(149, 257)
        Me.luPlanillaCalculo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.luPlanillaCalculo.Name = "luPlanillaCalculo"
        Me.luPlanillaCalculo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luPlanillaCalculo.Properties.DataSource = Me.TblPlanillasBS
        Me.luPlanillaCalculo.Size = New System.Drawing.Size(301, 23)
        Me.luPlanillaCalculo.TabIndex = 12
        '
        'luArea
        '
        Me.luArea.Location = New System.Drawing.Point(149, 191)
        Me.luArea.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.luArea.Name = "luArea"
        Me.luArea.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luArea.Properties.DataSource = Me.TblAreasBS
        Me.luArea.Size = New System.Drawing.Size(301, 23)
        Me.luArea.TabIndex = 10
        '
        'luProfesion
        '
        Me.luProfesion.Location = New System.Drawing.Point(149, 155)
        Me.luProfesion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.luProfesion.Name = "luProfesion"
        Me.luProfesion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luProfesion.Properties.DataSource = Me.TblProfesionesBS
        Me.luProfesion.Size = New System.Drawing.Size(301, 23)
        Me.luProfesion.TabIndex = 9
        '
        'browseButton
        '
        Me.browseButton.Enabled = False
        Me.browseButton.Location = New System.Drawing.Point(473, 318)
        Me.browseButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.browseButton.Name = "browseButton"
        Me.browseButton.Size = New System.Drawing.Size(128, 28)
        Me.browseButton.TabIndex = 18
        Me.browseButton.Text = "Cargar Foto..."
        Me.browseButton.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(473, 155)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(127, 155)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 55
        Me.PictureBox1.TabStop = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(17, 160)
        Me.LabelControl11.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(110, 16)
        Me.LabelControl11.TabIndex = 53
        Me.LabelControl11.Text = "Profesión/Estudios:"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(17, 261)
        Me.LabelControl10.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(120, 16)
        Me.LabelControl10.TabIndex = 51
        Me.LabelControl10.Text = "Planilla para Calculo:"
        '
        'txtSalarioMensUS
        '
        Me.txtSalarioMensUS.Enabled = False
        Me.txtSalarioMensUS.Location = New System.Drawing.Point(752, 192)
        Me.txtSalarioMensUS.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtSalarioMensUS.Name = "txtSalarioMensUS"
        Me.txtSalarioMensUS.Size = New System.Drawing.Size(93, 22)
        Me.txtSalarioMensUS.TabIndex = 14
        Me.txtSalarioMensUS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ckIsDolarizado
        '
        Me.ckIsDolarizado.Enabled = False
        Me.ckIsDolarizado.Location = New System.Drawing.Point(619, 193)
        Me.ckIsDolarizado.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ckIsDolarizado.Name = "ckIsDolarizado"
        Me.ckIsDolarizado.Properties.Caption = "Es Dolarizado:"
        Me.ckIsDolarizado.Size = New System.Drawing.Size(145, 20)
        Me.ckIsDolarizado.TabIndex = 49
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(16, 194)
        Me.LabelControl6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(95, 16)
        Me.LabelControl6.TabIndex = 47
        Me.LabelControl6.Text = "Área de trabajo:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(16, 228)
        Me.LabelControl5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(102, 16)
        Me.LabelControl5.TabIndex = 45
        Me.LabelControl5.Text = "Cargo que ocupa:"
        '
        'txtTelefonoPers2
        '
        Me.txtTelefonoPers2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonoPers2.Location = New System.Drawing.Point(764, 87)
        Me.txtTelefonoPers2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTelefonoPers2.MaxLength = 8
        Me.txtTelefonoPers2.Name = "txtTelefonoPers2"
        Me.txtTelefonoPers2.Size = New System.Drawing.Size(81, 22)
        Me.txtTelefonoPers2.TabIndex = 8
        Me.txtTelefonoPers2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(621, 91)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(119, 16)
        Me.LabelControl1.TabIndex = 43
        Me.LabelControl1.Text = "Telefono Personal 2:"
        '
        'txtApellidoColab
        '
        Me.txtApellidoColab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtApellidoColab.Location = New System.Drawing.Point(377, 55)
        Me.txtApellidoColab.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtApellidoColab.MaxLength = 50
        Me.txtApellidoColab.Name = "txtApellidoColab"
        Me.txtApellidoColab.Size = New System.Drawing.Size(223, 22)
        Me.txtApellidoColab.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(309, 58)
        Me.LabelControl4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl4.TabIndex = 41
        Me.LabelControl4.Text = "Apellidos:"
        '
        'txtNomColab
        '
        Me.txtNomColab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomColab.Location = New System.Drawing.Point(377, 26)
        Me.txtNomColab.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtNomColab.MaxLength = 50
        Me.txtNomColab.Name = "txtNomColab"
        Me.txtNomColab.Size = New System.Drawing.Size(223, 22)
        Me.txtNomColab.TabIndex = 3
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Location = New System.Drawing.Point(149, 119)
        Me.txtDireccion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDireccion.MaxLength = 100
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(696, 22)
        Me.txtDireccion.TabIndex = 5
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(17, 123)
        Me.LabelControl9.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(116, 16)
        Me.LabelControl9.TabIndex = 39
        Me.LabelControl9.Text = "Dirección domiciliar:"
        '
        'txtTelefonoPers1
        '
        Me.txtTelefonoPers1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonoPers1.Location = New System.Drawing.Point(764, 54)
        Me.txtTelefonoPers1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTelefonoPers1.MaxLength = 8
        Me.txtTelefonoPers1.Name = "txtTelefonoPers1"
        Me.txtTelefonoPers1.Size = New System.Drawing.Size(81, 22)
        Me.txtTelefonoPers1.TabIndex = 7
        Me.txtTelefonoPers1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(621, 26)
        Me.LabelControl7.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(115, 16)
        Me.LabelControl7.TabIndex = 35
        Me.LabelControl7.Text = "Telefono Domiciliar:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(621, 59)
        Me.LabelControl8.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(119, 16)
        Me.LabelControl8.TabIndex = 36
        Me.LabelControl8.Text = "Telefono Personal 1:"
        '
        'txtNumINSS
        '
        Me.txtNumINSS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumINSS.Location = New System.Drawing.Point(149, 87)
        Me.txtNumINSS.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtNumINSS.MaxLength = 10
        Me.txtNumINSS.Name = "txtNumINSS"
        Me.txtNumINSS.Size = New System.Drawing.Size(136, 22)
        Me.txtNumINSS.TabIndex = 2
        Me.txtNumINSS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNumID
        '
        Me.txtNumID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumID.Location = New System.Drawing.Point(149, 55)
        Me.txtNumID.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtNumID.MaxLength = 16
        Me.txtNumID.Name = "txtNumID"
        Me.txtNumID.Size = New System.Drawing.Size(136, 22)
        Me.txtNumID.TabIndex = 1
        Me.txtNumID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(17, 91)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(118, 16)
        Me.LabelControl3.TabIndex = 32
        Me.LabelControl3.Text = "Num. Seguro Social:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(17, 59)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(114, 16)
        Me.LabelControl2.TabIndex = 31
        Me.LabelControl2.Text = "Num. Identificacion:"
        '
        'deFechaBaja
        '
        Me.deFechaBaja.EditValue = Nothing
        Me.deFechaBaja.Location = New System.Drawing.Point(727, 320)
        Me.deFechaBaja.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.deFechaBaja.Name = "deFechaBaja"
        Me.deFechaBaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaBaja.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaBaja.Size = New System.Drawing.Size(120, 23)
        Me.deFechaBaja.TabIndex = 17
        '
        'deFechaIng
        '
        Me.deFechaIng.EditValue = Nothing
        Me.deFechaIng.Location = New System.Drawing.Point(727, 274)
        Me.deFechaIng.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.deFechaIng.Name = "deFechaIng"
        Me.deFechaIng.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaIng.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaIng.Size = New System.Drawing.Size(120, 23)
        Me.deFechaIng.TabIndex = 16
        '
        'lblFechaBaja
        '
        Me.lblFechaBaja.Location = New System.Drawing.Point(617, 324)
        Me.lblFechaBaja.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaBaja.Name = "lblFechaBaja"
        Me.lblFechaBaja.Size = New System.Drawing.Size(95, 21)
        Me.lblFechaBaja.TabIndex = 30
        Me.lblFechaBaja.Text = "Fecha Baja:"
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(617, 278)
        Me.lblFechaIngreso.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(109, 21)
        Me.lblFechaIngreso.TabIndex = 29
        Me.lblFechaIngreso.Text = "Fecha Ingreso:"
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Location = New System.Drawing.Point(619, 231)
        Me.ckIsActivo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Es Activo"
        Me.ckIsActivo.Size = New System.Drawing.Size(100, 20)
        Me.ckIsActivo.TabIndex = 15
        '
        'txtTelefonoDom
        '
        Me.txtTelefonoDom.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonoDom.Location = New System.Drawing.Point(764, 22)
        Me.txtTelefonoDom.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtTelefonoDom.MaxLength = 8
        Me.txtTelefonoDom.Name = "txtTelefonoDom"
        Me.txtTelefonoDom.Size = New System.Drawing.Size(81, 22)
        Me.txtTelefonoDom.TabIndex = 6
        Me.txtTelefonoDom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSalarioMensCS
        '
        Me.txtSalarioMensCS.Location = New System.Drawing.Point(752, 151)
        Me.txtSalarioMensCS.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtSalarioMensCS.MaxLength = 10
        Me.txtSalarioMensCS.Name = "txtSalarioMensCS"
        Me.txtSalarioMensCS.Size = New System.Drawing.Size(93, 22)
        Me.txtSalarioMensCS.TabIndex = 13
        Me.txtSalarioMensCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSalarioMens
        '
        Me.lblSalarioMens.Location = New System.Drawing.Point(617, 155)
        Me.lblSalarioMens.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSalarioMens.Name = "lblSalarioMens"
        Me.lblSalarioMens.Size = New System.Drawing.Size(147, 25)
        Me.lblSalarioMens.TabIndex = 5
        Me.lblSalarioMens.Text = "Salario Mensual C$:"
        '
        'lblNomColaborador
        '
        Me.lblNomColaborador.Location = New System.Drawing.Point(304, 30)
        Me.lblNomColaborador.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNomColaborador.Name = "lblNomColaborador"
        Me.lblNomColaborador.Size = New System.Drawing.Size(77, 25)
        Me.lblNomColaborador.TabIndex = 3
        Me.lblNomColaborador.Text = "Nombres:"
        '
        'lblCodColaborador
        '
        Me.lblCodColaborador.Location = New System.Drawing.Point(13, 30)
        Me.lblCodColaborador.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCodColaborador.Name = "lblCodColaborador"
        Me.lblCodColaborador.Size = New System.Drawing.Size(147, 25)
        Me.lblCodColaborador.TabIndex = 1
        Me.lblCodColaborador.Text = "Codigo Colaborador:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 363)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(865, 60)
        Me.Panel1.TabIndex = 1
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(752, 10)
        Me.cmdCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(95, 43)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(648, 10)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(97, 43)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'luCargo
        '
        Me.luCargo.Location = New System.Drawing.Point(149, 221)
        Me.luCargo.Name = "luCargo"
        Me.luCargo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luCargo.Properties.View = Me.GridLookUpEdit1View
        Me.luCargo.Size = New System.Drawing.Size(301, 23)
        Me.luCargo.TabIndex = 58
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'frmColaboradores
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(865, 423)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmColaboradores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Expediente de Colaboradores"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.luDependencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblPlanillasBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luPlanillaCalculo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCargosBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblAreasBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luProfesion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblProfesionesBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsDolarizado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaBaja.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIng.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.TblDependenciasBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luCargo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents deFechaBaja As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFechaIng As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaBaja As System.Windows.Forms.Label
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtTelefonoDom As System.Windows.Forms.TextBox
    Friend WithEvents txtSalarioMensCS As System.Windows.Forms.TextBox
    Friend WithEvents lblSalarioMens As System.Windows.Forms.Label
    Friend WithEvents lblNomColaborador As System.Windows.Forms.Label
    Friend WithEvents txtNomColab As System.Windows.Forms.TextBox
    Friend WithEvents lblCodColaborador As System.Windows.Forms.Label
    Friend WithEvents txtCodColab As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents txtNumINSS As System.Windows.Forms.TextBox
    Friend WithEvents txtNumID As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTelefonoPers1 As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtApellidoColab As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTelefonoPers2 As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtSalarioMensUS As System.Windows.Forms.TextBox
    Friend WithEvents ckIsDolarizado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents browseButton As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TblProfesionesBS As System.Windows.Forms.BindingSource
    Friend WithEvents TblAreasBS As System.Windows.Forms.BindingSource
    Friend WithEvents TblCargosBS As System.Windows.Forms.BindingSource
    Friend WithEvents TblPlanillasBS As System.Windows.Forms.BindingSource
    Friend WithEvents luPlanillaCalculo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luArea As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luProfesion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luDependencia As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TblDependenciasBS As System.Windows.Forms.BindingSource
    Friend WithEvents luCargo As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
End Class
