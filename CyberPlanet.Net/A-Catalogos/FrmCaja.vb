﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class FrmCaja
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Private Sub FrmCaja_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
        Cargar_Sucursal()
    End Sub
    Sub Cargar()
        Dim dt As DataTable = New DataTable()
        dgvCaja.Rows.Clear()
        sqlstring = "select Codigo_Caja as 'Codigo', Descripcion, ISNULL(Configuracion.Sucursales.Nombre,'-'), ISNULL(Cuenta_Contable,'-') as 'Cuenta', Cajas.Activo from Cajas left join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Cajas.Id_Sucursal"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvCaja.Rows.Add(row0(0), row0(1), row0(2), row0(3), row0(4))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Cargar_Sucursal()
        Dim dt As DataTable = New DataTable()
        cbSucursal.Properties.Items.Clear()
        sqlstring = "select Nombre from Configuracion.Sucursales where Activo=1"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbSucursal.Properties.Items.Add(row0(0))
            Next
            cbSucursal.SelectedIndex = 0
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub dgvCaja_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCaja.CellMouseClick
        With dgvCaja
            txtCodigo.Text = .Rows(.CurrentRow.Index).Cells(0).Value
            txtDescripcion.Text = .Rows(.CurrentRow.Index).Cells(1).Value
            cbSucursal.SelectedItem = .Rows(.CurrentRow.Index).Cells(2).Value
            txtCuenta.Text = .Rows(.CurrentRow.Index).Cells(3).Value
            ckbActivo.Checked = .Rows(.CurrentRow.Index).Cells(4).Value
        End With
    End Sub
    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If btnEditar.Text = "&Editar" Then
            If dgvCaja.SelectedRows.Count > 0 Then
                gbDato.Enabled = True
                txtDescripcion.Focus()
                btnNuevo.Enabled = False
                btnEditar.Text = "&Aceptar"
                btnCancelar.Text = "&Cancelar"
            Else
                MsgBox("No ha seleccionado registro")
            End If
        Else
            sqlstring = "update Cajas set [Descripcion]=@descripcion, [Activo]=@activo, [Id_Sucursal]=(select Key_Sucursal from Configuracion.Sucursales where Nombre=@sucursal),[Cuenta_Contable]=@cuenta where Codigo_Caja=@caja"
            Try
                con.Open()
                cmd = New SqlCommand(sqlstring, con)
                With cmd.Parameters
                    .AddWithValue("@descripcion", txtDescripcion.Text)
                    .AddWithValue("@activo", ckbActivo.Checked)
                    .AddWithValue("@sucursal", cbSucursal.Text)
                    .AddWithValue("@cuenta", txtCuenta.Text)
                    .AddWithValue("@caja", txtCodigo.Text)
                End With
                cmd.ExecuteNonQuery()
                con.Close()
                MsgBox("Registro Actualizado")
                clear()
            Catch ex As Exception
                MsgBox("Error: " + ex.Message)
                con.Close()
            End Try
        End If
    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        If btnNuevo.Text = "&Aceptar" Then
            sqlstring = "insert into Cajas (Codigo_Caja, Descripcion,Cuenta_Contable, Activo, Id_Sucursal) values (@caja, @descripcion, @cuenta, @activo, (select Key_Sucursal from Configuracion.Sucursales where Nombre=@sucursal))"
            Try
                con.Open()
                cmd = New SqlCommand(sqlstring, con)
                With cmd.Parameters
                    .AddWithValue("@descripcion", txtDescripcion.Text)
                    .AddWithValue("@activo", ckbActivo.Checked)
                    .AddWithValue("@sucursal", cbSucursal.Text)
                    .AddWithValue("@cuenta", txtCuenta.Text)
                    .AddWithValue("@caja", txtCodigo.Text)
                End With
                cmd.ExecuteNonQuery()
                con.Close()
                MsgBox("Registro Guardado")
                clear()
            Catch ex As Exception
                MsgBox("Error: " + ex.Message)
                con.Close()
            End Try
        Else
            clear()
            gbDato.Enabled = True
            txtCodigo.Text = dgvCaja.Rows(dgvCaja.Rows.Count - 1).Cells(0).Value + 1
            txtDescripcion.Focus()
            btnNuevo.Text = "&Aceptar"
            btnEditar.Text = "&Cancelar"
        End If
    End Sub
    Sub clear()
        Cargar()
        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        ckbActivo.Checked = True
        cbSucursal.SelectedIndex = 0
        txtCuenta.Text = ""
        btnEditar.Text = "&Editar"
        btnNuevo.Text = "&Agregar"
        gbDato.Enabled = False
        btnNuevo.Enabled = True
        dgvCaja.ClearSelection()
        btnCancelar.Text = "&Salir"
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If btnCancelar.Text = "&Salir" Then
            Me.Close()
            Me.Dispose()
        Else
            clear()
        End If
    End Sub

    Private Sub txtCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCuenta.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub
End Class