﻿Public Class frmUnidMed

    Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
        Dim strsql As String = ""

        strsql = "SELECT Codigo_UnidadMed,Nombre_UnidadMed FROM Unidades_Medida WHERE Codigo_UnidadMed='" & CodigoEntidad & "'"
        Dim tblDatosUnidMed As DataTable = SQL(strsql, "tblDatosUnidMed", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosUnidMed.Rows.Count <> 0 Then
            txtCodUnidMed.Text = tblDatosUnidMed.Rows(0).Item(0)
            txtNomUnidMed.Text = tblDatosUnidMed.Rows(0).Item(1)
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub frmUnidMed_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodUnidMed.Visible = True
            txtCodUnidMed.Visible = True
            txtCodUnidMed.Text = RegistroMaximo()
            txtNomUnidMed.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodUnidMed.Visible = True
            txtCodUnidMed.Enabled = False
            txtCodUnidMed.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoUnidMed As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoUnidMed.EditarUnidMed(CInt(IIf(txtCodUnidMed.Text = "", 0, txtCodUnidMed.Text)), txtNomUnidMed.Text.ToUpper, nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

End Class