﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditProdServ
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditProdServ))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.luPresenta = New System.Windows.Forms.ComboBox()
        Me.txtCodProd = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbUnidMedi = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblCodProdServ = New System.Windows.Forms.Label()
        Me.cmdAddPresent = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbModelos = New System.Windows.Forms.ComboBox()
        Me.cmbTipArticulo = New System.Windows.Forms.ComboBox()
        Me.txtNomProd = New System.Windows.Forms.TextBox()
        Me.cmdAddUnidMed = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbMarcas = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblNomProdServ = New System.Windows.Forms.Label()
        Me.cmbCatego = New System.Windows.Forms.ComboBox()
        Me.txtCodAlterno = New System.Windows.Forms.TextBox()
        Me.lblCostoProm = New System.Windows.Forms.Label()
        Me.txtExistMin = New System.Windows.Forms.TextBox()
        Me.cmbLineas = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblMarca = New System.Windows.Forms.Label()
        Me.cmbProve = New System.Windows.Forms.ComboBox()
        Me.txtNomGeneric = New System.Windows.Forms.TextBox()
        Me.lblModelo = New System.Windows.Forms.Label()
        Me.txtExistMax = New System.Windows.Forms.TextBox()
        Me.ckIsFactura = New DevExpress.XtraEditors.CheckEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.ckIsDescontinuado = New DevExpress.XtraEditors.CheckEdit()
        Me.CmbTipoProducto = New System.Windows.Forms.ComboBox()
        Me.cmdAddDepar = New DevExpress.XtraEditors.SimpleButton()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.ckIsExcento = New DevExpress.XtraEditors.CheckEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdAddMarca = New DevExpress.XtraEditors.SimpleButton()
        Me.lblFechaBaja = New System.Windows.Forms.Label()
        Me.lblLinea = New System.Windows.Forms.Label()
        Me.txtUnidperPakage = New System.Windows.Forms.TextBox()
        Me.deFechaIng = New DevExpress.XtraEditors.DateEdit()
        Me.cmdAddModelo = New DevExpress.XtraEditors.SimpleButton()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.deFechaBaja = New DevExpress.XtraEditors.DateEdit()
        Me.lblCateg = New System.Windows.Forms.Label()
        Me.ckIsPaquetes = New DevExpress.XtraEditors.CheckEdit()
        Me.txtCostoUS = New System.Windows.Forms.TextBox()
        Me.cmdAddCateg = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdAddProv = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCostoCS = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TblProveedorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WorkSystemDataSet = New SIGMA.WorkSystemDataSet()
        Me.Tbl_ProveedorTableAdapter = New SIGMA.WorkSystemDataSetTableAdapters.Tbl_ProveedorTableAdapter()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.TblBodegaAsocBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblProvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblLineaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblCategBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMarcaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblModeloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblBodegaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblUnidMedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblPresentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2.SuspendLayout()
        CType(Me.ckIsFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsDescontinuado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsExcento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsPaquetes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblProveedorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WorkSystemDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBodegaAsocBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLineaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCategBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMarcaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblModeloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBodegaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblUnidMedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblPresentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdCancelar)
        Me.Panel2.Controls.Add(Me.btnAceptar)
        Me.Panel2.Controls.Add(Me.luPresenta)
        Me.Panel2.Controls.Add(Me.txtCodProd)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.cmbUnidMedi)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.lblCodProdServ)
        Me.Panel2.Controls.Add(Me.cmdAddPresent)
        Me.Panel2.Controls.Add(Me.cmbModelos)
        Me.Panel2.Controls.Add(Me.cmbTipArticulo)
        Me.Panel2.Controls.Add(Me.txtNomProd)
        Me.Panel2.Controls.Add(Me.cmdAddUnidMed)
        Me.Panel2.Controls.Add(Me.cmbMarcas)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.lblNomProdServ)
        Me.Panel2.Controls.Add(Me.cmbCatego)
        Me.Panel2.Controls.Add(Me.txtCodAlterno)
        Me.Panel2.Controls.Add(Me.lblCostoProm)
        Me.Panel2.Controls.Add(Me.txtExistMin)
        Me.Panel2.Controls.Add(Me.cmbLineas)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.lblMarca)
        Me.Panel2.Controls.Add(Me.cmbProve)
        Me.Panel2.Controls.Add(Me.txtNomGeneric)
        Me.Panel2.Controls.Add(Me.lblModelo)
        Me.Panel2.Controls.Add(Me.txtExistMax)
        Me.Panel2.Controls.Add(Me.ckIsFactura)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.lblProveedor)
        Me.Panel2.Controls.Add(Me.ckIsDescontinuado)
        Me.Panel2.Controls.Add(Me.CmbTipoProducto)
        Me.Panel2.Controls.Add(Me.cmdAddDepar)
        Me.Panel2.Controls.Add(Me.lblFechaIngreso)
        Me.Panel2.Controls.Add(Me.ckIsExcento)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.cmdAddMarca)
        Me.Panel2.Controls.Add(Me.lblFechaBaja)
        Me.Panel2.Controls.Add(Me.lblLinea)
        Me.Panel2.Controls.Add(Me.txtUnidperPakage)
        Me.Panel2.Controls.Add(Me.deFechaIng)
        Me.Panel2.Controls.Add(Me.cmdAddModelo)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.deFechaBaja)
        Me.Panel2.Controls.Add(Me.lblCateg)
        Me.Panel2.Controls.Add(Me.ckIsPaquetes)
        Me.Panel2.Controls.Add(Me.txtCostoUS)
        Me.Panel2.Controls.Add(Me.cmdAddCateg)
        Me.Panel2.Controls.Add(Me.cmdAddProv)
        Me.Panel2.Controls.Add(Me.txtCostoCS)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(694, 316)
        Me.Panel2.TabIndex = 0
        Me.Panel2.Tag = "999999"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(610, 267)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cerrar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAceptar.Location = New System.Drawing.Point(526, 267)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(78, 35)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "&Agregar"
        Me.btnAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'luPresenta
        '
        Me.luPresenta.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.luPresenta.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.luPresenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.luPresenta.FormattingEnabled = True
        Me.luPresenta.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.luPresenta.Location = New System.Drawing.Point(438, 162)
        Me.luPresenta.Name = "luPresenta"
        Me.luPresenta.Size = New System.Drawing.Size(214, 21)
        Me.luPresenta.TabIndex = 68
        '
        'txtCodProd
        '
        Me.txtCodProd.BackColor = System.Drawing.Color.White
        Me.txtCodProd.Location = New System.Drawing.Point(100, 21)
        Me.txtCodProd.Name = "txtCodProd"
        Me.txtCodProd.ReadOnly = True
        Me.txtCodProd.Size = New System.Drawing.Size(119, 20)
        Me.txtCodProd.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(357, 165)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 17)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Presentación:"
        '
        'cmbUnidMedi
        '
        Me.cmbUnidMedi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbUnidMedi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbUnidMedi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnidMedi.FormattingEnabled = True
        Me.cmbUnidMedi.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbUnidMedi.Location = New System.Drawing.Point(438, 136)
        Me.cmbUnidMedi.Name = "cmbUnidMedi"
        Me.cmbUnidMedi.Size = New System.Drawing.Size(214, 21)
        Me.cmbUnidMedi.TabIndex = 67
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(357, 139)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 20)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "Unidad Medida:"
        '
        'lblCodProdServ
        '
        Me.lblCodProdServ.Location = New System.Drawing.Point(18, 24)
        Me.lblCodProdServ.Name = "lblCodProdServ"
        Me.lblCodProdServ.Size = New System.Drawing.Size(83, 20)
        Me.lblCodProdServ.TabIndex = 1
        Me.lblCodProdServ.Text = "Codigo Articulo:"
        '
        'cmdAddPresent
        '
        Me.cmdAddPresent.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddPresent.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddPresent.Appearance.Options.UseFont = True
        Me.cmdAddPresent.Appearance.Options.UseForeColor = True
        Me.cmdAddPresent.Location = New System.Drawing.Point(653, 163)
        Me.cmdAddPresent.Name = "cmdAddPresent"
        Me.cmdAddPresent.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddPresent.TabIndex = 18
        Me.cmdAddPresent.Text = "+"
        Me.cmdAddPresent.ToolTip = "Agregar un nuevo Modelo"
        '
        'cmbModelos
        '
        Me.cmbModelos.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbModelos.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbModelos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModelos.FormattingEnabled = True
        Me.cmbModelos.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbModelos.Location = New System.Drawing.Point(438, 110)
        Me.cmbModelos.Name = "cmbModelos"
        Me.cmbModelos.Size = New System.Drawing.Size(214, 21)
        Me.cmbModelos.TabIndex = 66
        '
        'cmbTipArticulo
        '
        Me.cmbTipArticulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipArticulo.FormattingEnabled = True
        Me.cmbTipArticulo.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbTipArticulo.Location = New System.Drawing.Point(99, 76)
        Me.cmbTipArticulo.Name = "cmbTipArticulo"
        Me.cmbTipArticulo.Size = New System.Drawing.Size(236, 21)
        Me.cmbTipArticulo.TabIndex = 4
        '
        'txtNomProd
        '
        Me.txtNomProd.BackColor = System.Drawing.Color.White
        Me.txtNomProd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomProd.Location = New System.Drawing.Point(337, 21)
        Me.txtNomProd.Name = "txtNomProd"
        Me.txtNomProd.Size = New System.Drawing.Size(339, 20)
        Me.txtNomProd.TabIndex = 1
        '
        'cmdAddUnidMed
        '
        Me.cmdAddUnidMed.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddUnidMed.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddUnidMed.Appearance.Options.UseFont = True
        Me.cmdAddUnidMed.Appearance.Options.UseForeColor = True
        Me.cmdAddUnidMed.Location = New System.Drawing.Point(653, 136)
        Me.cmdAddUnidMed.Name = "cmdAddUnidMed"
        Me.cmdAddUnidMed.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddUnidMed.TabIndex = 16
        Me.cmdAddUnidMed.Text = "+"
        Me.cmdAddUnidMed.ToolTip = "Agregar una nueva Marca"
        '
        'cmbMarcas
        '
        Me.cmbMarcas.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbMarcas.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbMarcas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMarcas.FormattingEnabled = True
        Me.cmbMarcas.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbMarcas.Location = New System.Drawing.Point(99, 188)
        Me.cmbMarcas.Name = "cmbMarcas"
        Me.cmbMarcas.Size = New System.Drawing.Size(214, 21)
        Me.cmbMarcas.TabIndex = 65
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(17, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 20)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Tipo Articulo:"
        '
        'lblNomProdServ
        '
        Me.lblNomProdServ.Location = New System.Drawing.Point(234, 24)
        Me.lblNomProdServ.Name = "lblNomProdServ"
        Me.lblNomProdServ.Size = New System.Drawing.Size(102, 20)
        Me.lblNomProdServ.TabIndex = 3
        Me.lblNomProdServ.Text = "Nombre del Articulo:"
        '
        'cmbCatego
        '
        Me.cmbCatego.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCatego.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCatego.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCatego.FormattingEnabled = True
        Me.cmbCatego.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbCatego.Location = New System.Drawing.Point(99, 162)
        Me.cmbCatego.Name = "cmbCatego"
        Me.cmbCatego.Size = New System.Drawing.Size(214, 21)
        Me.cmbCatego.TabIndex = 64
        '
        'txtCodAlterno
        '
        Me.txtCodAlterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodAlterno.Location = New System.Drawing.Point(100, 47)
        Me.txtCodAlterno.Name = "txtCodAlterno"
        Me.txtCodAlterno.Size = New System.Drawing.Size(119, 20)
        Me.txtCodAlterno.TabIndex = 2
        '
        'lblCostoProm
        '
        Me.lblCostoProm.Location = New System.Drawing.Point(356, 80)
        Me.lblCostoProm.Name = "lblCostoProm"
        Me.lblCostoProm.Size = New System.Drawing.Size(84, 20)
        Me.lblCostoProm.TabIndex = 8
        Me.lblCostoProm.Text = "Costo Promedio:"
        '
        'txtExistMin
        '
        Me.txtExistMin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExistMin.Location = New System.Drawing.Point(254, 239)
        Me.txtExistMin.Name = "txtExistMin"
        Me.txtExistMin.Size = New System.Drawing.Size(59, 20)
        Me.txtExistMin.TabIndex = 19
        '
        'cmbLineas
        '
        Me.cmbLineas.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbLineas.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbLineas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLineas.FormattingEnabled = True
        Me.cmbLineas.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbLineas.Location = New System.Drawing.Point(99, 136)
        Me.cmbLineas.Name = "cmbLineas"
        Me.cmbLineas.Size = New System.Drawing.Size(214, 21)
        Me.cmbLineas.TabIndex = 63
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(233, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 20)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Nombre Generico:"
        '
        'lblMarca
        '
        Me.lblMarca.Location = New System.Drawing.Point(18, 191)
        Me.lblMarca.Name = "lblMarca"
        Me.lblMarca.Size = New System.Drawing.Size(61, 20)
        Me.lblMarca.TabIndex = 17
        Me.lblMarca.Text = "Marca:"
        '
        'cmbProve
        '
        Me.cmbProve.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbProve.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbProve.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProve.FormattingEnabled = True
        Me.cmbProve.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbProve.Location = New System.Drawing.Point(99, 110)
        Me.cmbProve.Name = "cmbProve"
        Me.cmbProve.Size = New System.Drawing.Size(214, 21)
        Me.cmbProve.TabIndex = 62
        '
        'txtNomGeneric
        '
        Me.txtNomGeneric.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNomGeneric.Enabled = False
        Me.txtNomGeneric.Location = New System.Drawing.Point(337, 47)
        Me.txtNomGeneric.Name = "txtNomGeneric"
        Me.txtNomGeneric.Size = New System.Drawing.Size(339, 20)
        Me.txtNomGeneric.TabIndex = 3
        '
        'lblModelo
        '
        Me.lblModelo.Location = New System.Drawing.Point(357, 113)
        Me.lblModelo.Name = "lblModelo"
        Me.lblModelo.Size = New System.Drawing.Size(61, 17)
        Me.lblModelo.TabIndex = 18
        Me.lblModelo.Text = "Modelo:"
        '
        'txtExistMax
        '
        Me.txtExistMax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExistMax.Location = New System.Drawing.Point(254, 263)
        Me.txtExistMax.Name = "txtExistMax"
        Me.txtExistMax.Size = New System.Drawing.Size(59, 20)
        Me.txtExistMax.TabIndex = 20
        '
        'ckIsFactura
        '
        Me.ckIsFactura.Location = New System.Drawing.Point(18, 263)
        Me.ckIsFactura.Name = "ckIsFactura"
        Me.ckIsFactura.Properties.Caption = "Se Factura"
        Me.ckIsFactura.Size = New System.Drawing.Size(103, 18)
        Me.ckIsFactura.TabIndex = 61
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(18, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 20)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Codigo Alterno:"
        '
        'lblProveedor
        '
        Me.lblProveedor.Location = New System.Drawing.Point(18, 113)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(61, 17)
        Me.lblProveedor.TabIndex = 20
        Me.lblProveedor.Text = "Proveedor:"
        '
        'ckIsDescontinuado
        '
        Me.ckIsDescontinuado.Location = New System.Drawing.Point(357, 265)
        Me.ckIsDescontinuado.Name = "ckIsDescontinuado"
        Me.ckIsDescontinuado.Properties.Caption = "Descontinuado"
        Me.ckIsDescontinuado.Size = New System.Drawing.Size(103, 18)
        Me.ckIsDescontinuado.TabIndex = 21
        '
        'CmbTipoProducto
        '
        Me.CmbTipoProducto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbTipoProducto.FormattingEnabled = True
        Me.CmbTipoProducto.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.CmbTipoProducto.Location = New System.Drawing.Point(437, 188)
        Me.CmbTipoProducto.Name = "CmbTipoProducto"
        Me.CmbTipoProducto.Size = New System.Drawing.Size(215, 21)
        Me.CmbTipoProducto.TabIndex = 59
        '
        'cmdAddDepar
        '
        Me.cmdAddDepar.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddDepar.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddDepar.Appearance.Options.UseFont = True
        Me.cmdAddDepar.Appearance.Options.UseForeColor = True
        Me.cmdAddDepar.Location = New System.Drawing.Point(314, 136)
        Me.cmdAddDepar.Name = "cmdAddDepar"
        Me.cmdAddDepar.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddDepar.TabIndex = 8
        Me.cmdAddDepar.Text = "+"
        Me.cmdAddDepar.ToolTip = "Agregar un nuevo Departamento"
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(357, 218)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(61, 17)
        Me.lblFechaIngreso.TabIndex = 22
        Me.lblFechaIngreso.Text = "F. Ingreso:"
        '
        'ckIsExcento
        '
        Me.ckIsExcento.Location = New System.Drawing.Point(18, 239)
        Me.ckIsExcento.Name = "ckIsExcento"
        Me.ckIsExcento.Properties.Caption = "Excento IVA"
        Me.ckIsExcento.Size = New System.Drawing.Size(86, 18)
        Me.ckIsExcento.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(357, 191)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 20)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "Tipo Producto:"
        '
        'cmdAddMarca
        '
        Me.cmdAddMarca.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddMarca.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddMarca.Appearance.Options.UseFont = True
        Me.cmdAddMarca.Appearance.Options.UseForeColor = True
        Me.cmdAddMarca.Location = New System.Drawing.Point(315, 188)
        Me.cmdAddMarca.Name = "cmdAddMarca"
        Me.cmdAddMarca.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddMarca.TabIndex = 12
        Me.cmdAddMarca.Text = "+"
        Me.cmdAddMarca.ToolTip = "Agregar una nueva Marca"
        '
        'lblFechaBaja
        '
        Me.lblFechaBaja.Location = New System.Drawing.Point(357, 242)
        Me.lblFechaBaja.Name = "lblFechaBaja"
        Me.lblFechaBaja.Size = New System.Drawing.Size(47, 17)
        Me.lblFechaBaja.TabIndex = 23
        Me.lblFechaBaja.Text = "F. Baja:"
        '
        'lblLinea
        '
        Me.lblLinea.Location = New System.Drawing.Point(18, 139)
        Me.lblLinea.Name = "lblLinea"
        Me.lblLinea.Size = New System.Drawing.Size(77, 20)
        Me.lblLinea.TabIndex = 10
        Me.lblLinea.Text = "Linea:"
        '
        'txtUnidperPakage
        '
        Me.txtUnidperPakage.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnidperPakage.Enabled = False
        Me.txtUnidperPakage.Location = New System.Drawing.Point(254, 215)
        Me.txtUnidperPakage.Name = "txtUnidperPakage"
        Me.txtUnidperPakage.Size = New System.Drawing.Size(59, 20)
        Me.txtUnidperPakage.TabIndex = 57
        '
        'deFechaIng
        '
        Me.deFechaIng.EditValue = Nothing
        Me.deFechaIng.Location = New System.Drawing.Point(438, 215)
        Me.deFechaIng.Name = "deFechaIng"
        Me.deFechaIng.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaIng.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaIng.Size = New System.Drawing.Size(214, 20)
        Me.deFechaIng.TabIndex = 22
        '
        'cmdAddModelo
        '
        Me.cmdAddModelo.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddModelo.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddModelo.Appearance.Options.UseFont = True
        Me.cmdAddModelo.Appearance.Options.UseForeColor = True
        Me.cmdAddModelo.Location = New System.Drawing.Point(653, 110)
        Me.cmdAddModelo.Name = "cmdAddModelo"
        Me.cmdAddModelo.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddModelo.TabIndex = 14
        Me.cmdAddModelo.Text = "+"
        Me.cmdAddModelo.ToolTip = "Agregar un nuevo Modelo"
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(152, 217)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(110, 20)
        Me.Label11.TabIndex = 58
        Me.Label11.Text = "Unidades/Paquete:"
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(558, 79)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 20)
        Me.Label10.TabIndex = 55
        Me.Label10.Text = "US$:"
        Me.Label10.Visible = False
        '
        'deFechaBaja
        '
        Me.deFechaBaja.EditValue = Nothing
        Me.deFechaBaja.Enabled = False
        Me.deFechaBaja.Location = New System.Drawing.Point(438, 239)
        Me.deFechaBaja.Name = "deFechaBaja"
        Me.deFechaBaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaBaja.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaBaja.Size = New System.Drawing.Size(214, 20)
        Me.deFechaBaja.TabIndex = 28
        '
        'lblCateg
        '
        Me.lblCateg.Location = New System.Drawing.Point(18, 165)
        Me.lblCateg.Name = "lblCateg"
        Me.lblCateg.Size = New System.Drawing.Size(61, 20)
        Me.lblCateg.TabIndex = 11
        Me.lblCateg.Text = "Categoría:"
        '
        'ckIsPaquetes
        '
        Me.ckIsPaquetes.Location = New System.Drawing.Point(18, 215)
        Me.ckIsPaquetes.Name = "ckIsPaquetes"
        Me.ckIsPaquetes.Properties.Caption = "En Paquetes"
        Me.ckIsPaquetes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ckIsPaquetes.Size = New System.Drawing.Size(86, 18)
        Me.ckIsPaquetes.TabIndex = 56
        '
        'txtCostoUS
        '
        Me.txtCostoUS.Location = New System.Drawing.Point(595, 76)
        Me.txtCostoUS.Name = "txtCostoUS"
        Me.txtCostoUS.Size = New System.Drawing.Size(78, 20)
        Me.txtCostoUS.TabIndex = 53
        Me.txtCostoUS.Visible = False
        '
        'cmdAddCateg
        '
        Me.cmdAddCateg.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddCateg.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddCateg.Appearance.Options.UseFont = True
        Me.cmdAddCateg.Appearance.Options.UseForeColor = True
        Me.cmdAddCateg.Location = New System.Drawing.Point(314, 162)
        Me.cmdAddCateg.Name = "cmdAddCateg"
        Me.cmdAddCateg.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddCateg.TabIndex = 10
        Me.cmdAddCateg.Text = "+"
        Me.cmdAddCateg.ToolTip = "Agregar una nueva Categoría"
        '
        'cmdAddProv
        '
        Me.cmdAddProv.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddProv.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddProv.Appearance.Options.UseFont = True
        Me.cmdAddProv.Appearance.Options.UseForeColor = True
        Me.cmdAddProv.Location = New System.Drawing.Point(314, 110)
        Me.cmdAddProv.Name = "cmdAddProv"
        Me.cmdAddProv.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddProv.TabIndex = 6
        Me.cmdAddProv.Text = "+"
        Me.cmdAddProv.ToolTip = "Agregar un nuevo Proveedor"
        '
        'txtCostoCS
        '
        Me.txtCostoCS.Location = New System.Drawing.Point(475, 76)
        Me.txtCostoCS.Name = "txtCostoCS"
        Me.txtCostoCS.Size = New System.Drawing.Size(75, 20)
        Me.txtCostoCS.TabIndex = 23
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(156, 242)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 17)
        Me.Label7.TabIndex = 48
        Me.Label7.Text = "Existencia Minima:"
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(156, 266)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(106, 17)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Existencia Maxima:"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(452, 79)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(24, 20)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "C$:"
        '
        'TblProveedorBindingSource
        '
        Me.TblProveedorBindingSource.DataMember = "Tbl_Proveedor"
        Me.TblProveedorBindingSource.DataSource = Me.WorkSystemDataSet
        '
        'WorkSystemDataSet
        '
        Me.WorkSystemDataSet.DataSetName = "WorkSystemDataSet"
        Me.WorkSystemDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Tbl_ProveedorTableAdapter
        '
        Me.Tbl_ProveedorTableAdapter.ClearBeforeFill = True
        '
        'GridView1
        '
        Me.GridView1.Name = "GridView1"
        '
        'GridView2
        '
        Me.GridView2.Name = "GridView2"
        '
        'frmEditProdServ
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(694, 316)
        Me.Controls.Add(Me.Panel2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditProdServ"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edición de Productos / Servicios"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.ckIsFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsDescontinuado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsExcento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIng.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaBaja.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaBaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsPaquetes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblProveedorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WorkSystemDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBodegaAsocBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLineaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCategBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMarcaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblModeloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBodegaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblUnidMedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblPresentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents TblProvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblLineaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCategBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMarcaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblModeloBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBodegaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBodegaAsocBS As System.Windows.Forms.BindingSource
    Friend WithEvents TblUnidMedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblPresentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WorkSystemDataSet As SIGMA.WorkSystemDataSet
    Friend WithEvents TblProveedorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Tbl_ProveedorTableAdapter As SIGMA.WorkSystemDataSetTableAdapters.Tbl_ProveedorTableAdapter
    Friend WithEvents luPresenta As System.Windows.Forms.ComboBox
    Friend WithEvents txtCodProd As System.Windows.Forms.TextBox
    Friend WithEvents cmbUnidMedi As System.Windows.Forms.ComboBox
    Friend WithEvents lblCodProdServ As System.Windows.Forms.Label
    Friend WithEvents cmbModelos As System.Windows.Forms.ComboBox
    Friend WithEvents txtNomProd As System.Windows.Forms.TextBox
    Friend WithEvents cmbMarcas As System.Windows.Forms.ComboBox
    Friend WithEvents lblNomProdServ As System.Windows.Forms.Label
    Friend WithEvents cmbCatego As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostoProm As System.Windows.Forms.Label
    Friend WithEvents cmbLineas As System.Windows.Forms.ComboBox
    Friend WithEvents lblMarca As System.Windows.Forms.Label
    Friend WithEvents cmbProve As System.Windows.Forms.ComboBox
    Friend WithEvents lblModelo As System.Windows.Forms.Label
    Friend WithEvents ckIsFactura As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents CmbTipoProducto As System.Windows.Forms.ComboBox
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblFechaBaja As System.Windows.Forms.Label
    Friend WithEvents txtUnidperPakage As System.Windows.Forms.TextBox
    Friend WithEvents deFechaIng As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents deFechaBaja As DevExpress.XtraEditors.DateEdit
    Friend WithEvents ckIsPaquetes As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cmdAddCateg As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCostoCS As System.Windows.Forms.TextBox
    Friend WithEvents cmdAddProv As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCostoUS As System.Windows.Forms.TextBox
    Friend WithEvents lblCateg As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmdAddModelo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblLinea As System.Windows.Forms.Label
    Friend WithEvents cmdAddMarca As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ckIsExcento As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cmdAddDepar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ckIsDescontinuado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtExistMax As System.Windows.Forms.TextBox
    Friend WithEvents txtNomGeneric As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtExistMin As System.Windows.Forms.TextBox
    Friend WithEvents txtCodAlterno As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdAddUnidMed As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbTipArticulo As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddPresent As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
End Class
