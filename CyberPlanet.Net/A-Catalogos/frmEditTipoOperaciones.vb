﻿Public Class frmEditTipoOperaciones

	Public Sub CargarDatos(ByVal CodigoEntidad As Integer)
		Dim strsql As String = ""

		strsql = "SELECT Id_TipoOperacion,Nombre_TipoOperacion,EsIngreso as 'Es Ingreso', Codigo_Cuenta FROM Tbl_TipoOperaciones WHERE Id_TipoOperacion='" & CodigoEntidad & "'"
        Dim tblDatosTipoOper As DataTable = SQL(strsql, "tblDatosTipoOper", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosTipoOper.Rows.Count <> 0 Then
            txtCodTipOper.Text = tblDatosTipoOper.Rows(0).Item(0)
            txtNomTipOper.Text = tblDatosTipoOper.Rows(0).Item(1)
            ckIsIngreso.Checked = tblDatosTipoOper.Rows(0).Item(2)
            txtCuentaContable.Text = tblDatosTipoOper.Rows(0).Item(3)
        End If
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmEditTipoOperaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdic = 1 Then
            lblCodTipOper.Visible = True
            txtCodTipOper.Visible = True
            txtCodTipOper.Text = RegistroMaximo()
            txtNomTipOper.Focus()
        ElseIf nTipoEdic = 2 Then
            lblCodTipOper.Visible = True
            txtCodTipOper.Enabled = False
            txtCodTipOper.Visible = True
            CargarDatos(CodigoEntidad)
        ElseIf nTipoEdic = 3 Then
            Panel2.Enabled = False
            CargarDatos(CodigoEntidad)
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim CatalogoDepart As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = CatalogoDepart.EditarTipoOperaciones(CInt(IIf(txtCodTipOper.Text = "", 0, txtCodTipOper.Text)), txtNomTipOper.Text.ToUpper, IIf(ckIsIngreso.Checked = False, 0, 1), IIf(txtCuentaContable.Text = "", 0, txtCuentaContable.Text), nTipoEdic)
            If Resum = "OK" Then
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
            If NumCatalogo_Ant = 0 Then
                frmPrincipal.CargarDatos()
            End If
        End Try
    End Sub

	Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
		Cerrar()
	End Sub

End Class