﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.Tbl_ResumenContaBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.bbiReportTrans = New DevExpress.XtraBars.BarButtonItem()
        Me.Tbl_ResumenTransBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusBar = New System.Windows.Forms.StatusStrip()
        Me.TSSucursal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblSucursal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.LblNombreUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSFecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.LblFecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSHora = New System.Windows.Forms.ToolStripStatusLabel()
        Me.LblHora = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSProceso = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslbProceso = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bbiCompraRecargas = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem29 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem30 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem31 = New DevExpress.XtraBars.BarButtonItem()
        Me.rpContab = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup15 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiSolicitudesCheque = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiOrdenPago = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiGenerarCheques = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiEditComprob = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup10 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiEditCatalogo = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPlantillasCont = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiHacerCierre = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup13 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiRegGastos = New DevExpress.XtraBars.BarButtonItem()
        Me.rpgInformesFinan = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.BarButtonItem16 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem17 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem18 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem19 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem20 = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup27 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.BarButtonItem21 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem22 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem23 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem25 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem24 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem27 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem28 = New DevExpress.XtraBars.BarButtonItem()
        Me.rpRecursosHum = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiExpedEmpleado = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiControlVacaciones = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiLiquidacionesPers = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiGenerarPlanilla = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup20 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiControlHorasExtras = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiComisiones = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiOtrosIngresos = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup21 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiConveniosFinan = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPrestamosPers = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiOtrasDeducciones = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.BarButtonItem26 = New DevExpress.XtraBars.BarButtonItem()
        Me.rpTesoreria = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup25 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiProgramPagos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiOrdenesPago = New DevExpress.XtraBars.BarButtonItem()
        Me.rpCartera = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup22 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiVerMovCuenta = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup23 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiIngresosPagos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiIngresosNotaCred = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiIngresosNotaDebitos = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup24 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiLlamadasyProgram = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRptCartera = New DevExpress.XtraBars.BarButtonItem()
        Me.rpCentroSol = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiFacturacion = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiFverificacion = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiFendose = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiFcliente = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiDevolucion = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiCotizacion = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPedidos = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiReciboManual = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiAplicarAnticipo = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiArqueo = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup16 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiInformes = New DevExpress.XtraBars.BarButtonItem()
        Me.rpInventory = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnRemision = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiEntradas = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiSalidas = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiTransferencia = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiControlKardex = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup19 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiInfoAlmacen = New DevExpress.XtraBars.BarButtonItem()
        Me.rpProducción = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup28 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnRecetasProduccion = New DevExpress.XtraBars.BarButtonItem()
        Me.btnOrdenProd = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem32 = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCOrden = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiKit = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem34 = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup30 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btnGarantia = New DevExpress.XtraBars.BarButtonItem()
        Me.rpAdminLog = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiProyeccion = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPedidosProv = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCompraLocales = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiComprasForaneas = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiDevolProv = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup18 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiControlPedidos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiInformeLogistica = New DevExpress.XtraBars.BarButtonItem()
        Me.rpCatalogos = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup14 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiBodegas = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiMarcas = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiModelos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiDepartamentos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCategorias = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiProveedor = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCliente = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiProductos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiUnidMed = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPresentacion = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiVehiculo = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup11 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiDepart = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiMunic = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiZonDist = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiBarrios = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPageGroup29 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiCaja = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiTasaCambio = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiSuministros = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiActivosFijos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiTipoGastos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCajas = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiTerminales = New DevExpress.XtraBars.BarButtonItem()
        Me.rpPrincipal = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup26 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiActualizar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRefrescar = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiNuevo = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiModificar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiEliminar = New DevExpress.XtraBars.BarButtonItem()
        Me.rpgSaveDocument = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiGuardar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCancelar = New DevExpress.XtraBars.BarButtonItem()
        Me.rpgTaskDocument = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.bbiBuscar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiImprimir = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiExportar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiSesion = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCerrar = New DevExpress.XtraBars.BarButtonItem()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemComboBox2 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemComboBox3 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.bbiLlamadTelef = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRecargaTelef = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiOtrosServicios = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem9 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem10 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem11 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem12 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem14 = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCompraProd = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPrestxCobrar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPrestxPagar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCredxCobrar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCredtxCobrar = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiEstadIngreso = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiLlamadInternac = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonGroup1 = New DevExpress.XtraBars.BarButtonGroup()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem13 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem15 = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiCatalogo = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.bbiUsuarios = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiPermisos = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRfactura = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiRinventario = New DevExpress.XtraBars.BarButtonItem()
        Me.rpAdministracion = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup12 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.rpReportes = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup17 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.BarButtonItem33 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem36 = New DevExpress.XtraBars.BarButtonItem()
        Me.nbcNotificacion = New DevExpress.XtraNavBar.NavBarControl()
        Me.nbgPendiente = New DevExpress.XtraNavBar.NavBarGroup()
        Me.BarButtonItem35 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem37 = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.Tbl_ResumenContaBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tbl_ResumenTransBS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusBar.SuspendLayout()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nbcNotificacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "packages.png")
        Me.ImageList1.Images.SetKeyName(1, "protect blue.png")
        Me.ImageList1.Images.SetKeyName(2, "paint.png")
        Me.ImageList1.Images.SetKeyName(3, "download package.png")
        Me.ImageList1.Images.SetKeyName(4, "actions.png")
        Me.ImageList1.Images.SetKeyName(5, "misc blue.png")
        Me.ImageList1.Images.SetKeyName(6, "truck.png")
        Me.ImageList1.Images.SetKeyName(7, "users 1.png")
        Me.ImageList1.Images.SetKeyName(8, "Copia de trash_full.ico")
        Me.ImageList1.Images.SetKeyName(9, "run.png")
        Me.ImageList1.Images.SetKeyName(10, "kuser.png")
        Me.ImageList1.Images.SetKeyName(11, "games.png")
        Me.ImageList1.Images.SetKeyName(12, "kmultiple.ico")
        Me.ImageList1.Images.SetKeyName(13, "fileprint.png")
        Me.ImageList1.Images.SetKeyName(14, "folder_desktop.png")
        Me.ImageList1.Images.SetKeyName(15, "linphone.png")
        Me.ImageList1.Images.SetKeyName(16, "gaimphone2.png")
        Me.ImageList1.Images.SetKeyName(17, "klaptopdaemon.ico")
        Me.ImageList1.Images.SetKeyName(18, "kate.ico")
        Me.ImageList1.Images.SetKeyName(19, "ark.ico")
        Me.ImageList1.Images.SetKeyName(20, "kspread.ico")
        Me.ImageList1.Images.SetKeyName(21, "Copia de phone.ico")
        Me.ImageList1.Images.SetKeyName(22, "Copia de folder_graphics.ico")
        Me.ImageList1.Images.SetKeyName(23, "Copia de laptop.ico")
        Me.ImageList1.Images.SetKeyName(24, "Copia de recent_documents.ico")
        Me.ImageList1.Images.SetKeyName(25, "16 (File add).ico")
        Me.ImageList1.Images.SetKeyName(26, "029-app.ico")
        Me.ImageList1.Images.SetKeyName(27, "Comptes d'utilisateurs.ico")
        Me.ImageList1.Images.SetKeyName(28, "Date et heure.ico")
        Me.ImageList1.Images.SetKeyName(29, "Copia de folder_down.ico")
        Me.ImageList1.Images.SetKeyName(30, "cal.png")
        Me.ImageList1.Images.SetKeyName(31, "status_away.png")
        Me.ImageList1.Images.SetKeyName(32, "calculator_add.png")
        Me.ImageList1.Images.SetKeyName(33, "money_add.png")
        Me.ImageList1.Images.SetKeyName(34, "Notepad Bloc notes 2.png")
        Me.ImageList1.Images.SetKeyName(35, "txt.ico")
        Me.ImageList1.Images.SetKeyName(36, "money_delete.ico")
        Me.ImageList1.Images.SetKeyName(37, "Copia de folder_explorer.ico")
        Me.ImageList1.Images.SetKeyName(38, "money_dollar.ico")
        Me.ImageList1.Images.SetKeyName(39, "newspaper_delete.ico")
        Me.ImageList1.Images.SetKeyName(40, "newspaper_add.png")
        Me.ImageList1.Images.SetKeyName(41, "Date et heure.ico")
        Me.ImageList1.Images.SetKeyName(42, "filenew.ico")
        Me.ImageList1.Images.SetKeyName(43, "edit.png")
        Me.ImageList1.Images.SetKeyName(44, "trashcan_full.ico")
        Me.ImageList1.Images.SetKeyName(45, "button_ok.png")
        Me.ImageList1.Images.SetKeyName(46, "button_cancel.ico")
        Me.ImageList1.Images.SetKeyName(47, "filefind.png")
        Me.ImageList1.Images.SetKeyName(48, "spreadsheet.ico")
        Me.ImageList1.Images.SetKeyName(49, "fileprint.png")
        Me.ImageList1.Images.SetKeyName(50, "exit.ico")
        Me.ImageList1.Images.SetKeyName(51, "008-doc_edit.ico")
        Me.ImageList1.Images.SetKeyName(52, "008-doc_edit.ico")
        Me.ImageList1.Images.SetKeyName(53, "reminders.ico")
        Me.ImageList1.Images.SetKeyName(54, "kspread.ico")
        Me.ImageList1.Images.SetKeyName(55, "vcalendar.ico")
        Me.ImageList1.Images.SetKeyName(56, "view_tree.ico")
        Me.ImageList1.Images.SetKeyName(57, "barco.png")
        Me.ImageList1.Images.SetKeyName(58, "Camion DIMAR2.png")
        Me.ImageList1.Images.SetKeyName(59, "doc-receta.png")
        Me.ImageList1.Images.SetKeyName(60, "doc-check.png")
        Me.ImageList1.Images.SetKeyName(61, "user_edit.png")
        Me.ImageList1.Images.SetKeyName(62, "usuario-permisos.png")
        Me.ImageList1.Images.SetKeyName(63, "date_task.png")
        Me.ImageList1.Images.SetKeyName(64, "date_time_functions.png")
        Me.ImageList1.Images.SetKeyName(65, "update.png")
        Me.ImageList1.Images.SetKeyName(66, "page_white_add.png")
        Me.ImageList1.Images.SetKeyName(67, "page_white_delete.png")
        Me.ImageList1.Images.SetKeyName(68, "page_white_edit.png")
        Me.ImageList1.Images.SetKeyName(69, "page_white_find.png")
        Me.ImageList1.Images.SetKeyName(70, "cancel.png")
        Me.ImageList1.Images.SetKeyName(71, "save_close.png")
        Me.ImageList1.Images.SetKeyName(72, "cross.png")
        Me.ImageList1.Images.SetKeyName(73, "export_excel.png")
        Me.ImageList1.Images.SetKeyName(74, "liberar-icono-3901-32.png")
        Me.ImageList1.Images.SetKeyName(75, "release-icone-7042-32.png")
        Me.ImageList1.Images.SetKeyName(76, "vaciar-papelera-icono-4813-32.png")
        Me.ImageList1.Images.SetKeyName(77, "door_out.png")
        Me.ImageList1.Images.SetKeyName(78, "report_design.png")
        Me.ImageList1.Images.SetKeyName(79, "lorry_flatbed.png")
        Me.ImageList1.Images.SetKeyName(80, "category.png")
        Me.ImageList1.Images.SetKeyName(81, "legend.png")
        Me.ImageList1.Images.SetKeyName(82, "pluma-de-tinta-escribir-bien-icono-4325-32.png")
        Me.ImageList1.Images.SetKeyName(83, "bookmark_red.png")
        Me.ImageList1.Images.SetKeyName(84, "document_mark_as_final.png")
        Me.ImageList1.Images.SetKeyName(85, "installer_box.png")
        Me.ImageList1.Images.SetKeyName(86, "baggage_cart_box.png")
        Me.ImageList1.Images.SetKeyName(87, "box_closed.png")
        Me.ImageList1.Images.SetKeyName(88, "map.png")
        Me.ImageList1.Images.SetKeyName(89, "world.png")
        Me.ImageList1.Images.SetKeyName(90, "globe_model.png")
        Me.ImageList1.Images.SetKeyName(91, "box.png")
        Me.ImageList1.Images.SetKeyName(92, "box_resize.png")
        Me.ImageList1.Images.SetKeyName(93, "box_resize_actual.png")
        Me.ImageList1.Images.SetKeyName(94, "lorry_box.png")
        Me.ImageList1.Images.SetKeyName(95, "clipboard_invoice.png")
        Me.ImageList1.Images.SetKeyName(96, "money_dollar.png")
        Me.ImageList1.Images.SetKeyName(97, "total_plan_cost.png")
        Me.ImageList1.Images.SetKeyName(98, "restaurant_menu.png")
        Me.ImageList1.Images.SetKeyName(99, "document_prepare.png")
        Me.ImageList1.Images.SetKeyName(100, "paste_plain.png")
        Me.ImageList1.Images.SetKeyName(101, "sheduled_task.png")
        Me.ImageList1.Images.SetKeyName(102, "delay_delivery_outlook.png")
        Me.ImageList1.Images.SetKeyName(103, "account_balances.png")
        Me.ImageList1.Images.SetKeyName(104, "client_account_template.png")
        Me.ImageList1.Images.SetKeyName(105, "account_functions.png")
        Me.ImageList1.Images.SetKeyName(106, "credit.png")
        Me.ImageList1.Images.SetKeyName(107, "financial_functions.png")
        Me.ImageList1.Images.SetKeyName(108, "inbox_download.png")
        Me.ImageList1.Images.SetKeyName(109, "inbox_upload.png")
        Me.ImageList1.Images.SetKeyName(110, "direction.png")
        Me.ImageList1.Images.SetKeyName(111, "if_receipt_40830.ico")
        Me.ImageList1.Images.SetKeyName(112, "Iconshock-Free-Folder-Folder-invoices.ico")
        Me.ImageList1.Images.SetKeyName(113, "close_6303.ico")
        '
        'bbiReportTrans
        '
        Me.bbiReportTrans.Caption = "Reportes de Transacciones"
        Me.bbiReportTrans.Id = 33
        Me.bbiReportTrans.Name = "bbiReportTrans"
        Me.bbiReportTrans.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'StatusBar
        '
        Me.StatusBar.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSSucursal, Me.lblSucursal, Me.TSUsuario, Me.LblNombreUsuario, Me.TSFecha, Me.LblFecha, Me.TSHora, Me.LblHora, Me.TSProceso, Me.tsslbProceso, Me.TSProgressBar1, Me.ToolStripStatusLabel1})
        Me.StatusBar.Location = New System.Drawing.Point(0, 321)
        Me.StatusBar.Name = "StatusBar"
        Me.StatusBar.Padding = New System.Windows.Forms.Padding(1, 0, 2, 0)
        Me.StatusBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusBar.Size = New System.Drawing.Size(782, 25)
        Me.StatusBar.TabIndex = 5
        Me.StatusBar.Text = "StatusStrip1"
        '
        'TSSucursal
        '
        Me.TSSucursal.BackColor = System.Drawing.SystemColors.Control
        Me.TSSucursal.Name = "TSSucursal"
        Me.TSSucursal.Size = New System.Drawing.Size(70, 20)
        Me.TSSucursal.Text = "Sucursal :"
        '
        'lblSucursal
        '
        Me.lblSucursal.BackColor = System.Drawing.SystemColors.Control
        Me.lblSucursal.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSucursal.ForeColor = System.Drawing.Color.Navy
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(174, 20)
        Me.lblSucursal.Text = "DIMAR S.A. (CENTRAL)"
        '
        'TSUsuario
        '
        Me.TSUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.TSUsuario.Name = "TSUsuario"
        Me.TSUsuario.Size = New System.Drawing.Size(66, 20)
        Me.TSUsuario.Text = "Usuario :"
        '
        'LblNombreUsuario
        '
        Me.LblNombreUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.LblNombreUsuario.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNombreUsuario.ForeColor = System.Drawing.Color.Navy
        Me.LblNombreUsuario.Name = "LblNombreUsuario"
        Me.LblNombreUsuario.Size = New System.Drawing.Size(111, 20)
        Me.LblNombreUsuario.Text = "Administrador"
        '
        'TSFecha
        '
        Me.TSFecha.BackColor = System.Drawing.SystemColors.Control
        Me.TSFecha.Name = "TSFecha"
        Me.TSFecha.Size = New System.Drawing.Size(54, 20)
        Me.TSFecha.Text = "Fecha :"
        '
        'LblFecha
        '
        Me.LblFecha.BackColor = System.Drawing.SystemColors.Control
        Me.LblFecha.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFecha.ForeColor = System.Drawing.Color.Navy
        Me.LblFecha.Name = "LblFecha"
        Me.LblFecha.Size = New System.Drawing.Size(98, 20)
        Me.LblFecha.Text = "Fecha Actual"
        '
        'TSHora
        '
        Me.TSHora.BackColor = System.Drawing.SystemColors.Control
        Me.TSHora.Name = "TSHora"
        Me.TSHora.Size = New System.Drawing.Size(49, 20)
        Me.TSHora.Text = "Hora :"
        '
        'LblHora
        '
        Me.LblHora.BackColor = System.Drawing.SystemColors.Control
        Me.LblHora.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblHora.ForeColor = System.Drawing.Color.Navy
        Me.LblHora.Name = "LblHora"
        Me.LblHora.Size = New System.Drawing.Size(92, 20)
        Me.LblHora.Text = "Hora Actual"
        '
        'TSProceso
        '
        Me.TSProceso.BackColor = System.Drawing.SystemColors.Control
        Me.TSProceso.Name = "TSProceso"
        Me.TSProceso.Size = New System.Drawing.Size(95, 20)
        Me.TSProceso.Text = "En Ejecucion:"
        Me.TSProceso.Visible = False
        '
        'tsslbProceso
        '
        Me.tsslbProceso.BackColor = System.Drawing.SystemColors.Control
        Me.tsslbProceso.ForeColor = System.Drawing.Color.Red
        Me.tsslbProceso.Name = "tsslbProceso"
        Me.tsslbProceso.Size = New System.Drawing.Size(146, 20)
        Me.tsslbProceso.Text = "ACTUALIZANDO T/C"
        Me.tsslbProceso.Visible = False
        '
        'TSProgressBar1
        '
        Me.TSProgressBar1.Name = "TSProgressBar1"
        Me.TSProgressBar1.Size = New System.Drawing.Size(42, 19)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(153, 20)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Visible = False
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "Office 2010 Black"
        Me.DefaultLookAndFeel1.LookAndFeel.UseWindowsXPTheme = True
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Inventarios"
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Nothing
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'bbiCompraRecargas
        '
        Me.bbiCompraRecargas.Caption = "Compra de Recargas"
        Me.bbiCompraRecargas.Id = 60
        Me.bbiCompraRecargas.ImageIndex = 21
        Me.bbiCompraRecargas.Name = "bbiCompraRecargas"
        Me.bbiCompraRecargas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem29
        '
        Me.BarButtonItem29.Caption = "Orden de Pedidos"
        Me.BarButtonItem29.Id = 71
        Me.BarButtonItem29.ImageIndex = 48
        Me.BarButtonItem29.Name = "BarButtonItem29"
        Me.BarButtonItem29.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem30
        '
        Me.BarButtonItem30.Caption = "Ordenes de Producción"
        Me.BarButtonItem30.Id = 126
        Me.BarButtonItem30.Name = "BarButtonItem30"
        Me.BarButtonItem30.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem31
        '
        Me.BarButtonItem31.Caption = "Recetas de Producción"
        Me.BarButtonItem31.Id = 125
        Me.BarButtonItem31.Name = "BarButtonItem31"
        Me.BarButtonItem31.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpContab
        '
        Me.rpContab.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup15, Me.RibbonPageGroup10, Me.RibbonPageGroup9, Me.RibbonPageGroup13, Me.rpgInformesFinan, Me.RibbonPageGroup27})
        Me.rpContab.Name = "rpContab"
        Me.rpContab.Text = "Contabilidad"
        '
        'RibbonPageGroup15
        '
        Me.RibbonPageGroup15.ItemLinks.Add(Me.bbiSolicitudesCheque)
        Me.RibbonPageGroup15.ItemLinks.Add(Me.bbiOrdenPago)
        Me.RibbonPageGroup15.ItemLinks.Add(Me.bbiGenerarCheques)
        Me.RibbonPageGroup15.ItemLinks.Add(Me.bbiEditComprob)
        Me.RibbonPageGroup15.Name = "RibbonPageGroup15"
        Me.RibbonPageGroup15.Text = "Contabilidad"
        '
        'bbiSolicitudesCheque
        '
        Me.bbiSolicitudesCheque.Caption = "Solicitudes de Cheque"
        Me.bbiSolicitudesCheque.Id = 107
        Me.bbiSolicitudesCheque.ImageIndex = 51
        Me.bbiSolicitudesCheque.Name = "bbiSolicitudesCheque"
        Me.bbiSolicitudesCheque.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiSolicitudesCheque.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'bbiOrdenPago
        '
        Me.bbiOrdenPago.Caption = "Ordenes de Pago"
        Me.bbiOrdenPago.Id = 108
        Me.bbiOrdenPago.ImageIndex = 107
        Me.bbiOrdenPago.Name = "bbiOrdenPago"
        Me.bbiOrdenPago.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiGenerarCheques
        '
        Me.bbiGenerarCheques.Caption = "Generar Cheques"
        Me.bbiGenerarCheques.Id = 109
        Me.bbiGenerarCheques.ImageIndex = 54
        Me.bbiGenerarCheques.Name = "bbiGenerarCheques"
        Me.bbiGenerarCheques.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiGenerarCheques.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'bbiEditComprob
        '
        Me.bbiEditComprob.Caption = "Editor de Comprobantes"
        Me.bbiEditComprob.Id = 66
        Me.bbiEditComprob.ImageIndex = 55
        Me.bbiEditComprob.Name = "bbiEditComprob"
        Me.bbiEditComprob.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiEditComprob.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'RibbonPageGroup10
        '
        Me.RibbonPageGroup10.ItemLinks.Add(Me.bbiEditCatalogo)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.bbiPlantillasCont)
        Me.RibbonPageGroup10.Name = "RibbonPageGroup10"
        Me.RibbonPageGroup10.Text = "Resumen de Operaciones"
        '
        'bbiEditCatalogo
        '
        Me.bbiEditCatalogo.Caption = "Editor de Catalogo de Cuentas"
        Me.bbiEditCatalogo.Id = 30
        Me.bbiEditCatalogo.ImageIndex = 56
        Me.bbiEditCatalogo.Name = "bbiEditCatalogo"
        Me.bbiEditCatalogo.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPlantillasCont
        '
        Me.bbiPlantillasCont.Caption = "Plantillas Contables"
        Me.bbiPlantillasCont.Id = 32
        Me.bbiPlantillasCont.Name = "bbiPlantillasCont"
        Me.bbiPlantillasCont.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.bbiHacerCierre)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.Text = "Cierre Diario"
        Me.RibbonPageGroup9.Visible = False
        '
        'bbiHacerCierre
        '
        Me.bbiHacerCierre.Caption = "Realizar Cierre"
        Me.bbiHacerCierre.Id = 29
        Me.bbiHacerCierre.Name = "bbiHacerCierre"
        Me.bbiHacerCierre.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup13
        '
        Me.RibbonPageGroup13.ItemLinks.Add(Me.bbiRegGastos)
        Me.RibbonPageGroup13.Name = "RibbonPageGroup13"
        Me.RibbonPageGroup13.Text = "Gastos Operacionales"
        Me.RibbonPageGroup13.Visible = False
        '
        'bbiRegGastos
        '
        Me.bbiRegGastos.Caption = "Registro de Gastos Operacionales y Otros Gastos"
        Me.bbiRegGastos.Id = 52
        Me.bbiRegGastos.ImageIndex = 24
        Me.bbiRegGastos.Name = "bbiRegGastos"
        Me.bbiRegGastos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpgInformesFinan
        '
        Me.rpgInformesFinan.ItemLinks.Add(Me.BarButtonItem16)
        Me.rpgInformesFinan.ItemLinks.Add(Me.BarButtonItem17)
        Me.rpgInformesFinan.ItemLinks.Add(Me.BarButtonItem18)
        Me.rpgInformesFinan.ItemLinks.Add(Me.BarButtonItem19)
        Me.rpgInformesFinan.ItemLinks.Add(Me.BarButtonItem20)
        Me.rpgInformesFinan.Name = "rpgInformesFinan"
        Me.rpgInformesFinan.Text = "Informes Financieros"
        Me.rpgInformesFinan.Visible = False
        '
        'BarButtonItem16
        '
        Me.BarButtonItem16.Caption = "Libro Auxiliar de Cuentas"
        Me.BarButtonItem16.Id = 110
        Me.BarButtonItem16.Name = "BarButtonItem16"
        '
        'BarButtonItem17
        '
        Me.BarButtonItem17.Caption = "Balanza de Comprobación"
        Me.BarButtonItem17.Id = 111
        Me.BarButtonItem17.Name = "BarButtonItem17"
        '
        'BarButtonItem18
        '
        Me.BarButtonItem18.Caption = "Balance General"
        Me.BarButtonItem18.Id = 112
        Me.BarButtonItem18.Name = "BarButtonItem18"
        '
        'BarButtonItem19
        '
        Me.BarButtonItem19.Caption = "Estado de Resultados"
        Me.BarButtonItem19.Id = 113
        Me.BarButtonItem19.Name = "BarButtonItem19"
        '
        'BarButtonItem20
        '
        Me.BarButtonItem20.Caption = "Balance Comparativo"
        Me.BarButtonItem20.Id = 114
        Me.BarButtonItem20.Name = "BarButtonItem20"
        '
        'RibbonPageGroup27
        '
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem21)
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem22)
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem23)
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem25)
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem24)
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem27)
        Me.RibbonPageGroup27.ItemLinks.Add(Me.BarButtonItem28)
        Me.RibbonPageGroup27.Name = "RibbonPageGroup27"
        Me.RibbonPageGroup27.Text = "Formatos Auxiliares"
        Me.RibbonPageGroup27.Visible = False
        '
        'BarButtonItem21
        '
        Me.BarButtonItem21.Caption = "Plantilla de Retenciones"
        Me.BarButtonItem21.Id = 115
        Me.BarButtonItem21.Name = "BarButtonItem21"
        '
        'BarButtonItem22
        '
        Me.BarButtonItem22.Caption = "Plantilla de Ingresos"
        Me.BarButtonItem22.Id = 116
        Me.BarButtonItem22.Name = "BarButtonItem22"
        '
        'BarButtonItem23
        '
        Me.BarButtonItem23.Caption = "Plantilla de Credito Fiscal"
        Me.BarButtonItem23.Id = 117
        Me.BarButtonItem23.Name = "BarButtonItem23"
        '
        'BarButtonItem25
        '
        Me.BarButtonItem25.Caption = "Reporte de Exoneraciones"
        Me.BarButtonItem25.Id = 119
        Me.BarButtonItem25.Name = "BarButtonItem25"
        '
        'BarButtonItem24
        '
        Me.BarButtonItem24.Caption = "Declaración DMI"
        Me.BarButtonItem24.Id = 118
        Me.BarButtonItem24.Name = "BarButtonItem24"
        '
        'BarButtonItem27
        '
        Me.BarButtonItem27.Caption = "Declaración Alcaldía"
        Me.BarButtonItem27.Id = 120
        Me.BarButtonItem27.Name = "BarButtonItem27"
        '
        'BarButtonItem28
        '
        Me.BarButtonItem28.Caption = "BarButtonItem28"
        Me.BarButtonItem28.Id = 121
        Me.BarButtonItem28.Name = "BarButtonItem28"
        '
        'rpRecursosHum
        '
        Me.rpRecursosHum.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup7, Me.RibbonPageGroup20, Me.RibbonPageGroup21, Me.RibbonPageGroup8})
        Me.rpRecursosHum.Name = "rpRecursosHum"
        Me.rpRecursosHum.Text = "Recursos Humanos"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bbiExpedEmpleado)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bbiControlVacaciones)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bbiLiquidacionesPers)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bbiGenerarPlanilla)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.Text = "Colaboradores"
        '
        'bbiExpedEmpleado
        '
        Me.bbiExpedEmpleado.Caption = "Expediente de Empleado"
        Me.bbiExpedEmpleado.Id = 77
        Me.bbiExpedEmpleado.ImageIndex = 27
        Me.bbiExpedEmpleado.Name = "bbiExpedEmpleado"
        Me.bbiExpedEmpleado.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiControlVacaciones
        '
        Me.bbiControlVacaciones.Caption = "Control de Vacaciones"
        Me.bbiControlVacaciones.Id = 85
        Me.bbiControlVacaciones.ImageIndex = 28
        Me.bbiControlVacaciones.Name = "bbiControlVacaciones"
        Me.bbiControlVacaciones.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiLiquidacionesPers
        '
        Me.bbiLiquidacionesPers.Caption = "Liquidación de Personal"
        Me.bbiLiquidacionesPers.Id = 87
        Me.bbiLiquidacionesPers.ImageIndex = 29
        Me.bbiLiquidacionesPers.Name = "bbiLiquidacionesPers"
        Me.bbiLiquidacionesPers.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiGenerarPlanilla
        '
        Me.bbiGenerarPlanilla.Caption = "Generar Planilla"
        Me.bbiGenerarPlanilla.Id = 86
        Me.bbiGenerarPlanilla.ImageIndex = 30
        Me.bbiGenerarPlanilla.Name = "bbiGenerarPlanilla"
        Me.bbiGenerarPlanilla.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup20
        '
        Me.RibbonPageGroup20.ItemLinks.Add(Me.bbiControlHorasExtras)
        Me.RibbonPageGroup20.ItemLinks.Add(Me.bbiComisiones)
        Me.RibbonPageGroup20.ItemLinks.Add(Me.bbiOtrosIngresos)
        Me.RibbonPageGroup20.Name = "RibbonPageGroup20"
        Me.RibbonPageGroup20.Text = "Devengados"
        '
        'bbiControlHorasExtras
        '
        Me.bbiControlHorasExtras.Caption = "Horas Extras"
        Me.bbiControlHorasExtras.Id = 82
        Me.bbiControlHorasExtras.ImageIndex = 31
        Me.bbiControlHorasExtras.Name = "bbiControlHorasExtras"
        Me.bbiControlHorasExtras.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiComisiones
        '
        Me.bbiComisiones.Caption = "Comisiones"
        Me.bbiComisiones.Id = 83
        Me.bbiComisiones.ImageIndex = 32
        Me.bbiComisiones.Name = "bbiComisiones"
        Me.bbiComisiones.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiOtrosIngresos
        '
        Me.bbiOtrosIngresos.Caption = "Otros Ingresos"
        Me.bbiOtrosIngresos.Id = 81
        Me.bbiOtrosIngresos.ImageIndex = 33
        Me.bbiOtrosIngresos.Name = "bbiOtrosIngresos"
        Me.bbiOtrosIngresos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup21
        '
        Me.RibbonPageGroup21.ItemLinks.Add(Me.bbiConveniosFinan)
        Me.RibbonPageGroup21.ItemLinks.Add(Me.bbiPrestamosPers)
        Me.RibbonPageGroup21.ItemLinks.Add(Me.bbiOtrasDeducciones)
        Me.RibbonPageGroup21.Name = "RibbonPageGroup21"
        Me.RibbonPageGroup21.Text = "Deducciones"
        '
        'bbiConveniosFinan
        '
        Me.bbiConveniosFinan.Caption = "Convenios Financieros"
        Me.bbiConveniosFinan.Id = 78
        Me.bbiConveniosFinan.ImageIndex = 34
        Me.bbiConveniosFinan.Name = "bbiConveniosFinan"
        Me.bbiConveniosFinan.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPrestamosPers
        '
        Me.bbiPrestamosPers.Caption = "Prestamos a Personal"
        Me.bbiPrestamosPers.Id = 79
        Me.bbiPrestamosPers.ImageIndex = 35
        Me.bbiPrestamosPers.Name = "bbiPrestamosPers"
        Me.bbiPrestamosPers.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiOtrasDeducciones
        '
        Me.bbiOtrasDeducciones.Caption = "Otras Deducciones"
        Me.bbiOtrasDeducciones.Id = 80
        Me.bbiOtrasDeducciones.ImageIndex = 36
        Me.bbiOtrasDeducciones.Name = "bbiOtrasDeducciones"
        Me.bbiOtrasDeducciones.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BarButtonItem26)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.Text = "Control de Vacaciones"
        '
        'BarButtonItem26
        '
        Me.BarButtonItem26.Id = 84
        Me.BarButtonItem26.Name = "BarButtonItem26"
        '
        'rpTesoreria
        '
        Me.rpTesoreria.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup25})
        Me.rpTesoreria.Name = "rpTesoreria"
        Me.rpTesoreria.Text = "Tesorería"
        Me.rpTesoreria.Visible = False
        '
        'RibbonPageGroup25
        '
        Me.RibbonPageGroup25.ItemLinks.Add(Me.bbiProgramPagos)
        Me.RibbonPageGroup25.ItemLinks.Add(Me.bbiOrdenesPago)
        Me.RibbonPageGroup25.Name = "RibbonPageGroup25"
        Me.RibbonPageGroup25.Text = "Cuentas por Pagar"
        '
        'bbiProgramPagos
        '
        Me.bbiProgramPagos.Caption = "Programación de Pagos"
        Me.bbiProgramPagos.Id = 94
        Me.bbiProgramPagos.Name = "bbiProgramPagos"
        Me.bbiProgramPagos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiOrdenesPago
        '
        Me.bbiOrdenesPago.Caption = "Ordenes de Pago"
        Me.bbiOrdenesPago.Id = 95
        Me.bbiOrdenesPago.Name = "bbiOrdenesPago"
        Me.bbiOrdenesPago.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpCartera
        '
        Me.rpCartera.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup22, Me.RibbonPageGroup23, Me.RibbonPageGroup24})
        Me.rpCartera.Name = "rpCartera"
        Me.rpCartera.Text = "Cartera y Cobro"
        Me.rpCartera.Visible = False
        '
        'RibbonPageGroup22
        '
        Me.RibbonPageGroup22.ItemLinks.Add(Me.bbiVerMovCuenta)
        Me.RibbonPageGroup22.Name = "RibbonPageGroup22"
        Me.RibbonPageGroup22.Text = "Estado de Cuenta"
        '
        'bbiVerMovCuenta
        '
        Me.bbiVerMovCuenta.Caption = "Visualizar Movimientos de la Cuenta"
        Me.bbiVerMovCuenta.Id = 88
        Me.bbiVerMovCuenta.ImageIndex = 37
        Me.bbiVerMovCuenta.Name = "bbiVerMovCuenta"
        Me.bbiVerMovCuenta.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup23
        '
        Me.RibbonPageGroup23.ItemLinks.Add(Me.bbiIngresosPagos)
        Me.RibbonPageGroup23.ItemLinks.Add(Me.bbiIngresosNotaCred)
        Me.RibbonPageGroup23.ItemLinks.Add(Me.bbiIngresosNotaDebitos)
        Me.RibbonPageGroup23.Name = "RibbonPageGroup23"
        Me.RibbonPageGroup23.Text = "Pagos y Notas"
        '
        'bbiIngresosPagos
        '
        Me.bbiIngresosPagos.Caption = "Ingresar Pagos"
        Me.bbiIngresosPagos.Id = 89
        Me.bbiIngresosPagos.ImageIndex = 38
        Me.bbiIngresosPagos.Name = "bbiIngresosPagos"
        Me.bbiIngresosPagos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiIngresosNotaCred
        '
        Me.bbiIngresosNotaCred.Caption = "Ingresar Nota de Credito"
        Me.bbiIngresosNotaCred.Id = 90
        Me.bbiIngresosNotaCred.ImageIndex = 39
        Me.bbiIngresosNotaCred.Name = "bbiIngresosNotaCred"
        Me.bbiIngresosNotaCred.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiIngresosNotaDebitos
        '
        Me.bbiIngresosNotaDebitos.Caption = "Ingresar Nota de Debito"
        Me.bbiIngresosNotaDebitos.Id = 91
        Me.bbiIngresosNotaDebitos.ImageIndex = 40
        Me.bbiIngresosNotaDebitos.Name = "bbiIngresosNotaDebitos"
        Me.bbiIngresosNotaDebitos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup24
        '
        Me.RibbonPageGroup24.ItemLinks.Add(Me.bbiLlamadasyProgram)
        Me.RibbonPageGroup24.ItemLinks.Add(Me.bbiRptCartera)
        Me.RibbonPageGroup24.Name = "RibbonPageGroup24"
        Me.RibbonPageGroup24.Text = "Control de Cartera"
        '
        'bbiLlamadasyProgram
        '
        Me.bbiLlamadasyProgram.Caption = "Llamadas y Programaciones de Pagos"
        Me.bbiLlamadasyProgram.Id = 92
        Me.bbiLlamadasyProgram.ImageIndex = 41
        Me.bbiLlamadasyProgram.Name = "bbiLlamadasyProgram"
        Me.bbiLlamadasyProgram.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiRptCartera
        '
        Me.bbiRptCartera.Caption = "Reportes de Cartera"
        Me.bbiRptCartera.Id = 93
        Me.bbiRptCartera.Name = "bbiRptCartera"
        Me.bbiRptCartera.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpCentroSol
        '
        Me.rpCentroSol.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup4, Me.RibbonPageGroup2, Me.RibbonPageGroup3, Me.RibbonPageGroup16})
        Me.rpCentroSol.Name = "rpCentroSol"
        Me.rpCentroSol.Text = "Facturación"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.bbiFacturacion)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.bbiFverificacion)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.bbiFendose)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.bbiFcliente)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.bbiDevolucion)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "Ventas"
        '
        'bbiFacturacion
        '
        Me.bbiFacturacion.Caption = "Facturación"
        Me.bbiFacturacion.Id = 12
        Me.bbiFacturacion.ImageIndex = 112
        Me.bbiFacturacion.Name = "bbiFacturacion"
        Me.bbiFacturacion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiFverificacion
        '
        Me.bbiFverificacion.Caption = "Verificacion Factura"
        Me.bbiFverificacion.Id = 168
        Me.bbiFverificacion.ImageIndex = 111
        Me.bbiFverificacion.Name = "bbiFverificacion"
        Me.bbiFverificacion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiFendose
        '
        Me.bbiFendose.Caption = "Endose Factura"
        Me.bbiFendose.Id = 166
        Me.bbiFendose.ImageIndex = 30
        Me.bbiFendose.Name = "bbiFendose"
        Me.bbiFendose.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiFcliente
        '
        Me.bbiFcliente.Caption = "Verificacion Cliente"
        Me.bbiFcliente.Id = 141
        Me.bbiFcliente.ImageIndex = 104
        Me.bbiFcliente.Name = "bbiFcliente"
        Me.bbiFcliente.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiDevolucion
        '
        Me.bbiDevolucion.Caption = "Devoluciones"
        Me.bbiDevolucion.Id = 19
        Me.bbiDevolucion.ImageIndex = 14
        Me.bbiDevolucion.Name = "bbiDevolucion"
        Me.bbiDevolucion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiDevolucion.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bbiCotizacion)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bbiPedidos)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Solicitudes de Clientes"
        Me.RibbonPageGroup2.Visible = False
        '
        'bbiCotizacion
        '
        Me.bbiCotizacion.Caption = "Cotizaciones"
        Me.bbiCotizacion.Id = 14
        Me.bbiCotizacion.ImageIndex = 13
        Me.bbiCotizacion.Name = "bbiCotizacion"
        Me.bbiCotizacion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiCotizacion.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'bbiPedidos
        '
        Me.bbiPedidos.Caption = "Pedidos"
        Me.bbiPedidos.Id = 13
        Me.bbiPedidos.ImageIndex = 12
        Me.bbiPedidos.Name = "bbiPedidos"
        Me.bbiPedidos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiPedidos.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.bbiReciboManual)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.bbiAplicarAnticipo)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.bbiArqueo)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Cierre de Caja"
        Me.RibbonPageGroup3.Visible = False
        '
        'bbiReciboManual
        '
        Me.bbiReciboManual.Caption = "Ingresar Anticipo"
        Me.bbiReciboManual.Id = 122
        Me.bbiReciboManual.Name = "bbiReciboManual"
        Me.bbiReciboManual.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiAplicarAnticipo
        '
        Me.bbiAplicarAnticipo.Caption = "Aplicar Anticipos"
        Me.bbiAplicarAnticipo.Id = 123
        Me.bbiAplicarAnticipo.Name = "bbiAplicarAnticipo"
        Me.bbiAplicarAnticipo.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiArqueo
        '
        Me.bbiArqueo.Caption = "Generar Arqueo del Día"
        Me.bbiArqueo.Id = 68
        Me.bbiArqueo.Name = "bbiArqueo"
        Me.bbiArqueo.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup16
        '
        Me.RibbonPageGroup16.ItemLinks.Add(Me.bbiInformes)
        Me.RibbonPageGroup16.Name = "RibbonPageGroup16"
        Me.RibbonPageGroup16.Text = "Reportes"
        Me.RibbonPageGroup16.Visible = False
        '
        'bbiInformes
        '
        Me.bbiInformes.Caption = "Visualizar Informes CS"
        Me.bbiInformes.Id = 69
        Me.bbiInformes.Name = "bbiInformes"
        Me.bbiInformes.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpInventory
        '
        Me.rpInventory.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup5, Me.RibbonPageGroup19})
        Me.rpInventory.Name = "rpInventory"
        Me.rpInventory.Text = "Inventario"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.btnRemision)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.bbiEntradas)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.bbiSalidas)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.bbiTransferencia)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.bbiControlKardex)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "Transacciones"
        '
        'btnRemision
        '
        Me.btnRemision.Caption = "Remision de Mercaderia"
        Me.btnRemision.Id = 140
        Me.btnRemision.ImageIndex = 94
        Me.btnRemision.Name = "btnRemision"
        Me.btnRemision.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiEntradas
        '
        Me.bbiEntradas.Caption = "Entradas"
        Me.bbiEntradas.Id = 47
        Me.bbiEntradas.ImageIndex = 108
        Me.bbiEntradas.Name = "bbiEntradas"
        Me.bbiEntradas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiSalidas
        '
        Me.bbiSalidas.Caption = "Salidas"
        Me.bbiSalidas.Id = 48
        Me.bbiSalidas.ImageIndex = 109
        Me.bbiSalidas.Name = "bbiSalidas"
        Me.bbiSalidas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiTransferencia
        '
        Me.bbiTransferencia.Caption = "Traslados"
        Me.bbiTransferencia.Id = 49
        Me.bbiTransferencia.ImageIndex = 110
        Me.bbiTransferencia.Name = "bbiTransferencia"
        Me.bbiTransferencia.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiControlKardex
        '
        Me.bbiControlKardex.Caption = "Control de Kardex"
        Me.bbiControlKardex.Id = 50
        Me.bbiControlKardex.ImageIndex = 95
        Me.bbiControlKardex.Name = "bbiControlKardex"
        Me.bbiControlKardex.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup19
        '
        Me.RibbonPageGroup19.ItemLinks.Add(Me.bbiInfoAlmacen)
        Me.RibbonPageGroup19.Name = "RibbonPageGroup19"
        Me.RibbonPageGroup19.Text = "Reportes"
        Me.RibbonPageGroup19.Visible = False
        '
        'bbiInfoAlmacen
        '
        Me.bbiInfoAlmacen.Caption = "Informes de Almacen"
        Me.bbiInfoAlmacen.Id = 75
        Me.bbiInfoAlmacen.Name = "bbiInfoAlmacen"
        '
        'rpProducción
        '
        Me.rpProducción.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup28, Me.RibbonPageGroup30})
        Me.rpProducción.Name = "rpProducción"
        Me.rpProducción.Text = "Produccion"
        '
        'RibbonPageGroup28
        '
        Me.RibbonPageGroup28.ItemLinks.Add(Me.btnRecetasProduccion)
        Me.RibbonPageGroup28.ItemLinks.Add(Me.btnOrdenProd)
        Me.RibbonPageGroup28.ItemLinks.Add(Me.BarButtonItem32)
        Me.RibbonPageGroup28.ItemLinks.Add(Me.bbiCOrden)
        Me.RibbonPageGroup28.ItemLinks.Add(Me.bbiKit)
        Me.RibbonPageGroup28.ItemLinks.Add(Me.BarButtonItem34)
        Me.RibbonPageGroup28.Name = "RibbonPageGroup28"
        Me.RibbonPageGroup28.Text = "Operaciones de Producción"
        '
        'btnRecetasProduccion
        '
        Me.btnRecetasProduccion.Caption = "Recetas de Producción"
        Me.btnRecetasProduccion.Id = 125
        Me.btnRecetasProduccion.ImageIndex = 99
        Me.btnRecetasProduccion.Name = "btnRecetasProduccion"
        Me.btnRecetasProduccion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'btnOrdenProd
        '
        Me.btnOrdenProd.Caption = "Ordenes de Producción"
        Me.btnOrdenProd.Id = 126
        Me.btnOrdenProd.ImageIndex = 101
        Me.btnOrdenProd.Name = "btnOrdenProd"
        Me.btnOrdenProd.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem32
        '
        Me.BarButtonItem32.Caption = "Ordenes de Servicios"
        Me.BarButtonItem32.Id = 135
        Me.BarButtonItem32.ImageIndex = 100
        Me.BarButtonItem32.Name = "BarButtonItem32"
        Me.BarButtonItem32.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.BarButtonItem32.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'bbiCOrden
        '
        Me.bbiCOrden.Caption = "Cierre de Ordenes"
        Me.bbiCOrden.Id = 138
        Me.bbiCOrden.ImageIndex = 102
        Me.bbiCOrden.Name = "bbiCOrden"
        Me.bbiCOrden.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiKit
        '
        Me.bbiKit.Caption = "Salida de Fabrica"
        Me.bbiKit.Id = 178
        Me.bbiKit.ImageIndex = 91
        Me.bbiKit.Name = "bbiKit"
        Me.bbiKit.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem34
        '
        Me.BarButtonItem34.Caption = "Costos Indirectos y Mano  de Obra"
        Me.BarButtonItem34.Id = 139
        Me.BarButtonItem34.ImageIndex = 97
        Me.BarButtonItem34.Name = "BarButtonItem34"
        Me.BarButtonItem34.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup30
        '
        Me.RibbonPageGroup30.ItemLinks.Add(Me.btnGarantia)
        Me.RibbonPageGroup30.Name = "RibbonPageGroup30"
        Me.RibbonPageGroup30.Text = "RibbonPageGroup30"
        '
        'btnGarantia
        '
        Me.btnGarantia.Caption = "Garantias"
        Me.btnGarantia.Id = 169
        Me.btnGarantia.Name = "btnGarantia"
        '
        'rpAdminLog
        '
        Me.rpAdminLog.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup6, Me.RibbonPageGroup18})
        Me.rpAdminLog.Name = "rpAdminLog"
        Me.rpAdminLog.Text = "Logistica"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bbiProyeccion)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bbiPedidosProv)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bbiCompraLocales)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bbiComprasForaneas)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bbiDevolProv)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "Adquisiciones"
        '
        'bbiProyeccion
        '
        Me.bbiProyeccion.Caption = "Proyeccion de Pedidos"
        Me.bbiProyeccion.Id = 134
        Me.bbiProyeccion.ImageIndex = 64
        Me.bbiProyeccion.Name = "bbiProyeccion"
        Me.bbiProyeccion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPedidosProv
        '
        Me.bbiPedidosProv.Caption = "Orden de Pedidos"
        Me.bbiPedidosProv.Id = 71
        Me.bbiPedidosProv.ImageIndex = 63
        Me.bbiPedidosProv.Name = "bbiPedidosProv"
        Me.bbiPedidosProv.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCompraLocales
        '
        Me.bbiCompraLocales.Caption = "Compras Locales"
        Me.bbiCompraLocales.Id = 23
        Me.bbiCompraLocales.ImageIndex = 88
        Me.bbiCompraLocales.Name = "bbiCompraLocales"
        Me.bbiCompraLocales.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiComprasForaneas
        '
        Me.bbiComprasForaneas.Caption = "Compras Foraneas"
        Me.bbiComprasForaneas.Id = 74
        Me.bbiComprasForaneas.ImageIndex = 90
        Me.bbiComprasForaneas.Name = "bbiComprasForaneas"
        Me.bbiComprasForaneas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiDevolProv
        '
        Me.bbiDevolProv.Caption = "Devoluciones"
        Me.bbiDevolProv.Id = 24
        Me.bbiDevolProv.ImageIndex = 55
        Me.bbiDevolProv.Name = "bbiDevolProv"
        Me.bbiDevolProv.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiDevolProv.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'RibbonPageGroup18
        '
        Me.RibbonPageGroup18.ItemLinks.Add(Me.bbiControlPedidos)
        Me.RibbonPageGroup18.ItemLinks.Add(Me.bbiInformeLogistica)
        Me.RibbonPageGroup18.Name = "RibbonPageGroup18"
        Me.RibbonPageGroup18.Text = "Monitoreo de Pedidos"
        Me.RibbonPageGroup18.Visible = False
        '
        'bbiControlPedidos
        '
        Me.bbiControlPedidos.Caption = "Control de Estado de Pedidos"
        Me.bbiControlPedidos.Id = 72
        Me.bbiControlPedidos.Name = "bbiControlPedidos"
        '
        'bbiInformeLogistica
        '
        Me.bbiInformeLogistica.Caption = "Informes de Logistica"
        Me.bbiInformeLogistica.Id = 73
        Me.bbiInformeLogistica.Name = "bbiInformeLogistica"
        '
        'rpCatalogos
        '
        Me.rpCatalogos.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup14, Me.RibbonPageGroup11, Me.RibbonPageGroup29})
        Me.rpCatalogos.Name = "rpCatalogos"
        Me.rpCatalogos.Text = "Catalogos"
        '
        'RibbonPageGroup14
        '
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiBodegas)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiMarcas)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiModelos)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiDepartamentos)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiCategorias)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiProveedor)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiCliente)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiProductos)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiUnidMed)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiPresentacion)
        Me.RibbonPageGroup14.ItemLinks.Add(Me.bbiVehiculo)
        Me.RibbonPageGroup14.Name = "RibbonPageGroup14"
        Me.RibbonPageGroup14.Text = "Catalogo de Almacen"
        '
        'bbiBodegas
        '
        Me.bbiBodegas.Caption = "Bodegas"
        Me.bbiBodegas.Id = 1
        Me.bbiBodegas.ImageIndex = 0
        Me.bbiBodegas.Name = "bbiBodegas"
        Me.bbiBodegas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiMarcas
        '
        Me.bbiMarcas.Caption = "Marcas"
        Me.bbiMarcas.Id = 3
        Me.bbiMarcas.ImageIndex = 84
        Me.bbiMarcas.Name = "bbiMarcas"
        Me.bbiMarcas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiModelos
        '
        Me.bbiModelos.Caption = "Modelos"
        Me.bbiModelos.Id = 4
        Me.bbiModelos.ImageIndex = 2
        Me.bbiModelos.Name = "bbiModelos"
        Me.bbiModelos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiDepartamentos
        '
        Me.bbiDepartamentos.Caption = "Lineas"
        Me.bbiDepartamentos.Id = 5
        Me.bbiDepartamentos.ImageIndex = 80
        Me.bbiDepartamentos.Name = "bbiDepartamentos"
        Me.bbiDepartamentos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCategorias
        '
        Me.bbiCategorias.Caption = "Categorias"
        Me.bbiCategorias.Id = 6
        Me.bbiCategorias.ImageIndex = 81
        Me.bbiCategorias.Name = "bbiCategorias"
        Me.bbiCategorias.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiProveedor
        '
        Me.bbiProveedor.Caption = "Proveedores"
        Me.bbiProveedor.Id = 7
        Me.bbiProveedor.ImageIndex = 86
        Me.bbiProveedor.Name = "bbiProveedor"
        Me.bbiProveedor.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCliente
        '
        Me.bbiCliente.Caption = "Clientes"
        Me.bbiCliente.Id = 167
        Me.bbiCliente.ImageIndex = 27
        Me.bbiCliente.Name = "bbiCliente"
        Me.bbiCliente.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiProductos
        '
        Me.bbiProductos.Caption = "Productos y/o Servicios"
        Me.bbiProductos.Id = 2
        Me.bbiProductos.ImageIndex = 85
        Me.bbiProductos.Name = "bbiProductos"
        Me.bbiProductos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiUnidMed
        '
        Me.bbiUnidMed.Caption = "Unidad de Medida"
        Me.bbiUnidMed.Id = 96
        Me.bbiUnidMed.ImageIndex = 78
        Me.bbiUnidMed.Name = "bbiUnidMed"
        Me.bbiUnidMed.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPresentacion
        '
        Me.bbiPresentacion.Caption = "Presentación"
        Me.bbiPresentacion.Id = 97
        Me.bbiPresentacion.ImageIndex = 91
        Me.bbiPresentacion.Name = "bbiPresentacion"
        Me.bbiPresentacion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiVehiculo
        '
        Me.bbiVehiculo.Caption = "Vehiculos y Rutas"
        Me.bbiVehiculo.Id = 164
        Me.bbiVehiculo.ImageIndex = 79
        Me.bbiVehiculo.Name = "bbiVehiculo"
        Me.bbiVehiculo.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup11
        '
        Me.RibbonPageGroup11.ItemLinks.Add(Me.bbiDepart)
        Me.RibbonPageGroup11.ItemLinks.Add(Me.bbiMunic)
        Me.RibbonPageGroup11.ItemLinks.Add(Me.bbiZonDist)
        Me.RibbonPageGroup11.ItemLinks.Add(Me.bbiBarrios)
        Me.RibbonPageGroup11.Name = "RibbonPageGroup11"
        Me.RibbonPageGroup11.Text = "Regiones Geograficas"
        '
        'bbiDepart
        '
        Me.bbiDepart.Caption = "Departamentos"
        Me.bbiDepart.Id = 41
        Me.bbiDepart.Name = "bbiDepart"
        Me.bbiDepart.RibbonStyle = CType((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiMunic
        '
        Me.bbiMunic.Caption = "Municipios"
        Me.bbiMunic.Id = 42
        Me.bbiMunic.Name = "bbiMunic"
        '
        'bbiZonDist
        '
        Me.bbiZonDist.Caption = "Zonas /Distritos"
        Me.bbiZonDist.Id = 43
        Me.bbiZonDist.Name = "bbiZonDist"
        '
        'bbiBarrios
        '
        Me.bbiBarrios.Caption = "Barrios"
        Me.bbiBarrios.Id = 44
        Me.bbiBarrios.ImageIndex = 110
        Me.bbiBarrios.Name = "bbiBarrios"
        Me.bbiBarrios.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonPageGroup29
        '
        Me.RibbonPageGroup29.ItemLinks.Add(Me.bbiCaja)
        Me.RibbonPageGroup29.ItemLinks.Add(Me.bbiTasaCambio)
        Me.RibbonPageGroup29.Name = "RibbonPageGroup29"
        Me.RibbonPageGroup29.Text = "Extras"
        '
        'bbiCaja
        '
        Me.bbiCaja.Caption = "Cajas"
        Me.bbiCaja.Id = 162
        Me.bbiCaja.ImageIndex = 103
        Me.bbiCaja.Name = "bbiCaja"
        Me.bbiCaja.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiTasaCambio
        '
        Me.bbiTasaCambio.Caption = "T/C"
        Me.bbiTasaCambio.Id = 163
        Me.bbiTasaCambio.ImageIndex = 107
        Me.bbiTasaCambio.Name = "bbiTasaCambio"
        Me.bbiTasaCambio.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiSuministros
        '
        Me.bbiSuministros.Caption = "Suministros"
        Me.bbiSuministros.Id = 45
        Me.bbiSuministros.ImageIndex = 8
        Me.bbiSuministros.Name = "bbiSuministros"
        Me.bbiSuministros.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiActivosFijos
        '
        Me.bbiActivosFijos.Caption = "Activos Fijos"
        Me.bbiActivosFijos.Id = 46
        Me.bbiActivosFijos.ImageIndex = 9
        Me.bbiActivosFijos.Name = "bbiActivosFijos"
        Me.bbiActivosFijos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiTipoGastos
        '
        Me.bbiTipoGastos.Caption = "Tipos de Gastos"
        Me.bbiTipoGastos.Id = 53
        Me.bbiTipoGastos.ImageIndex = 11
        Me.bbiTipoGastos.Name = "bbiTipoGastos"
        Me.bbiTipoGastos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCajas
        '
        Me.bbiCajas.Caption = "Cajas"
        Me.bbiCajas.Id = 124
        Me.bbiCajas.Name = "bbiCajas"
        Me.bbiCajas.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiTerminales
        '
        Me.bbiTerminales.Caption = "Configuración"
        Me.bbiTerminales.Id = 54
        Me.bbiTerminales.Name = "bbiTerminales"
        '
        'rpPrincipal
        '
        Me.rpPrincipal.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup26, Me.bbiRefrescar, Me.rpgSaveDocument, Me.rpgTaskDocument})
        Me.rpPrincipal.Name = "rpPrincipal"
        Me.rpPrincipal.Text = "Principal"
        '
        'RibbonPageGroup26
        '
        Me.RibbonPageGroup26.ItemLinks.Add(Me.bbiActualizar, "AC")
        Me.RibbonPageGroup26.KeyTip = "SI"
        Me.RibbonPageGroup26.Name = "RibbonPageGroup26"
        Me.RibbonPageGroup26.Text = "Sincronizar"
        Me.RibbonPageGroup26.Visible = False
        '
        'bbiActualizar
        '
        Me.bbiActualizar.Caption = "Actualizar"
        Me.bbiActualizar.Id = 76
        Me.bbiActualizar.ImageIndex = 65
        Me.bbiActualizar.Name = "bbiActualizar"
        Me.bbiActualizar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiRefrescar
        '
        Me.bbiRefrescar.ItemLinks.Add(Me.bbiNuevo, "N")
        Me.bbiRefrescar.ItemLinks.Add(Me.bbiModificar, "M")
        Me.bbiRefrescar.ItemLinks.Add(Me.bbiEliminar, "E")
        Me.bbiRefrescar.KeyTip = "OP"
        Me.bbiRefrescar.Name = "bbiRefrescar"
        Me.bbiRefrescar.Text = "Opciones de Edicion"
        '
        'bbiNuevo
        '
        Me.bbiNuevo.Caption = "&Nuevo"
        Me.bbiNuevo.Id = 98
        Me.bbiNuevo.ImageIndex = 66
        Me.bbiNuevo.Name = "bbiNuevo"
        Me.bbiNuevo.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiModificar
        '
        Me.bbiModificar.Caption = "&Modificar"
        Me.bbiModificar.Id = 99
        Me.bbiModificar.ImageIndex = 68
        Me.bbiModificar.Name = "bbiModificar"
        Me.bbiModificar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiEliminar
        '
        Me.bbiEliminar.Caption = "&Eliminar"
        Me.bbiEliminar.Id = 100
        Me.bbiEliminar.ImageIndex = 76
        Me.bbiEliminar.Name = "bbiEliminar"
        Me.bbiEliminar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpgSaveDocument
        '
        Me.rpgSaveDocument.ItemLinks.Add(Me.bbiGuardar, "G")
        Me.rpgSaveDocument.ItemLinks.Add(Me.bbiCancelar, "C")
        Me.rpgSaveDocument.KeyTip = "AI"
        Me.rpgSaveDocument.Name = "rpgSaveDocument"
        Me.rpgSaveDocument.Text = "Acciones a Realizar"
        '
        'bbiGuardar
        '
        Me.bbiGuardar.Caption = "&Guardar"
        Me.bbiGuardar.Enabled = False
        Me.bbiGuardar.Id = 101
        Me.bbiGuardar.ImageIndex = 71
        Me.bbiGuardar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G))
        Me.bbiGuardar.Name = "bbiGuardar"
        Me.bbiGuardar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiGuardar.ShortcutKeyDisplayString = "Crl+G"
        '
        'bbiCancelar
        '
        Me.bbiCancelar.Caption = "&Cancelar"
        Me.bbiCancelar.Enabled = False
        Me.bbiCancelar.Id = 102
        Me.bbiCancelar.ImageIndex = 72
        Me.bbiCancelar.Name = "bbiCancelar"
        Me.bbiCancelar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpgTaskDocument
        '
        Me.rpgTaskDocument.ItemLinks.Add(Me.bbiBuscar, "B")
        Me.rpgTaskDocument.ItemLinks.Add(Me.bbiImprimir, "I")
        Me.rpgTaskDocument.ItemLinks.Add(Me.bbiExportar, "R")
        Me.rpgTaskDocument.ItemLinks.Add(Me.bbiSesion)
        Me.rpgTaskDocument.ItemLinks.Add(Me.bbiCerrar)
        Me.rpgTaskDocument.KeyTip = "OT"
        Me.rpgTaskDocument.Name = "rpgTaskDocument"
        Me.rpgTaskDocument.Text = "Otras Opciones"
        '
        'bbiBuscar
        '
        Me.bbiBuscar.Caption = "&Buscar"
        Me.bbiBuscar.Id = 103
        Me.bbiBuscar.ImageIndex = 69
        Me.bbiBuscar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B))
        Me.bbiBuscar.Name = "bbiBuscar"
        Me.bbiBuscar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiImprimir
        '
        Me.bbiImprimir.Caption = "Impresion"
        Me.bbiImprimir.Id = 165
        Me.bbiImprimir.ImageIndex = 13
        Me.bbiImprimir.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P))
        Me.bbiImprimir.Name = "bbiImprimir"
        Me.bbiImprimir.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiExportar
        '
        Me.bbiExportar.Caption = "Reportes"
        Me.bbiExportar.Id = 104
        Me.bbiExportar.ImageIndex = 73
        Me.bbiExportar.Name = "bbiExportar"
        Me.bbiExportar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiSesion
        '
        Me.bbiSesion.Caption = "Cerrar Sesion"
        Me.bbiSesion.Id = 177
        Me.bbiSesion.ImageIndex = 113
        Me.bbiSesion.ImageIndexDisabled = 1
        Me.bbiSesion.Name = "bbiSesion"
        Me.bbiSesion.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCerrar
        '
        Me.bbiCerrar.Caption = "Salir"
        Me.bbiCerrar.Id = 106
        Me.bbiCerrar.ImageIndex = 77
        Me.bbiCerrar.Name = "bbiCerrar"
        Me.bbiCerrar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"})
        Me.RepositoryItemComboBox1.LookAndFeel.SkinName = "Office 2010 Silver"
        Me.RepositoryItemComboBox1.LookAndFeel.UseWindowsXPTheme = True
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'RepositoryItemComboBox2
        '
        Me.RepositoryItemComboBox2.AutoHeight = False
        Me.RepositoryItemComboBox2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox2.LookAndFeel.SkinName = "Office 2010 Silver"
        Me.RepositoryItemComboBox2.LookAndFeel.UseWindowsXPTheme = True
        Me.RepositoryItemComboBox2.Name = "RepositoryItemComboBox2"
        '
        'RepositoryItemComboBox3
        '
        Me.RepositoryItemComboBox3.AutoHeight = False
        Me.RepositoryItemComboBox3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox3.LookAndFeel.SkinName = "Office 2010 Silver"
        Me.RepositoryItemComboBox3.LookAndFeel.UseWindowsXPTheme = True
        Me.RepositoryItemComboBox3.Name = "RepositoryItemComboBox3"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemSpinEdit1.LookAndFeel.SkinName = "Office 2010 Silver"
        Me.RepositoryItemSpinEdit1.LookAndFeel.UseWindowsXPTheme = True
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        '
        'bbiLlamadTelef
        '
        Me.bbiLlamadTelef.Caption = "Llamadas Nacionales"
        Me.bbiLlamadTelef.Id = 9
        Me.bbiLlamadTelef.ImageIndex = 15
        Me.bbiLlamadTelef.Name = "bbiLlamadTelef"
        Me.bbiLlamadTelef.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiRecargaTelef
        '
        Me.bbiRecargaTelef.Caption = "Recargas"
        Me.bbiRecargaTelef.Id = 10
        Me.bbiRecargaTelef.ImageIndex = 17
        Me.bbiRecargaTelef.Name = "bbiRecargaTelef"
        Me.bbiRecargaTelef.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiOtrosServicios
        '
        Me.bbiOtrosServicios.Caption = "Otros Servicios"
        Me.bbiOtrosServicios.Id = 11
        Me.bbiOtrosServicios.ImageIndex = 18
        Me.bbiOtrosServicios.Name = "bbiOtrosServicios"
        Me.bbiOtrosServicios.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem9
        '
        Me.BarButtonItem9.Caption = "BarButtonItem9"
        Me.BarButtonItem9.Id = 15
        Me.BarButtonItem9.Name = "BarButtonItem9"
        '
        'BarButtonItem10
        '
        Me.BarButtonItem10.Caption = "BarButtonItem10"
        Me.BarButtonItem10.Id = 16
        Me.BarButtonItem10.Name = "BarButtonItem10"
        '
        'BarButtonItem11
        '
        Me.BarButtonItem11.Caption = "BarButtonItem11"
        Me.BarButtonItem11.Id = 17
        Me.BarButtonItem11.Name = "BarButtonItem11"
        '
        'BarButtonItem12
        '
        Me.BarButtonItem12.Caption = "BarButtonItem12"
        Me.BarButtonItem12.Id = 18
        Me.BarButtonItem12.Name = "BarButtonItem12"
        '
        'BarButtonItem14
        '
        Me.BarButtonItem14.Caption = "BarButtonItem14"
        Me.BarButtonItem14.Id = 20
        Me.BarButtonItem14.Name = "BarButtonItem14"
        '
        'bbiCompraProd
        '
        Me.bbiCompraProd.Caption = "Compra de Productos"
        Me.bbiCompraProd.Id = 22
        Me.bbiCompraProd.ImageIndex = 20
        Me.bbiCompraProd.Name = "bbiCompraProd"
        Me.bbiCompraProd.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPrestxCobrar
        '
        Me.bbiPrestxCobrar.Caption = "Préstamos"
        Me.bbiPrestxCobrar.Id = 25
        Me.bbiPrestxCobrar.Name = "bbiPrestxCobrar"
        Me.bbiPrestxCobrar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPrestxPagar
        '
        Me.bbiPrestxPagar.Caption = "Préstamos"
        Me.bbiPrestxPagar.Id = 26
        Me.bbiPrestxPagar.Name = "bbiPrestxPagar"
        Me.bbiPrestxPagar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCredxCobrar
        '
        Me.bbiCredxCobrar.Caption = "Créditos"
        Me.bbiCredxCobrar.Id = 27
        Me.bbiCredxCobrar.Name = "bbiCredxCobrar"
        Me.bbiCredxCobrar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiCredtxCobrar
        '
        Me.bbiCredtxCobrar.Caption = "Créditos"
        Me.bbiCredtxCobrar.Id = 28
        Me.bbiCredtxCobrar.Name = "bbiCredtxCobrar"
        Me.bbiCredtxCobrar.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiEstadIngreso
        '
        Me.bbiEstadIngreso.Caption = "Balanza de Comprobación"
        Me.bbiEstadIngreso.Id = 31
        Me.bbiEstadIngreso.Name = "bbiEstadIngreso"
        Me.bbiEstadIngreso.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiEstadIngreso.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 34
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "BarButtonItem2"
        Me.BarButtonItem2.Id = 35
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "BarButtonItem3"
        Me.BarButtonItem3.Id = 36
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "BarButtonItem4"
        Me.BarButtonItem4.Id = 37
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "BarButtonItem5"
        Me.BarButtonItem5.Id = 38
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "BarButtonItem6"
        Me.BarButtonItem6.Id = 39
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "BarButtonItem7"
        Me.BarButtonItem7.Id = 40
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'bbiLlamadInternac
        '
        Me.bbiLlamadInternac.Caption = "Llamadas Internacionales"
        Me.bbiLlamadInternac.Id = 59
        Me.bbiLlamadInternac.ImageIndex = 16
        Me.bbiLlamadInternac.Name = "bbiLlamadInternac"
        Me.bbiLlamadInternac.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonGroup1
        '
        Me.BarButtonGroup1.Caption = "BarButtonGroup1"
        Me.BarButtonGroup1.Id = 61
        Me.BarButtonGroup1.ItemLinks.Add(Me.BarButtonItem8)
        Me.BarButtonGroup1.Name = "BarButtonGroup1"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "BarButtonItem8"
        Me.BarButtonItem8.Id = 62
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'BarButtonItem13
        '
        Me.BarButtonItem13.Caption = "BarButtonItem13"
        Me.BarButtonItem13.Id = 63
        Me.BarButtonItem13.Name = "BarButtonItem13"
        '
        'BarButtonItem15
        '
        Me.BarButtonItem15.Caption = "BarButtonItem15"
        Me.BarButtonItem15.Id = 64
        Me.BarButtonItem15.Name = "BarButtonItem15"
        '
        'bbiCatalogo
        '
        Me.bbiCatalogo.Caption = "Estados Financieros"
        Me.bbiCatalogo.Id = 65
        Me.bbiCatalogo.Name = "bbiCatalogo"
        Me.bbiCatalogo.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'RibbonControl1
        '
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Images = Me.ImageList1
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.bbiBodegas, Me.bbiProductos, Me.bbiMarcas, Me.bbiModelos, Me.bbiDepartamentos, Me.bbiCategorias, Me.bbiProveedor, Me.bbiLlamadTelef, Me.bbiRecargaTelef, Me.bbiOtrosServicios, Me.bbiFacturacion, Me.bbiPedidos, Me.bbiCotizacion, Me.BarButtonItem9, Me.BarButtonItem10, Me.BarButtonItem11, Me.BarButtonItem12, Me.bbiDevolucion, Me.BarButtonItem14, Me.bbiCompraProd, Me.bbiCompraLocales, Me.bbiDevolProv, Me.bbiPrestxCobrar, Me.bbiPrestxPagar, Me.bbiCredxCobrar, Me.bbiCredtxCobrar, Me.bbiHacerCierre, Me.bbiEditCatalogo, Me.bbiEstadIngreso, Me.bbiPlantillasCont, Me.BarButtonItem1, Me.BarButtonItem2, Me.BarButtonItem3, Me.BarButtonItem4, Me.BarButtonItem5, Me.BarButtonItem6, Me.BarButtonItem7, Me.bbiDepart, Me.bbiMunic, Me.bbiZonDist, Me.bbiBarrios, Me.bbiSuministros, Me.bbiActivosFijos, Me.bbiEntradas, Me.bbiSalidas, Me.bbiTransferencia, Me.bbiControlKardex, Me.bbiRegGastos, Me.bbiTipoGastos, Me.bbiTerminales, Me.bbiLlamadInternac, Me.BarButtonGroup1, Me.BarButtonItem8, Me.BarButtonItem13, Me.BarButtonItem15, Me.bbiCatalogo, Me.bbiEditComprob, Me.bbiArqueo, Me.bbiInformes, Me.bbiPedidosProv, Me.bbiControlPedidos, Me.bbiInformeLogistica, Me.bbiComprasForaneas, Me.bbiInfoAlmacen, Me.bbiActualizar, Me.bbiExpedEmpleado, Me.bbiConveniosFinan, Me.bbiPrestamosPers, Me.bbiOtrasDeducciones, Me.bbiOtrosIngresos, Me.bbiControlHorasExtras, Me.bbiComisiones, Me.BarButtonItem26, Me.bbiControlVacaciones, Me.bbiGenerarPlanilla, Me.bbiLiquidacionesPers, Me.bbiVerMovCuenta, Me.bbiIngresosPagos, Me.bbiIngresosNotaCred, Me.bbiIngresosNotaDebitos, Me.bbiLlamadasyProgram, Me.bbiRptCartera, Me.bbiProgramPagos, Me.bbiOrdenesPago, Me.bbiUnidMed, Me.bbiPresentacion, Me.bbiNuevo, Me.bbiModificar, Me.bbiEliminar, Me.bbiGuardar, Me.bbiCancelar, Me.bbiBuscar, Me.bbiExportar, Me.bbiCerrar, Me.bbiSolicitudesCheque, Me.bbiOrdenPago, Me.bbiGenerarCheques, Me.BarButtonItem16, Me.BarButtonItem17, Me.BarButtonItem18, Me.BarButtonItem19, Me.BarButtonItem20, Me.BarButtonItem21, Me.BarButtonItem22, Me.BarButtonItem23, Me.BarButtonItem24, Me.BarButtonItem25, Me.BarButtonItem27, Me.BarButtonItem28, Me.bbiReciboManual, Me.bbiAplicarAnticipo, Me.bbiCajas, Me.btnRecetasProduccion, Me.btnOrdenProd, Me.bbiProyeccion, Me.BarButtonItem32, Me.bbiCOrden, Me.BarButtonItem34, Me.btnRemision, Me.bbiFcliente, Me.bbiUsuarios, Me.bbiPermisos, Me.bbiRfactura, Me.bbiRinventario, Me.bbiCaja, Me.bbiTasaCambio, Me.bbiVehiculo, Me.bbiImprimir, Me.bbiFendose, Me.bbiCliente, Me.bbiFverificacion, Me.btnGarantia, Me.bbiSesion, Me.bbiKit})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.RibbonControl1.MaxItemId = 179
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.rpPrincipal, Me.rpCatalogos, Me.rpAdminLog, Me.rpProducción, Me.rpInventory, Me.rpCentroSol, Me.rpCartera, Me.rpTesoreria, Me.rpRecursosHum, Me.rpContab, Me.rpAdministracion, Me.rpReportes})
        Me.RibbonControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1, Me.RepositoryItemComboBox2, Me.RepositoryItemComboBox3, Me.RepositoryItemSpinEdit1})
        Me.RibbonControl1.Size = New System.Drawing.Size(794, 144)
        Me.RibbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
        '
        'bbiUsuarios
        '
        Me.bbiUsuarios.Caption = "Usuarios y Perfiles"
        Me.bbiUsuarios.Id = 152
        Me.bbiUsuarios.ImageIndex = 105
        Me.bbiUsuarios.Name = "bbiUsuarios"
        Me.bbiUsuarios.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiPermisos
        '
        Me.bbiPermisos.Caption = "Permisos"
        Me.bbiPermisos.Id = 153
        Me.bbiPermisos.ImageIndex = 62
        Me.bbiPermisos.Name = "bbiPermisos"
        Me.bbiPermisos.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        Me.bbiPermisos.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'bbiRfactura
        '
        Me.bbiRfactura.Caption = "Facturas"
        Me.bbiRfactura.Id = 155
        Me.bbiRfactura.ImageIndex = 103
        Me.bbiRfactura.Name = "bbiRfactura"
        Me.bbiRfactura.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bbiRinventario
        '
        Me.bbiRinventario.Caption = "Inventario"
        Me.bbiRinventario.Id = 157
        Me.bbiRinventario.ImageIndex = 85
        Me.bbiRinventario.Name = "bbiRinventario"
        Me.bbiRinventario.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'rpAdministracion
        '
        Me.rpAdministracion.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup12})
        Me.rpAdministracion.Name = "rpAdministracion"
        Me.rpAdministracion.Text = "Administracion"
        '
        'RibbonPageGroup12
        '
        Me.RibbonPageGroup12.ItemLinks.Add(Me.bbiUsuarios)
        Me.RibbonPageGroup12.ItemLinks.Add(Me.bbiPermisos)
        Me.RibbonPageGroup12.Name = "RibbonPageGroup12"
        Me.RibbonPageGroup12.Text = "Seguridad"
        '
        'rpReportes
        '
        Me.rpReportes.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup17})
        Me.rpReportes.Name = "rpReportes"
        Me.rpReportes.Text = "Reportes"
        '
        'RibbonPageGroup17
        '
        Me.RibbonPageGroup17.ItemLinks.Add(Me.bbiRfactura)
        Me.RibbonPageGroup17.ItemLinks.Add(Me.bbiRinventario)
        Me.RibbonPageGroup17.Name = "RibbonPageGroup17"
        '
        'BarButtonItem33
        '
        Me.BarButtonItem33.Caption = "Cotizaciones"
        Me.BarButtonItem33.Id = 14
        Me.BarButtonItem33.ImageIndex = 13
        Me.BarButtonItem33.Name = "BarButtonItem33"
        Me.BarButtonItem33.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem36
        '
        Me.BarButtonItem36.Caption = "Facturación"
        Me.BarButtonItem36.Id = 12
        Me.BarButtonItem36.ImageIndex = 103
        Me.BarButtonItem36.Name = "BarButtonItem36"
        Me.BarButtonItem36.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'nbcNotificacion
        '
        Me.nbcNotificacion.ActiveGroup = Me.nbgPendiente
        Me.nbcNotificacion.Appearance.Background.BackColor = System.Drawing.Color.LightSlateGray
        Me.nbcNotificacion.Appearance.Background.Options.UseBackColor = True
        Me.nbcNotificacion.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.nbcNotificacion.ContentButtonHint = Nothing
        Me.nbcNotificacion.Dock = System.Windows.Forms.DockStyle.Right
        Me.nbcNotificacion.GroupBackgroundImage = CType(resources.GetObject("nbcNotificacion.GroupBackgroundImage"), System.Drawing.Image)
        Me.nbcNotificacion.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.nbgPendiente})
        Me.nbcNotificacion.Location = New System.Drawing.Point(782, 144)
        Me.nbcNotificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.nbcNotificacion.Name = "nbcNotificacion"
        Me.nbcNotificacion.OptionsNavPane.ExpandedWidth = 12
        Me.nbcNotificacion.Size = New System.Drawing.Size(12, 202)
        Me.nbcNotificacion.TabIndex = 171
        Me.nbcNotificacion.Text = "NavBarControl1"
        Me.nbcNotificacion.View = New DevExpress.XtraNavBar.ViewInfo.StandardSkinNavigationPaneViewInfoRegistrator("DevExpress Dark Style")
        '
        'nbgPendiente
        '
        Me.nbgPendiente.Caption = "Pendientes"
        Me.nbgPendiente.Expanded = True
        Me.nbgPendiente.Name = "nbgPendiente"
        '
        'BarButtonItem35
        '
        Me.BarButtonItem35.Caption = "Salir"
        Me.BarButtonItem35.Id = 106
        Me.BarButtonItem35.ImageIndex = 77
        Me.BarButtonItem35.Name = "BarButtonItem35"
        Me.BarButtonItem35.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'BarButtonItem37
        '
        Me.BarButtonItem37.Caption = "Salir"
        Me.BarButtonItem37.Id = 106
        Me.BarButtonItem37.ImageIndex = 77
        Me.BarButtonItem37.Name = "BarButtonItem37"
        Me.BarButtonItem37.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(794, 346)
        Me.Controls.Add(Me.StatusBar)
        Me.Controls.Add(Me.nbcNotificacion)
        Me.Controls.Add(Me.RibbonControl1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmPrincipal"
        Me.Text = ".::: SISTEMA INTEGRAL GRUPO MANZONI (SIGMA) :::."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Tbl_ResumenContaBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tbl_ResumenTransBS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusBar.ResumeLayout(False)
        Me.StatusBar.PerformLayout()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nbcNotificacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents bbiReportTrans As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents StatusBar As System.Windows.Forms.StatusStrip
    Friend WithEvents TSProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents TSUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents LblNombreUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSFecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents LblFecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents Tbl_ResumenTransBS As System.Windows.Forms.BindingSource
    Friend WithEvents Tbl_ResumenContaBS As System.Windows.Forms.BindingSource
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TSHora As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents LblHora As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents bbiCompraRecargas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BarButtonItem29 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem30 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem31 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpContab As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup15 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiSolicitudesCheque As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiOrdenPago As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiGenerarCheques As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiEditComprob As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup10 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiEditCatalogo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPlantillasCont As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiHacerCierre As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup13 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiRegGastos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpgInformesFinan As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem16 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem17 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem18 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem19 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem20 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup27 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem21 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem22 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem23 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem25 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem24 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem27 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem28 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpRecursosHum As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiExpedEmpleado As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiControlVacaciones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiLiquidacionesPers As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiGenerarPlanilla As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup20 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiControlHorasExtras As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiComisiones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiOtrosIngresos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup21 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiConveniosFinan As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPrestamosPers As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiOtrasDeducciones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem26 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpTesoreria As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup25 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiProgramPagos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiOrdenesPago As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpCartera As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup22 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiVerMovCuenta As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup23 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiIngresosPagos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiIngresosNotaCred As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiIngresosNotaDebitos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup24 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiLlamadasyProgram As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRptCartera As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpCentroSol As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiFacturacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiDevolucion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiCotizacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPedidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiReciboManual As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiAplicarAnticipo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiArqueo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup16 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiInformes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpInventory As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiEntradas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiSalidas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiTransferencia As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiControlKardex As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup19 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiInfoAlmacen As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpProducción As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup28 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnRecetasProduccion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOrdenProd As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem32 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpAdminLog As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiProyeccion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPedidosProv As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCompraLocales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiComprasForaneas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiDevolProv As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup18 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiControlPedidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiInformeLogistica As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpCatalogos As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup14 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiBodegas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiMarcas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiModelos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiDepartamentos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCategorias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiProveedor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiProductos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiUnidMed As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPresentacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup11 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiDepart As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiMunic As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiZonDist As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiBarrios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiSuministros As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiActivosFijos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiTipoGastos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCajas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiTerminales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpPrincipal As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup26 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiActualizar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRefrescar As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiNuevo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiModificar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiEliminar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpgSaveDocument As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiGuardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCancelar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpgTaskDocument As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiBuscar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiExportar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCerrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemComboBox2 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemComboBox3 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents bbiLlamadTelef As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRecargaTelef As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiOtrosServicios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem9 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem10 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem11 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem12 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem14 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCompraProd As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPrestxCobrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPrestxPagar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCredxCobrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCredtxCobrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiEstadIngreso As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiLlamadInternac As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonGroup1 As DevExpress.XtraBars.BarButtonGroup
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem13 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem15 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiCatalogo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents bbiCOrden As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem34 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnRemision As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiFcliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem33 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TSSucursal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblSucursal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents rpAdministracion As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup12 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiUsuarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiPermisos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRfactura As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rpReportes As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup17 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem36 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiRinventario As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup29 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bbiCaja As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiTasaCambio As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiVehiculo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiImprimir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tsslbProceso As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSProceso As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents bbiFendose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents nbcNotificacion As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents bbiCliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiFverificacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents nbgPendiente As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents RibbonPageGroup30 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnGarantia As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiSesion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem35 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem37 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiKit As DevExpress.XtraBars.BarButtonItem
End Class
