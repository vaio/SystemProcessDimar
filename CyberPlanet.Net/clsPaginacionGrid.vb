﻿
Imports System.Data.SqlClient

Public Class clsPaginacionGrid

    Implements IDisposable

    Dim mconexion As String
    Dim strSql As String
    Dim isWebService As Boolean = False

    Private PgSize As Integer = 20
    Private CurrentPageIndex As Integer = 1
    Private TotalPage As Integer = 0

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

    Public Sub New(ByVal ConextionString As String)
        mconexion = ConextionString
    End Sub

    Public Function Sql(ByVal strSql As String) As DataSet
        Dim cn As New SqlConnection
        cn.ConnectionString = mconexion
        Try
            cn.Open()
            Dim sqlda As New SqlDataAdapter(strSql, cn)
            Dim ds As New DataSet
            sqlda.Fill(ds, "tblResultado")
            Sql = ds
        Catch ex As Exception
            Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Sql = Nothing
        Finally
            cn.Close()
        End Try
    End Function

End Class
