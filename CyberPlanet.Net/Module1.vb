﻿Imports System.Data.SqlClient

Module Module1
    Public nSucursal As Integer = My.Settings.Sucursal
    Public NumCatalogo As Integer
    Public TipoTransaccion As Integer
    Public TipoDocumento As Integer
    Public TipoMovimiento As Integer
    Public nTipoEdic As Integer
    Public nTipoEdicDet As Integer
    Public CodigoEntidad As String
    Public CodigoDoc As String
    Public Nuevo As Integer = 0

    Public nTipoEdic_Ant As Integer = 0
    Public NumCatalogo_Ant As Integer = 0
    Public CatalogoVisible As Boolean = False
    Public TransacVisible As Boolean = False
    Public DocumentosVisibles As Boolean = False
    Public nTerminal As Integer
    Public nTime As Integer
    Public nTipoEdicDoc As Integer
    Public nCodProveedor As Integer
    Public nCodCliente As Integer
    Public nIsCredito As Integer
    Public nProgramed As Integer = 0
    Public nFree As Integer = 0
    Public nOK As Integer = 0
    Public nPend As Integer = 0
    Public bGuardar As Boolean = False
    Public bVerGuardados As Boolean = False
    Public valTemp As String = ""

    Public UserID As String
    Public UserFullName As String
    Public UserName As String
    Public UserPass As String
    Public UserPerfil As String
    Public SucursalName As String
    Public LevelAccess As Integer

    Public itemAdd_Ant As Integer = 0

    Public listProveedores As DataTable
    Public Remitente As String

    Public Function SQL(ByVal strSql As String, ByVal NombreTabla As String, ByVal m_coneccion As String) As DataSet
        'If isWebService = False Then
        Dim exTipo As String = "consultar"
        Dim cnConec As New SqlConnection(m_coneccion)
        Try
            cnConec.Open()
            Dim daAdaptador As New SqlDataAdapter(strSql, cnConec)
            Dim dsSQL As New DataSet
            daAdaptador.Fill(dsSQL, NombreTabla)
            daAdaptador.Dispose()
            SQL = dsSQL
        Catch ex As SqlException
            Dim Repuesta As MsgBoxResult
            Repuesta = MsgBox("No se ha podido " + exTipo + " el registro." _
                                    + vbNewLine + vbNewLine + "<<<<<<Descripcion del error>>>>>>" _
                                    + vbNewLine + vbNewLine + "Er#: " + ex.ErrorCode.ToString _
                                    + vbNewLine + vbNewLine + "Descripcion: " + ex.Message _
                                    , MsgBoxStyle.Critical Or MsgBoxStyle.OkCancel)

            Return Nothing
        Finally
            cnConec.Close()
        End Try
    End Function

    Public Function RegistroMaximo()
        Dim strsql As String = ""
        If NumCatalogo = 1 Then                 'Bodega
            strsql = "SELECT isnull(MAX(Codigo_Bodega)+1,1) AS Maximo FROM Almacenes"
        ElseIf NumCatalogo = 2 Then         'Marca
            strsql = "SELECT isnull(MAX(Codigo_Marca)+1,1) AS Maximo FROM Tbl_Marca"
        ElseIf NumCatalogo = 3 Then         'Modelo
            strsql = "SELECT isnull(MAX(Id_Modelo)+1,1) AS Maximo FROM Tbl_Modelo Where Id_Marca = " + valTemp
        ElseIf NumCatalogo = 4 Then         'Departamento
            strsql = "SELECT isnull(MAX(Codigo_Linea)+1,1) AS Maximo FROM Linea_Producto"
        ElseIf NumCatalogo = 5 Then     'Categoria
            strsql = "SELECT isnull(MAX(Codigo_Categoria)+1,1) AS Maximo FROM Categoria_Producto WHERE Codigo_Linea = " + valTemp
        ElseIf NumCatalogo = 6 Then         'Proveedor
            strsql = "SELECT isnull(MAX(Codigo_Proveedor)+1,1) AS Maximo FROM Tbl_Proveedor"
        ElseIf NumCatalogo = 7 Then         'Proveedor
            strsql = "SELECT isnull(MAX(Codigo_UnidadMed)+1,1) AS Maximo FROM Unidades_Medida"
        ElseIf NumCatalogo = 8 Then         'Proveedor
            strsql = "SELECT isnull(MAX(Codigo_Presentacion)+1,1) AS Maximo FROM Presentacion"
        ElseIf NumCatalogo = 9 Then         'Producto
            strsql = "SELECT isnull(MAX(Codigo_Producto)+1,1) AS Maximo FROM Productos"
        ElseIf NumCatalogo = 10 Then         'Departamentos Pais
            strsql = "SELECT isnull(MAX(Id_Departamento)+1,1) AS Maximo FROM Catalogo.Departamento "
        ElseIf NumCatalogo = 11 Then      'Municipios
            strsql = "SELECT isnull(MAX(Id_Municipio)+1,1) AS Maximo FROM Tbl_Municipio"
        ElseIf NumCatalogo = 12 Then         'Zonas/Distritos
            strsql = "SELECT isnull(MAX(Id_Zona_Distrito)+1,1) AS Maximo FROM Tbl_Zonas_Distritos"
        ElseIf NumCatalogo = 13 Then         'Barrios
            strsql = "SELECT isnull(MAX(Id_Barrio)+1,1) AS Maximo FROM Tbl_Barrios"
        ElseIf NumCatalogo = 14 Then                 'Cliente
            strsql = "SELECT isnull(MAX(Codigo_Cliente)+1,1) AS Maximo FROM Clientes"
        ElseIf NumCatalogo = 15 Then 'Suministro
            strsql = "SELECT isnull(MAX(Id_Producto)+1,1) AS Maximo FROM Tbl_Producto"
        ElseIf NumCatalogo = 16 Then 'Activo Fijo
            strsql = "SELECT isnull(MAX(Id_Producto)+1,1) AS Maximo FROM Tbl_Producto"
        ElseIf NumCatalogo = 17 Then 'Colaboradores
            strsql = "SELECT isnull(MAX(Id_Empleado)+1,1) AS Maximo FROM Tbl_Empleados"
        ElseIf NumCatalogo = 18 Then 'TipoOperaciones
            strsql = "SELECT isnull(MAX(Id_TipoOperacion)+1,1) AS Maximo FROM Tbl_TipoOperaciones"
        ElseIf NumCatalogo = 19 Then 'Terminales
            strsql = "SELECT isnull(MAX(Id_Terminal)+1,1) AS Maximo FROM Tbl_Terminal"
        ElseIf NumCatalogo = 20 Then 'Terminales
            strsql = "SELECT isnull(MAX(Codigo_Reseta)+1,1) AS Maximo FROM Tbl_Recetas"
        ElseIf NumCatalogo = 21 Then
            strsql = "SELECT isnull(MAX(Numero_de_Compra)+1,1) AS Maximo FROM Compras WHERE CodigoSucursal = " & My.Settings.Sucursal
        ElseIf NumCatalogo = 22 Then
            strsql = "SELECT isnull(MAX(Numero_de_Documento)+1,1) AS Maximo FROM Compras_Extranjeras WHERE CodigoSucursal = " & My.Settings.Sucursal
        ElseIf NumCatalogo = 23 Then 'Terminales
            strsql = "SELECT isnull(MAX(Codigo_Orden_Produc)+1,1) AS Maximo FROM Orden_Produccion"
        ElseIf NumCatalogo = 27 Then 'Terminales
            strsql = "SELECT isnull(MAX(Codigo_Proyeccion)+1,1) AS Maximo FROM Proyeccion"
        ElseIf NumCatalogo = 29 Then
            strsql = "SELECT isnull(MAX(Codigo_Cierre)+1,1) AS Maximo FROM Orden_Cierre"
        ElseIf NumCatalogo = 30 Then
            strsql = "SELECT isnull(MAX(Codigo_CIF)+1,1) AS Maximo FROM CIF"
        ElseIf NumCatalogo = 31 Then
            strsql = "SELECT isnull(MAX(Codigo_Servicio)+1,1) AS Maximo FROM Cliente.Servicios WHERE Tipo_Orden = " + valTemp
        ElseIf NumCatalogo = 38 Then
            strsql = "SELECT isnull(MAX(Codigo_Pedido)+1,1) AS Maximo FROM Orden_Pedido"
        ElseIf NumCatalogo = 39 Then
            strsql = "SELECT isnull(MAX(Codigo_Traslado)+1,1) AS Maximo FROM Orden_Traslado"
        End If
        Dim tblRegMax As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)
        RegistroMaximo = tblRegMax.Rows(0).Item(0)
    End Function
    Public Function SiguienteDocumentoInv()
        Dim strsql As String = ""
        'Entrada, Salida, Factura, Compra, Transferencias, Recibos
        strsql = "SELECT (NumInicio + 1) AS Maximo FROM Tbl_ControlDocumentos where Id_TipoDocumento = " & TipoDocumento
        Dim tblNextDoc As DataTable = SQL(strsql, "tblSiguienteDoc", My.Settings.SolIndustrialCNX).Tables(0)
        SiguienteDocumentoInv = tblNextDoc.Rows(0).Item(0)
    End Function

    Public Function SerieDocumentoInv()
        Dim strsql As String = ""
        'Entrada, Salida, Factura, Compra, Transferencias, Recibos
        strsql = "SELECT SerieDocumento FROM Tbl_ControlDocumentos where Id_TipoDocumento = " & TipoDocumento
        Dim tblSerieDoc As DataTable = SQL(strsql, "tblSerieDoc", My.Settings.SolIndustrialCNX).Tables(0)
        SerieDocumentoInv = tblSerieDoc.Rows(0).Item(0)
    End Function

    Public Function SiguienteTransaccionDiario()
        Dim strsql As String = ""
        'Transacciones de Libro Diario
        strsql = "SELECT isnull(MAX(Id_TransaccionDiario)+1,1) AS Maximo FROM Tbl_MasterComprobanteDiario"
        Dim tblNextTrans As DataTable = SQL(strsql, "tblSigTransDiario", My.Settings.SolIndustrialCNX).Tables(0)
        SiguienteTransaccionDiario = tblNextTrans.Rows(0).Item(0)
    End Function
    Public Function ChangeToDouble(ByVal Str As String)
        Try
            If Str = Nothing Then

                ChangeToDouble = 0

            ElseIf Str = "." Then

                ChangeToDouble = "0."

            Else

                ChangeToDouble = CDbl(Str)

            End If
          
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Function

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

End Module