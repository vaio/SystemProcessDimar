﻿
Imports System.Data.SqlClient

Public Class clsProduccion

    Implements IDisposable

    Dim mconexion As String
    Dim strSql As String
    Dim isWebService As Boolean = False

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

    Public Sub New(ByVal ConextionString As String)
        mconexion = ConextionString
    End Sub

    Public Function Sql(ByVal strSql As String) As DataSet
        Dim cn As New SqlConnection
        cn.ConnectionString = mconexion
        Try
            cn.Open()
            Dim sqlda As New SqlDataAdapter(strSql, cn)
            Dim ds As New DataSet
            sqlda.Fill(ds, "tblNotas")
            Sql = ds
        Catch ex As Exception
            Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Sql = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    Public Function EditarRecetaEncabezado(ByVal CodReceta As String, ByVal CodProducto As String, ByVal Descripcion As String, ByVal Activo As Boolean, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdReceta As New SqlCommand("SP_AME_Receta", cnConec)
            cmdReceta.CommandType = CommandType.StoredProcedure
            cmdReceta.Parameters.Add("@CodReceta", SqlDbType.VarChar).Value = CodReceta
            cmdReceta.Parameters.Add("@CodProducto", SqlDbType.VarChar).Value = CodProducto
            cmdReceta.Parameters.Add("@Descripcion", SqlDbType.NVarChar).Value = Descripcion
            cmdReceta.Parameters.Add("@Activo", SqlDbType.Bit).Value = Activo
            cmdReceta.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdReceta.ExecuteReader()

            EditarRecetaEncabezado = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarRecetaEncabezado = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarRecetaDetalle(ByVal CodReceta As String, ByVal CodProducto As String, ByVal Cantidad As Double, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdReceta As New SqlCommand("SP_AME_Receta_Detalle", cnConec)
            cmdReceta.CommandType = CommandType.StoredProcedure
            cmdReceta.Parameters.Add("@CodReceta", SqlDbType.VarChar).Value = CodReceta
            cmdReceta.Parameters.Add("@CodProducto", SqlDbType.VarChar).Value = CodProducto
            cmdReceta.Parameters.Add("@Cantidad", SqlDbType.Float).Value = Cantidad
            cmdReceta.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdReceta.ExecuteReader()

            EditarRecetaDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarRecetaDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    'Public Function GetRecetaDetalle(ByVal CodigoProducto As String)
    '    Dim ID As String = ""
    '    Dim cnConec As New SqlConnection
    '    Dim data As DataTable = New DataTable()

    '    With cnConec
    '        .ConnectionString = mconexion
    '        .Open()
    '    End With

    '    Try
    '        Dim reader As SqlDataReader
    '        Dim cmdDetalle As New SqlCommand("[dbo].[SP_GET_Receta_MateriaPrima]", cnConec)
    '        cmdDetalle.CommandType = CommandType.StoredProcedure
    '        cmdDetalle.Parameters.Add("@CodigoProducto", SqlDbType.VarChar).Value = CodigoProducto

    '        reader = cmdDetalle.ExecuteReader()
    '        data.Load(reader)

    '        GetRecetaDetalle = "OK"

    '    Catch ex As SqlException
    '        MsgBox("Error: " + ex.Message)
    '        GetRecetaDetalle = "ERROR"
    '    Finally
    '        cnConec.Close()
    '        cnConec.Dispose()
    '    End Try
    'End Function

    Public Function EditarOrdenProduccion(ByVal Sucursal As Integer, ByVal CodigoOrden As String, ByVal IdSolicitante As Integer, ByVal CodigoProducto As String,
                                          ByVal CantRequerida As Integer, ByVal CostoRequerido As Double, ByVal CantElaborada As Integer, ByVal CostoElaborado As Double,
                                          ByVal Registrado As Date, ByVal Cerrado As Date?, ByVal Estado As Integer, ByVal Autorizado As Boolean, ByVal Activo As Boolean,
                                          ByVal TipoEdicion As Integer, ByVal Usuario As String)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_OrdenProduccion", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.VarChar).Value = Sucursal
            cmdOrden.Parameters.Add("@CodigoOrden", SqlDbType.VarChar).Value = CodigoOrden
            cmdOrden.Parameters.Add("@CodigoProducto", SqlDbType.VarChar).Value = CodigoProducto
            cmdOrden.Parameters.Add("@IdSolicitante", SqlDbType.Int).Value = IdSolicitante
            cmdOrden.Parameters.Add("@CantRequerida", SqlDbType.Int).Value = CantRequerida
            cmdOrden.Parameters.Add("@CostoRequerido", SqlDbType.Money).Value = CostoRequerido
            cmdOrden.Parameters.Add("@CantElaborada", SqlDbType.Int).Value = CantElaborada
            cmdOrden.Parameters.Add("@CostoElaborado", SqlDbType.Money).Value = CostoElaborado
            cmdOrden.Parameters.Add("@Registrado", SqlDbType.DateTime).Value = Registrado
            If Cerrado Is Nothing Then
                cmdOrden.Parameters.Add("@Cerrado", SqlDbType.DateTime).Value = DBNull.Value
            Else
                cmdOrden.Parameters.Add("@Cerrado", SqlDbType.DateTime).Value = Cerrado
            End If
            cmdOrden.Parameters.Add("@Estado", SqlDbType.Int).Value = Estado
            cmdOrden.Parameters.Add("@Autorizado", SqlDbType.Bit).Value = Autorizado
            cmdOrden.Parameters.Add("@Activo", SqlDbType.Bit).Value = Activo
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdOrden.Parameters.Add("@Usuario", SqlDbType.VarChar).Value = Usuario
            cmdOrden.ExecuteReader()

            EditarOrdenProduccion = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarOrdenProduccion = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarOrdenDetalle(ByVal Sucursal As Integer, ByVal CodigoOrden As String, ByVal CodigoProductoMaster As String, ByVal CodigoProducto As String,
                                       ByVal CantRequerida As Double, ByVal CantNoUtilizada As Double, ByVal Costo_Unitario As Double,
                                       ByVal EsAdicional As Boolean, ByVal Autorizado As Boolean, ByVal TipoEdicion As Integer, Sustituto As String)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_OrdenProduccion_Detalle", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.VarChar).Value = Sucursal
            cmdOrden.Parameters.Add("@CodigoOrden", SqlDbType.VarChar).Value = CodigoOrden
            cmdOrden.Parameters.Add("@CodigoProductoMaster", SqlDbType.VarChar).Value = CodigoProductoMaster
            cmdOrden.Parameters.Add("@CodigoProductoDetalle", SqlDbType.VarChar).Value = CodigoProducto
            cmdOrden.Parameters.Add("@CantRequerida", SqlDbType.Money).Value = CantRequerida
            cmdOrden.Parameters.Add("@CantNoUtilizada", SqlDbType.Money).Value = CantNoUtilizada
            cmdOrden.Parameters.Add("@Costo_Unitario", SqlDbType.Money).Value = Costo_Unitario
            cmdOrden.Parameters.Add("@EsAdicional", SqlDbType.Bit).Value = EsAdicional
            cmdOrden.Parameters.Add("@Autorizado", SqlDbType.Bit).Value = Autorizado
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdOrden.Parameters.Add("@sustituto", SqlDbType.VarChar).Value = Sustituto
            cmdOrden.ExecuteReader()

            EditarOrdenDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarOrdenDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarCierre(ByVal Codigo_Cierre As String, ByVal Anio As Integer, ByVal Mes As Integer,
                                   ByVal Dia As Integer, ByVal Cantidad_Ordenes As Integer, ByVal Monto_Cierre As Double,
                                   ByVal CIF_MOD As Double, ByVal Fecha_Ordenes As Date?, ByVal Ajustado As Boolean, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_Orden_Cierre", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Codigo_Cierre", SqlDbType.VarChar).Value = Codigo_Cierre
            cmdOrden.Parameters.Add("@Anio", SqlDbType.Int).Value = Anio
            cmdOrden.Parameters.Add("@Mes", SqlDbType.Int).Value = Mes
            cmdOrden.Parameters.Add("@Dia", SqlDbType.Int).Value = Dia
            cmdOrden.Parameters.Add("@Cantidad_Ordenes", SqlDbType.Int).Value = Cantidad_Ordenes
            cmdOrden.Parameters.Add("@Monto_Cierre", SqlDbType.Decimal).Value = Monto_Cierre
            cmdOrden.Parameters.Add("@CIF_MOD", SqlDbType.Decimal).Value = CIF_MOD

            If Fecha_Ordenes Is Nothing Then
                cmdOrden.Parameters.Add("@Fecha_Ordenes", SqlDbType.DateTime).Value = DBNull.Value
            Else
                cmdOrden.Parameters.Add("@Fecha_Ordenes", SqlDbType.DateTime).Value = Fecha_Ordenes
            End If

            cmdOrden.Parameters.Add("@Ajustado", SqlDbType.Bit).Value = Ajustado

            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdOrden.ExecuteReader()

            EditarCierre = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCierre = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarCierreDetalle(ByVal Codigo_Cierre As String, ByVal Anio As Integer, ByVal Mes As Integer,
                                   ByVal Dia As Integer, ByVal Orden As String, ByVal Monto As Double, ByVal Porc_CIF As Double,
                                   ByVal Orden_CIF As Double, ByVal Ajuste_CIF As Double, ByVal Costo_Total As Double,
                                   ByVal Activo As Boolean, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_Orden_Cierre_Detalle", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Codigo_Cierre", SqlDbType.VarChar).Value = Codigo_Cierre
            cmdOrden.Parameters.Add("@Anio", SqlDbType.Int).Value = Anio
            cmdOrden.Parameters.Add("@Mes", SqlDbType.Int).Value = Mes
            cmdOrden.Parameters.Add("@Dia", SqlDbType.Int).Value = Dia
            cmdOrden.Parameters.Add("@Orden", SqlDbType.VarChar).Value = Orden

            cmdOrden.Parameters.Add("@Monto", SqlDbType.Decimal).Value = Monto
            cmdOrden.Parameters.Add("@Porc_CIF", SqlDbType.Decimal).Value = Porc_CIF
            cmdOrden.Parameters.Add("@Orden_CIF", SqlDbType.Decimal).Value = Orden_CIF
            cmdOrden.Parameters.Add("@Ajuste_CIF", SqlDbType.Decimal).Value = Ajuste_CIF
            cmdOrden.Parameters.Add("@Costo_Total", SqlDbType.Decimal).Value = Costo_Total

            cmdOrden.Parameters.Add("@Activo", SqlDbType.Bit).Value = Activo

            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdOrden.ExecuteReader()

            EditarCierreDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCierreDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarCif(ByVal Codigo_CIF As String, ByVal Anio As Integer, ByVal Mes As Integer, ByVal Descripcion As String,
                              ByVal Inicial As Double, ByVal Final As Double, ByVal Valido As Boolean, ByVal nTipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_CIF", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Codigo_CIF", SqlDbType.VarChar).Value = Codigo_CIF
            cmdOrden.Parameters.Add("@Mes", SqlDbType.Int).Value = Mes
            cmdOrden.Parameters.Add("@Anio", SqlDbType.Int).Value = Anio
            cmdOrden.Parameters.Add("@Descripcion", SqlDbType.NVarChar).Value = Descripcion

            cmdOrden.Parameters.Add("@Inicial", SqlDbType.Decimal).Value = Inicial
            cmdOrden.Parameters.Add("@Final", SqlDbType.Decimal).Value = Final
            cmdOrden.Parameters.Add("@Valido", SqlDbType.Bit).Value = Valido

            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.ExecuteReader()

            EditarCif = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCif = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarCifDetalle(ByVal Codigo_CIF As String, ByVal Anio As Integer, ByVal Mes As Integer, ByVal Tipo_CIF As Integer,
                                     ByVal Monto_Inicial As Double, ByVal Monto_Final As Double, ByVal Nota As String, ByVal Anulado As Boolean, ByVal nTipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_CIF_Detalle", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Codigo_CIF", SqlDbType.VarChar).Value = Codigo_CIF
            cmdOrden.Parameters.Add("@Mes", SqlDbType.Int).Value = Mes
            cmdOrden.Parameters.Add("@Anio", SqlDbType.Int).Value = Anio
            cmdOrden.Parameters.Add("@Tipo_CIF", SqlDbType.Int).Value = Tipo_CIF

            cmdOrden.Parameters.Add("@Monto_Inicial", SqlDbType.Decimal).Value = Monto_Inicial
            cmdOrden.Parameters.Add("@Monto_Final", SqlDbType.Decimal).Value = Monto_Final
            cmdOrden.Parameters.Add("@Nota", SqlDbType.NVarChar).Value = Nota

            cmdOrden.Parameters.Add("@Anulado", SqlDbType.Bit).Value = Anulado

            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.ExecuteReader()

            EditarCifDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCifDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function AjusteCierre(ByVal Sucursal As Integer, ByVal Anio As Integer, ByVal Mes As Integer, ByVal Dia As Integer, ByVal Monto As Double)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_Ajustar_Cierre", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Sucursal", SqlDbType.Int).Value = Sucursal
            cmdOrden.Parameters.Add("@Anio", SqlDbType.Int).Value = Anio
            cmdOrden.Parameters.Add("@Mes", SqlDbType.Int).Value = Mes
            cmdOrden.Parameters.Add("@Dia", SqlDbType.Int).Value = Dia
            cmdOrden.Parameters.Add("@Monto", SqlDbType.Decimal).Value = Monto
            cmdOrden.ExecuteReader()

            AjusteCierre = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            AjusteCierre = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditProyeccion(ByVal Codigo_Proyeccion As String, ByVal Solicitante As String, ByVal FechaInicio As Date,
                                   ByVal FechaFin As Date, ByVal Registrado As Date, ByVal Tipo As Integer, ByVal Activo As Boolean,
                                   ByVal TipoEdicion As Integer, observacion As String)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_Proyeccion", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Codigo_Proyeccion", SqlDbType.VarChar).Value = Codigo_Proyeccion
            cmdOrden.Parameters.Add("@Solicitante", SqlDbType.Int).Value = Solicitante
            cmdOrden.Parameters.Add("@FechaInicio", SqlDbType.Date).Value = FechaInicio
            cmdOrden.Parameters.Add("@FechaFin", SqlDbType.Date).Value = FechaFin
            cmdOrden.Parameters.Add("@Registrado", SqlDbType.DateTime).Value = Registrado
            cmdOrden.Parameters.Add("@Tipo", SqlDbType.Int).Value = Tipo
            cmdOrden.Parameters.Add("@Activo", SqlDbType.Bit).Value = Activo
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdOrden.Parameters.Add("@Observacion", SqlDbType.VarChar).Value = observacion
            cmdOrden.Parameters.Add("@Sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
            cmdOrden.ExecuteReader()

            EditProyeccion = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditProyeccion = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditProyDetalle(ByVal CodigoOrden As String, ByVal CodigoProducto As String, ByVal CantRequerida As Double, ByVal Costounitario As Double, ByVal CostoTotal As Double, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_ProyeccionDet", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@Codigo_Proyeccion", SqlDbType.VarChar).Value = CodigoOrden
            cmdOrden.Parameters.Add("@Codigo_Producto", SqlDbType.VarChar).Value = CodigoProducto
            cmdOrden.Parameters.Add("@Cantidad", SqlDbType.Money).Value = CantRequerida
            cmdOrden.Parameters.Add("@Costo_Undidad", SqlDbType.Money).Value = Costounitario
            cmdOrden.Parameters.Add("@Costo_Total", SqlDbType.Money).Value = CostoTotal
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdOrden.Parameters.Add("@Sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
            cmdOrden.ExecuteReader()

            EditProyDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditProyDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditPedido(ByVal CodigoSucursal As Integer, ByVal Codigo_Pedido As String, ByVal Codigo_Proveedor As String, ByVal Concepto As String, ByVal Fecha As DateTime, ByVal Taza_Cambio As Decimal, ByVal Estado As Integer, ByVal nTipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_Pedido", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdOrden.Parameters.Add("@Codigo_Pedido", SqlDbType.VarChar).Value = Codigo_Pedido
            cmdOrden.Parameters.Add("@Codigo_Proveedor", SqlDbType.VarChar).Value = Codigo_Proveedor
            cmdOrden.Parameters.Add("@Concepto", SqlDbType.VarChar).Value = Concepto
            cmdOrden.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
            cmdOrden.Parameters.Add("@Taza_Cambio", SqlDbType.Decimal).Value = Taza_Cambio
            cmdOrden.Parameters.Add("@Estado", SqlDbType.Int).Value = Estado
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.ExecuteReader()

            EditPedido = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditPedido = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

End Class
