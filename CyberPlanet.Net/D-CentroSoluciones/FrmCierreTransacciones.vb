﻿Public Class FrmCierreTransacciones
    Dim nNextDiario As Integer
    Dim IsShow As Boolean
    Dim dFecha As DateTime
        Dim NegVal As Double

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        IsShow = False
        Me.Dispose()
        Close()
    End Sub

    Private Sub FrmCierreTransacciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        IsShow = True
        LblCodCierre.Text = MaximoCierre()
        CmbCajas.SelectedIndex = 0
        LimpiarTodo()
        CargarCajas()
        CargarBancos()
        CargarDatos()
    End Sub

    Public Function MaximoCierre()
        Dim strsqlMax As String = ""
        strsqlMax = "SELECT isnull(MAX(Id_Cierre)+1,1) AS Maximo FROM Master_CierreTrans where Id_Caja=" & CmbCajas.SelectedIndex + 1 & ""
        Dim tblRegMax As DataTable = SQL(strsqlMax, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)
        MaximoCierre = tblRegMax.Rows(0).Item(0)
    End Function

    Public Function GetTasaCambio()
        Dim strsqlTasa As String = ""
        strsqlTasa = "SELECT Tasa_Oficial FROM Tbl_TasasCambio where Fecha='" & Format(dFecha, "MM/dd/yyy") & "'"
        Dim tblRegTasa As DataTable = SQL(strsqlTasa, "tblRegTasa", My.Settings.SolIndustrialCNX).Tables(0)
        If tblRegTasa.Rows.Count = 0 Then
            GetTasaCambio = 0
        Else
            GetTasaCambio = tblRegTasa.Rows(0).Item(0)
        End If
    End Function

    Sub CargarCajas()
        Dim StrSql As String = "SELECT Descripcion FROM Cajas WHERE (Activo = 1) ORDER BY Codigo_Caja"
        Dim tblCajas As DataTable = SQL(StrSql, "tblCajas", My.Settings.SolIndustrialCNX).Tables(0)
        CmbCajas.Items.Clear()
        If tblCajas.Rows.Count > 0 Then
            For i = 0 To tblCajas.Rows.Count
                If i < tblCajas.Rows.Count Then
                    CmbCajas.Items.Add(tblCajas.Rows(i).Item(0))
                End If
            Next
        End If
        If CmbCajas.Items.Count > 0 Then
            CmbCajas.SelectedIndex = 0
        End If
    End Sub

    Sub CargarBancos()
        BS_Bancos.DataSource = SQL("select Id_Banco,Nombre_Banco from Tbl_Banco order by Id_Banco", "tblBancos", My.Settings.SolIndustrialCNX).Tables(0)
        luBancos.Properties.DataSource = BS_Bancos
        luBancos.Properties.DisplayMember = "Nombre_Banco"
        luBancos.Properties.ValueMember = "Id_Banco"
        luBancos.ItemIndex = 0
    End Sub

    Public Sub CargarDatos()
        Dim nCantreg As Integer = 0
        Dim tblDatosProdServ As DataTable
        LimpiarTodo()

        dFecha = CDate(dtpFechaCierre.Value)
        LblTasaCambio.Text = GetTasaCambio()

        'Carga las Ventas de Contado
        Dim StrSql As String = "SELECT SUM(Total_Venta) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS VentaBruta,SUM(Total_Descuento) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS TotalDescuento, ((SUM(SubTotal_Venta) * dbo.Tbl_TasasCambio.Tasa_Oficial)-(SUM(Total_Descuento) * dbo.Tbl_TasasCambio.Tasa_Oficial))  AS Subtotales, SUM(Total_Impuesto) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS SumIVA, SUM(Total_RetencionIR) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS SumIR, SUM(Total_RetencionIMI) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS SumIMI, SUM(Total_Neto) * dbo.Tbl_TasasCambio.Tasa_Oficial  as SumNeta, SUM(CS_Efectivo) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS CSEFE,  SUM(US_Efectivo) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS USEFE, SUM(CS_Cheque) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS CSCHK, SUM(US_Cheque) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS USCHK, SUM(CS_Tarjeta) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS CSTARJ, SUM(US_Tarjeta) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS USTARJ " & _
                      " FROM  dbo.Facturas INNER JOIN dbo.Tbl_TasasCambio ON dbo.Facturas.Fecha_Factura = dbo.Tbl_TasasCambio.Fecha " & _
                      " WHERE (dbo.Facturas.Fecha_Factura = '" & Format(dFecha, "MM/dd/yyy") & "') AND (Codigo_Sucursal = " & CmbCajas.SelectedIndex + 1 & ") AND (Anulada = 0)  AND Tipo_Venta=1" & _
                      " GROUP BY dbo.Tbl_TasasCambio.Tasa_Oficial "

        TblDatosProdServ = SQL(StrSql, "tblCierre", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatosProdServ.Rows.Count <> 0 Then
            If IsDBNull(tblDatosProdServ.Rows(0).Item(0)) Then
                Exit Sub
            End If
            LblVentaBruta_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(0), "###,###,###.00")
            LblDesc_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(1), "###,###,###.00")
            LblSubtotal_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(2), "###,###,###.00")
            LblIVA_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(3), "###,###,###.00")
            LblIR_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(4), "###,###,###.00")
            LblIMI_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(5), "###,###,###.00")
            LblVentaNeta_Cont.Text = Format(tblDatosProdServ.Rows(0).Item(6), "###,###,###.00")
        End If

        'Carga las ventas de Credito
        StrSql = "SELECT SUM(Total_Venta) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS VentaBruta,SUM(Total_Descuento) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS TotalDescuento, ((SUM(SubTotal_Venta) * dbo.Tbl_TasasCambio.Tasa_Oficial)-(SUM(Total_Descuento) * dbo.Tbl_TasasCambio.Tasa_Oficial)) AS Subtotales, SUM(Total_Impuesto) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS SumIVA, SUM(Total_RetencionIR) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS SumIR, SUM(Total_RetencionIMI) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS SumIMI, SUM(Total_Neto) * dbo.Tbl_TasasCambio.Tasa_Oficial  as SumNeta, SUM(CS_Efectivo) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS CSEFE,  SUM(US_Efectivo) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS USEFE, SUM(CS_Cheque) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS CSCHK, SUM(US_Cheque) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS USCHK, SUM(CS_Tarjeta) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS CSTARJ, SUM(US_Tarjeta) * dbo.Tbl_TasasCambio.Tasa_Oficial  AS USTARJ " & _
                      " FROM  dbo.Facturas INNER JOIN dbo.Tbl_TasasCambio ON dbo.Facturas.Fecha_Factura = dbo.Tbl_TasasCambio.Fecha " & _
                      " WHERE (dbo.Facturas.Fecha_Factura = '" & Format(dFecha, "MM/dd/yyy") & "') AND (Codigo_Sucursal = " & CmbCajas.SelectedIndex + 1 & ") AND (Anulada = 0)  AND Tipo_Venta=2" & _
                      " GROUP BY dbo.Tbl_TasasCambio.Tasa_Oficial "

        tblDatosProdServ = SQL(StrSql, "tblCierre", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatosProdServ.Rows.Count <> 0 Then
            If IsDBNull(tblDatosProdServ.Rows(0).Item(0)) Then
                Exit Sub
            End If
            LblVentaBruta_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(0), "###,###,###.00")
            LblDesc_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(1), "###,###,###.00")
            LblSubtotal_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(2), "###,###,###.00")
            LblIVA_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(3), "###,###,###.00")
            LblIR_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(4), "###,###,###.00")
            LblIMI_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(5), "###,###,###.00")
            LblVentaNeta_Cred.Text = Format(tblDatosProdServ.Rows(0).Item(6), "###,###,###.00")
        End If


            'LblVentaCred.Text = Format(tblDatosProdServ.Rows(0).Item(5), "###,###,###.00")
            TxtAnticiposAplicados.Text = Format(0, "###,###,###.00")
            txtRecibosAnticiposManual.Text = Format(0, "###,###,###.00")
            LblPorDepositar.Text = Format(CDbl(LblVentaNeta_Cont.Text) + CDbl(txtRecibosAnticiposManual.Text) - CDbl(TxtAnticiposAplicados.Text), "###,###,###.00")

            LblVentaBruta_Total.Text = Format((CDbl(LblVentaBruta_Cont.Text) + CDbl(LblVentaBruta_Cred.Text)) - (CDbl(LblDevBruta_Cont.Text) + CDbl(LblDevBruta_Cred.Text)), "###,###,###.00")
            LblDesc_Total.Text = Format((CDbl(LblDesc_Cont.Text) + CDbl(LblDesc_Cred.Text)) - (CDbl(LblDevDesc_Cont.Text) + CDbl(LblDevDesc_Cred.Text)), "###,###,###.00")
            LblSubtotal.Text = Format((CDbl(LblSubtotal_Cont.Text) + CDbl(LblSubtotal_Cred.Text)) - (CDbl(LblDevSubtotal_Cont.Text) + CDbl(LblDevSubtotal_Cred.Text)), "###,###,###.00")

            LblIVA_Total.Text = Format((CDbl(LblIVA_Cont.Text) + CDbl(LblIVA_Cred.Text)) - (CDbl(LblDevIVA_Cont.Text) + CDbl(LblDevIVA_Cred.Text)), "###,###,###.00")
            LblIMI_Total.Text = Format((CDbl(LblIMI_Cont.Text) + CDbl(LblIMI_Cred.Text)) - (CDbl(LblDevIMI_Cont.Text) + CDbl(LblDevIMI_Cred.Text)), "###,###,###.00")
            LblIR_Total.Text = Format((CDbl(LblIR_Cont.Text) + CDbl(LblIR_Cred.Text)) - (CDbl(LblDevIR_Cont.Text) + CDbl(LblDevIR_Cred.Text)), "###,###,###.00")
            LblVentaNeta_Total.Text = Format((CDbl(LblVentaNeta_Cont.Text) + CDbl(LblVentaNeta_Cred.Text)) - (CDbl(LblDevVentaNeta_Cont.Text) + CDbl(LblDevVentaNeta_Cred.Text)), "###,###,###.00")

            'txtEfectCord.Text = Format(tblDatosProdServ.Rows(0).Item(7), "###,###,###.00")
            'txtEfectDol.Text = Format(tblDatosProdServ.Rows(0).Item(8), "###,###,###.00")
            'txtTransfCord.Text = Format(tblDatosProdServ.Rows(0).Item(7), "###,###,###.00")
            'txtTransDol.Text = Format(tblDatosProdServ.Rows(0).Item(8), "###,###,###.00")
            'txtCheckCord.Text = Format(tblDatosProdServ.Rows(0).Item(9), "###,###,###.00")
            'txtCheckDol.Text = Format(tblDatosProdServ.Rows(0).Item(10), "###,###,###.00")
            'txtTarjCord.Text = Format(tblDatosProdServ.Rows(0).Item(11), "###,###,###.00")
            'txtTarjDol.Text = Format(tblDatosProdServ.Rows(0).Item(12), "###,###,###.00")

        GenerarCalculo()

    End Sub

    Sub LimpiarTodo()
        LblVentaBruta_Cont.Text = Format(0, "###,###,###.00")
        LblDesc_Cont.Text = Format(0, "###,###,###.00")
        LblSubtotal_Cont.Text = Format(0, "###,###,###.00")
        LblIVA_Cont.Text = Format(0, "###,###,###.00")
        LblIR_Cont.Text = Format(0, "###,###,###.00")
        LblIMI_Cont.Text = Format(0, "###,###,###.00")
        LblVentaNeta_Cont.Text = Format(0, "###,###,###.00")

        LblVentaBruta_Cred.Text = Format(0, "###,###,###.00")
        LblDesc_Cred.Text = Format(0, "###,###,###.00")
        LblSubtotal_Cred.Text = Format(0, "###,###,###.00")
        LblIVA_Cred.Text = Format(0, "###,###,###.00")
        LblIR_Cred.Text = Format(0, "###,###,###.00")
        LblIMI_Cred.Text = Format(0, "###,###,###.00")
        LblVentaNeta_Cred.Text = Format(0, "###,###,###.00")

        LblDevBruta_Cont.Text = Format(0, "###,###,###.00")
        LblDevDesc_Cont.Text = Format(0, "###,###,###.00")
        LblDevSubtotal_Cont.Text = Format(0, "###,###,###.00")
        LblDevIVA_Cont.Text = Format(0, "###,###,###.00")
        LblDevIR_Cont.Text = Format(0, "###,###,###.00")
        LblDevIMI_Cont.Text = Format(0, "###,###,###.00")
        LblDevVentaNeta_Cont.Text = Format(0, "###,###,###.00")

        LblDevBruta_Cred.Text = Format(0, "###,###,###.00")
        LblDevDesc_Cred.Text = Format(0, "###,###,###.00")
        LblDevSubtotal_Cred.Text = Format(0, "###,###,###.00")
        LblDevIVA_Cred.Text = Format(0, "###,###,###.00")
        LblDevIR_Cred.Text = Format(0, "###,###,###.00")
        LblDevIMI_Cred.Text = Format(0, "###,###,###.00")
        LblDevVentaNeta_Cred.Text = Format(0, "###,###,###.00")

        TxtAnticiposAplicados.Text = Format(0, "###,###,###.00")
        txtRecibosAnticiposManual.Text = Format(0, "###,###,###.00")
        'LblPorDepositar.Text = Format(CDbl(LblVentaNeta_Cont.Text) + CDbl(txtRecibosAnticiposManual.Text) - CDbl(TxtAnticiposAplicados.Text), "###,###,###.00")

        LblPorDepositar.Text = Format(0, "###,###,###.00")
        txtEfectCord.Text = Format(0, "###,###,###.00")
        txtEfectDol.Text = Format(0, "###,###,###.00")
        txtCheckCord.Text = Format(0, "###,###,###.00")
        txtCheckDol.Text = Format(0, "###,###,###.00")
        txtTransfCord.Text = Format(0, "###,###,###.00")
        txtTransDol.Text = Format(0, "###,###,###.00")
        txtTarjCord.Text = Format(0, "###,###,###.00")
        txtTarjDol.Text = Format(0, "###,###,###.00")

        LblDiferencia.Text = Format(0, "###,###,###.00")
        txtDepositoBanco.Text = Format(0, "###,###,###.00")
        txtRecibosAnticiposManual.Text = Format(0, "###,###,###.00")
    End Sub

    'Public Sub CargarFechas()
    '    Dim StrSql As String = "SELECT Fecha FROM Tbl_Transacciones WHERE (Anulado = 0) AND (Num_Lote IS NULL) GROUP BY Fecha"
    '    Dim tblFechas As DataTable = SQL(StrSql, "tblFechas", My.Settings.SolIndustrialCNX).Tables(0)
    '    CmbFechas.Items.Clear()
    '    If tblFechas.Rows.Count > 0 Then
    '        For i = 0 To tblFechas.Rows.Count
    '            If i < tblFechas.Rows.Count Then
    '                CmbFechas.Items.Add(Format(tblFechas.Rows(i).Item(0), "dd/MM/yyyy"))
    '            End If
    '        Next
    '    End If
    '    If CmbFechas.Items.Count > 0 Then
    '        CmbFechas.SelectedIndex = 0
    '    End If
    'End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim StrTrans As String = ""
        Dim CierreTrans As New clsAddTransaccion(My.Settings.SolIndustrialCNX)

        Dim StrMasterDiario As String = ""
        Dim strPadres As String
        Dim dtPadre As DataTable
        Dim sCodPadre As String
        Dim bIsHijo As Boolean = True
        Dim sNumCuenta As String

        Try
            StrTrans = CierreTrans.CerrarTransacciones(CDate(dtpFechaDep.Value), txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, txtCheckCord.Text, LblDiferencia.Text)
            If StrTrans = "OK" Then
                MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
                'frmTransacciones.CargarTransacciones()
                'frmPrincipal.CargarContabilidad()
                nNextDiario = SiguienteTransaccionDiario()
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

            ''*********************************CONTABILIZAR ESTA TRANSACCION*************************************************
            ''Master Comprobante
            'Dim sConceptoCierre As String = "Cierre de Caja del dia " & Now
            'StrMasterDiario = CierreTrans.AgregarMasterDiario(nNextDiario, Now, sConceptoCierre, CDbl(txtCheckDol.Text), 1)    'nTipoEdic
            'If StrMasterDiario <> "OK" Then
            '    MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            'End If

            ''Debito
            'sNumCuenta = 10102
            'StrMasterDiario = CierreTrans.AgregarDetalleDiario(nNextDiario, sNumCuenta, CDbl(txtCheckDol.Text), 0, 1)         'nTipoEdic
            'If StrMasterDiario <> "OK" Then
            '    MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            'End If

            ''**************APLICAR UN DEBITO A PADRES E HIJOS
            'Do While bIsHijo = True
            '    strPadres = "SELECT Cuenta_Padre FROM Tbl_CatalogoCuentas where (Codigo_Cuenta = '" & sNumCuenta & "')"
            '    dtPadre = SQL(strPadres, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)
            '    sCodPadre = dtPadre.Rows(0).Item(0)
            '    If sCodPadre <> "0" Then
            '        StrMasterDiario = CierreTrans.ActualizarCatalogo(sCodPadre, CDbl(txtCheckDol.Text), 1)             'nTipoEdic
            '        If StrMasterDiario <> "OK" Then
            '            MsgBox("Ha ocurrido un error al actualizar la cuenta en el catalogo.", MsgBoxStyle.Information, "SIGCA")
            '        Else
            '            bIsHijo = True
            '            sNumCuenta = sCodPadre
            '        End If
            '    Else
            '        bIsHijo = False
            '    End If
            'Loop
            ''************

            ''Credito
            'sNumCuenta = 10101
            'bIsHijo = True
            'StrMasterDiario = CierreTrans.AgregarDetalleDiario(nNextDiario, sNumCuenta, 0, CDbl(txtCheckDol.Text), 1)         'nTipoEdic
            'If StrMasterDiario <> "OK" Then
            '    MsgBox("Ha ocurrido un error al contabilizar esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            'End If

            ''**************APLICAR UN CREDITO A PADRES E HIJOS
            'Do While bIsHijo = True
            '    strPadres = "SELECT Cuenta_Padre FROM Tbl_CatalogoCuentas where (Codigo_Cuenta = '" & sNumCuenta & "')"
            '    dtPadre = SQL(strPadres, "tblSaldo", My.Settings.SolIndustrialCNX).Tables(0)
            '    sCodPadre = dtPadre.Rows(0).Item(0)
            '    If sCodPadre <> "0" Then
            '        StrMasterDiario = CierreTrans.ActualizarCatalogo(sCodPadre, CDbl(txtCheckDol.Text), 0)
            '        If StrMasterDiario <> "OK" Then
            '            MsgBox("Ha ocurrido un error al actualizar la cuenta en el catalogo.", MsgBoxStyle.Information, "SIGCA")
            '        Else
            '            bIsHijo = True
            '            sNumCuenta = sCodPadre
            '        End If
            '    Else
            '        bIsHijo = False
            '    End If
            'Loop
            ''************
            ''*************************************FIN DE CONTABILIZAR TRANSACCION

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            Cerrar()
        End Try
    End Sub

    'Private Sub txtConsumoEnergia_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtConsumoEnergia.KeyPress
    '    If InStr("0123456789" & Chr(8), e.KeyChar) Then
    '        e.Handled = False
    '    Else
    '        e.Handled = True
    '    End If
    'End Sub

    'Private Sub txtFisicoCaja_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFisicoCaja.KeyPress
    '    If InStr("0123456789" & Chr(8), e.KeyChar) Then
    '        e.Handled = False
    '    Else
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub txtEfectCord_TextChanged(sender As Object, e As EventArgs) Handles txtEfectCord.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtEfectDol_TextChanged(sender As Object, e As EventArgs) Handles txtEfectDol.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtCheckCord_TextChanged(sender As Object, e As EventArgs) Handles txtCheckCord.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtCheckDol_TextChanged(sender As Object, e As EventArgs) Handles txtCheckDol.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtTransfCord_TextChanged(sender As Object, e As EventArgs) Handles txtTransfCord.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtTransDol_TextChanged(sender As Object, e As EventArgs) Handles txtTransDol.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtTarjCord_TextChanged(sender As Object, e As EventArgs) Handles txtTarjCord.TextChanged
        GenerarCalculo()
    End Sub

    Private Sub txtTarjDol_TextChanged(sender As Object, e As EventArgs) Handles txtTarjDol.TextChanged
        GenerarCalculo()
    End Sub

    Sub GenerarCalculo()
        Try
            LblTotalDepCord.Text = Format(CDbl(IIf(txtEfectCord.Text = "", 0, txtEfectCord.Text)) + CDbl(IIf(txtCheckCord.Text = "", 0, txtCheckCord.Text)), "###,###,###.00")
            LblIngresosCS.Text = Format(CDbl(IIf(txtEfectCord.Text = "", 0, txtEfectCord.Text)) + CDbl(IIf(txtCheckCord.Text = "", 0, txtCheckCord.Text)) + CDbl(IIf(txtTarjCord.Text = "", 0, txtTarjCord.Text)), "###,###,###.00")

            LblTotalDepDol.Text = Format(CDbl(IIf(txtEfectDol.Text = "", 0, txtEfectDol.Text)) + CDbl(IIf(txtCheckDol.Text = "", 0, txtCheckDol.Text)), "###,###,###.00")
            LblIngresosUSS.Text = Format(CDbl(IIf(txtEfectDol.Text = "", 0, txtEfectDol.Text)) + CDbl(IIf(txtCheckDol.Text = "", 0, txtCheckDol.Text)) + CDbl(IIf(txtTarjDol.Text = "", 0, txtTarjDol.Text)), "###,###,###.00")

            LblEquivEfecCord.Text = Format(CDbl(IIf(txtEfectDol.Text = "", 0, txtEfectDol.Text)) * CDbl(IIf(LblTasaCambio.Text = "", 0, LblTasaCambio.Text)), "###,###,###.00")
            LblEquivChkCord.Text = Format(CDbl(IIf(txtCheckDol.Text = "", 0, txtCheckDol.Text)) * CDbl(IIf(LblTasaCambio.Text = "", 0, LblTasaCambio.Text)), "###,###,###.00")
            LblTotalDepEquiv.Text = Format(CDbl(IIf(LblTotalDepDol.Text = "", 0, LblTotalDepDol.Text)) * CDbl(IIf(LblTasaCambio.Text = "", 0, LblTasaCambio.Text)), "###,###,###.00")
            LblEquivTarjCord.Text = Format(CDbl(IIf(txtTarjDol.Text = "", 0, txtTarjDol.Text)) * CDbl(IIf(LblTasaCambio.Text = "", 0, LblTasaCambio.Text)), "###,###,###.00")
            LblIngresosEquivCS.Text = Format(CDbl(IIf(LblIngresosUSS.Text = "", 0, LblIngresosUSS.Text)) * CDbl(IIf(LblTasaCambio.Text = "", 0, LblTasaCambio.Text)), "###,###,###.00")

            LblTotalEfec.Text = Format(CDbl(IIf(txtEfectCord.Text = "", 0, txtEfectCord.Text)) + CDbl(IIf(LblEquivEfecCord.Text = "", 0, LblEquivEfecCord.Text)), "###,###,###.00")
            LblTotalCheck.Text = Format(CDbl(IIf(txtCheckCord.Text = "", 0, txtCheckCord.Text)) + CDbl(IIf(LblEquivChkCord.Text = "", 0, LblEquivChkCord.Text)), "###,###,###.00")
            LblTotalDepCS.Text = Format(CDbl(IIf(LblTotalDepCord.Text = "", 0, LblTotalDepCord.Text)) + CDbl(IIf(LblTotalDepEquiv.Text = "", 0, LblTotalDepEquiv.Text)), "###,###,###.00")
            LblTotalTarj.Text = Format(CDbl(IIf(txtTarjCord.Text = "", 0, txtTarjCord.Text)) + CDbl(IIf(LblEquivTarjCord.Text = "", 0, LblEquivTarjCord.Text)), "###,###,###.00")
            LblTotalDeposito.Text = Format(CDbl(IIf(LblIngresosCS.Text = "", 0, LblIngresosCS.Text)) + CDbl(IIf(LblIngresosEquivCS.Text = "", 0, LblIngresosEquivCS.Text)), "###,###,###.00")

            DiferenciaArqueo()
        Catch ex As Exception
            MsgBox("Se produjo un error cuando se realizaba el calculo de los datos ingresados. Verifique la entrada de datos", MsgBoxStyle.Exclamation, "WorkSystem.Net")
        End Try
    End Sub

    Sub DiferenciaArqueo()
        Dim nDif As Double
        nDif = (CDbl(IIf(LblPorDepositar.Text = "", 0, LblPorDepositar.Text)) - CDbl(IIf(LblTotalDeposito.Text = "", 0, LblTotalDeposito.Text)))
        LblDiferencia.Text = Format(nDif, "###,###,###.00")
        NegVal = Format(nDif, "###,###,###.00")
        If nDif < 0 Then
            LblDiferencia.ForeColor = Color.Red
            cmdAceptar.Enabled = False
        Else
            LblDiferencia.ForeColor = Color.Black
            cmdAceptar.Enabled = True
        End If
    End Sub

    Private Sub CmbCajas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CmbCajas.SelectedIndexChanged
        CargarDatos()
        LblCodCierre.Text = MaximoCierre()
    End Sub

    Private Sub LblDiferencia_DoubleClick(sender As Object, e As EventArgs) Handles LblDiferencia.DoubleClick
        If NegVal > 0 Then
            txtEfectCord.Text = Format(CDbl(LblDiferencia.Text), "###,###,###.00")
        End If
    End Sub

    'Private Sub txtRecuperacion_TextChanged(sender As Object, e As EventArgs) Handles txtRecuperacion.TextChanged
    '    Try
    '        LblPorDepositar.Text = Format(CDbl(IIf(LblVentaNeta_Cont.Text = "", 0, LblVentaNeta_Cont.Text)) + CDbl(IIf(txtRecibosAnticiposManual.Text = "", 0, txtRecibosAnticiposManual.Text)), "###,###,###.00")

    '        DiferenciaArqueo()
    '    Catch ex As Exception
    '        MsgBox("Se produjo un error cuando se realizaba el calculo de los datos ingresados. Verifique la entrada de datos", MsgBoxStyle.Exclamation, "WorkSystem.Net")
    '    End Try
    'End Sub

    Private Sub dtpFechaCierre_ValueChanged_1(sender As Object, e As EventArgs) Handles dtpFechaCierre.ValueChanged
            CargarDatos()
            LblCodCierre.Text = MaximoCierre()
    End Sub

End Class