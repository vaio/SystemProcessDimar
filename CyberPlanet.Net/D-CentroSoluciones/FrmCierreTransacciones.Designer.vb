﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCierreTransacciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCierreTransacciones))
    Me.BS_Bancos = New System.Windows.Forms.BindingSource(Me.components)
    Me.BS_DetalleDeposito = New System.Windows.Forms.BindingSource(Me.components)
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.GroupBox4 = New System.Windows.Forms.GroupBox()
    Me.LblSubtotal = New System.Windows.Forms.Label()
    Me.LblDevSubtotal_Cred = New System.Windows.Forms.Label()
    Me.LblDevSubtotal_Cont = New System.Windows.Forms.Label()
    Me.LblSubtotal_Cred = New System.Windows.Forms.Label()
    Me.LblSubtotal_Cont = New System.Windows.Forms.Label()
    Me.Label30 = New System.Windows.Forms.Label()
    Me.LblDesc_Total = New System.Windows.Forms.Label()
    Me.LblVentaNeta_Total = New System.Windows.Forms.Label()
    Me.LblIMI_Total = New System.Windows.Forms.Label()
    Me.LblIR_Total = New System.Windows.Forms.Label()
    Me.LblIVA_Total = New System.Windows.Forms.Label()
    Me.LblVentaBruta_Total = New System.Windows.Forms.Label()
    Me.Label59 = New System.Windows.Forms.Label()
    Me.LblDevDesc_Cred = New System.Windows.Forms.Label()
    Me.LblDevVentaNeta_Cred = New System.Windows.Forms.Label()
    Me.LblDevIMI_Cred = New System.Windows.Forms.Label()
    Me.LblDevIR_Cred = New System.Windows.Forms.Label()
    Me.LblDevIVA_Cred = New System.Windows.Forms.Label()
    Me.LblDevBruta_Cred = New System.Windows.Forms.Label()
    Me.LblDevDesc_Cont = New System.Windows.Forms.Label()
    Me.LblDevVentaNeta_Cont = New System.Windows.Forms.Label()
    Me.LblDevIMI_Cont = New System.Windows.Forms.Label()
    Me.LblDevIR_Cont = New System.Windows.Forms.Label()
    Me.LblDevIVA_Cont = New System.Windows.Forms.Label()
    Me.LblDevBruta_Cont = New System.Windows.Forms.Label()
    Me.LblDesc_Cred = New System.Windows.Forms.Label()
    Me.Label27 = New System.Windows.Forms.Label()
    Me.LblVentaNeta_Cred = New System.Windows.Forms.Label()
    Me.LblIMI_Cred = New System.Windows.Forms.Label()
    Me.LblIR_Cred = New System.Windows.Forms.Label()
    Me.Label36 = New System.Windows.Forms.Label()
    Me.LblIVA_Cred = New System.Windows.Forms.Label()
    Me.LblVentaBruta_Cred = New System.Windows.Forms.Label()
    Me.Label43 = New System.Windows.Forms.Label()
    Me.Label44 = New System.Windows.Forms.Label()
    Me.LblDesc_Cont = New System.Windows.Forms.Label()
    Me.Label29 = New System.Windows.Forms.Label()
    Me.LblVentaNeta_Cont = New System.Windows.Forms.Label()
    Me.Label26 = New System.Windows.Forms.Label()
    Me.LblIMI_Cont = New System.Windows.Forms.Label()
    Me.Label22 = New System.Windows.Forms.Label()
    Me.LblIR_Cont = New System.Windows.Forms.Label()
    Me.Label10 = New System.Windows.Forms.Label()
    Me.LblIVA_Cont = New System.Windows.Forms.Label()
    Me.LblVentaBruta_Cont = New System.Windows.Forms.Label()
    Me.Label17 = New System.Windows.Forms.Label()
    Me.Label20 = New System.Windows.Forms.Label()
    Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
    Me.LineShape3 = New Microsoft.VisualBasic.PowerPacks.LineShape()
    Me.LblDiferencia = New System.Windows.Forms.Label()
    Me.LblCodCierre = New System.Windows.Forms.Label()
    Me.Label9 = New System.Windows.Forms.Label()
    Me.Label40 = New System.Windows.Forms.Label()
    Me.LblTasaCambio = New System.Windows.Forms.Label()
    Me.Label15 = New System.Windows.Forms.Label()
    Me.Label33 = New System.Windows.Forms.Label()
    Me.CmbCajas = New System.Windows.Forms.ComboBox()
    Me.LblPorDepositar = New System.Windows.Forms.Label()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.TxtAnticiposAplicados = New System.Windows.Forms.TextBox()
    Me.Label21 = New System.Windows.Forms.Label()
    Me.txtRecibosAnticiposManual = New System.Windows.Forms.TextBox()
    Me.Label32 = New System.Windows.Forms.Label()
    Me.dtpFechaCierre = New System.Windows.Forms.DateTimePicker()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.Label25 = New System.Windows.Forms.Label()
    Me.txtTransDol = New System.Windows.Forms.TextBox()
    Me.txtTransfCord = New System.Windows.Forms.TextBox()
    Me.LblTotalTransf = New System.Windows.Forms.Label()
    Me.LblEquivTransfCord = New System.Windows.Forms.Label()
    Me.txtTarjDol = New System.Windows.Forms.TextBox()
    Me.txtCheckDol = New System.Windows.Forms.TextBox()
    Me.txtEfectDol = New System.Windows.Forms.TextBox()
    Me.txtTarjCord = New System.Windows.Forms.TextBox()
    Me.txtCheckCord = New System.Windows.Forms.TextBox()
    Me.txtEfectCord = New System.Windows.Forms.TextBox()
    Me.LblTotalDeposito = New System.Windows.Forms.Label()
    Me.LblTotalTarj = New System.Windows.Forms.Label()
    Me.LblTotalDepCS = New System.Windows.Forms.Label()
    Me.LblTotalCheck = New System.Windows.Forms.Label()
    Me.LblTotalEfec = New System.Windows.Forms.Label()
    Me.Label23 = New System.Windows.Forms.Label()
    Me.LblIngresosEquivCS = New System.Windows.Forms.Label()
    Me.LblIngresosUSS = New System.Windows.Forms.Label()
    Me.LblEquivTarjCord = New System.Windows.Forms.Label()
    Me.Label19 = New System.Windows.Forms.Label()
    Me.Label18 = New System.Windows.Forms.Label()
    Me.LblTotalDepEquiv = New System.Windows.Forms.Label()
    Me.LblEquivChkCord = New System.Windows.Forms.Label()
    Me.LblTotalDepDol = New System.Windows.Forms.Label()
    Me.LblTotalDepCord = New System.Windows.Forms.Label()
    Me.Label14 = New System.Windows.Forms.Label()
    Me.LblEquivEfecCord = New System.Windows.Forms.Label()
    Me.Label8 = New System.Windows.Forms.Label()
    Me.LblIngresosCS = New System.Windows.Forms.Label()
    Me.Label11 = New System.Windows.Forms.Label()
    Me.Label12 = New System.Windows.Forms.Label()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.xtpBancoyContab = New DevExpress.XtraTab.XtraTabControl()
    Me.xtpDepBanco = New DevExpress.XtraTab.XtraTabPage()
    Me.GroupBox3 = New System.Windows.Forms.GroupBox()
    Me.Label13 = New System.Windows.Forms.Label()
    Me.Label7 = New System.Windows.Forms.Label()
    Me.luBancos = New DevExpress.XtraEditors.LookUpEdit()
    Me.txtDepositoBanco = New System.Windows.Forms.TextBox()
    Me.btnElimDep = New System.Windows.Forms.Button()
    Me.btnAddDep = New System.Windows.Forms.Button()
    Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
    Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
    Me.Label41 = New System.Windows.Forms.Label()
    Me.CmbTipo = New System.Windows.Forms.ComboBox()
    Me.dtpFechaDep = New System.Windows.Forms.DateTimePicker()
    Me.Label16 = New System.Windows.Forms.Label()
    Me.Label37 = New System.Windows.Forms.Label()
    Me.Label38 = New System.Windows.Forms.Label()
    Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
    Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
    Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
    Me.xtpContabCierre = New DevExpress.XtraTab.XtraTabPage()
    Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
    Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
    Me.xtpContabDep = New DevExpress.XtraTab.XtraTabPage()
    Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
    Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
    CType(Me.BS_Bancos, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.BS_DetalleDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.Panel3.SuspendLayout()
    CType(Me.xtpBancoyContab, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.xtpBancoyContab.SuspendLayout()
    Me.xtpDepBanco.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    CType(Me.luBancos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.xtpContabCierre.SuspendLayout()
    CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.xtpContabDep.SuspendLayout()
    CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 608)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(821, 49)
    Me.Panel1.TabIndex = 1
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(722, 7)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 2
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdAceptar.Enabled = False
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(638, 7)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 1
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'Panel2
    '
    Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel2.Controls.Add(Me.GroupBox4)
    Me.Panel2.Controls.Add(Me.LblDiferencia)
    Me.Panel2.Controls.Add(Me.LblCodCierre)
    Me.Panel2.Controls.Add(Me.Label9)
    Me.Panel2.Controls.Add(Me.Label40)
    Me.Panel2.Controls.Add(Me.LblTasaCambio)
    Me.Panel2.Controls.Add(Me.Label15)
    Me.Panel2.Controls.Add(Me.Label33)
    Me.Panel2.Controls.Add(Me.CmbCajas)
    Me.Panel2.Controls.Add(Me.LblPorDepositar)
    Me.Panel2.Controls.Add(Me.GroupBox1)
    Me.Panel2.Controls.Add(Me.dtpFechaCierre)
    Me.Panel2.Controls.Add(Me.GroupBox2)
    Me.Panel2.Controls.Add(Me.Label1)
    Me.Panel2.Controls.Add(Me.Label2)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel2.Location = New System.Drawing.Point(0, 0)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(821, 372)
    Me.Panel2.TabIndex = 33
    '
    'GroupBox4
    '
    Me.GroupBox4.Controls.Add(Me.LblSubtotal)
    Me.GroupBox4.Controls.Add(Me.LblDevSubtotal_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevSubtotal_Cont)
    Me.GroupBox4.Controls.Add(Me.LblSubtotal_Cred)
    Me.GroupBox4.Controls.Add(Me.LblSubtotal_Cont)
    Me.GroupBox4.Controls.Add(Me.Label30)
    Me.GroupBox4.Controls.Add(Me.LblDesc_Total)
    Me.GroupBox4.Controls.Add(Me.LblVentaNeta_Total)
    Me.GroupBox4.Controls.Add(Me.LblIMI_Total)
    Me.GroupBox4.Controls.Add(Me.LblIR_Total)
    Me.GroupBox4.Controls.Add(Me.LblIVA_Total)
    Me.GroupBox4.Controls.Add(Me.LblVentaBruta_Total)
    Me.GroupBox4.Controls.Add(Me.Label59)
    Me.GroupBox4.Controls.Add(Me.LblDevDesc_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevVentaNeta_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevIMI_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevIR_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevIVA_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevBruta_Cred)
    Me.GroupBox4.Controls.Add(Me.LblDevDesc_Cont)
    Me.GroupBox4.Controls.Add(Me.LblDevVentaNeta_Cont)
    Me.GroupBox4.Controls.Add(Me.LblDevIMI_Cont)
    Me.GroupBox4.Controls.Add(Me.LblDevIR_Cont)
    Me.GroupBox4.Controls.Add(Me.LblDevIVA_Cont)
    Me.GroupBox4.Controls.Add(Me.LblDevBruta_Cont)
    Me.GroupBox4.Controls.Add(Me.LblDesc_Cred)
    Me.GroupBox4.Controls.Add(Me.Label27)
    Me.GroupBox4.Controls.Add(Me.LblVentaNeta_Cred)
    Me.GroupBox4.Controls.Add(Me.LblIMI_Cred)
    Me.GroupBox4.Controls.Add(Me.LblIR_Cred)
    Me.GroupBox4.Controls.Add(Me.Label36)
    Me.GroupBox4.Controls.Add(Me.LblIVA_Cred)
    Me.GroupBox4.Controls.Add(Me.LblVentaBruta_Cred)
    Me.GroupBox4.Controls.Add(Me.Label43)
    Me.GroupBox4.Controls.Add(Me.Label44)
    Me.GroupBox4.Controls.Add(Me.LblDesc_Cont)
    Me.GroupBox4.Controls.Add(Me.Label29)
    Me.GroupBox4.Controls.Add(Me.LblVentaNeta_Cont)
    Me.GroupBox4.Controls.Add(Me.Label26)
    Me.GroupBox4.Controls.Add(Me.LblIMI_Cont)
    Me.GroupBox4.Controls.Add(Me.Label22)
    Me.GroupBox4.Controls.Add(Me.LblIR_Cont)
    Me.GroupBox4.Controls.Add(Me.Label10)
    Me.GroupBox4.Controls.Add(Me.LblIVA_Cont)
    Me.GroupBox4.Controls.Add(Me.LblVentaBruta_Cont)
    Me.GroupBox4.Controls.Add(Me.Label17)
    Me.GroupBox4.Controls.Add(Me.Label20)
    Me.GroupBox4.Controls.Add(Me.ShapeContainer1)
    Me.GroupBox4.Location = New System.Drawing.Point(10, 47)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(671, 164)
    Me.GroupBox4.TabIndex = 27
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "Detalle de ventas del día"
    '
    'LblSubtotal
    '
    Me.LblSubtotal.BackColor = System.Drawing.Color.White
    Me.LblSubtotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblSubtotal.Location = New System.Drawing.Point(272, 132)
    Me.LblSubtotal.Name = "LblSubtotal"
    Me.LblSubtotal.Size = New System.Drawing.Size(71, 18)
    Me.LblSubtotal.TabIndex = 65
    Me.LblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevSubtotal_Cred
    '
    Me.LblDevSubtotal_Cred.BackColor = System.Drawing.Color.White
    Me.LblDevSubtotal_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevSubtotal_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevSubtotal_Cred.Location = New System.Drawing.Point(272, 106)
    Me.LblDevSubtotal_Cred.Name = "LblDevSubtotal_Cred"
    Me.LblDevSubtotal_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDevSubtotal_Cred.TabIndex = 64
    Me.LblDevSubtotal_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevSubtotal_Cont
    '
    Me.LblDevSubtotal_Cont.BackColor = System.Drawing.Color.White
    Me.LblDevSubtotal_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevSubtotal_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevSubtotal_Cont.Location = New System.Drawing.Point(272, 84)
    Me.LblDevSubtotal_Cont.Name = "LblDevSubtotal_Cont"
    Me.LblDevSubtotal_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDevSubtotal_Cont.TabIndex = 63
    Me.LblDevSubtotal_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblSubtotal_Cred
    '
    Me.LblSubtotal_Cred.BackColor = System.Drawing.Color.White
    Me.LblSubtotal_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblSubtotal_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblSubtotal_Cred.Location = New System.Drawing.Point(272, 62)
    Me.LblSubtotal_Cred.Name = "LblSubtotal_Cred"
    Me.LblSubtotal_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblSubtotal_Cred.TabIndex = 62
    Me.LblSubtotal_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblSubtotal_Cont
    '
    Me.LblSubtotal_Cont.BackColor = System.Drawing.Color.White
    Me.LblSubtotal_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblSubtotal_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblSubtotal_Cont.Location = New System.Drawing.Point(272, 39)
    Me.LblSubtotal_Cont.Name = "LblSubtotal_Cont"
    Me.LblSubtotal_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblSubtotal_Cont.TabIndex = 61
    Me.LblSubtotal_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label30
    '
    Me.Label30.AutoSize = True
    Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label30.Location = New System.Drawing.Point(274, 24)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(65, 12)
    Me.Label30.TabIndex = 60
    Me.Label30.Text = "SUBTOTALES"
    '
    'LblDesc_Total
    '
    Me.LblDesc_Total.BackColor = System.Drawing.Color.White
    Me.LblDesc_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDesc_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDesc_Total.Location = New System.Drawing.Point(195, 132)
    Me.LblDesc_Total.Name = "LblDesc_Total"
    Me.LblDesc_Total.Size = New System.Drawing.Size(71, 18)
    Me.LblDesc_Total.TabIndex = 58
    Me.LblDesc_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblVentaNeta_Total
    '
    Me.LblVentaNeta_Total.BackColor = System.Drawing.Color.SandyBrown
    Me.LblVentaNeta_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblVentaNeta_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblVentaNeta_Total.Location = New System.Drawing.Point(581, 131)
    Me.LblVentaNeta_Total.Name = "LblVentaNeta_Total"
    Me.LblVentaNeta_Total.Size = New System.Drawing.Size(72, 18)
    Me.LblVentaNeta_Total.TabIndex = 57
    Me.LblVentaNeta_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblIMI_Total
    '
    Me.LblIMI_Total.BackColor = System.Drawing.Color.White
    Me.LblIMI_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIMI_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIMI_Total.Location = New System.Drawing.Point(503, 131)
    Me.LblIMI_Total.Name = "LblIMI_Total"
    Me.LblIMI_Total.Size = New System.Drawing.Size(71, 18)
    Me.LblIMI_Total.TabIndex = 56
    Me.LblIMI_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblIR_Total
    '
    Me.LblIR_Total.BackColor = System.Drawing.Color.White
    Me.LblIR_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIR_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIR_Total.Location = New System.Drawing.Point(426, 131)
    Me.LblIR_Total.Name = "LblIR_Total"
    Me.LblIR_Total.Size = New System.Drawing.Size(71, 18)
    Me.LblIR_Total.TabIndex = 55
    Me.LblIR_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblIVA_Total
    '
    Me.LblIVA_Total.BackColor = System.Drawing.Color.White
    Me.LblIVA_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIVA_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIVA_Total.Location = New System.Drawing.Point(349, 132)
    Me.LblIVA_Total.Name = "LblIVA_Total"
    Me.LblIVA_Total.Size = New System.Drawing.Size(71, 18)
    Me.LblIVA_Total.TabIndex = 54
    Me.LblIVA_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblVentaBruta_Total
    '
    Me.LblVentaBruta_Total.BackColor = System.Drawing.Color.White
    Me.LblVentaBruta_Total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblVentaBruta_Total.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblVentaBruta_Total.Location = New System.Drawing.Point(118, 131)
    Me.LblVentaBruta_Total.Name = "LblVentaBruta_Total"
    Me.LblVentaBruta_Total.Size = New System.Drawing.Size(71, 18)
    Me.LblVentaBruta_Total.TabIndex = 53
    Me.LblVentaBruta_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label59
    '
    Me.Label59.AutoSize = True
    Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label59.Location = New System.Drawing.Point(17, 134)
    Me.Label59.Name = "Label59"
    Me.Label59.Size = New System.Drawing.Size(57, 12)
    Me.Label59.TabIndex = 52
    Me.Label59.Text = "TOTALES:"
    '
    'LblDevDesc_Cred
    '
    Me.LblDevDesc_Cred.BackColor = System.Drawing.Color.White
    Me.LblDevDesc_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevDesc_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevDesc_Cred.Location = New System.Drawing.Point(195, 106)
    Me.LblDevDesc_Cred.Name = "LblDevDesc_Cred"
    Me.LblDevDesc_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDevDesc_Cred.TabIndex = 51
    Me.LblDevDesc_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevVentaNeta_Cred
    '
    Me.LblDevVentaNeta_Cred.BackColor = System.Drawing.Color.SandyBrown
    Me.LblDevVentaNeta_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevVentaNeta_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevVentaNeta_Cred.Location = New System.Drawing.Point(581, 105)
    Me.LblDevVentaNeta_Cred.Name = "LblDevVentaNeta_Cred"
    Me.LblDevVentaNeta_Cred.Size = New System.Drawing.Size(72, 18)
    Me.LblDevVentaNeta_Cred.TabIndex = 50
    Me.LblDevVentaNeta_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevIMI_Cred
    '
    Me.LblDevIMI_Cred.BackColor = System.Drawing.Color.White
    Me.LblDevIMI_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevIMI_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevIMI_Cred.Location = New System.Drawing.Point(503, 105)
    Me.LblDevIMI_Cred.Name = "LblDevIMI_Cred"
    Me.LblDevIMI_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDevIMI_Cred.TabIndex = 49
    Me.LblDevIMI_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevIR_Cred
    '
    Me.LblDevIR_Cred.BackColor = System.Drawing.Color.White
    Me.LblDevIR_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevIR_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevIR_Cred.Location = New System.Drawing.Point(426, 105)
    Me.LblDevIR_Cred.Name = "LblDevIR_Cred"
    Me.LblDevIR_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDevIR_Cred.TabIndex = 48
    Me.LblDevIR_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevIVA_Cred
    '
    Me.LblDevIVA_Cred.BackColor = System.Drawing.Color.White
    Me.LblDevIVA_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevIVA_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevIVA_Cred.Location = New System.Drawing.Point(349, 106)
    Me.LblDevIVA_Cred.Name = "LblDevIVA_Cred"
    Me.LblDevIVA_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDevIVA_Cred.TabIndex = 47
    Me.LblDevIVA_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevBruta_Cred
    '
    Me.LblDevBruta_Cred.BackColor = System.Drawing.Color.White
    Me.LblDevBruta_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevBruta_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevBruta_Cred.Location = New System.Drawing.Point(118, 105)
    Me.LblDevBruta_Cred.Name = "LblDevBruta_Cred"
    Me.LblDevBruta_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDevBruta_Cred.TabIndex = 46
    Me.LblDevBruta_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevDesc_Cont
    '
    Me.LblDevDesc_Cont.BackColor = System.Drawing.Color.White
    Me.LblDevDesc_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevDesc_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevDesc_Cont.Location = New System.Drawing.Point(195, 84)
    Me.LblDevDesc_Cont.Name = "LblDevDesc_Cont"
    Me.LblDevDesc_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDevDesc_Cont.TabIndex = 45
    Me.LblDevDesc_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevVentaNeta_Cont
    '
    Me.LblDevVentaNeta_Cont.BackColor = System.Drawing.Color.SandyBrown
    Me.LblDevVentaNeta_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevVentaNeta_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevVentaNeta_Cont.Location = New System.Drawing.Point(581, 83)
    Me.LblDevVentaNeta_Cont.Name = "LblDevVentaNeta_Cont"
    Me.LblDevVentaNeta_Cont.Size = New System.Drawing.Size(72, 18)
    Me.LblDevVentaNeta_Cont.TabIndex = 44
    Me.LblDevVentaNeta_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevIMI_Cont
    '
    Me.LblDevIMI_Cont.BackColor = System.Drawing.Color.White
    Me.LblDevIMI_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevIMI_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevIMI_Cont.Location = New System.Drawing.Point(503, 83)
    Me.LblDevIMI_Cont.Name = "LblDevIMI_Cont"
    Me.LblDevIMI_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDevIMI_Cont.TabIndex = 43
    Me.LblDevIMI_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevIR_Cont
    '
    Me.LblDevIR_Cont.BackColor = System.Drawing.Color.White
    Me.LblDevIR_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevIR_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevIR_Cont.Location = New System.Drawing.Point(426, 83)
    Me.LblDevIR_Cont.Name = "LblDevIR_Cont"
    Me.LblDevIR_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDevIR_Cont.TabIndex = 42
    Me.LblDevIR_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevIVA_Cont
    '
    Me.LblDevIVA_Cont.BackColor = System.Drawing.Color.White
    Me.LblDevIVA_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevIVA_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevIVA_Cont.Location = New System.Drawing.Point(349, 84)
    Me.LblDevIVA_Cont.Name = "LblDevIVA_Cont"
    Me.LblDevIVA_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDevIVA_Cont.TabIndex = 41
    Me.LblDevIVA_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDevBruta_Cont
    '
    Me.LblDevBruta_Cont.BackColor = System.Drawing.Color.White
    Me.LblDevBruta_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDevBruta_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDevBruta_Cont.Location = New System.Drawing.Point(118, 83)
    Me.LblDevBruta_Cont.Name = "LblDevBruta_Cont"
    Me.LblDevBruta_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDevBruta_Cont.TabIndex = 40
    Me.LblDevBruta_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblDesc_Cred
    '
    Me.LblDesc_Cred.BackColor = System.Drawing.Color.White
    Me.LblDesc_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDesc_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDesc_Cred.Location = New System.Drawing.Point(195, 62)
    Me.LblDesc_Cred.Name = "LblDesc_Cred"
    Me.LblDesc_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblDesc_Cred.TabIndex = 39
    Me.LblDesc_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label27
    '
    Me.Label27.AutoSize = True
    Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label27.Location = New System.Drawing.Point(18, 64)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(94, 12)
    Me.Label27.TabIndex = 38
    Me.Label27.Text = "VENT. CREDITO:"
    '
    'LblVentaNeta_Cred
    '
    Me.LblVentaNeta_Cred.BackColor = System.Drawing.Color.SandyBrown
    Me.LblVentaNeta_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblVentaNeta_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblVentaNeta_Cred.Location = New System.Drawing.Point(581, 61)
    Me.LblVentaNeta_Cred.Name = "LblVentaNeta_Cred"
    Me.LblVentaNeta_Cred.Size = New System.Drawing.Size(72, 18)
    Me.LblVentaNeta_Cred.TabIndex = 37
    Me.LblVentaNeta_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblIMI_Cred
    '
    Me.LblIMI_Cred.BackColor = System.Drawing.Color.White
    Me.LblIMI_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIMI_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIMI_Cred.Location = New System.Drawing.Point(503, 61)
    Me.LblIMI_Cred.Name = "LblIMI_Cred"
    Me.LblIMI_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblIMI_Cred.TabIndex = 35
    Me.LblIMI_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblIR_Cred
    '
    Me.LblIR_Cred.BackColor = System.Drawing.Color.White
    Me.LblIR_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIR_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIR_Cred.Location = New System.Drawing.Point(426, 61)
    Me.LblIR_Cred.Name = "LblIR_Cred"
    Me.LblIR_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblIR_Cred.TabIndex = 33
    Me.LblIR_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label36
    '
    Me.Label36.AutoSize = True
    Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label36.Location = New System.Drawing.Point(17, 108)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(88, 12)
    Me.Label36.TabIndex = 32
    Me.Label36.Text = "DEV. CREDITO:"
    '
    'LblIVA_Cred
    '
    Me.LblIVA_Cred.BackColor = System.Drawing.Color.White
    Me.LblIVA_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIVA_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIVA_Cred.Location = New System.Drawing.Point(349, 62)
    Me.LblIVA_Cred.Name = "LblIVA_Cred"
    Me.LblIVA_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblIVA_Cred.TabIndex = 31
    Me.LblIVA_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblVentaBruta_Cred
    '
    Me.LblVentaBruta_Cred.BackColor = System.Drawing.Color.White
    Me.LblVentaBruta_Cred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblVentaBruta_Cred.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblVentaBruta_Cred.Location = New System.Drawing.Point(118, 61)
    Me.LblVentaBruta_Cred.Name = "LblVentaBruta_Cred"
    Me.LblVentaBruta_Cred.Size = New System.Drawing.Size(71, 18)
    Me.LblVentaBruta_Cred.TabIndex = 30
    Me.LblVentaBruta_Cred.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label43
    '
    Me.Label43.AutoSize = True
    Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label43.Location = New System.Drawing.Point(17, 42)
    Me.Label43.Name = "Label43"
    Me.Label43.Size = New System.Drawing.Size(99, 12)
    Me.Label43.TabIndex = 28
    Me.Label43.Text = "VENT. CONTADO:"
    '
    'Label44
    '
    Me.Label44.AutoSize = True
    Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label44.Location = New System.Drawing.Point(17, 86)
    Me.Label44.Name = "Label44"
    Me.Label44.Size = New System.Drawing.Size(93, 12)
    Me.Label44.TabIndex = 29
    Me.Label44.Text = "DEV. CONTADO:"
    '
    'LblDesc_Cont
    '
    Me.LblDesc_Cont.BackColor = System.Drawing.Color.White
    Me.LblDesc_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDesc_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDesc_Cont.Location = New System.Drawing.Point(195, 39)
    Me.LblDesc_Cont.Name = "LblDesc_Cont"
    Me.LblDesc_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblDesc_Cont.TabIndex = 27
    Me.LblDesc_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label29
    '
    Me.Label29.AutoSize = True
    Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label29.Location = New System.Drawing.Point(197, 24)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(69, 12)
    Me.Label29.TabIndex = 26
    Me.Label29.Text = "DESCUENTOS"
    '
    'LblVentaNeta_Cont
    '
    Me.LblVentaNeta_Cont.BackColor = System.Drawing.Color.SandyBrown
    Me.LblVentaNeta_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblVentaNeta_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblVentaNeta_Cont.Location = New System.Drawing.Point(581, 39)
    Me.LblVentaNeta_Cont.Name = "LblVentaNeta_Cont"
    Me.LblVentaNeta_Cont.Size = New System.Drawing.Size(72, 18)
    Me.LblVentaNeta_Cont.TabIndex = 25
    Me.LblVentaNeta_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label26
    '
    Me.Label26.AutoSize = True
    Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label26.Location = New System.Drawing.Point(577, 24)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(76, 12)
    Me.Label26.TabIndex = 24
    Me.Label26.Text = "VENTAS NETAS"
    Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'LblIMI_Cont
    '
    Me.LblIMI_Cont.BackColor = System.Drawing.Color.White
    Me.LblIMI_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIMI_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIMI_Cont.Location = New System.Drawing.Point(503, 38)
    Me.LblIMI_Cont.Name = "LblIMI_Cont"
    Me.LblIMI_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblIMI_Cont.TabIndex = 23
    Me.LblIMI_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label22
    '
    Me.Label22.AutoSize = True
    Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label22.Location = New System.Drawing.Point(501, 24)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(41, 12)
    Me.Label22.TabIndex = 22
    Me.Label22.Text = "IMI (1%)"
    '
    'LblIR_Cont
    '
    Me.LblIR_Cont.BackColor = System.Drawing.Color.White
    Me.LblIR_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIR_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIR_Cont.Location = New System.Drawing.Point(426, 39)
    Me.LblIR_Cont.Name = "LblIR_Cont"
    Me.LblIR_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblIR_Cont.TabIndex = 21
    Me.LblIR_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label10.Location = New System.Drawing.Point(424, 24)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(36, 12)
    Me.Label10.TabIndex = 20
    Me.Label10.Text = "IR (2%)"
    '
    'LblIVA_Cont
    '
    Me.LblIVA_Cont.BackColor = System.Drawing.Color.White
    Me.LblIVA_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIVA_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIVA_Cont.Location = New System.Drawing.Point(349, 39)
    Me.LblIVA_Cont.Name = "LblIVA_Cont"
    Me.LblIVA_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblIVA_Cont.TabIndex = 19
    Me.LblIVA_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblVentaBruta_Cont
    '
    Me.LblVentaBruta_Cont.BackColor = System.Drawing.Color.White
    Me.LblVentaBruta_Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblVentaBruta_Cont.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblVentaBruta_Cont.Location = New System.Drawing.Point(118, 39)
    Me.LblVentaBruta_Cont.Name = "LblVentaBruta_Cont"
    Me.LblVentaBruta_Cont.Size = New System.Drawing.Size(71, 18)
    Me.LblVentaBruta_Cont.TabIndex = 18
    Me.LblVentaBruta_Cont.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label17
    '
    Me.Label17.AutoSize = True
    Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label17.Location = New System.Drawing.Point(118, 24)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(71, 12)
    Me.Label17.TabIndex = 15
    Me.Label17.Text = "VENTA BRUTA"
    Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Label20
    '
    Me.Label20.AutoSize = True
    Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label20.Location = New System.Drawing.Point(347, 24)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(48, 12)
    Me.Label20.TabIndex = 17
    Me.Label20.Text = "IVA (15%)"
    '
    'ShapeContainer1
    '
    Me.ShapeContainer1.Location = New System.Drawing.Point(3, 16)
    Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
    Me.ShapeContainer1.Name = "ShapeContainer1"
    Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape3})
    Me.ShapeContainer1.Size = New System.Drawing.Size(665, 145)
    Me.ShapeContainer1.TabIndex = 59
    Me.ShapeContainer1.TabStop = False
    '
    'LineShape3
    '
    Me.LineShape3.BorderWidth = 3
    Me.LineShape3.Name = "LineShape3"
    Me.LineShape3.X1 = 115
    Me.LineShape3.X2 = 649
    Me.LineShape3.Y1 = 111
    Me.LineShape3.Y2 = 111
    '
    'LblDiferencia
    '
    Me.LblDiferencia.BackColor = System.Drawing.Color.LightSalmon
    Me.LblDiferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblDiferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblDiferencia.ForeColor = System.Drawing.Color.Red
    Me.LblDiferencia.Location = New System.Drawing.Point(708, 220)
    Me.LblDiferencia.Name = "LblDiferencia"
    Me.LblDiferencia.Size = New System.Drawing.Size(82, 18)
    Me.LblDiferencia.TabIndex = 0
    Me.LblDiferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblCodCierre
    '
    Me.LblCodCierre.BackColor = System.Drawing.Color.White
    Me.LblCodCierre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblCodCierre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblCodCierre.Location = New System.Drawing.Point(40, 13)
    Me.LblCodCierre.Name = "LblCodCierre"
    Me.LblCodCierre.Size = New System.Drawing.Size(80, 19)
    Me.LblCodCierre.TabIndex = 25
    Me.LblCodCierre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label9
    '
    Me.Label9.AutoSize = True
    Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label9.Location = New System.Drawing.Point(706, 208)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(61, 12)
    Me.Label9.TabIndex = 25
    Me.Label9.Text = "Diferencia:"
    '
    'Label40
    '
    Me.Label40.AutoSize = True
    Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label40.Location = New System.Drawing.Point(14, 17)
    Me.Label40.Name = "Label40"
    Me.Label40.Size = New System.Drawing.Size(24, 13)
    Me.Label40.TabIndex = 26
    Me.Label40.Text = "ID:"
    '
    'LblTasaCambio
    '
    Me.LblTasaCambio.BackColor = System.Drawing.Color.White
    Me.LblTasaCambio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTasaCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTasaCambio.Location = New System.Drawing.Point(714, 14)
    Me.LblTasaCambio.Name = "LblTasaCambio"
    Me.LblTasaCambio.Size = New System.Drawing.Size(84, 19)
    Me.LblTasaCambio.TabIndex = 22
    Me.LblTasaCambio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label15
    '
    Me.Label15.AutoSize = True
    Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label15.Location = New System.Drawing.Point(706, 164)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(78, 12)
    Me.Label15.TabIndex = 23
    Me.Label15.Text = "Por Depositar:"
    '
    'Label33
    '
    Me.Label33.AutoSize = True
    Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label33.Location = New System.Drawing.Point(611, 19)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(102, 13)
    Me.Label33.TabIndex = 23
    Me.Label33.Text = "Tasa de Cambio:"
    '
    'CmbCajas
    '
    Me.CmbCajas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.CmbCajas.FormattingEnabled = True
    Me.CmbCajas.Items.AddRange(New Object() {"Sucursal Montoya", "Sucursal Guanacaste", "Oficina Cartera y Cobro"})
    Me.CmbCajas.Location = New System.Drawing.Point(361, 14)
    Me.CmbCajas.Name = "CmbCajas"
    Me.CmbCajas.Size = New System.Drawing.Size(238, 21)
    Me.CmbCajas.TabIndex = 9
    '
    'LblPorDepositar
    '
    Me.LblPorDepositar.BackColor = System.Drawing.Color.SandyBrown
    Me.LblPorDepositar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblPorDepositar.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblPorDepositar.Location = New System.Drawing.Point(706, 178)
    Me.LblPorDepositar.Name = "LblPorDepositar"
    Me.LblPorDepositar.Size = New System.Drawing.Size(84, 18)
    Me.LblPorDepositar.TabIndex = 21
    Me.LblPorDepositar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.TxtAnticiposAplicados)
    Me.GroupBox1.Controls.Add(Me.Label21)
    Me.GroupBox1.Controls.Add(Me.txtRecibosAnticiposManual)
    Me.GroupBox1.Controls.Add(Me.Label32)
    Me.GroupBox1.Location = New System.Drawing.Point(690, 47)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(113, 109)
    Me.GroupBox1.TabIndex = 2
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Anticipos"
    '
    'TxtAnticiposAplicados
    '
    Me.TxtAnticiposAplicados.BackColor = System.Drawing.Color.AliceBlue
    Me.TxtAnticiposAplicados.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.TxtAnticiposAplicados.Location = New System.Drawing.Point(18, 38)
    Me.TxtAnticiposAplicados.Name = "TxtAnticiposAplicados"
    Me.TxtAnticiposAplicados.Size = New System.Drawing.Size(82, 18)
    Me.TxtAnticiposAplicados.TabIndex = 25
    Me.TxtAnticiposAplicados.Tag = "c"
    '
    'Label21
    '
    Me.Label21.AutoSize = True
    Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label21.Location = New System.Drawing.Point(16, 23)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(62, 12)
    Me.Label21.TabIndex = 24
    Me.Label21.Text = "Aplicados :"
    '
    'txtRecibosAnticiposManual
    '
    Me.txtRecibosAnticiposManual.BackColor = System.Drawing.Color.AliceBlue
    Me.txtRecibosAnticiposManual.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtRecibosAnticiposManual.Location = New System.Drawing.Point(17, 76)
    Me.txtRecibosAnticiposManual.Name = "txtRecibosAnticiposManual"
    Me.txtRecibosAnticiposManual.Size = New System.Drawing.Size(83, 18)
    Me.txtRecibosAnticiposManual.TabIndex = 22
    '
    'Label32
    '
    Me.Label32.AutoSize = True
    Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label32.Location = New System.Drawing.Point(16, 64)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(62, 12)
    Me.Label32.TabIndex = 20
    Me.Label32.Text = "Recibidos :"
    '
    'dtpFechaCierre
    '
    Me.dtpFechaCierre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dtpFechaCierre.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpFechaCierre.Location = New System.Drawing.Point(198, 13)
    Me.dtpFechaCierre.Name = "dtpFechaCierre"
    Me.dtpFechaCierre.Size = New System.Drawing.Size(103, 20)
    Me.dtpFechaCierre.TabIndex = 8
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.Label25)
    Me.GroupBox2.Controls.Add(Me.txtTransDol)
    Me.GroupBox2.Controls.Add(Me.txtTransfCord)
    Me.GroupBox2.Controls.Add(Me.LblTotalTransf)
    Me.GroupBox2.Controls.Add(Me.LblEquivTransfCord)
    Me.GroupBox2.Controls.Add(Me.txtTarjDol)
    Me.GroupBox2.Controls.Add(Me.txtCheckDol)
    Me.GroupBox2.Controls.Add(Me.txtEfectDol)
    Me.GroupBox2.Controls.Add(Me.txtTarjCord)
    Me.GroupBox2.Controls.Add(Me.txtCheckCord)
    Me.GroupBox2.Controls.Add(Me.txtEfectCord)
    Me.GroupBox2.Controls.Add(Me.LblTotalDeposito)
    Me.GroupBox2.Controls.Add(Me.LblTotalTarj)
    Me.GroupBox2.Controls.Add(Me.LblTotalDepCS)
    Me.GroupBox2.Controls.Add(Me.LblTotalCheck)
    Me.GroupBox2.Controls.Add(Me.LblTotalEfec)
    Me.GroupBox2.Controls.Add(Me.Label23)
    Me.GroupBox2.Controls.Add(Me.LblIngresosEquivCS)
    Me.GroupBox2.Controls.Add(Me.LblIngresosUSS)
    Me.GroupBox2.Controls.Add(Me.LblEquivTarjCord)
    Me.GroupBox2.Controls.Add(Me.Label19)
    Me.GroupBox2.Controls.Add(Me.Label18)
    Me.GroupBox2.Controls.Add(Me.LblTotalDepEquiv)
    Me.GroupBox2.Controls.Add(Me.LblEquivChkCord)
    Me.GroupBox2.Controls.Add(Me.LblTotalDepDol)
    Me.GroupBox2.Controls.Add(Me.LblTotalDepCord)
    Me.GroupBox2.Controls.Add(Me.Label14)
    Me.GroupBox2.Controls.Add(Me.LblEquivEfecCord)
    Me.GroupBox2.Controls.Add(Me.Label8)
    Me.GroupBox2.Controls.Add(Me.LblIngresosCS)
    Me.GroupBox2.Controls.Add(Me.Label11)
    Me.GroupBox2.Controls.Add(Me.Label12)
    Me.GroupBox2.Controls.Add(Me.Label5)
    Me.GroupBox2.Controls.Add(Me.Label3)
    Me.GroupBox2.Location = New System.Drawing.Point(10, 217)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(671, 137)
    Me.GroupBox2.TabIndex = 3
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Detalles del Arqueo"
    '
    'Label25
    '
    Me.Label25.AutoSize = True
    Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label25.Location = New System.Drawing.Point(297, 23)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(80, 12)
    Me.Label25.TabIndex = 50
    Me.Label25.Text = "Transferencias"
    '
    'txtTransDol
    '
    Me.txtTransDol.BackColor = System.Drawing.Color.AliceBlue
    Me.txtTransDol.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtTransDol.Location = New System.Drawing.Point(299, 60)
    Me.txtTransDol.Name = "txtTransDol"
    Me.txtTransDol.Size = New System.Drawing.Size(84, 18)
    Me.txtTransDol.TabIndex = 49
    Me.txtTransDol.Text = "0.00"
    Me.txtTransDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtTransfCord
    '
    Me.txtTransfCord.BackColor = System.Drawing.Color.AliceBlue
    Me.txtTransfCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtTransfCord.Location = New System.Drawing.Point(299, 38)
    Me.txtTransfCord.Name = "txtTransfCord"
    Me.txtTransfCord.Size = New System.Drawing.Size(84, 18)
    Me.txtTransfCord.TabIndex = 48
    Me.txtTransfCord.Text = "0.00"
    Me.txtTransfCord.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'LblTotalTransf
    '
    Me.LblTotalTransf.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalTransf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalTransf.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalTransf.Location = New System.Drawing.Point(299, 105)
    Me.LblTotalTransf.Name = "LblTotalTransf"
    Me.LblTotalTransf.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalTransf.TabIndex = 47
    Me.LblTotalTransf.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblEquivTransfCord
    '
    Me.LblEquivTransfCord.BackColor = System.Drawing.Color.White
    Me.LblEquivTransfCord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblEquivTransfCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblEquivTransfCord.Location = New System.Drawing.Point(299, 82)
    Me.LblEquivTransfCord.Name = "LblEquivTransfCord"
    Me.LblEquivTransfCord.Size = New System.Drawing.Size(84, 18)
    Me.LblEquivTransfCord.TabIndex = 46
    Me.LblEquivTransfCord.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'txtTarjDol
    '
    Me.txtTarjDol.BackColor = System.Drawing.Color.AliceBlue
    Me.txtTarjDol.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtTarjDol.Location = New System.Drawing.Point(479, 60)
    Me.txtTarjDol.Name = "txtTarjDol"
    Me.txtTarjDol.Size = New System.Drawing.Size(84, 18)
    Me.txtTarjDol.TabIndex = 45
    Me.txtTarjDol.Text = "0.00"
    Me.txtTarjDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtCheckDol
    '
    Me.txtCheckDol.BackColor = System.Drawing.Color.AliceBlue
    Me.txtCheckDol.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtCheckDol.Location = New System.Drawing.Point(208, 60)
    Me.txtCheckDol.Name = "txtCheckDol"
    Me.txtCheckDol.Size = New System.Drawing.Size(84, 18)
    Me.txtCheckDol.TabIndex = 44
    Me.txtCheckDol.Text = "0.00"
    Me.txtCheckDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtEfectDol
    '
    Me.txtEfectDol.BackColor = System.Drawing.Color.AliceBlue
    Me.txtEfectDol.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtEfectDol.Location = New System.Drawing.Point(118, 60)
    Me.txtEfectDol.Name = "txtEfectDol"
    Me.txtEfectDol.Size = New System.Drawing.Size(84, 18)
    Me.txtEfectDol.TabIndex = 43
    Me.txtEfectDol.Text = "0.00"
    Me.txtEfectDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtTarjCord
    '
    Me.txtTarjCord.BackColor = System.Drawing.Color.AliceBlue
    Me.txtTarjCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtTarjCord.Location = New System.Drawing.Point(479, 38)
    Me.txtTarjCord.Name = "txtTarjCord"
    Me.txtTarjCord.Size = New System.Drawing.Size(84, 18)
    Me.txtTarjCord.TabIndex = 42
    Me.txtTarjCord.Text = "0.00"
    Me.txtTarjCord.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtCheckCord
    '
    Me.txtCheckCord.BackColor = System.Drawing.Color.AliceBlue
    Me.txtCheckCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtCheckCord.Location = New System.Drawing.Point(208, 38)
    Me.txtCheckCord.Name = "txtCheckCord"
    Me.txtCheckCord.Size = New System.Drawing.Size(84, 18)
    Me.txtCheckCord.TabIndex = 41
    Me.txtCheckCord.Text = "0.00"
    Me.txtCheckCord.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txtEfectCord
    '
    Me.txtEfectCord.BackColor = System.Drawing.Color.AliceBlue
    Me.txtEfectCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtEfectCord.Location = New System.Drawing.Point(118, 38)
    Me.txtEfectCord.Name = "txtEfectCord"
    Me.txtEfectCord.Size = New System.Drawing.Size(84, 18)
    Me.txtEfectCord.TabIndex = 40
    Me.txtEfectCord.Text = "0.00"
    Me.txtEfectCord.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'LblTotalDeposito
    '
    Me.LblTotalDeposito.BackColor = System.Drawing.Color.SandyBrown
    Me.LblTotalDeposito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalDeposito.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalDeposito.Location = New System.Drawing.Point(569, 105)
    Me.LblTotalDeposito.Name = "LblTotalDeposito"
    Me.LblTotalDeposito.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalDeposito.TabIndex = 39
    Me.LblTotalDeposito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblTotalTarj
    '
    Me.LblTotalTarj.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalTarj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalTarj.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalTarj.Location = New System.Drawing.Point(479, 105)
    Me.LblTotalTarj.Name = "LblTotalTarj"
    Me.LblTotalTarj.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalTarj.TabIndex = 38
    Me.LblTotalTarj.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblTotalDepCS
    '
    Me.LblTotalDepCS.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalDepCS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalDepCS.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalDepCS.Location = New System.Drawing.Point(389, 105)
    Me.LblTotalDepCS.Name = "LblTotalDepCS"
    Me.LblTotalDepCS.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalDepCS.TabIndex = 37
    Me.LblTotalDepCS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblTotalCheck
    '
    Me.LblTotalCheck.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalCheck.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalCheck.Location = New System.Drawing.Point(208, 105)
    Me.LblTotalCheck.Name = "LblTotalCheck"
    Me.LblTotalCheck.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalCheck.TabIndex = 36
    Me.LblTotalCheck.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblTotalEfec
    '
    Me.LblTotalEfec.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalEfec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalEfec.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalEfec.Location = New System.Drawing.Point(118, 105)
    Me.LblTotalEfec.Name = "LblTotalEfec"
    Me.LblTotalEfec.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalEfec.TabIndex = 35
    Me.LblTotalEfec.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label23
    '
    Me.Label23.AutoSize = True
    Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label23.Location = New System.Drawing.Point(21, 108)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(51, 12)
    Me.Label23.TabIndex = 34
    Me.Label23.Text = "Total C$:"
    '
    'LblIngresosEquivCS
    '
    Me.LblIngresosEquivCS.BackColor = System.Drawing.Color.Gainsboro
    Me.LblIngresosEquivCS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIngresosEquivCS.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIngresosEquivCS.Location = New System.Drawing.Point(569, 83)
    Me.LblIngresosEquivCS.Name = "LblIngresosEquivCS"
    Me.LblIngresosEquivCS.Size = New System.Drawing.Size(84, 18)
    Me.LblIngresosEquivCS.TabIndex = 33
    Me.LblIngresosEquivCS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblIngresosUSS
    '
    Me.LblIngresosUSS.BackColor = System.Drawing.Color.Gainsboro
    Me.LblIngresosUSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIngresosUSS.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIngresosUSS.Location = New System.Drawing.Point(569, 60)
    Me.LblIngresosUSS.Name = "LblIngresosUSS"
    Me.LblIngresosUSS.Size = New System.Drawing.Size(84, 18)
    Me.LblIngresosUSS.TabIndex = 32
    Me.LblIngresosUSS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblEquivTarjCord
    '
    Me.LblEquivTarjCord.BackColor = System.Drawing.Color.White
    Me.LblEquivTarjCord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblEquivTarjCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblEquivTarjCord.Location = New System.Drawing.Point(479, 83)
    Me.LblEquivTarjCord.Name = "LblEquivTarjCord"
    Me.LblEquivTarjCord.Size = New System.Drawing.Size(84, 18)
    Me.LblEquivTarjCord.TabIndex = 31
    Me.LblEquivTarjCord.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label19
    '
    Me.Label19.AutoSize = True
    Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label19.Location = New System.Drawing.Point(567, 23)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(49, 12)
    Me.Label19.TabIndex = 30
    Me.Label19.Text = "Ingresos"
    '
    'Label18
    '
    Me.Label18.AutoSize = True
    Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label18.Location = New System.Drawing.Point(21, 86)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(86, 12)
    Me.Label18.TabIndex = 29
    Me.Label18.Text = "Equivalente C$:"
    '
    'LblTotalDepEquiv
    '
    Me.LblTotalDepEquiv.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalDepEquiv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalDepEquiv.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalDepEquiv.Location = New System.Drawing.Point(389, 82)
    Me.LblTotalDepEquiv.Name = "LblTotalDepEquiv"
    Me.LblTotalDepEquiv.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalDepEquiv.TabIndex = 28
    Me.LblTotalDepEquiv.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblEquivChkCord
    '
    Me.LblEquivChkCord.BackColor = System.Drawing.Color.White
    Me.LblEquivChkCord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblEquivChkCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblEquivChkCord.Location = New System.Drawing.Point(208, 82)
    Me.LblEquivChkCord.Name = "LblEquivChkCord"
    Me.LblEquivChkCord.Size = New System.Drawing.Size(84, 18)
    Me.LblEquivChkCord.TabIndex = 27
    Me.LblEquivChkCord.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblTotalDepDol
    '
    Me.LblTotalDepDol.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalDepDol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalDepDol.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalDepDol.Location = New System.Drawing.Point(389, 60)
    Me.LblTotalDepDol.Name = "LblTotalDepDol"
    Me.LblTotalDepDol.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalDepDol.TabIndex = 26
    Me.LblTotalDepDol.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'LblTotalDepCord
    '
    Me.LblTotalDepCord.BackColor = System.Drawing.Color.Gainsboro
    Me.LblTotalDepCord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblTotalDepCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTotalDepCord.Location = New System.Drawing.Point(389, 38)
    Me.LblTotalDepCord.Name = "LblTotalDepCord"
    Me.LblTotalDepCord.Size = New System.Drawing.Size(84, 18)
    Me.LblTotalDepCord.TabIndex = 25
    Me.LblTotalDepCord.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label14
    '
    Me.Label14.AutoSize = True
    Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label14.Location = New System.Drawing.Point(481, 23)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(46, 12)
    Me.Label14.TabIndex = 24
    Me.Label14.Text = "Tarjetas"
    '
    'LblEquivEfecCord
    '
    Me.LblEquivEfecCord.BackColor = System.Drawing.Color.White
    Me.LblEquivEfecCord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblEquivEfecCord.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblEquivEfecCord.Location = New System.Drawing.Point(118, 82)
    Me.LblEquivEfecCord.Name = "LblEquivEfecCord"
    Me.LblEquivEfecCord.Size = New System.Drawing.Size(84, 18)
    Me.LblEquivEfecCord.TabIndex = 6
    Me.LblEquivEfecCord.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label8
    '
    Me.Label8.AutoSize = True
    Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label8.Location = New System.Drawing.Point(387, 23)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(50, 12)
    Me.Label8.TabIndex = 19
    Me.Label8.Text = "Deposito"
    '
    'LblIngresosCS
    '
    Me.LblIngresosCS.BackColor = System.Drawing.Color.Gainsboro
    Me.LblIngresosCS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.LblIngresosCS.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblIngresosCS.Location = New System.Drawing.Point(569, 38)
    Me.LblIngresosCS.Name = "LblIngresosCS"
    Me.LblIngresosCS.Size = New System.Drawing.Size(84, 18)
    Me.LblIngresosCS.TabIndex = 5
    Me.LblIngresosCS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label11
    '
    Me.Label11.AutoSize = True
    Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label11.Location = New System.Drawing.Point(206, 23)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(49, 12)
    Me.Label11.TabIndex = 18
    Me.Label11.Text = "Cheques"
    '
    'Label12
    '
    Me.Label12.AutoSize = True
    Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label12.Location = New System.Drawing.Point(116, 23)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(48, 12)
    Me.Label12.TabIndex = 17
    Me.Label12.Text = "Efectivo"
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(21, 63)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(48, 12)
    Me.Label5.TabIndex = 11
    Me.Label5.Text = "Dolares:"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(21, 41)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(57, 12)
    Me.Label3.TabIndex = 9
    Me.Label3.Text = "Cordobas:"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(326, 16)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(36, 13)
    Me.Label1.TabIndex = 7
    Me.Label1.Text = "Caja:"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label2.Location = New System.Drawing.Point(150, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(46, 13)
    Me.Label2.TabIndex = 6
    Me.Label2.Text = "Fecha:"
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.xtpBancoyContab)
    Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel3.Location = New System.Drawing.Point(0, 372)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(821, 236)
    Me.Panel3.TabIndex = 34
    '
    'xtpBancoyContab
    '
    Me.xtpBancoyContab.Dock = System.Windows.Forms.DockStyle.Fill
    Me.xtpBancoyContab.Location = New System.Drawing.Point(0, 0)
    Me.xtpBancoyContab.Name = "xtpBancoyContab"
    Me.xtpBancoyContab.SelectedTabPage = Me.xtpDepBanco
    Me.xtpBancoyContab.Size = New System.Drawing.Size(821, 236)
    Me.xtpBancoyContab.TabIndex = 31
    Me.xtpBancoyContab.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpDepBanco, Me.xtpContabCierre, Me.xtpContabDep})
    '
    'xtpDepBanco
    '
    Me.xtpDepBanco.Controls.Add(Me.GroupBox3)
    Me.xtpDepBanco.Name = "xtpDepBanco"
    Me.xtpDepBanco.Size = New System.Drawing.Size(815, 208)
    Me.xtpDepBanco.Text = "Deposito Banco"
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.Label13)
    Me.GroupBox3.Controls.Add(Me.Label7)
    Me.GroupBox3.Controls.Add(Me.luBancos)
    Me.GroupBox3.Controls.Add(Me.txtDepositoBanco)
    Me.GroupBox3.Controls.Add(Me.btnElimDep)
    Me.GroupBox3.Controls.Add(Me.btnAddDep)
    Me.GroupBox3.Controls.Add(Me.GridControl1)
    Me.GroupBox3.Controls.Add(Me.Label41)
    Me.GroupBox3.Controls.Add(Me.CmbTipo)
    Me.GroupBox3.Controls.Add(Me.dtpFechaDep)
    Me.GroupBox3.Controls.Add(Me.Label16)
    Me.GroupBox3.Controls.Add(Me.Label37)
    Me.GroupBox3.Controls.Add(Me.Label38)
    Me.GroupBox3.Controls.Add(Me.ShapeContainer2)
    Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(815, 208)
    Me.GroupBox3.TabIndex = 24
    Me.GroupBox3.TabStop = False
    Me.GroupBox3.Text = "Detalle de Depositos"
    '
    'Label13
    '
    Me.Label13.AutoSize = True
    Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label13.Location = New System.Drawing.Point(688, 150)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(66, 12)
    Me.Label13.TabIndex = 49
    Me.Label13.Text = "Depositado:"
    '
    'Label7
    '
    Me.Label7.BackColor = System.Drawing.Color.SandyBrown
    Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label7.Location = New System.Drawing.Point(690, 162)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(86, 18)
    Me.Label7.TabIndex = 48
    Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'luBancos
    '
    Me.luBancos.Location = New System.Drawing.Point(385, 23)
    Me.luBancos.Name = "luBancos"
    Me.luBancos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.luBancos.Properties.Appearance.Options.UseFont = True
    Me.luBancos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luBancos.Properties.DataSource = Me.BS_Bancos
    Me.luBancos.Size = New System.Drawing.Size(200, 18)
    Me.luBancos.TabIndex = 47
    '
    'txtDepositoBanco
    '
    Me.txtDepositoBanco.BackColor = System.Drawing.Color.AliceBlue
    Me.txtDepositoBanco.Location = New System.Drawing.Point(690, 22)
    Me.txtDepositoBanco.Name = "txtDepositoBanco"
    Me.txtDepositoBanco.Size = New System.Drawing.Size(86, 20)
    Me.txtDepositoBanco.TabIndex = 46
    Me.txtDepositoBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'btnElimDep
    '
    Me.btnElimDep.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.btnElimDep.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnElimDep.Enabled = False
    Me.btnElimDep.Image = CType(resources.GetObject("btnElimDep.Image"), System.Drawing.Image)
    Me.btnElimDep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.btnElimDep.Location = New System.Drawing.Point(691, 99)
    Me.btnElimDep.Name = "btnElimDep"
    Me.btnElimDep.Size = New System.Drawing.Size(111, 35)
    Me.btnElimDep.TabIndex = 29
    Me.btnElimDep.Text = "&Eliminar Deposito"
    Me.btnElimDep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.btnElimDep.UseVisualStyleBackColor = True
    '
    'btnAddDep
    '
    Me.btnAddDep.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.btnAddDep.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnAddDep.Enabled = False
    Me.btnAddDep.Image = CType(resources.GetObject("btnAddDep.Image"), System.Drawing.Image)
    Me.btnAddDep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.btnAddDep.Location = New System.Drawing.Point(691, 58)
    Me.btnAddDep.Name = "btnAddDep"
    Me.btnAddDep.Size = New System.Drawing.Size(111, 35)
    Me.btnAddDep.TabIndex = 28
    Me.btnAddDep.Text = "&Agregar Deposito"
    Me.btnAddDep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.btnAddDep.UseVisualStyleBackColor = True
    '
    'GridControl1
    '
    Me.GridControl1.DataSource = Me.BS_DetalleDeposito
    Me.GridControl1.Location = New System.Drawing.Point(19, 58)
    Me.GridControl1.MainView = Me.GridView1
    Me.GridControl1.Name = "GridControl1"
    Me.GridControl1.Size = New System.Drawing.Size(648, 132)
    Me.GridControl1.TabIndex = 26
    Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
    '
    'GridView1
    '
    Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
    Me.GridView1.GridControl = Me.GridControl1
    Me.GridView1.Name = "GridView1"
    Me.GridView1.OptionsBehavior.Editable = False
    Me.GridView1.OptionsBehavior.ReadOnly = True
    Me.GridView1.OptionsCustomization.AllowColumnMoving = False
    Me.GridView1.OptionsCustomization.AllowColumnResizing = False
    Me.GridView1.OptionsCustomization.AllowGroup = False
    Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
    Me.GridView1.OptionsView.ShowGroupPanel = False
    '
    'Label41
    '
    Me.Label41.AutoSize = True
    Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label41.Location = New System.Drawing.Point(608, 27)
    Me.Label41.Name = "Label41"
    Me.Label41.Size = New System.Drawing.Size(80, 12)
    Me.Label41.TabIndex = 25
    Me.Label41.Text = "Este Deposito:"
    '
    'CmbTipo
    '
    Me.CmbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.CmbTipo.FormattingEnabled = True
    Me.CmbTipo.Items.AddRange(New Object() {"EFECTIVO", "CHEQUE", "TRANSFERENCIA", "TARJETA"})
    Me.CmbTipo.Location = New System.Drawing.Point(173, 23)
    Me.CmbTipo.Name = "CmbTipo"
    Me.CmbTipo.Size = New System.Drawing.Size(114, 20)
    Me.CmbTipo.TabIndex = 23
    '
    'dtpFechaDep
    '
    Me.dtpFechaDep.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dtpFechaDep.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpFechaDep.Location = New System.Drawing.Point(62, 23)
    Me.dtpFechaDep.Name = "dtpFechaDep"
    Me.dtpFechaDep.Size = New System.Drawing.Size(70, 18)
    Me.dtpFechaDep.TabIndex = 22
    '
    'Label16
    '
    Me.Label16.AutoSize = True
    Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label16.Location = New System.Drawing.Point(300, 27)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(79, 12)
    Me.Label16.TabIndex = 20
    Me.Label16.Text = "Cuenta Banco:"
    '
    'Label37
    '
    Me.Label37.AutoSize = True
    Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label37.Location = New System.Drawing.Point(17, 27)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(40, 12)
    Me.Label37.TabIndex = 15
    Me.Label37.Text = "Fecha:"
    '
    'Label38
    '
    Me.Label38.AutoSize = True
    Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label38.Location = New System.Drawing.Point(144, 26)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(30, 12)
    Me.Label38.TabIndex = 17
    Me.Label38.Text = "Tipo:"
    '
    'ShapeContainer2
    '
    Me.ShapeContainer2.Location = New System.Drawing.Point(3, 16)
    Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
    Me.ShapeContainer2.Name = "ShapeContainer2"
    Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2, Me.LineShape1})
    Me.ShapeContainer2.Size = New System.Drawing.Size(809, 189)
    Me.ShapeContainer2.TabIndex = 27
    Me.ShapeContainer2.TabStop = False
    '
    'LineShape2
    '
    Me.LineShape2.BorderColor = System.Drawing.SystemColors.ControlLightLight
    Me.LineShape2.Name = "LineShape2"
    Me.LineShape2.X1 = 240
    Me.LineShape2.X2 = 798
    Me.LineShape2.Y1 = -16
    Me.LineShape2.Y2 = -16
    '
    'LineShape1
    '
    Me.LineShape1.BorderWidth = 3
    Me.LineShape1.Name = "LineShape1"
    Me.LineShape1.X1 = 241
    Me.LineShape1.X2 = 799
    Me.LineShape1.Y1 = -16
    Me.LineShape1.Y2 = -16
    '
    'xtpContabCierre
    '
    Me.xtpContabCierre.Controls.Add(Me.GridControl2)
    Me.xtpContabCierre.Name = "xtpContabCierre"
    Me.xtpContabCierre.Size = New System.Drawing.Size(815, 208)
    Me.xtpContabCierre.Text = "Contabilización Cierre"
    '
    'GridControl2
    '
    Me.GridControl2.DataSource = Me.BS_DetalleDeposito
    Me.GridControl2.Location = New System.Drawing.Point(8, 12)
    Me.GridControl2.MainView = Me.GridView2
    Me.GridControl2.Name = "GridControl2"
    Me.GridControl2.Size = New System.Drawing.Size(947, 187)
    Me.GridControl2.TabIndex = 27
    Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
    '
    'GridView2
    '
    Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
    Me.GridView2.GridControl = Me.GridControl2
    Me.GridView2.Name = "GridView2"
    Me.GridView2.OptionsBehavior.Editable = False
    Me.GridView2.OptionsBehavior.ReadOnly = True
    Me.GridView2.OptionsCustomization.AllowColumnMoving = False
    Me.GridView2.OptionsCustomization.AllowColumnResizing = False
    Me.GridView2.OptionsCustomization.AllowGroup = False
    Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
    Me.GridView2.OptionsView.ShowGroupPanel = False
    '
    'xtpContabDep
    '
    Me.xtpContabDep.Controls.Add(Me.GridControl3)
    Me.xtpContabDep.Name = "xtpContabDep"
    Me.xtpContabDep.Size = New System.Drawing.Size(815, 208)
    Me.xtpContabDep.Text = "Contabilización Depositos"
    '
    'GridControl3
    '
    Me.GridControl3.DataSource = Me.BS_DetalleDeposito
    Me.GridControl3.Location = New System.Drawing.Point(10, 14)
    Me.GridControl3.MainView = Me.GridView3
    Me.GridControl3.Name = "GridControl3"
    Me.GridControl3.Size = New System.Drawing.Size(947, 187)
    Me.GridControl3.TabIndex = 28
    Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
    '
    'GridView3
    '
    Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
    Me.GridView3.GridControl = Me.GridControl3
    Me.GridView3.Name = "GridView3"
    Me.GridView3.OptionsBehavior.Editable = False
    Me.GridView3.OptionsBehavior.ReadOnly = True
    Me.GridView3.OptionsCustomization.AllowColumnMoving = False
    Me.GridView3.OptionsCustomization.AllowColumnResizing = False
    Me.GridView3.OptionsCustomization.AllowGroup = False
    Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
    Me.GridView3.OptionsView.ShowGroupPanel = False
    '
    'FrmCierreTransacciones
    '
    Me.AcceptButton = Me.cmdAceptar
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.cmdCancelar
    Me.ClientSize = New System.Drawing.Size(821, 657)
    Me.Controls.Add(Me.Panel3)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Panel1)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "FrmCierreTransacciones"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Cierre de Operaciones del día"
    CType(Me.BS_Bancos, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.BS_DetalleDeposito, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox4.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    CType(Me.xtpBancoyContab, System.ComponentModel.ISupportInitialize).EndInit()
    Me.xtpBancoyContab.ResumeLayout(False)
    Me.xtpDepBanco.ResumeLayout(False)
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    CType(Me.luBancos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.xtpContabCierre.ResumeLayout(False)
    CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
    Me.xtpContabDep.ResumeLayout(False)
    CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents BS_DetalleDeposito As System.Windows.Forms.BindingSource
    Friend WithEvents BS_Bancos As System.Windows.Forms.BindingSource
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents LblDesc_Total As System.Windows.Forms.Label
    Friend WithEvents LblVentaNeta_Total As System.Windows.Forms.Label
    Friend WithEvents LblIMI_Total As System.Windows.Forms.Label
    Friend WithEvents LblIR_Total As System.Windows.Forms.Label
    Friend WithEvents LblIVA_Total As System.Windows.Forms.Label
    Friend WithEvents LblVentaBruta_Total As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents LblDevDesc_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevVentaNeta_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevIMI_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevIR_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevIVA_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevBruta_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevDesc_Cont As System.Windows.Forms.Label
    Friend WithEvents LblDevVentaNeta_Cont As System.Windows.Forms.Label
    Friend WithEvents LblDevIMI_Cont As System.Windows.Forms.Label
    Friend WithEvents LblDevIR_Cont As System.Windows.Forms.Label
    Friend WithEvents LblDevIVA_Cont As System.Windows.Forms.Label
    Friend WithEvents LblDevBruta_Cont As System.Windows.Forms.Label
    Friend WithEvents LblDesc_Cred As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents LblVentaNeta_Cred As System.Windows.Forms.Label
    Friend WithEvents LblIMI_Cred As System.Windows.Forms.Label
    Friend WithEvents LblIR_Cred As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents LblIVA_Cred As System.Windows.Forms.Label
    Friend WithEvents LblVentaBruta_Cred As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents LblDesc_Cont As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents LblVentaNeta_Cont As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents LblIMI_Cont As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents LblIR_Cont As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LblIVA_Cont As System.Windows.Forms.Label
    Friend WithEvents LblVentaBruta_Cont As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape3 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LblDiferencia As System.Windows.Forms.Label
    Friend WithEvents LblCodCierre As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents LblTasaCambio As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents CmbCajas As System.Windows.Forms.ComboBox
    Friend WithEvents LblPorDepositar As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TxtAnticiposAplicados As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtRecibosAnticiposManual As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaCierre As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtTransDol As System.Windows.Forms.TextBox
    Friend WithEvents txtTransfCord As System.Windows.Forms.TextBox
    Friend WithEvents LblTotalTransf As System.Windows.Forms.Label
    Friend WithEvents LblEquivTransfCord As System.Windows.Forms.Label
    Friend WithEvents txtTarjDol As System.Windows.Forms.TextBox
    Friend WithEvents txtCheckDol As System.Windows.Forms.TextBox
    Friend WithEvents txtEfectDol As System.Windows.Forms.TextBox
    Friend WithEvents txtTarjCord As System.Windows.Forms.TextBox
    Friend WithEvents txtCheckCord As System.Windows.Forms.TextBox
    Friend WithEvents txtEfectCord As System.Windows.Forms.TextBox
    Friend WithEvents LblTotalDeposito As System.Windows.Forms.Label
    Friend WithEvents LblTotalTarj As System.Windows.Forms.Label
    Friend WithEvents LblTotalDepCS As System.Windows.Forms.Label
    Friend WithEvents LblTotalCheck As System.Windows.Forms.Label
    Friend WithEvents LblTotalEfec As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents LblIngresosEquivCS As System.Windows.Forms.Label
    Friend WithEvents LblIngresosUSS As System.Windows.Forms.Label
    Friend WithEvents LblEquivTarjCord As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents LblTotalDepEquiv As System.Windows.Forms.Label
    Friend WithEvents LblEquivChkCord As System.Windows.Forms.Label
    Friend WithEvents LblTotalDepDol As System.Windows.Forms.Label
    Friend WithEvents LblTotalDepCord As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents LblEquivEfecCord As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents LblIngresosCS As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents xtpBancoyContab As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpDepBanco As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents luBancos As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtDepositoBanco As System.Windows.Forms.TextBox
    Friend WithEvents btnElimDep As System.Windows.Forms.Button
    Friend WithEvents btnAddDep As System.Windows.Forms.Button
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents CmbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFechaDep As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents xtpContabCierre As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents xtpContabDep As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LblSubtotal As System.Windows.Forms.Label
    Friend WithEvents LblDevSubtotal_Cred As System.Windows.Forms.Label
    Friend WithEvents LblDevSubtotal_Cont As System.Windows.Forms.Label
    Friend WithEvents LblSubtotal_Cred As System.Windows.Forms.Label
    Friend WithEvents LblSubtotal_Cont As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
End Class
