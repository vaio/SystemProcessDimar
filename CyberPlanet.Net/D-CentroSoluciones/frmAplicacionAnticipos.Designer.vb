﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAplicacionAnticipos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAplicacionAnticipos))
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.PanelRecibosAnticipos = New DevExpress.XtraEditors.PanelControl()
    Me.Label39 = New System.Windows.Forms.Label()
    Me.TextBox6 = New System.Windows.Forms.TextBox()
    Me.Label36 = New System.Windows.Forms.Label()
    Me.Label34 = New System.Windows.Forms.Label()
    Me.TextBox4 = New System.Windows.Forms.TextBox()
    Me.Label29 = New System.Windows.Forms.Label()
    Me.TextBox3 = New System.Windows.Forms.TextBox()
    Me.Label28 = New System.Windows.Forms.Label()
    Me.TblRecibosAnticiposBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.TblFacturasAplicadasBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.ComboBox1 = New System.Windows.Forms.ComboBox()
    Me.btnAplicar = New System.Windows.Forms.Button()
    Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
    Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.TextBox5 = New System.Windows.Forms.TextBox()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.TextBox7 = New System.Windows.Forms.TextBox()
    Me.TextBox8 = New System.Windows.Forms.TextBox()
    Me.TextBox9 = New System.Windows.Forms.TextBox()
    Me.Panel1.SuspendLayout()
    CType(Me.PanelRecibosAnticipos, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.PanelRecibosAnticipos.SuspendLayout()
    CType(Me.TblRecibosAnticiposBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TblFacturasAplicadasBS, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel2.SuspendLayout()
    CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Controls.Add(Me.btnAplicar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 305)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(677, 49)
    Me.Panel1.TabIndex = 3
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(587, 6)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(503, 6)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'PanelRecibosAnticipos
    '
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox9)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox8)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label39)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox6)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label36)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label34)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox4)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label29)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox3)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label28)
    Me.PanelRecibosAnticipos.Dock = System.Windows.Forms.DockStyle.Top
    Me.PanelRecibosAnticipos.Location = New System.Drawing.Point(0, 0)
    Me.PanelRecibosAnticipos.Name = "PanelRecibosAnticipos"
    Me.PanelRecibosAnticipos.Size = New System.Drawing.Size(677, 101)
    Me.PanelRecibosAnticipos.TabIndex = 4
    '
    'Label39
    '
    Me.Label39.AutoSize = True
    Me.Label39.Location = New System.Drawing.Point(407, 73)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(143, 13)
    Me.Label39.TabIndex = 40
    Me.Label39.Text = "Monto Anticipos Disponibles:"
    '
    'TextBox6
    '
    Me.TextBox6.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox6.Location = New System.Drawing.Point(132, 13)
    Me.TextBox6.Name = "TextBox6"
    Me.TextBox6.Size = New System.Drawing.Size(86, 20)
    Me.TextBox6.TabIndex = 4
    Me.TextBox6.Tag = "c"
    Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Label36
    '
    Me.Label36.AutoSize = True
    Me.Label36.Location = New System.Drawing.Point(24, 15)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(93, 13)
    Me.Label36.TabIndex = 38
    Me.Label36.Text = "Código de Cliente:"
    '
    'Label34
    '
    Me.Label34.AutoSize = True
    Me.Label34.Location = New System.Drawing.Point(227, 73)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(110, 13)
    Me.Label34.TabIndex = 36
    Me.Label34.Text = "Anticipos Disponibles:"
    '
    'TextBox4
    '
    Me.TextBox4.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox4.Location = New System.Drawing.Point(338, 12)
    Me.TextBox4.Name = "TextBox4"
    Me.TextBox4.Size = New System.Drawing.Size(327, 20)
    Me.TextBox4.TabIndex = 5
    Me.TextBox4.Tag = "c"
    '
    'Label29
    '
    Me.Label29.AutoSize = True
    Me.Label29.Location = New System.Drawing.Point(238, 16)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(99, 13)
    Me.Label29.TabIndex = 30
    Me.Label29.Text = "Nombre del Cliente:"
    '
    'TextBox3
    '
    Me.TextBox3.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox3.Location = New System.Drawing.Point(127, 39)
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.Size = New System.Drawing.Size(538, 20)
    Me.TextBox3.TabIndex = 6
    Me.TextBox3.Tag = "c"
    '
    'Label28
    '
    Me.Label28.AutoSize = True
    Me.Label28.Location = New System.Drawing.Point(24, 46)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(70, 13)
    Me.Label28.TabIndex = 28
    Me.Label28.Text = "Observación:"
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.TextBox7)
    Me.Panel2.Controls.Add(Me.Label5)
    Me.Panel2.Controls.Add(Me.TextBox5)
    Me.Panel2.Controls.Add(Me.TextBox2)
    Me.Panel2.Controls.Add(Me.TextBox1)
    Me.Panel2.Controls.Add(Me.Label4)
    Me.Panel2.Controls.Add(Me.Label3)
    Me.Panel2.Controls.Add(Me.Label2)
    Me.Panel2.Controls.Add(Me.GridControl2)
    Me.Panel2.Controls.Add(Me.Label1)
    Me.Panel2.Controls.Add(Me.ComboBox1)
    Me.Panel2.Location = New System.Drawing.Point(12, 112)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(652, 178)
    Me.Panel2.TabIndex = 47
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(378, 24)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(107, 13)
    Me.Label1.TabIndex = 49
    Me.Label1.Text = "Facturas Pendientes:"
    '
    'ComboBox1
    '
    Me.ComboBox1.FormattingEnabled = True
    Me.ComboBox1.Items.AddRange(New Object() {"CHEQUE", "TRANSFERENCIA"})
    Me.ComboBox1.Location = New System.Drawing.Point(491, 21)
    Me.ComboBox1.Name = "ComboBox1"
    Me.ComboBox1.Size = New System.Drawing.Size(112, 21)
    Me.ComboBox1.TabIndex = 48
    '
    'btnAplicar
    '
    Me.btnAplicar.Location = New System.Drawing.Point(68, 12)
    Me.btnAplicar.Name = "btnAplicar"
    Me.btnAplicar.Size = New System.Drawing.Size(49, 23)
    Me.btnAplicar.TabIndex = 47
    Me.btnAplicar.Text = "Button1"
    Me.btnAplicar.UseVisualStyleBackColor = True
    '
    'GridControl2
    '
    Me.GridControl2.DataSource = Me.TblRecibosAnticiposBS
    Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Left
    Me.GridControl2.Location = New System.Drawing.Point(0, 0)
    Me.GridControl2.MainView = Me.GridView2
    Me.GridControl2.Name = "GridControl2"
    Me.GridControl2.Size = New System.Drawing.Size(325, 178)
    Me.GridControl2.TabIndex = 50
    Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
    '
    'GridView2
    '
    Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
    Me.GridView2.GridControl = Me.GridControl2
    Me.GridView2.Name = "GridView2"
    Me.GridView2.OptionsBehavior.Editable = False
    Me.GridView2.OptionsBehavior.ReadOnly = True
    Me.GridView2.OptionsCustomization.AllowColumnMoving = False
    Me.GridView2.OptionsCustomization.AllowColumnResizing = False
    Me.GridView2.OptionsCustomization.AllowGroup = False
    Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
    Me.GridView2.OptionsView.ShowGroupPanel = False
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(378, 54)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(40, 13)
    Me.Label2.TabIndex = 51
    Me.Label2.Text = "Monto:"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(378, 85)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(46, 13)
    Me.Label3.TabIndex = 52
    Me.Label3.Text = "Abonos:"
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(378, 147)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(37, 13)
    Me.Label4.TabIndex = 53
    Me.Label4.Text = "Saldo:"
    '
    'TextBox1
    '
    Me.TextBox1.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox1.Location = New System.Drawing.Point(491, 51)
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.ReadOnly = True
    Me.TextBox1.Size = New System.Drawing.Size(112, 20)
    Me.TextBox1.TabIndex = 54
    Me.TextBox1.Tag = "c"
    Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'TextBox2
    '
    Me.TextBox2.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox2.Location = New System.Drawing.Point(491, 82)
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.ReadOnly = True
    Me.TextBox2.Size = New System.Drawing.Size(112, 20)
    Me.TextBox2.TabIndex = 55
    Me.TextBox2.Tag = "c"
    Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'TextBox5
    '
    Me.TextBox5.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox5.Location = New System.Drawing.Point(491, 144)
    Me.TextBox5.Name = "TextBox5"
    Me.TextBox5.ReadOnly = True
    Me.TextBox5.Size = New System.Drawing.Size(112, 20)
    Me.TextBox5.TabIndex = 56
    Me.TextBox5.Tag = "c"
    Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(378, 118)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(92, 13)
    Me.Label5.TabIndex = 57
    Me.Label5.Text = "Aplicación Actual:"
    '
    'TextBox7
    '
    Me.TextBox7.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.TextBox7.Location = New System.Drawing.Point(491, 115)
    Me.TextBox7.Name = "TextBox7"
    Me.TextBox7.Size = New System.Drawing.Size(112, 20)
    Me.TextBox7.TabIndex = 58
    Me.TextBox7.Tag = "c"
    Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'TextBox8
    '
    Me.TextBox8.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox8.Location = New System.Drawing.Point(553, 70)
    Me.TextBox8.Name = "TextBox8"
    Me.TextBox8.ReadOnly = True
    Me.TextBox8.Size = New System.Drawing.Size(112, 20)
    Me.TextBox8.TabIndex = 55
    Me.TextBox8.Tag = "c"
    Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'TextBox9
    '
    Me.TextBox9.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox9.Location = New System.Drawing.Point(343, 70)
    Me.TextBox9.Name = "TextBox9"
    Me.TextBox9.ReadOnly = True
    Me.TextBox9.Size = New System.Drawing.Size(58, 20)
    Me.TextBox9.TabIndex = 56
    Me.TextBox9.Tag = "c"
    Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'frmAplicacionAnticipos
    '
    Me.AcceptButton = Me.cmdAceptar
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.cmdCancelar
    Me.ClientSize = New System.Drawing.Size(677, 354)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.PanelRecibosAnticipos)
    Me.Controls.Add(Me.Panel1)
    Me.Name = "frmAplicacionAnticipos"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Aplicacion de Anticipos"
    Me.Panel1.ResumeLayout(False)
    CType(Me.PanelRecibosAnticipos, System.ComponentModel.ISupportInitialize).EndInit()
    Me.PanelRecibosAnticipos.ResumeLayout(False)
    Me.PanelRecibosAnticipos.PerformLayout()
    CType(Me.TblRecibosAnticiposBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TblFacturasAplicadasBS, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents PanelRecibosAnticipos As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TblRecibosAnticiposBS As System.Windows.Forms.BindingSource
    Friend WithEvents TblFacturasAplicadasBS As System.Windows.Forms.BindingSource
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
