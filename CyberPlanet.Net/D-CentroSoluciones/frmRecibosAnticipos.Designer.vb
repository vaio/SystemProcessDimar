﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecibosAnticipos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecibosAnticipos))
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.PanelRecibosAnticipos = New DevExpress.XtraEditors.PanelControl()
    Me.Label42 = New System.Windows.Forms.Label()
    Me.Label39 = New System.Windows.Forms.Label()
    Me.TextBox6 = New System.Windows.Forms.TextBox()
    Me.Label36 = New System.Windows.Forms.Label()
    Me.Label35 = New System.Windows.Forms.Label()
    Me.Label34 = New System.Windows.Forms.Label()
    Me.Label31 = New System.Windows.Forms.Label()
    Me.ComboBox1 = New System.Windows.Forms.ComboBox()
    Me.TextBox5 = New System.Windows.Forms.TextBox()
    Me.Label30 = New System.Windows.Forms.Label()
    Me.TextBox4 = New System.Windows.Forms.TextBox()
    Me.Label29 = New System.Windows.Forms.Label()
    Me.TextBox3 = New System.Windows.Forms.TextBox()
    Me.Label28 = New System.Windows.Forms.Label()
    Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
    Me.Label27 = New System.Windows.Forms.Label()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.Label24 = New System.Windows.Forms.Label()
    Me.Panel1.SuspendLayout()
    CType(Me.PanelRecibosAnticipos, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.PanelRecibosAnticipos.SuspendLayout()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 151)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(684, 49)
    Me.Panel1.TabIndex = 1
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(594, 6)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(510, 6)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'PanelRecibosAnticipos
    '
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label42)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label39)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox6)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label36)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label35)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label34)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label31)
    Me.PanelRecibosAnticipos.Controls.Add(Me.ComboBox1)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox5)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label30)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox4)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label29)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox3)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label28)
    Me.PanelRecibosAnticipos.Controls.Add(Me.DateTimePicker1)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label27)
    Me.PanelRecibosAnticipos.Controls.Add(Me.TextBox2)
    Me.PanelRecibosAnticipos.Controls.Add(Me.Label24)
    Me.PanelRecibosAnticipos.Dock = System.Windows.Forms.DockStyle.Fill
    Me.PanelRecibosAnticipos.Location = New System.Drawing.Point(0, 0)
    Me.PanelRecibosAnticipos.Name = "PanelRecibosAnticipos"
    Me.PanelRecibosAnticipos.Size = New System.Drawing.Size(684, 151)
    Me.PanelRecibosAnticipos.TabIndex = 0
    '
    'Label42
    '
    Me.Label42.BackColor = System.Drawing.Color.White
    Me.Label42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label42.Location = New System.Drawing.Point(553, 112)
    Me.Label42.Name = "Label42"
    Me.Label42.Size = New System.Drawing.Size(112, 23)
    Me.Label42.TabIndex = 9
    Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label39
    '
    Me.Label39.AutoSize = True
    Me.Label39.Location = New System.Drawing.Point(407, 118)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(143, 13)
    Me.Label39.TabIndex = 40
    Me.Label39.Text = "Monto Anticipos Disponibles:"
    '
    'TextBox6
    '
    Me.TextBox6.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox6.Location = New System.Drawing.Point(127, 52)
    Me.TextBox6.Name = "TextBox6"
    Me.TextBox6.Size = New System.Drawing.Size(86, 20)
    Me.TextBox6.TabIndex = 4
    Me.TextBox6.Tag = "c"
    Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Label36
    '
    Me.Label36.AutoSize = True
    Me.Label36.Location = New System.Drawing.Point(19, 54)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(93, 13)
    Me.Label36.TabIndex = 38
    Me.Label36.Text = "Código de Cliente:"
    '
    'Label35
    '
    Me.Label35.BackColor = System.Drawing.Color.White
    Me.Label35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label35.Location = New System.Drawing.Point(335, 112)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(39, 23)
    Me.Label35.TabIndex = 8
    Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label34
    '
    Me.Label34.AutoSize = True
    Me.Label34.Location = New System.Drawing.Point(227, 118)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(110, 13)
    Me.Label34.TabIndex = 36
    Me.Label34.Text = "Anticipos Disponibles:"
    '
    'Label31
    '
    Me.Label31.AutoSize = True
    Me.Label31.Location = New System.Drawing.Point(315, 22)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(82, 13)
    Me.Label31.TabIndex = 35
    Me.Label31.Text = "Medio de Pago:"
    '
    'ComboBox1
    '
    Me.ComboBox1.FormattingEnabled = True
    Me.ComboBox1.Items.AddRange(New Object() {"CHEQUE", "TRANSFERENCIA"})
    Me.ComboBox1.Location = New System.Drawing.Point(399, 19)
    Me.ComboBox1.Name = "ComboBox1"
    Me.ComboBox1.Size = New System.Drawing.Size(120, 21)
    Me.ComboBox1.TabIndex = 2
    '
    'TextBox5
    '
    Me.TextBox5.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox5.Location = New System.Drawing.Point(587, 18)
    Me.TextBox5.Name = "TextBox5"
    Me.TextBox5.Size = New System.Drawing.Size(78, 20)
    Me.TextBox5.TabIndex = 3
    Me.TextBox5.Tag = "c"
    Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Label30
    '
    Me.Label30.AutoSize = True
    Me.Label30.Location = New System.Drawing.Point(541, 21)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(40, 13)
    Me.Label30.TabIndex = 32
    Me.Label30.Text = "Monto:"
    '
    'TextBox4
    '
    Me.TextBox4.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox4.Location = New System.Drawing.Point(338, 52)
    Me.TextBox4.Name = "TextBox4"
    Me.TextBox4.Size = New System.Drawing.Size(327, 20)
    Me.TextBox4.TabIndex = 5
    Me.TextBox4.Tag = "c"
    '
    'Label29
    '
    Me.Label29.AutoSize = True
    Me.Label29.Location = New System.Drawing.Point(233, 55)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(99, 13)
    Me.Label29.TabIndex = 30
    Me.Label29.Text = "Nombre del Cliente:"
    '
    'TextBox3
    '
    Me.TextBox3.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox3.Location = New System.Drawing.Point(127, 82)
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.Size = New System.Drawing.Size(538, 20)
    Me.TextBox3.TabIndex = 6
    Me.TextBox3.Tag = "c"
    '
    'Label28
    '
    Me.Label28.AutoSize = True
    Me.Label28.Location = New System.Drawing.Point(19, 85)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(70, 13)
    Me.Label28.TabIndex = 28
    Me.Label28.Text = "Observación:"
    '
    'DateTimePicker1
    '
    Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.DateTimePicker1.Location = New System.Drawing.Point(202, 19)
    Me.DateTimePicker1.Name = "DateTimePicker1"
    Me.DateTimePicker1.Size = New System.Drawing.Size(95, 20)
    Me.DateTimePicker1.TabIndex = 1
    '
    'Label27
    '
    Me.Label27.AutoSize = True
    Me.Label27.Location = New System.Drawing.Point(161, 22)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(40, 13)
    Me.Label27.TabIndex = 2
    Me.Label27.Text = "Fecha:"
    '
    'TextBox2
    '
    Me.TextBox2.BackColor = System.Drawing.Color.AliceBlue
    Me.TextBox2.Location = New System.Drawing.Point(77, 19)
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.Size = New System.Drawing.Size(73, 20)
    Me.TextBox2.TabIndex = 0
    Me.TextBox2.Tag = "c"
    Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'Label24
    '
    Me.Label24.AutoSize = True
    Me.Label24.Location = New System.Drawing.Point(19, 22)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(59, 13)
    Me.Label24.TabIndex = 1
    Me.Label24.Text = "N° Recibo:"
    '
    'frmRecibosAnticipos
    '
    Me.AcceptButton = Me.cmdAceptar
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.cmdCancelar
    Me.ClientSize = New System.Drawing.Size(684, 200)
    Me.Controls.Add(Me.PanelRecibosAnticipos)
    Me.Controls.Add(Me.Panel1)
    Me.Name = "frmRecibosAnticipos"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Recibos de Anticipos"
    Me.Panel1.ResumeLayout(False)
    CType(Me.PanelRecibosAnticipos, System.ComponentModel.ISupportInitialize).EndInit()
    Me.PanelRecibosAnticipos.ResumeLayout(False)
    Me.PanelRecibosAnticipos.PerformLayout()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents PanelRecibosAnticipos As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
End Class
