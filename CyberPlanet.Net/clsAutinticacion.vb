Option Explicit On
Option Strict On
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports System.Security.Cryptography.MD5
Imports System.Security.Cryptography

Public Class clsAutinticacion
    Implements IDisposable
    Dim TextoEnBytes As Byte()
    Dim HashEnBytes As Byte()
    Dim HashTexto As String

#Region "Variables de Propiedades"
    Dim m_coneccion As String
    Dim m_UserID As Short
    Dim m_Name As Short
    Dim m_User As Short
    Dim m_Pass As Short
    Dim isWebService As Boolean = False

    Private des As New TripleDESCryptoServiceProvider 'Algorithmo TripleDES
    Private hashmd5 As New MD5CryptoServiceProvider 'objeto md5
    Private myKey As String = "CriptoDimar2017"

#End Region
    Sub New(ByVal strConnFinanzas As String)  'Inicializacion dela Instancia
        Dim sb As New System.Data.SqlClient.SqlConnectionStringBuilder(strConnFinanzas)
        sb.ConnectTimeout = 5
        m_coneccion = sb.ConnectionString
    End Sub

    Sub Dispose() Implements IDisposable.Dispose
        'Cerrar archivos y liberar otros recursos aqui
    End Sub

#Region "METODOS"

    Public Function SQL(ByVal strSql As String) As DataSet
        Try
            Dim cnConec As New SqlConnection(m_coneccion)
            cnConec.Open()
            Dim daAdaptador As New SqlDataAdapter(strSql, cnConec)
            Dim dsSQL As New DataSet
            daAdaptador.Fill(dsSQL, "tblSeguridad")
            daAdaptador.Dispose()
            cnConec.Close()
            SQL = dsSQL
        Catch ex As Exception
            Call MsgBox(ex.Message & vbNewLine & "Consulte al departamento de Informatica", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Return Nothing
        End Try

    End Function

    Public Function Autenticar(ByVal User As String, ByVal Pass As String, ByVal Sucursal As Integer) As DataSet
        Dim strsql As String = ""

        Dim dsAuten As DataSet = SQL("SELECT * FROM vwVerUsuarios WHERE (IdSucursal = " + Sucursal.ToString + ") and (Usuario = N'" + User + "')")

        'VERIFICACION DE NOMBRE DE USUARIO
        If dsAuten.Tables(0).Rows.Count = 0 Then
            MsgBox("Nombre de Usuario Incorrecto." & vbNewLine & "No se encuentra registrado en Sucursal.", MsgBoxStyle.Critical, "Error de Inicio")
            dsAuten.Dispose()
            Return dsAuten
            Exit Function

        End If

        'VERIFICACION DE CONTRASEÑA DE USUARIO

        strsql = "SELECT * FROM vwVerUsuarios WHERE (IdSucursal = " + Sucursal.ToString + ") and (Usuario = N'" + User + "') and (Contrasena like '" + Encriptar(Pass) + "')"

        dsAuten = SQL(strsql)

        If dsAuten.Tables(0).Rows.Count <= 0 Then
            MsgBox("Contraseña de Usuario Incorrecta." & vbNewLine & "Consulte al departamento de Informatica", MsgBoxStyle.Exclamation, "Erro de Inicio")
            dsAuten.Dispose()
            Return dsAuten
            Exit Function
        End If

        If dsAuten.Tables(0).Rows(0).Item("PerfilID").Equals("0000") Then
            MsgBox("Usuario con permiso denegado." & vbNewLine & "Consulte al departamento de Informatica", MsgBoxStyle.Exclamation, "Erro de Inicio")
            dsAuten.Clear()
            Return dsAuten
            dsAuten.Dispose()
            Exit Function

        ElseIf dsAuten.Tables(0).Rows(0).Item("Activo").Equals(False) Then
            MsgBox("Usuario con permiso penegado" & vbNewLine & "Consulte al departamento de Informatica", MsgBoxStyle.Exclamation, "Erro de Inicio")
            dsAuten.Clear()
            Return dsAuten
            dsAuten.Dispose()
            Exit Function

        End If

        Return dsAuten
    End Function

    Public Function GetAccesoModulos(ByVal PerfilID As String) As DataSet
        Dim strsql As String = ""

        strsql = " SELECT DISTINCT Configuracion.Controles.ModuloID, Configuracion.Modulos.Nombre FROM Configuracion.Perfil_Detalle  "
        strsql = strsql + " INNER JOIN Configuracion.Controles ON Configuracion.Perfil_Detalle.ControlID = Configuracion.Controles.ControID INNER JOIN Configuracion.Modulos "
        strsql = strsql + " ON Configuracion.Controles.ModuloID = Configuracion.Modulos.ModuloID WHERE (Configuracion.Perfil_Detalle.PerfilID = " & PerfilID & ") "

        Dim dsModulos As DataSet = SQL(strsql)

        Return dsModulos
    End Function

    Public Function GetAccesoControles(ByVal PerfilID As String, ByVal ModuloID As String) As DataSet
        Dim strsql As String = ""

        strsql = " SELECT DISTINCT Configuracion.Perfil_Detalle.ControlID, Configuracion.Controles.Nombre FROM Configuracion.Perfil_Detalle INNER JOIN Configuracion.Controles "
        strsql = strsql + " ON Configuracion.Perfil_Detalle.ControlID = Configuracion.Controles.ControID WHERE (Configuracion.Controles.ModuloID = " & ModuloID & ") and (Configuracion.Perfil_Detalle.PerfilID = " & PerfilID & ") "

        Dim dsControles As DataSet = SQL(strsql)

        Return dsControles
    End Function

    Public Function editarUsuario(ByVal UsuarioID As String, ByVal Sucursal As Integer, ByVal IdEmpleado As Integer, ByVal Nombres As String, ByVal Apellidos As String,
                                            ByVal Cargo As Integer, ByVal Area As Integer, ByVal Usuario As String, ByVal Contrasena As String,
                                            ByVal PerfilID As String, ByVal Email As String, ByVal FechaIngreso As Date,
                                            ByVal Activo As Boolean, ByVal nTipoEdicion As Integer) As String
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = m_coneccion
            .Open()
        End With

        Try
            Dim cmdUsuario As New SqlCommand("SP_AME_Usuario", cnConec)
            cmdUsuario.CommandType = CommandType.StoredProcedure
            cmdUsuario.Parameters.Add("@UsuarioID", SqlDbType.NVarChar).Value = UsuarioID
            cmdUsuario.Parameters.Add("@Sucursal", SqlDbType.Int).Value = Sucursal
            cmdUsuario.Parameters.Add("@IdEmpleado", SqlDbType.Int).Value = IdEmpleado
            cmdUsuario.Parameters.Add("@Nombres", SqlDbType.NVarChar).Value = Nombres
            cmdUsuario.Parameters.Add("@Apellidos", SqlDbType.NVarChar).Value = Apellidos
            cmdUsuario.Parameters.Add("@Cargo", SqlDbType.Int).Value = Cargo
            cmdUsuario.Parameters.Add("@Area", SqlDbType.Int).Value = Area
            cmdUsuario.Parameters.Add("@Usuario", SqlDbType.NVarChar).Value = Usuario
            cmdUsuario.Parameters.Add("@Contrasena", SqlDbType.NVarChar).Value = Contrasena
            cmdUsuario.Parameters.Add("@PerfilID", SqlDbType.NVarChar).Value = PerfilID
            cmdUsuario.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Email
            cmdUsuario.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = FechaIngreso
            cmdUsuario.Parameters.Add("@Activo", SqlDbType.Bit).Value = Activo
            cmdUsuario.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion

            cmdUsuario.ExecuteReader()

            editarUsuario = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            editarUsuario = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function editarPerfil(ByVal PerfilID As String, ByVal Nombre As String, ByVal Estado As Boolean, ByVal nTipoEdicion As Integer) As String
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = m_coneccion
            .Open()
        End With

        Try
            Dim cmdPerfil As New SqlCommand("SP_AME_Perfil", cnConec)
            cmdPerfil.CommandType = CommandType.StoredProcedure
            cmdPerfil.Parameters.Add("@PerfilID", SqlDbType.NVarChar).Value = PerfilID
            cmdPerfil.Parameters.Add("@Nombre", SqlDbType.NVarChar).Value = Nombre
            cmdPerfil.Parameters.Add("@Estado", SqlDbType.Bit).Value = Estado
            cmdPerfil.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion

            cmdPerfil.ExecuteReader()

            editarPerfil = "OK"

        Catch ex As SqlException
            'MsgBox("Error: " + ex.Message)
            MsgBox("No se puede eliminar, el perfil tiene registros asociados.", MsgBoxStyle.Exclamation, "Información")
            editarPerfil = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function editarPerfilDetalle(ByVal PerfilID As String, ByVal ControlID As String, ByVal PermisoID As Integer, ByVal nTipoEdicion As Integer) As String
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = m_coneccion
            .Open()
        End With

        Try
            Dim cmdPerfilDetalle As New SqlCommand("SP_AME_Perfil_Detalle", cnConec)
            cmdPerfilDetalle.CommandType = CommandType.StoredProcedure
            cmdPerfilDetalle.Parameters.Add("@PerfilID", SqlDbType.NVarChar).Value = PerfilID
            cmdPerfilDetalle.Parameters.Add("@ControlID", SqlDbType.NVarChar).Value = ControlID
            cmdPerfilDetalle.Parameters.Add("@PermisoID", SqlDbType.Int).Value = PermisoID
            cmdPerfilDetalle.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion

            cmdPerfilDetalle.ExecuteReader()

            editarPerfilDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            editarPerfilDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

#End Region

#Region "PROPIEDADES"

    ReadOnly Property UltimoNumMasUno() As String
        Get
            Dim strsql As String = "SELECT isnull(MAX(UsuarioID)+1,1) AS Maximo FROM Configuracion.Usuarios"
            Try

                Dim tblRegMax As DataTable = SQL(strsql).Tables(0)
                Return Microsoft.VisualBasic.Right("0000" & tblRegMax.Rows(0).Item(0).ToString, 4)

            Catch ex As Exception
                Call MsgBox("No existe numeros disponibles" & vbNewLine & "Consulte al Departamento de Informatica", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
                Return Nothing
            End Try
        End Get
    End Property

    ReadOnly Property UltimoNumMasUnoPerfil() As String
        Get
            Dim strsql As String = "SELECT isnull(MAX(PerfilID)+1,1) AS Maximo FROM Configuracion.Perfiles"
            Try

                Dim tblRegMax As DataTable = SQL(strsql).Tables(0)
                Return Microsoft.VisualBasic.Right("0000" & tblRegMax.Rows(0).Item(0).ToString, 4)

            Catch ex As Exception
                Call MsgBox("No existe numeros disponibles" & vbNewLine & "Consulte al Departamento de Informatica", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
                Return Nothing
            End Try
        End Get
    End Property
#End Region

    Public Function Encriptar(ByVal texto As String) As String
        If Trim(texto) = "" Then
            Encriptar = ""
        Else
            des.Key = hashmd5.ComputeHash((New UnicodeEncoding).GetBytes(myKey))
            des.Mode = CipherMode.ECB
            Dim encrypt As ICryptoTransform = des.CreateEncryptor()
            Dim buff() As Byte = UnicodeEncoding.ASCII.GetBytes(texto)
            Encriptar = Convert.ToBase64String(encrypt.TransformFinalBlock(buff, 0, buff.Length))
        End If
        Return Encriptar
    End Function

    Public Function Desencriptar(ByVal texto As String) As String
        If Trim(texto) = "" Then
            Desencriptar = ""
        Else
            des.Key = hashmd5.ComputeHash((New UnicodeEncoding).GetBytes(myKey))
            des.Mode = CipherMode.ECB
            Dim desencrypta As ICryptoTransform = des.CreateDecryptor()
            Dim buff() As Byte = Convert.FromBase64String(texto)
            Desencriptar = UnicodeEncoding.ASCII.GetString(desencrypta.TransformFinalBlock(buff, 0, buff.Length))
        End If
        Return Desencriptar
    End Function

End Class
