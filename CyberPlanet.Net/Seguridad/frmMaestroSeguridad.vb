Public Class frmMaestroSeguridad

    Private Sub frmMaestroSeguridad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cargarPerfiles()
        cargarUsuarios()
        Me.nvgUsuario.Expanded = True
    End Sub

    Public Sub cargarPerfiles()
        Dim strsql As String = ""

        GridView2.Columns.Clear()

        strsql = " SELECT PerfilID, Nombre, Estado FROM Configuracion.Perfiles"

        grdPerfiles.DataSource = SQL(strsql, "tblUsuarios", My.Settings.SolIndustrialCNX).Tables(0)

        GridView2.BestFitColumns()
    End Sub

    Public Sub cargarUsuarios()
        Dim strsql As String = ""

        GridView1.Columns.Clear()

        strsql = " SELECT UsuarioID, Nombres, Apellidos, Usuario, Perfil, NombreCargo As 'Cargo', Nombre_Area As 'Area', Email, Ingreso, Activo FROM vwVerUsuarios where IdSucursal = " & My.Settings.Sucursal

        grdUsuarios.DataSource = SQL(strsql, "tblUsuarios", My.Settings.SolIndustrialCNX).Tables(0)

        GridView1.BestFitColumns()
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        If TabControl1.SelectedIndex = 0 Then
            Me.nvgUsuario.Expanded = True
            Me.nvgPerfil.Expanded = False
        Else
            Me.nvgUsuario.Expanded = False
            Me.nvgPerfil.Expanded = True
        End If
    End Sub

    Private Sub nvbNewUser_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbNewUser.LinkClicked
        nTipoEdic = 1
        frmSeguridadUsuariosAlta.ShowDialog()
    End Sub

    Private Sub nvbEditUser_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbEditUser.LinkClicked
        nTipoEdic = 2
        frmSeguridadUsuariosAlta.ShowDialog()
    End Sub

    Private Sub nvbDeleteUser_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbDeleteUser.LinkClicked
        nTipoEdic = 3
        frmSeguridadUsuariosAlta.ShowDialog()
    End Sub

    Private Sub nvbNewPerfil_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbNewPerfil.LinkClicked
        nTipoEdic = 1
        frmSeguridadPerfiesAlta.ShowDialog()
    End Sub

    Private Sub nvbEditPerfil_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbEditPerfil.LinkClicked
        nTipoEdic = 2
        frmSeguridadPerfiesAlta.ShowDialog()
    End Sub

    Private Sub nvbDeletePerfil_LinkClicked(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbDeletePerfil.LinkClicked
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim Resum, perfil, nombre As String
        Dim Activo As Boolean

        Try
            Dim result As Integer = MessageBox.Show("Desea eliminar el perfil?", "Confirmación de Acción", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                perfil = GridView2.GetRowCellValue(GridView2.GetSelectedRows(0), "PerfilID").ToString()
                nombre = GridView2.GetRowCellValue(GridView2.GetSelectedRows(0), "Nombre").ToString()
                Activo = GridView2.GetRowCellValue(GridView2.GetSelectedRows(0), "Estado").ToString()

                Resum = aSeguridad.editarPerfil(perfil, nombre, Activo, 3)

                If Resum = "OK" Then
                    MsgBox("Perfil Eliminado.", MsgBoxStyle.Information, "Información")
                    cargarPerfiles()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub NavBarControl1_ActiveGroupChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraNavBar.NavBarGroupEventArgs) Handles NavBarControl1.ActiveGroupChanged
        If Me.NavBarControl1.ActiveGroup.Name.Equals("nvgUsuario") Then
            Me.TabControl1.SelectedIndex = 0
        End If
        If Me.NavBarControl1.ActiveGroup.Name.Equals("nvgPerfil") Then
            Me.TabControl1.SelectedIndex = 1
        End If
    End Sub

    Private Sub frmMaestroSeguridad_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Dispose()
    End Sub

    Private Sub NavBarControl1_GroupExpanded(sender As Object, e As DevExpress.XtraNavBar.NavBarGroupEventArgs) Handles NavBarControl1.GroupExpanded

        If TabControl1.SelectedTab Is TabControl1.TabPages("tpUser") Then
            If e.Group Is nvgPerfil Then
                e.Group.Expanded = False
            End If
        Else
            If e.Group Is nvgUsuario Then
                e.Group.Expanded = False
            End If
        End If

    End Sub

    Private Sub frmMaestroSeguridad_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize

    End Sub

    Private Sub frmMaestroSeguridad_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        frmCatalogo.Close()
        NumCatalogo = 35
    End Sub
End Class