Imports System.Diagnostics.Process
Imports System.IO
Imports DevExpress.XtraBars

Public Class frmSingIn
    Private Const WM_SYSCOMMAND As Integer = &H112&
    Private Const MOUSE_MOVE As Integer = &HF012&
    '
    ' Declaraciones del API al estilo .NET
    <System.Runtime.InteropServices.DllImport("user32.DLL", EntryPoint:="ReleaseCapture")> _
    Private Shared Sub ReleaseCapture()
    End Sub
    '
    <System.Runtime.InteropServices.DllImport("user32.DLL", EntryPoint:="SendMessage")> _
    Private Shared Sub SendMessage(ByVal hWnd As System.IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As Integer)
    End Sub

    Private Sub moverForm()
        ReleaseCapture()
        SendMessage(Me.Handle, WM_SYSCOMMAND, MOUSE_MOVE, 0)
    End Sub

    Private Sub frmSingIn_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove, GroupControl1.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            moverForm()
        End If
    End Sub

    Private Sub frmSingIn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-ni")
        System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("es-ni")
        Timer1.Start()

        cargarSucursal()
        ocultarMenu()

    End Sub

    Public Sub cargarSucursal()
        cmbSucursal.DataSource = SQL("SELECT Key_Sucursal, Nombre FROM Configuracion.Sucursales where Activo = 1", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
        cmbSucursal.DisplayMember = "Nombre"
        cmbSucursal.ValueMember = "Key_Sucursal"
    End Sub

    Public Sub ocultarMenu()
        Try

            Dim ctrl() As Control = frmPrincipal.Controls.Find("RibbonControl1", True)
            Dim ribbon As RibbonControl = ctrl(0)

            For Each pages As RibbonPage In ribbon.Pages
                For Each group As RibbonPageGroup In pages.Groups
                    For Each item As BarButtonItemLink In group.ItemLinks
                        'item.Item.Visibility = BarItemVisibility.Never
                        item.Item.Enabled = False
                    Next
                    'group.Visible = False
                Next
                pages.Visible = False
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Opacity = Me.Opacity + 0.3
        If Me.Opacity = 1.0 Then
            Timer1.Stop()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim dsAutent, dsModulos, dsControles As DataSet
        Dim CodModulo, ModNombre, CodControl, ContNombre As String

        Try

            dsAutent = aSeguridad.Autenticar(Me.UsernameTextBox.Text.Trim, Me.PasswordTextBox.Text.Trim, cmbSucursal.SelectedValue)

        Catch ex As Exception
            dsAutent = Nothing
        End Try

        If dsAutent.Tables(0).Rows.Count <> 0 Then
            My.Settings.Sucursal = cmbSucursal.SelectedValue

            UserID = dsAutent.Tables(0).Rows(0).Item("UsuarioID")
            UserFullName = (dsAutent.Tables(0).Rows(0).Item("Nombres") + " " + dsAutent.Tables(0).Rows(0).Item("Apellidos"))
            UserName = dsAutent.Tables(0).Rows(0).Item("Usuario")
            UserPass = dsAutent.Tables(0).Rows(0).Item("Contrasena")
            UserPerfil = dsAutent.Tables(0).Rows(0).Item("PerfilID")
            SucursalName = dsAutent.Tables(0).Rows(0).Item("Sucursal")

            dsModulos = aSeguridad.GetAccesoModulos(UserPerfil)

            Dim ctrl() As Control = frmPrincipal.Controls.Find("RibbonControl1", True)
            Dim ribbon As RibbonControl = ctrl(0)

            If dsModulos.Tables(0).Rows.Count <> 0 Then
                CodModulo = ""

                For i As Integer = 0 To dsModulos.Tables(0).Rows.Count - 1
                    Try

                        CodModulo = dsModulos.Tables(0).Rows(i).Item("ModuloID")
                        ModNombre = dsModulos.Tables(0).Rows(i).Item("Nombre")

                        Dim pages As RibbonPage = ribbon.Pages(0).Collection.GetPageByName(ModNombre)
                        pages.Visible = True

                    Catch ex As Exception
                    End Try

                    dsControles = aSeguridad.GetAccesoControles(UserPerfil, CodModulo)

                    For j As Integer = 0 To dsControles.Tables(0).Rows.Count - 1
                        Try

                            CodControl = dsControles.Tables(0).Rows(j).Item("ControlID")
                            ContNombre = dsControles.Tables(0).Rows(j).Item("Nombre")

                            ribbon.Items.Item(ContNombre).Enabled = True

                        Catch ex As Exception
                        End Try

                        'ribbon.Items.Item(ContNombre).Visibility = BarItemVisibility.Always

                    Next

                    'For Each group As RibbonPageGroup In pages.Groups
                    '    For Each item As BarButtonItemLink In group.ItemLinks
                    '        If item.Item.Visibility = BarItemVisibility.Always Then
                    '            group.Visible = True
                    '            Exit For
                    '        End If
                    '    Next
                    'Next

                Next

            End If

            dsAutent.Dispose()
            frmPrincipal.Show()
            Me.Timer1.Stop()
            Me.Timer1.Dispose()
            Me.Timer1 = Nothing
            Me.Hide()

            aSeguridad.Dispose()
            aSeguridad = Nothing
        Else
            Me.UsernameTextBox.Select()
        End If
    End Sub

End Class
