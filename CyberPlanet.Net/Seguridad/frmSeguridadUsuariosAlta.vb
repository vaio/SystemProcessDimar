Imports System.Xml
Public Class frmSeguridadUsuariosAlta

    Private Sub cargarPerfiles()
        cmbPerfil.DataSource = SQL("SELECT PerfilID, Nombre, Estado FROM Configuracion.Perfiles", "tblPerfiles", My.Settings.SolIndustrialCNX).Tables(0)
        cmbPerfil.DisplayMember = "Nombre"
        cmbPerfil.ValueMember = "PerfilID"
    End Sub

    Private Sub cargarCargo()
        cmbCargo.DataSource = SQL("SELECT Id_Cargo, NombreCargo FROM Tbl_Cargos", "tblCargos", My.Settings.SolIndustrialCNX).Tables(0)
        cmbCargo.DisplayMember = "NombreCargo"
        cmbCargo.ValueMember = "Id_Cargo"
    End Sub

    Private Sub cargarArea()
        cmbArea.DataSource = SQL("SELECT Id_Area, Nombre_Area FROM Tbl_AreasEmpresa", "tblAreas", My.Settings.SolIndustrialCNX).Tables(0)
        cmbArea.DisplayMember = "Nombre_Area"
        cmbArea.ValueMember = "Id_Area"
    End Sub

    Private Sub ckbAutomatico_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbAutomatico.CheckedChanged

        If nTipoEdic = 1 = True Then
            If ckbAutomatico.Checked = False Then
                txtID.Enabled = True
                txtID.Focus()
                txtID.SelectAll()
            Else
                Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
                txtID.Enabled = False
                txtID.Text = aSeguridad.UltimoNumMasUno
                txtName.Focus()
                aSeguridad.Dispose()
                aSeguridad = Nothing
            End If
        End If
    End Sub

    Private Sub frmSeguridadUsuariosAlta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        cargarPerfiles()
        cargarCargo()
        cargarArea()

        If nTipoEdic = 1 Then
            txtID.Text = aSeguridad.UltimoNumMasUno
            Me.cmbArea.SelectedIndex = 0
            Me.cmbCargo.SelectedIndex = 0
            Me.cmbPerfil.SelectedIndex = 0
            Me.cbxState.SelectedIndex = 0
            aSeguridad.Dispose()
            aSeguridad = Nothing
        Else
            Me.txtID.Text = frmMaestroSeguridad.GridView1.GetRowCellValue(frmMaestroSeguridad.GridView1.GetSelectedRows(0), "UsuarioID").ToString()

            Try
                Dim strsql As String = ""
                strsql = "Select * FROM vwVerUsuarios where IdSucursal = " & My.Settings.Sucursal & " and UsuarioID = " & Me.txtID.Text

                Dim tblDatos As DataTable = SQL(strsql, "tblDatosOrden", My.Settings.SolIndustrialCNX).Tables(0)

                If tblDatos.Rows.Count <> 0 Then
                    Me.txtIdEmpleado.Text = IIf(IsDBNull(tblDatos.Rows(0).Item("IdEmpleado")), "", tblDatos.Rows(0).Item("IdEmpleado"))
                    Me.txtName.Text = tblDatos.Rows(0).Item("Nombres")
                    Me.txtLastName.Text = tblDatos.Rows(0).Item("Apellidos")
                    Me.cmbCargo.SelectedValue = tblDatos.Rows(0).Item("Id_Cargo")
                    Me.cmbArea.SelectedValue = tblDatos.Rows(0).Item("Id_Area")
                    Me.txtUser.Text = tblDatos.Rows(0).Item("Usuario")
                    Me.txtPass.Text = aSeguridad.Desencriptar(tblDatos.Rows(0).Item("Contrasena"))
                    Me.cmbPerfil.SelectedValue = tblDatos.Rows(0).Item("PerfilID")

                    If tblDatos.Rows(0).Item("Activo") = True Then
                        Me.cbxState.SelectedIndex = 1
                    Else
                        Me.cbxState.SelectedIndex = 0
                    End If

                    If Not IsDBNull(tblDatos.Rows(0).Item("IdEmpleado")) Then

                        strsql = "SELECT Nombres,  Apellidos, Id_Cargo 'Cargo', Id_Area 'Area' FROM vwVerEmpleados Where Activo = 1 and Id_Empleado = " & tblDatos.Rows(0).Item("IdEmpleado")

                        Dim Datos As DataTable = aSeguridad.SQL(strsql).Tables(0)

                        For i = 0 To Datos.Rows.Count - 1

                            txtName.Text = Datos.Rows(i).Item("Nombres")
                            txtLastName.Text = Datos.Rows(i).Item("Apellidos")
                            cmbCargo.SelectedValue = Datos.Rows(i).Item("Cargo")
                            cmbArea.SelectedValue = Datos.Rows(i).Item("Area")

                        Next

                    End If
                End If

            Catch ex As Exception

            End Try

            Me.ckbAutomatico.Enabled = False
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        LevelAccess = 0
        LimpiarTodo()
    End Sub

    Private Sub LimpiarTodo()
        nTipoEdic = 1
        Me.txtID.Text = ""
        Me.txtID.Enabled = False
        Me.txtName.Text = ""
        Me.txtLastName.Text = ""
        Me.cmbCargo.SelectedIndex = 0
        Me.cmbArea.SelectedIndex = 0
        Me.cmbPerfil.SelectedIndex = 0
        Me.txtUser.Text = ""
        Me.txtPass.Text = ""
        Me.cbxState.SelectedIndex = 0
        Me.ckbAutomatico.Enabled = True
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim Mensaje As String = ""
        Dim Resum As String = ""

        If txtID.Text = "" Then
            Call MsgBox("El ID est� vacio, ingrese el ID", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information)
            txtID.BackColor = Color.PowderBlue
            txtID.Focus()
            Exit Sub
        End If

        Try
            Dim intPrueba As Integer = CInt(txtID.Text)
        Catch ex As Exception
            Call MsgBox("Tipo de dato incorrecto", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly)
            txtID.BackColor = Color.PowderBlue
            txtID.Focus()
            Exit Sub
        End Try

        If txtName.Text = "" Then
            Call MsgBox("El Nombre est� vacio, ingrese el nombre.", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information)
            txtName.BackColor = Color.PowderBlue
            txtName.Focus()
            Exit Sub
        End If

        If txtLastName.Text = "" Then
            Call MsgBox("El Apellido est� vacio, ingrese el apellido.", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information)
            txtLastName.BackColor = Color.PowderBlue
            txtLastName.Focus()
            Exit Sub
        End If

        Try

            Select Case nTipoEdic
                Case 1
                    Mensaje = "Desea registrar la informaci�n?"
                Case 2
                    Mensaje = "Desea actualizar la informaci�n?"
                Case Else
                    Mensaje = "Desea eliminar al usuario?"
            End Select

            Dim result As Integer = MessageBox.Show(Mensaje, "Confirmaci�n de Acci�n", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then

                Resum = aSeguridad.editarUsuario(txtID.Text.Trim, My.Settings.Sucursal, IIf(txtIdEmpleado.Text = "", DBNull.Value, txtIdEmpleado.Text), txtName.Text.Trim, txtLastName.Text.Trim, _
                                                 Me.cmbCargo.SelectedValue, Me.cmbArea.SelectedValue, Me.txtUser.Text.Trim, _
                                                aSeguridad.Encriptar(Me.txtPass.Text.Trim), Me.cmbPerfil.SelectedValue.Trim, "", Date.Now, Me.cbxState.SelectedIndex, nTipoEdic)

                If Resum = "OK" Then
                    frmMaestroSeguridad.cargarUsuarios()
                End If

            End If

            aSeguridad.Dispose()
            aSeguridad = Nothing
            LimpiarTodo()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnEmpleado_Click(sender As Object, e As EventArgs) Handles btnEmpleado.Click
        frmBusquedaDoc.Text = "Empleados"
        frmBusquedaDoc.ShowDialog()
    End Sub

    Public Sub cargarEmpleado(ByVal Codigo As Integer)
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim strsql As String = ""

        txtIdEmpleado.Text = Codigo

        strsql = "SELECT Nombres,  Apellidos, Id_Cargo 'Cargo', Id_Area 'Area' FROM vwVerEmpleados Where Activo = 1 and Id_Empleado = " & Codigo

        Dim Datos As DataTable = aSeguridad.SQL(strsql).Tables(0)

        For i = 0 To Datos.Rows.Count - 1

            txtName.Text = Datos.Rows(i).Item("Nombres")
            txtLastName.Text = Datos.Rows(i).Item("Apellidos")
            cmbCargo.SelectedValue = Datos.Rows(i).Item("Cargo")
            cmbArea.SelectedValue = Datos.Rows(i).Item("Area")

        Next

    End Sub
End Class