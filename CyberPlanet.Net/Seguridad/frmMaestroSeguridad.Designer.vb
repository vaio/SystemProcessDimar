<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaestroSeguridad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaestroSeguridad))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.nvgUsuario = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nvbNewUser = New DevExpress.XtraNavBar.NavBarItem()
        Me.nvbEditUser = New DevExpress.XtraNavBar.NavBarItem()
        Me.nvbDeleteUser = New DevExpress.XtraNavBar.NavBarItem()
        Me.nvgPerfil = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nvbNewPerfil = New DevExpress.XtraNavBar.NavBarItem()
        Me.nvbEditPerfil = New DevExpress.XtraNavBar.NavBarItem()
        Me.nvbDeletePerfil = New DevExpress.XtraNavBar.NavBarItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpUser = New System.Windows.Forms.TabPage()
        Me.grdUsuarios = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.tpPerfil = New System.Windows.Forms.TabPage()
        Me.grdPerfiles = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.tpUser.SuspendLayout()
        CType(Me.grdUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPerfil.SuspendLayout()
        CType(Me.grdPerfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.NavBarControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.TabControl1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(587, 264)
        Me.SplitContainerControl1.SplitterPosition = 132
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.nvgUsuario
        Me.NavBarControl1.ContentButtonHint = Nothing
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.nvgUsuario, Me.nvgPerfil})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nvbNewUser, Me.nvbEditUser, Me.nvbDeleteUser, Me.nvbNewPerfil, Me.nvbEditPerfil, Me.nvbDeletePerfil})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 0)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.OptionsNavPane.ExpandedWidth = 128
        Me.NavBarControl1.Size = New System.Drawing.Size(128, 256)
        Me.NavBarControl1.StoreDefaultPaintStyleName = True
        Me.NavBarControl1.TabIndex = 0
        Me.NavBarControl1.Text = "NavBarControl1"
        '
        'nvgUsuario
        '
        Me.nvgUsuario.Caption = "Usuarios"
        Me.nvgUsuario.Expanded = True
        Me.nvgUsuario.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbNewUser), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbEditUser), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbDeleteUser)})
        Me.nvgUsuario.Name = "nvgUsuario"
        '
        'nvbNewUser
        '
        Me.nvbNewUser.Caption = "Nuevo Usuario"
        Me.nvbNewUser.LargeImage = CType(resources.GetObject("nvbNewUser.LargeImage"), System.Drawing.Image)
        Me.nvbNewUser.Name = "nvbNewUser"
        '
        'nvbEditUser
        '
        Me.nvbEditUser.Caption = "Editar Usuario"
        Me.nvbEditUser.LargeImage = CType(resources.GetObject("nvbEditUser.LargeImage"), System.Drawing.Image)
        Me.nvbEditUser.Name = "nvbEditUser"
        '
        'nvbDeleteUser
        '
        Me.nvbDeleteUser.Caption = "Borrar Usuario"
        Me.nvbDeleteUser.Name = "nvbDeleteUser"
        Me.nvbDeleteUser.Visible = False
        '
        'nvgPerfil
        '
        Me.nvgPerfil.Caption = "Perfiles"
        Me.nvgPerfil.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbNewPerfil), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbEditPerfil), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbDeletePerfil)})
        Me.nvgPerfil.Name = "nvgPerfil"
        '
        'nvbNewPerfil
        '
        Me.nvbNewPerfil.Caption = "Nuevo Perfil"
        Me.nvbNewPerfil.LargeImage = CType(resources.GetObject("nvbNewPerfil.LargeImage"), System.Drawing.Image)
        Me.nvbNewPerfil.Name = "nvbNewPerfil"
        '
        'nvbEditPerfil
        '
        Me.nvbEditPerfil.Caption = "Editar Perfil"
        Me.nvbEditPerfil.LargeImage = CType(resources.GetObject("nvbEditPerfil.LargeImage"), System.Drawing.Image)
        Me.nvbEditPerfil.Name = "nvbEditPerfil"
        '
        'nvbDeletePerfil
        '
        Me.nvbDeletePerfil.Caption = "Borrar Perfil"
        Me.nvbDeletePerfil.LargeImage = CType(resources.GetObject("nvbDeletePerfil.LargeImage"), System.Drawing.Image)
        Me.nvbDeletePerfil.Name = "nvbDeletePerfil"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tpUser)
        Me.TabControl1.Controls.Add(Me.tpPerfil)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(443, 256)
        Me.TabControl1.TabIndex = 0
        '
        'tpUser
        '
        Me.tpUser.Controls.Add(Me.grdUsuarios)
        Me.tpUser.Location = New System.Drawing.Point(4, 22)
        Me.tpUser.Name = "tpUser"
        Me.tpUser.Padding = New System.Windows.Forms.Padding(3)
        Me.tpUser.Size = New System.Drawing.Size(435, 230)
        Me.tpUser.TabIndex = 0
        Me.tpUser.Text = "Usuarios"
        Me.tpUser.UseVisualStyleBackColor = True
        '
        'grdUsuarios
        '
        Me.grdUsuarios.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdUsuarios.Location = New System.Drawing.Point(3, 3)
        Me.grdUsuarios.MainView = Me.GridView1
        Me.grdUsuarios.Name = "grdUsuarios"
        Me.grdUsuarios.Size = New System.Drawing.Size(429, 224)
        Me.grdUsuarios.TabIndex = 0
        Me.grdUsuarios.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.Gold
        Me.GridView1.Appearance.FocusedCell.BackColor2 = System.Drawing.Color.Orange
        Me.GridView1.Appearance.FocusedCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gold
        Me.GridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Orange
        Me.GridView1.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gold
        Me.GridView1.Appearance.HideSelectionRow.BackColor2 = System.Drawing.Color.Orange
        Me.GridView1.Appearance.HideSelectionRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.GridControl = Me.grdUsuarios
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowDetailButtons = False
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'tpPerfil
        '
        Me.tpPerfil.Controls.Add(Me.grdPerfiles)
        Me.tpPerfil.Location = New System.Drawing.Point(4, 22)
        Me.tpPerfil.Name = "tpPerfil"
        Me.tpPerfil.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPerfil.Size = New System.Drawing.Size(435, 230)
        Me.tpPerfil.TabIndex = 1
        Me.tpPerfil.Text = "Perfiles"
        Me.tpPerfil.UseVisualStyleBackColor = True
        '
        'grdPerfiles
        '
        Me.grdPerfiles.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdPerfiles.Location = New System.Drawing.Point(3, 3)
        Me.grdPerfiles.MainView = Me.GridView2
        Me.grdPerfiles.Name = "grdPerfiles"
        Me.grdPerfiles.Size = New System.Drawing.Size(429, 224)
        Me.grdPerfiles.TabIndex = 0
        Me.grdPerfiles.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView3})
        '
        'GridView2
        '
        Me.GridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.Gold
        Me.GridView2.Appearance.FocusedCell.BackColor2 = System.Drawing.Color.Orange
        Me.GridView2.Appearance.FocusedCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.GridView2.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gold
        Me.GridView2.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Orange
        Me.GridView2.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gold
        Me.GridView2.Appearance.HideSelectionRow.BackColor2 = System.Drawing.Color.Orange
        Me.GridView2.Appearance.HideSelectionRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView2.GridControl = Me.grdPerfiles
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsView.ShowDetailButtons = False
        Me.GridView2.OptionsView.ShowFooter = True
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.grdPerfiles
        Me.GridView3.Name = "GridView3"
        '
        'frmMaestroSeguridad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(587, 264)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "frmMaestroSeguridad"
        Me.Text = "..:: Maestro de Seguridad ::.."
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.tpUser.ResumeLayout(False)
        CType(Me.grdUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPerfil.ResumeLayout(False)
        CType(Me.grdPerfiles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents nvgUsuario As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpUser As System.Windows.Forms.TabPage
    Friend WithEvents tpPerfil As System.Windows.Forms.TabPage
    Friend WithEvents nvbDeleteUser As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvbEditUser As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvbNewUser As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvgPerfil As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nvbNewPerfil As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvbEditPerfil As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvbDeletePerfil As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents grdUsuarios As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grdPerfiles As DevExpress.XtraGrid.GridControl
  Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
  Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
End Class
