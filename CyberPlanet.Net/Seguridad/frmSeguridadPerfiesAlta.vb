Public Class frmSeguridadPerfiesAlta
    Dim ItemSelected, ItemSelected2 As Integer

    Private Sub frmSeguridadPerfiesAlta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim strsql As String = ""
        Dim Activo As Boolean = False

        If nTipoEdic = 1 Then
            txtID.Text = aSeguridad.UltimoNumMasUnoPerfil
            Me.cbxState.SelectedIndex = 1
            cargarNavControles()
            cargarNavUsuarios()
        Else
            Me.txtID.Text = frmMaestroSeguridad.GridView2.GetRowCellValue(frmMaestroSeguridad.GridView2.GetSelectedRows(0), "PerfilID").ToString()
            Me.txtName.Text = frmMaestroSeguridad.GridView2.GetRowCellValue(frmMaestroSeguridad.GridView2.GetSelectedRows(0), "Nombre").ToString()
            Activo = frmMaestroSeguridad.GridView2.GetRowCellValue(frmMaestroSeguridad.GridView2.GetSelectedRows(0), "Estado").ToString()

            If Activo = True Then
                Me.cbxState.SelectedIndex = 1
            Else
                Me.cbxState.SelectedIndex = 0
            End If

            strsql = " SELECT (Modls.Modulo + '/' + Contrl.Control) Descripcion, PerfDet.PerfilID, Contrl.ControID, Contrl.TipoControl, PerfDet.PermisoID "
            strsql = strsql + " FROM Configuracion.Perfil_Detalle PerfDet INNER JOIN Configuracion.Controles Contrl ON PerfDet.ControlID = Contrl.ControID INNER JOIN"
            strsql = strsql + " Configuracion.Modulos Modls ON Contrl.ModuloID = Modls.ModuloID WHERE PerfDet.PerfilID = N'" & Me.txtID.Text.Trim & "' ORDER BY Modls.ModuloID"

            Dim dsSeguridad As DataSet = aSeguridad.SQL(strsql)

            For i = 0 To dsSeguridad.Tables(0).Rows.Count - 1
                Dim LItem As New ListViewItem
                LItem.Tag = dsSeguridad.Tables(0).Rows(i).Item("PermisoID")
                LItem.Text = dsSeguridad.Tables(0).Rows(i).Item("Descripcion")
                LItem.ImageIndex = 0
                LItem.Name = dsSeguridad.Tables(0).Rows(i).Item("ControID")
                ListView1.Items.Add(LItem)
            Next

            Dim dsUsuarios As DataSet = aSeguridad.SQL("SELECT UsuarioID, (Nombres + ' ' + Apellidos) Nombre, Activo FROM Configuracion.Usuarios WHERE  Sucursal = 1 and PerfilID = '" & Me.txtID.Text.Trim & "'")

            For i = 0 To dsUsuarios.Tables(0).Rows.Count - 1
                Dim LItem As New ListViewItem
                LItem.Tag = dsUsuarios.Tables(0).Rows(i).Item("UsuarioID")
                LItem.Text = dsUsuarios.Tables(0).Rows(i).Item("Nombre")

                If dsUsuarios.Tables(0).Rows(i).Item("Activo") = True Then
                    LItem.ImageIndex = 1
                Else
                    LItem.ImageIndex = 2 
                End If

                LItem.Name = dsUsuarios.Tables(0).Rows(i).Item("UsuarioID")
                ListView2.Items.Add(LItem)

            Next
            cargarNavControles()
            cargarNavUsuarios()
        End If

        aSeguridad.Dispose()
        aSeguridad = Nothing
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        LevelAccess = 0
        LimpiarTodo()
    End Sub

    Private Sub ckbAutomatico_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbAutomatico.CheckedChanged
        If nTipoEdic = 1 Then
            If ckbAutomatico.Checked = False Then
                txtID.Enabled = True
                txtID.Focus()
                txtID.SelectAll()
            Else 
                Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
                txtID.Enabled = False
                txtID.Text = aSeguridad.UltimoNumMasUno
                txtName.Focus()
                aSeguridad.Dispose()
                aSeguridad = Nothing
            End If
        End If
    End Sub

    Private Sub cargarNavControles()
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim dsModulo As DataSet = aSeguridad.SQL("SELECT * FROM Configuracion.Modulos")

        For k = 0 To dsModulo.Tables(0).Rows.Count - 1
            Dim navc As New DevExpress.XtraNavBar.NavBarGroup
            navc.Tag = dsModulo.Tables(0).Rows(k).Item("ModuloID").ToString
            navc.Name = dsModulo.Tables(0).Rows(k).Item("ModuloID").ToString
            navc.Caption = dsModulo.Tables(0).Rows(k).Item("Modulo").ToString
            navc.LargeImageIndex = 0 'CType(dsModulo.Tables(0).Rows(k).Item("ImageIndex"), Integer)
            Me.nbcControl.Groups.Add(navc)

            Dim dsControl As DataSet = aSeguridad.SQL("SELECT ControID, Control FROM Configuracion.Controles WHERE ModuloID = '" & navc.Name & "'")

            For j = 0 To dsControl.Tables(0).Rows.Count - 1
                Dim nave As New DevExpress.XtraNavBar.NavBarItem
                nave.Tag = dsControl.Tables(0).Rows(j).Item("ControID").ToString
                nave.Name = dsControl.Tables(0).Rows(j).Item("ControID").ToString
                nave.Caption = dsControl.Tables(0).Rows(j).Item("Control").ToString
                nave.SmallImageIndex = 0

                nbcControl.Groups.Item(k).ItemLinks.Add(nave)
            Next

            dsControl.Dispose()
            dsControl = Nothing
        Next

        aSeguridad.Dispose()
        aSeguridad = Nothing
        dsModulo.Dispose()
        dsModulo = Nothing
    End Sub

    Private Sub cargarNavUsuarios()
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim dsArea As DataSet = aSeguridad.SQL("SELECT* FROM  Tbl_AreasEmpresa")

        For i = 0 To dsArea.Tables(0).Rows.Count - 1
            Dim nave As New DevExpress.XtraNavBar.NavBarGroup
            nave.Tag = dsArea.Tables(0).Rows(i).Item("Id_Area").ToString
            nave.Name = dsArea.Tables(0).Rows(i).Item("Id_Area").ToString
            nave.Caption = dsArea.Tables(0).Rows(i).Item("Nombre_Area").ToString
            nave.SmallImageIndex = 0 ' CType(dsArea.Tables(0).Rows(i).Item("ItemImagen"), Integer)
            Me.nbcUsuarios.Groups.Add(nave)

            Dim dsUser As DataSet = aSeguridad.SQL("SELECT UsuarioID, (Nombres + '' + Apellidos) Nombre, Activo FROM Configuracion.Usuarios WHERE  Sucursal = " & My.Settings.Sucursal & " and Area = '" & nave.Name & "'")

            For j = 0 To dsUser.Tables(0).Rows.Count - 1
                Dim navi As New DevExpress.XtraNavBar.NavBarItem
                navi.Tag = dsUser.Tables(0).Rows(j).Item("UsuarioID").ToString
                navi.Name = dsUser.Tables(0).Rows(j).Item("UsuarioID").ToString
                navi.Caption = dsUser.Tables(0).Rows(j).Item("Nombre").ToString

                If dsUser.Tables(0).Rows(j).Item("Activo").Equals(True) Then
                    navi.SmallImageIndex = 11
                Else
                    navi.SmallImageIndex = 12
                End If

                nbcUsuarios.Groups.Item(i).ItemLinks.Add(navi)
            Next

            dsUser.Dispose()
            dsUser = Nothing
        Next

        aSeguridad.Dispose()
        aSeguridad = Nothing
        dsArea.Dispose()
        dsArea = Nothing
    End Sub

    Private Sub LimpiarTodo()
        Me.ListView1.Clear()
        Me.ListView2.Clear()
        Me.ckbAutomatico.Checked = True
        Me.txtID.Text = ""
        Me.txtName.Text = ""
        Me.nbcUsuarios.Groups.Clear()
        Me.nbcControl.Groups.Clear()
        Me.RadioButton3.Checked = False
        Me.RadioButton1.Checked = False
        Me.RadioButton2.Checked = False
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub ListView1_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ListView1.DragDrop
        Dim textCaption As String = ""
        Dim i As Integer
        Dim link As DevExpress.XtraNavBar.NavBarItemLink = GetItemLink(e.Data)
        If Not link Is Nothing AndAlso link.Item.Enabled Then

            Dim strsql As String = "SELECT (Modls.Modulo + '/' + Contrl.Control) Descripcion FROM Configuracion.Controles Contrl INNER JOIN Configuracion.Modulos Modls ON Contrl.ModuloID = Modls.ModuloID WHERE Contrl.ControID = '" & link.Item.Tag & "'"
            Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count > 0 Then
                textCaption = tblDatos.Rows(0).Item("Descripcion")
            End If

            For i = 0 To ListView1.Items.Count - 1
                If ListView1.Items.Item(i).Text = textCaption Then
                    MsgBox("El Componente ya se encuentra esta listado", MsgBoxStyle.Exclamation, "Componente Listado")
                    Exit Sub
                End If
            Next

            If link.NavBar.Name = "nbcUsuarios" Then
                MsgBox("El campo arrastrado es para la lista de Usuarios", MsgBoxStyle.Exclamation, "Error")
                Exit Sub
            End If

            Dim LItem As New ListViewItem
            LItem.Tag = "1"
            LItem.ImageIndex = link.Item.SmallImageIndex
            LItem.Name = link.Item.Tag
            LItem.Text = textCaption
            ListView1.Items.Add(LItem)
        End If
    End Sub

    Private Sub ListView1_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ListView1.DragEnter
        Dim link As DevExpress.XtraNavBar.NavBarItemLink = GetItemLink(e.Data)
        If Not link Is Nothing AndAlso link.Item.Enabled Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        If ListView1.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Me.ItemSelected = ListView1.SelectedItems.Item(0).Index

        If ListView1.SelectedItems.Item(0).Tag = "0" Then
            Me.RadioButton1.Checked = True
            Me.RadioButton2.Checked = False
            Me.RadioButton3.Checked = False
            Me.RadioButton4.Checked = False
            Exit Sub
        End If
        If ListView1.SelectedItems.Item(0).Tag = "1" Then
            Me.RadioButton1.Checked = False
            Me.RadioButton2.Checked = True
            Me.RadioButton3.Checked = False
            Me.RadioButton4.Checked = False
            Exit Sub
        End If
        If ListView1.SelectedItems.Item(0).Tag = "2" Then
            Me.RadioButton1.Checked = False
            Me.RadioButton2.Checked = False
            Me.RadioButton3.Checked = True
            Me.RadioButton4.Checked = False
            Exit Sub
        End If
        If ListView1.SelectedItems.Item(0).Tag = "3" Then
            Me.RadioButton1.Checked = False
            Me.RadioButton2.Checked = False
            Me.RadioButton3.Checked = False
            Me.RadioButton4.Checked = True
            Exit Sub
        End If
    End Sub

    Private Sub ListView2_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ListView2.DragDrop
        Dim link As DevExpress.XtraNavBar.NavBarItemLink = GetItemLink(e.Data)
        If Not link Is Nothing AndAlso link.Item.Enabled Then
            Dim i As Integer
            For i = 0 To ListView2.Items.Count - 1
                If ListView2.Items.Item(i).Text = link.Caption Then
                    MsgBox("El Componente ya se encuentra esta listado", MsgBoxStyle.Exclamation, "Componente Listado")
                    Exit Sub
                End If
            Next
            If link.NavBar.Name = "nbcControl" Then
                MsgBox("El campo arrastrado es para la lista de Controles", MsgBoxStyle.Exclamation, "Error")
                Exit Sub
            End If
            Dim LItem As New ListViewItem
            LItem.Tag = "0"
            LItem.Text = link.Caption
            If link.Item.SmallImageIndex = 11 Then
                LItem.ImageIndex = 1
            Else
                LItem.ImageIndex = 2
            End If
            LItem.Name = link.Item.Tag
            ListView2.Items.Add(LItem)
        End If
    End Sub

    Private Sub ListView2_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ListView2.DragEnter
        Dim link As DevExpress.XtraNavBar.NavBarItemLink = GetItemLink(e.Data)
        If Not link Is Nothing AndAlso link.Item.Enabled Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub ListView2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView2.SelectedIndexChanged
        If ListView2.SelectedItems.Count = 0 Then
            Exit Sub
        End If
        Me.ItemSelected2 = ListView2.SelectedItems.Item(0).Index
    End Sub

    Private Function GetItemLink(ByVal data As IDataObject) As DevExpress.XtraNavBar.NavBarItemLink
        If Not TypeOf data.GetData(GetType(DevExpress.XtraNavBar.NavBarItemLink)) Is DevExpress.XtraNavBar.NavBarItemLink Then Return Nothing
        Return CType(data.GetData(GetType(DevExpress.XtraNavBar.NavBarItemLink)), DevExpress.XtraNavBar.NavBarItemLink)
    End Function

    Private Function GetListViewItems(ByVal data As IDataObject) As ListView.SelectedListViewItemCollection
        If Not TypeOf data.GetData(GetType(ListView.SelectedListViewItemCollection)) Is ListView.SelectedListViewItemCollection Then Return Nothing
        Return CType(data.GetData(GetType(ListView.SelectedListViewItemCollection)), ListView.SelectedListViewItemCollection)
    End Function

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            ListView1.Items.Item(Me.ItemSelected).Tag = "0"
            ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Black
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            ListView1.Items.Item(Me.ItemSelected).Tag = "1"
            ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Black
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            ListView1.Items.Item(Me.ItemSelected).Tag = "2"
            ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Black
        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton4.CheckedChanged
        If RadioButton4.Checked = True Then
            ListView1.Items.Item(Me.ItemSelected).Tag = "3"
            ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Red
        End If
    End Sub

    Private Sub tmDenegado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmDenegado.Click
        ListView1.Items.Item(Me.ItemSelected).Tag = "0"
        ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Black
        RadioButton1.Checked = True
    End Sub

    Private Sub tmAutorizado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmAutorizado.Click
        ListView1.Items.Item(Me.ItemSelected).Tag = "1"
        ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Black
        RadioButton2.Checked = True
    End Sub

    Private Sub tmReadOnly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmReadOnly.Click
        ListView1.Items.Item(Me.ItemSelected).Tag = "2"
        ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Black
        RadioButton3.Checked = True
    End Sub

    Private Sub tmDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmDelete.Click
        ListView1.Items.Item(Me.ItemSelected).Tag = "3"
        ListView1.Items.Item(Me.ItemSelected).ForeColor = Color.Red
        RadioButton4.Checked = True
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)
        Dim ControlID, PermisoID As String
        Dim Mensaje As String = ""
        Dim Resum As String = ""
        Dim ResumDetalle As String = ""

        If txtID.Text = "" Then
            Call MsgBox("El ID del perfil est� vacio.", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information)
            txtID.BackColor = Color.PowderBlue
            txtID.Focus()
            Exit Sub
        End If

        Try
            Dim intPrueba As Integer = CInt(txtID.Text)
        Catch ex As Exception
            Call MsgBox("Tipo de dato incorrecto", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly)
            txtID.BackColor = Color.PowderBlue
            txtID.Focus()
            Exit Sub
        End Try

        If txtName.Text = "" Then
            Call MsgBox("El Nombre del perfil est� vacio.", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information)
            txtName.BackColor = Color.PowderBlue
            txtName.Focus()
            Exit Sub
        End If

        Try

            Select Case nTipoEdic
                Case 1
                    Mensaje = "Desea registrar la informaci�n?"
                Case 2
                    Mensaje = "Desea actualizar la informaci�n?"
                Case Else
                    Mensaje = "Desea eliminar el perfil?"
            End Select

            Dim result As Integer = MessageBox.Show(Mensaje, "Confirmaci�n de Acci�n", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then

                Resum = aSeguridad.editarPerfil(Me.txtID.Text.Trim, Me.txtName.Text.Trim, IIf(Me.cbxState.SelectedIndex = 0, False, True), 1)

                If Resum = "OK" Then
                    Dim TableData As DataTable = DetallesPermiso()

                    For i As Integer = 0 To TableData.Rows.Count - 1
                        ControlID = TableData.Rows(i).Item("ControlID").ToString
                        PermisoID = TableData.Rows(i).Item("PermisoID").ToString

                        If PermisoID = 3 Then
                            ResumDetalle = aSeguridad.editarPerfilDetalle(Me.txtID.Text.Trim, ControlID, PermisoID, 3)
                        Else
                            ResumDetalle = aSeguridad.editarPerfilDetalle(Me.txtID.Text.Trim, ControlID, PermisoID, 1)
                        End If

                    Next

                    frmMaestroSeguridad.cargarPerfiles()
                    aSeguridad.Dispose()
                    aSeguridad = Nothing
                    LimpiarTodo()
                End If

            End If

            aSeguridad.Dispose()
            aSeguridad = Nothing

        Catch ex As Exception

        End Try

    End Sub

    Private Function DetallesPermiso() As DataTable
        Dim dtDetalle As New DataTable("tblDetalle")
        Dim dc As DataColumn
        'string
        dc = New DataColumn
        dc.DataType = System.Type.GetType("System.String")
        dc.ColumnName = "ControlID"
        dtDetalle.Columns.Add(dc)
        'string
        dc = New DataColumn
        dc.DataType = System.Type.GetType("System.String")
        dc.ColumnName = "PermisoID"
        dtDetalle.Columns.Add(dc)
        Dim i As Integer
        For i = 0 To Me.ListView1.Items.Count - 1
            Dim drDetalle As DataRow
            drDetalle = dtDetalle.NewRow
            drDetalle("ControlID") = ListView1.Items.Item(i).Name.ToString
            drDetalle("PermisoID") = ListView1.Items.Item(i).Tag.ToString
            dtDetalle.Rows.Add(drDetalle)
        Next
        Return dtDetalle
    End Function

End Class