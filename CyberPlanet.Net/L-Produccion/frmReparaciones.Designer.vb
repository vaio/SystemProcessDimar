﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReparaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReparaciones))
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btn_guardar = New System.Windows.Forms.Button()
        Me.btn_nuevo = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txt_no_factura_articulo = New System.Windows.Forms.TextBox()
        Me.cmbCondicion_articulo = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_cantidad_articulo = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_descripcion_articulo = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btn_mostrar_articulos = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_cod_articulo = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txt_Correo = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_Telefono_2 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_Telefono_1 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.btn_add_client = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_Apellidos = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbTipoCliente = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_Cod_cliente = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Nombres = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grvRemision = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmb_ord_de_serv_Tipo_de_estado = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.richDescripcion = New System.Windows.Forms.RichTextBox()
        Me.cmbPrioridad = New System.Windows.Forms.ComboBox()
        Me.txt_ord_serv_cod = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CmbTipoOrden = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.mainPanel.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.Panel3)
        Me.mainPanel.Controls.Add(Me.GroupBox4)
        Me.mainPanel.Controls.Add(Me.GroupBox3)
        Me.mainPanel.Controls.Add(Me.GroupBox2)
        Me.mainPanel.Controls.Add(Me.GroupBox1)
        Me.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mainPanel.Location = New System.Drawing.Point(0, 0)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1161, 516)
        Me.mainPanel.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btn_guardar)
        Me.Panel3.Controls.Add(Me.btn_nuevo)
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 464)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1161, 52)
        Me.Panel3.TabIndex = 4
        '
        'btn_guardar
        '
        Me.btn_guardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_guardar.Location = New System.Drawing.Point(461, 6)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(83, 40)
        Me.btn_guardar.TabIndex = 4
        Me.btn_guardar.Text = "&Guardar"
        Me.btn_guardar.UseVisualStyleBackColor = True
        '
        'btn_nuevo
        '
        Me.btn_nuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_nuevo.Location = New System.Drawing.Point(372, 6)
        Me.btn_nuevo.Name = "btn_nuevo"
        Me.btn_nuevo.Size = New System.Drawing.Size(83, 40)
        Me.btn_nuevo.TabIndex = 3
        Me.btn_nuevo.Text = "&Nuevo"
        Me.btn_nuevo.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Controls.Add(Me.btnModificar)
        Me.Panel2.Controls.Add(Me.btnAgregar)
        Me.Panel2.Location = New System.Drawing.Point(2, 1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(284, 50)
        Me.Panel2.TabIndex = 1
        '
        'btnEliminar
        '
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(191, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(83, 40)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(102, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(83, 40)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(12, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(83, 40)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txt_no_factura_articulo)
        Me.GroupBox4.Controls.Add(Me.cmbCondicion_articulo)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.txt_cantidad_articulo)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.txt_descripcion_articulo)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.btn_mostrar_articulos)
        Me.GroupBox4.Controls.Add(Me.txt_cod_articulo)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Location = New System.Drawing.Point(775, 12)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(380, 155)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Datos del Articulo"
        '
        'txt_no_factura_articulo
        '
        Me.txt_no_factura_articulo.Location = New System.Drawing.Point(254, 72)
        Me.txt_no_factura_articulo.MaxLength = 10
        Me.txt_no_factura_articulo.Name = "txt_no_factura_articulo"
        Me.txt_no_factura_articulo.Size = New System.Drawing.Size(122, 20)
        Me.txt_no_factura_articulo.TabIndex = 89
        Me.txt_no_factura_articulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbCondicion_articulo
        '
        Me.cmbCondicion_articulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCondicion_articulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCondicion_articulo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cmbCondicion_articulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCondicion_articulo.FormattingEnabled = True
        Me.cmbCondicion_articulo.Location = New System.Drawing.Point(80, 98)
        Me.cmbCondicion_articulo.Name = "cmbCondicion_articulo"
        Me.cmbCondicion_articulo.Size = New System.Drawing.Size(295, 21)
        Me.cmbCondicion_articulo.TabIndex = 88
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(8, 101)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(57, 13)
        Me.Label21.TabIndex = 87
        Me.Label21.Text = "Condición:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(188, 75)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(66, 13)
        Me.Label17.TabIndex = 85
        Me.Label17.Text = "No. Factura:"
        '
        'txt_cantidad_articulo
        '
        Me.txt_cantidad_articulo.Location = New System.Drawing.Point(80, 71)
        Me.txt_cantidad_articulo.MaxLength = 5
        Me.txt_cantidad_articulo.Name = "txt_cantidad_articulo"
        Me.txt_cantidad_articulo.Size = New System.Drawing.Size(104, 20)
        Me.txt_cantidad_articulo.TabIndex = 84
        Me.txt_cantidad_articulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(8, 74)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(52, 13)
        Me.Label16.TabIndex = 83
        Me.Label16.Text = "Cantidad:"
        '
        'txt_descripcion_articulo
        '
        Me.txt_descripcion_articulo.BackColor = System.Drawing.Color.White
        Me.txt_descripcion_articulo.Location = New System.Drawing.Point(80, 46)
        Me.txt_descripcion_articulo.Name = "txt_descripcion_articulo"
        Me.txt_descripcion_articulo.ReadOnly = True
        Me.txt_descripcion_articulo.Size = New System.Drawing.Size(295, 20)
        Me.txt_descripcion_articulo.TabIndex = 82
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 49)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 13)
        Me.Label15.TabIndex = 81
        Me.Label15.Text = "Articulo:"
        '
        'btn_mostrar_articulos
        '
        Me.btn_mostrar_articulos.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mostrar_articulos.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_mostrar_articulos.Appearance.Options.UseFont = True
        Me.btn_mostrar_articulos.Appearance.Options.UseForeColor = True
        Me.btn_mostrar_articulos.Image = CType(resources.GetObject("btn_mostrar_articulos.Image"), System.Drawing.Image)
        Me.btn_mostrar_articulos.Location = New System.Drawing.Point(355, 19)
        Me.btn_mostrar_articulos.Name = "btn_mostrar_articulos"
        Me.btn_mostrar_articulos.Size = New System.Drawing.Size(20, 19)
        Me.btn_mostrar_articulos.TabIndex = 78
        Me.btn_mostrar_articulos.ToolTip = "Agregar un nuevo Proveedor"
        '
        'txt_cod_articulo
        '
        Me.txt_cod_articulo.BackColor = System.Drawing.Color.White
        Me.txt_cod_articulo.Location = New System.Drawing.Point(80, 19)
        Me.txt_cod_articulo.Name = "txt_cod_articulo"
        Me.txt_cod_articulo.ReadOnly = True
        Me.txt_cod_articulo.Size = New System.Drawing.Size(269, 20)
        Me.txt_cod_articulo.TabIndex = 80
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 21)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(43, 13)
        Me.Label14.TabIndex = 79
        Me.Label14.Text = "Codigo:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txt_Correo)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.txt_Telefono_2)
        Me.GroupBox3.Controls.Add(Me.Label19)
        Me.GroupBox3.Controls.Add(Me.txt_Telefono_1)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.btn_add_client)
        Me.GroupBox3.Controls.Add(Me.txt_Apellidos)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.cmbTipoCliente)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.txt_Cod_cliente)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txt_Nombres)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Location = New System.Drawing.Point(389, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(380, 155)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos del Cliente"
        '
        'txt_Correo
        '
        Me.txt_Correo.BackColor = System.Drawing.Color.White
        Me.txt_Correo.Enabled = False
        Me.txt_Correo.Location = New System.Drawing.Point(78, 124)
        Me.txt_Correo.Name = "txt_Correo"
        Me.txt_Correo.Size = New System.Drawing.Size(296, 20)
        Me.txt_Correo.TabIndex = 83
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 127)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(35, 13)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Email:"
        '
        'txt_Telefono_2
        '
        Me.txt_Telefono_2.BackColor = System.Drawing.Color.White
        Me.txt_Telefono_2.Enabled = False
        Me.txt_Telefono_2.Location = New System.Drawing.Point(263, 98)
        Me.txt_Telefono_2.Name = "txt_Telefono_2"
        Me.txt_Telefono_2.Size = New System.Drawing.Size(111, 20)
        Me.txt_Telefono_2.TabIndex = 81
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(197, 101)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(61, 13)
        Me.Label19.TabIndex = 80
        Me.Label19.Text = "Telefono 2:"
        '
        'txt_Telefono_1
        '
        Me.txt_Telefono_1.BackColor = System.Drawing.Color.White
        Me.txt_Telefono_1.Enabled = False
        Me.txt_Telefono_1.Location = New System.Drawing.Point(78, 98)
        Me.txt_Telefono_1.Name = "txt_Telefono_1"
        Me.txt_Telefono_1.Size = New System.Drawing.Size(111, 20)
        Me.txt_Telefono_1.TabIndex = 79
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 101)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(61, 13)
        Me.Label18.TabIndex = 78
        Me.Label18.Text = "Telefono 1:"
        '
        'btn_add_client
        '
        Me.btn_add_client.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_add_client.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_add_client.Appearance.Options.UseFont = True
        Me.btn_add_client.Appearance.Options.UseForeColor = True
        Me.btn_add_client.Image = CType(resources.GetObject("btn_add_client.Image"), System.Drawing.Image)
        Me.btn_add_client.Location = New System.Drawing.Point(354, 19)
        Me.btn_add_client.Name = "btn_add_client"
        Me.btn_add_client.Size = New System.Drawing.Size(20, 19)
        Me.btn_add_client.TabIndex = 16
        Me.btn_add_client.ToolTip = "Agregar un nuevo Proveedor"
        '
        'txt_Apellidos
        '
        Me.txt_Apellidos.BackColor = System.Drawing.Color.White
        Me.txt_Apellidos.Enabled = False
        Me.txt_Apellidos.Location = New System.Drawing.Point(78, 72)
        Me.txt_Apellidos.Name = "txt_Apellidos"
        Me.txt_Apellidos.Size = New System.Drawing.Size(296, 20)
        Me.txt_Apellidos.TabIndex = 77
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 75)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 76
        Me.Label10.Text = "Apellidos:"
        '
        'cmbTipoCliente
        '
        Me.cmbTipoCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbTipoCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbTipoCliente.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cmbTipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoCliente.Enabled = False
        Me.cmbTipoCliente.FormattingEnabled = True
        Me.cmbTipoCliente.Location = New System.Drawing.Point(78, 19)
        Me.cmbTipoCliente.Name = "cmbTipoCliente"
        Me.cmbTipoCliente.Size = New System.Drawing.Size(111, 21)
        Me.cmbTipoCliente.TabIndex = 75
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 13)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "Tipo Cliente:"
        '
        'txt_Cod_cliente
        '
        Me.txt_Cod_cliente.BackColor = System.Drawing.Color.White
        Me.txt_Cod_cliente.Enabled = False
        Me.txt_Cod_cliente.Location = New System.Drawing.Point(244, 19)
        Me.txt_Cod_cliente.Name = "txt_Cod_cliente"
        Me.txt_Cod_cliente.Size = New System.Drawing.Size(104, 20)
        Me.txt_Cod_cliente.TabIndex = 18
        Me.txt_Cod_cliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(195, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Codigo:"
        '
        'txt_Nombres
        '
        Me.txt_Nombres.BackColor = System.Drawing.Color.White
        Me.txt_Nombres.Enabled = False
        Me.txt_Nombres.Location = New System.Drawing.Point(78, 46)
        Me.txt_Nombres.Name = "txt_Nombres"
        Me.txt_Nombres.Size = New System.Drawing.Size(296, 20)
        Me.txt_Nombres.TabIndex = 64
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 63
        Me.Label4.Text = "Nombres:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Tabla)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 173)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1152, 290)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detalle de la Orden"
        '
        'Tabla
        '
        Me.Tabla.Location = New System.Drawing.Point(6, 19)
        Me.Tabla.MainView = Me.GridView1
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(1122, 264)
        Me.Tabla.TabIndex = 55
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1, Me.grvRemision})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'grvRemision
        '
        Me.grvRemision.GridControl = Me.Tabla
        Me.grvRemision.Name = "grvRemision"
        Me.grvRemision.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvRemision.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvRemision.OptionsBehavior.Editable = False
        Me.grvRemision.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvRemision.OptionsView.AllowHtmlDrawGroups = False
        Me.grvRemision.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvRemision.OptionsView.ShowFooter = True
        Me.grvRemision.OptionsView.ShowGroupPanel = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmb_ord_de_serv_Tipo_de_estado)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.richDescripcion)
        Me.GroupBox1.Controls.Add(Me.cmbPrioridad)
        Me.GroupBox1.Controls.Add(Me.txt_ord_serv_cod)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.CmbTipoOrden)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(380, 155)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Servicio"
        '
        'cmb_ord_de_serv_Tipo_de_estado
        '
        Me.cmb_ord_de_serv_Tipo_de_estado.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmb_ord_de_serv_Tipo_de_estado.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmb_ord_de_serv_Tipo_de_estado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cmb_ord_de_serv_Tipo_de_estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmb_ord_de_serv_Tipo_de_estado.Enabled = False
        Me.cmb_ord_de_serv_Tipo_de_estado.FormattingEnabled = True
        Me.cmb_ord_de_serv_Tipo_de_estado.Location = New System.Drawing.Point(239, 47)
        Me.cmb_ord_de_serv_Tipo_de_estado.Name = "cmb_ord_de_serv_Tipo_de_estado"
        Me.cmb_ord_de_serv_Tipo_de_estado.Size = New System.Drawing.Size(136, 21)
        Me.cmb_ord_de_serv_Tipo_de_estado.TabIndex = 92
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(4, 72)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(68, 18)
        Me.Label22.TabIndex = 89
        Me.Label22.Text = "Descripción:"
        '
        'richDescripcion
        '
        Me.richDescripcion.Location = New System.Drawing.Point(73, 74)
        Me.richDescripcion.MaxLength = 250
        Me.richDescripcion.Name = "richDescripcion"
        Me.richDescripcion.Size = New System.Drawing.Size(301, 71)
        Me.richDescripcion.TabIndex = 88
        Me.richDescripcion.Text = ""
        '
        'cmbPrioridad
        '
        Me.cmbPrioridad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbPrioridad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbPrioridad.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cmbPrioridad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrioridad.FormattingEnabled = True
        Me.cmbPrioridad.Location = New System.Drawing.Point(73, 44)
        Me.cmbPrioridad.Name = "cmbPrioridad"
        Me.cmbPrioridad.Size = New System.Drawing.Size(111, 21)
        Me.cmbPrioridad.TabIndex = 78
        '
        'txt_ord_serv_cod
        '
        Me.txt_ord_serv_cod.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_ord_serv_cod.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txt_ord_serv_cod.Location = New System.Drawing.Point(239, 19)
        Me.txt_ord_serv_cod.Name = "txt_ord_serv_cod"
        Me.txt_ord_serv_cod.Size = New System.Drawing.Size(135, 22)
        Me.txt_ord_serv_cod.TabIndex = 77
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(4, 49)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 18)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Prioridad:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(190, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 66
        Me.Label5.Text = "Estado:"
        '
        'CmbTipoOrden
        '
        Me.CmbTipoOrden.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CmbTipoOrden.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbTipoOrden.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.CmbTipoOrden.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbTipoOrden.FormattingEnabled = True
        Me.CmbTipoOrden.Location = New System.Drawing.Point(73, 19)
        Me.CmbTipoOrden.Name = "CmbTipoOrden"
        Me.CmbTipoOrden.Size = New System.Drawing.Size(111, 21)
        Me.CmbTipoOrden.TabIndex = 62
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 61
        Me.Label3.Text = "Tipo Orden:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(190, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Codigo:"
        '
        'frmReparaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1161, 516)
        Me.Controls.Add(Me.mainPanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReparaciones"
        Me.Text = "Orden de Servicio"
        Me.mainPanel.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvRemision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_Cod_cliente As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CmbTipoOrden As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_Nombres As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvRemision As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbTipoCliente As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_Apellidos As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_ord_serv_cod As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btn_add_client As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_cantidad_articulo As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_descripcion_articulo As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btn_mostrar_articulos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txt_cod_articulo As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_Correo As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txt_Telefono_2 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txt_Telefono_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cmbPrioridad As System.Windows.Forms.ComboBox
    Friend WithEvents richDescripcion As System.Windows.Forms.RichTextBox
    Friend WithEvents cmbCondicion_articulo As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents cmb_ord_de_serv_Tipo_de_estado As System.Windows.Forms.ComboBox
    Friend WithEvents btn_nuevo As System.Windows.Forms.Button
    Friend WithEvents btn_guardar As System.Windows.Forms.Button
    Friend WithEvents txt_no_factura_articulo As System.Windows.Forms.TextBox
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
End Class
