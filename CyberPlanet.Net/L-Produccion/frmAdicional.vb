﻿Imports System.Xml

Public Class frmAdicional

    Private Buscar_producto As New frmBusqueda_Prod("@cod_bodega", 3, SqlDbType.Int)

    Private Validar_cantidad As New Validar

    Public Cod_Orden As String
    Private strsql As String



    Public Sub Cerrar()

        Me.Dispose()
        Close()


    End Sub

    Sub CargarDatosProducto(ByVal nNumProd As String)
        Dim strsql As String = ""

        If NumCatalogo = 39 Then
            strsql = "SELECT Prd.Codigo_Producto, Prd.Nombre_Producto, dbo.getCostoProducto(Prd.Codigo_Producto) As Costo_Promedio, Inv.UndTotal_Existencia As 'Existencia_Actual' FROM Productos Prd INNER JOIN "
            strsql = strsql & "Inventario Inv ON Prd.Codigo_Producto = Inv.Codigo_Producto where Prd.Codigo_Producto = '" & nNumProd & "' And Inv.Codigo_Bodega = " & frmTraslados.cmbBdOrigen.SelectedValue
        
        Else
            strsql = "SELECT Codigo_Producto,Nombre_Producto,dbo.getCostoProducto(Codigo_Producto) As Costo_Promedio,Existencia_Actual from Productos where Codigo_Producto = '" & nNumProd & "'"

        End If

        Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)

        If TblDatosProd.Rows.Count <> 0 Then
            lblDescripcion.Text = TblDatosProd.Rows(0).Item("Nombre_Producto")
            lblExistencia.Text = TblDatosProd.Rows(0).Item("Existencia_Actual")
            lblCostoUnitario.Text = Format(TblDatosProd.Rows(0).Item("Costo_Promedio"), "###,###,###.0000")
            txtCantidad.Text = 0
            txtCantidad.Focus()
            txtCantidad.Select()
        Else
            MsgBox("No existe ningún producto existente con el código ingresado.", MsgBoxStyle.Information, "Información")
            txtCodigoProd.Text = ""
            txtCodigoProd.Focus()
        End If
    End Sub

    Sub CargarDatosDetalle()
        Dim strsql, Codigo, nNumProd As String
        Dim Bandera As Boolean = False
        If NumCatalogo = 38 Then
            If frmOrdenPedido.grvDetalle.RowCount > 0 And nTipoEdicDet = 2 Then
                Codigo = frmOrdenPedido.txtNumDoc.Text
                nNumProd = frmOrdenPedido.grvDetalle.GetRowCellValue(frmOrdenPedido.grvDetalle.GetSelectedRows(0), "Codigo").ToString()
                strsql = "SELECT Cant_Requerida As 'Cantidad', Costo_Unitario FROM Orden_Pedido_Detalle WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Pedido = '" & Codigo & "' and Codigo_Producto = '" & nNumProd & "'"
                Bandera = True
            End If
        ElseIf NumCatalogo = 39 Then
            If frmTraslados.grvDetalle.RowCount > 0 And nTipoEdicDet = 2 Then
                Codigo = frmTraslados.txtCodigo.Text
                nNumProd = frmTraslados.grvDetalle.GetRowCellValue(frmTraslados.grvDetalle.GetSelectedRows(0), "Codigo").ToString()
                strsql = "SELECT Cantidad, Costo_Unitario, Costo_Promedio FROM Orden_Traslado_Detalle WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Traslado = '" & Codigo & "' and Codigo_Producto = '" & nNumProd & "'"
                Bandera = True
            End If
        End If

        If Bandera = True Then
            Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)



            txtCodigoProd.Text = nNumProd

            If NumCatalogo = 38 Then
                lblCostoUnitario.Text = Format(TblDatosProd.Rows(0).Item("Costo_Unitario"), "###,###,###.0000")

            ElseIf NumCatalogo = 39 Then
                If frmTraslados.Aplicado = False Then
                    lblCostoUnitario.Text = Format(TblDatosProd.Rows(0).Item("Costo_Promedio"), "###,###,###.0000")
                Else
                    lblCostoUnitario.Text = Format(TblDatosProd.Rows(0).Item("Costo_Unitario"), "###,###,###.0000")
                End If
            End If

            txtCantidad.Text = TblDatosProd.Rows(0).Item("Cantidad")

            txtCantidad.Focus()
            txtCantidad.Select()

        End If
    End Sub

    Private Sub frmAdicional_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCodigoProd.Text = ""
        lblDescripcion.Text = ""
        lblCostoUnitario.Text = "0.0000"
        lblExistencia.Text = "0.0000"
        txtCantidad.Text = 0
        txtCodigoProd.Focus()

        Select Case NumCatalogo

            Case 23

                With frmOrdenProd

                    Cod_Orden = .lblCodigoOrden.Text

                End With

                strsql = "select * from vw_ORDEN_PROD_SUSTITUTO where Orden='" + Cod_Orden + "' and Sucursal=" & My.Settings.Sucursal & ""

                Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)
                dgvSustituto.DataSource = TblDatosProd
                dgvSustituto.Columns("Sucursal").Visible = False
                dgvSustituto.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill

            Case 27
                PictureBox1.Visible = False
                txtCod_Receta.Visible = False
                lbRund.Visible = False

                txtCod_Receta.Text = "asdas"

            Case 38 To 39

                PictureBox1.Visible = False
                txtCod_Receta.Visible = False
                lbRund.Visible = False

                CargarDatosDetalle()

        End Select

        Expand()

        Validar_cantidad.agregar_control(New List(Of Control)({txtCantidad})).ActivarEventosNumericos()

    End Sub

    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged
        lblCostoTotal.Text = (ChangeToDouble(lblCostoUnitario.Text) * ChangeToDouble(txtCantidad.Text))

        If NumCatalogo = 23 Then
            If ChangeToDouble(txtCantidad.Text) > ChangeToDouble(lblExistencia.Text) Then
                txtCantidad.Text = lblExistencia.Text
                MsgBox("La cantidad no puede ser mayor que la existencia.", MsgBoxStyle.Information, "Información")
            Else
                lblCostoTotal.Text = (ChangeToDouble(lblCostoUnitario.Text) * ChangeToDouble(txtCantidad.Text))
            End If
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim Codigo As String = ""
        Dim Producto As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try
            If NumCatalogo = 23 Then

                Codigo = frmOrdenProd.lblCodigoOrden.Text
                Producto = frmOrdenProd.cmbProducProducir.SelectedValue.ToString()

                If Not lblDescripcion.Text = "" Then

                    If CDbl(txtCantidad.Text) <= CDbl(lblExistencia.Text) Then

                        Resum = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, Codigo, Producto, txtCodigoProd.Text, txtCantidad.Text, 0, lblCostoUnitario.Text, True, False, 1, txtCod_Receta.Text)

                        If Resum = "OK" Then
                            frmOrdenProd.CargarDetalle()
                            Cerrar()
                        End If

                    Else
                        MsgBox("No hay en existencia para la cantidad requerida.", MsgBoxStyle.Critical, "Información")
                    End If

                Else
                    MsgBox("Debe seleccionar un material.", MsgBoxStyle.Information, "Información")
                End If

            ElseIf NumCatalogo = 27 Then
                Codigo = frmProyeccion.txtCodigoOrden.Text

                Resum = Produccion.EditProyDetalle(Codigo, txtCodigoProd.Text, txtCantidad.Text, lblCostoUnitario.Text, lblCostoTotal.Text, 1)

                If Resum = "OK" Then
                    frmProyeccion.CargarDetalle(Codigo)
                    Cerrar()
                End If

            ElseIf NumCatalogo = 38 Then
                Codigo = frmOrdenPedido.txtNumDoc.Text

                Resum = Inventario.EditarPedidoDeta(My.Settings.Sucursal, Codigo, txtCodigoProd.Text, CDbl(lblExistencia.Text), CDbl(txtCantidad.Text), CDbl(lblCostoUnitario.Text), CDbl(lblCostoTotal.Text), nTipoEdicDet)

                If Resum = "OK" Then
                    frmOrdenPedido.CargarDetalle(Codigo)
                    Cerrar()
                End If

            ElseIf NumCatalogo = 39 Then
                Codigo = frmTraslados.txtCodigo.Text

                Resum = Inventario.EditarTrasladoDeta(My.Settings.Sucursal, Codigo, txtCodigoProd.Text, CDbl(txtCantidad.Text), CDbl(lblCostoUnitario.Text), nTipoEdicDet)

                If Resum = "OK" Then
                    frmTraslados.CargarDetalle(Codigo)
                    Cerrar()
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtCodigoProd_MouseClick(sender As Object, e As MouseEventArgs) Handles txtCodigoProd.MouseClick
        If NumCatalogo = 23 Then

            Buscar_producto.mostrar_por_bodega(3)

            If Buscar_producto.get_info.get_codigo = String.Empty Then Exit Sub

            txtCodigoProd.Text = Buscar_producto.get_info.get_codigo
            lblDescripcion.Text = Buscar_producto.get_info.get_descripcion
            lblCostoUnitario.Text = FormatNumber(Buscar_producto.get_info.get_costo, 4)
            lblExistencia.Text = FormatNumber(Buscar_producto.get_info.get_existencia, 4)

        Else
            frmBusquedaProd.Show()

        End If


    End Sub
    Private Sub txtCod_Receta_MouseClick(sender As Object, e As MouseEventArgs) Handles txtCod_Receta.MouseClick
        Expand()
    End Sub
    Sub Expand()
        If dgvSustituto.Visible = False Then
            Me.Height = 435
            dgvSustituto.Visible = True

        Else
            Me.Height = 237
            dgvSustituto.Visible = False
        End If
    End Sub

    Private Sub txtCodigoProd_TextChanged(sender As Object, e As EventArgs) Handles txtCodigoProd.TextChanged

        If Not txtCodigoProd.Text = "" Then
            CargarDatosProducto(txtCodigoProd.Text)
        Else
            lblDescripcion.Text = ""
            lblCostoUnitario.Text = "0.0000"
            lblExistencia.Text = "0.0000"
            txtCantidad.Text = 0
            txtCodigoProd.Focus()
        End If
    End Sub

    Private Sub lblCostoTotal_TextChanged(sender As Object, e As EventArgs) Handles lblCostoTotal.TextChanged, txtCod_Receta.TextChanged

        Dim valor As Decimal = ChangeToDouble(lblCostoTotal.Text)

        If valor > 0 And IsNumeric(valor) = True And (txtCod_Receta.Text <> "" Or txtCod_Receta.Visible = False) Then

            cmdAceptar.Enabled = True
        Else
            cmdAceptar.Enabled = False
        End If
    End Sub

    Private Sub dgvSustituto_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvSustituto.CellMouseDoubleClick
        Try
            txtCod_Receta.Text = dgvSustituto.Rows(dgvSustituto.CurrentRow.Index).Cells("Codigo").Value.ToString
            lbRund.Text = dgvSustituto.Rows(dgvSustituto.CurrentRow.Index).Cells("Unidad").Value.ToString
            txtCantidad.Focus()
        Catch ex As Exception
        End Try
        Expand()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub frmAdicional_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Cerrar()
    End Sub
End Class