﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
'-----------------------------
Imports System.Xml
Imports DevExpress.XtraGrid.Columns
Imports System.Threading
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmOrdenProd
    Private _propertyName As Rectangle
    Public IdEstado As Integer
    Public ResumMaster As String = ""
    Public ResumMDetalle As String = ""
    Public ResumEntrada As String = ""
    Public ResumSalida As String = ""
    Public ResumEntradaDeta As String = ""
    Public ResumSalidaDeta As String = ""
    Public CodMaxEntrada As String = ""
    Public CodMaxSalida As String = ""
    Public CodTipo As Integer = 0
    Public Autorizado As Boolean = False
    Dim Color_Existencia As Color = Color.Coral
    Dim Color_Adicional As Color = Color.Gold
    Dim Color_Autorizado As Color = Color.MediumSpringGreen
    Dim Color_Remplazado As Color = Color.Violet
    '-PRINT------------
    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    Dim misValue As Object = System.Reflection.Missing.Value
    '------------------

    Public Sub CargarDatos(ByVal Codigo As String)
        Dim strsql As String = ""
        Try
            strsql = "SELECT Codigo_Orden_Produc, Solicitante, Registrado, Cerrado, Activo, Cod_Producto_Master, Cantidad_Requerida, " & _
                     "Cantidad_Elaborado, Autorizado FROM Orden_Produccion where CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Orden_Produc = '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatosOrden", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count <> 0 Then
                obtenerEstado(Codigo)

                lblCodigoOrden.Text = tblDatos.Rows(0).Item("Codigo_Orden_Produc")
                CmbSolicitante.SelectedValue = tblDatos.Rows(0).Item("Solicitante")
                dtpRegistrado.Text = tblDatos.Rows(0).Item("Registrado")
                dtpCerrado.Text = IIf(tblDatos.Rows(0).Item("Cerrado") Is DBNull.Value, "", tblDatos.Rows(0).Item("Cerrado"))
                ckIsActivo.Checked = tblDatos.Rows(0).Item("Activo")
                cmbProducProducir.SelectedValue = tblDatos.Rows(0).Item("Cod_Producto_Master")
                txtCantidadPro.Text = tblDatos.Rows(0).Item("Cantidad_Requerida")
                txtElaborado.Text = tblDatos.Rows(0).Item("Cantidad_Elaborado")
                If txtElaborado.Text = 0 Then
                    CargarListaNoUtilizada()
                End If
                Autorizado = tblDatos.Rows(0).Item("Autorizado")

                ActivarControles()
                ActivarPaneles()
                cambiarTexto()

                CargarDetalle()
                CargarRemision(Codigo)
                CargarFinal(Codigo)
                'Print()
            End If
        Catch ex As Exception
        End Try

    End Sub

    Public Sub cargarSolicitante()
        CmbSolicitante.DataSource = SQL("select Id_Empleado as 'Codigo',Nombres+' '+Apellidos as 'Nombre Completo' from Tbl_Empleados where Id_Cargo=8 and Activo=1", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
        'CmbSolicitante.DataSource = SQL("SELECT  Codigo_Supervisor AS 'Codigo', Nombre_Supervisor AS 'Nombre Completo' FROM  Supervisores WHERE (Activo = 1) ORDER BY 'Codigo'", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0) ' CAMBIO DE OBTENCION DE EMPLEADP: TBL SUPERVISOR A TBL_EMPLEADO
        CmbSolicitante.DisplayMember = "Nombre Completo"
        CmbSolicitante.ValueMember = "Codigo"
    End Sub

    Private Sub cargarProductoFinal()
        Dim strsql As String = ""

        strsql = "select Codigo_Producto AS Codigo, (Codigo_Producto + ' - ' + Nombre_Producto) AS Nombre from [dbo].[Productos] where Id_ProductosTipo in (1,2) "
        strsql = strsql + "and Codigo_Producto in (Select rec.Codigo_Prod_Resultante from Tbl_Recetas rec where rec.Activo = 1) order by Codigo_Producto asc "

        cmbProducProducir.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
        cmbProducProducir.DisplayMember = "Nombre"
        cmbProducProducir.ValueMember = "Codigo"
    End Sub

    Public Sub CargarFinal(ByVal Codigo As String)
        Dim strsql As String = ""

        Try
            strsql = "SELECT [Cod. Producto], [Nombre Producto], Requerido, " + txtElaborado.Text + " As Elaborado, Medida, [C. Estimado], [C. No Utilizado], [Cif/Mod], [C. Real] " & _
                     "FROM  vwVerOrdenProductoFinal where [Cod. OrdenProd] =  '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatosOrden", My.Settings.SolIndustrialCNX).Tables(0)
            grdFinal.DataSource = tblDatos

            If tblDatos.Rows.Count > 0 Then
                lblCif.Text = tblDatos.Rows(0).Item("Cif/Mod")
            End If

        Catch ex As Exception
        End Try

    End Sub

    Public Sub cambiarTexto()
        Dim strsql As String = "SELECT PRDT.Id_ProductosTipo, PRDT.Descripion, Unidades_Medida.Descripcion_Unidad As Medida FROM Productos AS PRD " & _
                               "INNER JOIN Productos_Tipo AS PRDT ON PRD.Id_ProductosTipo = PRDT.Id_ProductosTipo INNER JOIN Unidades_Medida ON " & _
                               "PRD.Codigo_UnidMedida = Unidades_Medida.Codigo_UnidadMed WHERE (PRD.Codigo_Producto = '" + cmbProducProducir.SelectedValue.ToString + "')"

        Dim tblDatos As DataTable = SQL(strsql, "tblDato", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatos.Rows.Count <> 0 Then
            txtUnidadMed.Text = tblDatos.Rows(0).Item("Medida")
            CodTipo = tblDatos.Rows(0).Item("Id_ProductosTipo")
            btnCerrarOrden.Text = "Liquidar y Cerrar Orden"

            Select Case CodTipo
                Case 0
                    lblTextoFinal.Text = "MATERIA PRIMA ELABORADO"
                Case 1
                    lblTextoFinal.Text = "PRODUCTO SEMI-PROCESADO ELABORADO"
                Case 2
                    lblTextoFinal.Text = "PRODUCTO TERMINADO ELABORADO"
                    btnCerrarOrden.Text = "Trasladar a Cierre"
                Case Else
                    lblTextoFinal.Text = "SIN ESPECIFICAR"
            End Select
        End If
    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",") Then
            e.Handled = True
            'ElseIf e.KeyChar = "," Then                    NO PERMITIR COMAS
            '    e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub CargarLista()
        Dim strsql As String = ""
        Dim CostoUnd As Double = 0
        Dim CostoTotal As Double = 0

        grvDetalle.Columns.Clear()
        If (cmbProducProducir.Items.Count > 0 And txtCantidadPro.Text >= "0") Then
            strsql = "exec [dbo].[SP_GET_Receta_MateriaPrima] '" & cmbProducProducir.SelectedValue.ToString & "', '" & txtCantidadPro.Text & "'"

            Dim Detalle As DataTable = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
            grdDetalle.DataSource = Detalle
            grvDetalle.Columns("Codigo").Width = 75
            grvDetalle.Columns("Existencia").Width = 80
            grvDetalle.Columns("Existencia").SummaryItem.DisplayFormat = "{0:#.##}"
            grvDetalle.Columns("Cantidad").Width = 80
            grvDetalle.Columns("Cantidad").SummaryItem.DisplayFormat = "{0:#.##}"
            grvDetalle.Columns("Materia Prima").Width = 300
            grvDetalle.Columns("Medida").Width = 48
            grvDetalle.Columns("Costo Und").Width = 100
            grvDetalle.Columns("Costo Total").Width = 100

            grvDetalle.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvDetalle.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:#.####}"
            grvDetalle.OptionsView.ShowFooter = True

            grvDetalle.OptionsSelection.EnableAppearanceFocusedRow = False

            If Detalle.Rows.Count > 0 Then
                For i As Integer = 0 To Detalle.Rows.Count - 1
                    CostoUnd = CostoUnd + ((CDbl(Detalle.Rows(i).Item("Cantidad").ToString) / CDec(txtCantidadPro.Text)) * CDbl(Detalle.Rows(i).Item("Costo Und").ToString))
                    CostoTotal = CostoTotal + Detalle.Rows(i).Item("Costo Total").ToString
                Next i

                lblCostoUnd.Text = CDbl(CostoUnd)
                lblCostoTotal.Text = CDbl(CostoTotal)

            Else
                lblCostoUnd.Text = 0
                lblCostoTotal.Text = 0
            End If
        End If

    End Sub
    Private Sub CargarListaNoUtilizada()
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim strsql As String = ""
        Dim Cantidad As Decimal = 0
        Dim Codigo As String = ""
        Dim CantidadDet As String = ""
        Dim Costo As String = ""

        grvRemision.Columns.Clear()
        Try
            Cantidad = (CInt(txtCantidadPro.Text) - CInt(txtElaborado.Text))

            If (Convert.ToInt32(cmbProducProducir.SelectedValue(0)) > 0) And Cantidad >= 0 And IdEstado = 3 Then
                Try
                    strsql = "exec [dbo].[SP_GET_Receta_MateriaPrima] '" & cmbProducProducir.SelectedValue.ToString & "', '" & Cantidad & "'"
                    Dim Table As DataTable = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
                    Dim TableDos As DataTable

                    For i As Integer = 0 To Table.Rows.Count - 1
                        Codigo = Table.Rows(i).Item("Codigo").ToString
                        CantidadDet = Table.Rows(i).Item("Cantidad").ToString

                        strsql = "SELECT Costo_Unitario FROM Orden_Produccion_Detalle WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Orden_Produc = '" & lblCodigoOrden.Text & "' and Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "' and Cod_Producto_Detalle = '" & Codigo & "'"
                        TableDos = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)

                        If TableDos.Rows.Count > 0 Then
                            Costo = TableDos.Rows(0).Item("Costo_Unitario").ToString
                            ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, 0, CantidadDet, Costo, False, False, 2, Nothing)
                        End If
                    Next i
                    Try
                        Dim tabletres As DataTable
                        strsql = "select * from Orden_Produccion_Detalle where Codigo_Orden_Produc='" + lblCodigoOrden.Text + "' and CodigoSucursal=" & My.Settings.Sucursal & " and Autorizado=1"
                        tabletres = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
                        If tabletres.Rows.Count > 0 Then
                            For i = 0 To tabletres.Rows.Count - 1
                                Codigo = tabletres.Rows(i).Item("Cod_Producto_Detalle").ToString
                                Cantidad = tabletres.Rows(i).Item("Cantidad_Requerida").ToString
                                CantidadDet = (Cantidad / CDec(txtCantidadPro.Text) * CDec(txtElaborado.Text))
                                Cantidad = Cantidad - CantidadDet
                                Costo = tabletres.Rows(i).Item("Costo_Unitario").ToString
                                ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, 0, Cantidad, Costo, True, True, 2, Nothing)
                            Next
                        End If
                    Catch ex As Exception

                    End Try
                    CargarRemision(lblCodigoOrden.Text)
                    CargarFinal(lblCodigoOrden.Text)
                Catch ex As Exception
                End Try


            ElseIf Cantidad < 0 Then
                MsgBox("La cantidad elaborada no puede ser mayor que: " & txtCantidadPro.Text)
                txtElaborado.Text = txtCantidadPro.Text
                txtElaborado.Select()
            End If
        Catch ex As Exception
            txtElaborado.Text = 0
            txtElaborado.SelectAll()
        End Try
    End Sub
    Sub updateNoUtilizado()
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Codigo As String = ""
        Dim Cantidad As String = ""
        Dim Costo As String = ""
        Try
            For i As Integer = 0 To grvRemision.DataRowCount - 1
                Codigo = grvRemision.GetRowCellValue(i, "Codigo")
                Cantidad = grvRemision.GetRowCellValue(i, "Cantidad")
                Costo = grvRemision.GetRowCellValue(i, "Costo Und")

                ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, 0, Cantidad, Costo, False, False, 2, Nothing)
            Next i
            CargarRemision(lblCodigoOrden.Text)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ActivarControles()
        CmbSolicitante.Enabled = False
        cmbProducProducir.Enabled = False
        txtCantidadPro.Enabled = False
        dtpRegistrado.Enabled = False
        dtpCerrado.Enabled = False
        ckIsActivo.Enabled = False
        btnAgregarMat.Enabled = False
        btnQuitarMat.Enabled = False
        btnAplicar.Enabled = False
        btnAutorizar.Enabled = False

        txtElaborado.Enabled = False
        'txtColaboradores.enbaled =False
        btnLiquidar.Enabled = False
        btnCerrarOrden.Enabled = False

        If IdEstado = 0 Then
            If nTipoEdic = 1 Or nTipoEdic = 2 Then
                CmbSolicitante.Enabled = True
                cmbProducProducir.Enabled = True
                txtCantidadPro.Enabled = True
                dtpRegistrado.Enabled = True
                'ckIsActivo.Enabled = True
            End If
        ElseIf IdEstado = 1 Then
            txtElaborado.Enabled = True
            'txtColaboradores.enbaled =True
            btnLiquidar.Enabled = True
        ElseIf IdEstado = 2023 Or IdEstado = 2024 Then
            btnAgregarMat.Enabled = True
            btnQuitarMat.Enabled = True
            btnAplicar.Enabled = True
        ElseIf IdEstado = 2025 Then
            btnAutorizar.Enabled = True
        ElseIf IdEstado = 3 Then
            btnCerrarOrden.Enabled = True

            txtElaborado.Enabled = True
            'txtColaboradores.enbaled =True
            btnLiquidar.Enabled = True

            Select Case CodTipo
                Case 1
                    dtpCerrado.Value = Now.Date
                    dtpCerrado.Enabled = True
                Case 2
                    dtpCerrado.Enabled = False
            End Select
        End If
    End Sub

    Private Sub ActivarPaneles()
        Panel2.Enabled = False
        pnlAgregar.Enabled = False
        'pnlRemision.Enabled = False
        Panel3.Enabled = False

        If IdEstado = 0 Then
            If nTipoEdic = 1 Then
                Panel2.Enabled = True
            End If

        ElseIf IdEstado = 2023 Or IdEstado = 2024 Or IdEstado = 2025 Then
            Panel2.Enabled = True

        ElseIf IdEstado = 1 Or IdEstado = 3 Then
            Panel2.Enabled = True
            pnlAgregar.Enabled = True
            'pnlRemision.Enabled = True
            Panel3.Enabled = True

            If IdEstado = 3 Then
                pnlAgregar.Enabled = False
            End If

        ElseIf IdEstado = 2029 Then
            Panel2.Enabled = True
            'pnlRemision.Enabled = True
            Panel3.Enabled = True
        End If

    End Sub

    Public Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = True
        frmPrincipal.bbiEliminar.Enabled = True
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Private Sub obtenerEstado(ByVal Codigo As String)
        Dim strsql As String = ""
        Dim valor As String = ""

        strsql = "SELECT TOP 1 CD.IdCategoriaDetalle, CD.Estado FROM Orden_Produccion OP INNER JOIN Categorias_Detalle CD ON OP.Estado = CD.IdCategoriaDetalle WHERE (OP.Activo = 1) AND (OP.Codigo_Orden_Produc = '" & Codigo & "')"
        Dim Data As DataTable = SQL(strsql, "tblEstado", My.Settings.SolIndustrialCNX).Tables(0)

        If Data.Rows.Count > 0 Then
            IdEstado = Data.Rows(0).Item("IdCategoriaDetalle")
            lblEstado.Text = Data.Rows(0).Item("Estado").ToString
        Else
            IdEstado = 0
            lblEstado.Text = "SIN REGISTRAR"
        End If

        Select Case IdEstado
            Case 1
                pnlAgregar.Enabled = True
                btnCerrarOrden.Enabled = False
                txtElaborado.Enabled = True
                'txtColaboradores.enbaled =True
                btnLiquidar.Enabled = True
            Case 3
                pnlAgregar.Enabled = False
                btnCerrarOrden.Enabled = True
                txtElaborado.Enabled = True
                'txtColaboradores.enbaled =True
                btnLiquidar.Enabled = True
            Case 19
                pnlAgregar.Enabled = False
                btnCerrarOrden.Enabled = False
                txtElaborado.Enabled = False
                'txtColaboradores.enbaled =False
                btnLiquidar.Enabled = False
        End Select

    End Sub

    Public Sub CargarDetalle()
        Dim strsql As String = ""
        Dim CostoUnd As Double = 0
        Dim CostoTotal As Double = 0
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        grvDetalle.Columns.Clear()

        Try
            strsql = "SELECT Codigo, [Existencia], [Cantidad], [Materia Prima], Medida, [Costo Und], [Costo Total], EsAdicional, Autorizado, Sustituto " & _
                     "FROM vwVerOrdenDetalle where CodigoSucursal = " & My.Settings.Sucursal & " and Orden = '" & lblCodigoOrden.Text & "' and Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "'"

            Dim Detalle As DataTable = SQL(strsql, "tblDetalle", My.Settings.SolIndustrialCNX).Tables(0)

            grdDetalle.DataSource = Detalle
            grvDetalle.Columns("Codigo").Width = 75
            grvDetalle.Columns("Existencia").Width = 80
            grvDetalle.Columns("Existencia").SummaryItem.DisplayFormat = "{0:#.##}"
            grvDetalle.Columns("Cantidad").Width = 80
            grvDetalle.Columns("Cantidad").SummaryItem.DisplayFormat = "{0:#.##}"
            grvDetalle.Columns("Materia Prima").Width = 300
            grvDetalle.Columns("Medida").Width = 50
            grvDetalle.Columns("Costo Und").Width = 100
            grvDetalle.Columns("Costo Total").Width = 100
            grvDetalle.Columns("EsAdicional").Visible = False
            grvDetalle.Columns("Autorizado").Visible = False
            grvDetalle.Columns("Sustituto").Visible = False

            grvDetalle.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvDetalle.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:#.####}"
            grvDetalle.OptionsView.ShowFooter = True

            grvDetalle.OptionsSelection.EnableAppearanceFocusedRow = False

            If Detalle.Rows.Count > 0 Then
                Dim Cantidad As Decimal = (CInt(txtCantidadPro.Text) - CInt(txtElaborado.Text))
                For i As Integer = 0 To Detalle.Rows.Count - 1
                    CostoUnd = CostoUnd + ((CDbl(Detalle.Rows(i).Item("Cantidad").ToString) / CInt(txtCantidadPro.Text)) * CDbl(Detalle.Rows(i).Item("Costo Und").ToString))
                    CostoTotal = CostoTotal + Detalle.Rows(i).Item("Costo Total").ToString

                    'If Detalle.Rows(i).Item("Autorizado") = True Then
                    '    Dim Codigo As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(i), "Codigo")
                    '    Dim CantidadDet As Decimal = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(i), "Cantidad") / Cantidad
                    '    Dim Costo As Decimal = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(i), "Costo Und") / Cantidad
                    '    ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, 0, CantidadDet, Costo, True, True, 2)
                    'End If
           
                Next i



                'For i = 0 To grvDetalle.RowCount - 1
                '    'If grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(i), "Autorizado") = 1 Then

                '    '    'Codigo = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo")
                '    '    'CantidadDet = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad") / Cantidad
                '    '    'Costo = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Und") / Cantidad
                '    '    'ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, 0, CantidadDet, Costo, False, False, 2)
                '    'End If
                'Next







                lblCostoUnd.Text = CDbl(CostoUnd)
                lblCostoTotal.Text = CDbl(CostoTotal)

            Else
                lblCostoUnd.Text = 0
                lblCostoTotal.Text = 0
            End If
            For i = 0 To grvDetalle.RowCount - 1
                If grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(i), "Autorizado") = 1 Then

                    'Codigo = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo")
                    'CantidadDet = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad") / Cantidad
                    'Costo = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Und") / Cantidad
                    'ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, 0, CantidadDet, Costo, False, False, 2)
                End If
            Next

        Catch ex As Exception
        End Try
    End Sub

    Private Sub CargarRemision(ByVal CodigoOrden As String)
        Dim strsql As String = ""
        Dim strsql2 As String = ""

        Try
            strsql = "SELECT PRD.Codigo_Producto As 'Codigo', ORD.Cantidad_NoUtilizada AS 'Cantidad', PRD.Nombre_Producto As 'Materia Prima', UMD.Descripcion_Unidad As Medida, ORD.Costo_Unitario AS 'Costo Und.' , (ORD.Cantidad_NoUtilizada * ORD.Costo_Unitario) AS 'Costo Total', ORD.EsAdicional, ORD.Autorizado FROM Orden_Produccion_Detalle ORD INNER JOIN Productos PRD ON ORD.Cod_Producto_Detalle = PRD.Codigo_Producto INNER JOIN Unidades_Medida UMD ON PRD.Codigo_UnidMedida = UMD.Codigo_UnidadMed where ORD.Cantidad_NoUtilizada > 0 and ORD.CodigoSucursal = " & My.Settings.Sucursal & " and ORD.Codigo_Orden_Produc = '" & CodigoOrden & "' and ORD.Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "' and ORD.Cantidad_Requerida > 0 order by PRD.Codigo_Producto"

            'strsql = "SELECT PRD.Codigo_Producto As 'Codigo', ORD.Cantidad_NoUtilizada AS 'Cantidad', PRD.Nombre_Producto As 'Materia Prima', UMD.Descripcion_Unidad As Medida, " & _
            '         "ORD.Costo_Unitario AS 'Costo Und.' , (ORD.Cantidad_NoUtilizada * ORD.Costo_Unitario) AS 'Costo Total', ORD.EsAdicional, ORD.Autorizado " & _
            '         "FROM Orden_Produccion_Detalle ORD INNER JOIN Productos PRD ON ORD.Cod_Producto_Detalle = PRD.Codigo_Producto INNER JOIN Unidades_Medida UMD ON PRD.Codigo_UnidMedida = UMD.Codigo_UnidadMed " & _
            '         "where ORD.Cantidad_NoUtilizada > 0 and ORD.CodigoSucursal = " & My.Settings.Sucursal & " and ORD.Codigo_Orden_Produc = '" & CodigoOrden & "' and ORD.Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "' order by PRD.Codigo_Producto "
            '       no tomaba en cuenta materiales adicionales, sustitutos, etc...    





            grdRemision.DataSource = SQL(strsql, "tblDatosRemision", My.Settings.SolIndustrialCNX).Tables(0)
            grvRemision.Columns("Codigo").Width = 75
            grvRemision.Columns("Cantidad").Width = 80
            grvRemision.Columns("Materia Prima").Width = 300
            grvRemision.Columns("Medida").Width = 50
            grvRemision.Columns("Costo Und.").Width = 100
            grvRemision.Columns("Costo Total").Width = 100
            grvRemision.Columns("EsAdicional").Visible = False
            grvRemision.Columns("Autorizado").Visible = False

            grvRemision.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvRemision.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:#.####}"
            grvRemision.OptionsView.ShowFooter = True

        Catch ex As Exception
        End Try

    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Public Sub nuevo()
        nTipoEdic = 1
        lblCodigoOrden.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        obtenerEstado(lblCodigoOrden.Text)
        PermitirBotonesEdicion(False)
        cargarSolicitante()
        cargarProductoFinal()
        ckIsActivo.Checked = True
        dtpRegistrado.Value = Now.Date
        txtCantidad.Text = 0
        txtElaborado.Text = 0
        txtColaboradores.Text = 0
        txtCantidadPro.Text = 1
        ActivarControles()
        ActivarPaneles()
        CargarFinal(lblCodigoOrden.Text)
    End Sub

    Function Cancelar()
        ActivarControles()
        ActivarPaneles()
        PermitirBotonesEdicion(True)
    End Function

    Sub GuardarMasterDoc()
        Dim Codigo As String = ""
        Dim Cantidad As String = ""
        Dim Costo As String = ""
        Dim strsql As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim Cant As Decimal = 0

        Try
            Cant = txtCantidadPro.Text

        Catch ex As Exception
            MsgBox("Ingrese cantidad a Producir")
            txtCantidadPro.Focus()
            Exit Sub
        End Try
        Try
            If CmbSolicitante.SelectedValue > 0 And Cant > 0 Then

                Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then

                    ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), lblCostoTotal.Text, 0, 0, dtpRegistrado.Value, CType(Nothing, DateTime?), 2023, Autorizado, ckIsActivo.Checked, 1, frmPrincipal.LblNombreUsuario.Text)

                    If ResumMaster = "OK" Then

                        strsql = "exec [dbo].[SP_GET_Receta_MateriaPrima] '" & cmbProducProducir.SelectedValue.ToString & "', '" & txtCantidadPro.Text & "'"

                        Dim Detalle As DataTable = SQL(strsql, "tblDetalle", My.Settings.SolIndustrialCNX).Tables(0)

                        For i As Integer = 0 To Detalle.Rows.Count - 1
                            Codigo = Detalle.Rows(i).Item("Codigo").ToString
                            Cantidad = Detalle.Rows(i).Item("Cantidad").ToString
                            Costo = Detalle.Rows(i).Item("Costo Und").ToString

                            ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, Cantidad, 0, Costo, False, False, 1, Nothing)

                        Next i
                    Else
                        MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                    End If

                    PermitirBotonesEdicion(True)
                    CargarDatos(lblCodigoOrden.Text)
                End If
            Else
                Dim mensaje As String = ("Debe seleccionar los siguientes datos:" + Chr(13) + Chr(13) + "- Solicitante" + Chr(13) + "- Producto a elaborar" + Chr(13) + "- Cantidad a Producir Mayor a 0")
                MsgBox(mensaje, MsgBoxStyle.Information, "Información")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Function aplicar() As Integer
        Dim strsql As String = ""
        Dim valor As Integer = 0
        '-----------------------
        '0- nulo
        '1- materiales pendiente a autorizar
        '2- no existencias (e recetas)
        '3- Materiales pendiente a remplazar

        '------------------------------------------MANTERIALES PENDIENTE A REMPLAZAR--
        strsql = "SELECT  Cod_Producto_Detalle FROM Orden_Produccion_Detalle PDT left outer join Inventario INV on INV.Codigo_Producto=PDT.Cod_Producto_Detalle and INV.Codigo_Bodega=3 where PDT.CodigoSucursal = " & My.Settings.Sucursal & " and PDT.Codigo_Orden_Produc ='" & lblCodigoOrden.Text & "' and INV.Codigo_Producto is null and PDT.Cantidad_Requerida > 0 and Sustituto is null"

        Dim tblDatos3 As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatos3.Rows.Count > 0 Then
            valor = 3
            Return valor
            Exit Function
        End If

        '------------------------------------------AUTORIZAR-----
        strsql = "SELECT top 1 Codigo_Orden_Produc FROM Orden_Produccion_Detalle where CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Orden_Produc ='" & lblCodigoOrden.Text & "' and Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "' and EsAdicional = 1 and Autorizado = 0"
        Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatos.Rows.Count > 0 Then
            valor = 1
            Return valor
            Exit Function
        End If

        '------------------------------------------EXISTENCIAS--
        strsql = "SELECT  Cod_Producto_Detalle FROM Orden_Produccion_Detalle PDT left outer join Inventario INV on INV.Codigo_Producto=PDT.Cod_Producto_Detalle and INV.Codigo_Bodega=3 where PDT.CodigoSucursal = " & My.Settings.Sucursal & " and PDT.Codigo_Orden_Produc ='" & lblCodigoOrden.Text & "' and PDT.Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "' and PDT.Cantidad_Requerida > 0 and INV.UndTotal_Existencia < PDT.Cantidad_Requerida"
        'strsql = "SELECT  Cod_Producto_Detalle FROM Orden_Produccion_Detalle PDT left outer join Inventario INV on INV.Codigo_Producto=PDT.Cod_Producto_Detalle and INV.Codigo_Bodega=3 where PDT.CodigoSucursal = " & My.Settings.Sucursal & " and PDT.Codigo_Orden_Produc ='" & lblCodigoOrden.Text & "' and PDT.Cod_Producto_Master = '" & cmbProducProducir.SelectedValue.ToString & "' and INV.Codigo_Producto is null and PDT.Cantidad_Requerida > 0"

        Dim tblDatos2 As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatos2.Rows.Count > 0 Then
            valor = 2
            Return valor
            Exit Function
        End If
       

        Return valor
    End Function

    Public Sub aplicarAutorizado()
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Codigo As String = ""
        Dim Cantidad As String = ""
        Dim Costo As String = ""
        Dim EsAdicional As Boolean = False
        Dim Sustituto As String = ""

        Try
            For i As Integer = 0 To grvDetalle.DataRowCount - 1
                Codigo = grvDetalle.GetRowCellValue(i, "Codigo")
                Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                Costo = grvDetalle.GetRowCellValue(i, "Costo Und")
                EsAdicional = grvDetalle.GetRowCellValue(i, "EsAdicional")
                Sustituto = grvDetalle.GetRowCellValue(i, "Sustituto")
                If EsAdicional = True Then
                    ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, Cantidad, 0, Costo, True, True, 2, Nothing)
                ElseIf Sustituto <> "" Then
                    ResumMDetalle = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, Codigo, Cantidad, 0, Costo, True, True, 2, Sustituto)
                End If
            Next i

            ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), lblCostoTotal.Text, 0, 0, dtpRegistrado.Value, CType(Nothing, DateTime?), 2024, Autorizado, ckIsActivo.Checked, 2, frmPrincipal.LblNombreUsuario.Text)

            If ResumMaster = "OK" Then
                CargarDatos(lblCodigoOrden.Text)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmOrdenProd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strsql As String = ""
        PermitirBotonesEdicion(True)

        lblCodigoOrden.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        obtenerEstado(lblCodigoOrden.Text)
        cargarSolicitante()
        cargarProductoFinal()
        ckIsActivo.Checked = True
        dtpRegistrado.Value = Now.Date
        txtCantidad.Text = 0
        txtCantidadPro.Text = 1
        txtElaborado.Text = 0
        txtColaboradores.Text = 0
        ActivarControles()
        ActivarPaneles()
        lblCif.Text = 0

        lblExist.BackColor = Color_Existencia
        lblSinAuto.BackColor = Color_Adicional
        lblAutorizado.BackColor = Color_Autorizado
        lblRemplazado.BackColor = Color_Remplazado

    End Sub

    Public Sub Modificar()
        If lblEstado.Text <> "SIN APLICAR" Then MsgBox("No puede modificarse una orden aplicada") : Exit Sub

        CmbSolicitante.Enabled = True
        cmbProducProducir.Enabled = True
        txtCantidadPro.Enabled = True
        dtpRegistrado.Enabled = True

        PermitirBotonesEdicion(False)

    End Sub

    Private Sub cmbProducProducir_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProducProducir.SelectedIndexChanged
        Try
            Dim Cant As Decimal = txtCantidadPro.Text

            If IdEstado = 0 And Cant > 0 Then
                CargarLista()
                cambiarTexto()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtCantidadPro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidadPro.KeyPress
        NumerosyDecimal(txtCantidadPro, e)
    End Sub

    Private Sub txtCantidadPro_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadPro.TextChanged
        grdDetalle.FocusedView.ClearDocument()
        Try
            Dim Cant As Decimal = txtCantidadPro.Text

            If IdEstado = 0 And Cant > 0 Then
                CargarLista()
            ElseIf Cant > 0 Then
                CargarLista()
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click
        Dim Codigo As String = ""
        Dim Cantidad As String = ""
        Dim Costo As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try
            If CmbSolicitante.SelectedValue > 0 Then
                Select Case aplicar()
                    Case 0

                        Dim result As Integer = MessageBox.Show("Desea aplicar la orden de producción?", "Confirmación de Registro", MessageBoxButtons.YesNo)
                        If result = DialogResult.Yes Then

                            ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), lblCostoTotal.Text, 0, 0, dtpRegistrado.Value, CType(Nothing, DateTime?), 3, Autorizado, ckIsActivo.Checked, 2, frmPrincipal.LblNombreUsuario.Text)

                            CodMaxSalida = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(3, 5), 8)
                            ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCTOS EN PROCESOS", lblCostoTotal.Text, dtpRegistrado.Value, 5, False, 1, 3)

                            CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(18, 4), 8)
                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCCION EN TRANSITO", lblCostoTotal.Text, dtpRegistrado.Value, 4, False, 1, 18)

                            If ResumMaster = "OK" Then
                                Dim tcantidad As Decimal = 0
                                Dim tcosto As Decimal = 0
                                For i = 0 To grvDetalle.DataRowCount - 1
                                    If grvDetalle.GetRowCellValue(i, "Cantidad") > 0 Then
                                        For j = i To grvDetalle.DataRowCount - 1
                                            If grvDetalle.GetRowCellValue(i, "Codigo") = grvDetalle.GetRowCellValue(j, "Codigo") Then
                                                Codigo = grvDetalle.GetRowCellValue(j, "Codigo")
                                                tcantidad += grvDetalle.GetRowCellValue(j, "Cantidad")
                                                tcosto += grvDetalle.GetRowCellValue(j, "Costo Und")
                                                i = j
                                            End If
                                        Next

                                        ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, My.Settings.Sucursal, Codigo, tcantidad, tcosto, "", 3, False, 1, 5)
                                        ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Codigo, tcantidad, tcosto, "", 18, False, 1, 4)

                                        tcantidad = 0
                                        tcosto = 0
                                    End If
                                Next
                                CargarDatos(lblCodigoOrden.Text)

                                Select Case MsgBox("Desea imprimir la ORDEN DE PRODUCCION?", MsgBoxStyle.YesNo, "")
                                    Case MsgBoxResult.Yes
                                        Print()
                                End Select
                            Else
                                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                            End If
                        End If
                    Case 1
                        Dim result As Integer = MessageBox.Show("Se trasladara la orden a Autorización. Desea Continuar?", "Confirmación de Traslado", MessageBoxButtons.YesNo)
                        If result = DialogResult.Yes Then
                            ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), lblCostoTotal.Text, 0, 0, dtpRegistrado.Value, CType(Nothing, DateTime?), 2025, Autorizado, ckIsActivo.Checked, 2, frmPrincipal.LblNombreUsuario.Text)

                            If ResumMaster = "OK" Then
                                MsgBox("La orden fue trasladada.", MsgBoxStyle.Information, "Información")
                                CargarDatos(lblCodigoOrden.Text)
                            End If
                        End If
                    Case 2
                        MsgBox("Existencias Insuficientes.", MsgBoxStyle.Exclamation, "Informacion")

                    Case 3
                        MsgBox("Existencias Insuficientes." & vbNewLine & "Remplazar materiales que no tengan existencias.", MsgBoxStyle.Exclamation, "Informacion")
                End Select
            Else
                Dim mensaje As String = ("Debe seleccionar los siguientes datos:" + Chr(13) + Chr(13) + "- Solicitante" + Chr(13) + "- Producto a elaborar")
                MsgBox(mensaje, MsgBoxStyle.Information, "Información")

            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub
    '-----PRINT-----------------
    Sub Print()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        'IO.Directory.Delete(sPath)
        Dim Fpath As String = sPath & "\Orden Produccion.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Orden_Produccion, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Orden Produccion.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Orden1")
        xlWorkSheet.Cells(3, 5) = lblCodigoOrden.Text 'codigo
        xlWorkSheet.Cells(4, 3) = CmbSolicitante.Text 'Solicitante
        xlWorkSheet.Cells(5, 3) = cmbProducProducir.Text 'producto
        xlWorkSheet.Cells(4, 7) = dtpRegistrado.Text 'Iniciado
        xlWorkSheet.Cells(4, 10) = "" 'finalidado NULO
        xlWorkSheet.Cells(5, 10) = lblEstado.Text 'estado
        Dim i As Integer = 1
        Dim f As Integer = 9
        Do While f <= 44
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 4) = ""
            xlWorkSheet.Cells(f, 5) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            f = f + 1
        Loop
        f = 9
        Do While i <= grvDetalle.RowCount
            xlWorkSheet.Cells(f, 1) = i
            xlWorkSheet.Cells(f, 2) = grvDetalle.GetRowCellValue(i - 1, "Codigo")
            xlWorkSheet.Cells(f, 3) = grvDetalle.GetRowCellValue(i - 1, "Existencia")
            xlWorkSheet.Cells(f, 4) = grvDetalle.GetRowCellValue(i - 1, "Cantidad")
            xlWorkSheet.Cells(f, 5) = grvDetalle.GetRowCellValue(i - 1, "Materia Prima")
            xlWorkSheet.Cells(f, 9) = grvDetalle.GetRowCellValue(i - 1, "Medida")
            xlWorkSheet.Cells(f, 10) = grvDetalle.GetRowCellValue(i - 1, "Costo Und")
            xlWorkSheet.Cells(f, 11) = grvDetalle.GetRowCellValue(i - 1, "Costo Total")

            xlWorkSheet.Cells(46, 2) = i
            xlWorkSheet.Cells(46, 4) = txtCantidadPro.Text
            xlWorkSheet.Cells(46, 7) = lblCostoUnd.Text
            xlWorkSheet.Cells(46, 9) = lblCostoTotal.Text
            xlWorkSheet.Cells(46, 11) = lblCif.Text
            'If i <= 44 Then
            '    xlWorkSheet.Cells(f + 1, 5) = "------ULTIMA LINA------"
            'End If
            i = i + 1
            f = f + 1
        Loop
        xlWorkBook.Application.DisplayAlerts = False
        Try
            xlWorkBook.Save()
        Catch ex As Exception
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End Try
        '-----------------PRINT EXCEL--------------------------------
        xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        '------------------------------------------------------------
        xlWorkBook.Close()
        xlApp.Quit()
        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub
    '-----END PRINT-----------------
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            Dim CodigoProducto As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
            Dim Cantidad As Integer = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad")
            Dim Costo As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Und")
            Dim EsAdicional As Boolean = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "EsAdicional")
            Dim Autorizado As Boolean = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Autorizado")

            If txtCantidad.Text > 0 And txtCantidad.Text <= Cantidad Then
                Dim CantUtilizada As Integer = Convert.ToInt32(txtCantidad.Text)
                Resum = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, CodigoProducto, Cantidad, CantUtilizada, Costo, EsAdicional, Autorizado, 1, Nothing)
                If Resum = "OK" Then
                    txtCantidad.Text = 0
                    CargarRemision(lblCodigoOrden.Text)
                    CargarFinal(lblCodigoOrden.Text)
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            Else
                MsgBox(("La cantidad debe ser mayor a 0 y menor o igual que " & Cantidad.ToString & "."), MsgBoxStyle.Information, "SIGCA")
                txtCantidad.Text = 0
                txtCantidad.Focus()
            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            Dim CodigoProducto As String = grvRemision.GetRowCellValue(grvRemision.GetSelectedRows(0), "Codigo").ToString()
            Dim Cantidad As Integer = grvRemision.GetRowCellValue(grvRemision.GetSelectedRows(0), "Cantidad")
            Dim Costo As Double = grvRemision.GetRowCellValue(grvRemision.GetSelectedRows(0), "Costo Und.")
            Dim EsAdicional As Boolean = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "EsAdicional")
            Dim Autorizado As Boolean = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Autorizado")

            If txtCantidad.Text > 0 Then
                Dim CantUtilizada As Integer = Convert.ToInt32(txtCantidad.Text)

                If Cantidad > txtCantidad.Text Then
                    Resum = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, CodigoProducto, 0, (Cantidad - CantUtilizada), Costo, EsAdicional, Autorizado, 2, Nothing)
                Else
                    Resum = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, CodigoProducto, 0, 0, Costo, EsAdicional, Autorizado, 2, Nothing)
                End If

                If Resum = "OK" Then
                    txtCantidad.Text = 0
                    CargarRemision(lblCodigoOrden.Text)
                    CargarFinal(lblCodigoOrden.Text)
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            Else
                MsgBox("La cantidad debe ser mayor a 0.", MsgBoxStyle.Information, "Información")
                txtCantidad.Text = 0
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub grdDetalle_DoubleClick(sender As Object, e As EventArgs) Handles grdDetalle.DoubleClick
        txtCantidad.Focus()
        txtCantidad.SelectAll()
    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        NumerosyDecimal(txtCantidad, e)
    End Sub

    Private Sub btnLiquidar_Click(sender As Object, e As EventArgs) Handles btnLiquidar.Click
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        If IdEstado = 1 Then
            Dim result As Integer = MessageBox.Show("Desea liquidar la orden de producción?", "Confirmación de Liquidación", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Try
                    Dim Codigo As String = ""
                    Dim Cantidad As String = ""
                    Dim Costo As String = ""
                    Dim CostoTotal As Double = 0

                    If Integer.Parse(txtElaborado.Text) >= 0 And Integer.Parse(txtElaborado.Text) <= Integer.Parse(txtCantidadPro.Text) Then

                        ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), lblCostoTotal.Text, Convert.ToInt32(txtElaborado.Text), (lblCostoTotal.Text - CostoTotal), dtpRegistrado.Value, CType(Nothing, DateTime?), 3, Autorizado, ckIsActivo.Checked, 2, frmPrincipal.LblNombreUsuario.Text)
                        CargarFinal(lblCodigoOrden.Text)

                        If grvRemision.RowCount > 0 Then

                            For i As Integer = 0 To grvRemision.DataRowCount - 1
                                CostoTotal = CostoTotal + grvRemision.GetRowCellValue(i, "Costo Total")
                            Next

                            CodMaxSalida = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(18, 5), 8)
                            ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCCION EN TRANSITO", CostoTotal, dtpRegistrado.Value, 5, False, 1, 18)

                            CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(3, 4), 8)
                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS", CostoTotal, dtpRegistrado.Value, 4, False, 1, 3)

                            If ResumMaster = "OK" Then

                                For i As Integer = 0 To grvRemision.DataRowCount - 1
                                    Codigo = grvRemision.GetRowCellValue(i, "Codigo")
                                    Cantidad = grvRemision.GetRowCellValue(i, "Cantidad")
                                    Costo = grvRemision.GetRowCellValue(i, "Costo Und.")

                                    ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", 18, False, 1, 5)
                                    ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", 3, False, 1, 4)

                                Next i

                            End If
                        End If

                        If grvFinal.RowCount > 0 Then
                            Codigo = grvFinal.GetRowCellValue(0, "Cod. Producto")
                            Cantidad = grvFinal.GetRowCellValue(0, "Elaborado")
                            Costo = grvFinal.GetRowCellValue(0, "C. Real")

                            If Cantidad > 0 Then
                                CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(3, 4), 8)
                                ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS", Costo, Date.Now, 4, False, 1, 3)

                                ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Codigo, Cantidad, (Costo / Cantidad), "", 3, False, 1, 4)

                            End If
                        End If

                        If ResumMaster = "OK" Then
                            MsgBox("Liquidación de Orden Realizada.", MsgBoxStyle.Information, "Operacion Finalizada")
                            CargarDatos(lblCodigoOrden.Text)
                        End If

                    Else
                        MsgBox("La cantidad elaborada debe ser mayor a 0 y menor o igual que " + txtCantidadPro.Text, MsgBoxStyle.Information, "Información")
                        txtElaborado.Focus()
                        txtElaborado.SelectAll()
                    End If

                Catch ex As Exception

                End Try
            End If
        Else
            MsgBox("Solo se pueden liquidar ordenes en proceso.", MsgBoxStyle.Information, "Información")
        End If
    End Sub

    Private Sub txtElaborado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtElaborado.KeyPress
        NumerosyDecimal(txtElaborado, e)
    End Sub

    Private Sub btnCerrarOrden_Click(sender As Object, e As EventArgs) Handles btnCerrarOrden.Click
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim Codigo As String = ""
        Dim Cantidad As String = ""
        Dim Costo As String = ""
        Dim CostoTotal As Double = 0
        Dim textMensaje As String = ""
        Dim textOk As String = ""

        If IdEstado = 3 Then
            Select Case CodTipo
                Case 1
                    textMensaje = "Desea cerrar la orden de producción?"
                    textOk = "Cierre de Orden Realizada."
                Case 2
                    textMensaje = "Desea trasladar la orden de producción?"
                    textOk = "Traslado de Orden Realizada."
            End Select

            Dim result As Integer = MessageBox.Show(textMensaje, "Confirmación de Acción", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Try
                    If grvFinal.RowCount > 0 Then

                        If grvRemision.RowCount > 0 Then

                            For i As Integer = 0 To grvRemision.DataRowCount - 1
                                CostoTotal = CostoTotal + grvRemision.GetRowCellValue(i, "Costo Total")
                            Next

                            CodMaxSalida = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(18, 5), 8)
                            ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCCION EN TRANSITO", CostoTotal, dtpRegistrado.Value, 5, False, 1, 18)

                            CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(3, 4), 8)
                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS", CostoTotal, dtpRegistrado.Value, 4, False, 1, 3)

                            If ResumEntrada = "OK" Then

                                For i As Integer = 0 To grvRemision.DataRowCount - 1
                                    Codigo = grvRemision.GetRowCellValue(i, "Codigo")
                                    Cantidad = grvRemision.GetRowCellValue(i, "Cantidad")
                                    Costo = grvRemision.GetRowCellValue(i, "Costo Und.")

                                    ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", 18, False, 1, 5)
                                    ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", 3, False, 1, 4)

                                Next i

                                Dim strsql As String = ("Select Cod_Producto_Detalle 'Codigo', (Cantidad_Requerida - Cantidad_NoUtilizada) 'Cantidad', Costo_Unitario 'Costo Und.' FROM Orden_Produccion_Detalle WHERE (Codigo_Orden_Produc = '" + lblCodigoOrden.Text + "') AND (CodigoSucursal = " + My.Settings.Sucursal.ToString + ") and (Cantidad_Requerida - Cantidad_NoUtilizada) > 0")
                                Dim tblUtilizado As DataTable = SQL(strsql, "tblUtilizado", My.Settings.SolIndustrialCNX).Tables(0)

                                CodMaxSalida = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(18, 5), 8)
                                ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCCION EN TRANSITO", 0, dtpRegistrado.Value, 5, False, 1, 18)

                                For i As Integer = 0 To tblUtilizado.Rows.Count - 1
                                    Codigo = tblUtilizado.Rows(i).Item("Codigo").ToString
                                    Cantidad = tblUtilizado.Rows(i).Item("Cantidad").ToString
                                    Costo = tblUtilizado.Rows(i).Item("Costo Und.").ToString

                                    ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", 18, False, 1, 5)

                                Next

                            End If

                        Else
                            CodMaxSalida = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(18, 5), 8)
                            ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCCION EN TRANSITO", lblCostoTotal.Text, dtpRegistrado.Value, 5, False, 1, 18)

                            If ResumSalida = "OK" Then

                                For i As Integer = 0 To grvDetalle.DataRowCount - 1
                                    Codigo = grvDetalle.GetRowCellValue(i, "Codigo")
                                    Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                                    Costo = grvDetalle.GetRowCellValue(i, "Costo Und")

                                    If Costo > 0 And Cantidad > 0 Then
                                        ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", 18, False, 1, 5)
                                    End If
                                Next i

                            End If

                        End If

                        Select Case CodTipo
                            Case 1
                                For i As Integer = 0 To grvFinal.DataRowCount - 1
                                    Codigo = grvFinal.GetRowCellValue(i, "Cod. Producto")
                                    Cantidad = grvFinal.GetRowCellValue(i, "Elaborado")
                                    Costo = grvFinal.GetRowCellValue(i, "C. Real")

                                    CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(3, 4), 8)
                                    ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, lblCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS", Costo, dtpCerrado.Value, 4, False, 1, 3)
                                    ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Codigo, Cantidad, (Costo / Cantidad), "", 3, False, 1, 4)
                                Next i
                                ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), grvFinal.GetRowCellValue(0, "C. Estimado"), Convert.ToInt32(grvFinal.GetRowCellValue(0, "Elaborado")), grvFinal.GetRowCellValue(0, "C. Real"), dtpRegistrado.Value, dtpCerrado.Value, 19, Autorizado, ckIsActivo.Checked, 2, frmPrincipal.LblNombreUsuario.Text)
                            Case 2
                                ResumMaster = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, lblCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), grvFinal.GetRowCellValue(0, "C. Estimado"), Convert.ToInt32(grvFinal.GetRowCellValue(0, "Elaborado")), grvFinal.GetRowCellValue(0, "C. Real"), dtpRegistrado.Value, Nothing, 2029, Autorizado, ckIsActivo.Checked, 2, frmPrincipal.LblNombreUsuario.Text)
                        End Select

                        If ResumMaster = "OK" Then
                            MsgBox(textOk, MsgBoxStyle.Information, "Acción Finalizada")
                            CargarDatos(lblCodigoOrden.Text)
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        Else
            MsgBox("Solo se pueden procesar ordenes liquidadas.", MsgBoxStyle.Information, "Información")
        End If
    End Sub

    Private Sub txtElaborado_TextChanged(sender As Object, e As EventArgs) Handles txtElaborado.TextChanged
        CargarListaNoUtilizada()
    End Sub

    Private Sub btnAgregarMat_Click(sender As Object, e As EventArgs) Handles btnAgregarMat.Click
        frmAdicional.ShowDialog()
    End Sub

    Private Sub frmOrdenProd_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        frmCatalogo.Close()
        NumCatalogo = 23
    End Sub

    Private Sub grvDetalle_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grvDetalle.RowStyle
        Dim view As ColumnView = DirectCast(sender, ColumnView)

        If view.IsValidRowHandle(e.RowHandle) Then

            Dim Adicional As Boolean = view.GetRowCellValue(e.RowHandle, "EsAdicional")
            Dim Autorizado As Boolean = view.GetRowCellValue(e.RowHandle, "Autorizado")
            Dim Existencia As Double = view.GetRowCellValue(e.RowHandle, "Existencia")
            Dim Cantidad As Double = view.GetRowCellValue(e.RowHandle, "Cantidad")

            If Cantidad > Existencia Then
                e.Appearance.BackColor = Color_Existencia
            End If

            If Adicional = True And Autorizado = False Then
                e.Appearance.BackColor = Color_Adicional
            End If

            If Adicional = True And Autorizado = True Then
                e.Appearance.BackColor = Color_Autorizado
            End If

            If Cantidad = 0 Then
                e.Appearance.BackColor = Color_Remplazado
            End If

        End If

    End Sub

    Private Sub btnAutorizar_Click(sender As Object, e As EventArgs) Handles btnAutorizar.Click
        frmAutorizar.ShowDialog()
        CargarDatos(lblCodigoOrden.Text)
    End Sub

    Private Sub grvRemision_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grvRemision.RowStyle
        Dim view As ColumnView = DirectCast(sender, ColumnView)

        If view.IsValidRowHandle(e.RowHandle) Then

            Dim Adicional As Boolean = view.GetRowCellValue(e.RowHandle, "EsAdicional")
            Dim Autorizado As Boolean = view.GetRowCellValue(e.RowHandle, "Autorizado")

            If Adicional = True And Autorizado = False Then
                e.Appearance.BackColor = Color_Adicional
            End If

            If Adicional = True And Autorizado = True Then
                e.Appearance.BackColor = Color_Autorizado
            End If

        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnQuitarMat.Click
        Dim Resum As String = ""
        Dim Codigo As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            Dim CodigoProducto As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
            Dim Cantidad As Integer = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad")
            Dim Costo As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Und")
            Dim EsAdicional As Boolean = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "EsAdicional")
            Dim Autorizado As Boolean = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Autorizado")

            If EsAdicional = True And Autorizado = False Then

                Dim result As Integer = MessageBox.Show("Desea remover el material adicional?", "Confirmación de Acción", MessageBoxButtons.YesNo)

                If result = DialogResult.Yes Then
                    Resum = Produccion.EditarOrdenDetalle(My.Settings.Sucursal, lblCodigoOrden.Text, cmbProducProducir.SelectedValue.ToString, CodigoProducto, Cantidad, 0, Costo, EsAdicional, Autorizado, 3, Nothing)

                    If Resum = "OK" Then
                        MsgBox("Material removido correctamente.", MsgBoxStyle.Information, "Información")
                        CargarDetalle()
                    End If
                End If

            ElseIf Autorizado = True Then

                MsgBox("No se pueden eliminar materiales cuando ya fue autorizada la orden.", MsgBoxStyle.Critical, "Información")

            Else

                MsgBox("Solo se pueden quitar los materiales adicionales.", MsgBoxStyle.Critical, "Información")

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdDetalle_Click(sender As Object, e As EventArgs) Handles grdDetalle.Click
        btnAgregar.Enabled = True
        btnQuitar.Enabled = False
    End Sub

    Private Sub grdRemision_Click(sender As Object, e As EventArgs) Handles grdRemision.Click
        btnAgregar.Enabled = False
        btnQuitar.Enabled = True
    End Sub

    Private Sub frmOrdenProd_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub

    Private Sub grdDetalle_Leave(sender As Object, e As EventArgs) Handles grdDetalle.Leave
        grvDetalle.OptionsSelection.EnableAppearanceFocusedRow = False
    End Sub

    Private Sub grdDetalle_Enter(sender As Object, e As EventArgs) Handles grdDetalle.Enter
        grvDetalle.OptionsSelection.EnableAppearanceFocusedRow = True
    End Sub

    Private Sub txtColaboradores_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles txtColaboradores.MouseDoubleClick

        FrmProd_Colaboradores.Carga(lblCodigoOrden.Text, My.Settings.Sucursal)
    End Sub

End Class
