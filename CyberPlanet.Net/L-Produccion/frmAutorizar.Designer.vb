﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutorizar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutorizar))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dgvSustituto = New System.Windows.Forms.DataGridView()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblExistencia = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblAdicional = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbDescripcion2 = New System.Windows.Forms.Label()
        Me.lbDescripcion1 = New System.Windows.Forms.Label()
        Me.lbAdicional = New System.Windows.Forms.Label()
        Me.lbExistencia = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvSustituto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.lblExistencia)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.lblAdicional)
        Me.Panel1.Controls.Add(Me.dgvSustituto)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(385, 294)
        Me.Panel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdCancelar)
        Me.Panel2.Controls.Add(Me.cmdAceptar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 245)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(385, 49)
        Me.Panel2.TabIndex = 71
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(295, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(211, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Autorizar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPassword.Location = New System.Drawing.Point(262, 15)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(111, 20)
        Me.txtPassword.TabIndex = 70
        Me.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(192, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 69
        Me.Label4.Text = "Contraseña:"
        '
        'dgvSustituto
        '
        Me.dgvSustituto.AllowUserToAddRows = False
        Me.dgvSustituto.AllowUserToDeleteRows = False
        Me.dgvSustituto.AllowUserToResizeColumns = False
        Me.dgvSustituto.AllowUserToResizeRows = False
        Me.dgvSustituto.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSustituto.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvSustituto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSustituto.Location = New System.Drawing.Point(12, 49)
        Me.dgvSustituto.MultiSelect = False
        Me.dgvSustituto.Name = "dgvSustituto"
        Me.dgvSustituto.ReadOnly = True
        Me.dgvSustituto.RowHeadersVisible = False
        Me.dgvSustituto.RowHeadersWidth = 4
        Me.dgvSustituto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSustituto.Size = New System.Drawing.Size(361, 126)
        Me.dgvSustituto.TabIndex = 72
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(97, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(67, 13)
        Me.Label18.TabIndex = 94
        Me.Label18.Text = "A Remplazar"
        '
        'lblExistencia
        '
        Me.lblExistencia.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblExistencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblExistencia.Location = New System.Drawing.Point(82, 18)
        Me.lblExistencia.Name = "lblExistencia"
        Me.lblExistencia.Size = New System.Drawing.Size(14, 14)
        Me.lblExistencia.TabIndex = 93
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(26, 18)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(50, 13)
        Me.Label17.TabIndex = 92
        Me.Label17.Text = "Adicional"
        '
        'lblAdicional
        '
        Me.lblAdicional.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblAdicional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAdicional.Location = New System.Drawing.Point(12, 18)
        Me.lblAdicional.Name = "lblAdicional"
        Me.lblAdicional.Size = New System.Drawing.Size(14, 14)
        Me.lblAdicional.TabIndex = 91
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.lbDescripcion2)
        Me.GroupBox1.Controls.Add(Me.lbDescripcion1)
        Me.GroupBox1.Controls.Add(Me.lbAdicional)
        Me.GroupBox1.Controls.Add(Me.lbExistencia)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 181)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(361, 58)
        Me.GroupBox1.TabIndex = 95
        Me.GroupBox1.TabStop = False
        '
        'lbDescripcion2
        '
        Me.lbDescripcion2.AutoSize = True
        Me.lbDescripcion2.Location = New System.Drawing.Point(32, 36)
        Me.lbDescripcion2.Name = "lbDescripcion2"
        Me.lbDescripcion2.Size = New System.Drawing.Size(0, 13)
        Me.lbDescripcion2.TabIndex = 99
        '
        'lbDescripcion1
        '
        Me.lbDescripcion1.AutoSize = True
        Me.lbDescripcion1.Location = New System.Drawing.Point(31, 16)
        Me.lbDescripcion1.Name = "lbDescripcion1"
        Me.lbDescripcion1.Size = New System.Drawing.Size(0, 13)
        Me.lbDescripcion1.TabIndex = 97
        '
        'lbAdicional
        '
        Me.lbAdicional.BackColor = System.Drawing.SystemColors.Control
        Me.lbAdicional.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbAdicional.Location = New System.Drawing.Point(13, 13)
        Me.lbAdicional.Name = "lbAdicional"
        Me.lbAdicional.Size = New System.Drawing.Size(27, 20)
        Me.lbAdicional.TabIndex = 96
        Me.lbAdicional.Text = "*"
        '
        'lbExistencia
        '
        Me.lbExistencia.BackColor = System.Drawing.SystemColors.Control
        Me.lbExistencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbExistencia.Location = New System.Drawing.Point(13, 31)
        Me.lbExistencia.Name = "lbExistencia"
        Me.lbExistencia.Size = New System.Drawing.Size(28, 21)
        Me.lbExistencia.TabIndex = 100
        Me.lbExistencia.Text = "*"
        '
        'frmAutorizar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(385, 294)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAutorizar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Autorizar Cambios"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvSustituto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents dgvSustituto As System.Windows.Forms.DataGridView
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblExistencia As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblAdicional As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lbExistencia As System.Windows.Forms.Label
    Friend WithEvents lbDescripcion2 As System.Windows.Forms.Label
    Friend WithEvents lbAdicional As System.Windows.Forms.Label
    Friend WithEvents lbDescripcion1 As System.Windows.Forms.Label
End Class
