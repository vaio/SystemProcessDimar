﻿Imports System.Data.SqlClient
Imports DevExpress.Utils

Public Class frmReparaciones

    Private Articulos As New frmBusqueda_Prod("cod_bodega", 2, SqlDbType.Int)
    Private Clientes As New fmBusqueda_Client
    Private Validar_Campos As New Validar

    Private AgregarProducto As New frmProductoAdicional
    Private OrdenServDetalle As New Dictionary(Of Llaves, Material_garantia)

    Private Structure Llaves

        Public _cod_producto As String
        Public _adicional As Boolean
        Public _autorizado As Boolean
        Public _activo As Boolean

    End Structure

    'Dim TipoOrden As Integer = 0
    'Dim Prioridad As Integer = 0

    'Public Sub Cerrar()
    '    '    Me.Dispose()
    '    '  Close()
    'End Sub

    'Function cargarPrefijo(ByVal Codigo As String) As String
    '    Dim valor As String = ""
    '    Dim strsql As String = "SELECT Prefijo FROM Catalogo.Tipos_Servicios Serv where Serv.Activo = 1 and Codigo_Servicio = '" & Codigo & "'"

    '    Dim Table As DataTable = SQL(strsql, "tblServicio", My.Settings.SolIndustrialCNX).Tables(0)
    '    If Table.Rows.Count > 0 Then
    '        valor = Table.Rows(0).Item("Prefijo").ToString()
    '    End If

    '    Return valor
    'End Function

    'Public Sub cargarEstado(ByVal Codigo As String)
    '    Dim strsql As String = " '" & Codigo & "'"

    ' Dim Table As DataTable = SQL(strsql, "tblEstado", My.Settings.SolIndustrialCNX).Tables(0)

    '    If Table.Rows.Count > 0 Then
    '        lblEstado.Text = Table.Rows(0).Item("Prefijo").ToString()
    '    End If

    'End Sub

    'Public Sub cargarTipoOrden()
    '    CmbTipoOrden.DisplayMember = "Nombre"
    '    CmbTipoOrden.ValueMember = "Codigo_Servicio"
    '    CmbTipoOrden.DataSource = SQL("SELECT Codigo_Servicio, Nombre FROM Catalogo.Tipos_Servicios Serv where Serv.Activo = 1", "tblServicio", My.Settings.SolIndustrialCNX).Tables(0)
    'End Sub

    'Public Sub cargarPrioridad()
    '    cmbPrioridad.DataSource = SQL("SELECT Id_Prioridad, Nombre FROM Catalogo.Prioridad Prio WHERE Prio.Activo = 1 Order By Orden", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
    '    cmbPrioridad.DisplayMember = "Nombre"
    '    cmbPrioridad.ValueMember = "Id_Prioridad"
    'End Sub

    'Public Sub cargarTipoCliente()
    '    cmbTipoCliente.DataSource = SQL("SELECT Id_Tipo, Descripcion FROM Catalogo.Tipo_Cliente Where Activo = 1", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
    '    cmbTipoCliente.DisplayMember = "Descripcion"
    '    cmbTipoCliente.ValueMember = "Id_Tipo"
    'End Sub

    'Public Sub cargarTipoArticulo()
    '    cmbArticulo.DataSource = SQL("SELECT Id_Procedencia, Descripcion FROM Catalogo.Procedencia_Articulo Where Activo = 1", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
    '    cmbArticulo.DisplayMember = "Descripcion"
    '    cmbArticulo.ValueMember = "Id_Procedencia"
    'End Sub

    'Public Sub cargarCondicion()
    '    cmbCondicion.DataSource = SQL("SELECT Id_Condicion, Descripcion FROM Catalogo.Condicion_Articulo Where Activo = 1", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
    '    cmbCondicion.DisplayMember = "Descripcion"
    '    cmbCondicion.ValueMember = "Id_Condicion"
    'End Sub

    'Private Sub frmReparaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    '    cargarTipoOrden()
    '    cargarPrioridad()
    '    cargarTipoCliente()
    '    cargarTipoArticulo()
    '    cargarCondicion()

    '    lblEstado.Text = "PENDIENTE"

    '    dtRecibido.Value = Now.Date
    '    dtEntrega.Value = Now.Date
    'End Sub

    'Private Sub CmbTipoOrden_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CmbTipoOrden.SelectedIndexChanged
    '    valTemp = CmbTipoOrden.SelectedValue
    '    lblCodigo.Text = (cargarPrefijo(valTemp) & Microsoft.VisualBasic.Right("0000000" & RegistroMaximo(), 7))
    'End Sub

    Private Sub frmReparaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        set_setting()

        set_data()

        AddHandler btn_add_client.Click, AddressOf mostrar_clientes
        AddHandler btn_mostrar_articulos.Click, AddressOf mostrar_articulos
        AddHandler btn_nuevo.Click, AddressOf nuevo
        AddHandler btn_guardar.Click, AddressOf guardar
        AddHandler btnAgregar.Click, AddressOf agregar_producto
        AddHandler btnEliminar.Click, AddressOf eliminar
        AddHandler btnModificar.Click, AddressOf modificar

        Validar_Campos.agregar_control((New List(Of Control)({txt_cantidad_articulo, txt_no_factura_articulo}))).SoloEnteros()

    End Sub

    Private Sub set_data()

        CmbTipoOrden.DataSource = data_fill_up_comb.get_tipos_de_ordenes()

        cmbPrioridad.DataSource = data_fill_up_comb.get_tipos_de_prioridades()

        cmbTipoCliente.DataSource = data_fill_up_comb.get_tipos_de_clientes()

        cmbCondicion_articulo.DataSource = data_fill_up_comb.get_tipos_condiciones_del_articulo()

        cmb_ord_de_serv_Tipo_de_estado.DataSource = data_orden_de_servicio.get_tipos_de_estados()

        CmbTipoOrden.DisplayMember = "Servicio"
        CmbTipoOrden.ValueMember = "Codigo"

        cmbPrioridad.DisplayMember = "Prioridad"
        cmbPrioridad.ValueMember = "Codigo"

        cmbTipoCliente.DisplayMember = "Tipo"
        cmbTipoCliente.ValueMember = "Codigo"

        cmbCondicion_articulo.DisplayMember = "Condicion"
        cmbCondicion_articulo.ValueMember = "Codigo"

        cmb_ord_de_serv_Tipo_de_estado.DisplayMember = "nombre"
        cmb_ord_de_serv_Tipo_de_estado.ValueMember = "codigo"


    End Sub
    Private Sub set_setting()

        AddHandler MyBase.Resize, AddressOf responsive

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None

    End Sub

    Private Sub responsive()

        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None

    End Sub

    Private Sub mostrar_clientes()

        Clientes.StartPosition = FormStartPosition.CenterScreen
        Clientes.ShowDialog()

        set_data_to_cliente(Clientes.Curr_Cliente)

    End Sub

    Private Sub set_data_to_cliente(ByRef Cliente As Cliente)

        If Cliente.get_codigo = String.Empty Then Exit Sub

        txt_Cod_cliente.Text = Cliente.get_codigo
        txt_Nombres.Text = Cliente.get_nombres
        txt_Apellidos.Text = Cliente.get_apellidos
        txt_Telefono_1.Text = Cliente.get_Telefono_1
        txt_Telefono_2.Text = Cliente.get_Telefono_2
        txt_Correo.Text = Cliente.get_Correo
        cmbTipoCliente.SelectedValue = Cliente.get_Id_tipo

    End Sub

    Private Sub mostrar_articulos()

        Articulos.mostrar(3) ' 3 = columna; sera ocultada

        txt_cod_articulo.Text = Articulos.get_info.get_codigo
        txt_descripcion_articulo.Text = Articulos.get_info.get_descripcion

    End Sub

    Public Sub nuevo()

        limpiar()

        txt_ord_serv_cod.Text = data_orden_de_servicio.get_codigo

    End Sub

    Public Sub guardar()

        Dim Repuesta = MsgBox("Guardar?", MsgBoxStyle.OkCancel) : If Repuesta = MsgBoxResult.Cancel Then Exit Sub

        Dim Resultado = data_orden_de_servicio.insert_master(
            Funciones.Param("@cod_sucursal_", 1, SqlDbType.Int),
            Funciones.Param("@cod_ord_serv_", txt_ord_serv_cod.Text, SqlDbType.NVarChar),
            Funciones.Param("@cod_client_", txt_Cod_cliente.Text, SqlDbType.NVarChar),
            Funciones.Param("@cod_product_", txt_cod_articulo.Text, SqlDbType.NVarChar),
            Funciones.Param("@orden_serv_tipo_", CmbTipoOrden.SelectedValue, SqlDbType.Int),
            Funciones.Param("@ord_serv_prioridad_", cmbPrioridad.SelectedValue, SqlDbType.Int),
            Funciones.Param("@ord_serv_estado_", cmb_ord_de_serv_Tipo_de_estado.SelectedValue, SqlDbType.Int),
            Funciones.Param("@ord_serv_descripcion_", richDescripcion.Text, SqlDbType.NVarChar),
            Funciones.Param("@articulo_cantidad_", ChangeToDouble(txt_cantidad_articulo.Text), SqlDbType.Int),
            Funciones.Param("@articulo_codigo_", txt_no_factura_articulo.Text, SqlDbType.NVarChar),
            Funciones.Param("@articulo_condicion_", cmbCondicion_articulo.SelectedValue, SqlDbType.Int),
            Funciones.Param("@tipo_de_accion_", 1, SqlDbType.Int))

        If Resultado = False Then MsgBox("Listo agregado")

    End Sub

    Private Sub limpiar()

        Funciones.Limpiar(Me.GroupBox1.Controls)
        Funciones.Limpiar(Me.GroupBox3.Controls)
        Funciones.Limpiar(Me.GroupBox4.Controls)


        '----------------- Combobox ----------------------------

        cmb_ord_de_serv_Tipo_de_estado.SelectedValue = 1
        cmbCondicion_articulo.SelectedValue = 1
        cmbPrioridad.SelectedValue = 1
        CmbTipoOrden.SelectedValue = "000001"
        cmbTipoCliente.SelectedValue = 1

        '-------------------------------------------------------

    End Sub
    Private Sub agregar_producto()

        AgregarProducto = New frmProductoAdicional() With {.StartPosition = FormStartPosition.CenterParent}
        AgregarProducto.ShowDialog()

        With AgregarProducto.my_material

            If Not .get_codigo = String.Empty Then

                Dim key As New Llaves With {._adicional = AgregarProducto.my_material.get_adicional, ._autorizado = AgregarProducto.my_material.get_autorizado, ._cod_producto = AgregarProducto.my_material.get_codigo, ._activo = AgregarProducto.my_material.get_activo}

                If OrdenServDetalle.ContainsKey(key) = True Then

                    OrdenServDetalle.Item(key).get_cantidad += .get_cantidad

                    OrdenServDetalle.Item(key).get_costo_total += (OrdenServDetalle.Item(key).get_cantidad * .get_costo)

                Else

                    OrdenServDetalle.Add(key, AgregarProducto.my_material)

                End If

            End If

        End With

        If OrdenServDetalle.Count = 0 Then Tabla.DataSource = Nothing Else Tabla.DataSource = Nothing : Tabla.DataSource = OrdenServDetalle.Values : ajuste_tabla()

    End Sub

    Private Sub modificar()

        If OrdenServDetalle.Count = 0 Then Exit Sub

        AgregarProducto = New frmProductoAdicional() With {.StartPosition = FormStartPosition.CenterParent}

        AgregarProducto.btn_materiales.Enabled = False

        AgregarProducto.get_material_modificar(GridView1.GetFocusedRow())

        AgregarProducto.ShowDialog()

        With AgregarProducto.my_material

            If .get_codigo = String.Empty Then Exit Sub

            Dim key As New Llaves With {._adicional = AgregarProducto.my_material.get_adicional, ._autorizado = AgregarProducto.my_material.get_autorizado, ._cod_producto = AgregarProducto.my_material.get_codigo, ._activo = AgregarProducto.my_material.get_activo}

            If Not OrdenServDetalle.ContainsKey(key) = True Then Exit Sub

            OrdenServDetalle.Item(key).get_cantidad = .get_cantidad

            OrdenServDetalle.Item(key).get_costo_total = FormatNumber((OrdenServDetalle.Item(key).get_cantidad * .get_costo), 2)

        End With

        If OrdenServDetalle.Count = 0 Then Tabla.DataSource = Nothing Else Tabla.DataSource = Nothing : Tabla.DataSource = OrdenServDetalle.Values : ajuste_tabla()

    End Sub

    Private Sub eliminar()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        With GridView1

            Dim material As New Material_garantia

            material = .GetFocusedRow()

            Dim key As New Llaves With {._cod_producto = material.get_codigo, ._adicional = material.get_adicional, ._autorizado = material.get_autorizado, ._activo = material.get_activo}

            OrdenServDetalle.Remove(key)

            If OrdenServDetalle.Count = 0 Then
                Tabla.DataSource = Nothing
                GridView1.Columns.Clear()
            Else
                Tabla.DataSource = Nothing
                Tabla.DataSource = OrdenServDetalle.Values
                ajuste_tabla()
            End If


        End With


    End Sub

    Private Sub ajuste_tabla()

        '   If GridView1.RowCount = 0 Then Exit Sub

        '    If Not GridView1.Columns.Count = 9 Then Exit Sub

        With GridView1

            For i = 0 To .Columns.Count - 1

                .Columns(i).Visible = False

            Next

            .Columns(5).VisibleIndex = 0
            .Columns(6).VisibleIndex = 1
            .Columns(9).VisibleIndex = 2
            .Columns(8).VisibleIndex = 3
            .Columns(0).VisibleIndex = 4
            .Columns(7).VisibleIndex = 5
            .Columns(1).VisibleIndex = 6
            .Columns(2).VisibleIndex = 7
            .Columns(3).VisibleIndex = 8
            .Columns(4).VisibleIndex = 9

            .Columns(7).DisplayFormat.FormatType = FormatType.Numeric
            .Columns(7).DisplayFormat.FormatString = "c2"
            .Columns(1).DisplayFormat.FormatType = FormatType.Numeric
            .Columns(1).DisplayFormat.FormatString = "c2"

            .BestFitColumns()

        End With

        'codigo = 5
        'materiaprima = 6
        'medida = 9
        'existencia = 8
        'cantidad = 0
        'costounitario = 7
        'costototal = 1
        'adicinal = 2
        'autorizado = 3
        'activo = 4

    End Sub

End Class