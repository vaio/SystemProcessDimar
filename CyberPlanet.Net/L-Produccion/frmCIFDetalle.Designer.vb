﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCIFDetalle
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCIFDetalle))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtNota = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFinal = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtInicial = New System.Windows.Forms.TextBox()
        Me.lblCodProdServ = New System.Windows.Forms.Label()
        Me.cmdAddProv = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbTipoCif = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtNota)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtFinal)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtInicial)
        Me.Panel1.Controls.Add(Me.lblCodProdServ)
        Me.Panel1.Controls.Add(Me.cmdAddProv)
        Me.Panel1.Controls.Add(Me.cmbTipoCif)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(437, 194)
        Me.Panel1.TabIndex = 0
        '
        'txtNota
        '
        Me.txtNota.Location = New System.Drawing.Point(82, 68)
        Me.txtNota.Multiline = True
        Me.txtNota.Name = "txtNota"
        Me.txtNota.Size = New System.Drawing.Size(323, 67)
        Me.txtNota.TabIndex = 70
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(12, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 20)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Nota:"
        '
        'txtFinal
        '
        Me.txtFinal.Location = New System.Drawing.Point(286, 39)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(119, 20)
        Me.txtFinal.TabIndex = 67
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(216, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 20)
        Me.Label1.TabIndex = 68
        Me.Label1.Text = "Costo Final:"
        '
        'txtInicial
        '
        Me.txtInicial.Location = New System.Drawing.Point(82, 39)
        Me.txtInicial.Name = "txtInicial"
        Me.txtInicial.Size = New System.Drawing.Size(119, 20)
        Me.txtInicial.TabIndex = 65
        '
        'lblCodProdServ
        '
        Me.lblCodProdServ.Location = New System.Drawing.Point(12, 42)
        Me.lblCodProdServ.Name = "lblCodProdServ"
        Me.lblCodProdServ.Size = New System.Drawing.Size(83, 20)
        Me.lblCodProdServ.TabIndex = 66
        Me.lblCodProdServ.Text = "Costo Inicial:"
        '
        'cmdAddProv
        '
        Me.cmdAddProv.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddProv.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddProv.Appearance.Options.UseFont = True
        Me.cmdAddProv.Appearance.Options.UseForeColor = True
        Me.cmdAddProv.Location = New System.Drawing.Point(407, 12)
        Me.cmdAddProv.Name = "cmdAddProv"
        Me.cmdAddProv.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddProv.TabIndex = 64
        Me.cmdAddProv.Text = "+"
        Me.cmdAddProv.ToolTip = "Agregar un nuevo Proveedor"
        '
        'cmbTipoCif
        '
        Me.cmbTipoCif.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbTipoCif.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbTipoCif.FormattingEnabled = True
        Me.cmbTipoCif.Items.AddRange(New Object() {"PRODUCTO", "SERVICIO"})
        Me.cmbTipoCif.Location = New System.Drawing.Point(82, 12)
        Me.cmbTipoCif.Name = "cmbTipoCif"
        Me.cmbTipoCif.Size = New System.Drawing.Size(323, 21)
        Me.cmbTipoCif.TabIndex = 63
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "Tipo Costo:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdCancelar)
        Me.Panel2.Controls.Add(Me.cmdAceptar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 145)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(437, 49)
        Me.Panel2.TabIndex = 2
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(339, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(255, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'frmCIFDetalle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(437, 194)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCIFDetalle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalle de Costos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbTipoCif As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddProv As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtInicial As System.Windows.Forms.TextBox
    Friend WithEvents lblCodProdServ As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNota As System.Windows.Forms.TextBox
End Class
