﻿Imports System.Xml
Public Class frmAdminCIF
    Public ResumMaster As String = ""

    Public Sub CargarDatos(ByVal Codigo As String, ByVal Anio As Integer, ByVal Mes As Integer)
        Dim strsql As String = ""

        Try
            strsql = "SELECT Codigo_CIF, Mes, Anio, Descripcion, Inicial, Final, Valido FROM CIF WHERE Codigo_CIF = '" & Codigo & "' and Mes = '" & Mes & "' and Anio = '" & Anio & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count <> 0 Then
                txtCodigoCif.Text = tblDatos.Rows(0).Item("Codigo_CIF")
                txtDescripcion.Text = IIf(tblDatos.Rows(0).Item("Descripcion") Is DBNull.Value, "", tblDatos.Rows(0).Item("Descripcion"))
                txtInicial.Text = IIf(tblDatos.Rows(0).Item("Inicial") Is DBNull.Value, 0, tblDatos.Rows(0).Item("Inicial"))
                txtFinal.Text = IIf(tblDatos.Rows(0).Item("Final") Is DBNull.Value, 0, tblDatos.Rows(0).Item("Final"))
                cmbAnio.SelectedValue = tblDatos.Rows(0).Item("Anio")
                cmbMes.SelectedValue = tblDatos.Rows(0).Item("Mes")
                ckIsActivo.Checked = tblDatos.Rows(0).Item("Valido")

                CargarDetalle(txtCodigoCif.Text, cmbAnio.SelectedValue, cmbMes.SelectedValue)

            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub CargarDetalle(ByVal Codigo As String, ByVal Anio As Integer, ByVal Mes As Integer)
        Dim strsql As String = ""
        Dim CostoIni, CostoFin, Ajuste As Double
        CostoIni = 0
        CostoFin = 0
        Ajuste = 0

        grvCostosCif.Columns.Clear()

        Try
            strsql = "exec [dbo].[SP_GET_Cif_Mes] '" & Codigo & "'," & Mes & "," & Anio

            Dim Detalle As DataTable = SQL(strsql, "tblCostos", My.Settings.SolIndustrialCNX).Tables(0)

            grdCostosCif.DataSource = Detalle
            grvCostosCif.Columns(0).Width = 300
            grvCostosCif.Columns(1).Width = 80
            grvCostosCif.Columns(2).Width = 80
            grvCostosCif.Columns(3).Width = 80

            If Detalle.Rows.Count > 0 Then
                For i As Integer = 0 To Detalle.Rows.Count - 1
                    CostoIni = CostoIni + Detalle.Rows(i).Item("Costo Inicial").ToString
                    CostoFin = CostoFin + Detalle.Rows(i).Item("Costo Final").ToString
                    Ajuste = Ajuste + Detalle.Rows(i).Item("Ajuste").ToString
                Next i

                txtInicial.Text = CostoIni
                txtFinal.Text = CostoFin
                txtAjuste.Text = Ajuste

                btnAgregar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True

                cargarOrdenes()
            Else
                txtAjuste.Text = 0

                btnAgregar.Enabled = True
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub limpiar()
        txtCodigoCif.Text = Microsoft.VisualBasic.Right("000000" & RegistroMaximo(), 6)

        ckIsActivo.Checked = True
        cmbMes.SelectedValue = Date.Now.Month
        cmbAnio.SelectedValue = Date.Now.Year
        txtAjuste.Text = 0
        txtInicial.Text = 0
        txtFinal.Text = 0
        grvCostosCif.Columns.Clear()
        grvOrdenes.Columns.Clear()
        txtEstado.Text = "SIN AJUSTAR"
    End Sub

    Public Sub nuevo()
        NumCatalogo = 30
        nTipoEdic = 1
        limpiar()
        activarPaneles()
        PermitirBotonesEdicion(False)
    End Sub

    Public Sub activarPaneles()
        grbDatos.Enabled = True
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Public Sub cargarAnio()
        cmbAnio.DisplayMember = "Anio"
        cmbAnio.ValueMember = "Anio"
        cmbAnio.DataSource = SQL("SELECT Anio FROM Anios", "tblAnio", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Public Sub cargarMes()
        cmbMes.DisplayMember = "Descripcion"
        cmbMes.ValueMember = "Mes"
        cmbMes.DataSource = SQL("SELECT Mes, Descripcion FROM Meses", "tblMes", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Public Function validar(ByVal Anio As Integer, ByVal Mes As Integer) As Boolean
        Dim strsql As String = ""
        Dim valido As Boolean

        strsql = "SELECT Codigo_CIF FROM CIF WHERE Mes = " & Mes & " AND Anio = " & Anio
        Dim Data As DataTable = SQL(strsql, "tblValidar", My.Settings.SolIndustrialCNX).Tables(0)

        If Data.Rows.Count > 0 Then
            valido = False
        Else
            valido = True
        End If

        Return valido
    End Function

    Public Sub cargarOrdenes()
        Dim strsql As String
        Dim Mes, Anio As String
        Dim Estado As String

        grvOrdenes.Columns.Clear()

        Mes = cmbMes.SelectedValue.ToString
        Anio = cmbAnio.SelectedValue.ToString

        strsql = "SELECT (CASE WHEN Ajustado = 1 THEN 'Ajustado' ELSE 'Sin Ajustar' END) Estado,OC.Dia,COUNT(OCD.Orden) 'Ordenes', "
        strsql = strsql + " Sum(OCD.Costo_Total) 'Costo Total',(CASE WHEN Ajustado = 1 THEN 0 ELSE [dbo].[getPorcCIF](" + Anio + "," + Mes + ") END) 'Ajuste Diario',"
        strsql = strsql + " (Sum(OCD.Costo_Total) + (CASE WHEN Ajustado = 1 THEN 0 ELSE [dbo].[getPorcCIF](" + Anio + "," + Mes + ") END)) 'Total Ajustado'"
        strsql = strsql + " FROM Orden_Cierre OC INNER JOIN Orden_Cierre_Detalle OCD ON OC.Codigo_Cierre = OCD.Codigo_Cierre AND OC.Anio = OCD.Anio"
        strsql = strsql + " AND OC.Mes = OCD.Mes AND OC.Dia = OCD.Dia Where OC.Mes = " + Mes + " and OC.Anio = " & Anio & " group by OC.Dia,Ajustado"

        grdOrdenes.DataSource = SQL(strsql, "tblOrdenes", My.Settings.SolIndustrialCNX).Tables(0)

        grvOrdenes.Columns("Estado").Width = 60
        grvOrdenes.Columns("Dia").Width = 28
        grvOrdenes.Columns("Ordenes").Width = 50
        grvOrdenes.Columns("Costo Total").Width = 80
        grvOrdenes.Columns("Ajuste Diario").Width = 80
        grvOrdenes.Columns("Total Ajustado").Width = 80

        If grvOrdenes.DataRowCount > 0 Then
            For i As Integer = 0 To grvOrdenes.DataRowCount - 1
                Estado = grvOrdenes.GetRowCellValue(i, "Estado")

                If Estado = "Ajustado" Then
                    txtEstado.Text = "AJUSTADO"
                    btnAutorizar.Enabled = False
                    Panel7.Enabled = False
                    Exit For
                Else
                    txtEstado.Text = "SIN AJUSTAR"
                    btnAutorizar.Enabled = True
                    Panel7.Enabled = True
                    Exit For
                End If
            Next
        Else
            txtEstado.Text = "SIN AJUSTAR"
        End If

    End Sub

    Public Sub guardarMasterDoc()
        Dim strsql As String = ""
        Dim msg As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            If validar(cmbAnio.SelectedValue, cmbMes.SelectedValue) = True Then
                Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then

                    ResumMaster = Produccion.EditarCif(txtCodigoCif.Text, cmbAnio.SelectedValue, cmbMes.SelectedValue, txtDescripcion.Text, txtInicial.Text, txtFinal.Text, ckIsActivo.Checked, 1)

                    If ResumMaster = "OK" Then
                        PermitirBotonesEdicion(True)
                        CargarDatos(txtCodigoCif.Text, cmbAnio.SelectedValue, cmbMes.SelectedValue)
                    End If

                End If
            Else
                msg = "El CIF de " & cmbMes.Text & " ya esta registrado."
                MsgBox(msg, MsgBoxStyle.Information, "Información")
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub Cerrar()
        PermitirBotonesEdicion(True)
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmAdminCIF_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtEstado.Text = "SIN AJUSTAR"
        cargarAnio()
        cargarMes()
    End Sub

    Private Sub frmAdminCIF_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 30
    End Sub

    Private Sub cmbMes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMes.SelectedIndexChanged
        lblCierre.Text = "Cierres Aplicados en " & cmbMes.Text & " - " & cmbAnio.Text
        cargarOrdenes()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdic = 1
        frmCIFDetalle.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdic = 2
        frmCIFDetalle.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim result As Integer = MessageBox.Show("Seguro que desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            nTipoEdic = 3
            frmCIFDetalle.ShowDialog()
        End If
    End Sub

    Private Sub btnAutorizar_Click(sender As Object, e As EventArgs) Handles btnAutorizar.Click
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Anio, Mes, Dia As Integer
        Dim Monto As Double = 0
        Dim Count As Integer = 0

        Try

            If grvOrdenes.DataRowCount > 0 Then
                Dim result As Integer = MessageBox.Show("Desea ajustar los cierres?", "Confirmación de Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If result = DialogResult.Yes Then

                    For i As Integer = 0 To grvOrdenes.DataRowCount - 1
                        Anio = cmbAnio.SelectedValue
                        Mes = cmbMes.SelectedValue
                        Dia = grvOrdenes.GetRowCellValue(i, "Dia")
                        Monto = grvOrdenes.GetRowCellValue(i, "Ajuste Diario")

                        ResumMaster = Produccion.AjusteCierre(My.Settings.Sucursal, Anio, Mes, Dia, Monto)

                        If ResumMaster = "OK" Then
                            Count = Count + 1
                        End If

                    Next i

                    If grvOrdenes.DataRowCount = Count Then
                        MsgBox("Ajuste realizado correctamente.", MsgBoxStyle.Information, "Información")
                        cargarOrdenes()
                    Else
                        MsgBox("Error al aplicar ajuste.", MsgBoxStyle.Critical, "Información")
                    End If

                End If
            Else
                Dim result As Integer = MessageBox.Show("No hay ordenes pendientes de ajustar.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmAdminCIF_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub

    Private Sub txtEstado_TextChanged(sender As Object, e As EventArgs) Handles txtEstado.TextChanged
        If txtEstado.Text = "SIN AJUSTAR" Then
            If grvOrdenes.RowCount > 0 Then
                btnAutorizar.Enabled = True
                Panel7.Enabled = True
            End If
        Else
            btnAutorizar.Enabled = False
            Panel7.Enabled = False
        End If
    End Sub
End Class