﻿Imports System.Globalization
Imports System.Threading

Public Class frmCierreOrden
    Public ResumEntrada As String = ""
    Public ResumSalida As String = ""
    Public ResumEntradaDeta As String = ""
    Public ResumSalidaDeta As String = ""
    Public CodMaxEntrada As String = ""
    Public CodMaxSalida As String = ""

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub getPendiente()
        Dim strsql As String = ""
        Dim valor As String = ""

        Try
            strsql = "SELECT COUNT(DISTINCT CAST(Registrado AS DATE)) FROM Orden_Produccion WHERE Estado = 2029"
            Dim Data As DataTable = SQL(strsql, "tblPendientes", My.Settings.SolIndustrialCNX).Tables(0)

            If Data.Rows.Count > 0 Then
                txtPendiente.Text = Data.Rows(0).Item(0)
            Else
                txtPendiente.Text = 0
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub CargarCIF()
        Dim strsql As String = ""
        Dim CostoCIF As Double = 0

        grvCIF.Columns.Clear()

        Try

            Dim yavalio = conexion_to_db.GetData("SP_GET_Cif_Orden", Funciones.Param("@Fecha", deRegistrado.Value, SqlDbType.DateTime))

            Dim DetalleCIF As DataTable = yavalio

            grdCIF.DataSource = DetalleCIF
            grvCIF.Columns("Descripcion").Width = 190
            grvCIF.Columns("Costo (mes)").Width = 80
            grvCIF.Columns("Costo (mes)").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            grvCIF.Columns("Costo (mes)").DisplayFormat.FormatString = "{0:#,###,##0.0000}"
            grvCIF.Columns("Costo (dia)").Width = 80
            grvCIF.Columns("Costo (dia)").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            grvCIF.Columns("Costo (dia)").DisplayFormat.FormatString = "{0:#,###,##0.0000}"

            grvCIF.Columns("Costo (mes)").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvCIF.Columns("Costo (mes)").SummaryItem.DisplayFormat = "{0:#,###,##0.0000}"

            grvCIF.Columns("Costo (dia)").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvCIF.Columns("Costo (dia)").SummaryItem.DisplayFormat = "{0:#,###,##0.0000}"

            grvCIF.OptionsView.ShowFooter = True

            If DetalleCIF.Rows.Count > 0 Then
                For i As Integer = 0 To DetalleCIF.Rows.Count - 1
                    CostoCIF = CostoCIF + DetalleCIF.Rows(i).Item(2).ToString
                Next i

                txtCif.Text = CostoCIF
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarOrdenes()
        Dim strsql As String = ""
        Dim CostoOrden As Double = 0
        Dim CostoTotal As Double = 0

        grvDetalle.Columns.Clear()

        Try
      
            Dim yavalio = conexion_to_db.GetData("SP_GET_Orden_Cierre", Funciones.Param("@Fecha", deRegistrado.Value, SqlDbType.DateTime))

            Dim DetalleCIF As DataTable = yavalio

            Dim DetalleCierre As DataTable = yavalio

            grdDetalle.DataSource = DetalleCierre
            grvDetalle.Columns(0).Width = 60
            grvDetalle.Columns(1).Width = 340
            grvDetalle.Columns(2).Width = 60
            grvDetalle.Columns(3).Width = 85
            grvDetalle.Columns(4).Width = 85
            grvDetalle.Columns(5).Width = 85
            grvDetalle.Columns(5).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            grvDetalle.Columns(5).DisplayFormat.FormatString = "{0: #.####%}"
            grvDetalle.Columns(6).Width = 85
            grvDetalle.Columns(7).Width = 85

            txtCantidad.Text = grvDetalle.RowCount

            If DetalleCierre.Rows.Count > 0 Then
                For i As Integer = 0 To DetalleCierre.Rows.Count - 1
                    CostoOrden = CostoOrden + DetalleCierre.Rows(i).Item(4).ToString
                    CostoTotal = CostoTotal + DetalleCierre.Rows(i).Item(7).ToString
                Next i

                txtCostoOrden.Text = CostoOrden
                txtCostoTotal.Text = CostoTotal
            Else
                txtCostoOrden.Text = 0
                txtCostoTotal.Text = txtCif.Text
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub frmCierreOrden_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCodigoCierre.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        deRegistrado.Value = Date.Now
        getPendiente()
    End Sub

    Private Sub deRegistrado_EditValueChanged(sender As Object, e As EventArgs) Handles deRegistrado.ValueChanged
        CargarCIF()
        CargarOrdenes()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        frmBusquedaDoc.Text = "Cierres sin Aplicar"
        frmBusquedaDoc.Size = New Size(364, 324)
        frmBusquedaDoc.ShowDialog()
    End Sub

    Private Sub btnAutorizar_Click(sender As Object, e As EventArgs) Handles btnAutorizar.Click
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim strsql As String = ""
        Dim Codigo As String = ""
        Dim ResumCierre As String = ""
        Dim ResumCierreDeta As String = ""
        Dim Resum As String = ""
        Dim Costo As Double = 0
        Dim CostoTotal As Double = 0
        Dim PorceCIF As Double = 0
        Dim CIF As Double = 0
        Dim Cantidad As Integer = 0

        Try
            If grvDetalle.RowCount > 0 Then
                If Not grvCIF.RowCount > 0 Then MsgBox("Costos no registrados.") : Exit Sub

                Dim result As Integer = MessageBox.Show("Desea aplicar el cierre?", "Confirmación de Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    Dim Anio, Mes, Dia As Integer

                    Anio = deRegistrado.Value.Year
                    Mes = deRegistrado.Value.Month
                    Dia = deRegistrado.Value.Day

                    ResumCierre = Produccion.EditarCierre(txtCodigoCierre.Text, Anio, Mes, Dia, txtCantidad.Text, txtCostoTotal.Text, txtCif.Text, deRegistrado.Value, False, 1)

                    For i As Integer = 0 To grvDetalle.DataRowCount - 1
                        Codigo = grvDetalle.GetRowCellValue(i, "Codigo")
                        Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                        Costo = grvDetalle.GetRowCellValue(i, "C. Producción")
                        CostoTotal = grvDetalle.GetRowCellValue(i, "Costo Total")
                        PorceCIF = grvDetalle.GetRowCellValue(i, "% CIF & MOD")
                        CIF = grvDetalle.GetRowCellValue(i, "CIF & MOD")

                        strsql = "SELECT Top 1 Cod_Producto_Master Producto, Solicitante, Cantidad_Requerida CantidadR, Costo_Requerido CostoR, Cantidad_Elaborado CantidadE, "
                        strsql = strsql & "Costo_Elaborado CostoE, Registrado, Autorizado FROM Orden_Produccion where CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Orden_Produc = '" & Codigo & "'"

                        Dim Data As DataTable = SQL(strsql, "tblOrden", My.Settings.SolIndustrialCNX).Tables(0)

                        Resum = Produccion.EditarOrdenProduccion(My.Settings.Sucursal, Codigo, Data.Rows(0).Item(1), Data.Rows(0).Item(0), Convert.ToInt32(Data.Rows(0).Item(2)), Data.Rows(0).Item(3), Convert.ToInt32(Data.Rows(0).Item(4)), Data.Rows(0).Item(5), Data.Rows(0).Item(6), Date.Now, 19, Data.Rows(0).Item(7), True, 2, frmPrincipal.LblNombreUsuario.Text)
                        ResumCierreDeta = Produccion.EditarCierreDetalle(txtCodigoCierre.Text, Anio, Mes, Dia, Codigo, Costo, (PorceCIF * 100), CIF, 0, CostoTotal, True, 1)

                        CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(3, 4), 8)
                        ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, 1, Codigo, 17, "ENTRADA A BODEGA DE PRODUCTO TERMINADO", CostoTotal, Date.Now, 4, False, 1, 3)
                        ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Data.Rows(0).Item(0), Cantidad, (CostoTotal / Cantidad), "", 3, False, 1, 4)

                    Next i

                    If Resum = "OK" Then
                        MsgBox("Cierre realizado correctamente.", MsgBoxStyle.Information, "Información")
                        getPendiente()
                        CargarCIF()
                        CargarOrdenes()
                    End If

                End If
            Else
                MsgBox("No hay ordenes que cerrar.", MsgBoxStyle.Information, "Información")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmCierreOrden_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub

    Private Sub frmCierreOrden_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 29
    End Sub

End Class