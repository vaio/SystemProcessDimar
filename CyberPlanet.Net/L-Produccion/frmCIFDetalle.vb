﻿Imports System.Xml
Imports System.Text.RegularExpressions
Public Class frmCIFDetalle
    Public Resum As String = ""
    Dim CodigoCIF As String
    Dim Anio, Mes As Integer

    Public Sub Cerrar()
        nTipoEdic = 1
        Limpiar()
        Me.Dispose()
        Close()
    End Sub

    Public Sub Limpiar()
        cmbTipoCif.SelectedIndex = 0
        txtInicial.Text = 0
        txtFinal.Text = 0
        txtNota.Text = ""
    End Sub

    Public Sub cargarDatos()
        Dim strsql As String = ""

        Try
            If frmAdminCIF.grvCostosCif.RowCount > 0 Then
                Dim registro As String = frmAdminCIF.grvCostosCif.GetRowCellValue(frmAdminCIF.grvCostosCif.GetSelectedRows(0), "Detalle Costo").ToString()

                strsql = " SELECT CIF_Detalle.Tipo_CIF, CIF_Detalle.Monto_Inicial, CIF_Detalle.Monto_Final, CIF_Detalle.Nota"
                strsql = strsql & " FROM CIF_Tipo INNER JOIN CIF_Detalle ON CIF_Tipo.Id_Tipo = CIF_Detalle.Tipo_CIF"
                strsql = strsql & " WHERE CIF_Tipo.Descripcion = '" & registro & "' and CIF_Detalle.Codigo_CIF = '" & CodigoCIF & "'"
                strsql = strsql & " and CIF_Detalle.Mes = " & Mes & " and CIF_Detalle.Anio = " & Anio

                Dim Data As DataTable = SQL(strsql, "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)

                If Data.Rows.Count > 0 Then
                    cmbTipoCif.SelectedValue = Data.Rows(0).Item("Tipo_CIF")
                    txtInicial.Text = Data.Rows(0).Item("Monto_Inicial")
                    txtFinal.Text = Data.Rows(0).Item("Monto_Final")
                    txtNota.Text = Data.Rows(0).Item("Nota")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub cargarTipos()
        cmbTipoCif.DisplayMember = "Descripcion"
        cmbTipoCif.ValueMember = "Id_Tipo"
        cmbTipoCif.DataSource = SQL("SELECT Id_Tipo, Descripcion FROM CIF_Tipo WHERE Activo = 1", "tblTipoCifAnio", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Private Sub frmCIFDetalle_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargarTipos()
        txtInicial.Text = 0
        txtFinal.Text = 0
        txtNota.Text = ""

        CodigoCIF = frmAdminCIF.txtCodigoCif.Text
        Anio = frmAdminCIF.cmbAnio.SelectedValue
        Mes = frmAdminCIF.cmbMes.SelectedValue

        If nTipoEdic = 1 Then
            cmbTipoCif.Enabled = True
            txtInicial.Enabled = True
            txtFinal.Enabled = False
            txtNota.Enabled = True

        ElseIf nTipoEdic = 2 Then
            cmbTipoCif.Enabled = False
            txtInicial.Enabled = False
            txtFinal.Enabled = True
            txtNota.Enabled = True

            cargarDatos()
        ElseIf nTipoEdic = 3 Then
            cmbTipoCif.Enabled = False
            txtInicial.Enabled = False
            txtFinal.Enabled = False
            txtNota.Enabled = False

            cargarDatos()
        End If
       
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim strsql As String = ""
        Dim msg As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            'Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)
            'If result = DialogResult.Yes Then

            Resum = Produccion.EditarCifDetalle(CodigoCIF, Anio, Mes, cmbTipoCif.SelectedValue, txtInicial.Text, txtFinal.Text, txtNota.Text, False, nTipoEdic)

            If Resum = "OK" Then
                Cerrar()
                frmAdminCIF.CargarDetalle(CodigoCIF, Anio, Mes)
                frmAdminCIF.cargarOrdenes()
            End If

            'End If
        Catch ex As Exception

        End Try
    End Sub
End Class