﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrdenProd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrdenProd))
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblRemplazado = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnQuitarMat = New System.Windows.Forms.Button()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.btnAutorizar = New System.Windows.Forms.Button()
        Me.btnAgregarMat = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblAutorizado = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblSinAuto = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblExist = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.pnlRemision = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.grdRemision = New DevExpress.XtraGrid.GridControl()
        Me.grvRemision = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.dtpCerrado = New System.Windows.Forms.DateTimePicker()
        Me.txtColaboradores = New System.Windows.Forms.TextBox()
        Me.dtpRegistrado = New System.Windows.Forms.DateTimePicker()
        Me.lblCif = New System.Windows.Forms.Label()
        Me.lblCostoTotal = New System.Windows.Forms.Label()
        Me.lblCostoUnd = New System.Windows.Forms.Label()
        Me.lblCodigoOrden = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbProducProducir = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCantidadPro = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.CmbSolicitante = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlTerminados = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtUnidadMed = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnCerrarOrden = New System.Windows.Forms.Button()
        Me.txtElaborado = New System.Windows.Forms.TextBox()
        Me.btnLiquidar = New System.Windows.Forms.Button()
        Me.lblTextoFinal = New System.Windows.Forms.Label()
        Me.grdFinal = New DevExpress.XtraGrid.GridControl()
        Me.grvFinal = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.pnlAgregar = New System.Windows.Forms.Panel()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.grdDetalle = New DevExpress.XtraGrid.GridControl()
        Me.grvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.mainPanel.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlRemision.SuspendLayout()
        CType(Me.grdRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTerminados.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.grdFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAgregar.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.Label18)
        Me.mainPanel.Controls.Add(Me.lblRemplazado)
        Me.mainPanel.Controls.Add(Me.Panel2)
        Me.mainPanel.Controls.Add(Me.Label17)
        Me.mainPanel.Controls.Add(Me.lblAutorizado)
        Me.mainPanel.Controls.Add(Me.Label16)
        Me.mainPanel.Controls.Add(Me.lblSinAuto)
        Me.mainPanel.Controls.Add(Me.Label15)
        Me.mainPanel.Controls.Add(Me.lblExist)
        Me.mainPanel.Controls.Add(Me.lblEstado)
        Me.mainPanel.Controls.Add(Me.Label13)
        Me.mainPanel.Controls.Add(Me.Label11)
        Me.mainPanel.Controls.Add(Me.pnlRemision)
        Me.mainPanel.Controls.Add(Me.Panel4)
        Me.mainPanel.Controls.Add(Me.pnlTerminados)
        Me.mainPanel.Controls.Add(Me.Panel1)
        Me.mainPanel.Location = New System.Drawing.Point(0, 0)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1160, 493)
        Me.mainPanel.TabIndex = 0
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(648, 4)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(66, 13)
        Me.Label18.TabIndex = 90
        Me.Label18.Text = "Remplazado"
        '
        'lblRemplazado
        '
        Me.lblRemplazado.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblRemplazado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRemplazado.Location = New System.Drawing.Point(634, 4)
        Me.lblRemplazado.Name = "lblRemplazado"
        Me.lblRemplazado.Size = New System.Drawing.Size(14, 14)
        Me.lblRemplazado.TabIndex = 89
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnQuitarMat)
        Me.Panel2.Controls.Add(Me.btnAplicar)
        Me.Panel2.Controls.Add(Me.btnAutorizar)
        Me.Panel2.Controls.Add(Me.btnAgregarMat)
        Me.Panel2.Location = New System.Drawing.Point(214, 101)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(360, 28)
        Me.Panel2.TabIndex = 73
        '
        'btnQuitarMat
        '
        Me.btnQuitarMat.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnQuitarMat.Image = CType(resources.GetObject("btnQuitarMat.Image"), System.Drawing.Image)
        Me.btnQuitarMat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnQuitarMat.Location = New System.Drawing.Point(200, 1)
        Me.btnQuitarMat.Name = "btnQuitarMat"
        Me.btnQuitarMat.Size = New System.Drawing.Size(77, 27)
        Me.btnQuitarMat.TabIndex = 65
        Me.btnQuitarMat.Text = "Quitar"
        Me.btnQuitarMat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnQuitarMat.UseVisualStyleBackColor = True
        '
        'btnAplicar
        '
        Me.btnAplicar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAplicar.Image = CType(resources.GetObject("btnAplicar.Image"), System.Drawing.Image)
        Me.btnAplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAplicar.Location = New System.Drawing.Point(280, 1)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(77, 27)
        Me.btnAplicar.TabIndex = 62
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'btnAutorizar
        '
        Me.btnAutorizar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAutorizar.Image = CType(resources.GetObject("btnAutorizar.Image"), System.Drawing.Image)
        Me.btnAutorizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAutorizar.Location = New System.Drawing.Point(38, 1)
        Me.btnAutorizar.Name = "btnAutorizar"
        Me.btnAutorizar.Size = New System.Drawing.Size(77, 27)
        Me.btnAutorizar.TabIndex = 64
        Me.btnAutorizar.Text = "Autorizar"
        Me.btnAutorizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAutorizar.UseVisualStyleBackColor = True
        '
        'btnAgregarMat
        '
        Me.btnAgregarMat.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAgregarMat.Image = CType(resources.GetObject("btnAgregarMat.Image"), System.Drawing.Image)
        Me.btnAgregarMat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregarMat.Location = New System.Drawing.Point(119, 1)
        Me.btnAgregarMat.Name = "btnAgregarMat"
        Me.btnAgregarMat.Size = New System.Drawing.Size(77, 27)
        Me.btnAgregarMat.TabIndex = 63
        Me.btnAgregarMat.Text = "Agregar"
        Me.btnAgregarMat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregarMat.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(521, 4)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(109, 13)
        Me.Label17.TabIndex = 88
        Me.Label17.Text = "(Adicional) Autorizado"
        '
        'lblAutorizado
        '
        Me.lblAutorizado.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblAutorizado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAutorizado.Location = New System.Drawing.Point(507, 4)
        Me.lblAutorizado.Name = "lblAutorizado"
        Me.lblAutorizado.Size = New System.Drawing.Size(14, 14)
        Me.lblAutorizado.TabIndex = 87
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(386, 4)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(118, 13)
        Me.Label16.TabIndex = 86
        Me.Label16.Text = "(Adicional) Sin Autorizar"
        '
        'lblSinAuto
        '
        Me.lblSinAuto.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblSinAuto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSinAuto.Location = New System.Drawing.Point(372, 4)
        Me.lblSinAuto.Name = "lblSinAuto"
        Me.lblSinAuto.Size = New System.Drawing.Size(14, 14)
        Me.lblSinAuto.TabIndex = 85
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(297, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 13)
        Me.Label15.TabIndex = 84
        Me.Label15.Text = "Sin Existencia"
        '
        'lblExist
        '
        Me.lblExist.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblExist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblExist.Location = New System.Drawing.Point(283, 4)
        Me.lblExist.Name = "lblExist"
        Me.lblExist.Size = New System.Drawing.Size(14, 14)
        Me.lblExist.TabIndex = 83
        '
        'lblEstado
        '
        Me.lblEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.ForeColor = System.Drawing.Color.Red
        Me.lblEstado.Location = New System.Drawing.Point(1008, 3)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(147, 17)
        Me.lblEstado.TabIndex = 82
        Me.lblEstado.Text = "SIN REGISTRAR"
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Navy
        Me.Label13.Location = New System.Drawing.Point(828, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(71, 17)
        Me.Label13.TabIndex = 81
        Me.Label13.Text = "ESTADO:"
        Me.Label13.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Navy
        Me.Label11.Location = New System.Drawing.Point(2, 1)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(269, 15)
        Me.Label11.TabIndex = 80
        Me.Label11.Text = "REMISION DE ORDEN DE PRODUCCIÓN"
        '
        'pnlRemision
        '
        Me.pnlRemision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlRemision.Controls.Add(Me.Label9)
        Me.pnlRemision.Controls.Add(Me.grdRemision)
        Me.pnlRemision.Location = New System.Drawing.Point(578, 99)
        Me.pnlRemision.Name = "pnlRemision"
        Me.pnlRemision.Size = New System.Drawing.Size(581, 251)
        Me.pnlRemision.TabIndex = 76
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(3, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(199, 15)
        Me.Label9.TabIndex = 55
        Me.Label9.Text = "MATERIALES NO UTILIZADOS"
        '
        'grdRemision
        '
        Me.grdRemision.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.grdRemision.Location = New System.Drawing.Point(0, 31)
        Me.grdRemision.MainView = Me.grvRemision
        Me.grdRemision.Name = "grdRemision"
        Me.grdRemision.Size = New System.Drawing.Size(579, 218)
        Me.grdRemision.TabIndex = 54
        Me.grdRemision.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvRemision})
        '
        'grvRemision
        '
        Me.grvRemision.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvRemision.GridControl = Me.grdRemision
        Me.grvRemision.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvRemision.Name = "grvRemision"
        Me.grvRemision.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvRemision.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvRemision.OptionsBehavior.Editable = False
        Me.grvRemision.OptionsCustomization.AllowColumnMoving = False
        Me.grvRemision.OptionsCustomization.AllowColumnResizing = False
        Me.grvRemision.OptionsCustomization.AllowGroup = False
        Me.grvRemision.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvRemision.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvRemision.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.grvRemision.OptionsView.AllowHtmlDrawGroups = False
        Me.grvRemision.OptionsView.ColumnAutoWidth = False
        Me.grvRemision.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvRemision.OptionsView.ShowFooter = True
        Me.grvRemision.OptionsView.ShowGroupPanel = False
        Me.grvRemision.PaintStyleName = "UltraFlat"
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.dtpCerrado)
        Me.Panel4.Controls.Add(Me.dtpRegistrado)
        Me.Panel4.Controls.Add(Me.lblCif)
        Me.Panel4.Controls.Add(Me.lblCostoTotal)
        Me.Panel4.Controls.Add(Me.lblCostoUnd)
        Me.Panel4.Controls.Add(Me.lblCodigoOrden)
        Me.Panel4.Controls.Add(Me.Label10)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.cmbProducProducir)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.txtCantidadPro)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.Label2)
        Me.Panel4.Controls.Add(Me.ckIsActivo)
        Me.Panel4.Controls.Add(Me.lblFechaIngreso)
        Me.Panel4.Controls.Add(Me.CmbSolicitante)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(3, 20)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1157, 77)
        Me.Panel4.TabIndex = 78
        '
        'dtpCerrado
        '
        Me.dtpCerrado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCerrado.Location = New System.Drawing.Point(890, 11)
        Me.dtpCerrado.Name = "dtpCerrado"
        Me.dtpCerrado.Size = New System.Drawing.Size(130, 20)
        Me.dtpCerrado.TabIndex = 84
        '
        'txtColaboradores
        '
        Me.txtColaboradores.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtColaboradores.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtColaboradores.Location = New System.Drawing.Point(127, 5)
        Me.txtColaboradores.Name = "txtColaboradores"
        Me.txtColaboradores.ReadOnly = True
        Me.txtColaboradores.Size = New System.Drawing.Size(75, 20)
        Me.txtColaboradores.TabIndex = 73
        Me.txtColaboradores.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtColaboradores.Visible = False
        '
        'dtpRegistrado
        '
        Me.dtpRegistrado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRegistrado.Location = New System.Drawing.Point(696, 11)
        Me.dtpRegistrado.Name = "dtpRegistrado"
        Me.dtpRegistrado.Size = New System.Drawing.Size(130, 20)
        Me.dtpRegistrado.TabIndex = 73
        '
        'lblCif
        '
        Me.lblCif.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCif.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCif.Location = New System.Drawing.Point(1070, 44)
        Me.lblCif.Name = "lblCif"
        Me.lblCif.Size = New System.Drawing.Size(68, 20)
        Me.lblCif.TabIndex = 83
        Me.lblCif.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCostoTotal
        '
        Me.lblCostoTotal.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCostoTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoTotal.Location = New System.Drawing.Point(952, 43)
        Me.lblCostoTotal.Name = "lblCostoTotal"
        Me.lblCostoTotal.Size = New System.Drawing.Size(68, 20)
        Me.lblCostoTotal.TabIndex = 82
        Me.lblCostoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCostoUnd
        '
        Me.lblCostoUnd.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCostoUnd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoUnd.Location = New System.Drawing.Point(829, 43)
        Me.lblCostoUnd.Name = "lblCostoUnd"
        Me.lblCostoUnd.Size = New System.Drawing.Size(68, 20)
        Me.lblCostoUnd.TabIndex = 81
        Me.lblCostoUnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCodigoOrden
        '
        Me.lblCodigoOrden.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCodigoOrden.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCodigoOrden.Location = New System.Drawing.Point(88, 11)
        Me.lblCodigoOrden.Name = "lblCodigoOrden"
        Me.lblCodigoOrden.Size = New System.Drawing.Size(92, 20)
        Me.lblCodigoOrden.TabIndex = 80
        Me.lblCodigoOrden.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(1038, 47)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(26, 13)
        Me.Label10.TabIndex = 76
        Me.Label10.Text = "CIF:"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(840, 13)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 17)
        Me.Label12.TabIndex = 75
        Me.Label12.Text = "Cerrado:"
        '
        'cmbProducProducir
        '
        Me.cmbProducProducir.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbProducProducir.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbProducProducir.BackColor = System.Drawing.Color.White
        Me.cmbProducProducir.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProducProducir.FormattingEnabled = True
        Me.cmbProducProducir.Location = New System.Drawing.Point(88, 43)
        Me.cmbProducProducir.Name = "cmbProducProducir"
        Me.cmbProducProducir.Size = New System.Drawing.Size(537, 21)
        Me.cmbProducProducir.TabIndex = 73
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(903, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "C. Total:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(778, 47)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "Costo U:"
        '
        'txtCantidadPro
        '
        Me.txtCantidadPro.Location = New System.Drawing.Point(696, 44)
        Me.txtCantidadPro.MaxLength = 10
        Me.txtCantidadPro.Name = "txtCantidadPro"
        Me.txtCantidadPro.Size = New System.Drawing.Size(68, 20)
        Me.txtCantidadPro.TabIndex = 68
        Me.txtCantidadPro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(640, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Cantidad:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Producto:"
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Enabled = False
        Me.ckIsActivo.Location = New System.Drawing.Point(1078, 11)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Activo"
        Me.ckIsActivo.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.ckIsActivo.Size = New System.Drawing.Size(60, 18)
        Me.ckIsActivo.TabIndex = 64
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(645, 14)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(52, 16)
        Me.lblFechaIngreso.TabIndex = 63
        Me.lblFechaIngreso.Text = "Iniciado:"
        '
        'CmbSolicitante
        '
        Me.CmbSolicitante.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CmbSolicitante.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbSolicitante.FormattingEnabled = True
        Me.CmbSolicitante.Location = New System.Drawing.Point(251, 11)
        Me.CmbSolicitante.Name = "CmbSolicitante"
        Me.CmbSolicitante.Size = New System.Drawing.Size(374, 21)
        Me.CmbSolicitante.TabIndex = 60
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(186, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Solicitante:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Codigo Orden:"
        '
        'pnlTerminados
        '
        Me.pnlTerminados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTerminados.Controls.Add(Me.Panel3)
        Me.pnlTerminados.Controls.Add(Me.btnLiquidar)
        Me.pnlTerminados.Controls.Add(Me.lblTextoFinal)
        Me.pnlTerminados.Controls.Add(Me.grdFinal)
        Me.pnlTerminados.Controls.Add(Me.pnlAgregar)
        Me.pnlTerminados.Location = New System.Drawing.Point(3, 352)
        Me.pnlTerminados.Name = "pnlTerminados"
        Me.pnlTerminados.Size = New System.Drawing.Size(1156, 141)
        Me.pnlTerminados.TabIndex = 77
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label19)
        Me.Panel3.Controls.Add(Me.txtColaboradores)
        Me.Panel3.Controls.Add(Me.txtUnidadMed)
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.Controls.Add(Me.btnCerrarOrden)
        Me.Panel3.Controls.Add(Me.txtElaborado)
        Me.Panel3.Location = New System.Drawing.Point(520, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(631, 28)
        Me.Panel3.TabIndex = 73
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(8, 6)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(118, 13)
        Me.Label19.TabIndex = 74
        Me.Label19.Text = "Numero Colaboradores:"
        '
        'txtUnidadMed
        '
        Me.txtUnidadMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUnidadMed.Enabled = False
        Me.txtUnidadMed.Location = New System.Drawing.Point(403, 3)
        Me.txtUnidadMed.Name = "txtUnidadMed"
        Me.txtUnidadMed.Size = New System.Drawing.Size(75, 20)
        Me.txtUnidadMed.TabIndex = 72
        Me.txtUnidadMed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(213, 6)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(103, 13)
        Me.Label14.TabIndex = 71
        Me.Label14.Text = "Cantidad Elaborada:"
        '
        'btnCerrarOrden
        '
        Me.btnCerrarOrden.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnCerrarOrden.Image = CType(resources.GetObject("btnCerrarOrden.Image"), System.Drawing.Image)
        Me.btnCerrarOrden.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCerrarOrden.Location = New System.Drawing.Point(484, 1)
        Me.btnCerrarOrden.Name = "btnCerrarOrden"
        Me.btnCerrarOrden.Size = New System.Drawing.Size(146, 27)
        Me.btnCerrarOrden.TabIndex = 64
        Me.btnCerrarOrden.Text = "Liquidar y Cerrar Orden"
        Me.btnCerrarOrden.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCerrarOrden.UseVisualStyleBackColor = True
        '
        'txtElaborado
        '
        Me.txtElaborado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtElaborado.Location = New System.Drawing.Point(322, 3)
        Me.txtElaborado.Name = "txtElaborado"
        Me.txtElaborado.Size = New System.Drawing.Size(75, 20)
        Me.txtElaborado.TabIndex = 65
        Me.txtElaborado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnLiquidar
        '
        Me.btnLiquidar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnLiquidar.Image = CType(resources.GetObject("btnLiquidar.Image"), System.Drawing.Image)
        Me.btnLiquidar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLiquidar.Location = New System.Drawing.Point(279, 3)
        Me.btnLiquidar.Name = "btnLiquidar"
        Me.btnLiquidar.Size = New System.Drawing.Size(110, 27)
        Me.btnLiquidar.TabIndex = 63
        Me.btnLiquidar.Text = "Liquidar"
        Me.btnLiquidar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLiquidar.UseVisualStyleBackColor = True
        Me.btnLiquidar.Visible = False
        '
        'lblTextoFinal
        '
        Me.lblTextoFinal.AutoSize = True
        Me.lblTextoFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTextoFinal.ForeColor = System.Drawing.Color.Navy
        Me.lblTextoFinal.Location = New System.Drawing.Point(2, 8)
        Me.lblTextoFinal.Name = "lblTextoFinal"
        Me.lblTextoFinal.Size = New System.Drawing.Size(254, 15)
        Me.lblTextoFinal.TabIndex = 55
        Me.lblTextoFinal.Text = "PRODUCTO TERMINADO ELABORADO"
        '
        'grdFinal
        '
        Me.grdFinal.Location = New System.Drawing.Point(1, 32)
        Me.grdFinal.MainView = Me.grvFinal
        Me.grdFinal.Name = "grdFinal"
        Me.grdFinal.Size = New System.Drawing.Size(1155, 105)
        Me.grdFinal.TabIndex = 54
        Me.grdFinal.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvFinal})
        '
        'grvFinal
        '
        Me.grvFinal.GridControl = Me.grdFinal
        Me.grvFinal.Name = "grvFinal"
        Me.grvFinal.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvFinal.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvFinal.OptionsBehavior.Editable = False
        Me.grvFinal.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvFinal.OptionsView.AllowHtmlDrawGroups = False
        Me.grvFinal.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvFinal.OptionsView.ShowFooter = True
        Me.grvFinal.OptionsView.ShowGroupPanel = False
        Me.grvFinal.PaintStyleName = "UltraFlat"
        '
        'pnlAgregar
        '
        Me.pnlAgregar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAgregar.Controls.Add(Me.btnQuitar)
        Me.pnlAgregar.Controls.Add(Me.btnAgregar)
        Me.pnlAgregar.Controls.Add(Me.Label7)
        Me.pnlAgregar.Controls.Add(Me.txtCantidad)
        Me.pnlAgregar.Location = New System.Drawing.Point(427, 8)
        Me.pnlAgregar.Name = "pnlAgregar"
        Me.pnlAgregar.Size = New System.Drawing.Size(73, 251)
        Me.pnlAgregar.TabIndex = 79
        Me.pnlAgregar.Visible = False
        '
        'btnQuitar
        '
        Me.btnQuitar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnQuitar.ForeColor = System.Drawing.Color.Navy
        Me.btnQuitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnQuitar.Location = New System.Drawing.Point(-1, 146)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(73, 35)
        Me.btnQuitar.TabIndex = 20
        Me.btnQuitar.Text = "<< Utilizar"
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAgregar.ForeColor = System.Drawing.Color.Navy
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(-1, 105)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(73, 35)
        Me.btnAgregar.TabIndex = 19
        Me.btnAgregar.Text = "Regresar >>"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 55)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Cantidad"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(5, 71)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(62, 20)
        Me.txtCantidad.TabIndex = 17
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.grdDetalle)
        Me.Panel1.Location = New System.Drawing.Point(3, 99)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(569, 251)
        Me.Panel1.TabIndex = 75
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(3, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(185, 15)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "MATERIALES SOLICITADOS"
        '
        'grdDetalle
        '
        Me.grdDetalle.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.grdDetalle.Location = New System.Drawing.Point(0, 30)
        Me.grdDetalle.MainView = Me.grvDetalle
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(567, 219)
        Me.grdDetalle.TabIndex = 53
        Me.grdDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalle})
        '
        'grvDetalle
        '
        Me.grvDetalle.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvDetalle.GridControl = Me.grdDetalle
        Me.grvDetalle.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvDetalle.Name = "grvDetalle"
        Me.grvDetalle.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalle.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalle.OptionsBehavior.Editable = False
        Me.grvDetalle.OptionsBehavior.ReadOnly = True
        Me.grvDetalle.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetalle.OptionsCustomization.AllowColumnResizing = False
        Me.grvDetalle.OptionsCustomization.AllowGroup = False
        Me.grvDetalle.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.grvDetalle.OptionsView.AllowHtmlDrawGroups = False
        Me.grvDetalle.OptionsView.ColumnAutoWidth = False
        Me.grvDetalle.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetalle.OptionsView.ShowFooter = True
        Me.grvDetalle.OptionsView.ShowGroupPanel = False
        Me.grvDetalle.PaintStyleName = "UltraFlat"
        '
        'frmOrdenProd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1162, 494)
        Me.Controls.Add(Me.mainPanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOrdenProd"
        Me.Text = "Orden de Produccion"
        Me.mainPanel.ResumeLayout(False)
        Me.mainPanel.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.pnlRemision.ResumeLayout(False)
        Me.pnlRemision.PerformLayout()
        CType(Me.grdRemision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvRemision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTerminados.ResumeLayout(False)
        Me.pnlTerminados.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.grdFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvFinal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAgregar.ResumeLayout(False)
        Me.pnlAgregar.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents pnlRemision As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents grdRemision As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvRemision As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents pnlAgregar As System.Windows.Forms.Panel
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadPro As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents CmbSolicitante As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pnlTerminados As System.Windows.Forms.Panel
    Friend WithEvents lblTextoFinal As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents grdDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cmbProducProducir As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnCerrarOrden As System.Windows.Forms.Button
    Friend WithEvents btnLiquidar As System.Windows.Forms.Button
    Friend WithEvents grdFinal As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvFinal As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtElaborado As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtUnidadMed As System.Windows.Forms.TextBox
    Friend WithEvents btnAgregarMat As System.Windows.Forms.Button
    Friend WithEvents btnAutorizar As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnQuitarMat As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblExist As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblSinAuto As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblAutorizado As System.Windows.Forms.Label
    Friend WithEvents lblCodigoOrden As System.Windows.Forms.Label
    Friend WithEvents lblCostoUnd As System.Windows.Forms.Label
    Friend WithEvents lblCostoTotal As System.Windows.Forms.Label
    Friend WithEvents lblCif As System.Windows.Forms.Label
    Friend WithEvents dtpCerrado As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpRegistrado As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblRemplazado As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtColaboradores As System.Windows.Forms.TextBox
End Class
