﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCierreOrden
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCierreOrden))
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.deRegistrado = New System.Windows.Forms.DateTimePicker()
        Me.txtCostoTotal = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCostoOrden = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCif = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAutorizar = New System.Windows.Forms.Button()
        Me.txtPendiente = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.txtCodigoCierre = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grdDetalle = New DevExpress.XtraGrid.GridControl()
        Me.grvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grbTransporte = New System.Windows.Forms.GroupBox()
        Me.grdCIF = New DevExpress.XtraGrid.GridControl()
        Me.grvCIF = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.mainPanel.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbTransporte.SuspendLayout()
        CType(Me.grdCIF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvCIF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.GroupBox2)
        Me.mainPanel.Controls.Add(Me.GroupBox1)
        Me.mainPanel.Controls.Add(Me.grbTransporte)
        Me.mainPanel.Location = New System.Drawing.Point(1, 0)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1159, 481)
        Me.mainPanel.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.deRegistrado)
        Me.GroupBox2.Controls.Add(Me.txtCostoTotal)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtCostoOrden)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtCif)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.btnAutorizar)
        Me.GroupBox2.Controls.Add(Me.txtPendiente)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtCantidad)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.lblFechaIngreso)
        Me.GroupBox2.Controls.Add(Me.txtCodigoCierre)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1153, 87)
        Me.GroupBox2.TabIndex = 85
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Información de Cierre de Ordenes"
        '
        'deRegistrado
        '
        Me.deRegistrado.CustomFormat = "dd/MM/yyyy"
        Me.deRegistrado.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.deRegistrado.Location = New System.Drawing.Point(288, 19)
        Me.deRegistrado.Name = "deRegistrado"
        Me.deRegistrado.Size = New System.Drawing.Size(139, 20)
        Me.deRegistrado.TabIndex = 91
        '
        'txtCostoTotal
        '
        Me.txtCostoTotal.Location = New System.Drawing.Point(520, 53)
        Me.txtCostoTotal.Name = "txtCostoTotal"
        Me.txtCostoTotal.ReadOnly = True
        Me.txtCostoTotal.Size = New System.Drawing.Size(147, 20)
        Me.txtCostoTotal.TabIndex = 90
        Me.txtCostoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(450, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 89
        Me.Label6.Text = "Costo Total:"
        '
        'txtCostoOrden
        '
        Me.txtCostoOrden.Location = New System.Drawing.Point(324, 53)
        Me.txtCostoOrden.Name = "txtCostoOrden"
        Me.txtCostoOrden.ReadOnly = True
        Me.txtCostoOrden.Size = New System.Drawing.Size(111, 20)
        Me.txtCostoOrden.TabIndex = 88
        Me.txtCostoOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(209, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 13)
        Me.Label5.TabIndex = 87
        Me.Label5.Text = "Costo de Producción:"
        '
        'txtCif
        '
        Me.txtCif.Location = New System.Drawing.Point(90, 53)
        Me.txtCif.Name = "txtCif"
        Me.txtCif.ReadOnly = True
        Me.txtCif.Size = New System.Drawing.Size(111, 20)
        Me.txtCif.TabIndex = 86
        Me.txtCif.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 85
        Me.Label3.Text = "CIF + MOD:"
        '
        'btnAutorizar
        '
        Me.btnAutorizar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAutorizar.Image = CType(resources.GetObject("btnAutorizar.Image"), System.Drawing.Image)
        Me.btnAutorizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAutorizar.Location = New System.Drawing.Point(1005, 15)
        Me.btnAutorizar.Name = "btnAutorizar"
        Me.btnAutorizar.Size = New System.Drawing.Size(143, 27)
        Me.btnAutorizar.TabIndex = 84
        Me.btnAutorizar.Text = "Liquidar y Aplicar Cierre"
        Me.btnAutorizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAutorizar.UseVisualStyleBackColor = True
        '
        'txtPendiente
        '
        Me.txtPendiente.Location = New System.Drawing.Point(711, 18)
        Me.txtPendiente.Name = "txtPendiente"
        Me.txtPendiente.ReadOnly = True
        Me.txtPendiente.Size = New System.Drawing.Size(80, 20)
        Me.txtPendiente.TabIndex = 83
        Me.txtPendiente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(607, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 13)
        Me.Label2.TabIndex = 82
        Me.Label2.Text = "Cierres Pendientes:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(498, 18)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.ReadOnly = True
        Me.txtCantidad.Size = New System.Drawing.Size(95, 20)
        Me.txtCantidad.TabIndex = 81
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(414, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 80
        Me.Label4.Text = "Cant. Ordenes:"
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(800, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(69, 24)
        Me.Button1.TabIndex = 59
        Me.Button1.Text = "Mostrar"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(209, 21)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(73, 16)
        Me.lblFechaIngreso.TabIndex = 77
        Me.lblFechaIngreso.Text = "Fecha Cierre:"
        '
        'txtCodigoCierre
        '
        Me.txtCodigoCierre.Location = New System.Drawing.Point(90, 19)
        Me.txtCodigoCierre.Name = "txtCodigoCierre"
        Me.txtCodigoCierre.ReadOnly = True
        Me.txtCodigoCierre.Size = New System.Drawing.Size(111, 20)
        Me.txtCodigoCierre.TabIndex = 16
        Me.txtCodigoCierre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Codigo Cierre:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grdDetalle)
        Me.GroupBox1.Location = New System.Drawing.Point(436, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(720, 385)
        Me.GroupBox1.TabIndex = 84
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Listado de Ordenes en Espera"
        '
        'grdDetalle
        '
        Me.grdDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDetalle.Location = New System.Drawing.Point(3, 16)
        Me.grdDetalle.MainView = Me.grvDetalle
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(714, 366)
        Me.grdDetalle.TabIndex = 54
        Me.grdDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalle})
        '
        'grvDetalle
        '
        Me.grvDetalle.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvDetalle.GridControl = Me.grdDetalle
        Me.grvDetalle.GroupFormat = ""
        Me.grvDetalle.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvDetalle.Name = "grvDetalle"
        Me.grvDetalle.OptionsBehavior.Editable = False
        Me.grvDetalle.OptionsBehavior.ReadOnly = True
        Me.grvDetalle.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetalle.OptionsCustomization.AllowColumnResizing = False
        Me.grvDetalle.OptionsCustomization.AllowGroup = False
        Me.grvDetalle.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvDetalle.OptionsView.ColumnAutoWidth = False
        Me.grvDetalle.OptionsView.ShowGroupPanel = False
        '
        'grbTransporte
        '
        Me.grbTransporte.Controls.Add(Me.grdCIF)
        Me.grbTransporte.Location = New System.Drawing.Point(3, 96)
        Me.grbTransporte.Name = "grbTransporte"
        Me.grbTransporte.Size = New System.Drawing.Size(430, 385)
        Me.grbTransporte.TabIndex = 83
        Me.grbTransporte.TabStop = False
        Me.grbTransporte.Text = "Detalle de Costo CIF/MOD"
        '
        'grdCIF
        '
        Me.grdCIF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdCIF.Location = New System.Drawing.Point(3, 16)
        Me.grdCIF.MainView = Me.grvCIF
        Me.grdCIF.Name = "grdCIF"
        Me.grdCIF.Size = New System.Drawing.Size(424, 366)
        Me.grdCIF.TabIndex = 54
        Me.grdCIF.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvCIF})
        '
        'grvCIF
        '
        Me.grvCIF.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvCIF.GridControl = Me.grdCIF
        Me.grvCIF.Name = "grvCIF"
        Me.grvCIF.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvCIF.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvCIF.OptionsBehavior.Editable = False
        Me.grvCIF.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvCIF.OptionsView.AllowHtmlDrawGroups = False
        Me.grvCIF.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvCIF.OptionsView.ShowFooter = True
        Me.grvCIF.OptionsView.ShowGroupPanel = False
        '
        'frmCierreOrden
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1161, 482)
        Me.Controls.Add(Me.mainPanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCierreOrden"
        Me.Text = "Cierre de Ordenes ""Productos Terminados"""
        Me.mainPanel.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbTransporte.ResumeLayout(False)
        CType(Me.grdCIF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvCIF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents grdCIF As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvCIF As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grbTransporte As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCodigoCierre As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPendiente As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnAutorizar As System.Windows.Forms.Button
    Friend WithEvents grdDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtCostoTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCostoOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCif As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents deRegistrado As System.Windows.Forms.DateTimePicker
End Class
