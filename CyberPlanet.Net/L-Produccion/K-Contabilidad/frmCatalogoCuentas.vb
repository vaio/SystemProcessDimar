﻿Public Class frmCatalogoCuentas
        Dim strsqlSigCuenta As String

    Private Sub frmCatalogoCuentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
      CargarCatalogo()
      DesactivarCampos(True)
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
          frmPrincipal.bbiNuevo.Enabled = bEstado
          frmPrincipal.bbiModificar.Enabled = bEstado
          frmPrincipal.bbiEliminar.Enabled = bEstado
          frmPrincipal.bbiGuardar.Enabled = Not bEstado
          frmPrincipal.bbiCancelar.Enabled = Not bEstado
          frmPrincipal.bbiBuscar.Enabled = bEstado
          frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        Panel1.Enabled = Not bIsActivo
    End Sub

    Sub LimpiarCampos()
        If CInt(txtNivel.Text) = 6 Then
             cmbNivel.SelectedIndex = (CInt(IIf(txtNivel.Text = "", 0, txtNivel.Text)) - 1)
        Else
              cmbNivel.SelectedIndex = CInt(IIf(txtNivel.Text = "", 0, txtNivel.Text))
        End If
      txtCuentaEdit.Text = ""
      txtNombreCuenta.Text = ""
      txtDescripcionCuenta.Text = ""
      cmbTipoCuenta.SelectedIndex = 0
      cmbTipoSaldo.SelectedIndex = 0
      chkEsDetalle.Checked = False
      chkEsDeshabilitada.Checked = False
    End Sub

    Function Actualizar()
            CargarCatalogo()
    End Function

    Function Nuevo()
          PermitirBotonesEdicion(False)
          DesactivarCampos(False)
          LimpiarCampos()
          cmbNivel.Focus()
          txtCuentaEdit.Text = IIf(IsDBNull(CalcularSiguienteCuenta()), 1, CalcularSiguienteCuenta())
    End Function

    Function CalcularSiguienteCuenta()
        CalcularSiguienteCuenta = 0
        If cmbNivel.Text = 1 Then
            If txtNivel.Text = "" Or txtNivel.Text = "0" Then
                CalcularSiguienteCuenta = 1
            Else
                strsqlSigCuenta = "Select max(Cuenta)+1 as maximo from Tbl_CatalogoCuentas"
            End If
        ElseIf cmbNivel.Text = 2 Then
            strsqlSigCuenta = "Select max(SCuenta)+1 as maximo from Tbl_CatalogoCuentas where Cuenta= " & txtCuenta.Text
        ElseIf cmbNivel.Text = 3 Then
            strsqlSigCuenta = "Select max(SSCuenta)+1 as maximo from Tbl_CatalogoCuentas where Cuenta=" & txtCuenta.Text & " and SCuenta=" & txtSCuenta.Text
        ElseIf cmbNivel.Text = 4 Then
            strsqlSigCuenta = "Select max(SSSCuenta)+1 as maximo from Tbl_CatalogoCuentas where Cuenta=" & txtCuenta.Text & " and SCuenta=" & txtSCuenta.Text & " and SSCuenta=" & txtSSCuenta.Text
        ElseIf cmbNivel.Text = 5 Then
            strsqlSigCuenta = "Select max(SSSSCuenta)+1 as maximo from Tbl_CatalogoCuentas where Cuenta=" & txtCuenta.Text & " and SCuenta=" & txtSCuenta.Text & " and SSCuenta=" & txtSSCuenta.Text & " and SSSCuenta=" & txtSSSCuenta.Text
        ElseIf cmbNivel.Text = 6 Then
            strsqlSigCuenta = "Select max(SSSSSCuenta)+1 as maximo from Tbl_CatalogoCuentas where Cuenta=" & txtCuenta.Text & " and SCuenta=" & txtSCuenta.Text & " and SSCuenta=" & txtSSCuenta.Text & " and SSSCuenta=" & txtSSSCuenta.Text & " and SSSSCuenta=" & txtSSSSCuenta.Text
        End If
        If (CInt(cmbNivel.Text) >= 1 And CInt(cmbNivel.Text) <= 6 And CalcularSiguienteCuenta = 0) Then
            Dim dtCatalogoCuenta As DataTable = SQL(strsqlSigCuenta, "tblSigCuenta", My.Settings.SolIndustrialCNX).Tables(0)
            If dtCatalogoCuenta.Rows.Count > 0 Then
                If (CInt(cmbNivel.Text) >= 1 And CInt(cmbNivel.Text) <= 3) Then
                    CalcularSiguienteCuenta = dtCatalogoCuenta.Rows(0).Item("maximo")
                ElseIf (CInt(cmbNivel.Text) = 4) Then
                    CalcularSiguienteCuenta = Strings.Right("00" & dtCatalogoCuenta.Rows(0).Item("maximo"), 2)
                ElseIf (CInt(cmbNivel.Text) >= 5 And CInt(cmbNivel.Text) <= 6) Then
                    CalcularSiguienteCuenta = Strings.Right("000" & dtCatalogoCuenta.Rows(0).Item("maximo"), 3)
                End If
            Else
                CalcularSiguienteCuenta = 1
            End If
        End If
    End Function

    Function Modificar()
          PermitirBotonesEdicion(False)
          DesactivarCampos(False)
          CargarEdicionCuenta()
          cmbNivel.Focus()
    End Function

    Function Eliminar()
          PermitirBotonesEdicion(False)
          DesactivarCampos(True)
          CargarEdicionCuenta()
          cmbNivel.Focus()
    End Function

    Sub CargarEdicionCuenta()
        If CInt(txtNivel.Text) = 6 Then
             cmbNivel.SelectedIndex = (CInt(IIf(txtNivel.Text = "", 0, txtNivel.Text)) - 1)
        Else
              cmbNivel.SelectedIndex = CInt(IIf(txtNivel.Text = "", 0, txtNivel.Text))
        End If
        txtCuentaEdit.Text = ""
        txtNombreCuenta.Text = ""
        txtDescripcionCuenta.Text = ""
        cmbTipoCuenta.SelectedIndex = 0
        cmbTipoSaldo.SelectedIndex = 0
        chkEsDetalle.Checked = False
        chkEsDeshabilitada.Checked = False

        If TblCatalogoBindingSource.Count > 0 Then
              Dim registro As DataRowView = TblCatalogoBindingSource.Current
              cmbNivel.Text = registro.Item("Nivel")
              If cmbNivel.Text = 1 Then
                    txtCuentaEdit.Text = registro.Item("Cuenta")
              ElseIf cmbNivel.Text = 2 Then
                    txtCuentaEdit.Text = registro.Item("SCuenta")
              ElseIf cmbNivel.Text = 3 Then
                    txtCuentaEdit.Text = registro.Item("SSCuenta")
              ElseIf cmbNivel.Text = 4 Then
                    txtCuentaEdit.Text = registro.Item("SSSCuenta")
              ElseIf cmbNivel.Text = 5 Then
                    txtCuentaEdit.Text = registro.Item("SSSSCuenta")
              ElseIf cmbNivel.Text = 6 Then
                    txtCuentaEdit.Text = registro.Item("SSSSSCuenta")
              End If
              txtNombreCuenta.Text = registro.Item("Nombre_Cuenta")
              txtDescripcionCuenta.Text = ""
              cmbTipoCuenta.SelectedIndex = (registro.Item("Tipo_Cuenta") - 1)
              cmbTipoSaldo.SelectedIndex = (registro.Item("Tipo_Saldo") - 1)
              chkEsDetalle.Checked = registro.Item("IsDetalle")
              chkEsDeshabilitada.Checked = False


              'txtSCuenta.Text = registro.Item("Nombre_Cuenta")
              'txtSSCuenta.Text = registro.Item("SSCuenta")
              'txtSSSCuenta.Text = Strings.Right("00" & registro.Item("SSSCuenta"), 2)
              'txtSSSSCuenta.Text = Strings.Right("000" & registro.Item("SSSSCuenta"), 3)
              'txtSSSSSCuenta.Text = Strings.Right("000" & registro.Item("SSSSSCuenta"), 3)
              'txtCuentaContable.Text = txtCuenta.Text & txtSCuenta.Text & txtSSCuenta.Text & txtSSSCuenta.Text & txtSSSSCuenta.Text & txtSSSSSCuenta.Text
              'Dim nLongitud As Integer = Strings.Len(Strings.Trim(IIf(txtCuenta.Text = 0, "", txtCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSCuenta.Text = 0, "", txtSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSCuenta.Text = 0, "", txtSSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSSCuenta.Text = 0, "", txtSSSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSSSCuenta.Text = 0, "", txtSSSSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSSSSCuenta.Text = 0, "", txtSSSSSCuenta.Text)))
              'txtNivel.Text = IIf(nLongitud = 1, 1, IIf(nLongitud = 2, 2, IIf(nLongitud = 3, 3, IIf(nLongitud = 5, 4, IIf(nLongitud = 8, 5, IIf(nLongitud = 11, 6, 0))))))
              'cmbNivel.SelectedIndex = CInt(txtNivel.Text)
        'Else
        '      txtCuentaContable.Text = "0000000000000"
        '      txtNivel.Text = 0
        '      cmbNivel.SelectedIndex = CInt(txtNivel.Text)
        '      frmPrincipal.bbiModificar.Enabled = False
        '      frmPrincipal.bbiEliminar.Enabled = False
        '      'btnAnular.Enabled = False
        '      CodigoEntidad = 0
        End If

    End Sub

    Function Cancelar()
          LimpiarCampos()
          LimpiarCodificacion()
          nTipoEdic = 0
          DesactivarCampos(True)
          PermitirBotonesEdicion(True)
    End Function

    Function Guardar()
        GuardarCuentaCatalogo()
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        nTipoEdic = 0
    End Function

  Sub CargarCatalogo()
      Cursor.Current = Cursors.WaitCursor
      Dim StrSqlCatalogo As String = ""
      'GridView1.Columns.Clear()
      'If ChkMostrarBalance.Checked = False Then
            StrSqlCatalogo = "SELECT Cuenta,SCuenta,SSCuenta, SSSCuenta, SSSSCuenta, SSSSSCuenta, ([Espacios] + Nombre_Cuenta) as [Nombre_Cuenta], Nivel, Tipo_Cuenta, Tipo_Saldo, IsDetalle FROM Tbl_CatalogoCuentas order by Cuenta,SCuenta,SSCuenta, SSSCuenta, SSSSCuenta, SSSSSCuenta"
      'Else
    '	StrSqlCatalogo = "SELECT Codigo_Cuenta,Nombre_Cuenta,Debito,Credito,Saldo,IsDetalle FROM Tbl_CatalogoCuentas where Tipo_Cuenta=2"	' where "
    'End If
        TblCatalogoBindingSource.DataSource = SQL(StrSqlCatalogo, "tblCatalogo", My.Settings.SolIndustrialCNX).Tables(0)
        EncabezadosCatalogo()
      Cursor.Current = Cursors.Default
  End Sub


    Sub EncabezadosCatalogo()
        GridView1.Columns.Clear()
        GridView1.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "Cuenta"
        gc0.Caption = "Cuenta"
        gc0.FieldName = "Cuenta"
        gc0.Width = 60
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "SCuenta"
        gc1.Caption = "SCuenta"
        gc1.FieldName = "SCuenta"
        gc1.Width = 70
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "SSCuenta"
        gc2.Caption = "SSCuenta"
        gc2.FieldName = "SSCuenta"
        gc2.Width = 80
        GridView1.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "SSSCuenta"
        gc3.Caption = "SSSCuenta"
        gc3.FieldName = "SSSCuenta"
        gc3.Width = 80
        GridView1.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3

        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "SSSSCuenta"
        gc4.Caption = "SSSSCuenta"
        gc4.FieldName = "SSSSCuenta"
        gc4.Width = 100
        GridView1.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4

        Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
        gc5.Name = "SSSSSCuenta"
        gc5.Caption = "SSSSSCuenta"
        gc5.FieldName = "SSSSSCuenta"
        gc5.Width = 100
        GridView1.Columns.Add(gc5)
        gc5.Visible = True
        gc5.VisibleIndex = 5

        Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
        gc6.Name = "Nombre_Cuenta"
        gc6.Caption = "Nombre_Cuenta"
        gc6.FieldName = "Nombre_Cuenta"
        gc6.Width = 500
        GridView1.Columns.Add(gc6)
        gc6.Visible = True
        gc6.VisibleIndex = 6

        Dim gc7 As New DevExpress.XtraGrid.Columns.GridColumn
        gc7.Name = "Nivel"
        gc7.Caption = "Nivel"
        gc7.FieldName = "Nivel"
        gc7.Width = 80
        GridView1.Columns.Add(gc7)
        gc7.Visible = True
        gc7.VisibleIndex = 7

        Dim gc8 As New DevExpress.XtraGrid.Columns.GridColumn
        gc8.Name = "Tipo_Cuenta"
        gc8.Caption = "Tipo_Cuenta"
        gc8.FieldName = "Tipo_Cuenta"
        gc8.Width = 100
        GridView1.Columns.Add(gc8)
        gc8.Visible = True
        gc8.VisibleIndex = 8

        Dim gc9 As New DevExpress.XtraGrid.Columns.GridColumn
        gc9.Name = "Tipo_Saldo"
        gc9.Caption = "Tipo_Saldo"
        gc9.FieldName = "Tipo_Saldo"
        gc4.Width = 100
        GridView1.Columns.Add(gc9)
        gc9.Visible = True
        gc9.VisibleIndex = 9

        Dim gc10 As New DevExpress.XtraGrid.Columns.GridColumn
        gc10.Name = "IsDetalle"
        gc10.Caption = "IsDetalle"
        gc10.FieldName = "IsDetalle"
        gc10.Width = 80
        GridView1.Columns.Add(gc10)
        gc10.Visible = True
        gc10.VisibleIndex = 10

    End Sub

    'Private Sub ChkMostrarBalance_CheckedChanged(sender As Object, e As EventArgs) Handles ChkMostrarBalance.CheckedChanged
    '	If ChkMostrarBalance.Checked = False Then
    '		ChkMostrarBalance.Text = "Mostrar Estado de Resultados"
    '	Else
    '		ChkMostrarBalance.Text = "Mostrar Balance General"
    '	End If
    '	CargarCatalogo()
    'End Sub

    Sub LimpiarCodificacion()
          txtCuentaContable.Clear()
          txtCuenta.Text = Strings.Left(txtCuentaContable.Text, 1)
          txtSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 2), 1)
          txtSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 3), 1)
          txtSSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 5), 2)
          txtSSSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 9), 3)
          txtSSSSSCuenta.Text = Strings.Right(txtCuentaContable.Text, 3)
    End Sub

    Private Sub txtCuentaContable_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCuentaContable.KeyPress
        If Asc(e.KeyChar) = Keys.Enter Then
              'txtNivel.Text = 0
              txtCuenta.Text = Strings.Left(txtCuentaContable.Text, 1)
              txtSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 2), 1)
              txtSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 3), 1)
              txtSSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 5), 2)
              txtSSSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 9), 3)
              txtSSSSSCuenta.Text = Strings.Right(txtCuentaContable.Text, 3)
        End If
    End Sub

    Private Sub GridControl1_Click(sender As Object, e As EventArgs) Handles GridControl1.Click
          txtCuenta.Text = Strings.Left(txtCuentaContable.Text, 1)
          txtSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 2), 1)
          txtSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 3), 1)
          txtSSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 5), 2)
          txtSSSSCuenta.Text = Strings.Right(Strings.Left(txtCuentaContable.Text, 9), 3)
          txtSSSSSCuenta.Text = Strings.Right(txtCuentaContable.Text, 3)
    End Sub

    Private Sub TblCatalogoBindingSource_CurrentChanged(sender As Object, e As EventArgs) Handles TblCatalogoBindingSource.CurrentChanged
        If TblCatalogoBindingSource.Count > 0 Then
            frmPrincipal.bbiModificar.Enabled = True
            frmPrincipal.bbiEliminar.Enabled = True
            'btnAnular.Enabled = True
            Dim registro As DataRowView = TblCatalogoBindingSource.Current
            txtCuenta.Text = Strings.Right("0" & registro.Item("Cuenta"), 1)
            txtSCuenta.Text = Strings.Right("0" & registro.Item("SCuenta"), 1)
            txtSSCuenta.Text = Strings.Right("0" & registro.Item("SSCuenta"), 1)
            txtSSSCuenta.Text = Strings.Right("00" & registro.Item("SSSCuenta"), 2)
            txtSSSSCuenta.Text = Strings.Right("000" & registro.Item("SSSSCuenta"), 3)
            txtSSSSSCuenta.Text = Strings.Right("000" & registro.Item("SSSSSCuenta"), 3)
            txtCuentaContable.Text = txtCuenta.Text & txtSCuenta.Text & txtSSCuenta.Text & txtSSSCuenta.Text & txtSSSSCuenta.Text & txtSSSSSCuenta.Text
            Dim nLongitud As Integer = Strings.Len(Strings.Trim(IIf(txtCuenta.Text = 0, "", txtCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSCuenta.Text = 0, "", txtSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSCuenta.Text = 0, "", txtSSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSSCuenta.Text = 0, "", txtSSSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSSSCuenta.Text = 0, "", txtSSSSCuenta.Text))) + Strings.Len(Strings.Trim(IIf(txtSSSSSCuenta.Text = 0, "", txtSSSSSCuenta.Text)))
            txtNivel.Text = IIf(nLongitud = 1, 1, IIf(nLongitud = 2, 2, IIf(nLongitud = 3, 3, IIf(nLongitud = 5, 4, IIf(nLongitud = 8, 5, IIf(nLongitud = 11, 6, 0))))))
            If CInt(txtNivel.Text) = 6 Then
                cmbNivel.SelectedIndex = (CInt(txtNivel.Text) - 1)
            Else
                cmbNivel.SelectedIndex = CInt(txtNivel.Text)
            End If
        Else
            txtCuentaContable.Text = "0000000000000"
            txtNivel.Text = 0
            cmbNivel.SelectedIndex = CInt(txtNivel.Text)
            frmPrincipal.bbiModificar.Enabled = False
            frmPrincipal.bbiEliminar.Enabled = False
            'btnAnular.Enabled = False
            CodigoEntidad = 0
        End If
    End Sub

    Sub GuardarCuentaCatalogo()
        Dim Resum As String = ""
        Dim CatalogoCuentas As New clsContabilidad(My.Settings.SolIndustrialCNX)

        If txtNombreCuenta.Text = "" Then
            MsgBox("No ha escrito el nombre de la cuenta que desa ingresar. Esriba el nombre de la cuenta e intentelo nuevamente.", MsgBoxStyle.Information, "SIGCA")
            Exit Sub
        End If

        Try
            Resum = CatalogoCuentas.EditarCatalogoCuenta(IIf(cmbNivel.Text = 1, txtCuentaEdit.Text, txtCuenta.Text), IIf(cmbNivel.Text = 2, txtCuentaEdit.Text, txtSCuenta.Text), IIf(cmbNivel.Text = 3, txtCuentaEdit.Text, txtSSCuenta.Text), IIf(cmbNivel.Text = 4, txtCuentaEdit.Text, txtSSSCuenta.Text), IIf(cmbNivel.Text = 5, txtCuentaEdit.Text, txtSSSSCuenta.Text), IIf(cmbNivel.Text = 6, txtCuentaEdit.Text, txtSSSSSCuenta.Text), txtNombreCuenta.Text, txtDescripcionCuenta.Text, CInt(cmbNivel.Text), (cmbTipoCuenta.SelectedIndex + 1), (cmbTipoSaldo.SelectedIndex + 1), chkEsDetalle.Checked, nTipoEdic)
            If Resum = "OK" Then
                LimpiarCampos()
                CargarCatalogo()
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        '    Cerrar()
            If NumCatalogo_Ant = 0 Then
            End If
        End Try
    End Sub

    Private Sub cmbNivel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbNivel.SelectedIndexChanged
          txtCuentaEdit.Text = IIf(IsDBNull(CalcularSiguienteCuenta()), 1, CalcularSiguienteCuenta())
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmCatalogoCuentas_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave
          NumCatalogo = 0
          frmPrincipal.bbiExportar.Enabled = False
    End Sub

End Class