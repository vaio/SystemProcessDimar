﻿Public Class frmDetalleComprob

Dim KeyPressed As Char

    Private Sub frmDetalleComprob_Load(sender As Object, e As EventArgs) Handles MyBase.Load
          'txtNumCuentaCont.Text = ""
          CargarCuentas()
          lblDescripcion.Text = ""
          cmbTipo.SelectedIndex = 0
          luTasaCambio.Text = Now.Date
          txtMonto.Text = Format(0, "###,###,###.00")
          If nTipoEdicDet <> 1 Then
              CargarCurrentDetalle() 'Si la opción seleccionada no fué Nuevo, entonces cargar los datos del documento actual.
          End If
          If nTipoEdicDet = 3 Then 'Si la opción seleccionada fué Eliminar, entonces desactiva el panel que contiene los controles.
              Panel2.Enabled = False
          End If
          txtCuenta.Focus()
    End Sub

    Sub CargarCurrentDetalle()

    End Sub

    Sub CargarCuentas()
        cbxCuenta.Properties.Items.Clear()
        cbxCuenta.Properties.Items.Add("")
        For Each dr As DataRow In SQL("SELECT Cuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=1 order by Cuenta", "tblCuentas", My.Settings.SolIndustrialCNX).Tables(0).Rows
          cbxCuenta.Properties.Items.Add(dr.Item("Nombre_Cuenta"))
        Next
        cbxCuenta.SelectedIndex = 0
    End Sub

    Sub CargarSCuentas()
        If cbxCuenta.Text <> "" Then
            cbxSCuenta.Properties.Items.Clear()
            cbxSCuenta.Properties.Items.Add("")
            For Each dr As DataRow In SQL(String.Format("SELECT SCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=2 and Cuenta={0} order by SCuenta", GetNumCuenta(cbxCuenta.Text, 1)), "tblSCuenta", My.Settings.SolIndustrialCNX).Tables(0).Rows
              cbxSCuenta.Properties.Items.Add(dr.Item("Nombre_Cuenta"))
            Next
            cbxSCuenta.SelectedIndex = 0
        Else
            cbxSCuenta.Properties.Items.Clear()
            cbxSCuenta.Properties.Items.Add("")
            cbxSCuenta.SelectedIndex = 0
        End If
    End Sub

    Sub CargarSSCuentas()
        If cbxSCuenta.Text <> "" Then
            cbxSSCuenta.Properties.Items.Clear()
            cbxSSCuenta.Properties.Items.Add("")
            For Each dr As DataRow In SQL(String.Format("SELECT SSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=3 and SCuenta={0} and Cuenta={1} order by SSCuenta", GetNumCuenta(cbxSCuenta.Text, 2), GetNumCuenta(cbxCuenta.Text, 1)), "tblSSCuenta", My.Settings.SolIndustrialCNX).Tables(0).Rows
              cbxSSCuenta.Properties.Items.Add(dr.Item("Nombre_Cuenta"))
            Next
            cbxSSCuenta.SelectedIndex = 0
        Else
            cbxSSCuenta.Properties.Items.Clear()
            cbxSSCuenta.Properties.Items.Add("")
            cbxSSCuenta.SelectedIndex = 0
        End If
    End Sub

    Sub CargarSSSCuentas()
        If cbxSSCuenta.Text <> "" Then
            cbxSSSCuenta.Properties.Items.Clear()
            cbxSSSCuenta.Properties.Items.Add("")
            For Each dr As DataRow In SQL(String.Format("SELECT SSSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=4 and SSCuenta={0} and SCuenta={1} and Cuenta={2} order by SSSCuenta", GetNumCuenta(cbxSSCuenta.Text, 3), GetNumCuenta(cbxSCuenta.Text, 2), GetNumCuenta(cbxCuenta.Text, 1)), "tblSSSCuenta", My.Settings.SolIndustrialCNX).Tables(0).Rows
              cbxSSSCuenta.Properties.Items.Add(dr.Item("Nombre_Cuenta"))
            Next
            cbxSSSCuenta.SelectedIndex = 0
        Else
            cbxSSSCuenta.Properties.Items.Clear()
            cbxSSSCuenta.Properties.Items.Add("")
            cbxSSSCuenta.SelectedIndex = 0
        End If
    End Sub

    Sub CargarSSSSCuentas()
        If cbxSSSCuenta.Text <> "" Then
            cbxSSSSCuenta.Properties.Items.Clear()
            cbxSSSSCuenta.Properties.Items.Add("")
            For Each dr As DataRow In SQL(String.Format("SELECT SSSSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=5 and SSSCuenta={0} and SSCuenta={1} and SCuenta={2} and Cuenta={3} order by SSSSCuenta", GetNumCuenta(cbxSSSCuenta.Text, 4), GetNumCuenta(cbxSSCuenta.Text, 3), GetNumCuenta(cbxSCuenta.Text, 2), GetNumCuenta(cbxCuenta.Text, 1)), "tblSSSCuenta", My.Settings.SolIndustrialCNX).Tables(0).Rows
              cbxSSSSCuenta.Properties.Items.Add(dr.Item("Nombre_Cuenta"))
            Next
            cbxSSSSCuenta.SelectedIndex = 0
        Else
            cbxSSSSCuenta.Properties.Items.Clear()
            cbxSSSSCuenta.Properties.Items.Add("")
            cbxSSSSCuenta.SelectedIndex = 0
        End If
    End Sub

    Sub CargarSSSSSCuentas()
        If cbxSSSSCuenta.Text <> "" Then
            cbxSSSSSCuenta.Properties.Items.Clear()
            cbxSSSSSCuenta.Properties.Items.Add("")
            For Each dr As DataRow In SQL(String.Format("SELECT SSSSSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=6 and SSSSCuenta={0} and SSSCuenta={1} and SSCuenta={2} and SCuenta={3} and Cuenta={4} order by SSSSSCuenta", GetNumCuenta(cbxSSSSCuenta.Text, 5), GetNumCuenta(cbxSSSCuenta.Text, 4), GetNumCuenta(cbxSSCuenta.Text, 3), GetNumCuenta(cbxSCuenta.Text, 2), GetNumCuenta(cbxCuenta.Text, 1)), "tblSSSSCuenta", My.Settings.SolIndustrialCNX).Tables(0).Rows
              cbxSSSSSCuenta.Properties.Items.Add(dr.Item("Nombre_Cuenta"))
            Next
            cbxSSSSSCuenta.SelectedIndex = 0
        Else
            cbxSSSSSCuenta.Properties.Items.Clear()
            cbxSSSSSCuenta.Properties.Items.Add("")
            cbxSSSSSCuenta.SelectedIndex = 0
        End If
    End Sub

    'Private Sub luCuenta_EditValueChanged(sender As Object, e As EventArgs) Handles luCuenta.EditValueChanged
    '    lblDescripcion.Text = luCuenta.Text
    '    txtNumCuentaCont.Text = ""
    '    txtNumCuentaCont.Text = luCuenta.EditValue '& luSCuenta.EditValue & luSSCuenta.EditValue & Strings.Right("00" & luSSSCuenta.EditValue, 2) & Strings.Right("000" & luSSSSCuenta.EditValue, 3) & Strings.Right("000" & luSSSSSCuenta.EditValue, 3)
    '    luSCuenta.Enabled = True
    '    tblSCuentaBS.DataSource = SQL("SELECT SCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=2 and Cuenta=" & luCuenta.EditValue & " order by SCuenta", "tblSCuenta", My.Settings.SolIndustrialCNX).Tables(0)
    '    luSCuenta.Properties.DataSource = tblSCuentaBS
    '    luSCuenta.Properties.DisplayMember = "Nombre_Cuenta"
    '    luSCuenta.Properties.ValueMember = "SCuenta"
    '    luSCuenta.Properties.AllowNullInput = True

    'End Sub

    'Private Sub luSCuenta_EditValueChanged(sender As Object, e As EventArgs) Handles luSCuenta.EditValueChanged
    '    lblDescripcion.Text = luSCuenta.Text
    '    txtNumCuentaCont.Text = luCuenta.EditValue & luSCuenta.EditValue '& luSSCuenta.EditValue & Strings.Right("00" & luSSSCuenta.EditValue, 2) & Strings.Right("000" & luSSSSCuenta.EditValue, 3) & Strings.Right("000" & luSSSSSCuenta.EditValue, 3)
    '    luSSCuenta.Enabled = True
    '    tblSSCuentaBS.DataSource = SQL("SELECT SSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=3 and SCuenta=" & luSCuenta.EditValue & " and Cuenta=" & luCuenta.EditValue & " order by SSCuenta", "tblSSCuenta", My.Settings.SolIndustrialCNX).Tables(0)
    '    luSSCuenta.Properties.DataSource = tblSSCuentaBS
    '    luSSCuenta.Properties.DisplayMember = "Nombre_Cuenta"
    '    luSSCuenta.Properties.ValueMember = "SSCuenta"
    '    luSSCuenta.ItemIndex = -1
    'End Sub

    'Private Sub luSSCuenta_EditValueChanged(sender As Object, e As EventArgs) Handles luSSCuenta.EditValueChanged
    '    lblDescripcion.Text = luSSCuenta.Text
    '    txtNumCuentaCont.Text = luCuenta.EditValue & luSCuenta.EditValue & luSSCuenta.EditValue '& Strings.Right("00" & luSSSCuenta.EditValue, 2) & Strings.Right("000" & luSSSSCuenta.EditValue, 3) & Strings.Right("000" & luSSSSSCuenta.EditValue, 3)
    '    luSSSCuenta.Enabled = True
    '    tblSSSCuentaBS.DataSource = SQL("SELECT SSSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=4 and SSCuenta=" & luSSCuenta.EditValue & " and SCuenta=" & luSCuenta.EditValue & " and Cuenta=" & luCuenta.EditValue & " order by SSSCuenta", "tblSSSCuenta", My.Settings.SolIndustrialCNX).Tables(0)
    '    luSSSCuenta.Properties.DataSource = tblSSSCuentaBS
    '    luSSSCuenta.Properties.DisplayMember = "Nombre_Cuenta"
    '    luSSSCuenta.Properties.ValueMember = "SSSCuenta"
    '    luSSSCuenta.ItemIndex = -1
    'End Sub

    'Private Sub luSSSCuenta_EditValueChanged(sender As Object, e As EventArgs) Handles luSSSCuenta.EditValueChanged
    '    lblDescripcion.Text = luSSSCuenta.Text
    '    txtNumCuentaCont.Text = luCuenta.EditValue & luSCuenta.EditValue & luSSCuenta.EditValue & Strings.Right("00" & luSSSCuenta.EditValue, 2) '& Strings.Right("000" & luSSSSCuenta.EditValue, 3) & Strings.Right("000" & luSSSSSCuenta.EditValue, 3)
    '    luSSSSCuenta.Enabled = True
    '    tblSSSSCuentaBS.DataSource = SQL("SELECT SSSSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=5 and SSSCuenta=" & luSSSCuenta.EditValue & " and SSCuenta=" & luSSCuenta.EditValue & " and SCuenta=" & luSCuenta.EditValue & " and Cuenta=" & luCuenta.EditValue & " order by SSSSCuenta", "tblSSSCuenta", My.Settings.SolIndustrialCNX).Tables(0)
    '    luSSSSCuenta.Properties.DataSource = tblSSSSCuentaBS
    '    luSSSSCuenta.Properties.DisplayMember = "Nombre_Cuenta"
    '    luSSSSCuenta.Properties.ValueMember = "SSSSCuenta"
    '    luSSSSCuenta.ItemIndex = -1
    'End Sub

    'Private Sub luSSSSCuenta_EditValueChanged(sender As Object, e As EventArgs) Handles luSSSSCuenta.EditValueChanged
    '    lblDescripcion.Text = luSSSSCuenta.Text
    '    txtNumCuentaCont.Text = luCuenta.EditValue & luSCuenta.EditValue & luSSCuenta.EditValue & Strings.Right("00" & luSSSCuenta.EditValue, 2) & "-" & Strings.Right("000" & luSSSSCuenta.EditValue, 3) '& Strings.Right("000" & luSSSSSCuenta.EditValue, 3)
    '    luSSSSSCuenta.Enabled = True
    '    tblSSSSSCuentaBS.DataSource = SQL("SELECT SSSSSCuenta,Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=6 and SSSSCuenta=" & luSSSSCuenta.EditValue & " and SSSCuenta=" & luSSSCuenta.EditValue & " and SSCuenta=" & luSSCuenta.EditValue & " and SCuenta=" & luSCuenta.EditValue & " and Cuenta=" & luCuenta.EditValue & " order by SSSSSCuenta", "tblSSSSCuenta", My.Settings.SolIndustrialCNX).Tables(0)
    '    luSSSSSCuenta.Properties.DataSource = tblSSSSSCuentaBS
    '    luSSSSSCuenta.Properties.DisplayMember = "Nombre_Cuenta"
    '    luSSSSSCuenta.Properties.ValueMember = "SSSSSCuenta"
    '    luSSSSSCuenta.ItemIndex = -1
    'End Sub

    'Private Sub luSSSSSCuenta_EditValueChanged(sender As Object, e As EventArgs) Handles luSSSSSCuenta.EditValueChanged
    '    lblDescripcion.Text = luSSSSSCuenta.Text
    '    txtNumCuentaCont.Text = luCuenta.EditValue & luSCuenta.EditValue & luSSCuenta.EditValue & Strings.Right("00" & luSSSCuenta.EditValue, 2) & "-" & Strings.Right("000" & luSSSSCuenta.EditValue, 3) & "-" & Strings.Right("000" & luSSSSSCuenta.EditValue, 3)
    '    luSSSSSCuenta.Enabled = True
    'End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    'Private Sub txtNumCuentaCont_TextChanged(sender As Object, e As EventArgs) Handles txtNumCuentaCont.TextChanged

    '      If txtNumCuentaCont.Text = "-" Or txtNumCuentaCont.Text = "--" Or txtNumCuentaCont.Text = "00" Or txtNumCuentaCont.Text = "00-000" Then
    '          txtNumCuentaCont.Text = ""
    '      End If

    '      Dim LenStr As Integer = Strings.Len(txtNumCuentaCont.Text)
    '      Dim NextNum As String

    '      If LenStr = 6 Then 'And LenStr <= 9
    '          NextNum = Strings.Right(txtNumCuentaCont.Text, 1)
    '          txtNumCuentaCont.Text = String.Format("{0}{1}{2}{3}-{4}", GetNumCuenta(cbxCuenta.Text, 1), GetNumCuenta(cbxSCuenta.Text, 2), GetNumCuenta(cbxSSCuenta.Text, 3), Strings.Right("00" & GetNumCuenta(cbxSSSCuenta.Text, 4), 2), IIf(NextNum = "-", "", Strings.Right("000" & GetNumCuenta(cbxSSSSCuenta.Text, 5), 3) & NextNum))
    '          txtNumCuentaCont.SelectionStart = 7
    '      ElseIf LenStr = 10 Then
    '          NextNum = Strings.Right(txtNumCuentaCont.Text, 1)
    '          txtNumCuentaCont.Text = String.Format("{0}{1}{2}{3}-{4}-{5}", GetNumCuenta(cbxCuenta.Text, 1), GetNumCuenta(cbxSCuenta.Text, 2), GetNumCuenta(cbxSSCuenta.Text, 3), Strings.Right("00" & GetNumCuenta(cbxSSSCuenta.Text, 4), 2), Strings.Right("000" & GetNumCuenta(cbxSSSSCuenta.Text, 5), 3), IIf(NextNum = "-", "", Strings.Right("000" & GetNumCuenta(cbxSSSSSCuenta.Text, 5), 3) & NextNum))
    '          txtNumCuentaCont.SelectionStart = 11
    '      End If
    '      If LenStr = 1 Then
    '            cbxCuenta.Text = GetNombreCuenta(txtNumCuentaCont.Text, 1)
    '            txtNumCuentaCont.SelectionStart = 1
    '            cbxSCuenta.SelectedIndex = 0
    '      ElseIf LenStr = 2 Then
    '            cbxCuenta.Text = GetNombreCuenta(txtNumCuentaCont.Text, 1)
    '            cbxSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 2), 2), 1)
    '            cbxSSCuenta.SelectedIndex = 0
    '            txtNumCuentaCont.SelectionStart = 2
    '      ElseIf LenStr = 3 Then
    '            cbxCuenta.Text = GetNombreCuenta(txtNumCuentaCont.Text, 1)
    '            cbxSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 2), 2), 1)
    '            cbxSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 3), 3), 1)
    '            cbxSSSCuenta.SelectedIndex = 0
    '            txtNumCuentaCont.SelectionStart = 3
    '      ElseIf LenStr = 5 Then
    '            cbxCuenta.Text = GetNombreCuenta(txtNumCuentaCont.Text, 1)
    '            cbxSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 2), 2), 1)
    '            cbxSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 3), 3), 1)
    '            cbxSSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 4), 5), 2)
    '            cbxSSSSCuenta.SelectedIndex = 0
    '            txtNumCuentaCont.SelectionStart = 5
    '      ElseIf LenStr = 9 Then
    '            cbxCuenta.Text = GetNombreCuenta(txtNumCuentaCont.Text, 1)
    '            cbxSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 2), 2), 1)
    '            cbxSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 3), 3), 1)
    '            cbxSSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 4), 5), 2)
    '            cbxSSSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 5), 9), 3)
    '            cbxSSSSSCuenta.SelectedIndex = 0
    '            txtNumCuentaCont.SelectionStart = 9
    '      ElseIf LenStr = 13 Then
    '            cbxCuenta.Text = GetNombreCuenta(txtNumCuentaCont.Text, 1)
    '            cbxSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 2), 2), 1)
    '            cbxSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 3), 3), 1)
    '            cbxSSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 4), 5), 2)
    '            cbxSSSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 5), 9), 3)
    '            cbxSSSSSCuenta.Text = Strings.Right(Strings.Left(GetNombreCuenta(txtNumCuentaCont.Text, 6), 13), 3)
    '            txtNumCuentaCont.SelectionStart = 13
    '      End If
    'End Sub

    Function GetNombreCuenta(ByVal sNumCuenta As String, ByVal nNivel As Integer)
        Dim strsqlNombreCnt As String = ""
        If sNumCuenta <> "" Then
            If nNivel = 1 Then
                strsqlNombreCnt = String.Format("SELECT Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=1 and Cuenta='{0}' ", sNumCuenta)
            ElseIf nNivel = 2 Then
                strsqlNombreCnt = String.Format("SELECT Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=2 and SCuenta='{0}' ", sNumCuenta)
            ElseIf nNivel = 3 Then
                strsqlNombreCnt = String.Format("SELECT Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=3 and SSCuenta='{0}' ", sNumCuenta)
            ElseIf nNivel = 4 Then
                strsqlNombreCnt = String.Format("SELECT Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=4 and SSSCuenta='{0}' ", sNumCuenta)
            ElseIf nNivel = 5 Then
                strsqlNombreCnt = String.Format("SELECT Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=5 and SSSSCuenta='{0}' ", sNumCuenta)
            ElseIf nNivel = 6 Then
                strsqlNombreCnt = String.Format("SELECT Nombre_Cuenta FROM Tbl_CatalogoCuentas where Nivel=6 and SSSSSCuenta='{0}' ", sNumCuenta)
            End If
            Dim tblNumCuenta As DataTable = SQL(strsqlNombreCnt, "tblNumCuenta", My.Settings.SolIndustrialCNX).Tables(0)
            If tblNumCuenta.Rows.Count <> 0 Then
              GetNombreCuenta = tblNumCuenta.Rows(0).Item(0)
            End If
        End If
    End Function

    'Private Sub txtNumCuentaCont_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumCuentaCont.KeyPress
    '        If Asc(e.KeyChar) = Keys.Back Then
    '              cbxCuenta.SelectedIndex = 0
    '              cbxSCuenta.Properties.Items.Clear()
    '              cbxSSCuenta.Properties.Items.Clear()
    '              cbxSSSCuenta.Properties.Items.Clear()
    '              cbxSSSSCuenta.Properties.Items.Clear()
    '              cbxSSSSSCuenta.Properties.Items.Clear()
    '              txtNumCuentaCont.Text = ""
    '              txtNumCuentaCont.Focus()
    '        End If
    'End Sub

    Private Sub cbxCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxCuenta.SelectedIndexChanged
        lblDescripcion.Text = cbxCuenta.Text
        'txtNumCuentaCont.Text = ""
        txtCuenta.Text = GetNumCuenta(cbxCuenta.Text, 1)
        cbxSCuenta.Enabled = True
        CargarSCuentas()
        'cbxSCuenta.Focus()
    End Sub

    Private Sub cbxSCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSCuenta.SelectedIndexChanged
        lblDescripcion.Text = cbxSCuenta.Text
        'txtNumCuentaCont.Text = ""
        txtCuenta.Text = GetNumCuenta(cbxCuenta.Text, 1)
        txtSCuenta.Text = GetNumCuenta(cbxSCuenta.Text, 2)  'GetNumCuenta(cbxCuenta.Text, 1) & 
        cbxSSCuenta.Enabled = True
        CargarSSCuentas()
        'cbxSSCuenta.Focus()
    End Sub

    Private Sub cbxSSCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSSCuenta.SelectedIndexChanged
        lblDescripcion.Text = cbxSSCuenta.Text
        'txtNumCuentaCont.Text = ""
        txtCuenta.Text = GetNumCuenta(cbxCuenta.Text, 1)
        txtSCuenta.Text = GetNumCuenta(cbxSCuenta.Text, 2)  'GetNumCuenta(cbxCuenta.Text, 1) & 
        txtSSCuenta.Text = GetNumCuenta(cbxSSCuenta.Text, 3) 'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) &
        cbxSSSCuenta.Enabled = True
        CargarSSSCuentas()
        'cbxSSSCuenta.Focus()
    End Sub

    Private Sub cbxSSSCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSSSCuenta.SelectedIndexChanged
        lblDescripcion.Text = cbxSSSCuenta.Text
        'txtNumCuentaCont.Text = ""
        txtCuenta.Text = GetNumCuenta(cbxCuenta.Text, 1)
        txtSCuenta.Text = GetNumCuenta(cbxSCuenta.Text, 2)  'GetNumCuenta(cbxCuenta.Text, 1) & 
        txtSSCuenta.Text = GetNumCuenta(cbxSSCuenta.Text, 3) 'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) &
        txtSSSCuenta.Text = Strings.Right(IIf(cbxSSSCuenta.Text = "", "", "00") & GetNumCuenta(cbxSSSCuenta.Text, 4), 2)   'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) & GetNumCuenta(cbxSSCuenta.Text, 3) &
        cbxSSSSCuenta.Enabled = True
        CargarSSSSCuentas()
        'cbxSSSSCuenta.Focus()
    End Sub

    Private Sub cbxSSSSCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSSSSCuenta.SelectedIndexChanged
        lblDescripcion.Text = cbxSSSSCuenta.Text
        'txtNumCuentaCont.Text = ""
        txtCuenta.Text = GetNumCuenta(cbxCuenta.Text, 1)
        txtSCuenta.Text = GetNumCuenta(cbxSCuenta.Text, 2)  'GetNumCuenta(cbxCuenta.Text, 1) & 
        txtSSCuenta.Text = GetNumCuenta(cbxSSCuenta.Text, 3) 'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) &
        txtSSSCuenta.Text = Strings.Right(IIf(cbxSSSCuenta.Text = "", "", "00") & GetNumCuenta(cbxSSSCuenta.Text, 4), 2)   'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) & GetNumCuenta(cbxSSCuenta.Text, 3) &
        txtSSSSCuenta.Text = String.Format("{0}", Strings.Right(IIf(cbxSSSSCuenta.Text = "", "", "000") & GetNumCuenta(cbxSSSSCuenta.Text, 5), 3))
        cbxSSSSSCuenta.Enabled = True
        CargarSSSSSCuentas()
        'cbxSSSSSCuenta.Focus()
    End Sub

    Private Sub cbxSSSSSCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSSSSSCuenta.SelectedIndexChanged
        lblDescripcion.Text = cbxSSSSSCuenta.Text
        'txtNumCuentaCont.Text = ""
        txtCuenta.Text = GetNumCuenta(cbxCuenta.Text, 1)
        txtSCuenta.Text = GetNumCuenta(cbxSCuenta.Text, 2)  'GetNumCuenta(cbxCuenta.Text, 1) & 
        txtSSCuenta.Text = GetNumCuenta(cbxSSCuenta.Text, 3) 'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) &
        txtSSSCuenta.Text = Strings.Right(IIf(cbxSSSCuenta.Text = "", "", "00") & GetNumCuenta(cbxSSSCuenta.Text, 4), 2)   'GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) & GetNumCuenta(cbxSSCuenta.Text, 3) &
        txtSSSSCuenta.Text = String.Format("{0}", Strings.Right(IIf(cbxSSSSCuenta.Text = "", "", "000") & GetNumCuenta(cbxSSSSCuenta.Text, 5), 3))
        txtSSSSSCuenta.Text = String.Format("{0}", Strings.Right(IIf(cbxSSSSSCuenta.Text = "", "", "000") & GetNumCuenta(cbxSSSSSCuenta.Text, 6), 3))
    End Sub

    'Sub Get_NumCuentasString()
    '      txtNumCuentaCont.Text = GetNumCuenta(cbxCuenta.Text, 1) & GetNumCuenta(cbxSCuenta.Text, 2) & GetNumCuenta(cbxSSCuenta.Text, 3) & Strings.Right("00" & GetNumCuenta(cbxSSSCuenta.Text, 4), 2) & "-" & Strings.Right("000" & GetNumCuenta(cbxSSSSCuenta.Text, 5), 3) & "-" & Strings.Right("000" & GetNumCuenta(cbxSSSSSCuenta.Text, 6), 3)
    'End Sub

    Function GetNumCuenta(ByVal sNomCuenta As String, ByVal nNivel As Integer)
        Dim strsqlNumCnt As String = ""
        If sNomCuenta <> "" Then
            If nNivel = 1 Then
                strsqlNumCnt = String.Format("SELECT Cuenta FROM Tbl_CatalogoCuentas where Nivel=1 and Nombre_Cuenta='{0}' order by Cuenta", sNomCuenta)
            ElseIf nNivel = 2 Then
                strsqlNumCnt = String.Format("SELECT SCuenta FROM Tbl_CatalogoCuentas where Nivel=2 and Nombre_Cuenta='{0}' order by Cuenta", sNomCuenta)
            ElseIf nNivel = 3 Then
                strsqlNumCnt = String.Format("SELECT SSCuenta FROM Tbl_CatalogoCuentas where Nivel=3 and Nombre_Cuenta='{0}' order by Cuenta", sNomCuenta)
            ElseIf nNivel = 4 Then
                strsqlNumCnt = String.Format("SELECT SSSCuenta FROM Tbl_CatalogoCuentas where Nivel=4 and Nombre_Cuenta='{0}' order by Cuenta", sNomCuenta)
            ElseIf nNivel = 5 Then
                strsqlNumCnt = String.Format("SELECT SSSSCuenta FROM Tbl_CatalogoCuentas where Nivel=5 and Nombre_Cuenta='{0}' order by Cuenta", sNomCuenta)
            ElseIf nNivel = 6 Then
                strsqlNumCnt = String.Format("SELECT SSSSSCuenta FROM Tbl_CatalogoCuentas where Nivel=6 and Nombre_Cuenta='{0}' order by Cuenta", sNomCuenta)
            End If
            Dim tblNumCuenta As DataTable = SQL(strsqlNumCnt, "tblNumCuenta", My.Settings.SolIndustrialCNX).Tables(0)
            If tblNumCuenta.Rows.Count <> 0 Then
              GetNumCuenta = tblNumCuenta.Rows(0).Item(0)
            End If
        End If
    End Function

    'Private Sub txtCuenta_TextChanged(sender As Object, e As EventArgs) Handles txtCuenta.TextChanged
    '  'If txtCuenta.Text <> "" And txtCuenta.Text.Length = 1 Then
    '  '  txtSCuenta.Focus()
    '  'End If
    'End Sub

    'Private Sub txtSCuenta_TextChanged(sender As Object, e As EventArgs) Handles txtSCuenta.TextChanged
    '  'If txtSCuenta.Text <> "" And txtSCuenta.Text.Length = 1 Then
    '  '  txtSSCuenta.Focus()
    '  'End If
    'End Sub

    'Private Sub txtSSCuenta_TextChanged(sender As Object, e As EventArgs) Handles txtSSCuenta.TextChanged
    '  'If txtSSCuenta.Text <> "" And txtSSCuenta.Text.Length = 1 Then
    '  '  txtSSSCuenta.Focus()
    '  'End If
    'End Sub

    'Private Sub txtSSSCuenta_TextChanged(sender As Object, e As EventArgs) Handles txtSSSCuenta.TextChanged
    '  'If txtSSSCuenta.Text <> "" And txtSSSCuenta.Text.Length = 2 Then
    '  '  txtSSSSCuenta.Focus()
    '  'End If
    'End Sub

    Private Sub txtSSSSCuenta_TextChanged(sender As Object, e As EventArgs) Handles txtSSSSCuenta.TextChanged
      If txtSSSSCuenta.Text <> "" And txtSSSSCuenta.Text.Length >= 1 Then
        LblGuion1.Visible = True
      Else
        LblGuion1.Visible = False
      End If
    End Sub

    Private Sub txtSSSSSCuenta_TextChanged(sender As Object, e As EventArgs) Handles txtSSSSSCuenta.TextChanged
      If txtSSSSSCuenta.Text <> "" And txtSSSSSCuenta.Text.Length >= 1 Then
        LblGuion2.Visible = True
      Else
        LblGuion2.Visible = False
      End If
    End Sub

    Private Sub txtCuenta_Leave(sender As Object, e As EventArgs) Handles txtCuenta.Leave
      If txtCuenta.Text = "" Then
          cbxCuenta.Focus()
      ElseIf txtCuenta.Text <> "" Then
          txtSCuenta.Focus()
      End If
    End Sub

    Private Sub txtCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCuenta.KeyPress
        If [Char].IsNumber(e.KeyChar) Then
            If e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c Then
                KeyPressed = e.KeyChar
            End If
        ElseIf Not [Char].IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtCuenta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtCuenta.KeyUp
        If e.KeyCode >= 96 And e.KeyCode <= 105 Then
            cbxCuenta.Text = GetNombreCuenta(txtCuenta.Text, 1)
        Else
            cbxCuenta.Text = ""
        End If
    End Sub

    Private Sub txtSCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSCuenta.KeyPress
        If [Char].IsNumber(e.KeyChar) Then
            If e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c Then
                KeyPressed = e.KeyChar
            End If
        ElseIf Not [Char].IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtSCuenta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSCuenta.KeyUp
        If e.KeyCode >= 96 And e.KeyCode <= 105 Then
            cbxSCuenta.Text = GetNombreCuenta(txtSCuenta.Text, 2)
        Else
            cbxSCuenta.Text = ""
        End If
    End Sub

    Private Sub txtSSCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSSCuenta.KeyPress
        If [Char].IsNumber(e.KeyChar) Then
            If e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c Then
                KeyPressed = e.KeyChar
            End If
        ElseIf Not [Char].IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtSSCuenta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSSCuenta.KeyUp
        If e.KeyCode >= 96 And e.KeyCode <= 105 Then
            cbxSSCuenta.Text = GetNombreCuenta(txtSSCuenta.Text, 3)
        Else
            cbxSSCuenta.Text = ""
        End If
    End Sub

    Private Sub txtSSSCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSSSCuenta.KeyPress
        If [Char].IsNumber(e.KeyChar) Then
            If e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c Then
                KeyPressed = e.KeyChar
            End If
        ElseIf Not [Char].IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtSSSCuenta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSSSCuenta.KeyUp
        If e.KeyCode >= 96 And e.KeyCode <= 105 Then
            cbxSSSCuenta.Text = GetNombreCuenta(txtSSSCuenta.Text, 4)
        Else
            cbxSSSCuenta.Text = ""
            cbxSSSCuenta.Text = ""
        End If
    End Sub

    Private Sub txtSSSSCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSSSSCuenta.KeyPress
        If [Char].IsNumber(e.KeyChar) Then
            If e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c Then
                KeyPressed = e.KeyChar
            End If
        ElseIf Not [Char].IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtSSSSCuenta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSSSSCuenta.KeyUp
        If e.KeyCode >= 96 And e.KeyCode <= 105 Then
            cbxSSSSCuenta.Text = GetNombreCuenta(txtSSSSCuenta.Text, 4)
        Else
            cbxSSSSCuenta.Text = ""
        End If
    End Sub

    Private Sub txtSSSSSCuenta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSSSSSCuenta.KeyPress
        If [Char].IsNumber(e.KeyChar) Then
            If e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c Then
                KeyPressed = e.KeyChar
            End If
        ElseIf Not [Char].IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtSSSSSCuenta_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSSSSSCuenta.KeyUp
        If e.KeyCode >= 96 And e.KeyCode <= 105 Then
            cbxSSSSSCuenta.Text = GetNombreCuenta(txtSSSSSCuenta.Text, 4)
        Else
            cbxSSSSSCuenta.Text = ""
        End If
    End Sub

Private Sub txtSSSSSCuenta_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSSSSSCuenta.KeyDown
    If e.KeyCode = Keys.Back Then
        If txtSSSSSCuenta.Text = "" Then
            txtSSSSCuenta.Focus()
        End If
    End If
End Sub
End Class