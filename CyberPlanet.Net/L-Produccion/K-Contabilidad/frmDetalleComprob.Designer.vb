﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDetalleComprob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDetalleComprob))
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.cmdCancelar = New System.Windows.Forms.Button()
    Me.cmdAceptar = New System.Windows.Forms.Button()
    Me.txtNumCuentaCont = New System.Windows.Forms.TextBox()
    Me.lblDescripcion = New System.Windows.Forms.Label()
    Me.Label7 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.cmbTipo = New System.Windows.Forms.ComboBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.Label8 = New System.Windows.Forms.Label()
    Me.Label9 = New System.Windows.Forms.Label()
    Me.Label10 = New System.Windows.Forms.Label()
    Me.luTasaCambio = New DevExpress.XtraEditors.LookUpEdit()
    Me.Label11 = New System.Windows.Forms.Label()
    Me.txtMonto = New System.Windows.Forms.TextBox()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.LblGuion2 = New DevExpress.XtraEditors.LabelControl()
    Me.LblGuion1 = New DevExpress.XtraEditors.LabelControl()
    Me.txtSSSSCuenta = New System.Windows.Forms.TextBox()
    Me.txtCuenta = New System.Windows.Forms.TextBox()
    Me.txtSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSSSSCuenta = New System.Windows.Forms.TextBox()
    Me.cbxSSSSSCuenta = New DevExpress.XtraEditors.ComboBoxEdit()
    Me.cbxSSSSCuenta = New DevExpress.XtraEditors.ComboBoxEdit()
    Me.cbxSSSCuenta = New DevExpress.XtraEditors.ComboBoxEdit()
    Me.cbxSSCuenta = New DevExpress.XtraEditors.ComboBoxEdit()
    Me.cbxSCuenta = New DevExpress.XtraEditors.ComboBoxEdit()
    Me.cbxCuenta = New DevExpress.XtraEditors.ComboBoxEdit()
    Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
    Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
    Me.luCuenta = New DevExpress.XtraEditors.LookUpEdit()
    Me.tblSSSSSCuentaBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.tblSSSSCuentaBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.tblSSSCuentaBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.tblSSCuentaBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.tblSCuentaBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.tblCuentaBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.luSCuenta = New DevExpress.XtraEditors.LookUpEdit()
    Me.luSSCuenta = New DevExpress.XtraEditors.LookUpEdit()
    Me.Panel1.SuspendLayout()
    CType(Me.luTasaCambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel2.SuspendLayout()
    CType(Me.cbxSSSSSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.cbxSSSSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.cbxSSSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.cbxSSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.cbxSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.cbxCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.luCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tblSSSSSCuentaBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tblSSSSCuentaBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tblSSSCuentaBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tblSSCuentaBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tblSCuentaBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tblCuentaBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.luSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.luSSCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.luSSCuenta)
    Me.Panel1.Controls.Add(Me.luSCuenta)
    Me.Panel1.Controls.Add(Me.luCuenta)
    Me.Panel1.Controls.Add(Me.cmdCancelar)
    Me.Panel1.Controls.Add(Me.cmdAceptar)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 141)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(662, 49)
    Me.Panel1.TabIndex = 0
    '
    'cmdCancelar
    '
    Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
    Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdCancelar.Location = New System.Drawing.Point(560, 6)
    Me.cmdCancelar.Name = "cmdCancelar"
    Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
    Me.cmdCancelar.TabIndex = 1
    Me.cmdCancelar.Text = "&Cancelar"
    Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdCancelar.UseVisualStyleBackColor = True
    '
    'cmdAceptar
    '
    Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
    Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdAceptar.Location = New System.Drawing.Point(476, 6)
    Me.cmdAceptar.Name = "cmdAceptar"
    Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
    Me.cmdAceptar.TabIndex = 0
    Me.cmdAceptar.Text = "&Aceptar"
    Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.cmdAceptar.UseVisualStyleBackColor = True
    '
    'txtNumCuentaCont
    '
    Me.txtNumCuentaCont.Location = New System.Drawing.Point(336, 16)
    Me.txtNumCuentaCont.Name = "txtNumCuentaCont"
    Me.txtNumCuentaCont.Size = New System.Drawing.Size(140, 20)
    Me.txtNumCuentaCont.TabIndex = 0
    Me.txtNumCuentaCont.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'lblDescripcion
    '
    Me.lblDescripcion.BackColor = System.Drawing.SystemColors.InactiveBorder
    Me.lblDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblDescripcion.Location = New System.Drawing.Point(94, 86)
    Me.lblDescripcion.Name = "lblDescripcion"
    Me.lblDescripcion.Size = New System.Drawing.Size(544, 20)
    Me.lblDescripcion.TabIndex = 13
    '
    'Label7
    '
    Me.Label7.AutoSize = True
    Me.Label7.Location = New System.Drawing.Point(501, 111)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(40, 13)
    Me.Label7.TabIndex = 9
    Me.Label7.Text = "Monto:"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(22, 87)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(66, 13)
    Me.Label2.TabIndex = 1
    Me.Label2.Text = "Descripción:"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(22, 18)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(89, 13)
    Me.Label1.TabIndex = 0
    Me.Label1.Text = "Cuenta Contable:"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label6.Location = New System.Drawing.Point(23, 48)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(38, 12)
    Me.Label6.TabIndex = 15
    Me.Label6.Text = "Cuenta:"
    '
    'cmbTipo
    '
    Me.cmbTipo.FormattingEnabled = True
    Me.cmbTipo.Items.AddRange(New Object() {"D", "C"})
    Me.cmbTipo.Location = New System.Drawing.Point(300, 108)
    Me.cmbTipo.Name = "cmbTipo"
    Me.cmbTipo.Size = New System.Drawing.Size(46, 21)
    Me.cmbTipo.TabIndex = 14
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(126, 48)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(44, 12)
    Me.Label3.TabIndex = 19
    Me.Label3.Text = "SCuenta:"
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(229, 48)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(50, 12)
    Me.Label4.TabIndex = 21
    Me.Label4.Text = "SSCuenta:"
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(334, 48)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(56, 12)
    Me.Label5.TabIndex = 23
    Me.Label5.Text = "SSSCuenta:"
    '
    'Label8
    '
    Me.Label8.AutoSize = True
    Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label8.Location = New System.Drawing.Point(438, 49)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(62, 12)
    Me.Label8.TabIndex = 25
    Me.Label8.Text = "SSSSCuenta:"
    '
    'Label9
    '
    Me.Label9.AutoSize = True
    Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label9.Location = New System.Drawing.Point(539, 48)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(68, 12)
    Me.Label9.TabIndex = 27
    Me.Label9.Text = "SSSSSCuenta:"
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.Location = New System.Drawing.Point(263, 111)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(31, 13)
    Me.Label10.TabIndex = 29
    Me.Label10.Text = "D/H:"
    '
    'luTasaCambio
    '
    Me.luTasaCambio.Location = New System.Drawing.Point(389, 108)
    Me.luTasaCambio.Name = "luTasaCambio"
    Me.luTasaCambio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luTasaCambio.Size = New System.Drawing.Size(92, 20)
    Me.luTasaCambio.TabIndex = 15
    '
    'Label11
    '
    Me.Label11.AutoSize = True
    Me.Label11.Location = New System.Drawing.Point(358, 111)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(29, 13)
    Me.Label11.TabIndex = 31
    Me.Label11.Text = "T/C:"
    '
    'txtMonto
    '
    Me.txtMonto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.txtMonto.Location = New System.Drawing.Point(540, 109)
    Me.txtMonto.Name = "txtMonto"
    Me.txtMonto.Size = New System.Drawing.Size(98, 20)
    Me.txtMonto.TabIndex = 16
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.txtNumCuentaCont)
    Me.Panel2.Controls.Add(Me.LblGuion2)
    Me.Panel2.Controls.Add(Me.LblGuion1)
    Me.Panel2.Controls.Add(Me.txtSSSSCuenta)
    Me.Panel2.Controls.Add(Me.txtCuenta)
    Me.Panel2.Controls.Add(Me.txtSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSSSSCuenta)
    Me.Panel2.Controls.Add(Me.cbxSSSSSCuenta)
    Me.Panel2.Controls.Add(Me.cbxSSSSCuenta)
    Me.Panel2.Controls.Add(Me.cbxSSSCuenta)
    Me.Panel2.Controls.Add(Me.cbxSSCuenta)
    Me.Panel2.Controls.Add(Me.cbxSCuenta)
    Me.Panel2.Controls.Add(Me.cbxCuenta)
    Me.Panel2.Controls.Add(Me.txtMonto)
    Me.Panel2.Controls.Add(Me.Label11)
    Me.Panel2.Controls.Add(Me.luTasaCambio)
    Me.Panel2.Controls.Add(Me.Label10)
    Me.Panel2.Controls.Add(Me.Label9)
    Me.Panel2.Controls.Add(Me.Label8)
    Me.Panel2.Controls.Add(Me.Label5)
    Me.Panel2.Controls.Add(Me.Label4)
    Me.Panel2.Controls.Add(Me.Label3)
    Me.Panel2.Controls.Add(Me.cmbTipo)
    Me.Panel2.Controls.Add(Me.Label6)
    Me.Panel2.Controls.Add(Me.Label1)
    Me.Panel2.Controls.Add(Me.Label2)
    Me.Panel2.Controls.Add(Me.Label7)
    Me.Panel2.Controls.Add(Me.lblDescripcion)
    Me.Panel2.Controls.Add(Me.ShapeContainer1)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel2.Location = New System.Drawing.Point(0, 0)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(662, 141)
    Me.Panel2.TabIndex = 0
    '
    'LblGuion2
    '
    Me.LblGuion2.Appearance.BackColor = System.Drawing.Color.White
    Me.LblGuion2.Appearance.Font = New System.Drawing.Font("Tahoma", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblGuion2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
    Me.LblGuion2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
    Me.LblGuion2.Location = New System.Drawing.Point(205, 14)
    Me.LblGuion2.Name = "LblGuion2"
    Me.LblGuion2.Size = New System.Drawing.Size(7, 18)
    Me.LblGuion2.TabIndex = 52
    Me.LblGuion2.Text = "-"
    Me.LblGuion2.Visible = False
    '
    'LblGuion1
    '
    Me.LblGuion1.Appearance.BackColor = System.Drawing.Color.White
    Me.LblGuion1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblGuion1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
    Me.LblGuion1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
    Me.LblGuion1.Location = New System.Drawing.Point(171, 14)
    Me.LblGuion1.Name = "LblGuion1"
    Me.LblGuion1.Size = New System.Drawing.Size(7, 18)
    Me.LblGuion1.TabIndex = 51
    Me.LblGuion1.Text = "-"
    Me.LblGuion1.Visible = False
    '
    'txtSSSSCuenta
    '
    Me.txtSSSSCuenta.BackColor = System.Drawing.Color.White
    Me.txtSSSSCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtSSSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSSSCuenta.Location = New System.Drawing.Point(179, 18)
    Me.txtSSSSCuenta.Name = "txtSSSSCuenta"
    Me.txtSSSSCuenta.Size = New System.Drawing.Size(27, 13)
    Me.txtSSSSCuenta.TabIndex = 4
    Me.txtSSSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtCuenta
    '
    Me.txtCuenta.BackColor = System.Drawing.Color.White
    Me.txtCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtCuenta.Location = New System.Drawing.Point(121, 18)
    Me.txtCuenta.Name = "txtCuenta"
    Me.txtCuenta.Size = New System.Drawing.Size(13, 13)
    Me.txtCuenta.TabIndex = 0
    Me.txtCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSCuenta
    '
    Me.txtSCuenta.BackColor = System.Drawing.Color.White
    Me.txtSCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSCuenta.Location = New System.Drawing.Point(130, 18)
    Me.txtSCuenta.Name = "txtSCuenta"
    Me.txtSCuenta.Size = New System.Drawing.Size(13, 13)
    Me.txtSCuenta.TabIndex = 1
    Me.txtSCuenta.Tag = ""
    Me.txtSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSCuenta
    '
    Me.txtSSCuenta.BackColor = System.Drawing.Color.White
    Me.txtSSCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSCuenta.Location = New System.Drawing.Point(140, 18)
    Me.txtSSCuenta.Name = "txtSSCuenta"
    Me.txtSSCuenta.Size = New System.Drawing.Size(13, 13)
    Me.txtSSCuenta.TabIndex = 2
    Me.txtSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSSCuenta
    '
    Me.txtSSSCuenta.BackColor = System.Drawing.Color.White
    Me.txtSSSCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtSSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSSCuenta.Location = New System.Drawing.Point(149, 18)
    Me.txtSSSCuenta.Name = "txtSSSCuenta"
    Me.txtSSSCuenta.Size = New System.Drawing.Size(22, 13)
    Me.txtSSSCuenta.TabIndex = 3
    Me.txtSSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSSSSCuenta
    '
    Me.txtSSSSSCuenta.BackColor = System.Drawing.Color.White
    Me.txtSSSSSCuenta.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtSSSSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSSSSCuenta.Location = New System.Drawing.Point(213, 18)
    Me.txtSSSSSCuenta.Name = "txtSSSSSCuenta"
    Me.txtSSSSSCuenta.Size = New System.Drawing.Size(27, 13)
    Me.txtSSSSSCuenta.TabIndex = 6
    Me.txtSSSSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'cbxSSSSSCuenta
    '
    Me.cbxSSSSSCuenta.Location = New System.Drawing.Point(541, 62)
    Me.cbxSSSSSCuenta.Name = "cbxSSSSSCuenta"
    Me.cbxSSSSSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.cbxSSSSSCuenta.Size = New System.Drawing.Size(97, 20)
    Me.cbxSSSSSCuenta.TabIndex = 12
    '
    'cbxSSSSCuenta
    '
    Me.cbxSSSSCuenta.Location = New System.Drawing.Point(438, 62)
    Me.cbxSSSSCuenta.Name = "cbxSSSSCuenta"
    Me.cbxSSSSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.cbxSSSSCuenta.Size = New System.Drawing.Size(97, 20)
    Me.cbxSSSSCuenta.TabIndex = 11
    '
    'cbxSSSCuenta
    '
    Me.cbxSSSCuenta.Location = New System.Drawing.Point(334, 62)
    Me.cbxSSSCuenta.Name = "cbxSSSCuenta"
    Me.cbxSSSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.cbxSSSCuenta.Size = New System.Drawing.Size(97, 20)
    Me.cbxSSSCuenta.TabIndex = 10
    '
    'cbxSSCuenta
    '
    Me.cbxSSCuenta.Location = New System.Drawing.Point(231, 62)
    Me.cbxSSCuenta.Name = "cbxSSCuenta"
    Me.cbxSSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.cbxSSCuenta.Size = New System.Drawing.Size(97, 20)
    Me.cbxSSCuenta.TabIndex = 9
    '
    'cbxSCuenta
    '
    Me.cbxSCuenta.Location = New System.Drawing.Point(128, 63)
    Me.cbxSCuenta.Name = "cbxSCuenta"
    Me.cbxSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.cbxSCuenta.Size = New System.Drawing.Size(97, 20)
    Me.cbxSCuenta.TabIndex = 8
    '
    'cbxCuenta
    '
    Me.cbxCuenta.Location = New System.Drawing.Point(25, 63)
    Me.cbxCuenta.Name = "cbxCuenta"
    Me.cbxCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.cbxCuenta.Size = New System.Drawing.Size(97, 20)
    Me.cbxCuenta.TabIndex = 7
    '
    'ShapeContainer1
    '
    Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
    Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
    Me.ShapeContainer1.Name = "ShapeContainer1"
    Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape1})
    Me.ShapeContainer1.Size = New System.Drawing.Size(662, 141)
    Me.ShapeContainer1.TabIndex = 49
    Me.ShapeContainer1.TabStop = False
    '
    'RectangleShape1
    '
    Me.RectangleShape1.BackColor = System.Drawing.Color.White
    Me.RectangleShape1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
    Me.RectangleShape1.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
    Me.RectangleShape1.Location = New System.Drawing.Point(118, 13)
    Me.RectangleShape1.Name = "RectangleShape1"
    Me.RectangleShape1.Size = New System.Drawing.Size(123, 20)
    '
    'luCuenta
    '
    Me.luCuenta.Location = New System.Drawing.Point(25, 17)
    Me.luCuenta.Name = "luCuenta"
    Me.luCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luCuenta.Properties.DataSource = Me.tblCuentaBS
    Me.luCuenta.Size = New System.Drawing.Size(77, 20)
    Me.luCuenta.TabIndex = 2
    '
    'luSCuenta
    '
    Me.luSCuenta.Location = New System.Drawing.Point(108, 17)
    Me.luSCuenta.Name = "luSCuenta"
    Me.luSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luSCuenta.Properties.DataSource = Me.tblSCuentaBS
    Me.luSCuenta.Size = New System.Drawing.Size(77, 20)
    Me.luSCuenta.TabIndex = 3
    '
    'luSSCuenta
    '
    Me.luSSCuenta.Location = New System.Drawing.Point(191, 17)
    Me.luSSCuenta.Name = "luSSCuenta"
    Me.luSSCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luSSCuenta.Properties.DataSource = Me.tblSSCuentaBS
    Me.luSSCuenta.Size = New System.Drawing.Size(77, 20)
    Me.luSSCuenta.TabIndex = 4
    '
    'frmDetalleComprob
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(662, 190)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Panel1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmDetalleComprob"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Detalle Comprobante"
    Me.Panel1.ResumeLayout(False)
    CType(Me.luTasaCambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.cbxSSSSSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.cbxSSSSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.cbxSSSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.cbxSSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.cbxSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.cbxCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.luCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tblSSSSSCuentaBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tblSSSSCuentaBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tblSSSCuentaBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tblSSCuentaBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tblSCuentaBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tblCuentaBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.luSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.luSSCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents tblCuentaBS As System.Windows.Forms.BindingSource
    Friend WithEvents tblSCuentaBS As System.Windows.Forms.BindingSource
    Friend WithEvents tblSSCuentaBS As System.Windows.Forms.BindingSource
    Friend WithEvents tblSSSCuentaBS As System.Windows.Forms.BindingSource
    Friend WithEvents tblSSSSCuentaBS As System.Windows.Forms.BindingSource
    Friend WithEvents tblSSSSSCuentaBS As System.Windows.Forms.BindingSource
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents luTasaCambio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents txtNumCuentaCont As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cbxSSSSSCuenta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbxSSSSCuenta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbxSSSCuenta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbxSSCuenta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbxSCuenta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbxCuenta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtCuenta As System.Windows.Forms.TextBox
    Friend WithEvents txtSCuenta As System.Windows.Forms.TextBox
    Friend WithEvents txtSSCuenta As System.Windows.Forms.TextBox
    Friend WithEvents txtSSSCuenta As System.Windows.Forms.TextBox
    Friend WithEvents txtSSSSCuenta As System.Windows.Forms.TextBox
    Friend WithEvents txtSSSSSCuenta As System.Windows.Forms.TextBox
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents LblGuion2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LblGuion1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents luCuenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luSSCuenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents luSCuenta As DevExpress.XtraEditors.LookUpEdit
End Class
