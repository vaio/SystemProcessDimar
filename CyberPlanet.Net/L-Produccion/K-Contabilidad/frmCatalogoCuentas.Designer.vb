﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCatalogoCuentas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.LblTitulo = New System.Windows.Forms.Label()
    Me.TblCatalogoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.txtCuentaEdit = New System.Windows.Forms.TextBox()
    Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
    Me.cmbNivel = New System.Windows.Forms.ComboBox()
    Me.chkEsDeshabilitada = New System.Windows.Forms.CheckBox()
    Me.chkEsDetalle = New System.Windows.Forms.CheckBox()
    Me.cmbTipoSaldo = New System.Windows.Forms.ComboBox()
    Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
    Me.cmbTipoCuenta = New System.Windows.Forms.ComboBox()
    Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
    Me.txtDescripcionCuenta = New System.Windows.Forms.TextBox()
    Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
    Me.txtNombreCuenta = New System.Windows.Forms.TextBox()
    Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.txtCuentaContable = New System.Windows.Forms.MaskedTextBox()
    Me.txtNivel = New System.Windows.Forms.TextBox()
    Me.txtCuenta = New System.Windows.Forms.TextBox()
    Me.txtSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSSSCuenta = New System.Windows.Forms.TextBox()
    Me.txtSSSSSCuenta = New System.Windows.Forms.TextBox()
    Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
    Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
    Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
    CType(Me.TblCatalogoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.Panel2.SuspendLayout()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'LblTitulo
    '
    Me.LblTitulo.BackColor = System.Drawing.SystemColors.GradientActiveCaption
    Me.LblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.LblTitulo.Dock = System.Windows.Forms.DockStyle.Top
    Me.LblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTitulo.ForeColor = System.Drawing.SystemColors.Highlight
    Me.LblTitulo.Location = New System.Drawing.Point(0, 0)
    Me.LblTitulo.Name = "LblTitulo"
    Me.LblTitulo.Size = New System.Drawing.Size(1233, 48)
    Me.LblTitulo.TabIndex = 5
    Me.LblTitulo.Text = "Catalogo de Cuentas"
    Me.LblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'TblCatalogoBindingSource
    '
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.txtCuentaEdit)
    Me.Panel1.Controls.Add(Me.LabelControl14)
    Me.Panel1.Controls.Add(Me.LabelControl13)
    Me.Panel1.Controls.Add(Me.cmbNivel)
    Me.Panel1.Controls.Add(Me.chkEsDeshabilitada)
    Me.Panel1.Controls.Add(Me.chkEsDetalle)
    Me.Panel1.Controls.Add(Me.cmbTipoSaldo)
    Me.Panel1.Controls.Add(Me.LabelControl12)
    Me.Panel1.Controls.Add(Me.cmbTipoCuenta)
    Me.Panel1.Controls.Add(Me.LabelControl11)
    Me.Panel1.Controls.Add(Me.txtDescripcionCuenta)
    Me.Panel1.Controls.Add(Me.LabelControl10)
    Me.Panel1.Controls.Add(Me.txtNombreCuenta)
    Me.Panel1.Controls.Add(Me.LabelControl9)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
    Me.Panel1.Location = New System.Drawing.Point(967, 48)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(266, 507)
    Me.Panel1.TabIndex = 1
    '
    'txtCuentaEdit
    '
    Me.txtCuentaEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtCuentaEdit.Location = New System.Drawing.Point(184, 15)
    Me.txtCuentaEdit.Name = "txtCuentaEdit"
    Me.txtCuentaEdit.Size = New System.Drawing.Size(66, 20)
    Me.txtCuentaEdit.TabIndex = 1
    Me.txtCuentaEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'LabelControl14
    '
    Me.LabelControl14.Location = New System.Drawing.Point(129, 17)
    Me.LabelControl14.Name = "LabelControl14"
    Me.LabelControl14.Size = New System.Drawing.Size(39, 13)
    Me.LabelControl14.TabIndex = 29
    Me.LabelControl14.Text = "Cuenta:"
    '
    'LabelControl13
    '
    Me.LabelControl13.Location = New System.Drawing.Point(16, 17)
    Me.LabelControl13.Name = "LabelControl13"
    Me.LabelControl13.Size = New System.Drawing.Size(27, 13)
    Me.LabelControl13.TabIndex = 28
    Me.LabelControl13.Text = "Nivel:"
    '
    'cmbNivel
    '
    Me.cmbNivel.FormattingEnabled = True
    Me.cmbNivel.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6"})
    Me.cmbNivel.Location = New System.Drawing.Point(49, 14)
    Me.cmbNivel.Name = "cmbNivel"
    Me.cmbNivel.Size = New System.Drawing.Size(45, 21)
    Me.cmbNivel.TabIndex = 0
    '
    'chkEsDeshabilitada
    '
    Me.chkEsDeshabilitada.AutoSize = True
    Me.chkEsDeshabilitada.Location = New System.Drawing.Point(14, 379)
    Me.chkEsDeshabilitada.Name = "chkEsDeshabilitada"
    Me.chkEsDeshabilitada.Size = New System.Drawing.Size(127, 17)
    Me.chkEsDeshabilitada.TabIndex = 7
    Me.chkEsDeshabilitada.Text = "Cuenta Deshabilitada"
    Me.chkEsDeshabilitada.UseVisualStyleBackColor = True
    '
    'chkEsDetalle
    '
    Me.chkEsDetalle.AutoSize = True
    Me.chkEsDetalle.Location = New System.Drawing.Point(15, 356)
    Me.chkEsDetalle.Name = "chkEsDetalle"
    Me.chkEsDetalle.Size = New System.Drawing.Size(111, 17)
    Me.chkEsDetalle.TabIndex = 6
    Me.chkEsDetalle.Text = "Es Cuenta Detalle"
    Me.chkEsDetalle.UseVisualStyleBackColor = True
    '
    'cmbTipoSaldo
    '
    Me.cmbTipoSaldo.FormattingEnabled = True
    Me.cmbTipoSaldo.Items.AddRange(New Object() {"Saldo Deudor (Débito)", "Saldo Acreedor (Crédito)"})
    Me.cmbTipoSaldo.Location = New System.Drawing.Point(14, 322)
    Me.cmbTipoSaldo.Name = "cmbTipoSaldo"
    Me.cmbTipoSaldo.Size = New System.Drawing.Size(234, 21)
    Me.cmbTipoSaldo.TabIndex = 5
    '
    'LabelControl12
    '
    Me.LabelControl12.Location = New System.Drawing.Point(16, 303)
    Me.LabelControl12.Name = "LabelControl12"
    Me.LabelControl12.Size = New System.Drawing.Size(169, 13)
    Me.LabelControl12.TabIndex = 23
    Me.LabelControl12.Text = "Tipo de Saldo (Naturaleza Cuenta):"
    '
    'cmbTipoCuenta
    '
    Me.cmbTipoCuenta.FormattingEnabled = True
    Me.cmbTipoCuenta.Items.AddRange(New Object() {"Cuenta de Balance", "Cuenta de Resultado"})
    Me.cmbTipoCuenta.Location = New System.Drawing.Point(14, 267)
    Me.cmbTipoCuenta.Name = "cmbTipoCuenta"
    Me.cmbTipoCuenta.Size = New System.Drawing.Size(234, 21)
    Me.cmbTipoCuenta.TabIndex = 4
    '
    'LabelControl11
    '
    Me.LabelControl11.Location = New System.Drawing.Point(15, 248)
    Me.LabelControl11.Name = "LabelControl11"
    Me.LabelControl11.Size = New System.Drawing.Size(77, 13)
    Me.LabelControl11.TabIndex = 21
    Me.LabelControl11.Text = "Tipo de Cuenta:"
    '
    'txtDescripcionCuenta
    '
    Me.txtDescripcionCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtDescripcionCuenta.Location = New System.Drawing.Point(15, 123)
    Me.txtDescripcionCuenta.MaxLength = 250
    Me.txtDescripcionCuenta.Multiline = True
    Me.txtDescripcionCuenta.Name = "txtDescripcionCuenta"
    Me.txtDescripcionCuenta.Size = New System.Drawing.Size(236, 112)
    Me.txtDescripcionCuenta.TabIndex = 3
    '
    'LabelControl10
    '
    Me.LabelControl10.Location = New System.Drawing.Point(16, 104)
    Me.LabelControl10.Name = "LabelControl10"
    Me.LabelControl10.Size = New System.Drawing.Size(122, 13)
    Me.LabelControl10.TabIndex = 19
    Me.LabelControl10.Text = "Descripción de la Cuenta:"
    '
    'txtNombreCuenta
    '
    Me.txtNombreCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtNombreCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtNombreCuenta.Location = New System.Drawing.Point(15, 74)
    Me.txtNombreCuenta.MaxLength = 100
    Me.txtNombreCuenta.Name = "txtNombreCuenta"
    Me.txtNombreCuenta.Size = New System.Drawing.Size(236, 20)
    Me.txtNombreCuenta.TabIndex = 2
    '
    'LabelControl9
    '
    Me.LabelControl9.Location = New System.Drawing.Point(16, 55)
    Me.LabelControl9.Name = "LabelControl9"
    Me.LabelControl9.Size = New System.Drawing.Size(105, 13)
    Me.LabelControl9.TabIndex = 17
    Me.LabelControl9.Text = "Nombre de la Cuenta:"
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.txtCuentaContable)
    Me.Panel2.Controls.Add(Me.txtNivel)
    Me.Panel2.Controls.Add(Me.txtCuenta)
    Me.Panel2.Controls.Add(Me.txtSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSSSCuenta)
    Me.Panel2.Controls.Add(Me.txtSSSSSCuenta)
    Me.Panel2.Controls.Add(Me.LabelControl8)
    Me.Panel2.Controls.Add(Me.LabelControl7)
    Me.Panel2.Controls.Add(Me.LabelControl6)
    Me.Panel2.Controls.Add(Me.LabelControl5)
    Me.Panel2.Controls.Add(Me.LabelControl4)
    Me.Panel2.Controls.Add(Me.LabelControl3)
    Me.Panel2.Controls.Add(Me.LabelControl2)
    Me.Panel2.Controls.Add(Me.LabelControl1)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel2.Location = New System.Drawing.Point(0, 48)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(967, 94)
    Me.Panel2.TabIndex = 0
    '
    'txtCuentaContable
    '
    Me.txtCuentaContable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtCuentaContable.Location = New System.Drawing.Point(122, 10)
    Me.txtCuentaContable.Mask = "00000-000-000"
    Me.txtCuentaContable.Name = "txtCuentaContable"
    Me.txtCuentaContable.Size = New System.Drawing.Size(120, 20)
    Me.txtCuentaContable.TabIndex = 0
    Me.txtCuentaContable.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtNivel
    '
    Me.txtNivel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtNivel.Location = New System.Drawing.Point(66, 36)
    Me.txtNivel.Name = "txtNivel"
    Me.txtNivel.Size = New System.Drawing.Size(42, 20)
    Me.txtNivel.TabIndex = 1
    Me.txtNivel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtCuenta
    '
    Me.txtCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtCuenta.Location = New System.Drawing.Point(66, 62)
    Me.txtCuenta.Name = "txtCuenta"
    Me.txtCuenta.Size = New System.Drawing.Size(42, 20)
    Me.txtCuenta.TabIndex = 2
    Me.txtCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSCuenta
    '
    Me.txtSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSCuenta.Location = New System.Drawing.Point(173, 62)
    Me.txtSCuenta.Name = "txtSCuenta"
    Me.txtSCuenta.Size = New System.Drawing.Size(42, 20)
    Me.txtSCuenta.TabIndex = 3
    Me.txtSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSCuenta
    '
    Me.txtSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSCuenta.Location = New System.Drawing.Point(284, 62)
    Me.txtSSCuenta.Name = "txtSSCuenta"
    Me.txtSSCuenta.Size = New System.Drawing.Size(42, 20)
    Me.txtSSCuenta.TabIndex = 4
    Me.txtSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSSCuenta
    '
    Me.txtSSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSSCuenta.Location = New System.Drawing.Point(409, 62)
    Me.txtSSSCuenta.Name = "txtSSSCuenta"
    Me.txtSSSCuenta.Size = New System.Drawing.Size(42, 20)
    Me.txtSSSCuenta.TabIndex = 5
    Me.txtSSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSSSCuenta
    '
    Me.txtSSSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSSSCuenta.Location = New System.Drawing.Point(528, 62)
    Me.txtSSSSCuenta.Name = "txtSSSSCuenta"
    Me.txtSSSSCuenta.Size = New System.Drawing.Size(42, 20)
    Me.txtSSSSCuenta.TabIndex = 6
    Me.txtSSSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSSSSSCuenta
    '
    Me.txtSSSSSCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSSSSSCuenta.Location = New System.Drawing.Point(648, 62)
    Me.txtSSSSSCuenta.Name = "txtSSSSSCuenta"
    Me.txtSSSSSCuenta.Size = New System.Drawing.Size(42, 20)
    Me.txtSSSSSCuenta.TabIndex = 7
    Me.txtSSSSSCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'LabelControl8
    '
    Me.LabelControl8.Location = New System.Drawing.Point(23, 13)
    Me.LabelControl8.Name = "LabelControl8"
    Me.LabelControl8.Size = New System.Drawing.Size(85, 13)
    Me.LabelControl8.TabIndex = 15
    Me.LabelControl8.Text = "Cuenta Contable:"
    '
    'LabelControl7
    '
    Me.LabelControl7.Location = New System.Drawing.Point(23, 39)
    Me.LabelControl7.Name = "LabelControl7"
    Me.LabelControl7.Size = New System.Drawing.Size(27, 13)
    Me.LabelControl7.TabIndex = 13
    Me.LabelControl7.Text = "Nivel:"
    '
    'LabelControl6
    '
    Me.LabelControl6.Location = New System.Drawing.Point(578, 65)
    Me.LabelControl6.Name = "LabelControl6"
    Me.LabelControl6.Size = New System.Drawing.Size(69, 13)
    Me.LabelControl6.TabIndex = 11
    Me.LabelControl6.Text = "SSSSSCuenta:"
    '
    'LabelControl5
    '
    Me.LabelControl5.Location = New System.Drawing.Point(462, 65)
    Me.LabelControl5.Name = "LabelControl5"
    Me.LabelControl5.Size = New System.Drawing.Size(63, 13)
    Me.LabelControl5.TabIndex = 9
    Me.LabelControl5.Text = "SSSSCuenta:"
    '
    'LabelControl4
    '
    Me.LabelControl4.Location = New System.Drawing.Point(346, 65)
    Me.LabelControl4.Name = "LabelControl4"
    Me.LabelControl4.Size = New System.Drawing.Size(57, 13)
    Me.LabelControl4.TabIndex = 7
    Me.LabelControl4.Text = "SSSCuenta:"
    '
    'LabelControl3
    '
    Me.LabelControl3.Location = New System.Drawing.Point(231, 65)
    Me.LabelControl3.Name = "LabelControl3"
    Me.LabelControl3.Size = New System.Drawing.Size(51, 13)
    Me.LabelControl3.TabIndex = 5
    Me.LabelControl3.Text = "SSCuenta:"
    '
    'LabelControl2
    '
    Me.LabelControl2.Location = New System.Drawing.Point(122, 65)
    Me.LabelControl2.Name = "LabelControl2"
    Me.LabelControl2.Size = New System.Drawing.Size(45, 13)
    Me.LabelControl2.TabIndex = 3
    Me.LabelControl2.Text = "SCuenta:"
    '
    'LabelControl1
    '
    Me.LabelControl1.Location = New System.Drawing.Point(23, 65)
    Me.LabelControl1.Name = "LabelControl1"
    Me.LabelControl1.Size = New System.Drawing.Size(39, 13)
    Me.LabelControl1.TabIndex = 1
    Me.LabelControl1.Text = "Cuenta:"
    '
    'GridControl1
    '
    Me.GridControl1.DataSource = Me.TblCatalogoBindingSource
    Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GridControl1.Location = New System.Drawing.Point(0, 142)
    Me.GridControl1.MainView = Me.GridView1
    Me.GridControl1.Name = "GridControl1"
    Me.GridControl1.Size = New System.Drawing.Size(967, 413)
    Me.GridControl1.TabIndex = 11
    Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
    '
    'GridView1
    '
    Me.GridView1.GridControl = Me.GridControl1
    Me.GridView1.Name = "GridView1"
    Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
    Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
    Me.GridView1.OptionsBehavior.Editable = False
    Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
    Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
    Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
    Me.GridView1.OptionsView.ShowFooter = True
    Me.GridView1.OptionsView.ShowGroupPanel = False
    Me.GridView1.PaintStyleName = "UltraFlat"
    '
    'frmCatalogoCuentas
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1233, 555)
    Me.Controls.Add(Me.GridControl1)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.LblTitulo)
    Me.Name = "frmCatalogoCuentas"
    Me.Text = "Catalogo de Cuentas"
    CType(Me.TblCatalogoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

End Sub
  Friend WithEvents LblTitulo As System.Windows.Forms.Label
 Friend WithEvents TblCatalogoBindingSource As System.Windows.Forms.BindingSource
 Friend WithEvents Panel1 As System.Windows.Forms.Panel
 Friend WithEvents Panel2 As System.Windows.Forms.Panel
 Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
 Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
 Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents txtNivel As System.Windows.Forms.TextBox
 Friend WithEvents txtCuenta As System.Windows.Forms.TextBox
 Friend WithEvents txtSCuenta As System.Windows.Forms.TextBox
 Friend WithEvents txtSSCuenta As System.Windows.Forms.TextBox
 Friend WithEvents txtSSSCuenta As System.Windows.Forms.TextBox
 Friend WithEvents txtSSSSCuenta As System.Windows.Forms.TextBox
 Friend WithEvents txtSSSSSCuenta As System.Windows.Forms.TextBox
 Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents txtCuentaContable As System.Windows.Forms.MaskedTextBox
 Friend WithEvents txtNombreCuenta As System.Windows.Forms.TextBox
 Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents txtDescripcionCuenta As System.Windows.Forms.TextBox
 Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents cmbTipoCuenta As System.Windows.Forms.ComboBox
 Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents chkEsDetalle As System.Windows.Forms.CheckBox
 Friend WithEvents cmbTipoSaldo As System.Windows.Forms.ComboBox
 Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents chkEsDeshabilitada As System.Windows.Forms.CheckBox
 Friend WithEvents txtCuentaEdit As System.Windows.Forms.TextBox
 Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
 Friend WithEvents cmbNivel As System.Windows.Forms.ComboBox
End Class
