﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdminCIF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdminCIF))
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.grdOrdenes = New DevExpress.XtraGrid.GridControl()
        Me.grvOrdenes = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtAjuste = New System.Windows.Forms.TextBox()
        Me.btnAutorizar = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.lblCierre = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grdCostosCif = New DevExpress.XtraGrid.GridControl()
        Me.grvCostosCif = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.txtFinal = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtInicial = New System.Windows.Forms.TextBox()
        Me.grbDatos = New System.Windows.Forms.GroupBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbMes = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmbAnio = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCodigoCif = New System.Windows.Forms.TextBox()
        Me.mainPanel.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.grdOrdenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvOrdenes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.grdCostosCif, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvCostosCif, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.grbDatos.SuspendLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.Panel2)
        Me.mainPanel.Controls.Add(Me.Panel1)
        Me.mainPanel.Controls.Add(Me.grbDatos)
        Me.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mainPanel.Location = New System.Drawing.Point(0, 0)
        Me.mainPanel.Margin = New System.Windows.Forms.Padding(4)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1548, 590)
        Me.mainPanel.TabIndex = 7
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.grdOrdenes)
        Me.Panel2.Controls.Add(Me.Panel6)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(1017, 70)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(531, 520)
        Me.Panel2.TabIndex = 9
        '
        'grdOrdenes
        '
        Me.grdOrdenes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdOrdenes.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.grdOrdenes.Location = New System.Drawing.Point(0, 45)
        Me.grdOrdenes.MainView = Me.grvOrdenes
        Me.grdOrdenes.Margin = New System.Windows.Forms.Padding(4)
        Me.grdOrdenes.Name = "grdOrdenes"
        Me.grdOrdenes.Size = New System.Drawing.Size(531, 411)
        Me.grdOrdenes.TabIndex = 15
        Me.grdOrdenes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvOrdenes})
        '
        'grvOrdenes
        '
        Me.grvOrdenes.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvOrdenes.GridControl = Me.grdOrdenes
        Me.grvOrdenes.Name = "grvOrdenes"
        Me.grvOrdenes.OptionsBehavior.Editable = False
        Me.grvOrdenes.OptionsBehavior.ReadOnly = True
        Me.grvOrdenes.OptionsCustomization.AllowColumnMoving = False
        Me.grvOrdenes.OptionsCustomization.AllowColumnResizing = False
        Me.grvOrdenes.OptionsCustomization.AllowGroup = False
        Me.grvOrdenes.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvOrdenes.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvOrdenes.OptionsView.ColumnAutoWidth = False
        Me.grvOrdenes.OptionsView.ShowGroupPanel = False
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Controls.Add(Me.txtAjuste)
        Me.Panel6.Controls.Add(Me.btnAutorizar)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel6.Location = New System.Drawing.Point(0, 456)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(531, 64)
        Me.Panel6.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 23)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 17)
        Me.Label6.TabIndex = 87
        Me.Label6.Text = "Ajuste Total:"
        '
        'txtAjuste
        '
        Me.txtAjuste.Location = New System.Drawing.Point(108, 20)
        Me.txtAjuste.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAjuste.Name = "txtAjuste"
        Me.txtAjuste.ReadOnly = True
        Me.txtAjuste.Size = New System.Drawing.Size(121, 22)
        Me.txtAjuste.TabIndex = 86
        Me.txtAjuste.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnAutorizar
        '
        Me.btnAutorizar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAutorizar.Enabled = False
        Me.btnAutorizar.Image = CType(resources.GetObject("btnAutorizar.Image"), System.Drawing.Image)
        Me.btnAutorizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAutorizar.Location = New System.Drawing.Point(382, 6)
        Me.btnAutorizar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAutorizar.Name = "btnAutorizar"
        Me.btnAutorizar.Size = New System.Drawing.Size(132, 49)
        Me.btnAutorizar.TabIndex = 85
        Me.btnAutorizar.Text = "Ajustar Cierres"
        Me.btnAutorizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAutorizar.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.txtEstado)
        Me.Panel5.Controls.Add(Me.lblCierre)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(531, 45)
        Me.Panel5.TabIndex = 13
        '
        'txtEstado
        '
        Me.txtEstado.BackColor = System.Drawing.Color.White
        Me.txtEstado.Location = New System.Drawing.Point(350, 10)
        Me.txtEstado.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.ReadOnly = True
        Me.txtEstado.Size = New System.Drawing.Size(175, 22)
        Me.txtEstado.TabIndex = 81
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCierre
        '
        Me.lblCierre.AutoSize = True
        Me.lblCierre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCierre.ForeColor = System.Drawing.Color.Navy
        Me.lblCierre.Location = New System.Drawing.Point(12, 11)
        Me.lblCierre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCierre.Name = "lblCierre"
        Me.lblCierre.Size = New System.Drawing.Size(180, 18)
        Me.lblCierre.TabIndex = 56
        Me.lblCierre.Text = "Cierres Aplicados del Mes"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.grdCostosCif)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(0, 70)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1009, 520)
        Me.Panel1.TabIndex = 8
        '
        'grdCostosCif
        '
        Me.grdCostosCif.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdCostosCif.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.grdCostosCif.Location = New System.Drawing.Point(0, 45)
        Me.grdCostosCif.MainView = Me.grvCostosCif
        Me.grdCostosCif.Margin = New System.Windows.Forms.Padding(4)
        Me.grdCostosCif.Name = "grdCostosCif"
        Me.grdCostosCif.Size = New System.Drawing.Size(1009, 411)
        Me.grdCostosCif.TabIndex = 16
        Me.grdCostosCif.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvCostosCif})
        '
        'grvCostosCif
        '
        Me.grvCostosCif.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvCostosCif.GridControl = Me.grdCostosCif
        Me.grvCostosCif.Name = "grvCostosCif"
        Me.grvCostosCif.OptionsBehavior.Editable = False
        Me.grvCostosCif.OptionsBehavior.ReadOnly = True
        Me.grvCostosCif.OptionsCustomization.AllowColumnMoving = False
        Me.grvCostosCif.OptionsCustomization.AllowColumnResizing = False
        Me.grvCostosCif.OptionsCustomization.AllowGroup = False
        Me.grvCostosCif.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvCostosCif.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvCostosCif.OptionsView.ShowGroupPanel = False
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1009, 45)
        Me.Panel4.TabIndex = 12
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(4, 11)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(235, 18)
        Me.Label8.TabIndex = 55
        Me.Label8.Text = "Costos Indirectos y Mano de Obra"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Panel7)
        Me.Panel3.Controls.Add(Me.txtFinal)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txtInicial)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 456)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1009, 64)
        Me.Panel3.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(777, 23)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 17)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Costo Final:"
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.btnEliminar)
        Me.Panel7.Controls.Add(Me.btnModificar)
        Me.Panel7.Controls.Add(Me.btnAgregar)
        Me.Panel7.Location = New System.Drawing.Point(3, 1)
        Me.Panel7.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(379, 62)
        Me.Panel7.TabIndex = 1
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(255, 5)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(111, 49)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Enabled = False
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(136, 5)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(111, 49)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Enabled = False
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(16, 5)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(111, 49)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'txtFinal
        '
        Me.txtFinal.Location = New System.Drawing.Point(868, 20)
        Me.txtFinal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.ReadOnly = True
        Me.txtFinal.Size = New System.Drawing.Size(121, 22)
        Me.txtFinal.TabIndex = 22
        Me.txtFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(549, 23)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 17)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Costo Inicial:"
        '
        'txtInicial
        '
        Me.txtInicial.Location = New System.Drawing.Point(647, 20)
        Me.txtInicial.Margin = New System.Windows.Forms.Padding(4)
        Me.txtInicial.Name = "txtInicial"
        Me.txtInicial.ReadOnly = True
        Me.txtInicial.Size = New System.Drawing.Size(121, 22)
        Me.txtInicial.TabIndex = 20
        Me.txtInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'grbDatos
        '
        Me.grbDatos.Controls.Add(Me.txtDescripcion)
        Me.grbDatos.Controls.Add(Me.Label7)
        Me.grbDatos.Controls.Add(Me.ckIsActivo)
        Me.grbDatos.Controls.Add(Me.Label1)
        Me.grbDatos.Controls.Add(Me.cmbMes)
        Me.grbDatos.Controls.Add(Me.Label3)
        Me.grbDatos.Controls.Add(Me.cmbAnio)
        Me.grbDatos.Controls.Add(Me.Label2)
        Me.grbDatos.Controls.Add(Me.txtCodigoCif)
        Me.grbDatos.Dock = System.Windows.Forms.DockStyle.Top
        Me.grbDatos.Enabled = False
        Me.grbDatos.Location = New System.Drawing.Point(0, 0)
        Me.grbDatos.Margin = New System.Windows.Forms.Padding(4)
        Me.grbDatos.Name = "grbDatos"
        Me.grbDatos.Padding = New System.Windows.Forms.Padding(4)
        Me.grbDatos.Size = New System.Drawing.Size(1548, 70)
        Me.grbDatos.TabIndex = 0
        Me.grbDatos.TabStop = False
        Me.grbDatos.Text = "Datos del Documento"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.White
        Me.txtDescripcion.Location = New System.Drawing.Point(769, 28)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(655, 22)
        Me.txtDescripcion.TabIndex = 67
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(673, 32)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 17)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Descripción:"
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Location = New System.Drawing.Point(1452, 31)
        Me.ckIsActivo.Margin = New System.Windows.Forms.Padding(4)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Activo"
        Me.ckIsActivo.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.ckIsActivo.Size = New System.Drawing.Size(80, 21)
        Me.ckIsActivo.TabIndex = 65
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(491, 32)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 17)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Mes:"
        '
        'cmbMes
        '
        Me.cmbMes.BackColor = System.Drawing.Color.White
        Me.cmbMes.FormattingEnabled = True
        Me.cmbMes.Location = New System.Drawing.Point(537, 28)
        Me.cmbMes.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbMes.Name = "cmbMes"
        Me.cmbMes.Size = New System.Drawing.Size(127, 24)
        Me.cmbMes.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(299, 32)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 17)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Año:"
        '
        'cmbAnio
        '
        Me.cmbAnio.BackColor = System.Drawing.Color.White
        Me.cmbAnio.FormattingEnabled = True
        Me.cmbAnio.Location = New System.Drawing.Point(345, 28)
        Me.cmbAnio.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbAnio.Name = "cmbAnio"
        Me.cmbAnio.Size = New System.Drawing.Size(127, 24)
        Me.cmbAnio.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 32)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(132, 17)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Codigo Documento:"
        '
        'txtCodigoCif
        '
        Me.txtCodigoCif.BackColor = System.Drawing.Color.White
        Me.txtCodigoCif.Location = New System.Drawing.Point(155, 28)
        Me.txtCodigoCif.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigoCif.Name = "txtCodigoCif"
        Me.txtCodigoCif.ReadOnly = True
        Me.txtCodigoCif.Size = New System.Drawing.Size(121, 22)
        Me.txtCodigoCif.TabIndex = 18
        Me.txtCodigoCif.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'frmAdminCIF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1548, 590)
        Me.Controls.Add(Me.mainPanel)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdminCIF"
        Me.Text = ".:::. Costos Indirectos y Mano de Obra .:::."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mainPanel.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.grdOrdenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvOrdenes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.grdCostosCif, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvCostosCif, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.grbDatos.ResumeLayout(False)
        Me.grbDatos.PerformLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents grbDatos As System.Windows.Forms.GroupBox
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbMes As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmbAnio As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoCif As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents grdOrdenes As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvOrdenes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grdCostosCif As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvCostosCif As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnAutorizar As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtAjuste As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtInicial As System.Windows.Forms.TextBox
    Friend WithEvents lblCierre As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtEstado As System.Windows.Forms.TextBox
End Class
