﻿Imports System.Security.Cryptography.MD5
Imports System.Security.Cryptography
Imports System.Text

Public Class frmAutorizar
    Dim TextoEnBytes As Byte()
    Dim HashEnBytes As Byte()
    Dim HashTexto As String
    Dim strsql As String
    Public Cod_Orden As String
    Dim Color_Existencia As Color = Color.White
    Dim Color_Adicional As Color = Color.Gold

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Public Function Validar(ByVal Password As String) As Boolean
        Dim Valid As Boolean = False
        Dim strsql As String = ""

        Try
            strsql = "SELECT TOP 1 Usu.Usuario FROM Configuracion.Usuarios Usu "
            strsql = strsql & " INNER JOIN Configuracion.Permisos Per ON Usu.UsuarioID = Per.FkUsuario"
            strsql = strsql & " Where Per.Autoriza = 1 and Usu.Contrasena like '" & Password & "'"

            Dim tblValidar As DataTable = SQL(strsql, "tblValidar", My.Settings.SolIndustrialCNX).Tables(0)

            If tblValidar.Rows.Count > 0 Then
                Valid = True
            End If

        Catch ex As Exception
        End Try

        Return Valid
    End Function

    Private Sub frmAutorizar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPassword.Text = ""
        lblAdicional.BackColor = Color_Adicional
        lblExistencia.BackColor = Color_Existencia
        lbAdicional.ForeColor = Color_Adicional
        lbExistencia.ForeColor = Color_Existencia
        Select Case NumCatalogo
            Case 23
                With frmOrdenProd
                    Cod_Orden = .lblCodigoOrden.Text
                End With
                strsql = "select * from vw_ORDEN_PROD_AUTORIZAR where Orden='" + Cod_Orden + "' and Sucursal=" & My.Settings.Sucursal & ""
                Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)
                With dgvSustituto
                    .DataSource = TblDatosProd
                    .Columns("Sucursal").Visible = False
                    .Columns("Orden").Visible = False
                    .Columns("Descripcion1").Visible = False
                    .Columns("Descripcion2").Visible = False
                    .Columns("Codigo").DefaultCellStyle.BackColor = Color_Adicional
                    .Columns("Cantidad").DefaultCellStyle.BackColor = Color_Adicional
                    .Columns("Codigo2").HeaderText = "Codigo"
                    .Columns("Cantidad2").HeaderText = "Cantidad"
                    .AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                    .ClearSelection()
                    .DefaultCellStyle.SelectionBackColor = SystemColors.Menu
                    .DefaultCellStyle.SelectionForeColor = Color.Black
                End With
        End Select
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim aSeguridad As New clsAutinticacion(My.Settings.SolIndustrialCNX)

        If Validar(aSeguridad.Encriptar(txtPassword.Text)) = True Then
            frmOrdenProd.aplicarAutorizado()
            Cerrar()
            MsgBox("Registros aurtorizados correctamente.", MsgBoxStyle.Information, "Información")
        Else
            txtPassword.Text = ""
            txtPassword.Focus()
            MsgBox("Contraseña incorrecta.", MsgBoxStyle.Critical, "Información")
        End If

    End Sub
    Private Sub dgvSustituto_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvSustituto.CellMouseClick
        lbDescripcion1.Text = dgvSustituto.Rows(dgvSustituto.CurrentRow.Index).Cells("Descripcion1").Value.ToString
        lbDescripcion2.Text = dgvSustituto.Rows(dgvSustituto.CurrentRow.Index).Cells("Descripcion2").Value.ToString
        dgvSustituto.ClearSelection()
    End Sub

    Private Sub dgvSustituto_MouseEnter(sender As Object, e As EventArgs) Handles dgvSustituto.MouseEnter
        lbAdicional.Visible = True
        lbExistencia.Visible = True
        lbDescripcion1.Visible = True
        lbDescripcion2.Visible = True
    End Sub
    Private Sub dgvSustituto_MouseLeave(sender As Object, e As EventArgs) Handles dgvSustituto.MouseLeave
        lbAdicional.Visible = False
        lbExistencia.Visible = False
        lbDescripcion1.Visible = False
        lbDescripcion2.Visible = False
    End Sub
End Class