﻿
Public Class frmRecetas

    Public Sub CargarDatos(ByVal CodigoReceta As String)
        Dim strsql As String = ""
        Dim strsql2 As String = ""

        Try
            strsql = "SELECT TBR.Codigo_Reseta, TBR.Codigo_Prod_Resultante, TBR.Descripcion, TBR.Activo, PRT.Id_ProductosTipo FROM Tbl_Recetas AS TBR INNER JOIN Productos AS PRD ON TBR.Codigo_Prod_Resultante = PRD.Codigo_Producto INNER JOIN Productos_Tipo AS PRT ON PRD.Id_ProductosTipo = PRT.Id_ProductosTipo where TBR.Codigo_Reseta ='" & CodigoReceta & "'"

            Dim tblDatosReceta As DataTable = SQL(strsql, "tblDatosReceta", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatosReceta.Rows.Count <> 0 Then

                lblCodigo.Text = tblDatosReceta.Rows(0).Item(0)
                txtDescripcion.Text = tblDatosReceta.Rows(0).Item(2)
                ckIsActivo.Checked = tblDatosReceta.Rows(0).Item(3)
                luProductoFinal.EditValue = tblDatosReceta.Rows(0).Item(1)

                cargarProductoReceta(tblDatosReceta.Rows(0).Item(4))
                CargarReceta(CodigoReceta)

                nTipoEdic = 2
            End If
        Catch ex As Exception
            'MsgBox("Ha ocurrido un error, La factura no pudo se accesada", MsgBoxStyle.Exclamation, "Error")
            'Exit Sub
        End Try
    End Sub

    Public Sub CargarReceta(ByVal CodigoReceta As String)
        Dim strsql As String = ""
        Dim strsql2 As String = ""

        Try
            strsql = "SELECT Prd.Codigo_Producto AS 'Codigo' , tblDet.Cantidad AS 'Cantidad', Prd.Nombre_Producto AS 'Producto', dbo.getCostoProducto(Prd.Codigo_Producto) AS 'Costo/Und', (tblDet.Cantidad * dbo.getCostoProducto(Prd.Codigo_Producto)) AS 'Costo Total' FROM Tbl_Recetas_Detalle tblDet INNER JOIN Productos Prd ON tblDet.Codigo_Prod_Ingrediente = Prd.Codigo_Producto where tblDet.Codigo_Reseta = '" & CodigoReceta & "' order by Prd.Codigo_Producto"

            grdDetalle.DataSource = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
            grvDetalle.Columns("Codigo").Width = 75
            grvDetalle.Columns("Cantidad").Width = 80
            grvDetalle.Columns("Producto").Width = 300
            grvDetalle.Columns("Costo/Und").Width = 100
            grvDetalle.Columns("Costo Total").Width = 100
            grvDetalle.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvDetalle.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:#.####}"
            grvDetalle.OptionsView.ShowFooter = True

        Catch ex As Exception
            'MsgBox("Ha ocurrido un error, La factura no pudo se accesada", MsgBoxStyle.Exclamation, "Error")
            'Exit Sub
        End Try
    End Sub

    Private Sub cargarProductoFinal()

        If nTipoEdic = 1 Then
            luProductoFinal.Properties.DataSource = SQL("select Codigo_Producto AS Codigo, Nombre_Producto AS Nombre from [dbo].[Productos] where Id_ProductosTipo in (1,2) and not (Codigo_Producto in (Select Codigo_Prod_Resultante from Tbl_Recetas)) order by Nombre_Producto", "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
        Else
            luProductoFinal.Properties.DataSource = SQL("select Codigo_Producto AS Codigo, Nombre_Producto AS Nombre from [dbo].[Productos] where Id_ProductosTipo in (1,2) order by Nombre_Producto", "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
        End If

        luProductoFinal.Properties.DisplayMember = "Nombre"
        luProductoFinal.Properties.ValueMember = "Codigo"

        luProductoFinal.ItemIndex = 0

    End Sub

    Private Sub cargarProductoReceta(ByVal IdTipo As Integer)
        Dim strsql As String

        If IdTipo = 1 Then
            strsql = "select Codigo_Producto AS Codigo, Nombre_Producto AS Nombre, Und.Descripcion_Unidad Medida, dbo.getCostoProducto(Codigo_Producto) AS 'Costo USS' from [dbo].[Productos] INNER JOIN Unidades_Medida Und ON Productos.Codigo_UnidMedida = Und.Codigo_UnidadMed where Id_ProductosTipo in (0,1) order by Codigo_Producto"
        Else
            strsql = "select Codigo_Producto AS Codigo, Nombre_Producto AS Nombre, Und.Descripcion_Unidad Medida, dbo.getCostoProducto(Codigo_Producto) AS 'Costo USS' from [dbo].[Productos] INNER JOIN Unidades_Medida Und ON Productos.Codigo_UnidMedida = Und.Codigo_UnidadMed where Id_ProductosTipo in (0,1) order by Codigo_Producto"
        End If

        Dim tblDatosProductos As DataTable = Nothing
        grvProductos.Columns.Clear()
        grdProductos.DataSource = SQL(strsql, "tblDatosProductos", My.Settings.SolIndustrialCNX).Tables(0)
        grvProductos.Columns("Codigo").Width = 75
        grvProductos.Columns("Nombre").Width = 300
        grvProductos.Columns("Medida").Width = 48
        grvProductos.Columns("Costo USS").Width = 100
    End Sub

    Private Sub frmRecetas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Panel3.Enabled = False
        Panel4.Enabled = False
        Panel5.Enabled = False

        cargarProductoFinal()
        txtCantidad.Text = 0
        grvDetalle.Columns.Clear()
        cmdAddProv.Enabled = True

        If nTipoEdic = 1 Then
            luProductoFinal.Enabled = True
            txtDescripcion.Text = ""
            lblCodigo.Text = Microsoft.VisualBasic.Right("000000" & RegistroMaximo(), 6)
            ckIsActivo.Checked = 1
        ElseIf nTipoEdic = 2 Then
            luProductoFinal.Enabled = False
            cmdAddProv.Enabled = False
            Panel3.Enabled = True
            Panel4.Enabled = True
            Panel5.Enabled = True
            CargarDatos(CodigoEntidad)

            btnAgregar.Enabled = False
            btnQuitar.Enabled = False
        End If

    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf (e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",")) Or (e.KeyChar = "." And Not CajaTexto.Text.IndexOf(".")) Then
            e.Handled = True
        ElseIf e.KeyChar = "," Or e.KeyChar = "." Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            Resum = Produccion.EditarRecetaEncabezado(IIf(lblCodigo.Text = "", "000000", lblCodigo.Text), IIf(luProductoFinal.GetColumnValue("Codigo") = "", "000000", luProductoFinal.GetColumnValue("Codigo")), txtDescripcion.Text.ToUpper(), ckIsActivo.Checked, nTipoEdic)
            If Resum = "OK" Then
                MsgBox("Encabezado actualizado correctamente", MsgBoxStyle.Information, "CyberPlanet.Net")

                'lblCodigo.Enabled = False
                luProductoFinal.Enabled = False
                cmdAddProv.Enabled = False
                Panel3.Enabled = True
                Panel4.Enabled = True
                Panel5.Enabled = True
                CargarDatos(lblCodigo.Text)

            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Dim ProductoIngrediente As String = grvProductos.GetRowCellValue(grvProductos.GetSelectedRows(0), "Codigo").ToString()

        Try
            If txtCantidad.Text > 0 Then
                Resum = Produccion.EditarRecetaDetalle(IIf(lblCodigo.Text = "", "000000", lblCodigo.Text), IIf(ProductoIngrediente = "", "000000", ProductoIngrediente), txtCantidad.Text, 1)
                If Resum = "OK" Then
                    txtCantidad.Text = 0
                    CargarReceta(lblCodigo.Text)
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            Else
                MsgBox("La cantidad debe ser mayor a 0.", MsgBoxStyle.Information, "SIGCA")
                txtCantidad.Text = 0
                txtCantidad.Focus()
            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Dim ProductoIngrediente As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
        Dim Cantidad As Integer = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad")

        Try
            If txtCantidad.Text > 0 Then
                If Cantidad > txtCantidad.Text Then
                    Resum = Produccion.EditarRecetaDetalle(IIf(lblCodigo.Text = "", "000000", lblCodigo.Text), IIf(ProductoIngrediente = "", "000000", ProductoIngrediente), (Cantidad - txtCantidad.Text), 2)
                Else
                    Resum = Produccion.EditarRecetaDetalle(IIf(lblCodigo.Text = "", "000000", lblCodigo.Text), IIf(ProductoIngrediente = "", "000000", ProductoIngrediente), txtCantidad.Text, 3)
                End If

                If Resum = "OK" Then
                    txtCantidad.Text = 0
                    CargarReceta(lblCodigo.Text)
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            Else
                MsgBox("La cantidad debe ser mayor a 0.", MsgBoxStyle.Information, "SIGCA")
                txtCantidad.Text = 0
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub luProductoFinal_EditValueChanged(sender As Object, e As EventArgs) Handles luProductoFinal.EditValueChanged
        Dim strsql As String = ""

        Try
            strsql = "SELECT Prt.Id_ProductosTipo FROM Productos Prd INNER JOIN Productos_Tipo Prt ON Prd.Id_ProductosTipo = Prt.Id_ProductosTipo where  Prd.Codigo_Producto = '" & IIf(luProductoFinal.GetColumnValue("Codigo") = "", "000000", luProductoFinal.GetColumnValue("Codigo")) & "'"
            Dim tblTipoProducto As DataTable = SQL(strsql, "tblTipoProducto", My.Settings.SolIndustrialCNX).Tables(0)

            If tblTipoProducto.Rows.Count <> 0 Then

                cargarProductoReceta(tblTipoProducto.Rows(0).Item(0))

            End If
        Catch ex As Exception
            'MsgBox("Ha ocurrido un error, La factura no pudo se accesada", MsgBoxStyle.Exclamation, "Error")
            'Exit Sub
        End Try
    End Sub

    Private Sub frmRecetas_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Dim strsql As String = "Select * from vwRecetas"
        Dim tblDatosCatalogo As DataTable = Nothing
        frmCatalogo.GridView1.Columns.Clear()
        frmCatalogo.TblCatalogosBS.DataSource = SQL(strsql, "tblDatosCatalogo", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Private Sub txtCantidad_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantidad.KeyPress
        NumerosyDecimal(txtCantidad, e)
    End Sub

    Private Sub cmdAddProv_Click(sender As Object, e As EventArgs) Handles cmdAddProv.Click
        'itemAdd_Ant = luProv.ItemIndex
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 9
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        cargarProductoFinal()
    End Sub

    Private Sub txtDescripcion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescripcion.KeyPress
        e.KeyChar = Char.ToUpper(e.KeyChar)
    End Sub

    Private Sub grdProductos_Click(sender As Object, e As EventArgs) Handles grdProductos.Click
        btnAgregar.Enabled = True
        btnQuitar.Enabled = False
    End Sub

    Private Sub grdDetalle_Click(sender As Object, e As EventArgs) Handles grdDetalle.Click
        btnAgregar.Enabled = False
        btnQuitar.Enabled = True
    End Sub

End Class