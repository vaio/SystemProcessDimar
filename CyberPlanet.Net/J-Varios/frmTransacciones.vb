﻿Public Class frmTransacciones

	Private Sub frmTransacciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		TransacVisible = True
	End Sub

	Private Sub frmTransacciones_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave
        TransacVisible = False
	End Sub

	Sub AbrirEditorTransacciones()
        If TipoTransaccion = 8 Then
            'frmMultifuncional.lblDescripcion.Text = "Venta de Productos."
            'frmMultifuncional.ShowDialog()
        ElseIf TipoTransaccion = 9 Then
            'frmMultifuncional.lblDescripcion.Text = "Compra de Productos."
            'frmMultifuncional.ShowDialog()
        ElseIf TipoTransaccion = 11 Then
            'frmCompraSuministrovb.lblDescripcion.Text = "Compra de Suministros."
            frmCompraSuministrovb.ShowDialog()
        ElseIf TipoTransaccion = 12 Then
            frmCompraActivoFijo.ShowDialog()
        ElseIf TipoTransaccion = 13 Then
            'frmRegistroGastos.lblDescripcion.Text = "Gastos Operacionales."
            frmRegistroGastos.ShowDialog()
        End If
	End Sub

	Private Sub btnRealizarCierre_Click(sender As Object, e As EventArgs) Handles btnRealizarCierre.Click
		FrmCierreTransacciones.ShowDialog()
	End Sub

    'Sub CargarTransacciones()
    '	Dim StrsqlTrans As String = ""

    '	StrsqlTrans = "Select * from vw_Transacciones where "

    '	If ChkMostrarCierre.Checked = True Then
    '		StrsqlTrans = StrsqlTrans & " Not Num_Lote Is null and "
    '		ChkMostrarCierre.Text = "Mostrar Abiertas"
    '	Else
    '		StrsqlTrans = StrsqlTrans & " Num_Lote Is null and "
    '		ChkMostrarCierre.Text = "Mostrar Cerradas"
    '	End If

    '	If frmPrincipal.cmbTipoVista.SelectedIndex = 0 Then
    '		StrsqlTrans = StrsqlTrans & " Fecha = '" & Format(frmPrincipal.dtpDias.Value, "yyyy/MM/dd") & "' "
    '	ElseIf frmPrincipal.cmbTipoVista.SelectedIndex = 1 Then
    '		StrsqlTrans = StrsqlTrans & " month(Fecha) = " & (frmPrincipal.cmbMeses.SelectedIndex + 1) & " and year(Fecha) = " & CInt(frmPrincipal.cmbAnio.Text) & " "
    '	End If
    '	StrsqlTrans = StrsqlTrans & " order by [N°] desc"
    '	'frmTransacciones.GridView1.Columns.Clear()
    '       'TblBSTransacciones.DataSource = SQL(StrsqlTrans, "tblDatosTransacciones", My.Settings.SolIndustrialCNX).Tables(0)
    '	If TblBSTransacciones.Count > 0 Then
    '		If ChkMostrarCierre.Checked = True Then
    '			btnRealizarCierre.Enabled = False
    '		Else
    '			btnRealizarCierre.Enabled = True
    '		End If
    '	Else
    '		btnRealizarCierre.Enabled = False
    '	End If
    '	EncabezadosTrans()
    'End Sub


    'Sub EncabezadosTrans()
    '	GridView1.Columns.Clear()
    '	GridView1.OptionsView.ShowFooter = True

    '	Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
    '	gc0.Name = "N°"
    '	gc0.Caption = "N°"
    '	gc0.FieldName = "N°"
    '	gc0.Width = 30
    '	GridView1.Columns.Add(gc0)
    '	gc0.Visible = True
    '	gc0.VisibleIndex = 0
    '	GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

    '	Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
    '	gc1.Name = "Tipo"
    '	gc1.Caption = "Tipo"
    '	gc1.FieldName = "Tipo"
    '	gc1.Width = 80
    '	GridView1.Columns.Add(gc1)
    '	gc1.Visible = True
    '	gc1.VisibleIndex = 1

    '	Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
    '	gc2.Name = "Fecha"
    '	gc2.Caption = "Fecha"
    '	gc2.FieldName = "Fecha"
    '	gc2.Width = 50
    '	GridView1.Columns.Add(gc2)
    '	gc2.Visible = True
    '	gc2.VisibleIndex = 2

    '	Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
    '	gc3.Name = "Concepto"
    '	gc3.Caption = "Concepto"
    '	gc3.FieldName = "Concepto"
    '	gc3.Width = 250
    '	GridView1.Columns.Add(gc3)
    '	gc3.Visible = True
    '	gc3.VisibleIndex = 3

    '	Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
    '	gc4.Name = "Debito"
    '	gc4.Caption = "Debito"
    '	gc4.FieldName = "Debito"
    '	gc4.DisplayFormat.FormatString = "{0:$0.00}"
    '	gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
    '	gc4.Width = 50
    '	GridView1.Columns.Add(gc4)
    '	gc4.Visible = True
    '	gc4.VisibleIndex = 4
    '	GridView1.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
    '	GridView1.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"

    '	Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
    '	gc5.Name = "Credito"
    '	gc5.Caption = "Credito"
    '	gc5.FieldName = "Credito"
    '	gc5.DisplayFormat.FormatString = "{0:$0.00}"
    '	gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
    '	gc5.Width = 50
    '	GridView1.Columns.Add(gc5)
    '	gc5.Visible = True
    '	gc5.VisibleIndex = 5
    '	GridView1.Columns(5).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
    '	GridView1.Columns(5).SummaryItem.DisplayFormat = "{0:$0.00}"

    'End Sub

    'Private Sub ChkMostrarCierre_CheckedChanged(sender As Object, e As EventArgs) Handles ChkMostrarCierre.CheckedChanged
    '	CargarTransacciones()
    'End Sub

End Class