﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelCliente))
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.luCliente = New DevExpress.XtraEditors.LookUpEdit()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.cmdCancelar = New System.Windows.Forms.Button()
		Me.cmdAceptar = New System.Windows.Forms.Button()
		Me.TblClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.Panel2.SuspendLayout()
		CType(Me.luCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.TblClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.Label2)
		Me.Panel2.Controls.Add(Me.luCliente)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(0, 0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(430, 63)
		Me.Panel2.TabIndex = 7
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(28, 25)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(42, 13)
		Me.Label2.TabIndex = 6
		Me.Label2.Text = "Cliente:"
		'
		'luCliente
		'
		Me.luCliente.Location = New System.Drawing.Point(93, 22)
		Me.luCliente.Name = "luCliente"
		Me.luCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.luCliente.Size = New System.Drawing.Size(311, 20)
		Me.luCliente.TabIndex = 4
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.cmdCancelar)
		Me.Panel1.Controls.Add(Me.cmdAceptar)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(0, 63)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(430, 49)
		Me.Panel1.TabIndex = 6
		'
		'cmdCancelar
		'
		Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
		Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdCancelar.Location = New System.Drawing.Point(340, 6)
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Text = "&Cancelar"
		Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdCancelar.UseVisualStyleBackColor = True
		'
		'cmdAceptar
		'
		Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.cmdAceptar.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
		Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cmdAceptar.Location = New System.Drawing.Point(256, 6)
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Text = "&Aceptar"
		Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.cmdAceptar.UseVisualStyleBackColor = True
		'
		'frmSelCliente
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(430, 112)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.Name = "frmSelCliente"
		Me.Text = "Seleccionar Cliente"
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		CType(Me.luCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		CType(Me.TblClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

End Sub
		Friend WithEvents Panel2 As System.Windows.Forms.Panel
		Friend WithEvents Label2 As System.Windows.Forms.Label
		Friend WithEvents luCliente As DevExpress.XtraEditors.LookUpEdit
		Friend WithEvents Panel1 As System.Windows.Forms.Panel
		Friend WithEvents cmdCancelar As System.Windows.Forms.Button
		Friend WithEvents cmdAceptar As System.Windows.Forms.Button
	Friend WithEvents TblClienteBindingSource As System.Windows.Forms.BindingSource
End Class
