﻿Public Class frmDetalleFactura

    Private Sub frmDocumentosInv_Leave(sender As Object, e As EventArgs) Handles Me.Leave
        'frmPrincipal.MostrarResumen(True)
    End Sub

    Private Sub frmDocumentosInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TituloDocumento()
        Cargar_MesDiaAnioDocumentos()
        CargarDataDocumentos()
        ActivarBotones()
        LblTitulo.Left = (btnNuevo.Width * 4) + 30
        LblTitulo.Width = Me.Width - (btnNuevo.Width * 4) - 220
    End Sub

    Sub ActivarBotones()
        If TblBSDocumentos.Count > 0 Then
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        Else
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Sub TituloDocumento()
        If TipoDocumento = 1 Then
            Me.LblTitulo.Text = "Entradas de Inventario."
        ElseIf TipoDocumento = 2 Then
            Me.LblTitulo.Text = "Salidas de Inventario."
        ElseIf TipoDocumento = 3 Then
            Me.LblTitulo.Text = "Facturación de Productos."
        ElseIf TipoDocumento = 4 Then
            Me.LblTitulo.Text = "Compras de Productos."
        ElseIf TipoDocumento = 5 Then
            Me.LblTitulo.Text = "Transferencias de Inventario."
        End If
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        nTipoEdicDoc = 1
        AbrirMasterDoc()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdicDoc = 2
        AbrirMasterDoc()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        nTipoEdicDoc = 3
        AbrirMasterDoc()
    End Sub

    Sub AbrirMasterDoc()
        'frmMasterDocInventario.MdiParent = frmPrincipal
        'frmMasterDocInventario.WindowState = FormWindowState.Maximized
        'frmMasterDocInventario.Show()
    End Sub

    Sub Cargar_MesDiaAnioDocumentos()
        'frmPrincipal.cmbTipoVista.SelectedIndex = 0
        'frmPrincipal.dtpDias.Value = Now
        'frmPrincipal.cmbMeses.Text = MonthName(Now.Month)
        'frmPrincipal.cmbAnio.Text = Now.Year
    End Sub

    Sub CargarDataDocumentos()
        Dim StrsqlDoc As String = ""

        If TipoDocumento = 0 Then
            Exit Sub
        End If

        If TipoDocumento = 1 Then               'Entrada
            StrsqlDoc = "Select * from vw_Entradas "
        ElseIf TipoDocumento = 2 Then       'Salida
            StrsqlDoc = "Select * from vw_Salidas "
        ElseIf TipoDocumento = 3 Then       'Ventas
            StrsqlDoc = "Select * from vw_Ventas "
        ElseIf TipoDocumento = 4 Then       'Compras0
            StrsqlDoc = "Select * from vw_Compras "
        ElseIf TipoDocumento = 5 Then       'Transferencias
            StrsqlDoc = "Select * from vw_Transferencias "
        End If

        'If frmPrincipal.cmbTipoVista.SelectedIndex = 0 Then
        '    'StrsqlDoc = StrsqlDoc & "where Fecha_Factura = '" & Format(frmPrincipal.dtpDias.Value, "yyyy/MM/dd") & "' "
        'ElseIf frmPrincipal.cmbTipoVista.SelectedIndex = 1 Then
        '    'StrsqlDoc = StrsqlDoc & "where month(Fecha_Factura) = " & (frmPrincipal.cmbMeses.SelectedIndex + 1) & " and year(Fecha_Factura) = " & CInt(frmPrincipal.cmbAnio.Text) & " "
        'End If
        StrsqlDoc = StrsqlDoc & " order by [N°] asc"
        TblBSDocumentos.DataSource = SQL(StrsqlDoc, "tblDatosTransacciones", My.Settings.SolIndustrialCNX).Tables(0)

        If TipoDocumento = 3 Then       'Ventas
            EncabezadosDoc()
        ElseIf TipoDocumento = 4 Then

        Else
            EncabezadosDoc()
        End If
    End Sub

    Sub EncabezadosDoc()
        GridView1.Columns.Clear()
        GridView1.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "N°"
        gc0.Caption = "N°"
        gc0.FieldName = "N°"
        gc0.Width = 20
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Serie"
        gc1.Caption = "Serie"
        gc1.FieldName = "Serie"
        gc1.Width = 6
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "TipoVenta"
        gc2.Caption = "TipoVenta"
        gc2.FieldName = "TipoVenta"
        gc2.Width = 8
        GridView1.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "Fecha"
        gc3.Caption = "Fecha"
        gc3.FieldName = "Fecha"
        gc3.Width = 30
        GridView1.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3

        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "Vencimiento"
        gc4.Caption = "Vencimiento"
        gc4.FieldName = "Vencimiento"
        gc4.Width = 30
        GridView1.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4

        Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
        gc5.Name = "Cliente"
        gc5.Caption = "Cliente"
        gc5.FieldName = "Cliente"
        gc5.Width = 60
        GridView1.Columns.Add(gc5)
        gc5.Visible = True
        gc5.VisibleIndex = 5

        Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
        gc6.Name = "Observacion"
        gc6.Caption = "Observacion"
        gc6.FieldName = "Observacion"
        gc6.Width = 80
        GridView1.Columns.Add(gc6)
        gc6.Visible = True
        gc6.VisibleIndex = 6

        Dim gc7 As New DevExpress.XtraGrid.Columns.GridColumn
        gc7.Name = "Costo_Venta"
        gc7.Caption = "Costo_Venta"
        gc7.FieldName = "Costo_Venta"
        gc7.DisplayFormat.FormatString = "{0:$0.00}"
        gc7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc7.Width = 30
        GridView1.Columns.Add(gc7)
        gc7.Visible = True
        gc7.VisibleIndex = 7
        GridView1.Columns(7).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(7).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc8 As New DevExpress.XtraGrid.Columns.GridColumn
        gc8.Name = "TotalVenta"
        gc8.Caption = "TotalVenta"
        gc8.FieldName = "TotalVenta"
        gc8.DisplayFormat.FormatString = "{0:$0.00}"
        gc8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc8.Width = 30
        GridView1.Columns.Add(gc8)
        gc8.Visible = True
        gc8.VisibleIndex = 8
        GridView1.Columns(8).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(8).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc9 As New DevExpress.XtraGrid.Columns.GridColumn
        gc9.Name = "TotalDescuento"
        gc9.Caption = "TotalDescuento"
        gc9.FieldName = "TotalDescuento"
        gc9.DisplayFormat.FormatString = "{0:$0.00}"
        gc9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc9.Width = 30
        GridView1.Columns.Add(gc9)
        gc9.Visible = True
        gc9.VisibleIndex = 9
        GridView1.Columns(9).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(9).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc10 As New DevExpress.XtraGrid.Columns.GridColumn
        gc10.Name = "Subtotal"
        gc10.Caption = "Subtotal"
        gc10.FieldName = "Subtotal"
        gc10.DisplayFormat.FormatString = "{0:$0.00}"
        gc10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc10.Width = 30
        GridView1.Columns.Add(gc10)
        gc10.Visible = True
        gc10.VisibleIndex = 10
        GridView1.Columns(10).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(10).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc11 As New DevExpress.XtraGrid.Columns.GridColumn
        gc11.Name = "Impuestos"
        gc11.Caption = "Impuestos"
        gc11.FieldName = "Impuestos"
        gc11.DisplayFormat.FormatString = "{0:$0.00}"
        gc11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc11.Width = 30
        GridView1.Columns.Add(gc11)
        gc11.Visible = True
        gc11.VisibleIndex = 11
        GridView1.Columns(11).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(11).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc12 As New DevExpress.XtraGrid.Columns.GridColumn
        gc12.Name = "RetencionesIR"
        gc12.Caption = "RetencionesIR"
        gc12.FieldName = "RetencionesIR"
        gc12.DisplayFormat.FormatString = "{0:$0.00}"
        gc12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc12.Width = 30
        GridView1.Columns.Add(gc12)
        gc12.Visible = True
        gc12.VisibleIndex = 12
        GridView1.Columns(12).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(12).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc13 As New DevExpress.XtraGrid.Columns.GridColumn
        gc13.Name = "RetencionesIMI"
        gc13.Caption = "RetencionesIMI"
        gc13.FieldName = "RetencionesIMI"
        gc13.DisplayFormat.FormatString = "{0:$0.00}"
        gc13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc13.Width = 30
        GridView1.Columns.Add(gc13)
        gc13.Visible = True
        gc13.VisibleIndex = 13
        GridView1.Columns(13).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(13).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc14 As New DevExpress.XtraGrid.Columns.GridColumn
        gc14.Name = "Total_Neto"
        gc14.Caption = "Total_Neto"
        gc14.FieldName = "Total_Neto"
        gc14.DisplayFormat.FormatString = "{0:$0.00}"
        gc14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc14.Width = 30
        GridView1.Columns.Add(gc14)
        gc14.Visible = True
        gc14.VisibleIndex = 14
        GridView1.Columns(14).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(14).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc15 As New DevExpress.XtraGrid.Columns.GridColumn
        gc15.Name = "Abonos"
        gc15.Caption = "Abonos"
        gc15.FieldName = "Abonos"
        gc15.DisplayFormat.FormatString = "{0:$0.00}"
        gc15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc15.Width = 30
        GridView1.Columns.Add(gc15)
        gc15.Visible = True
        gc15.VisibleIndex = 15
        GridView1.Columns(15).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(15).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc16 As New DevExpress.XtraGrid.Columns.GridColumn
        gc16.Name = "Saldo"
        gc16.Caption = "Saldo"
        gc16.FieldName = "Saldo"
        gc16.DisplayFormat.FormatString = "{0:$0.00}"
        gc16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc16.Width = 30
        GridView1.Columns.Add(gc16)
        gc16.Visible = True
        gc16.VisibleIndex = 16
        GridView1.Columns(16).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(16).SummaryItem.DisplayFormat = "{0:$0.00}"
    End Sub

    Sub EncabezadosDocCompras()
        GridView1.Columns.Clear()
        GridView1.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "N°"
        gc0.Caption = "N°"
        gc0.FieldName = "N°"
        gc0.Width = 20
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Tipo Compra"
        gc1.Caption = "Tipo Compra"
        gc1.FieldName = "Tipo Compra"
        gc1.Width = 12
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "Fecha"
        gc2.Caption = "Fecha"
        gc2.FieldName = "Fecha"
        gc2.Width = 10
        GridView1.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "Vencimiento"
        gc3.Caption = "Vencimiento"
        gc3.FieldName = "Vencimiento"
        gc3.Width = 10
        GridView1.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3

        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "Total_Compra"
        gc4.Caption = "Total_Compra"
        gc4.FieldName = "Total_Compra"
        gc4.DisplayFormat.FormatString = "{0:$0.00}"
        gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc4.Width = 40
        GridView1.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4
        GridView1.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
        gc5.Name = "Impuestos"
        gc5.Caption = "Impuestos"
        gc5.FieldName = "Impuestos"
        gc5.DisplayFormat.FormatString = "{0:$0.00}"
        gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc5.Width = 40
        GridView1.Columns.Add(gc5)
        gc5.Visible = True
        gc5.VisibleIndex = 5
        GridView1.Columns(5).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(5).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
        gc6.Name = "Total_Neto"
        gc6.Caption = "Total_Neto"
        gc6.FieldName = "Total_Neto"
        gc6.DisplayFormat.FormatString = "{0:$0.00}"
        gc6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc6.Width = 40
        GridView1.Columns.Add(gc6)
        gc6.Visible = True
        gc6.VisibleIndex = 6
        GridView1.Columns(6).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc7 As New DevExpress.XtraGrid.Columns.GridColumn
        gc7.Name = "Abonos"
        gc7.Caption = "Abonos"
        gc7.FieldName = "Abonos"
        gc7.DisplayFormat.FormatString = "{0:$0.00}"
        gc7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc7.Width = 40
        GridView1.Columns.Add(gc7)
        gc7.Visible = True
        gc7.VisibleIndex = 7
        GridView1.Columns(7).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(7).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc8 As New DevExpress.XtraGrid.Columns.GridColumn
        gc8.Name = "Saldo"
        gc8.Caption = "Saldo"
        gc8.FieldName = "Saldo"
        gc8.DisplayFormat.FormatString = "{0:$0.00}"
        gc8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc8.Width = 40
        GridView1.Columns.Add(gc8)
        gc8.Visible = True
        gc8.VisibleIndex = 8
        GridView1.Columns(8).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(8).SummaryItem.DisplayFormat = "{0:$0.00}"

    End Sub

    Sub EncabezadosDocVentas()
        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "N°"
        gc0.Caption = "N°"
        gc0.FieldName = "N°"
        gc0.Width = 20
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Serie"
        gc1.Caption = "Serie"
        gc1.FieldName = "Serie"
        gc1.Width = 20
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1
        GridView1.Columns(1).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "Tipo Venta"
        gc2.Caption = "Tipo Venta"
        gc2.FieldName = "Tipo Venta"
        gc2.Width = 12
        GridView1.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "Fecha"
        gc3.Caption = "Fecha"
        gc3.FieldName = "Fecha"
        gc3.Width = 10
        GridView1.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3

        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "Vencimiento"
        gc4.Caption = "Vencimiento"
        gc4.FieldName = "Vencimiento"
        gc4.Width = 10
        GridView1.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4

        Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
        gc5.Name = "Total_Costos"
        gc5.Caption = "Total_Costos"
        gc5.FieldName = "Total_Costos"
        gc5.DisplayFormat.FormatString = "{0:$0.00}"
        gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc5.Width = 40
        GridView1.Columns.Add(gc5)
        gc5.Visible = True
        gc5.VisibleIndex = 5
        GridView1.Columns(5).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(5).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
        gc6.Name = "TotalVenta"
        gc6.Caption = "TotalVenta"
        gc6.FieldName = "TotalVenta"
        gc6.DisplayFormat.FormatString = "{0:$0.00}"
        gc6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc6.Width = 40
        GridView1.Columns.Add(gc6)
        gc6.Visible = True
        gc6.VisibleIndex = 6
        GridView1.Columns(6).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc7 As New DevExpress.XtraGrid.Columns.GridColumn
        gc7.Name = "Impuestos"
        gc7.Caption = "Impuestos"
        gc7.FieldName = "Impuestos"
        gc7.DisplayFormat.FormatString = "{0:$0.00}"
        gc7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc7.Width = 40
        GridView1.Columns.Add(gc7)
        gc7.Visible = True
        gc7.VisibleIndex = 7
        GridView1.Columns(7).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(7).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc8 As New DevExpress.XtraGrid.Columns.GridColumn
        gc8.Name = "Total_Neto"
        gc8.Caption = "Total_Neto"
        gc8.FieldName = "Total_Neto"
        gc8.DisplayFormat.FormatString = "{0:$0.00}"
        gc8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc8.Width = 40
        GridView1.Columns.Add(gc8)
        gc8.Visible = True
        gc8.VisibleIndex = 8
        GridView1.Columns(8).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(8).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc9 As New DevExpress.XtraGrid.Columns.GridColumn
        gc9.Name = "Abonos"
        gc9.Caption = "Abonos"
        gc9.FieldName = "Abonos"
        gc9.DisplayFormat.FormatString = "{0:$0.00}"
        gc9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc9.Width = 40
        GridView1.Columns.Add(gc9)
        gc9.Visible = True
        gc9.VisibleIndex = 9
        GridView1.Columns(9).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(9).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc10 As New DevExpress.XtraGrid.Columns.GridColumn
        gc10.Name = "Saldo"
        gc10.Caption = "Saldo"
        gc10.FieldName = "Saldo"
        gc10.DisplayFormat.FormatString = "{0:$0.00}"
        gc10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc10.Width = 40
        GridView1.Columns.Add(gc10)
        gc10.Visible = True
        gc10.VisibleIndex = 10
        GridView1.Columns(10).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(10).SummaryItem.DisplayFormat = "{0:$0.00}"

    End Sub

    Private Sub btn_Exportar_Click(sender As Object, e As EventArgs) Handles btn_Exportar.Click
        Dim PS As DevExpress.XtraPrinting.PrintingSystem = New DevExpress.XtraPrinting.PrintingSystem
        Dim PCL As DevExpress.XtraPrinting.PrintableComponentLink = New DevExpress.XtraPrinting.PrintableComponentLink(PS)
        PCL.Component = GridControl1
        PCL.Margins.Left = 50 ' = 1/2 inch
        PCL.Margins.Right = 50
        PCL.Margins.Top = 50
        PCL.Margins.Bottom = 50
        PCL.Landscape = True
        PCL.PaperKind = System.Drawing.Printing.PaperKind.LetterRotated
        PCL.EnablePageDialog = True
        PCL.CreateDocument()

        PS.PreviewFormEx.MdiParent = frmPrincipal
        PS.PreviewFormEx.Show()
        PS.PreviewFormEx.Text = "Reporte de Facturación"
        PS.Document.Name = "Reporte de Facturación"
    End Sub

End Class

