﻿Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository

Public Class frmSearch

    Public Sub CargarDatos()
        Dim strsql As String = ""

        Try
            'If NumCatalogo = 22 Then
            '    Dim codigo As String = ""

            '    codigo = frmMasterCompraForanea.txtNumDocumento.Text
            '    strsql = String.Format("SELECT * FROM [dbo].[vwDetallePago] PAG WHERE [PAG].[Cod. Documento] = '{0}'", codigo)

            '    grdBuscar.DataSource = SQL(strsql, "tblDatosgoPagos", My.Settings.SolIndustrialCNX).Tables(0)

            'Else

            If NumCatalogo = 21 Or NumCatalogo = 22 Then
                strsql = "SELECT Fecha, Tasa_Oficial FROM Tbl_TasasCambio ORDER BY Fecha DESC"

                grdBuscar.DataSource = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            ElseIf NumCatalogo = 23 Then
                strsql = "SELECT * FROM [dbo].[vwVerOrdenProduccion] ODP ORDER BY [ODP].[Registrado] DESC"

                grdBuscar.DataSource = SQL(strsql, "tblDatosOrdenes", My.Settings.SolIndustrialCNX).Tables(0)

            ElseIf NumCatalogo = 24 Then
                strsql = "select Codigo_Producto AS Codigo, Nombre_Producto AS Nombre from [dbo].[Productos] order by Codigo_Alterno"

                grdBuscar.DataSource = SQL(strsql, "tblDatosProductos", My.Settings.SolIndustrialCNX).Tables(0)
                grvBuscar.Columns(0).Width = 5
                grvBuscar.Columns(1).Width = 200

            ElseIf NumCatalogo = 25 Then
                strsql = "SELECT Mov.Codigo_Movimiento As Codigo,CAST(Mov.Fecha AS Date) AS Fecha,  Mov.Codigo_Documento As 'No. Documento', Categorias_Detalle.Descripcion As 'Tipo Documento', " & _
                         "Mov.Comentario, Mov.Total_Neto As 'Costo Total' FROM Movimientos_Almacenes AS Mov INNER JOIN Categorias_Detalle ON Mov.Tipo_Documento = Categorias_Detalle.IdCategoriaDetalle " & _
                         "WHERE (Mov.Tipo_Movimiento = 4) AND Mov.Anulada = 0 ORDER BY Fecha DESC "

                grdBuscar.DataSource = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)
                grvBuscar.Columns(0).Width = 30
                grvBuscar.Columns(1).Width = 30
                grvBuscar.Columns(2).Width = 30
                grvBuscar.Columns(3).Width = 80
                grvBuscar.Columns(4).Width = 120
                grvBuscar.Columns(5).Width = 30

            ElseIf NumCatalogo = 26 Then
                strsql = "SELECT Mov.Codigo_Movimiento As Codigo, CAST(Mov.Fecha AS Date) AS Fecha,  Mov.Codigo_Documento As 'No. Documento', Categorias_Detalle.Descripcion As 'Tipo Documento', " & _
                         "Mov.Comentario, Mov.Total_Neto As 'Costo Total' FROM Movimientos_Almacenes AS Mov INNER JOIN Categorias_Detalle ON Mov.Tipo_Documento = Categorias_Detalle.IdCategoriaDetalle " & _
                         "WHERE (Mov.Tipo_Movimiento = 5) AND Mov.Anulada = 0 ORDER BY Fecha DESC "

                grdBuscar.DataSource = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            End If

        Catch ex As Exception
        End Try
    End Sub

    Public Sub Cerrar()
        'nTipoEdic = 2
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmSearchOrdenProd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            CargarDatos()
    End Sub

    Private Sub cmdAceptar_Click_1(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Dim Codigo As String = ""
        Dim Dato1 As String = ""
        Try
            If NumCatalogo = 21 Then
                Codigo = grvBuscar.GetRowCellValue(grvBuscar.GetSelectedRows(0), "Tasa_Oficial").ToString()
                frmMasterCompraLocal.txtTazaCambio.Text = Codigo
                Cerrar()
            ElseIf NumCatalogo = 22 Then
                Codigo = grvBuscar.GetRowCellValue(grvBuscar.GetSelectedRows(0), "Tasa_Oficial").ToString()
                frmMasterCompraForanea.txtTasaCambio.Text = Codigo
                Cerrar()
            ElseIf NumCatalogo = 23 Then
                Codigo = grvBuscar.GetRowCellValue(grvBuscar.GetSelectedRows(0), "Orden").ToString()
                frmOrdenProd.CargarDatos(Codigo)
                Cerrar()
            ElseIf NumCatalogo = 24 Then
                Codigo = grvBuscar.GetRowCellValue(grvBuscar.GetSelectedRows(0), "Codigo").ToString()
                frmKardexInv.CargarDatos(Codigo)
                Cerrar()
            ElseIf NumCatalogo = 25 Then
                Codigo = grvBuscar.GetRowCellValue(grvBuscar.GetSelectedRows(0), "Codigo").ToString()
                frmEntradaSalida.CargarDatos(Codigo, frmEntradaSalida.cmbBodega.SelectedValue)
                Cerrar()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cmdCancelar_Click_1(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub
End Class