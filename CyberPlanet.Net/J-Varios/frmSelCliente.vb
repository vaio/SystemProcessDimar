﻿Public Class frmSelCliente

Private Sub frmSelCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		CargarClientes()

End Sub

Public Sub CargarClientes()
		Dim strSql As String = "Select Id_Cliente, Nombre_Cliente_Empresa from Tbl_Clientes where Activo=1 order by Id_Cliente"
        TblClienteBindingSource.DataSource = SQL(strSql, "tblCliente", My.Settings.SolIndustrialCNX).Tables(0)
		luCliente.Properties.DataSource = TblClienteBindingSource
		luCliente.Properties.DisplayMember = "Nombre_Cliente_Empresa"
		luCliente.Properties.ValueMember = "Id_Cliente"
		luCliente.ItemIndex = 0
End Sub

	Public Sub Cerrar()
		Me.Dispose()
		Close()
	End Sub


Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
		nCodCliente = luCliente.EditValue
		Cerrar()
End Sub

Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
		nCodCliente = luCliente.EditValue
		Cerrar()
End Sub
End Class