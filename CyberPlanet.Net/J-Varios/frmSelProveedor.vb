﻿Public Class frmSelProveedor

Private Sub frmSelProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
	CargarProveedores()
End Sub

Public Sub CargarProveedores()
		Dim strSql As String = "Select Codigo_Proveedor, Nombre_Proveedor from Tbl_Proveedor where Activo=1 order by Codigo_Proveedor"
        TblProvBindingSource.DataSource = SQL(strSql, "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
		luProveedor.Properties.DataSource = TblProvBindingSource
		luProveedor.Properties.DisplayMember = "Nombre_Proveedor"
		luProveedor.Properties.ValueMember = "Codigo_Proveedor"
		luProveedor.ItemIndex = 0
End Sub

Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
			frmCompraRecargas.lblFormaPago.BackColor = Color.Gainsboro
			frmCompraRecargas.chkFormaPago.Text = "Credito"
			nIsCredito = 0
			nCodProveedor = 0
			Cerrar()
End Sub

	Public Sub Cerrar()
		Me.Dispose()
		Close()
	End Sub

Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
		nCodProveedor = luProveedor.EditValue
		Cerrar()
End Sub
End Class