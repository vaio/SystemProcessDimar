﻿
Imports System.Data.SqlClient

Public Class clsCatalogos

    Implements IDisposable

    Dim mconexion As String
    Dim strSql As String
    Dim isWebService As Boolean = False

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

    Public Sub New(ByVal ConextionString As String)
        mconexion = ConextionString
    End Sub

    Public Function Sql(ByVal strSql As String) As DataSet
        Dim cn As New SqlConnection
        cn.ConnectionString = mconexion
        Try
            cn.Open()
            Dim sqlda As New SqlDataAdapter(strSql, cn)
            Dim ds As New DataSet
            sqlda.Fill(ds, "tblNotas")
            Sql = ds
        Catch ex As Exception
			Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Sql = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    Public Function EditarProdtInvent(ByVal CodBodega As Integer, ByVal CodProd As String, ByVal TipoEdicionBod As Integer)
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdProdtInvent As New SqlCommand("SP_InOut_Bodega", cnConec)
            cmdProdtInvent.CommandType = CommandType.StoredProcedure
            cmdProdtInvent.Parameters.Add("@CodBodega", SqlDbType.Int).Value = CodBodega
            cmdProdtInvent.Parameters.Add("@CodProd", SqlDbType.VarChar).Value = CodProd
            cmdProdtInvent.Parameters.Add("@nTipoEdicionBod", SqlDbType.Int).Value = TipoEdicionBod
            cmdProdtInvent.ExecuteReader()

            EditarProdtInvent = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarProdtInvent = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarProdtPrecio(ByVal CodProducto As String, ByVal CodTipo As String, ByVal Cordobas As Double, ByVal TipoEdicion As Integer)
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdProdtInvent As New SqlCommand("SP_AME_Precio", cnConec)
            cmdProdtInvent.CommandType = CommandType.StoredProcedure
            cmdProdtInvent.Parameters.Add("@CodProducto", SqlDbType.VarChar).Value = CodProducto
            cmdProdtInvent.Parameters.Add("@CodTipo", SqlDbType.VarChar).Value = CodTipo
            cmdProdtInvent.Parameters.Add("@Cordoba", SqlDbType.Money).Value = Cordobas
            cmdProdtInvent.Parameters.Add("@Dolares", SqlDbType.Money).Value = 0
            cmdProdtInvent.Parameters.Add("@Euros", SqlDbType.Money).Value = 0
            cmdProdtInvent.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Date.Now
            cmdProdtInvent.Parameters.Add("@Activo", SqlDbType.Bit).Value = True
            cmdProdtInvent.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdProdtInvent.ExecuteReader()

            EditarProdtPrecio = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarProdtPrecio = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarBodegas(ByVal Sucursal As Integer, ByVal IdBodega As Integer, ByVal NombreBodega As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Almacen", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@sucursal", SqlDbType.Int).Value = Sucursal
            cmdComisiones.Parameters.Add("@CodBodega", SqlDbType.Int).Value = IdBodega
            cmdComisiones.Parameters.Add("@NombreBodega", SqlDbType.NVarChar).Value = NombreBodega
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarBodegas = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarBodegas = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarMarcas(ByVal IdMarca As Integer, ByVal NombreMarca As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Marcas", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdMarca", SqlDbType.Int).Value = IdMarca
            cmdComisiones.Parameters.Add("@NombreMarca", SqlDbType.NVarChar).Value = NombreMarca
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarMarcas = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMarcas = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarModelos(ByVal Id_Modelo As Integer, ByVal Nombre_Modelo As String, ByVal Codigo_Marca As Integer, ByVal TipoEdicion As Integer, ByVal Pre_marca As Integer, pre_modelo As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Modelos", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdModelo", SqlDbType.Int).Value = Id_Modelo
            cmdComisiones.Parameters.Add("@NombreModelo", SqlDbType.NVarChar).Value = Nombre_Modelo
            cmdComisiones.Parameters.Add("@Idmarca", SqlDbType.Int).Value = Codigo_Marca
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.Parameters.Add("@pre_marca", SqlDbType.Int).Value = Pre_marca
            cmdComisiones.Parameters.Add("@pre_modelo", SqlDbType.Int).Value = pre_modelo
            cmdComisiones.ExecuteReader()

            EditarModelos = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarModelos = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarLinea(ByVal IdLinea As Integer, ByVal NombreLinea As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Lineas", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdLinea", SqlDbType.Int).Value = IdLinea
            cmdComisiones.Parameters.Add("@NombreLinea", SqlDbType.NVarChar).Value = NombreLinea
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarLinea = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarLinea = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarCategoria(ByVal Codigo_Categoria As String, ByVal Nombre_Categoria As String, ByVal Codigo_Linea As Integer, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Categoria", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdCateg", SqlDbType.VarChar).Value = Codigo_Categoria
            cmdComisiones.Parameters.Add("@NombreCateg", SqlDbType.NVarChar).Value = Nombre_Categoria
            cmdComisiones.Parameters.Add("@IdLinea", SqlDbType.Int).Value = Codigo_Linea
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarCategoria = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCategoria = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function MigrarActualizacionDatos(ByVal nTipUpdate As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
          .ConnectionString = mconexion
          .Open()
        End With

        Try
          Dim cmdMigrarData As New SqlCommand("[SP_MigrarDatos]", cnConec)
          cmdMigrarData.CommandType = CommandType.StoredProcedure
          'cmdMigrarData.Parameters.Add("@TipoUpdate", SqlDbType.Int).Value = nTipUpdate
          cmdMigrarData.ExecuteReader()

          MigrarActualizacionDatos = "OK"

        Catch ex As SqlException
          MsgBox("Error: " + ex.Message)
          MigrarActualizacionDatos = "ERROR"
        Finally
          cnConec.Close()
          cnConec.Dispose()
        End Try

    End Function

    Public Function EditarProv(ByVal sCodigoProveedor As String, ByVal sNum_RUC As String, ByVal sNombreProveedor As String, ByVal sNombreContacto As String, ByVal sTelProv As String,
      ByVal sTelContac As String, ByVal sEmail As String, ByVal sDireccion1 As String, ByVal sDireccion2 As String, ByVal nTipProv As Integer, ByVal nProced As Integer, ByVal nTipLiquid As Integer,
      ByVal nTipCompra As Integer, ByVal nPlazo As Integer, ByVal mLimitCred As Double, ByVal nIsIR As Integer, ByVal nIsIMI As Integer, ByVal nIsIVA As Integer, ByVal nTasaIR As Integer, ByVal nTasaIMI As Integer, ByVal nTasaIVA As Integer, ByVal sCuentaContable As String, ByVal TipoEdicion As Integer)

        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
          .ConnectionString = mconexion
          .Open()
        End With

        Try
          Dim cmdComisiones As New SqlCommand("[SP_AME_Proveedor]", cnConec)
          cmdComisiones.CommandType = CommandType.StoredProcedure
          cmdComisiones.Parameters.Add("@IdProv", SqlDbType.NVarChar).Value = sCodigoProveedor
          cmdComisiones.Parameters.Add("@NumRUC", SqlDbType.NVarChar).Value = sNum_RUC
          cmdComisiones.Parameters.Add("@NombreProv", SqlDbType.NVarChar).Value = sNombreProveedor
          cmdComisiones.Parameters.Add("@NombreContac", SqlDbType.NVarChar).Value = sNombreContacto
          cmdComisiones.Parameters.Add("@TelefProv", SqlDbType.NVarChar).Value = sTelProv
          cmdComisiones.Parameters.Add("@TelefContac", SqlDbType.NVarChar).Value = sTelContac
          cmdComisiones.Parameters.Add("@email", SqlDbType.NVarChar).Value = sEmail
          cmdComisiones.Parameters.Add("@DirOfic1", SqlDbType.NVarChar).Value = sDireccion1
          cmdComisiones.Parameters.Add("@DirOfic2", SqlDbType.NVarChar).Value = sDireccion2
          cmdComisiones.Parameters.Add("@TipoProv", SqlDbType.Int).Value = nTipProv
          cmdComisiones.Parameters.Add("@Proced", SqlDbType.Int).Value = nProced
          cmdComisiones.Parameters.Add("@TipLiquid", SqlDbType.Int).Value = nTipLiquid
          cmdComisiones.Parameters.Add("@TipComp", SqlDbType.Int).Value = nTipCompra
          cmdComisiones.Parameters.Add("@Plazo", SqlDbType.Int).Value = nPlazo
          cmdComisiones.Parameters.Add("@LimitCred", SqlDbType.Money).Value = mLimitCred
          cmdComisiones.Parameters.Add("@IsIR", SqlDbType.Int).Value = nIsIR
          cmdComisiones.Parameters.Add("@IsIMI", SqlDbType.Int).Value = nIsIMI
          cmdComisiones.Parameters.Add("@IsIVA", SqlDbType.Int).Value = nIsIVA
          cmdComisiones.Parameters.Add("@TasaIR", SqlDbType.Int).Value = nTasaIR
          cmdComisiones.Parameters.Add("@TasaIMI", SqlDbType.Int).Value = nTasaIMI
          cmdComisiones.Parameters.Add("@TasaIVA", SqlDbType.Int).Value = nTasaIVA
          cmdComisiones.Parameters.Add("@CuentaCont", SqlDbType.NVarChar).Value = sCuentaContable
          cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
          cmdComisiones.ExecuteReader()

          EditarProv = "OK"

        Catch ex As SqlException
          MsgBox("Error: " + ex.Message)
          EditarProv = "ERROR"
        Finally
          cnConec.Close()
          cnConec.Dispose()
        End Try

      End Function

    Public Function EditarProducto(ByVal Id_Producto As String, ByVal Nombre_Producto As String, ByVal CodAlterno As String, ByVal Nombre_Generico As String, ByVal TipArticulo As Integer,
                                   ByVal Codigo_Proveedor As String, ByVal Codigo_Linea As Integer, ByVal Codigo_Categoria As String, ByVal Codigo_Marca As Integer, ByVal Id_Modelo As Integer,
                                   ByVal Codigo_UnidMed As Integer, ByVal Codigo_Present As Integer, ByVal ExistMin As Integer, ByVal ExistMax As Integer, ByVal IsPackage As Integer, UnidxPac As Integer,
                                   ByVal IsExcento As Integer, ByVal CostoCS As Double, ByVal CostoUS As Double, ByVal TipoEdicion As Integer, ByVal TipoProducto As Integer, ByVal Descontinuado As Boolean,
                                   ByVal SeFactura As Boolean, ByVal Ingreso As Date?, ByVal Baja As Date?)

        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With
        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Productos", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdProducto", SqlDbType.VarChar).Value = Id_Producto
            cmdComisiones.Parameters.Add("@NombreProd", SqlDbType.NVarChar).Value = Nombre_Producto
            cmdComisiones.Parameters.Add("@CodAlterno", SqlDbType.NVarChar).Value = CodAlterno
            cmdComisiones.Parameters.Add("@NombreGenerico", SqlDbType.NVarChar).Value = Nombre_Generico
            cmdComisiones.Parameters.Add("@TipoArtic", SqlDbType.Int).Value = TipArticulo
            cmdComisiones.Parameters.Add("@IdProv", SqlDbType.VarChar).Value = Codigo_Proveedor
            cmdComisiones.Parameters.Add("@IdLinea", SqlDbType.Int).Value = Codigo_Linea
            cmdComisiones.Parameters.Add("@IdCateg", SqlDbType.VarChar).Value = Codigo_Categoria
            cmdComisiones.Parameters.Add("@IdMarca", SqlDbType.Int).Value = Codigo_Marca
            cmdComisiones.Parameters.Add("@IdModelo", SqlDbType.Int).Value = Id_Modelo
            cmdComisiones.Parameters.Add("@IdUnidMed", SqlDbType.Int).Value = Codigo_UnidMed
            cmdComisiones.Parameters.Add("@IdPresent", SqlDbType.Int).Value = Codigo_Present

            cmdComisiones.Parameters.Add("@ExitMin", SqlDbType.Int).Value = ExistMin
            cmdComisiones.Parameters.Add("@ExistMax", SqlDbType.Int).Value = ExistMax
            cmdComisiones.Parameters.Add("@InPakage", SqlDbType.Int).Value = IsPackage
            cmdComisiones.Parameters.Add("@UnitxPakage", SqlDbType.Int).Value = UnidxPac

            cmdComisiones.Parameters.Add("@IsExcento", SqlDbType.Int).Value = IsExcento
            cmdComisiones.Parameters.Add("@CostoCS", SqlDbType.Float).Value = CostoCS
            cmdComisiones.Parameters.Add("@CostoUSS", SqlDbType.Float).Value = CostoUS

            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.Parameters.Add("@nTipoProducto", SqlDbType.Int).Value = TipoProducto
            cmdComisiones.Parameters.Add("@Descontinuado", SqlDbType.Bit).Value = Descontinuado
            cmdComisiones.Parameters.Add("@SeFactura", SqlDbType.Bit).Value = SeFactura


            If Ingreso Is Nothing Or Ingreso = "12:00:00 AM" Then
                cmdComisiones.Parameters.Add("@Ingreso", SqlDbType.DateTime).Value = DBNull.Value
            Else
                cmdComisiones.Parameters.Add("@Ingreso", SqlDbType.DateTime).Value = Ingreso
            End If

            If Baja Is Nothing Or Baja = "12:00:00 AM" Then
                cmdComisiones.Parameters.Add("@Baja", SqlDbType.DateTime).Value = DBNull.Value
            Else
                cmdComisiones.Parameters.Add("@Baja", SqlDbType.DateTime).Value = Baja
            End If

            cmdComisiones.ExecuteReader()

            EditarProducto = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarProducto = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarDepartPais(ByVal IdDepartPais As Integer, ByVal NombreDepartPais As String, ByVal TipoEdicion As Integer)
      Dim ID As String = ""
      Dim cnConec As New SqlConnection

      With cnConec
        .ConnectionString = mconexion
        .Open()
      End With

      Try
        Dim cmdComisiones As New SqlCommand("SP_AME_DepartamentosPais", cnConec)
        cmdComisiones.CommandType = CommandType.StoredProcedure
        cmdComisiones.Parameters.Add("@IdDepartPais", SqlDbType.Int).Value = IdDepartPais
        cmdComisiones.Parameters.Add("@NombreDepartPais", SqlDbType.NVarChar).Value = NombreDepartPais
        cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
        cmdComisiones.ExecuteReader()

        EditarDepartPais = "OK"

      Catch ex As SqlException
        MsgBox("Error: " + ex.Message)
        EditarDepartPais = "ERROR"
      Finally
        cnConec.Close()
        cnConec.Dispose()
      End Try

    End Function

    Public Function EditarMunicipio(ByVal Id_Municipio As Integer, ByVal Nombre_Municipio As String, ByVal Codigo_LineaPais As Integer, ByVal TipoEdicion As Integer)
      Dim ID As String = ""
      Dim cnConec As New SqlConnection

      With cnConec
        .ConnectionString = mconexion
        .Open()
      End With

      Try
        Dim cmdComisiones As New SqlCommand("SP_AME_Municipio", cnConec)
        cmdComisiones.CommandType = CommandType.StoredProcedure
        cmdComisiones.Parameters.Add("@IdMunicipio", SqlDbType.Int).Value = Id_Municipio
        cmdComisiones.Parameters.Add("@NombreMunicipio", SqlDbType.NVarChar).Value = Nombre_Municipio
        cmdComisiones.Parameters.Add("@IdDepartPais", SqlDbType.Int).Value = Codigo_LineaPais
        cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
        cmdComisiones.ExecuteReader()

        EditarMunicipio = "OK"

      Catch ex As SqlException
        MsgBox("Error: " + ex.Message)
        EditarMunicipio = "ERROR"
      Finally
        cnConec.Close()
        cnConec.Dispose()
      End Try

    End Function

    Public Function EditarDistritoZona(ByVal Id_DistZona As Integer, ByVal Nombre_DistZona As String, ByVal Id_Municipio As Integer, ByVal TipoEdicion As Integer)
      Dim ID As String = ""
      Dim cnConec As New SqlConnection

      With cnConec
        .ConnectionString = mconexion
        .Open()
      End With

      Try
        Dim cmdComisiones As New SqlCommand("SP_AME_DistZona", cnConec)
        cmdComisiones.CommandType = CommandType.StoredProcedure
        cmdComisiones.Parameters.Add("@IdDistZona", SqlDbType.Int).Value = Id_DistZona
        cmdComisiones.Parameters.Add("@NombreDistZona", SqlDbType.NVarChar).Value = Nombre_DistZona
        cmdComisiones.Parameters.Add("@IdMunicipio", SqlDbType.Int).Value = Id_Municipio
        cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
        cmdComisiones.ExecuteReader()

        EditarDistritoZona = "OK"

      Catch ex As SqlException
        MsgBox("Error: " + ex.Message)
        EditarDistritoZona = "ERROR"
      Finally
        cnConec.Close()
        cnConec.Dispose()
      End Try

    End Function

    Public Function EditarBarrio(ByVal Id_Barrio As Integer, ByVal Nombre_Barrio As String, ByVal Id_Zona_Distrito As Integer, ByVal TipoEdicion As Integer)
      Dim ID As String = ""
      Dim cnConec As New SqlConnection

      With cnConec
        .ConnectionString = mconexion
        .Open()
      End With

      Try
        Dim cmdComisiones As New SqlCommand("SP_AME_Barrio", cnConec)
        cmdComisiones.CommandType = CommandType.StoredProcedure
        cmdComisiones.Parameters.Add("@IdBarrio", SqlDbType.Int).Value = Id_Barrio
        cmdComisiones.Parameters.Add("@NombreBarrio", SqlDbType.NVarChar).Value = Nombre_Barrio
        cmdComisiones.Parameters.Add("@IdDistZona", SqlDbType.Int).Value = Id_Zona_Distrito
        cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
        cmdComisiones.ExecuteReader()

        EditarBarrio = "OK"

      Catch ex As SqlException
        MsgBox("Error: " + ex.Message)
        EditarBarrio = "ERROR"
      Finally
        cnConec.Close()
        cnConec.Dispose()
      End Try

    End Function

    Public Function EditarCliente(ByVal TipoEdicion As Integer, codigo As String, cedula As String, nombre As String, apellido As String, telefono1 As String, telefono2 As String,
                                  correo1 As String, correo2 As String, direccion1 As String, direccion2 As String, fechaI As Date, fechaF As Date, activo As Boolean, clasificacion As String,
                                  departamento As String, municipio As String, distrito As String, barrio As String, correo3 As String, telefono3 As String, direccion3 As String, Ingresos As Decimal,
                                  Egresos As Decimal, cantCredito As Decimal, LimiteCredito As Decimal, Saldo As Decimal, SolicitudesPendiente As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Cliente", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            With cmdComisiones.Parameters
                .AddWithValue("@tipo_edicion", SqlDbType.Int).Value = TipoEdicion
                .AddWithValue("@codigo", SqlDbType.VarChar).Value = codigo
                .AddWithValue("@cedula", SqlDbType.VarChar).Value = cedula
                .AddWithValue("nombre", SqlDbType.VarChar).Value = nombre
                .AddWithValue("@apellido", SqlDbType.VarChar).Value = apellido
                .AddWithValue("@direccion1", SqlDbType.VarChar).Value = direccion1
                .AddWithValue("@direccion2", SqlDbType.VarChar).Value = direccion2
                .AddWithValue("@direccion3", SqlDbType.VarChar).Value = direccion3
                .AddWithValue("@telefono1", SqlDbType.VarChar).Value = telefono1
                .AddWithValue("@telefono2", SqlDbType.VarChar).Value = telefono2
                .AddWithValue("@telefono3", SqlDbType.VarChar).Value = telefono3
                .AddWithValue("@correo1", SqlDbType.VarChar).Value = correo1
                .AddWithValue("@correo2", SqlDbType.VarChar).Value = correo2
                .AddWithValue("@correo3", SqlDbType.VarChar).Value = correo3
                .AddWithValue("@ingreso", SqlDbType.Money).Value = Ingresos
                .AddWithValue("@egreso", SqlDbType.Money).Value = Egresos
                .AddWithValue("@limite_credito", SqlDbType.Money).Value = LimiteCredito
                .AddWithValue("@solicitud_pendiente", SqlDbType.Int).Value = SolicitudesPendiente
                .AddWithValue("@cant_credito", SqlDbType.Int).Value = cantCredito
                .AddWithValue("@saldo", SqlDbType.Money).Value = Saldo
                .AddWithValue("@clasificacion", SqlDbType.Int).Value = clasificacion
                .AddWithValue("@departamento", SqlDbType.VarChar).Value = departamento
                .AddWithValue("@municipio", SqlDbType.VarChar).Value = municipio
                .AddWithValue("@distrito", SqlDbType.VarChar).Value = distrito
                .AddWithValue("@barrio", SqlDbType.VarChar).Value = barrio
                .AddWithValue("@fechaI", SqlDbType.Date).Value = fechaI
                .AddWithValue("@fechaF", SqlDbType.Date).Value = fechaF
                .AddWithValue("@activo", SqlDbType.Bit).Value = activo
            End With

            cmdComisiones.ExecuteReader()

            EditarCliente = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCliente = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function
    Public Function EditarTransUnion(ByVal Edicion As Integer, Id_Cliente As String, Nombre As String, Apellido As String, Cedula As String, Id_Ruta As Integer, Observacion As String, Fecha As Date, Estado As Integer, dimar As Boolean, inficsa As Boolean, barrio As Integer)
        Dim cnConec As New SqlConnection
        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With
        Try
            Dim cmd As New SqlCommand("SP_Transunion", cnConec)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@edicion", Edicion)
                .AddWithValue("@codigo", Id_Cliente)
                .AddWithValue("@nombre", Nombre)
                .AddWithValue("@apellido", Apellido)
                .AddWithValue("@cedula", Cedula)
                .AddWithValue("@ruta", Id_Ruta)
                .AddWithValue("@observacion", Observacion)
                .AddWithValue("@fecha", Fecha)
                .AddWithValue("@estado", Estado)
                .AddWithValue("@dimar", dimar)
                .AddWithValue("@inficsa", inficsa)
                .AddWithValue("@barrio", barrio)
            End With
            cmd.ExecuteReader()
            EditarTransUnion = "OK"
        Catch ex As Exception
            cnConec.Close()
            cnConec.Dispose()
            MsgBox("Error: " + ex.Message)
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarColaboradores(ByVal Id_Colaborador As Integer, ByVal Nombre_Colabordor As String, Apellido_Colaborador As String, ByVal Telefono As String, ByVal Salario As String, ByVal TipoEdicion As Integer, identifiacion As String, inss As String,
                                        profesion As Integer, area As Integer, cargo As Integer, activo As Boolean, dependencia As Integer, direccion As String, fecha As Date)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Colaborador", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdColab", SqlDbType.Int).Value = Id_Colaborador
            cmdComisiones.Parameters.Add("@NombreColab", SqlDbType.NVarChar).Value = Nombre_Colabordor
            cmdComisiones.Parameters.Add("@SalMens", SqlDbType.NVarChar).Value = Salario
            cmdComisiones.Parameters.Add("@TelefContac", SqlDbType.NVarChar).Value = Telefono
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            With cmdComisiones.Parameters
                .AddWithValue("@ApellidoColab", Apellido_Colaborador)
                .AddWithValue("@identificacion", identifiacion)
                .AddWithValue("@inss", inss)
                .AddWithValue("@profesion", profesion)
                .AddWithValue("@area", area)
                .AddWithValue("@cargo", cargo)
                .AddWithValue("@activo", activo)
                .AddWithValue("@dependencia", dependencia)
                .AddWithValue("@fecha", fecha)
                .AddWithValue("@direccion", direccion)
            End With
            cmdComisiones.ExecuteReader()

            EditarColaboradores = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarColaboradores = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarTipoOperaciones(ByVal IdTipoOper As Integer, ByVal NombreTipoOper As String, ByVal EsIngreso As Boolean, ByVal CuentaCont As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_TipoOperaciones", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdTipoOper", SqlDbType.Int).Value = IdTipoOper
            cmdComisiones.Parameters.Add("@NombreTipoOper", SqlDbType.NVarChar).Value = NombreTipoOper
            cmdComisiones.Parameters.Add("@EsIngreso", SqlDbType.NVarChar).Value = EsIngreso
            cmdComisiones.Parameters.Add("@CuentaCont", SqlDbType.Int).Value = CuentaCont
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarTipoOperaciones = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarTipoOperaciones = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarTerminales(ByVal IdTerminal As Integer, ByVal NombreTerminal As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Terminales", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdTerminal", SqlDbType.Int).Value = IdTerminal
            cmdComisiones.Parameters.Add("@NombreTerminal", SqlDbType.NVarChar).Value = NombreTerminal
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarTerminales = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarTerminales = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarUnidMed(ByVal IdUnidMed As Integer, ByVal NombreUnidMed As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_UnidMed", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdUnidMed", SqlDbType.Int).Value = IdUnidMed
            cmdComisiones.Parameters.Add("@NombreUnidMed", SqlDbType.NVarChar).Value = NombreUnidMed
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarUnidMed = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarUnidMed = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarPresent(ByVal IdPresent As Integer, ByVal NombrePresent As String, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdComisiones As New SqlCommand("SP_AME_Present", cnConec)
            cmdComisiones.CommandType = CommandType.StoredProcedure
            cmdComisiones.Parameters.Add("@IdPresent", SqlDbType.Int).Value = IdPresent
            cmdComisiones.Parameters.Add("@NombrePresent", SqlDbType.NVarChar).Value = NombrePresent
            cmdComisiones.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdComisiones.ExecuteReader()

            EditarPresent = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarPresent = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

End Class
