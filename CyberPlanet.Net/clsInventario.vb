﻿
Imports System.Data.SqlClient

Public Class clsInventario

    Implements IDisposable

    Dim mconexion As String
    Dim strSql As String
    Dim isWebService As Boolean = False

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

    Public Sub New(ByVal ConextionString As String)
        mconexion = ConextionString
    End Sub

    Public Function Sql(ByVal strSql As String) As DataSet
        Dim cn As New SqlConnection
        cn.ConnectionString = mconexion
        Try
            cn.Open()
            Dim sqlda As New SqlDataAdapter(strSql, cn)
            Dim ds As New DataSet
            sqlda.Fill(ds, "tblNotas")
            Sql = ds
        Catch ex As Exception
            Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Sql = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    Public Function RegMaxMovimiento(ByVal Bodega As Integer, ByVal Movimiento As Integer)
        strSql = "SELECT isnull(MAX(Codigo_Movimiento)+1,1) AS Maximo FROM Movimientos_Almacenes "
        strSql = strSql & "WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Bodega = " & Bodega & " and Tipo_Movimiento = " & Movimiento
        Dim tblRegMax As DataTable = Sql(strSql).Tables(0)
        RegMaxMovimiento = tblRegMax.Rows(0).Item(0)
    End Function

    Public Function EditarMovimientoAlmacen(ByVal CodigoMovimiento As String, ByVal CodigoSucursal As Integer, ByVal CodigoDocumento As String,
                                     ByVal Tipo_Documento As Integer, ByVal Comentario As String, ByVal Total_Neto As Double, ByVal Fecha As Date?,
                                     ByVal Tipo_Movimiento As Integer, ByVal Anulada As Double, ByVal nTipoEdicion As Integer, Bodega As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdReceta As New SqlCommand("SP_AME_Movimientos_Almacenes", cnConec)
            cmdReceta.CommandType = CommandType.StoredProcedure
            cmdReceta.Parameters.Add("@CodigoMovimiento", SqlDbType.VarChar).Value = CodigoMovimiento
            cmdReceta.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdReceta.Parameters.Add("@CodigoDocumento", SqlDbType.VarChar).Value = CodigoDocumento

            cmdReceta.Parameters.Add("@Tipo_Documento", SqlDbType.Int).Value = Tipo_Documento
            cmdReceta.Parameters.Add("@Comentario", SqlDbType.VarChar).Value = Comentario
            cmdReceta.Parameters.Add("@Total_Neto", SqlDbType.Money).Value = Total_Neto

            If Fecha Is Nothing Then
                cmdReceta.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = DBNull.Value
            Else
                cmdReceta.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
            End If

            cmdReceta.Parameters.Add("@Tipo_Movimiento", SqlDbType.Int).Value = Tipo_Movimiento
            cmdReceta.Parameters.Add("@Anulada", SqlDbType.Bit).Value = Anulada
            cmdReceta.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdReceta.Parameters.Add("@Bodega", SqlDbType.Int).Value = Bodega
            cmdReceta.ExecuteReader()

            EditarMovimientoAlmacen = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMovimientoAlmacen = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarMovimientoAlmacenDetalle(ByVal Codigo_Movimiento As String, ByVal Sucursal As Integer, ByVal Codigo_Producto As String, ByVal Cantidad As Double,
                                            ByVal Precio_Unitario As Double, ByVal Codigo_Factura As String, ByVal Bodega As Integer,
                                            ByVal Anulada As Boolean, ByVal nTipoEdicion As Integer, TipoMovimiento As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdReceta As New SqlCommand("SP_AME_Movimientos_Almacenes_Detalle", cnConec)
            cmdReceta.CommandType = CommandType.StoredProcedure
            cmdReceta.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = Sucursal
            cmdReceta.Parameters.Add("@Bodega", SqlDbType.Int).Value = Bodega
            cmdReceta.Parameters.Add("@Tipo_Movimiento", SqlDbType.Int).Value = TipoMovimiento
            cmdReceta.Parameters.Add("@Codigo_Movimiento", SqlDbType.VarChar).Value = Codigo_Movimiento
            cmdReceta.Parameters.Add("@Codigo_Producto", SqlDbType.VarChar).Value = Codigo_Producto
            cmdReceta.Parameters.Add("@Cantidad", SqlDbType.Float).Value = Cantidad
            cmdReceta.Parameters.Add("@Precio_Unitario", SqlDbType.Float).Value = Precio_Unitario
            cmdReceta.Parameters.Add("@Codigo_Factura", SqlDbType.VarChar).Value = Codigo_Factura
            cmdReceta.Parameters.Add("@Anulada", SqlDbType.Bit).Value = Anulada
            cmdReceta.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion

            cmdReceta.ExecuteReader()

            EditarMovimientoAlmacenDetalle = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMovimientoAlmacenDetalle = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarTraslado(ByVal CodigoSucursal As Integer, ByVal Codigo_Traslado As String, ByVal Bodega_Origen As Integer,
                                 ByVal Bodega_Destino As Integer, ByVal Comentario As String, ByVal Fecha As Date, ByVal Aplicado As Boolean,
                                 ByVal Activo As Boolean, ByVal nTipoEdicion As Integer, referencia As String)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_OrdenTraslado", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdOrden.Parameters.Add("@Codigo_Traslado", SqlDbType.VarChar).Value = Codigo_Traslado
            cmdOrden.Parameters.Add("@Bodega_Origen", SqlDbType.Int).Value = Bodega_Origen
            cmdOrden.Parameters.Add("@Bodega_Destino", SqlDbType.Int).Value = Bodega_Destino
            cmdOrden.Parameters.Add("@Comentario", SqlDbType.NVarChar).Value = Comentario
            cmdOrden.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
            cmdOrden.Parameters.Add("@Aplicado", SqlDbType.Bit).Value = Aplicado
            cmdOrden.Parameters.Add("@Activo", SqlDbType.Bit).Value = Activo
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.Parameters.Add("@Referencia", SqlDbType.VarChar).Value = referencia
            cmdOrden.ExecuteReader()

            EditarTraslado = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarTraslado = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarTrasladoDeta(ByVal CodigoSucursal As Integer, ByVal Codigo_Traslado As String, ByVal Codigo_Producto As String,
                                 ByVal Cantidad As Double, ByVal Costo_Unitario As Double, ByVal nTipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_OrdenTraslado_Detalle", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdOrden.Parameters.Add("@Codigo_Traslado", SqlDbType.VarChar).Value = Codigo_Traslado
            cmdOrden.Parameters.Add("@Codigo_Producto", SqlDbType.VarChar).Value = Codigo_Producto
            cmdOrden.Parameters.Add("@Cantidad", SqlDbType.Decimal).Value = Cantidad
            cmdOrden.Parameters.Add("@Costo_Unitario", SqlDbType.Decimal).Value = Costo_Unitario
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.ExecuteReader()

            EditarTrasladoDeta = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarTrasladoDeta = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarPedido(ByVal CodigoSucursal As Integer, ByVal Codigo_Pedido As String, ByVal Codigo_Proveedor As String,
                                 ByVal Concepto As String, ByVal Fecha As Date, ByVal Taza_Cambio As Decimal, ByVal Estado As Integer,
                                 ByVal nTipoEdicion As Integer, referencia As String)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_Pedido", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdOrden.Parameters.Add("@Codigo_Pedido", SqlDbType.VarChar).Value = Codigo_Pedido
            cmdOrden.Parameters.Add("@Codigo_Proveedor", SqlDbType.VarChar).Value = Codigo_Proveedor
            cmdOrden.Parameters.Add("@Concepto", SqlDbType.NVarChar).Value = Concepto
            cmdOrden.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
            cmdOrden.Parameters.Add("@Taza_Cambio", SqlDbType.Decimal).Value = Taza_Cambio
            cmdOrden.Parameters.Add("@Estado", SqlDbType.Int).Value = Estado
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.Parameters.Add("@Referencia", SqlDbType.VarChar).Value = referencia
            cmdOrden.ExecuteReader()

            EditarPedido = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarPedido = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarPedidoDeta(ByVal CodigoSucursal As Integer, ByVal Codigo_Pedido As String, ByVal Codigo_Producto As String,
                                     ByVal Cant_Existencia As Decimal, ByVal Cant_Requerida As Decimal, ByVal Costo_Unitario As Decimal,
                                     ByVal Costo_Total As Decimal, ByVal nTipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdOrden As New SqlCommand("SP_AME_Pedido_Detalle", cnConec)
            cmdOrden.CommandType = CommandType.StoredProcedure
            cmdOrden.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdOrden.Parameters.Add("@Codigo_Pedido", SqlDbType.VarChar).Value = Codigo_Pedido
            cmdOrden.Parameters.Add("@Codigo_Producto", SqlDbType.VarChar).Value = Codigo_Producto
            cmdOrden.Parameters.Add("@Cant_Existencia", SqlDbType.Decimal).Value = Cant_Existencia
            cmdOrden.Parameters.Add("@Cant_Requerida", SqlDbType.Decimal).Value = Cant_Requerida
            cmdOrden.Parameters.Add("@Costo_Unitario", SqlDbType.Decimal).Value = Costo_Unitario
            cmdOrden.Parameters.Add("@Costo_Total", SqlDbType.Decimal).Value = Costo_Total
            cmdOrden.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdOrden.ExecuteReader()

            EditarPedidoDeta = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarPedidoDeta = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

End Class
