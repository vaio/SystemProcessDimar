﻿Imports System.Data.SqlClient

Public Class clsContabilidad

    Implements IDisposable

    Dim mconexion As String
    Dim strSql As String
    Dim isWebService As Boolean = False

#Region " IDisposable Support "

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    'To detect redundant calls

    'IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

    Public Sub New(ByVal ConextionString As String)
        mconexion = ConextionString
    End Sub

    Public Function Sql(ByVal strSql As String) As DataSet
        Dim cn As New SqlConnection
        cn.ConnectionString = mconexion
        Try
            cn.Open()
            Dim sqlda As New SqlDataAdapter(strSql, cn)
            Dim ds As New DataSet
            sqlda.Fill(ds, "tblNotas")
            Sql = ds
        Catch ex As Exception
      Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Sql = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    Public Function EditarCatalogoCuenta(ByVal nCuenta As String, ByVal nSCuenta As String, ByVal nSSCuenta As String, ByVal nSSSCuenta As String, ByVal nSSSSCuenta As String, ByVal nSSSSSCuenta As String,
    ByVal sNomCuenta As String, ByVal sDescripcion As String, ByVal nNivelCnta As Integer, ByVal nTipoCuenta As Integer, ByVal nTipoSaldo As Integer, ByVal bIsDetalle As Boolean, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_AME_CatalogoCuentas", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@Cuenta", SqlDbType.NVarChar).Value = nCuenta
            cmdDocument.Parameters.Add("@SCuenta", SqlDbType.NVarChar).Value = nSCuenta
            cmdDocument.Parameters.Add("@SSCuenta", SqlDbType.NVarChar).Value = nSSCuenta
            cmdDocument.Parameters.Add("@SSSCuenta", SqlDbType.NVarChar).Value = nSSSCuenta
            cmdDocument.Parameters.Add("@SSSSCuenta", SqlDbType.NVarChar).Value = nSSSSCuenta
            cmdDocument.Parameters.Add("@SSSSSCuenta", SqlDbType.NVarChar).Value = nSSSSSCuenta
            cmdDocument.Parameters.Add("@NomCuenta", SqlDbType.NVarChar).Value = sNomCuenta
            cmdDocument.Parameters.Add("@Descripcion", SqlDbType.NVarChar).Value = sDescripcion
            cmdDocument.Parameters.Add("@nNivelCuenta", SqlDbType.Int).Value = nNivelCnta
            cmdDocument.Parameters.Add("@TipoCuenta", SqlDbType.Int).Value = nTipoCuenta
            cmdDocument.Parameters.Add("@TipoSaldo", SqlDbType.Int).Value = nTipoSaldo
            cmdDocument.Parameters.Add("@IsDetalle", SqlDbType.Bit).Value = IIf(bIsDetalle = True, 1, 0)
            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocument.ExecuteReader()

            EditarCatalogoCuenta = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarCatalogoCuenta = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarMasterComprobDiario(ByVal NumComprob As Integer, ByVal TipoComprob As Integer, ByVal Fecha As Date, ByVal sDescripcion As String,
    ByVal Monto As Double, ByVal IsImpreso As Boolean, ByVal IsAplicado As Boolean, ByVal IsAnulado As Boolean, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_AME_MasterComprobDiario", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@NumComprob", SqlDbType.Int).Value = NumComprob
            cmdDocument.Parameters.Add("@TipoComprob", SqlDbType.Int).Value = TipoComprob
            cmdDocument.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha
            cmdDocument.Parameters.Add("@Descripcion", SqlDbType.NVarChar).Value = sDescripcion
            cmdDocument.Parameters.Add("@Monto", SqlDbType.Money).Value = Monto
            cmdDocument.Parameters.Add("@IsImpreso", SqlDbType.Bit).Value = IIf(IsImpreso = True, 1, 0)
            cmdDocument.Parameters.Add("@IsAplicado", SqlDbType.Bit).Value = IIf(IsAplicado = True, 1, 0)
            cmdDocument.Parameters.Add("@IsAnulado", SqlDbType.Bit).Value = IIf(IsAnulado = True, 1, 0)
            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocument.ExecuteReader()

            EditarMasterComprobDiario = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMasterComprobDiario = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarMovDimactral(ByVal CuentaM As String, ByVal Subctam As String, ByVal Ssctam As String,
                                       ByVal Concepto As String, ByVal TipoCompte As String, ByVal NumCompte As String,
                                       ByVal FechaCompte As Date, ByVal FechaGrab As Date, ByVal SigRegis As String,
                                       ByVal ValcReg As Decimal, ByVal Valdreg As Decimal, ByVal TasaCamb As Decimal,
                                       ByVal CampTotal As String, ByVal MenError As String, ByVal CodOpera As String,
                                       ByVal NumSol As String, ByVal Usuario As String, ByVal Equipo As String,
                                       ByVal NumComp As String, ByVal CodSuc As String, ByVal Borrado As String,
                                       ByVal Concil As String, ByVal TipoEdicion As String)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_AME_Mov_Dimarctral", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@CUENTAM", SqlDbType.Char).Value = CuentaM
            cmdDocument.Parameters.Add("@SUBCTAM", SqlDbType.Char).Value = Subctam
            cmdDocument.Parameters.Add("@SSCTAM", SqlDbType.Char).Value = Ssctam
            cmdDocument.Parameters.Add("@CONCEPTO_REGISTRO", SqlDbType.Char).Value = Concepto
            cmdDocument.Parameters.Add("@TIPO_COMBPTE", SqlDbType.Char).Value = TipoCompte
            cmdDocument.Parameters.Add("@NUMERO_COMBPTE", SqlDbType.Char).Value = NumCompte
            cmdDocument.Parameters.Add("@FECHA_COMBPTE", SqlDbType.DateTime).Value = FechaCompte
            cmdDocument.Parameters.Add("@FECHA_GRABACION", SqlDbType.DateTime).Value = FechaGrab
            cmdDocument.Parameters.Add("@SIGNO_REGISTRO", SqlDbType.Char).Value = SigRegis
            cmdDocument.Parameters.Add("@VALORC_REGISTRO", SqlDbType.Decimal).Value = ValcReg
            cmdDocument.Parameters.Add("@VALORD_REGISTRO", SqlDbType.Decimal).Value = Valdreg
            cmdDocument.Parameters.Add("@TASA_CAMBIO", SqlDbType.Decimal).Value = TasaCamb
            cmdDocument.Parameters.Add("@CAMPO_TOTAL", SqlDbType.Char).Value = CampTotal
            cmdDocument.Parameters.Add("@MEN_ERROR", SqlDbType.Char).Value = MenError
            cmdDocument.Parameters.Add("@CODIGO_OPERA", SqlDbType.Char).Value = CodOpera
            cmdDocument.Parameters.Add("@NUMSOL", SqlDbType.Char).Value = NumSol
            cmdDocument.Parameters.Add("@USUARIO", SqlDbType.Char).Value = Usuario
            cmdDocument.Parameters.Add("@EQUIPO", SqlDbType.Char).Value = Equipo
            cmdDocument.Parameters.Add("@NUMCOMP", SqlDbType.Char).Value = NumComp
            cmdDocument.Parameters.Add("@CODSUC", SqlDbType.Char).Value = CodSuc
            cmdDocument.Parameters.Add("@BORRADO", SqlDbType.Char).Value = Borrado
            cmdDocument.Parameters.Add("@CONCILIACION", SqlDbType.Char).Value = Concil

            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocument.ExecuteReader()

            EditarMovDimactral = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMovDimactral = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    'Public Function EditarComprobanteCheque(ByVal nCuenta As String, ByVal nSCuenta As String, ByVal nSSCuenta As String, ByVal nSSSCuenta As String, ByVal nSSSSCuenta As String, ByVal nSSSSSCuenta As String,
    'ByVal sNomCuenta As String, ByVal sDescripcion As String, ByVal nTipoCuenta As Integer, ByVal nTipoSaldo As Integer, ByVal bIsDetalle As Boolean, ByVal TipoEdicion As Integer)
    '    Dim ID As String = ""
    '    Dim cnConec As New SqlConnection

    '    With cnConec
    '        .ConnectionString = mconexion
    '        .Open()
    '    End With

    '    Try
    '        Dim cmdDocument As New SqlCommand("SP_AME_CatalogoCuentas", cnConec)
    '        cmdDocument.CommandType = CommandType.StoredProcedure
    '        cmdDocument.Parameters.Add("@Cuenta", SqlDbType.NVarChar).Value = nCuenta
    '        cmdDocument.Parameters.Add("@SCuenta", SqlDbType.NVarChar).Value = nSCuenta
    '        cmdDocument.Parameters.Add("@SSCuenta", SqlDbType.NVarChar).Value = nSSCuenta
    '        cmdDocument.Parameters.Add("@SSSCuenta", SqlDbType.NVarChar).Value = nSSSCuenta
    '        cmdDocument.Parameters.Add("@SSSSCuenta", SqlDbType.NVarChar).Value = nSSSSCuenta
    '        cmdDocument.Parameters.Add("@SSSSSCuenta", SqlDbType.NVarChar).Value = nSSSSSCuenta
    '        cmdDocument.Parameters.Add("@NomCuenta", SqlDbType.NVarChar).Value = sNomCuenta
    '        cmdDocument.Parameters.Add("@Descripcion", SqlDbType.NVarChar).Value = sDescripcion
    '        cmdDocument.Parameters.Add("@TipoCuenta", SqlDbType.Int).Value = nTipoCuenta
    '        cmdDocument.Parameters.Add("@TipoSaldo", SqlDbType.Int).Value = nTipoSaldo
    '        cmdDocument.Parameters.Add("@IsDetalle", SqlDbType.Bit).Value = IIf(bIsDetalle = True, 1, 0)
    '        cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
    '        cmdDocument.ExecuteReader()

    '        EditarCatalogoCuenta = "OK"

    '    Catch ex As SqlException
    '        MsgBox("Error: " + ex.Message)
    '        EditarCatalogoCuenta = "ERROR"
    '    Finally
    '        cnConec.Close()
    '        cnConec.Dispose()
    '    End Try

    'End Function

End Class
