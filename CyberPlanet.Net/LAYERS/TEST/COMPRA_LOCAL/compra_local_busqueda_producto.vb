﻿Public Class compra_local_busqueda_producto

    Private cod_producto = String.Empty
    Private nombre As String = String.Empty
    Private existencia As Double = 0
    Private costo As Double = 0
    Private bodega As Integer = 2

    Public ReadOnly Property get_codigo As String
        Get
            Return Me.cod_producto
        End Get
    End Property
    Public ReadOnly Property get_nombre As String
        Get
            Return Me.nombre
        End Get
    End Property
    Public ReadOnly Property get_existencia As Double
        Get
            Return Me.existencia
        End Get
    End Property
    Public ReadOnly Property get_costo As Double
        Get
            Return Me.costo
        End Get
    End Property

    Public WriteOnly Property set_bodega As Integer
        Set(value As Integer)
            Me.bodega = value
        End Set
    End Property


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        start()

    End Sub

    Private Sub start()

        AddHandler GridView1.DoubleClick, AddressOf get_producto
        AddHandler cmdAceptar.Click, AddressOf get_producto

        set_datos()

    End Sub
    Private Sub set_datos()

        Tabla.DataSource = data_producto.get_existencia_costo_por_bodega_por_producto(Funciones.Param("@cod_bodega", Me.bodega, SqlDbType.Int))

        GridView1.BestFitColumns()

        GridView1.Columns(2).Visible = False

    End Sub
    Private Sub get_producto()

        Try

            If GridView1.SelectedRowsCount < 0 Then Exit Sub

            If GridView1.RowCount = 0 Then Exit Sub


            cod_producto = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
            nombre = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre_Producto").ToString()
            existencia = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Existencia actual").ToString()
            costo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Costo promedio").ToString()


        Catch ex As Exception

            Console.Write("TROLL ERRO DETECTED xD")

            Exit Sub

        End Try


        Me.Close()

    End Sub
    Private Sub cancelar()

        cod_producto = String.Empty
        cod_producto = String.Empty
        nombre = String.Empty
        existencia = 0
        costo = 0

        Me.Close()

    End Sub

End Class