﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class compra_local
    Inherits System.Windows.Forms.Form
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(compra_local))
        Me.pMain = New System.Windows.Forms.Panel()
        Me.pBody = New System.Windows.Forms.Panel()
        Me.grdDetalle = New DevExpress.XtraGrid.GridControl()
        Me.grvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.pRight = New System.Windows.Forms.Panel()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.pTotales = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtIVAExcento = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTotalbruto = New System.Windows.Forms.TextBox()
        Me.txtSubtotal = New System.Windows.Forms.TextBox()
        Me.Lbl_IR = New System.Windows.Forms.Label()
        Me.txtIMI = New System.Windows.Forms.TextBox()
        Me.txtIR = New System.Windows.Forms.TextBox()
        Me.Lbl_IVA = New System.Windows.Forms.Label()
        Me.Lbl_IMI = New System.Windows.Forms.Label()
        Me.txtTotalNeto = New System.Windows.Forms.TextBox()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pDown = New System.Windows.Forms.Panel()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.pButtons = New System.Windows.Forms.Panel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.pTop = New System.Windows.Forms.Panel()
        Me.cmbTipExoneracion = New DevExpress.XtraEditors.LookUpEdit()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnTasaCambio = New System.Windows.Forms.Button()
        Me.rbDolares = New System.Windows.Forms.RadioButton()
        Me.rbCordobas = New System.Windows.Forms.RadioButton()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtTazaCambio = New System.Windows.Forms.TextBox()
        Me.Fecha_Recepcionado = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCK_Descuento = New System.Windows.Forms.TextBox()
        Me.gbRetenciones = New System.Windows.Forms.GroupBox()
        Me.txtPorcIR = New System.Windows.Forms.TextBox()
        Me.txtPorcIMI = New System.Windows.Forms.TextBox()
        Me.CkIR = New System.Windows.Forms.CheckBox()
        Me.CkIMI = New System.Windows.Forms.CheckBox()
        Me.Ck_Descuento = New System.Windows.Forms.CheckBox()
        Me.txtFactura = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbCredito = New System.Windows.Forms.RadioButton()
        Me.rbContado = New System.Windows.Forms.RadioButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPlazo = New System.Windows.Forms.TextBox()
        Me.cmdAddProveedor = New DevExpress.XtraEditors.SimpleButton()
        Me.Fecha_Vencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Fecha_de_documento = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbProveedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNumDoc = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pMain.SuspendLayout()
        Me.pBody.SuspendLayout()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pRight.SuspendLayout()
        Me.pTotales.SuspendLayout()
        Me.pDown.SuspendLayout()
        Me.pButtons.SuspendLayout()
        Me.pTop.SuspendLayout()
        CType(Me.cmbTipExoneracion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.gbRetenciones.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cmbProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.pBody)
        Me.pMain.Controls.Add(Me.pDown)
        Me.pMain.Controls.Add(Me.pTop)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1552, 687)
        Me.pMain.TabIndex = 0
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.grdDetalle)
        Me.pBody.Controls.Add(Me.pRight)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 194)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(1552, 419)
        Me.pBody.TabIndex = 2
        '
        'grdDetalle
        '
        Me.grdDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDetalle.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        GridLevelNode1.RelationName = "Level1"
        Me.grdDetalle.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.grdDetalle.Location = New System.Drawing.Point(0, 0)
        Me.grdDetalle.MainView = Me.grvDetalle
        Me.grdDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(1290, 419)
        Me.grdDetalle.TabIndex = 5
        Me.grdDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalle})
        '
        'grvDetalle
        '
        Me.grvDetalle.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvDetalle.GridControl = Me.grdDetalle
        Me.grvDetalle.GroupFormat = ""
        Me.grvDetalle.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvDetalle.Name = "grvDetalle"
        Me.grvDetalle.OptionsBehavior.Editable = False
        Me.grvDetalle.OptionsBehavior.ReadOnly = True
        Me.grvDetalle.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetalle.OptionsCustomization.AllowColumnResizing = False
        Me.grvDetalle.OptionsCustomization.AllowGroup = False
        Me.grvDetalle.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvDetalle.OptionsView.ColumnAutoWidth = False
        Me.grvDetalle.OptionsView.ShowGroupPanel = False
        '
        'pRight
        '
        Me.pRight.Controls.Add(Me.lblEstado)
        Me.pRight.Controls.Add(Me.pTotales)
        Me.pRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.pRight.Location = New System.Drawing.Point(1290, 0)
        Me.pRight.Name = "pRight"
        Me.pRight.Size = New System.Drawing.Size(262, 419)
        Me.pRight.TabIndex = 0
        '
        'lblEstado
        '
        Me.lblEstado.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblEstado.Location = New System.Drawing.Point(4, 3)
        Me.lblEstado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(249, 25)
        Me.lblEstado.TabIndex = 81
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pTotales
        '
        Me.pTotales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pTotales.Controls.Add(Me.Label15)
        Me.pTotales.Controls.Add(Me.txtIVAExcento)
        Me.pTotales.Controls.Add(Me.Label13)
        Me.pTotales.Controls.Add(Me.txtTotalbruto)
        Me.pTotales.Controls.Add(Me.txtSubtotal)
        Me.pTotales.Controls.Add(Me.Lbl_IR)
        Me.pTotales.Controls.Add(Me.txtIMI)
        Me.pTotales.Controls.Add(Me.txtIR)
        Me.pTotales.Controls.Add(Me.Lbl_IVA)
        Me.pTotales.Controls.Add(Me.Lbl_IMI)
        Me.pTotales.Controls.Add(Me.txtTotalNeto)
        Me.pTotales.Controls.Add(Me.txtIVA)
        Me.pTotales.Controls.Add(Me.Label6)
        Me.pTotales.Controls.Add(Me.txtDescuento)
        Me.pTotales.Controls.Add(Me.Label12)
        Me.pTotales.Controls.Add(Me.Label9)
        Me.pTotales.Location = New System.Drawing.Point(4, 35)
        Me.pTotales.Margin = New System.Windows.Forms.Padding(4)
        Me.pTotales.Name = "pTotales"
        Me.pTotales.Size = New System.Drawing.Size(248, 267)
        Me.pTotales.TabIndex = 80
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 103)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 17)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "IVA Exce.:"
        '
        'txtIVAExcento
        '
        Me.txtIVAExcento.BackColor = System.Drawing.Color.White
        Me.txtIVAExcento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIVAExcento.Location = New System.Drawing.Point(84, 97)
        Me.txtIVAExcento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIVAExcento.Name = "txtIVAExcento"
        Me.txtIVAExcento.ReadOnly = True
        Me.txtIVAExcento.Size = New System.Drawing.Size(128, 24)
        Me.txtIVAExcento.TabIndex = 15
        Me.txtIVAExcento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(1, 71)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(82, 17)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Total Bruto:"
        '
        'txtTotalbruto
        '
        Me.txtTotalbruto.BackColor = System.Drawing.Color.White
        Me.txtTotalbruto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalbruto.Location = New System.Drawing.Point(84, 65)
        Me.txtTotalbruto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalbruto.Name = "txtTotalbruto"
        Me.txtTotalbruto.ReadOnly = True
        Me.txtTotalbruto.Size = New System.Drawing.Size(128, 24)
        Me.txtTotalbruto.TabIndex = 13
        Me.txtTotalbruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSubtotal
        '
        Me.txtSubtotal.BackColor = System.Drawing.Color.White
        Me.txtSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubtotal.Location = New System.Drawing.Point(84, 4)
        Me.txtSubtotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.ReadOnly = True
        Me.txtSubtotal.Size = New System.Drawing.Size(128, 24)
        Me.txtSubtotal.TabIndex = 0
        Me.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Lbl_IR
        '
        Me.Lbl_IR.AutoSize = True
        Me.Lbl_IR.Location = New System.Drawing.Point(4, 167)
        Me.Lbl_IR.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_IR.Name = "Lbl_IR"
        Me.Lbl_IR.Size = New System.Drawing.Size(25, 17)
        Me.Lbl_IR.TabIndex = 12
        Me.Lbl_IR.Text = "IR:"
        '
        'txtIMI
        '
        Me.txtIMI.BackColor = System.Drawing.Color.White
        Me.txtIMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIMI.Location = New System.Drawing.Point(84, 192)
        Me.txtIMI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIMI.Name = "txtIMI"
        Me.txtIMI.ReadOnly = True
        Me.txtIMI.Size = New System.Drawing.Size(128, 24)
        Me.txtIMI.TabIndex = 3
        Me.txtIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIR
        '
        Me.txtIR.BackColor = System.Drawing.Color.White
        Me.txtIR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIR.Location = New System.Drawing.Point(84, 161)
        Me.txtIR.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIR.Name = "txtIR"
        Me.txtIR.ReadOnly = True
        Me.txtIR.Size = New System.Drawing.Size(128, 24)
        Me.txtIR.TabIndex = 2
        Me.txtIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Lbl_IVA
        '
        Me.Lbl_IVA.AutoSize = True
        Me.Lbl_IVA.Location = New System.Drawing.Point(4, 135)
        Me.Lbl_IVA.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_IVA.Name = "Lbl_IVA"
        Me.Lbl_IVA.Size = New System.Drawing.Size(33, 17)
        Me.Lbl_IVA.TabIndex = 8
        Me.Lbl_IVA.Text = "IVA:"
        '
        'Lbl_IMI
        '
        Me.Lbl_IMI.AutoSize = True
        Me.Lbl_IMI.Location = New System.Drawing.Point(4, 198)
        Me.Lbl_IMI.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl_IMI.Name = "Lbl_IMI"
        Me.Lbl_IMI.Size = New System.Drawing.Size(29, 17)
        Me.Lbl_IMI.TabIndex = 6
        Me.Lbl_IMI.Text = "IMI:"
        '
        'txtTotalNeto
        '
        Me.txtTotalNeto.BackColor = System.Drawing.Color.White
        Me.txtTotalNeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalNeto.Location = New System.Drawing.Point(84, 224)
        Me.txtTotalNeto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalNeto.Name = "txtTotalNeto"
        Me.txtTotalNeto.ReadOnly = True
        Me.txtTotalNeto.Size = New System.Drawing.Size(128, 24)
        Me.txtTotalNeto.TabIndex = 5
        Me.txtTotalNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIVA
        '
        Me.txtIVA.BackColor = System.Drawing.Color.White
        Me.txtIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIVA.Location = New System.Drawing.Point(84, 129)
        Me.txtIVA.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.ReadOnly = True
        Me.txtIVA.Size = New System.Drawing.Size(128, 24)
        Me.txtIVA.TabIndex = 4
        Me.txtIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(4, 10)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 17)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Subtotal:"
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.White
        Me.txtDescuento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescuento.Location = New System.Drawing.Point(84, 33)
        Me.txtDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.ReadOnly = True
        Me.txtDescuento.Size = New System.Drawing.Size(128, 24)
        Me.txtDescuento.TabIndex = 1
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(4, 230)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(50, 17)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Total:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(4, 39)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 17)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Descuento:"
        '
        'pDown
        '
        Me.pDown.Controls.Add(Me.btnAplicar)
        Me.pDown.Controls.Add(Me.pButtons)
        Me.pDown.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pDown.Location = New System.Drawing.Point(0, 613)
        Me.pDown.Name = "pDown"
        Me.pDown.Size = New System.Drawing.Size(1552, 74)
        Me.pDown.TabIndex = 1
        '
        'btnAplicar
        '
        Me.btnAplicar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAplicar.Image = CType(resources.GetObject("btnAplicar.Image"), System.Drawing.Image)
        Me.btnAplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAplicar.Location = New System.Drawing.Point(1369, 9)
        Me.btnAplicar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(170, 57)
        Me.btnAplicar.TabIndex = 5
        Me.btnAplicar.Text = "&Aplicar Compra"
        Me.btnAplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'pButtons
        '
        Me.pButtons.Controls.Add(Me.btnEliminar)
        Me.pButtons.Controls.Add(Me.btnModificar)
        Me.pButtons.Controls.Add(Me.btnAgregar)
        Me.pButtons.Location = New System.Drawing.Point(-12, 4)
        Me.pButtons.Margin = New System.Windows.Forms.Padding(4)
        Me.pButtons.Name = "pButtons"
        Me.pButtons.Size = New System.Drawing.Size(379, 62)
        Me.pButtons.TabIndex = 4
        '
        'btnEliminar
        '
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(255, 5)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(111, 49)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(136, 5)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(111, 49)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(16, 5)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(111, 49)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'pTop
        '
        Me.pTop.Controls.Add(Me.cmbTipExoneracion)
        Me.pTop.Controls.Add(Me.GroupBox3)
        Me.pTop.Controls.Add(Me.Fecha_Recepcionado)
        Me.pTop.Controls.Add(Me.Label11)
        Me.pTop.Controls.Add(Me.txtCK_Descuento)
        Me.pTop.Controls.Add(Me.gbRetenciones)
        Me.pTop.Controls.Add(Me.Ck_Descuento)
        Me.pTop.Controls.Add(Me.txtFactura)
        Me.pTop.Controls.Add(Me.Label5)
        Me.pTop.Controls.Add(Me.txtObservacion)
        Me.pTop.Controls.Add(Me.Label10)
        Me.pTop.Controls.Add(Me.Label3)
        Me.pTop.Controls.Add(Me.GroupBox2)
        Me.pTop.Controls.Add(Me.cmdAddProveedor)
        Me.pTop.Controls.Add(Me.Fecha_Vencimiento)
        Me.pTop.Controls.Add(Me.Label7)
        Me.pTop.Controls.Add(Me.Fecha_de_documento)
        Me.pTop.Controls.Add(Me.Label4)
        Me.pTop.Controls.Add(Me.cmbProveedor)
        Me.pTop.Controls.Add(Me.Label2)
        Me.pTop.Controls.Add(Me.txtNumDoc)
        Me.pTop.Controls.Add(Me.Label1)
        Me.pTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pTop.Location = New System.Drawing.Point(0, 0)
        Me.pTop.Name = "pTop"
        Me.pTop.Size = New System.Drawing.Size(1552, 194)
        Me.pTop.TabIndex = 0
        '
        'cmbTipExoneracion
        '
        Me.cmbTipExoneracion.Location = New System.Drawing.Point(177, 137)
        Me.cmbTipExoneracion.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbTipExoneracion.Name = "cmbTipExoneracion"
        Me.cmbTipExoneracion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbTipExoneracion.Size = New System.Drawing.Size(323, 22)
        Me.cmbTipExoneracion.TabIndex = 58
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnTasaCambio)
        Me.GroupBox3.Controls.Add(Me.rbDolares)
        Me.GroupBox3.Controls.Add(Me.rbCordobas)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.txtTazaCambio)
        Me.GroupBox3.Location = New System.Drawing.Point(1132, 103)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(411, 63)
        Me.GroupBox3.TabIndex = 57
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Moneda"
        '
        'btnTasaCambio
        '
        Me.btnTasaCambio.Enabled = False
        Me.btnTasaCambio.Image = CType(resources.GetObject("btnTasaCambio.Image"), System.Drawing.Image)
        Me.btnTasaCambio.Location = New System.Drawing.Point(365, 22)
        Me.btnTasaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.btnTasaCambio.Name = "btnTasaCambio"
        Me.btnTasaCambio.Size = New System.Drawing.Size(35, 28)
        Me.btnTasaCambio.TabIndex = 62
        Me.btnTasaCambio.UseVisualStyleBackColor = True
        '
        'rbDolares
        '
        Me.rbDolares.AutoSize = True
        Me.rbDolares.Location = New System.Drawing.Point(112, 27)
        Me.rbDolares.Margin = New System.Windows.Forms.Padding(4)
        Me.rbDolares.Name = "rbDolares"
        Me.rbDolares.Size = New System.Drawing.Size(78, 21)
        Me.rbDolares.TabIndex = 1
        Me.rbDolares.Text = "Dolares"
        Me.rbDolares.UseVisualStyleBackColor = True
        '
        'rbCordobas
        '
        Me.rbCordobas.AutoSize = True
        Me.rbCordobas.Checked = True
        Me.rbCordobas.Location = New System.Drawing.Point(16, 27)
        Me.rbCordobas.Margin = New System.Windows.Forms.Padding(4)
        Me.rbCordobas.Name = "rbCordobas"
        Me.rbCordobas.Size = New System.Drawing.Size(90, 21)
        Me.rbCordobas.TabIndex = 0
        Me.rbCordobas.TabStop = True
        Me.rbCordobas.Text = "Cordobas"
        Me.rbCordobas.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(192, 30)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 17)
        Me.Label14.TabIndex = 25
        Me.Label14.Text = "Taza Cambio:"
        '
        'txtTazaCambio
        '
        Me.txtTazaCambio.BackColor = System.Drawing.Color.White
        Me.txtTazaCambio.Enabled = False
        Me.txtTazaCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTazaCambio.Location = New System.Drawing.Point(291, 23)
        Me.txtTazaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTazaCambio.Name = "txtTazaCambio"
        Me.txtTazaCambio.Size = New System.Drawing.Size(72, 26)
        Me.txtTazaCambio.TabIndex = 9
        Me.txtTazaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Fecha_Recepcionado
        '
        Me.Fecha_Recepcionado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_Recepcionado.Location = New System.Drawing.Point(127, 65)
        Me.Fecha_Recepcionado.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_Recepcionado.Name = "Fecha_Recepcionado"
        Me.Fecha_Recepcionado.Size = New System.Drawing.Size(141, 22)
        Me.Fecha_Recepcionado.TabIndex = 55
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(22, 69)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(103, 17)
        Me.Label11.TabIndex = 56
        Me.Label11.Text = "Recepcionado:"
        '
        'txtCK_Descuento
        '
        Me.txtCK_Descuento.BackColor = System.Drawing.Color.White
        Me.txtCK_Descuento.Enabled = False
        Me.txtCK_Descuento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCK_Descuento.Location = New System.Drawing.Point(1465, 24)
        Me.txtCK_Descuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCK_Descuento.MaxLength = 5
        Me.txtCK_Descuento.Name = "txtCK_Descuento"
        Me.txtCK_Descuento.Size = New System.Drawing.Size(77, 26)
        Me.txtCK_Descuento.TabIndex = 54
        Me.txtCK_Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gbRetenciones
        '
        Me.gbRetenciones.Controls.Add(Me.txtPorcIR)
        Me.gbRetenciones.Controls.Add(Me.txtPorcIMI)
        Me.gbRetenciones.Controls.Add(Me.CkIR)
        Me.gbRetenciones.Controls.Add(Me.CkIMI)
        Me.gbRetenciones.Enabled = False
        Me.gbRetenciones.Location = New System.Drawing.Point(850, 103)
        Me.gbRetenciones.Margin = New System.Windows.Forms.Padding(4)
        Me.gbRetenciones.Name = "gbRetenciones"
        Me.gbRetenciones.Padding = New System.Windows.Forms.Padding(4)
        Me.gbRetenciones.Size = New System.Drawing.Size(275, 63)
        Me.gbRetenciones.TabIndex = 43
        Me.gbRetenciones.TabStop = False
        Me.gbRetenciones.Text = "Retenciones"
        '
        'txtPorcIR
        '
        Me.txtPorcIR.BackColor = System.Drawing.Color.White
        Me.txtPorcIR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcIR.Location = New System.Drawing.Point(61, 23)
        Me.txtPorcIR.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcIR.Name = "txtPorcIR"
        Me.txtPorcIR.ReadOnly = True
        Me.txtPorcIR.Size = New System.Drawing.Size(48, 26)
        Me.txtPorcIR.TabIndex = 1
        Me.txtPorcIR.Text = "0"
        Me.txtPorcIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPorcIMI
        '
        Me.txtPorcIMI.BackColor = System.Drawing.Color.White
        Me.txtPorcIMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcIMI.Location = New System.Drawing.Point(195, 23)
        Me.txtPorcIMI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcIMI.Name = "txtPorcIMI"
        Me.txtPorcIMI.ReadOnly = True
        Me.txtPorcIMI.Size = New System.Drawing.Size(48, 26)
        Me.txtPorcIMI.TabIndex = 3
        Me.txtPorcIMI.Text = "0"
        Me.txtPorcIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CkIR
        '
        Me.CkIR.AutoSize = True
        Me.CkIR.Location = New System.Drawing.Point(16, 27)
        Me.CkIR.Margin = New System.Windows.Forms.Padding(4)
        Me.CkIR.Name = "CkIR"
        Me.CkIR.Size = New System.Drawing.Size(115, 21)
        Me.CkIR.TabIndex = 0
        Me.CkIR.Text = "IR               %"
        Me.CkIR.UseVisualStyleBackColor = True
        '
        'CkIMI
        '
        Me.CkIMI.AutoSize = True
        Me.CkIMI.Location = New System.Drawing.Point(144, 26)
        Me.CkIMI.Margin = New System.Windows.Forms.Padding(4)
        Me.CkIMI.Name = "CkIMI"
        Me.CkIMI.Size = New System.Drawing.Size(119, 21)
        Me.CkIMI.TabIndex = 2
        Me.CkIMI.Text = "IMI               %"
        Me.CkIMI.UseVisualStyleBackColor = True
        '
        'Ck_Descuento
        '
        Me.Ck_Descuento.AutoSize = True
        Me.Ck_Descuento.Location = New System.Drawing.Point(1347, 28)
        Me.Ck_Descuento.Margin = New System.Windows.Forms.Padding(4)
        Me.Ck_Descuento.Name = "Ck_Descuento"
        Me.Ck_Descuento.Size = New System.Drawing.Size(118, 21)
        Me.Ck_Descuento.TabIndex = 53
        Me.Ck_Descuento.Text = " % Descuento"
        Me.Ck_Descuento.UseVisualStyleBackColor = True
        '
        'txtFactura
        '
        Me.txtFactura.BackColor = System.Drawing.Color.White
        Me.txtFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFactura.Location = New System.Drawing.Point(127, 103)
        Me.txtFactura.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFactura.MaxLength = 50
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.Size = New System.Drawing.Size(373, 26)
        Me.txtFactura.TabIndex = 38
        Me.txtFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(516, 66)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 17)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Concepto:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Location = New System.Drawing.Point(603, 63)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.MaxLength = 250
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(939, 26)
        Me.txtObservacion.TabIndex = 47
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(22, 109)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 17)
        Me.Label10.TabIndex = 52
        Me.Label10.Text = "Facturas:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 143)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(147, 17)
        Me.Label3.TabIndex = 46
        Me.Label3.Text = "Tipo Exoneracion IVA:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbCredito)
        Me.GroupBox2.Controls.Add(Me.rbContado)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtPlazo)
        Me.GroupBox2.Location = New System.Drawing.Point(510, 103)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(332, 63)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Compra"
        '
        'rbCredito
        '
        Me.rbCredito.AutoSize = True
        Me.rbCredito.Location = New System.Drawing.Point(112, 27)
        Me.rbCredito.Margin = New System.Windows.Forms.Padding(4)
        Me.rbCredito.Name = "rbCredito"
        Me.rbCredito.Size = New System.Drawing.Size(74, 21)
        Me.rbCredito.TabIndex = 1
        Me.rbCredito.Text = "Credito"
        Me.rbCredito.UseVisualStyleBackColor = True
        '
        'rbContado
        '
        Me.rbContado.AutoSize = True
        Me.rbContado.Checked = True
        Me.rbContado.Location = New System.Drawing.Point(16, 27)
        Me.rbContado.Margin = New System.Windows.Forms.Padding(4)
        Me.rbContado.Name = "rbContado"
        Me.rbContado.Size = New System.Drawing.Size(82, 21)
        Me.rbContado.TabIndex = 0
        Me.rbContado.TabStop = True
        Me.rbContado.Text = "Contado"
        Me.rbContado.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(192, 30)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 17)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Plazo:"
        '
        'txtPlazo
        '
        Me.txtPlazo.BackColor = System.Drawing.Color.White
        Me.txtPlazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlazo.Location = New System.Drawing.Point(248, 23)
        Me.txtPlazo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPlazo.MaxLength = 3
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(71, 26)
        Me.txtPlazo.TabIndex = 9
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmdAddProveedor
        '
        Me.cmdAddProveedor.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddProveedor.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddProveedor.Appearance.Options.UseFont = True
        Me.cmdAddProveedor.Appearance.Options.UseForeColor = True
        Me.cmdAddProveedor.Location = New System.Drawing.Point(1308, 28)
        Me.cmdAddProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdAddProveedor.Name = "cmdAddProveedor"
        Me.cmdAddProveedor.Size = New System.Drawing.Size(27, 23)
        Me.cmdAddProveedor.TabIndex = 45
        Me.cmdAddProveedor.Text = "+"
        Me.cmdAddProveedor.ToolTip = "Agregar un nuevo Departamento"
        '
        'Fecha_Vencimiento
        '
        Me.Fecha_Vencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_Vencimiento.Location = New System.Drawing.Point(356, 65)
        Me.Fecha_Vencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_Vencimiento.Name = "Fecha_Vencimiento"
        Me.Fecha_Vencimiento.Size = New System.Drawing.Size(144, 22)
        Me.Fecha_Vencimiento.TabIndex = 41
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(295, 69)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 17)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "Vence:"
        '
        'Fecha_de_documento
        '
        Me.Fecha_de_documento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_de_documento.Location = New System.Drawing.Point(356, 27)
        Me.Fecha_de_documento.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_de_documento.Name = "Fecha_de_documento"
        Me.Fecha_de_documento.Size = New System.Drawing.Size(144, 22)
        Me.Fecha_de_documento.TabIndex = 40
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(295, 29)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 17)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "Fecha:"
        '
        'cmbProveedor
        '
        Me.cmbProveedor.Location = New System.Drawing.Point(603, 27)
        Me.cmbProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbProveedor.Name = "cmbProveedor"
        Me.cmbProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbProveedor.Size = New System.Drawing.Size(697, 22)
        Me.cmbProveedor.TabIndex = 44
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(516, 31)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Proveedor:"
        '
        'txtNumDoc
        '
        Me.txtNumDoc.BackColor = System.Drawing.Color.White
        Me.txtNumDoc.Enabled = False
        Me.txtNumDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDoc.Location = New System.Drawing.Point(127, 24)
        Me.txtNumDoc.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(141, 26)
        Me.txtNumDoc.TabIndex = 37
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 29)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 17)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "N° Documento:"
        '
        'compra_local
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1552, 687)
        Me.ControlBox = False
        Me.Controls.Add(Me.pMain)
        Me.Name = "compra_local"
        Me.Text = "CompLocales"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pMain.ResumeLayout(False)
        Me.pBody.ResumeLayout(False)
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pRight.ResumeLayout(False)
        Me.pTotales.ResumeLayout(False)
        Me.pTotales.PerformLayout()
        Me.pDown.ResumeLayout(False)
        Me.pButtons.ResumeLayout(False)
        Me.pTop.ResumeLayout(False)
        Me.pTop.PerformLayout()
        CType(Me.cmbTipExoneracion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gbRetenciones.ResumeLayout(False)
        Me.gbRetenciones.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cmbProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents pDown As System.Windows.Forms.Panel
    Friend WithEvents pTop As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnTasaCambio As System.Windows.Forms.Button
    Friend WithEvents rbDolares As System.Windows.Forms.RadioButton
    Friend WithEvents rbCordobas As System.Windows.Forms.RadioButton
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtTazaCambio As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_Recepcionado As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCK_Descuento As System.Windows.Forms.TextBox
    Friend WithEvents gbRetenciones As System.Windows.Forms.GroupBox
    Friend WithEvents txtPorcIR As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcIMI As System.Windows.Forms.TextBox
    Friend WithEvents CkIR As System.Windows.Forms.CheckBox
    Friend WithEvents CkIMI As System.Windows.Forms.CheckBox
    Friend WithEvents Ck_Descuento As System.Windows.Forms.CheckBox
    Friend WithEvents txtFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rbContado As System.Windows.Forms.RadioButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPlazo As System.Windows.Forms.TextBox
    Friend WithEvents cmdAddProveedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Fecha_Vencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Fecha_de_documento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNumDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pRight As System.Windows.Forms.Panel
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents pTotales As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtIVAExcento As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtTotalbruto As System.Windows.Forms.TextBox
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_IR As System.Windows.Forms.Label
    Friend WithEvents txtIMI As System.Windows.Forms.TextBox
    Friend WithEvents txtIR As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_IVA As System.Windows.Forms.Label
    Friend WithEvents Lbl_IMI As System.Windows.Forms.Label
    Friend WithEvents txtTotalNeto As System.Windows.Forms.TextBox
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents grdDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents pButtons As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents cmbTipExoneracion As DevExpress.XtraEditors.LookUpEdit
End Class
