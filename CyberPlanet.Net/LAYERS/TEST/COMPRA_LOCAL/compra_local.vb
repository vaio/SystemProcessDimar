﻿Imports DevExpress.XtraEditors.Controls
Imports SIGMA

Public Class compra_local

    Implements IMenu

    Private Proveedores As New Dictionary(Of String, Proveedor)
    Private ValidarCampoSoloEnteros As New Validar
    Private ValidarTextFactura As New Validar
    Private ValidarCampoDescuento As New Validar With {.CantDecimal = 2}

    Private cod_producto As String
    Private costo_dolar As Decimal
    Private costo_cordoba As Decimal
    Private cantidad As Double
    Private bonificacion As Double
    Private descuento As Double
    Private vencimiento As Date
    Private bodega As Integer

    Public Sub start() Handles Me.Load

        NumCatalogo = 21

        AddHandler Me.Resize, AddressOf responsive

        '-------------------------------- Botones  ----------------------------------------------

        AddHandler btnAgregar.Click, Sub(sender, e) gestion_sobre_producto(1)
        AddHandler btnModificar.Click, Sub(sender, e) gestion_sobre_producto(2)
        AddHandler btnEliminar.Click, Sub(sender, e) gestion_sobre_producto(3)

        AddHandler btnTasaCambio.Click, AddressOf get_otra_tasa_de_cambio

        AddHandler btnAplicar.Click, AddressOf Aplicar


        '------------------------------ Fechas   -------------------------------------------------


        AddHandler Fecha_de_documento.ValueChanged, AddressOf validar_fecha_documento_compra
        AddHandler Fecha_Vencimiento.ValueChanged, AddressOf validar_fecha_documento_vencimiento
        AddHandler Fecha_de_documento.ValueChanged, AddressOf get_tasa_de_cambio_por_fecha

        '-------------------------------- Otros ---------------------------------------------------

        AddHandler txtCK_Descuento.TextChanged, AddressOf validar_descuento_documento
        AddHandler cmbProveedor.EditValueChanged, AddressOf set_valores_de_proveedores
        AddHandler Ck_Descuento.CheckedChanged, AddressOf validar_descuento_check
        AddHandler txtPlazo.TextChanged, AddressOf validar_plazo_documento


        ValidarTextFactura.agregar_control(New List(Of Control)({txtFactura})).PermitirLetrasNumeros()
        ValidarCampoSoloEnteros.agregar_control(New List(Of Control)({txtPlazo})).SoloEnteros()
        ValidarCampoDescuento.agregar_control(New List(Of Control)({txtCK_Descuento})).ActivarEventosNumericos()

        responsive()
        ajustar()
        validar_descuento_check()
        get_tasa_de_cambio_por_fecha()
        gestion_agregar_producto_botones(False, False)
        set_valores_de_proveedores()

    End Sub
    Private Sub responsive()

        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None

    End Sub
    Private Sub get_proveedores()

        Proveedores = data_proveedor.get_datos_basicos_proveedores

        cmbProveedor.Properties.DataSource = Proveedores.Values

        cmbProveedor.Properties.DisplayMember = "get_Nombre"
        cmbProveedor.Properties.ValueMember = "get_ID"

        cmbProveedor.Properties.Columns.Add(New LookUpColumnInfo("get_ID", "No.", 20))

        cmbProveedor.Properties.Columns.Add(New LookUpColumnInfo("get_Nombre", "Proveedor", 100))

        cmbProveedor.Properties.Columns(0).Visible = False

        cmbProveedor.EditValue = "001"

        cmbProveedor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub
    Private Sub ajustar()

        Dim tipo_exoneraciones As New DataTable

        With tipo_exoneraciones

            .Columns.Add("id")
            .Columns.Add("Descripcion")

        End With

        tipo_exoneraciones.Rows.Add(0, "NINGUNA")
        tipo_exoneraciones.Rows.Add(1, "CARTA DE FRANQUISIA")
        tipo_exoneraciones.Rows.Add(2, "CARTA MINISTERIAL")
        tipo_exoneraciones.Rows.Add(3, "FORMATO DE EXONERACION DE IVA")

        cmbTipExoneracion.Properties.DataSource = tipo_exoneraciones

        cmbTipExoneracion.Properties.DisplayMember = "Descripcion"
        cmbTipExoneracion.Properties.ValueMember = "id"

        cmbTipExoneracion.Properties.Columns.Add(New LookUpColumnInfo("id", "No.", 20))

        cmbTipExoneracion.Properties.Columns.Add(New LookUpColumnInfo("Descripcion", "Descripcion", 100))

        cmbTipExoneracion.Properties.Columns(0).Visible = False

        cmbTipExoneracion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        cmbTipExoneracion.EditValue = 0

        cmbTipExoneracion.ItemIndex = 0

        get_proveedores()


    End Sub
    Private Sub validar_descuento_check()

        If Ck_Descuento.Checked = True Then

            txtCK_Descuento.Enabled = True
            txtCK_Descuento.Text = String.Empty

        Else

            txtCK_Descuento.Enabled = False
            txtCK_Descuento.Text = String.Empty

        End If

    End Sub
    Private Sub validar_fecha_documento_compra()

        Dim fecha_de_compra As Date = Fecha_de_documento.Value.Date
        Dim vencimiento As Date = Fecha_Vencimiento.Value.Date

        If fecha_de_compra.Date.Date > vencimiento.Date.Date Then

            MsgBox("Fecha de vencimiento no puede ser inferior a la fecha de compra.", MsgBoxStyle.Critical) : Fecha_de_documento.Value = Date.Now.Date

        ElseIf fecha_de_compra > Date.Now() Then


            MsgBox("Fecha de compra no puede ser mayor a la fecha actual.", MsgBoxStyle.Critical) : Fecha_de_documento.Value = Date.Now.Date


        Else

            txtPlazo.Text = IIf(DateDiff(DateInterval.Day, fecha_de_compra, vencimiento) = 0, "", DateDiff(DateInterval.Day, fecha_de_compra, vencimiento))

        End If

    End Sub
    Private Sub validar_fecha_documento_vencimiento()

        Dim fecha_de_compra As Date = Fecha_de_documento.Value.Date
        Dim vencimiento As Date = Fecha_Vencimiento.Value.Date

        If fecha_de_compra.Date.Date > vencimiento.Date.Date Then

            MsgBox("Fecha de compra no puede ser mayor a la fecha de vencimiento.", MsgBoxStyle.Critical) : Fecha_Vencimiento.Value = Date.Now.Date

        Else

            txtPlazo.Text = IIf(DateDiff(DateInterval.Day, fecha_de_compra, vencimiento) = 0, "", DateDiff(DateInterval.Day, fecha_de_compra, vencimiento))

        End If

    End Sub
    Private Sub validar_plazo_documento()

        If ChangeToDouble(txtPlazo.Text) = 0 Then Fecha_Vencimiento.Value = Fecha_de_documento.Value

        If Not ChangeToDouble(txtPlazo.Text) = 0 Then Fecha_Vencimiento.Value = Fecha_de_documento.Value.AddDays(ChangeToDouble(txtPlazo.Text))

    End Sub
    Private Sub validar_descuento_documento()

        Dim decuento As Double = ChangeToDouble(txtCK_Descuento.Text)

        If decuento > 99.99 Then txtCK_Descuento.Text = String.Empty


    End Sub
    Private Sub get_otra_tasa_de_cambio()

        NumCatalogo = 21

        Dim Tasa As New frmTasa_de_cambio With {.Size = New Size(364, 324), .Text = "Taza de Cambio", .StartPosition = FormStartPosition.CenterParent}

        Tasa.ShowDialog()

        If Tasa.get_tasa = 0 Then Exit Sub

        txtTazaCambio.Text = FormatNumber(Tasa.get_tasa, 4)

    End Sub
    Private Sub get_tasa_de_cambio_por_fecha()

        txtTazaCambio.Text = FormatNumber(data_tasa_de_cambio.get_tasa_de_cambio_actual(Funciones.Param("@Fecha", Fecha_de_documento.Value, SqlDbType.Date)), 4)

    End Sub
    Private Sub set_valores_de_proveedores()

        Dim Proveedor As New Proveedor

        Proveedor = Proveedores.Item(cmbProveedor.EditValue)

        CkIR.Checked = Proveedor.get_Is_IR

        CkIMI.Checked = Proveedor.get_Is_IMI

        txtPorcIR.Text = FormatNumber(Proveedor.get_Tasa_IR, 2)

        txtPorcIMI.Text = FormatNumber(Proveedor.get_Tasa_IMI, 2)



    End Sub
    '-------------------------------------------- Menu --------------------------------------------
    Public Function nuevo() As Integer Implements IMenu.nuevo

        Funciones.Limpiar(pTop.Controls)
        Funciones.Limpiar(pTotales.Controls)

        cmbProveedor.EditValue = "001"
        cmbTipExoneracion.ItemIndex = 0
        Ck_Descuento.Checked = False

        grdDetalle.DataSource = Nothing

        txtNumDoc.Text = data_compra_local.get_the_last_No_document

        txtTazaCambio.Text = FormatNumber(data_tasa_de_cambio.get_tasa_de_cambio_actual(), 4)

        PermitirBotonesEdicion(False)
        DesactivarCampos(False)

        gestion_agregar_producto_botones(False, False)

        validar_descuento_check()

        set_valores_de_proveedores()

        Return 0

    End Function

    Public Function guardar() As Integer Implements IMenu.guardar


        Try

            If validar_documento_master() = False Then MsgBox("Ops! verificar que el documento tenga un concepto, tasa de cambio mayor que 0 y numero de factura.", MsgBoxStyle.Exclamation) : Return 1

            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            If Not result = DialogResult.Yes Then Exit Function

            Dim Listo = data_compra_local.set_master(Funciones.Param("@Sucursal", My.Settings.Sucursal, SqlDbType.Int),
                                                        Funciones.Param("@NumCompra", txtNumDoc.Text, SqlDbType.NChar),
                                                        Funciones.Param("@TipoCompra", IIf(rbContado.Checked = True, 1, 2), SqlDbType.Int),
                                                        Funciones.Param("@NumProv", cmbProveedor.EditValue, SqlDbType.NChar),
                                                        Funciones.Param("@TipoDoc", 1, SqlDbType.Int),
                                                        Funciones.Param("@Fecha", Fecha_de_documento.Value, SqlDbType.Date),
                                                        Funciones.Param("@FechaVenc", Fecha_Vencimiento.Value, SqlDbType.Date),
                                                        Funciones.Param("@FechaRecep", Fecha_Recepcionado.Value, SqlDbType.Date),
                                                        Funciones.Param("@Plazo", ChangeToDouble(txtPlazo.Text), SqlDbType.Int),
                                                        Funciones.Param("@Porcentaje_RetIR", ChangeToDouble(txtPorcIR.Text), SqlDbType.Decimal),
                                                        Funciones.Param("@Porcentaje_RetIMI", ChangeToDouble(txtPorcIMI.Text), SqlDbType.Decimal),
                                                        Funciones.Param("@Observ", txtObservacion.Text.ToUpper, SqlDbType.NChar),
                                                        Funciones.Param("@TipoExon", cmbTipExoneracion.EditValue, SqlDbType.Int),
                                                        Funciones.Param("@NoFactura", txtFactura.Text, SqlDbType.Char),
                                                        Funciones.Param("@IsPorcentaje", Ck_Descuento.Checked, SqlDbType.Bit),
                                                        Funciones.Param("@Descuento", ChangeToDouble(txtCK_Descuento.Text), SqlDbType.Money),
                                                        Funciones.Param("@Aplicado", False, SqlDbType.Bit),
                                                        Funciones.Param("@MonedaCordoba", IIf(rbCordobas.Checked = True, True, False), SqlDbType.Bit),
                                                        Funciones.Param("@TazaCambio", ChangeToDouble(txtTazaCambio.Text), SqlDbType.Decimal),
                                                        Funciones.Param("@nTipoEdicion", nTipoEdic, SqlDbType.Int)
                                                        )



            If Listo = 0 Then

                MsgBox("La operación se realizó de forma exitosa", MsgBoxStyle.Information, "Información")

                CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), cmbProveedor.EditValue)

                PermitirBotonesEdicion(True)
                DesactivarCampos(True)

                gestion_agregar_producto_botones(True, IIf(grvDetalle.RowCount <= 0, False, True))

                Return 0

            Else

                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")

                gestion_agregar_producto_botones(False, False)

                Return 1

            End If

        Catch ex As Exception

            Call MsgBox("Error: " + ex.Message)

        End Try

    End Function
    '----------------------------------------------------------------------------------------------
    Private Function validar_documento_master() As Boolean


        If txtFactura.Text = String.Empty Then Return False
        If txtObservacion.Text = String.Empty Then Return False
        If ChangeToDouble(txtTazaCambio.Text) = 0 Then Return False

        Return True

    End Function

    Private Sub gestion_sobre_producto(ByRef accion As Integer) ' 1 agregar, 2 actualiza, 3 elimina

        Dim Agregar_producto As New compra_local_agregar_producto With {.StartPosition = FormStartPosition.CenterParent, .set_tasa = ChangeToDouble(txtTazaCambio.Text), .set_Ck_Descuento = Ck_Descuento.Checked, .set_Porc_Descuento = ChangeToDouble(txtCK_Descuento.Text), .set_Cordoba_Dolar = get_Cordoba_Dolares()}

        If accion = 1 Or accion = 2 Then If Ck_Descuento.Checked = True And ChangeToDouble(txtCK_Descuento.Text) = 0 Then MsgBox("Ops! vaya! has aplicado descuento con valor 0. por favor vuelve e ingresa un monto de descuento. de lo contrario deshabilita el descuento.", MsgBoxStyle.Exclamation) : Exit Sub

        If accion = 2 Or accion = 3 Then

            If grvDetalle.RowCount = 0 Then Exit Sub

            Agregar_producto.btn_buscar_producto.Enabled = False

            Agregar_producto.luBodegas.Enabled = False

            set_datos_producto_actualizar(Agregar_producto)

            Agregar_producto.get_data_to_compra_master()  'inserta  los datos del gid en los controles del formulario agregar producto 

        End If

        If accion = 3 Then Agregar_producto.pBody.Enabled = False

        Agregar_producto.ShowDialog()

        If Agregar_producto.get_cod_producto = String.Empty Then Exit Sub

        get_datos_producto(Agregar_producto)

        agregar_producto_a_detalle(accion)

    End Sub

    Private Sub get_datos_producto(ByRef Producto As compra_local_agregar_producto)

        cod_producto = Producto.get_cod_producto
        costo_dolar = Producto.get_costo_dolar
        costo_cordoba = Producto.get_costo_cordoba
        cantidad = Producto.get_cantidad
        bonificacion = Producto.get_bonificacion
        descuento = Producto.get_descuento
        vencimiento = Producto.get_vencimiento
        bodega = Producto.get_bodega

    End Sub

    Private Sub set_datos_producto_actualizar(ByRef Producto As compra_local_agregar_producto)

        If grvDetalle.SelectedRowsCount = 0 Then Exit Sub

        Producto.get_cod_producto = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
        Producto.get_descripcion = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Descripcion").ToString()
        Producto.get_bonificacion = CDbl(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cant. Bonificada").ToString())
        Producto.get_vencimiento = CDate(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Fecha expiracion").ToString())
        Producto.get_bodega = CInt(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "CodBodega").ToString())
        Producto.get_cantidad = CDbl(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad").ToString())

        If get_Cordoba_Dolares() = True Then

            Producto.get_costo_cordoba = CDbl(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo_Unitario").ToString())

            Producto.get_costo_dolar = FormatNumber((Producto.get_costo_cordoba / ChangeToDouble(txtTazaCambio.Text)), 4)

            Producto.get_descuento = CDbl(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Descuento").ToString())

        Else

            Producto.get_costo_cordoba = ChangeToDouble(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo_Unitario").ToString()) * ChangeToDouble(txtTazaCambio.Text)

            Producto.get_costo_dolar = ChangeToDouble(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo_Unitario").ToString())

            Producto.get_descuento = FormatNumber(CDbl(grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Descuento").ToString()) * ChangeToDouble(txtTazaCambio.Text), 4)

        End If

    End Sub

    Private Sub agregar_producto_a_detalle(ByRef TipoEdiccion As Integer)

        Dim Result = data_compra_local.set_detalle(
            Funciones.Param("@Sucursal", nSucursal, SqlDbType.Int),
            Funciones.Param("@NumCompra", txtNumDoc.Text, SqlDbType.NChar),
            Funciones.Param("@Bodega", bodega, SqlDbType.Int),
            Funciones.Param("@CodProd", cod_producto, SqlDbType.NChar),
            Funciones.Param("@FechaExp", vencimiento, SqlDbType.DateTime),
            Funciones.Param("@Cant", cantidad, SqlDbType.Int),
            Funciones.Param("@CantBonif", bonificacion, SqlDbType.Int),
            Funciones.Param("@CantDev", 0, SqlDbType.Int),
            Funciones.Param("@PrecUnit", costo_cordoba, SqlDbType.Float),
            Funciones.Param("@PrecUnitUS", costo_dolar, SqlDbType.Float),
            Funciones.Param("@Desc", descuento, SqlDbType.Money),
            Funciones.Param("@nTipoEdicion", TipoEdiccion, SqlDbType.Int)
            )

        If Result = 0 Then

            CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), cmbProveedor.EditValue)

        Else

            MsgBox("Ops! algo salio mal! verificar con soporte tecnico.", MsgBoxStyle.Critical)

        End If

    End Sub

    Private Function get_Cordoba_Dolares() As Boolean

        If rbCordobas.Checked = True Then Return True Else Return False

    End Function
    Private Sub gestion_agregar_producto_botones(ByRef estado As Boolean, estado_aplicar As Boolean)

        btnAgregar.Enabled = estado
        btnAplicar.Enabled = estado_aplicar
        btnModificar.Enabled = estado
        btnEliminar.Enabled = estado

    End Sub

    Public Function historial() As Integer Implements IMenu.historial

        frmMasterCompraLocal.Buscar()

    End Function

    Public Function reporte() As Integer Implements IMenu.imprimir

        Return 1

    End Function
    '--------------------------------------- reutilizacion de codigo! -----------------------------------------------------



    Sub CargarDetalleDoc(ByVal nSucursal As Integer, ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)

        Dim strSqlDoc = String.Format("Select * from vwComprasData where CodigoSucursal = {0} and Numero_de_Compra = '{1}' ", My.Settings.Sucursal, sNumDoc)
        Dim Master_compra As DataTable = SQL(strSqlDoc, "tblDatosMasterDoc", My.Settings.SolIndustrialCNX).Tables(0)

        If Master_compra.Rows.Count > 0 Then

            txtNumDoc.Text = Master_compra.Rows(0).Item("Numero_de_Compra")
            cmbProveedor.EditValue = Master_compra.Rows(0).Item("Codigo_Proveedor")
            Fecha_de_documento.Value = Master_compra.Rows(0).Item("Fecha")
            Fecha_Vencimiento.Value = Master_compra.Rows(0).Item("FechaVencimiento")
            Fecha_Recepcionado.Value = IIf(Master_compra.Rows(0).Item("FechaRecepcion") Is DBNull.Value, Date.Now, Master_compra.Rows(0).Item("FechaRecepcion"))
            txtPlazo.Text = Master_compra.Rows(0).Item("Plazo")
            txtObservacion.Text = Master_compra.Rows(0).Item("Observaciones")
            cmbTipExoneracion.EditValue = Master_compra.Rows(0).Item("TipoExoneracion")
            txtSubtotal.Text = FormatNumber(Master_compra.Rows(0).Item("Total_Compra"), 2)
            txtDescuento.Text = FormatNumber(Master_compra.Rows(0).Item("Total_Descuentos"), 2)
            txtIR.Text = FormatNumber(IIf(Master_compra.Rows(0).Item("Total_RetencionIR") Is DBNull.Value, 0, Master_compra.Rows(0).Item("Total_RetencionIR")), 2)
            txtIMI.Text = FormatNumber(IIf(Master_compra.Rows(0).Item("Total_RetencionIMI") Is DBNull.Value, 0, Master_compra.Rows(0).Item("Total_RetencionIMI")), 2)
            txtIVA.Text = FormatNumber(IIf(Master_compra.Rows(0).Item("Total_ImpuestoIVA") Is DBNull.Value, 0, Master_compra.Rows(0).Item("Total_ImpuestoIVA")), 2)
            txtTotalNeto.Text = FormatNumber(IIf(Master_compra.Rows(0).Item("Total_Neto") Is DBNull.Value, 0, Master_compra.Rows(0).Item("Total_Neto")), 2)
            txtFactura.Text = Master_compra.Rows(0).Item("NoFactura")
            Ck_Descuento.Checked = Master_compra.Rows(0).Item("IsPorcentaje")
            txtCK_Descuento.Text = FormatNumber(Master_compra.Rows(0).Item("Descuento"), 2)
            txtTazaCambio.Text = Master_compra.Rows(0).Item("TazaCambio")
            txtTotalbruto.Text = FormatNumber(IIf(Master_compra.Rows(0).Item("Total_Bruto") Is DBNull.Value, 0, Master_compra.Rows(0).Item("Total_Bruto")), 2)
            txtIVAExcento.Text = FormatNumber(IIf(Master_compra.Rows(0).Item("Total_IVA_Excento") Is DBNull.Value, 0, Master_compra.Rows(0).Item("Total_IVA_Excento")), 2)

            If Master_compra.Rows(0).Item("MonedaCordoba") = True Then
                rbCordobas.Checked = True
            Else
                rbDolares.Checked = True
            End If

            lblEstado.Text = "SIN APLICAR"

            If Master_compra.Rows(0).Item("Aplicado") = True Then
                lblEstado.Text = "COMPRA APLICADA"

                gestion_agregar_producto_botones(False, False)

            Else

                gestion_agregar_producto_botones(True, True)

            End If

            CargarDetalleCompra(sNumDoc, nTipoCompra, sProveedor)

        Else
            nuevo()

            txtNumDoc.Focus()

            gestion_agregar_producto_botones(False, False)

        End If
    End Sub

    Sub CargarDetalleCompra(ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)
        Dim StrsqlDetDoc As String = ""
        Dim TableDatos As DataTable

        StrsqlDetDoc = "Select Codigo,Descripcion,CodBodega,Bodega,Entrada,Fecha_Expiracion as 'Fecha expiracion',Excento_IVA,Cantidad,[Cant. Bonificada],Costo_Unitario,[Sub-Total],Descuento,Total from vwDetalleComprasLoc where "
        StrsqlDetDoc = String.Format("{0} CodigoSucursal = {1} and  Numero_de_Compra = '{2}' ", StrsqlDetDoc, My.Settings.Sucursal, sNumDoc)
        ' StrsqlDetDoc = String.Format("{0} and Tipo_Compra = {1} and Codigo_Proveedor = '{2}'", StrsqlDetDoc, nTipoCompra, sProveedor)

        TableDatos = SQL(StrsqlDetDoc, "tblDetalleDoc", My.Settings.SolIndustrialCNX).Tables(0)

        grdDetalle.DataSource = TableDatos

        grvDetalle.Columns("Codigo").Width = 75
        grvDetalle.Columns("Descripcion").Width = 400
        grvDetalle.Columns("CodBodega").Visible = False
        grvDetalle.Columns("Bodega").Width = 200
        grvDetalle.Columns("Entrada").Width = 90
        grvDetalle.Columns("Fecha expiracion").Width = 100
        grvDetalle.Columns("Excento_IVA").Width = 70
        grvDetalle.Columns("Cantidad").Width = 80
        grvDetalle.Columns("Cant. Bonificada").Width = 100
        grvDetalle.Columns("Costo_Unitario").Width = 100
        grvDetalle.Columns("Sub-Total").Width = 100
        grvDetalle.Columns("Descuento").Width = 100
        grvDetalle.Columns("Total").Width = 100

        If Not lblEstado.Text = "COMPRA APLICADA" Then

            If grvDetalle.RowCount <= 0 Then
                Ck_Descuento.Enabled = True
                frmPrincipal.bbiEliminar.Enabled = True
                btnAgregar.Enabled = True
                btnAplicar.Enabled = True
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
                btnAplicar.Enabled = False
            Else
                Ck_Descuento.Enabled = False
                txtCK_Descuento.Enabled = False
                frmPrincipal.bbiEliminar.Enabled = False
                btnAgregar.Enabled = True
                btnAplicar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
                btnAplicar.Enabled = True
            End If

        End If
    End Sub
    Public Function Modificar() As Integer Implements IMenu.modificar
        If Not lblEstado.Text = "COMPRA APLICADA" Then
            nTipoEdic = 2
            PermitirBotonesEdicion(False)
            DesactivarCampos(False)
            txtNumDoc.Focus()

            gestion_agregar_producto_botones(False, False)

            If grvDetalle.RowCount <= 0 Then
                Ck_Descuento.Enabled = True

            Else
                Ck_Descuento.Enabled = False
                txtCK_Descuento.Enabled = False
            End If

            Return 0

        Else

            MsgBox("No se puede modificar una compra aplicada.", MsgBoxStyle.Information, "Información")

            Return 1

        End If

    End Function
    Public Function eliminar() As Integer Implements IMenu.eliminar
        '' pendiente

        Return 0

    End Function
    Public Function salir() As Integer Implements IMenu.salir

        Me.Close()

        Return 0

    End Function
    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)

        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado

    End Sub
    Sub DesactivarCampos(ByVal bIsActivo As Boolean)

        pTop.Enabled = Not bIsActivo
        cmbProveedor.Enabled = Not bIsActivo
        Fecha_de_documento.Enabled = Not bIsActivo
        Fecha_Vencimiento.Enabled = Not bIsActivo
        Fecha_Recepcionado.Enabled = Not bIsActivo
        txtFactura.Enabled = Not bIsActivo
        CkIR.Enabled = Not bIsActivo
        CkIMI.Enabled = Not bIsActivo
        txtPorcIR.Enabled = Not bIsActivo
        txtPorcIMI.Enabled = Not bIsActivo
        cmdAddProveedor.Enabled = Not bIsActivo
        txtCK_Descuento.Enabled = Not bIsActivo
        Ck_Descuento.Enabled = Not bIsActivo
        txtPlazo.Enabled = Not bIsActivo
        txtObservacion.Enabled = Not bIsActivo
        cmbTipExoneracion.Enabled = Not bIsActivo

    End Sub
    Public Function Cancelar() As Integer Implements IMenu.cancelar

        nTipoEdic = 1
        NumCatalogo = 21

        CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), cmbProveedor.EditValue)

        PermitirBotonesEdicion(True)

        DesactivarCampos(True)

        If grvDetalle.RowCount <= 0 Then
            frmPrincipal.bbiEliminar.Enabled = True
        Else
            frmPrincipal.bbiEliminar.Enabled = False
        End If

        Return 0

    End Function

    Private Sub Aplicar()

        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim Resum As String = ""
        Dim ResumEntrada As String = ""
        Dim CodigoProd As String = ""
        Dim strsql As String = ""
        Dim Cantidad As Integer = 0
        Dim Costo As Double = 0
        Dim Cod_bodega As Integer = 0
        Dim bodega As String = String.Empty
        Dim CodMaxEntrada As String

        Try

            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            If Not result = DialogResult.Yes Then Exit Sub

            If Not grvDetalle.RowCount > 0 Then Exit Sub

            For i As Integer = 0 To grvDetalle.DataRowCount - 1

                CodigoProd = grvDetalle.GetRowCellValue(i, "Codigo")
                Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                Costo = grvDetalle.GetRowCellValue(i, "Costo_Unitario")
                Cod_bodega = grvDetalle.GetRowCellValue(i, "CodBodega")
                bodega = grvDetalle.GetRowCellValue(i, "Bodega")

                strsql = String.Format("SELECT Codigo_Movimiento FROM Movimientos_Almacenes WHERE CodigoSucursal = {0} and Bodega = {1} and Tipo_Movimiento = 4 and Codigo_Documento = {2} and Tipo_Documento = 18", My.Settings.Sucursal, Cod_bodega, txtNumDoc.Text)
                Dim tblCodigo As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

                If tblCodigo.Rows.Count > 0 Then

                    CodMaxEntrada = tblCodigo.Rows(0).Item(0)

                Else
                    CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(Cod_bodega, 4), 8)

                    ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, txtNumDoc.Text, 18, ("ENTRADA A BODEGA DE " + bodega.ToUpper), 0, Date.Now, 4, False, 1, Cod_bodega)
                End If

                ''''''''''''''''''''''''''''''''''''''' written by bob ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Dim bonificacion As Double = grvDetalle.GetRowCellValue(i, "Cant. Bonificada")

                Dim cantidad_real As Double = bonificacion + Cantidad

                Costo = IIf(rbDolares.Checked = True, Costo * ChangeToDouble(txtTazaCambio.Text), Costo)

                Dim costo_real As Double = ((Cantidad * Costo) / cantidad_real)

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Resum = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, CodigoProd, cantidad_real, FormatNumber(costo_real, 4), txtFactura.Text, Cod_bodega, 0, 1, 4)

                '----------------------------------------------------------------------------------------------------------------------------------

            Next

            If Resum = "OK" Then Resum = MasterDocument.EditarMasterCompraLocal(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), cmbProveedor.EditValue, 1, Fecha_de_documento.Value, Fecha_Vencimiento.Value, Fecha_Recepcionado.Value, CInt(txtPlazo.Text), CDbl(txtPorcIR.Text), CDbl(txtPorcIMI.Text), txtObservacion.Text.ToUpper, cmbTipExoneracion.EditValue, txtFactura.Text, Ck_Descuento.Checked, txtCK_Descuento.Text, True, IIf(rbCordobas.Checked = True, True, False), CDbl(txtTazaCambio.Text), 2)

            Resum = MasterDocument.EditarMasterPagos(nSucursal,
                                                     cmbProveedor.EditValue,
                                                     18,
                                                     txtNumDoc.Text,
                                                     Fecha_de_documento.Value,
                                                     Fecha_Vencimiento.Value,
                                                     txtNumDoc.Text,
                                                     2021,
                                                     txtFactura.Text,
                                                     IIf(rbDolares.Checked = True, ChangeToDouble(txtSubtotal.Text) * ChangeToDouble(txtTazaCambio.Text), ChangeToDouble(txtSubtotal.Text)),
                                                     IIf(rbDolares.Checked = True, ChangeToDouble(txtIR.Text) * ChangeToDouble(txtTazaCambio.Text), ChangeToDouble(txtIR.Text)),
                                                     IIf(rbDolares.Checked = True, ChangeToDouble(txtIMI.Text) * ChangeToDouble(txtTazaCambio.Text), ChangeToDouble(txtIMI.Text)),
                                                     IIf(rbDolares.Checked = True, ChangeToDouble(txtIVA.Text) * ChangeToDouble(txtTazaCambio.Text), ChangeToDouble(txtIVA.Text)),
                                                     0, 0, 0, Date.Now, True, 1)

            MsgBox("Compra aplicada correctamente", MsgBoxStyle.Information, "Información")

            CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), cmbProveedor.EditValue)

            Dim Reporte_de_movimiento As New reporte_moviemiento_almacen.compra_local(nSucursal, txtNumDoc.Text)

            Reporte_de_movimiento.visualizar()

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub
End Class

