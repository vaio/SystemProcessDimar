﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class compra_local_agregar_producto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(compra_local_agregar_producto))
        Me.pBody = New System.Windows.Forms.Panel()
        Me.total_cordoba = New System.Windows.Forms.TextBox()
        Me.total_dolar = New System.Windows.Forms.TextBox()
        Me.btn_buscar_producto = New System.Windows.Forms.Button()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCostoUnitUSS = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCodigoProd = New System.Windows.Forms.TextBox()
        Me.txtPorcenDescuento = New System.Windows.Forms.TextBox()
        Me.txtPorcentaje = New System.Windows.Forms.CheckBox()
        Me.luBodegas = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCantBonif = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.deFechaExp = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCostoUnit = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.pBody.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luBodegas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.total_cordoba)
        Me.pBody.Controls.Add(Me.total_dolar)
        Me.pBody.Controls.Add(Me.btn_buscar_producto)
        Me.pBody.Controls.Add(Me.Tabla)
        Me.pBody.Controls.Add(Me.Label12)
        Me.pBody.Controls.Add(Me.txtCostoUnitUSS)
        Me.pBody.Controls.Add(Me.Label11)
        Me.pBody.Controls.Add(Me.txtCodigoProd)
        Me.pBody.Controls.Add(Me.txtPorcenDescuento)
        Me.pBody.Controls.Add(Me.txtPorcentaje)
        Me.pBody.Controls.Add(Me.luBodegas)
        Me.pBody.Controls.Add(Me.txtDescuento)
        Me.pBody.Controls.Add(Me.Label10)
        Me.pBody.Controls.Add(Me.Label9)
        Me.pBody.Controls.Add(Me.txtCantBonif)
        Me.pBody.Controls.Add(Me.Label8)
        Me.pBody.Controls.Add(Me.deFechaExp)
        Me.pBody.Controls.Add(Me.Label6)
        Me.pBody.Controls.Add(Me.Label5)
        Me.pBody.Controls.Add(Me.txtCostoUnit)
        Me.pBody.Controls.Add(Me.Label1)
        Me.pBody.Controls.Add(Me.Label2)
        Me.pBody.Controls.Add(Me.Label7)
        Me.pBody.Controls.Add(Me.Label3)
        Me.pBody.Controls.Add(Me.txtCantidad)
        Me.pBody.Controls.Add(Me.lblDescripcion)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 0)
        Me.pBody.Margin = New System.Windows.Forms.Padding(4)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(674, 420)
        Me.pBody.TabIndex = 2
        '
        'total_cordoba
        '
        Me.total_cordoba.BackColor = System.Drawing.Color.Cornsilk
        Me.total_cordoba.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.total_cordoba.Enabled = False
        Me.total_cordoba.Location = New System.Drawing.Point(167, 288)
        Me.total_cordoba.Margin = New System.Windows.Forms.Padding(4)
        Me.total_cordoba.MaxLength = 12
        Me.total_cordoba.Name = "total_cordoba"
        Me.total_cordoba.Size = New System.Drawing.Size(130, 22)
        Me.total_cordoba.TabIndex = 9
        '
        'total_dolar
        '
        Me.total_dolar.BackColor = System.Drawing.Color.Cornsilk
        Me.total_dolar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.total_dolar.Enabled = False
        Me.total_dolar.Location = New System.Drawing.Point(167, 257)
        Me.total_dolar.Margin = New System.Windows.Forms.Padding(4)
        Me.total_dolar.MaxLength = 12
        Me.total_dolar.Name = "total_dolar"
        Me.total_dolar.Size = New System.Drawing.Size(130, 22)
        Me.total_dolar.TabIndex = 8
        '
        'btn_buscar_producto
        '
        Me.btn_buscar_producto.ForeColor = System.Drawing.Color.Green
        Me.btn_buscar_producto.Location = New System.Drawing.Point(303, 17)
        Me.btn_buscar_producto.Name = "btn_buscar_producto"
        Me.btn_buscar_producto.Size = New System.Drawing.Size(41, 30)
        Me.btn_buscar_producto.TabIndex = 39
        Me.btn_buscar_producto.Text = "+"
        Me.btn_buscar_producto.UseVisualStyleBackColor = True
        '
        'Tabla
        '
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        GridLevelNode1.RelationName = "Level1"
        Me.Tabla.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.Tabla.Location = New System.Drawing.Point(313, 126)
        Me.Tabla.MainView = Me.GridView1
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(335, 256)
        Me.Tabla.TabIndex = 38
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowViewCaption = True
        Me.GridView1.ViewCaption = "Existencias en Bodegas"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(27, 257)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(106, 17)
        Me.Label12.TabIndex = 37
        Me.Label12.Text = "Costo Total ($):"
        '
        'txtCostoUnitUSS
        '
        Me.txtCostoUnitUSS.BackColor = System.Drawing.Color.Cornsilk
        Me.txtCostoUnitUSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCostoUnitUSS.Enabled = False
        Me.txtCostoUnitUSS.Location = New System.Drawing.Point(167, 190)
        Me.txtCostoUnitUSS.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCostoUnitUSS.MaxLength = 12
        Me.txtCostoUnitUSS.Name = "txtCostoUnitUSS"
        Me.txtCostoUnitUSS.Size = New System.Drawing.Size(130, 22)
        Me.txtCostoUnitUSS.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(27, 193)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(123, 17)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Costo Unitario ($):"
        '
        'txtCodigoProd
        '
        Me.txtCodigoProd.BackColor = System.Drawing.Color.Cornsilk
        Me.txtCodigoProd.Enabled = False
        Me.txtCodigoProd.Location = New System.Drawing.Point(167, 22)
        Me.txtCodigoProd.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigoProd.Name = "txtCodigoProd"
        Me.txtCodigoProd.Size = New System.Drawing.Size(132, 22)
        Me.txtCodigoProd.TabIndex = 1
        '
        'txtPorcenDescuento
        '
        Me.txtPorcenDescuento.BackColor = System.Drawing.Color.White
        Me.txtPorcenDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPorcenDescuento.Enabled = False
        Me.txtPorcenDescuento.Location = New System.Drawing.Point(167, 324)
        Me.txtPorcenDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcenDescuento.MaxLength = 5
        Me.txtPorcenDescuento.Name = "txtPorcenDescuento"
        Me.txtPorcenDescuento.Size = New System.Drawing.Size(130, 22)
        Me.txtPorcenDescuento.TabIndex = 10
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.AutoSize = True
        Me.txtPorcentaje.Location = New System.Drawing.Point(31, 325)
        Me.txtPorcentaje.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(118, 21)
        Me.txtPorcentaje.TabIndex = 31
        Me.txtPorcentaje.Text = " % Descuento"
        Me.txtPorcentaje.UseVisualStyleBackColor = True
        '
        'luBodegas
        '
        Me.luBodegas.Location = New System.Drawing.Point(167, 90)
        Me.luBodegas.Margin = New System.Windows.Forms.Padding(4)
        Me.luBodegas.Name = "luBodegas"
        Me.luBodegas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luBodegas.Size = New System.Drawing.Size(486, 23)
        Me.luBodegas.TabIndex = 3
        '
        'txtDescuento
        '
        Me.txtDescuento.BackColor = System.Drawing.Color.White
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescuento.Location = New System.Drawing.Point(167, 357)
        Me.txtDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescuento.MaxLength = 12
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(130, 22)
        Me.txtDescuento.TabIndex = 11
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(27, 361)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 17)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Descuento:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 160)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(138, 17)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Cantidad Bonificada:"
        '
        'txtCantBonif
        '
        Me.txtCantBonif.BackColor = System.Drawing.Color.White
        Me.txtCantBonif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCantBonif.Location = New System.Drawing.Point(167, 158)
        Me.txtCantBonif.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantBonif.MaxLength = 12
        Me.txtCantBonif.Name = "txtCantBonif"
        Me.txtCantBonif.Size = New System.Drawing.Size(130, 22)
        Me.txtCantBonif.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(395, 26)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 17)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Fecha Expiración:"
        Me.Label8.Visible = False
        '
        'deFechaExp
        '
        Me.deFechaExp.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaExp.Location = New System.Drawing.Point(524, 22)
        Me.deFechaExp.Margin = New System.Windows.Forms.Padding(4)
        Me.deFechaExp.Name = "deFechaExp"
        Me.deFechaExp.Size = New System.Drawing.Size(129, 22)
        Me.deFechaExp.TabIndex = 4
        Me.deFechaExp.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 94)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Bodega:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 290)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(115, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Costo Total (C$):"
        '
        'txtCostoUnit
        '
        Me.txtCostoUnit.BackColor = System.Drawing.Color.Cornsilk
        Me.txtCostoUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCostoUnit.Enabled = False
        Me.txtCostoUnit.Location = New System.Drawing.Point(167, 223)
        Me.txtCostoUnit.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCostoUnit.MaxLength = 12
        Me.txtCostoUnit.Name = "txtCostoUnit"
        Me.txtCostoUnit.Size = New System.Drawing.Size(130, 22)
        Me.txtCostoUnit.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo Producto:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 57)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripción:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 128)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 17)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Cantidad:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(27, 226)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Costo Unitario (C$):"
        '
        'txtCantidad
        '
        Me.txtCantidad.BackColor = System.Drawing.Color.White
        Me.txtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCantidad.Location = New System.Drawing.Point(167, 126)
        Me.txtCantidad.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidad.MaxLength = 12
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(130, 22)
        Me.txtCantidad.TabIndex = 4
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.Cornsilk
        Me.lblDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDescripcion.Location = New System.Drawing.Point(167, 55)
        Me.lblDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(487, 24)
        Me.lblDescripcion.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 420)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(674, 60)
        Me.Panel1.TabIndex = 3
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(544, 10)
        Me.cmdCancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(104, 43)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(432, 10)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(104, 43)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'compra_local_agregar_producto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(674, 480)
        Me.ControlBox = False
        Me.Controls.Add(Me.pBody)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "compra_local_agregar_producto"
        Me.Text = "frmAgregar_productos_compras"
        Me.pBody.ResumeLayout(False)
        Me.pBody.PerformLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luBodegas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnitUSS As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoProd As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcenDescuento As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcentaje As System.Windows.Forms.CheckBox
    Friend WithEvents luBodegas As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCantBonif As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents deFechaExp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnit As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btn_buscar_producto As System.Windows.Forms.Button
    Friend WithEvents total_cordoba As System.Windows.Forms.TextBox
    Friend WithEvents total_dolar As System.Windows.Forms.TextBox
End Class
