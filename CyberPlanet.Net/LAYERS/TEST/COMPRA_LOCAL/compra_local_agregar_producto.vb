﻿Imports DevExpress.XtraEditors.Controls

Public Class compra_local_agregar_producto

    Private Tasa As Double 'hola probando lala
    Private Ck_Descuento As Boolean
    Private Porc_Descuento As Double
    Private Cordoba_dolar_compra As Boolean

    Private cod_producto As String
    Private costo_dolar As Decimal
    Private costo_cordoba As Decimal
    Private cantidad As Double
    Private bonificacion As Double
    Private descuento As Double
    Private vencimiento As Date
    Private bodega As Integer
    Private descripcion As String

    Public Property get_cod_producto As String
        Get
            Return Me.cod_producto
        End Get
        Set(value As String)
            Me.cod_producto = value
        End Set
    End Property
    Public Property get_costo_dolar As Decimal
        Get
            Return Me.costo_dolar
        End Get
        Set(value As Decimal)
            Me.costo_dolar = value
        End Set
    End Property
    Public Property get_costo_cordoba As Decimal
        Get
            Return Me.costo_cordoba
        End Get
        Set(value As Decimal)
            Me.costo_cordoba = value
        End Set
    End Property
    Public Property get_cantidad As Double
        Get
            Return Me.cantidad
        End Get
        Set(value As Double)
            Me.cantidad = value
        End Set
    End Property
    Public Property get_bonificacion As Double
        Get
            Return Me.bonificacion
        End Get
        Set(value As Double)
            Me.bonificacion = value
        End Set
    End Property
    Public Property get_descuento As Double
        Get
            Return Me.descuento
        End Get
        Set(value As Double)
            Me.descuento = value
        End Set
    End Property
    Public Property get_vencimiento As Date
        Get
            Return Me.vencimiento
        End Get
        Set(value As Date)
            Me.vencimiento = value
        End Set
    End Property
    Public Property get_bodega As Integer
        Get
            Return Me.bodega
        End Get
        Set(value As Integer)
            Me.bodega = value
        End Set
    End Property
    Public Property get_descripcion As String
        Get
            Return Me.descripcion
        End Get
        Set(value As String)
            Me.descripcion = value
        End Set
    End Property


    Public WriteOnly Property set_tasa As Double
        Set(value As Double)
            Me.Tasa = value
        End Set
    End Property
    Public WriteOnly Property set_Ck_Descuento As Double
        Set(value As Double)
            Me.Ck_Descuento = value
        End Set
    End Property
    Public WriteOnly Property set_Porc_Descuento As Double
        Set(value As Double)
            Me.Porc_Descuento = value
        End Set
    End Property
    Public WriteOnly Property set_Cordoba_Dolar As Boolean
        Set(value As Boolean)
            Me.Cordoba_dolar_compra = value
        End Set
    End Property

    Private ValidarCampoCantidad As New Validar With {.CantDecimal = 2}
    Private ValidarCampoDescuento As New Validar With {.CantDecimal = 4}
    Private ValidarCamposMoneda As New Validar With {.CantDecimal = 2}

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        set_bodegas()

        AddHandler Me.Load, AddressOf start

    End Sub

    Private Sub start()

        AddHandler btn_buscar_producto.Click, AddressOf get_producto
        AddHandler txtPorcentaje.CheckedChanged, AddressOf CkDescuento
        AddHandler txtCantidad.TextChanged, AddressOf set_calcular_total

        AddHandler txtPorcenDescuento.TextChanged, AddressOf set_calculo_descuento
        AddHandler txtPorcenDescuento.TextChanged, AddressOf validar_descuento_documento
        AddHandler txtCostoUnit.TextChanged, AddressOf set_calculo_descuento
        AddHandler txtCostoUnitUSS.TextChanged, AddressOf set_calculo_descuento
        AddHandler txtCantidad.TextChanged, AddressOf set_calculo_descuento
        AddHandler luBodegas.EditValueChanged, AddressOf get_costo_promedio_por_bodega
        AddHandler txtDescuento.TextChanged, AddressOf validar_descuento_no_mayor


        AddHandler total_cordoba.TextChanged, AddressOf set_calcular_total
        AddHandler total_dolar.TextChanged, AddressOf set_calcular_total


        AddHandler cmdAceptar.Click, AddressOf aceptar
        AddHandler cmdCancelar.Click, AddressOf cancelar

        get_datos_de_compras()

        ValidarCampoCantidad.agregar_control(New List(Of Control)({txtCantidad, txtCantBonif})).ActivarEventosNumericos()
        ValidarCampoDescuento.agregar_control(New List(Of Control)({txtPorcenDescuento})).ActivarEventosNumericos()
        ValidarCamposMoneda.agregar_control(New List(Of Control)({total_cordoba, txtDescuento, total_dolar})).ActivarEventosNumericos()


    End Sub

    Private Sub aceptar()

        If validar_aceptar() = False Then Exit Sub

        If validar_descuento_aceptar() = False Then MsgBox("Ops! no esta permitido ingresar un descuento mayor al total comprado por articulo", MsgBoxStyle.Exclamation) : Exit Sub

        send_data_to_compra_master()

        Me.Close()

    End Sub
    Private Function validar_aceptar() As Boolean

        If txtCodigoProd.Text = String.Empty Then MsgBox("Ops! no puedes proceder sin antes seleccionar un articulo", MsgBoxStyle.Exclamation) : Return False

        If ChangeToDouble(txtCantidad.Text) = 0 Then MsgBox("Ops! no puedes proceder sin antes ingresar una cantidad mayor a 0", MsgBoxStyle.Exclamation) : Return False

        If ChangeToDouble(total_cordoba.Text) = 0 Or ChangeToDouble(total_dolar.Text) = 0 Then MsgBox("Ops! no puedes proceder sin antes ingresar un total mayor a 0", MsgBoxStyle.Exclamation) : Return False

        Return True

    End Function
    Private Function validar_descuento_aceptar() As Boolean

        If ChangeToDouble(txtDescuento.Text) > ChangeToDouble(total_cordoba.Text) Then Return False

        Return True

    End Function

    Private Sub cancelar()

        Me.cod_producto = String.Empty
        Me.costo_dolar = 0
        Me.costo_cordoba = 0
        Me.cantidad = 0
        Me.bonificacion = 0
        Me.descuento = 0
        Me.vencimiento = Date.Now

        Me.Close()

    End Sub

    Private Sub send_data_to_compra_master()

        Me.bodega = luBodegas.EditValue
        Me.cod_producto = txtCodigoProd.Text
        Me.costo_dolar = ChangeToDouble(txtCostoUnitUSS.Text)
        Me.costo_cordoba = ChangeToDouble(txtCostoUnit.Text)
        Me.cantidad = ChangeToDouble(txtCantidad.Text)
        Me.bonificacion = ChangeToDouble(txtCantBonif.Text)
        Me.descuento = ChangeToDouble(txtDescuento.Text)
        Me.vencimiento = deFechaExp.Value


    End Sub
    Public Sub get_data_to_compra_master()

        luBodegas.EditValue = Me.bodega

        lblDescripcion.Text = Me.descripcion
        deFechaExp.Value = Me.vencimiento

        txtCodigoProd.Text = Me.cod_producto
        txtCantidad.Text = Me.cantidad
        txtCantBonif.Text = Me.bonificacion

        If Cordoba_dolar_compra = True Then

            total_cordoba.Text = FormatNumber(Me.costo_cordoba * Me.cantidad, 4)

        Else

            total_dolar.Text = FormatNumber(Me.costo_dolar * Me.cantidad, 4)

        End If

        txtDescuento.Text = Me.descuento


    End Sub
    Private Sub get_datos_de_compras()

        If Ck_Descuento = True Then txtPorcenDescuento.Text = Porc_Descuento : txtPorcentaje.Checked = Ck_Descuento

        If Cordoba_dolar_compra = True Then total_cordoba.Enabled = True : total_dolar.Enabled = False : total_cordoba.BackColor = Color.WhiteSmoke

        If Cordoba_dolar_compra = False Then total_cordoba.Enabled = False : total_dolar.Enabled = True : total_dolar.BackColor = Color.WhiteSmoke

    End Sub


    Private Sub set_bodegas()

        luBodegas.Properties.DataSource = data_fill_up_comb.get_bodegas_M_PT

        luBodegas.Properties.DisplayMember = "Nombre_Bodega"
        luBodegas.Properties.ValueMember = "Codigo_Bodega"

        luBodegas.Properties.Columns.Add(New LookUpColumnInfo("Codigo_Bodega", "No.", 20))

        luBodegas.Properties.Columns.Add(New LookUpColumnInfo("Nombre_Bodega", "BODEGA DE DESTINO", 100))

        luBodegas.Properties.Columns(0).Visible = False

        luBodegas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        luBodegas.EditValue = 2

    End Sub

    Private Sub get_producto()

        Dim Productos As New compra_local_busqueda_producto With {.StartPosition = FormStartPosition.CenterParent, .set_bodega = luBodegas.EditValue}

        Productos.ShowDialog()

        get_data_producto(Productos)

    End Sub

    Private Sub get_data_producto(ByRef Producto As compra_local_busqueda_producto)

        If Producto.get_codigo = String.Empty Then Exit Sub

        txtCodigoProd.Text = Producto.get_codigo
        lblDescripcion.Text = Producto.get_nombre
        txtCostoUnit.Text = FormatNumber(Producto.get_costo, 4)
        txtCostoUnitUSS.Text = FormatNumber((Producto.get_costo / Tasa), 4)

        Tabla.DataSource = data_producto.get_existencia_en_bodegas(Producto.get_codigo)

        GridView1.BestFitColumns()

    End Sub

    Private Sub set_calcular_total()

        Dim Cantidad = ChangeToDouble(txtCantidad.Text)

        If Cantidad = 0 Then txtCostoUnit.Text = FormatNumber(0, 4) : txtCostoUnitUSS.Text = FormatNumber(0, 4) : If Cordoba_dolar_compra = True Then total_dolar.Text = FormatNumber(0, 4) : Exit Sub Else total_cordoba.Text = FormatNumber(0, 4) : Exit Sub

        'If Cordoba_dolar_compra = True Then

        '    txtCostoUnitUSS.Text = FormatNumber((ChangeToDouble(txtCostoUnit.Text) / Tasa), 4)

        'Else

        '    txtCostoUnit.Text = FormatNumber((ChangeToDouble(txtCostoUnitUSS.Text) * Tasa), 4)

        'End If

        'total_cordoba.Text = FormatNumber(Cantidad * ChangeToDouble(txtCostoUnit.Text), 4)

        'total_dolar.Text = FormatNumber(Cantidad * ChangeToDouble(txtCostoUnitUSS.Text), 4)

        If Cordoba_dolar_compra = True Then

            txtCostoUnitUSS.Text = (((ChangeToDouble(total_cordoba.Text) / Tasa) / Cantidad))

            total_dolar.Text = ChangeToDouble(txtCostoUnitUSS.Text) * Cantidad

            txtCostoUnit.Text = (((ChangeToDouble(total_dolar.Text) / Cantidad) * Tasa))

        Else

            txtCostoUnit.Text = (((ChangeToDouble(total_dolar.Text) / Cantidad) * Tasa))

            txtCostoUnitUSS.Text = ((ChangeToDouble(total_dolar.Text) / Cantidad))

            total_cordoba.Text = ChangeToDouble(txtCostoUnit.Text) * Cantidad

        End If

    End Sub

    Private Sub set_calculo_descuento()

        If txtPorcentaje.Checked = False Then Exit Sub

        Dim Porcentaje As Double = (ChangeToDouble(txtPorcenDescuento.Text) / 100)

        txtDescuento.Text = FormatNumber(Porcentaje * ChangeToDouble(total_cordoba.Text))

    End Sub

    Private Sub get_costo_promedio_por_bodega()

        If txtCodigoProd.Text = String.Empty Then Exit Sub

        txtCostoUnit.Text = FormatNumber(data_producto.get_existencia_costo_por_bodega_por_producto(Funciones.Param("@cod_bodega", Me.luBodegas.EditValue, SqlDbType.Int), Funciones.Param("@cod_producto", txtCodigoProd.Text, SqlDbType.NChar)).Rows(0).Item(3), 4)

    End Sub


    Private Sub CkDescuento()

        If txtPorcentaje.Checked = True Then

            txtDescuento.Text = String.Empty
            txtDescuento.Enabled = False
            txtPorcenDescuento.Enabled = True

        Else

            txtDescuento.Text = String.Empty
            txtDescuento.Enabled = True
            txtPorcenDescuento.Enabled = False
            txtPorcenDescuento.Text = String.Empty

        End If

    End Sub

    Private Sub validar_descuento_documento()

        Dim decuento As Double = ChangeToDouble(txtPorcenDescuento.Text)

        If decuento > 99.99 Then txtPorcenDescuento.Text = String.Empty

    End Sub

    Private Sub validar_descuento_no_mayor()

        If ChangeToDouble(txtDescuento.Text) > ChangeToDouble(total_cordoba.Text) Then txtDescuento.Text = String.Empty

        If (ChangeToDouble(txtDescuento.Text) / Tasa) > ChangeToDouble(total_dolar.Text) Then txtDescuento.Text = String.Empty

    End Sub


End Class


