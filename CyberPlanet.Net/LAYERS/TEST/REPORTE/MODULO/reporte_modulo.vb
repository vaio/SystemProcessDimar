﻿Imports Microsoft.Reporting.WinForms
Imports System
Imports System.Drawing.Printing

Public NotInheritable Class reporte_modulo

    Private Shared sucursal As Integer
    Private Shared numero_de_documento As String

    Public Class compra_foranea

        Implements IReporte

        Private curr_compra As New compra

        Private compra_foranea_detalle As New List(Of BuilderDetalleCompra)

        Private Structure proveedor_datos

            Public nombre As String
            Public numero_de_factura As String

        End Structure

        Private Structure iva_impuesto

            Public iva_poliza As Double
            Public iva_agente_aduanero As Double
            Public iva_almacen As Double

        End Structure

        Public Sub New(sucursal_ As Integer, numero_de_documento_ As String)

            sucursal = sucursal_
            numero_de_documento = numero_de_documento_

        End Sub

        Private Sub get_datos() Implements IReporte.obtener_datos

            curr_compra = New compra(sucursal, numero_de_documento).get_compra

            For Each x As DataRow In curr_compra.get_detalle.Rows

                Dim curr_detalle = New BuilderDetalleCompra

                With curr_detalle

                    .Producto = x.Item(4)
                    .Recibidas = x.Item(6)
                    .Medidas = x.Item(5)
                    .Costo_Fob = x.Item(7)
                    .Flete_seguro = x.Item(10)
                    .Almacen = x.Item(11)
                    .Total_CIF_dolar = x.Item(12)
                    .Total_CIF_cordoba = x.Item(13)
                    .Total_gastos_intro_cordoba = x.Item(14)
                    .Impuesto_poliza_cordoba = x.Item(15)
                    .DAI_cordoba = x.Item(16)
                    .Total_cordoba = x.Item(17)
                    .C_unitario_cordoba = x.Item(18)

                End With

                compra_foranea_detalle.Add(curr_detalle)

            Next

        End Sub
        Private Function movimientos() As String

            movimientos = String.Empty

            Dim datos_de_compra = data_compra_foranea.get_reporte_master_movimiento(sucursal, numero_de_documento)

            Dim cant_total_rows = datos_de_compra.Rows.Count

            Dim cant_curr_rows = 0

            For Each movimiento As DataRow In datos_de_compra.Rows

                cant_curr_rows += 1

                If Not cant_curr_rows = cant_total_rows Then movimientos += String.Format("{0} {1} / ", movimiento.Item(6), movimiento.Item(7)) Else movimientos += String.Format("{0} {1} ", movimiento.Item(6), movimiento.Item(7))

            Next

        End Function
        Private Function concatenar_proveedor() As proveedor_datos

            concatenar_proveedor.nombre = String.Empty : concatenar_proveedor.numero_de_factura = String.Empty

            Dim proveedores = data_compra_foranea.get_proveedores(Funciones.Param("@numero_de_sucursal", nSucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.VarChar))

            Dim cant_total_rows = proveedores.Rows.Count

            Dim cant_curr_rows = 0

            For Each p As DataRow In proveedores.Rows

                cant_curr_rows += 1

                If Not cant_curr_rows = cant_total_rows Then concatenar_proveedor.nombre += p.Item(3).ToString + " / " Else concatenar_proveedor.nombre += p.Item(3).ToString

                If Not cant_curr_rows = cant_total_rows Then concatenar_proveedor.numero_de_factura += p.Item(4).ToString + " / " Else concatenar_proveedor.numero_de_factura += p.Item(4).ToString


            Next

        End Function

        Private Function get_iva_impuesto() As iva_impuesto

            get_iva_impuesto.iva_agente_aduanero = 0 : get_iva_impuesto.iva_almacen = 0 : get_iva_impuesto.iva_poliza = 0

            Dim curr_impuestos = data_compra_foranea.get_impuestos(Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.VarChar))

            For Each impuesto As impuesto In curr_impuestos.Values

                Select Case impuesto.get_tipo_documento

                    Case 2021

                        get_iva_impuesto.iva_poliza = impuesto.get_iva

                    Case 1021

                        get_iva_impuesto.iva_agente_aduanero = impuesto.get_iva

                    Case 1019

                        get_iva_impuesto.iva_almacen = impuesto.get_iva

                End Select

            Next

        End Function

        Public Sub visualizar() Implements IReporte.visualizar

            get_datos()

            Dim proveedor_datos = concatenar_proveedor()
            Dim iva_impuestos = get_iva_impuesto()

            Dim Previsualizacion As New DevxVistaPrevia

            Dim Reporte As New dev_reporte_compra_foranea(compra_foranea_detalle,
                                           String.Format("{0} / {1}", curr_compra.get_numero_de_documento_poliza, curr_compra.get_numero_de_documento_consecutivo),
                                           curr_compra.get_fecha_de_documento,
                                           proveedor_datos.nombre,
                                           proveedor_datos.numero_de_factura,
                                           movimientos(),
                                           iva_impuestos.iva_poliza,
                                           iva_impuestos.iva_agente_aduanero,
                                           iva_impuestos.iva_almacen,
                                           curr_compra.get_tasa_cambio_de_documento
                                           )

            Previsualizacion.DocumentViewer1.DocumentSource = Reporte

            Reporte.CreateDocument()

            Previsualizacion.Show()

        End Sub

    End Class

    Public Class BuilderDetalleCompra

        Public Property Producto As String
        Public Property Recibidas As Double
        Public Property Medidas As String
        Public Property Costo_Fob As String
        Public Property Flete_seguro As Double
        Public Property Almacen As Double
        Public Property Total_CIF_dolar As Double
        Public Property Total_CIF_cordoba As Double
        Public Property Total_gastos_intro_cordoba As Double
        Public Property Impuesto_poliza_cordoba As Double
        Public Property DAI_cordoba As Double
        Public Property Total_cordoba As Double
        Public Property C_unitario_cordoba As Double

    End Class

End Class
