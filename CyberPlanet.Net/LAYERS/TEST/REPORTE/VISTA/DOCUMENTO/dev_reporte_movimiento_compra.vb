﻿Public Class dev_reporte_movimiento_compra

    Public Sub New(_p_SUCURSAL As String,
                   _p_BODEGA As String,
                   _p_NUMERO_MOVIMIENTO As String,
                   _p_LOCAL_FORANEA As String,
                   _p_PROVEEDOR As String,
                   _p_TIPO_DE_COMPRA As String,
                   _p_NUMERO_FACTURA As String,
                   _p_OBSERCACIONES As String,
                   _p_DETALLE As reporte_moviemiento_almacen.detalle_movimiento)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().


        P_SUCURSAL.Value = _p_SUCURSAL
        P_BODEGA.Value = _p_BODEGA
        P_NUMERO_MOVIMIENTO.Value = _p_NUMERO_MOVIMIENTO
        P_LOCAL_FORANEA.Value = _p_LOCAL_FORANEA
        P_PROVEEDOR.Value = _p_PROVEEDOR
        P_TIPO_DE_COMPRA.Value = _p_TIPO_DE_COMPRA
        P_NUMERO_FACTURA.Value = _p_NUMERO_FACTURA
        P_OBSERVACIONES.Value = _p_OBSERCACIONES

        ObjectDataSource1.DataSource = _p_DETALLE

        For Each X As DevExpress.XtraReports.Parameters.Parameter In Me.Parameters

            X.Visible = False

        Next


    End Sub

End Class