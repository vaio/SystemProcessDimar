﻿Public Class dev_reporte_compra_foranea

    Public Sub New(detalle As List(Of reporte_modulo.BuilderDetalleCompra),
                   P_NUMERO_ As String,
                   P_FECHA_FACTURA_ As Date,
                   P_PROVEEDOR_ As String,
                   P_FACTURA_ As String,
                   P_ENTRADA_ As String,
                   P_IVA_POLIZA_ As Double,
                   P_IVA_AGENTE_ As Double,
                   P_IVA_ALMACEN_ As Double,
                   P_TASA_DE_CAMBIO_ As Decimal)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().


        ObjectDataSource1.DataSource = detalle

        P_NUMERO.Value = P_NUMERO_
        P_FECHA_FACTURA.Value = P_FECHA_FACTURA_
        P_PROVEEDOR.Value = P_PROVEEDOR_
        P_FACTURA.Value = P_FACTURA_
        P_ENTRADA.Value = P_ENTRADA_
        P_IVA_POLIZA.Value = P_IVA_POLIZA_
        P_IVA_AGENTE_ADUANERO.Value = P_IVA_AGENTE_
        P_IVA_ALMACEN.Value = P_IVA_ALMACEN_
        P_IVA_TOTAL.Value = P_IVA_POLIZA_ + P_IVA_AGENTE_ + P_IVA_ALMACEN_
        P_TASA_DE_CAMBIO.Value = P_TASA_DE_CAMBIO_

        For Each X As DevExpress.XtraReports.Parameters.Parameter In Me.Parameters

            X.Visible = False

        Next

    End Sub

    '(compra_foranea_detalle,
    '                                       String.Format("{0} / {1}", curr_compra.get_numero_de_documento_poliza, curr_compra.get_numero_de_documento_consecutivo),
    '                                       curr_compra.get_fecha_de_documento,
    '                                       proveedor_datos.nombre,
    '                                       proveedor_datos.numero_de_factura,
    '                                       movimientos(),
    '                                       iva_impuestos.iva_poliza,
    '                                       iva_impuestos.iva_agente_aduanero,
    '                                       iva_impuestos.iva_almacen,
    '                                       curr_compra.get_tasa_cambio_de_documento
    '                                       )

End Class