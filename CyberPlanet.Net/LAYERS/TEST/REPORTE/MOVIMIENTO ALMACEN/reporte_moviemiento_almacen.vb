﻿Imports Microsoft.Reporting.WinForms

Public NotInheritable Class reporte_moviemiento_almacen

    Private Structure datos_requerido

        Public sucursal As Integer
        Public numero_documento As String

    End Structure

    Public Class compra_foranea

        Implements IReporte

        Private var As New datos_requerido

        Private info As New List(Of master_movimiento_compra)

        Public Sub New(sucursal_ As Integer, numero_documento_ As String)

            var.sucursal = sucursal_
            var.numero_documento = numero_documento_

        End Sub

        Private Sub obtener_datos() Implements IReporte.obtener_datos

            Dim datos_de_compra = data_compra_foranea.get_reporte_master_movimiento(var.sucursal, var.numero_documento)

            For Each curr_movimiento As DataRow In datos_de_compra.Rows

                Dim movimiento As New master_movimiento_compra With {.sucursal = curr_movimiento.Item(0),
                                                                     .tipo_compra = curr_movimiento.Item(1),
                                                                     .nombre_proveedor = String.Format("{0} liquidacion # {1}", curr_movimiento.Item(2), curr_movimiento.Item(3)),
                                                                     .bodega_destino = curr_movimiento.Item(4),
                                                                     .id_bodega_destino = curr_movimiento.Item(5),
                                                                     .numero_movimiento = curr_movimiento.Item(6),
                                                                     .fecha_movimiento = curr_movimiento.Item(7),
                                                                     .observacion = curr_movimiento.Item(8)}

                Dim curr_detalle_from_db = data_compra_local.get_movimiento_detalle_reporte(Funciones.Param("@sucursal", var.sucursal, SqlDbType.Int),
                                                                                          Funciones.Param("@tipo_movimiento", 4, SqlDbType.Int),
                                                                                          Funciones.Param("@numero_bodega", movimiento.id_bodega_destino, SqlDbType.Int),
                                                                                          Funciones.Param("@numero_movimiento", movimiento.numero_movimiento, SqlDbType.NChar))

                With curr_detalle_from_db.Rows(0)

                    movimiento.detalle_tabla.codigo = .Item(0).ToString
                    movimiento.detalle_tabla.producto = .Item(1).ToString
                    movimiento.detalle_tabla.udm = .Item(2).ToString
                    movimiento.detalle_tabla.cantidad = .Item(3).ToString
                    movimiento.detalle_tabla.costo = .Item(4).ToString

                End With


                info.Add(movimiento)

            Next

        End Sub

        Public Sub visualizar() Implements IReporte.visualizar

            obtener_datos()

            For Each movimiento As master_movimiento_compra In info

                Dim Previsualizacion As New DevxVistaPrevia

                Dim Reporte As New dev_reporte_movimiento_compra(SucursalName,
                                                            "ENTRADA A LA BODEGA " + movimiento.bodega_destino,
                                                            String.Format("{0} - {1}", movimiento.numero_movimiento, movimiento.fecha_movimiento.ToShortDateString),
                                                            "COMPRA FORANEA",
                                                            movimiento.nombre_proveedor.ToUpper,
                                                            "COMPRA DE " + movimiento.tipo_compra.ToUpper,
                                                            String.Format("Factura No. {0} - {1}", movimiento.numero_factura.ToUpper, movimiento.fecha_recepcion.ToShortDateString),
                                                            movimiento.observacion,
                                                            movimiento.detalle_tabla)

                Previsualizacion.DocumentViewer1.DocumentSource = Reporte

                Reporte.CreateDocument()

                Previsualizacion.Show()


                'Dim var_reporte As New var_reporte With {.vista = New vista_previa, .rds = New ReportDataSource, .lr = New LocalReport}

                'var_reporte.vista.WindowState = FormWindowState.Maximized

                'var_reporte.vista.Reporte.Reset()
                'var_reporte.lr = var_reporte.vista.Reporte.LocalReport

                'var_reporte.lr.ReportEmbeddedResource = "SIGMA.reporte_compra_movimiento.rdlc"

                'var_reporte.lr.SetParameters(New ReportParameter("empresa", SucursalName))
                'var_reporte.lr.SetParameters(New ReportParameter("bodega", "ENTRADA A LA BODEGA " + movimiento.bodega_destino))
                'var_reporte.lr.SetParameters(New ReportParameter("tipo_compra", SucursalName))
                'var_reporte.lr.SetParameters(New ReportParameter("numero_movimiento", movimiento.numero_movimiento))
                'var_reporte.lr.SetParameters(New ReportParameter("fecha_movimiento", movimiento.fecha_movimiento.ToShortDateString))
                'var_reporte.lr.SetParameters(New ReportParameter("proveedor", movimiento.nombre_proveedor.ToUpper))
                'var_reporte.lr.SetParameters(New ReportParameter("observacion", movimiento.observacion))
                'var_reporte.lr.SetParameters(New ReportParameter("local_foranea", "COMPRA FORANEA"))

                'var_reporte.rds.Value = movimiento.detalle_tabla

                'var_reporte.rds.Name = "movimiento_compra_local"

                'var_reporte.lr.DataSources.Clear()

                'var_reporte.lr.DataSources.Add(var_reporte.rds)

                'var_reporte.vista.Reporte.ZoomMode = ZoomMode.PageWidth

                'var_reporte.vista.ShowDialog()

            Next

        End Sub

    End Class

    Public Class compra_local

        Implements IReporte

        Private var As New datos_requerido

        Private info As New List(Of master_movimiento_compra)

        Public Sub New(sucursal_ As Integer, numero_documento_ As String)

            var.sucursal = sucursal_
            var.numero_documento = numero_documento_

        End Sub

        Private Sub get_datos() Implements IReporte.obtener_datos

            Dim datos_de_compra = data_compra_local.get_master_reporte(Funciones.Param("@sucursal", var.sucursal, SqlDbType.Int), Funciones.Param("@numero_documento_compra", var.numero_documento, SqlDbType.NChar))

            For Each curr_movimiento As DataRow In datos_de_compra.Rows

                Dim movimiento As New master_movimiento_compra With {.sucursal = curr_movimiento.Item(0),
                                                                     .tipo_compra = curr_movimiento.Item(1),
                                                                     .numero_factura = curr_movimiento.Item(2),
                                                                     .fecha_recepcion = curr_movimiento.Item(3),
                                                                     .nombre_proveedor = curr_movimiento.Item(4),
                                                                     .bodega_destino = curr_movimiento.Item(5),
                                                                     .id_bodega_destino = curr_movimiento.Item(6),
                                                                     .numero_movimiento = curr_movimiento.Item(7),
                                                                     .fecha_movimiento = curr_movimiento.Item(8),
                                                                     .observacion = curr_movimiento.Item(9)}

                Dim curr_detalle_from_db = data_compra_local.get_movimiento_detalle_reporte(Funciones.Param("@sucursal", var.sucursal, SqlDbType.Int),
                                                                                          Funciones.Param("@tipo_movimiento", 4, SqlDbType.Int),
                                                                                          Funciones.Param("@numero_bodega", movimiento.id_bodega_destino, SqlDbType.Int),
                                                                                          Funciones.Param("@numero_movimiento", movimiento.numero_movimiento, SqlDbType.NChar))

                movimiento.detalle_tabla = New detalle_movimiento

                With curr_detalle_from_db.Rows(0)

                    movimiento.detalle_tabla.codigo = .Item(0).ToString
                    movimiento.detalle_tabla.producto = .Item(1).ToString
                    movimiento.detalle_tabla.udm = .Item(2).ToString
                    movimiento.detalle_tabla.cantidad = .Item(3)
                    movimiento.detalle_tabla.costo = .Item(4)

                End With

                info.Add(movimiento)

            Next

        End Sub

        Public Sub visualizar() Implements IReporte.visualizar

            get_datos()

            For Each movimiento As master_movimiento_compra In info

                Dim Previsualizacion As New DevxVistaPrevia

                Dim Reporte As New dev_reporte_movimiento_compra(SucursalName,
                                                            "ENTRADA A LA BODEGA " + movimiento.bodega_destino,
                                                            String.Format("{0} - {1}", movimiento.numero_movimiento, movimiento.fecha_movimiento.ToShortDateString),
                                                            "COMPRA LOCAL",
                                                            movimiento.nombre_proveedor.ToUpper,
                                                            "COMPRA DE " + movimiento.tipo_compra.ToUpper,
                                                            String.Format("Factura No. {0} - {1}", movimiento.numero_factura.ToUpper, movimiento.fecha_recepcion.ToShortDateString),
                                                            movimiento.observacion,
                                                            movimiento.detalle_tabla)

                Previsualizacion.DocumentViewer1.DocumentSource = Reporte

                Reporte.CreateDocument()

                Previsualizacion.Show()

            Next

        End Sub

    End Class

    Private Structure master_movimiento_compra

        Public sucursal As String
        Public tipo_compra As String
        Public numero_factura As String
        Public fecha_recepcion As Date
        Public nombre_proveedor As String
        Public bodega_destino As String
        Public id_bodega_destino As Integer
        Public numero_movimiento As String
        Public fecha_movimiento As Date
        Public observacion As String

        Public detalle_tabla As detalle_movimiento

    End Structure

    Public Class detalle_movimiento

        Public Property codigo As String
        Public Property producto As String
        Public Property udm As String
        Public Property cantidad As Double
        Public Property costo As Double

    End Class

    'Public NotInheritable Class almacen_detalle

    '    Private id_ As String
    '    Private producto_ As String
    '    Private medida_ As String
    '    Private cantidad_ As Decimal
    '    Private costo_ As Decimal

    '    Public ReadOnly Property id As Integer
    '        Get
    '            Return Me.id_
    '        End Get
    '    End Property
    '    Public ReadOnly Property producto As String
    '        Get
    '            Return Me.producto_
    '        End Get
    '    End Property
    '    Public ReadOnly Property medida As String
    '        Get
    '            Return Me.medida_
    '        End Get
    '    End Property
    '    Public ReadOnly Property cantidad As Decimal
    '        Get
    '            Return Me.cantidad_
    '        End Get
    '    End Property
    '    Public ReadOnly Property valor As Decimal
    '        Get
    '            Return (Me.cantidad_ * Me.costo_)
    '        End Get
    '    End Property

    '    Public Sub New(DATA As DataRow)

    '        With DATA

    '            Me.id_ = .Item(0)
    '            Me.producto_ = .Item(1)
    '            Me.medida_ = .Item(2)
    '            Me.cantidad_ = .Item(3)
    '            Me.costo_ = .Item(4)

    '        End With

    '    End Sub

    'End Class

End Class
