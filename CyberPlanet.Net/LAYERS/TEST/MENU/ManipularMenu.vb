﻿Public NotInheritable Class ManipularMenu

    Private Sub New()

    End Sub

    Public Shared Function modificar(ByRef MainMenu As IMenu) As Integer

        Return MainMenu.modificar()

    End Function
    Public Shared Function buscar(ByRef MainMenu As IMenu) As Integer

        Return MainMenu.historial()

    End Function

    Public Shared Function guardar(ByRef MainMenu As IMenu) As Integer

        Return MainMenu.guardar()

    End Function

    Public Shared Function cancelar(ByRef MainMenu As IMenu) As Integer

        Return MainMenu.cancelar()

    End Function

End Class
