﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NUEVO_MENU
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.contendor = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.Logistica = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.menu_compra_foranea = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_compra_local = New DevExpress.XtraBars.BarButtonItem()
        Me.controles_de_menu = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.menu_grupo_botones = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.menu_nuevo = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_eliminar = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_modificar = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_cancelar = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_guardar = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_buscar = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_imprimir = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_salir = New DevExpress.XtraBars.BarButtonItem()
        Me.menu_principal = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btn_bodega = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_marca = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_linea = New DevExpress.XtraBars.BarButtonItem()
        Me.Catalogos = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.btn_modelo = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.contendor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.menu_principal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'contendor
        '
        Me.contendor.MdiParent = Me
        '
        'Logistica
        '
        Me.Logistica.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2})
        Me.Logistica.Name = "Logistica"
        Me.Logistica.Text = "Logistica"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.menu_compra_foranea)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.menu_compra_local)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "RibbonPageGroup2"
        '
        'menu_compra_foranea
        '
        Me.menu_compra_foranea.Caption = "Compra foranea"
        Me.menu_compra_foranea.Id = 2
        Me.menu_compra_foranea.Name = "menu_compra_foranea"
        '
        'menu_compra_local
        '
        Me.menu_compra_local.Caption = "Compra locales"
        Me.menu_compra_local.Id = 3
        Me.menu_compra_local.Name = "menu_compra_local"
        '
        'controles_de_menu
        '
        Me.controles_de_menu.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.menu_grupo_botones})
        Me.controles_de_menu.Name = "controles_de_menu"
        Me.controles_de_menu.Text = "Menu"
        '
        'menu_grupo_botones
        '
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_nuevo)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_eliminar)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_modificar)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_cancelar)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_guardar)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_buscar)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_imprimir)
        Me.menu_grupo_botones.ItemLinks.Add(Me.menu_salir)
        Me.menu_grupo_botones.Name = "menu_grupo_botones"
        '
        'menu_nuevo
        '
        Me.menu_nuevo.Caption = "Nuevo"
        Me.menu_nuevo.Id = 10
        Me.menu_nuevo.Name = "menu_nuevo"
        '
        'menu_eliminar
        '
        Me.menu_eliminar.Caption = "Eliminar"
        Me.menu_eliminar.Id = 11
        Me.menu_eliminar.Name = "menu_eliminar"
        '
        'menu_modificar
        '
        Me.menu_modificar.Caption = "Modificar"
        Me.menu_modificar.Id = 13
        Me.menu_modificar.Name = "menu_modificar"
        '
        'menu_cancelar
        '
        Me.menu_cancelar.Caption = "Cancelar"
        Me.menu_cancelar.Id = 14
        Me.menu_cancelar.Name = "menu_cancelar"
        '
        'menu_guardar
        '
        Me.menu_guardar.Caption = "Guardar"
        Me.menu_guardar.Id = 12
        Me.menu_guardar.Name = "menu_guardar"
        '
        'menu_buscar
        '
        Me.menu_buscar.Caption = "Buscar"
        Me.menu_buscar.Id = 15
        Me.menu_buscar.Name = "menu_buscar"
        '
        'menu_imprimir
        '
        Me.menu_imprimir.Caption = "Imprimir"
        Me.menu_imprimir.Id = 16
        Me.menu_imprimir.Name = "menu_imprimir"
        '
        'menu_salir
        '
        Me.menu_salir.Caption = "Salir"
        Me.menu_salir.Id = 17
        Me.menu_salir.Name = "menu_salir"
        '
        'menu_principal
        '
        Me.menu_principal.ExpandCollapseItem.Id = 0
        Me.menu_principal.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.menu_principal.ExpandCollapseItem, Me.menu_compra_foranea, Me.menu_compra_local, Me.menu_nuevo, Me.menu_eliminar, Me.menu_guardar, Me.menu_modificar, Me.menu_cancelar, Me.menu_buscar, Me.menu_imprimir, Me.menu_salir, Me.btn_bodega, Me.btn_marca, Me.btn_linea, Me.btn_modelo})
        Me.menu_principal.Location = New System.Drawing.Point(0, 0)
        Me.menu_principal.MaxItemId = 22
        Me.menu_principal.Name = "menu_principal"
        Me.menu_principal.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.controles_de_menu, Me.Logistica, Me.Catalogos})
        Me.menu_principal.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2007
        Me.menu_principal.Size = New System.Drawing.Size(977, 176)
        '
        'btn_bodega
        '
        Me.btn_bodega.Caption = "Bodegas"
        Me.btn_bodega.Id = 18
        Me.btn_bodega.Name = "btn_bodega"
        '
        'btn_marca
        '
        Me.btn_marca.Caption = "Marcas"
        Me.btn_marca.Id = 19
        Me.btn_marca.Name = "btn_marca"
        '
        'btn_linea
        '
        Me.btn_linea.Caption = "Lineas"
        Me.btn_linea.Id = 20
        Me.btn_linea.Name = "btn_linea"
        '
        'Catalogos
        '
        Me.Catalogos.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.Catalogos.Name = "Catalogos"
        Me.Catalogos.Text = "Catalogos"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btn_bodega)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btn_marca)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btn_linea)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btn_modelo)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "RibbonPageGroup1"
        '
        'btn_modelo
        '
        Me.btn_modelo.Caption = "Modelos"
        Me.btn_modelo.Id = 21
        Me.btn_modelo.Name = "btn_modelo"
        '
        'NUEVO_MENU
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(977, 577)
        Me.Controls.Add(Me.menu_principal)
        Me.IsMdiContainer = True
        Me.Name = "NUEVO_MENU"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SIGMA Vaio estuvo aqui!"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.contendor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.menu_principal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents contendor As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents menu_principal As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents menu_compra_foranea As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_compra_local As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_nuevo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_eliminar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_guardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_modificar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_cancelar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_buscar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents menu_imprimir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents controles_de_menu As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents menu_grupo_botones As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents Logistica As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents menu_salir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Catalogos As RibbonPage
    Friend WithEvents btn_bodega As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup1 As RibbonPageGroup
    Friend WithEvents btn_marca As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_linea As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_modelo As DevExpress.XtraBars.BarButtonItem
End Class
