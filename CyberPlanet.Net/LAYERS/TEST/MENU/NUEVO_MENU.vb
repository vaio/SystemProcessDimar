﻿Imports SIGMA

Public Class NUEVO_MENU

    Implements IMenu

    Private ventanas As New Dictionary(Of String, gestor_ventana)

    Private Class gestor_ventana

        Private ventana_ As IMenu

        Private var_menu_botones As New puerta_logica_menu

        Public ReadOnly Property var_menu_estado As puerta_logica_menu
            Get
                Return var_menu_botones
            End Get
        End Property

        Protected Friend Structure puerta_logica_menu

            Public nuevo_ As Boolean
            Public guardar_ As Boolean
            Public modificar_ As Boolean
            Public eliminar_ As Boolean
            Public cancelar_ As Boolean
            Public imprimir_ As Boolean
            Public reporte_ As Boolean
            Public buscar_ As Boolean
            Public salir_ As Boolean

        End Structure


        Public Sub New(ventana As Form)

            Me.ventana_ = ventana

            set_status_new_button()

        End Sub

        Public ReadOnly Property get_ventana
            Get
                Return ventana_
            End Get
        End Property

        Public Sub nuevo()

            If Not ventana_.nuevo() = 0 Then Exit Sub

            var_menu_botones.nuevo_ = False
            var_menu_botones.guardar_ = True
            var_menu_botones.modificar_ = False
            var_menu_botones.eliminar_ = False
            var_menu_botones.cancelar_ = True
            var_menu_botones.imprimir_ = False
            var_menu_botones.reporte_ = False
            var_menu_botones.buscar_ = False
            var_menu_botones.salir_ = True

        End Sub

        Public Sub guardar()

            If Not ventana_.guardar() = 0 Then Exit Sub

            var_menu_botones.nuevo_ = True
            var_menu_botones.guardar_ = False
            var_menu_botones.modificar_ = True
            var_menu_botones.eliminar_ = True
            var_menu_botones.cancelar_ = False
            var_menu_botones.imprimir_ = False
            var_menu_botones.reporte_ = False
            var_menu_botones.buscar_ = True
            var_menu_botones.salir_ = True

        End Sub

        Public Sub modificar()

            If Not ventana_.modificar() = 0 Then Exit Sub

            var_menu_botones.nuevo_ = True
            var_menu_botones.guardar_ = True
            var_menu_botones.modificar_ = False
            var_menu_botones.eliminar_ = False
            var_menu_botones.cancelar_ = True
            var_menu_botones.imprimir_ = False
            var_menu_botones.reporte_ = False
            var_menu_botones.buscar_ = False
            var_menu_botones.salir_ = True

        End Sub

        Public Sub eliminar()

            ventana_.eliminar()

        End Sub
        Public Sub cancelar()

            If ventana_.cancelar() = 0 Then set_status_new_button()

        End Sub
        Public Sub imprimir()

            ventana_.imprimir()

        End Sub
        Public Sub historial()

            If Not ventana_.historial() = 0 Then Exit Sub

            set_status_find_button()

        End Sub
        Public Sub salir()

            ventana_.salir()

        End Sub

        Private Sub set_status_new_button()

            var_menu_botones.nuevo_ = True
            var_menu_botones.guardar_ = False
            var_menu_botones.modificar_ = False
            var_menu_botones.eliminar_ = False
            var_menu_botones.cancelar_ = False
            var_menu_botones.imprimir_ = False
            var_menu_botones.reporte_ = False
            var_menu_botones.buscar_ = True
            var_menu_botones.salir_ = True


        End Sub

        Private Sub set_status_find_button()

            var_menu_botones.nuevo_ = True
            var_menu_botones.guardar_ = False
            var_menu_botones.modificar_ = True
            var_menu_botones.eliminar_ = True
            var_menu_botones.cancelar_ = False
            var_menu_botones.imprimir_ = True
            var_menu_botones.reporte_ = True
            var_menu_botones.buscar_ = True
            var_menu_botones.salir_ = True

        End Sub

    End Class

    Private Sub start() Handles Me.Load

        AddHandler menu_nuevo.ItemClick, AddressOf nuevo
        AddHandler menu_modificar.ItemClick, AddressOf modificar
        AddHandler menu_eliminar.ItemClick, AddressOf eliminar
        AddHandler menu_guardar.ItemClick, AddressOf guardar
        AddHandler menu_cancelar.ItemClick, AddressOf cancelar
        AddHandler menu_buscar.ItemClick, AddressOf historial
        AddHandler menu_salir.ItemClick, AddressOf salir
        AddHandler menu_imprimir.ItemClick, AddressOf imprimir

        AddHandler menu_compra_foranea.ItemClick, Sub(sender, e) open_ventana(New compra_foranea)
        AddHandler menu_compra_local.ItemClick, Sub(sender, e) open_ventana(New compra_local)
        AddHandler btn_bodega.ItemClick, Sub(sender, e) open_ventana(New bodega)
        AddHandler btn_marca.ItemClick, Sub(sender, e) open_ventana(New marca)
        AddHandler btn_linea.ItemClick, Sub(sender, e) open_ventana(New linea)
        AddHandler btn_modelo.ItemClick, Sub(sender, e) open_ventana(New modelo)

        AddHandler contendor.SelectedPageChanged, AddressOf get_active_tab

        set_menu_estado_desactivado()

    End Sub
    Private Sub find_ventana(ByVal Ventana As Form)

        For Each v In MdiChildren

            If v.Text = Ventana.Text Then

                contendor.Pages(v).MdiChild.Activate()

                Exit Sub

            End If

        Next

    End Sub
    Private Sub open_ventana(ByVal Ventana As Form)

        If ventanas.ContainsKey(Ventana.Text) Then

            find_ventana(Ventana)

        Else

            ventanas.Add(Ventana.Text, New gestor_ventana(Ventana))

            Dim curr_ventana = ventanas.Item(Ventana.Text).get_ventana

            curr_ventana.MdiParent = Me

            curr_ventana.Show()

        End If

    End Sub
    Private Sub set_menu_estado(estado As gestor_ventana.puerta_logica_menu)

        menu_nuevo.Enabled = estado.nuevo_
        menu_modificar.Enabled = estado.modificar_
        menu_eliminar.Enabled = estado.eliminar_
        menu_guardar.Enabled = estado.guardar_
        menu_cancelar.Enabled = estado.cancelar_
        menu_buscar.Enabled = estado.buscar_
        menu_imprimir.Enabled = estado.imprimir_
        menu_salir.Enabled = estado.salir_

    End Sub

    Private Sub get_active_tab()

        If Not IsNothing(contendor.SelectedPage) Then

            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

        Else

            set_menu_estado_desactivado()

        End If

    End Sub

    Private Sub set_menu_estado_desactivado()

        menu_nuevo.Enabled = False
        menu_modificar.Enabled = False
        menu_eliminar.Enabled = False
        menu_guardar.Enabled = False
        menu_cancelar.Enabled = False
        menu_buscar.Enabled = False
        menu_imprimir.Enabled = False
        menu_salir.Enabled = False

    End Sub
    Public Function nuevo() As Integer Implements IMenu.nuevo

        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).nuevo()
            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

    Public Function guardar() As Integer Implements IMenu.guardar


        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).guardar()
            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

    Public Function eliminar() As Integer Implements IMenu.eliminar


        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).eliminar()
            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

    Public Function cancelar() As Integer Implements IMenu.cancelar

        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).cancelar()
            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

    Public Function modificar() As Integer Implements IMenu.modificar

        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).modificar()
            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

    Public Function historial() As Integer Implements IMenu.historial


        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).historial()
            set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

    Public Function salir() As Integer Implements IMenu.salir

        If MdiChildren.Length = 0 Then Return 1

        Dim key = contendor.SelectedPage.Text

        ventanas.Item(key).salir()

        ventanas.Remove(key)

        Return 0

    End Function
    Public Function imprimir() As Integer Implements IMenu.imprimir

        If Not MdiChildren.Length = 0 Then

            ventanas.Item(contendor.SelectedPage.Text).imprimir() ': set_menu_estado(ventanas.Item(contendor.SelectedPage.Text).var_menu_estado)

            Return 0

        End If

        Return 1

    End Function

End Class