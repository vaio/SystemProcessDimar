﻿Public Class recibo

    Private id_sucursal As Integer
    Private id_cartera As Integer
    Private id_documento As String
    Private id_documento_tipo As Integer
    Private id_cliente As Integer
    Private id_colector As Integer

    Private id_comision_colector As Integer
    Private id_comision_coordinador As Integer
    Private id_comision_supervisor As Integer
    Private id_comision_oficina As Integer
    Private id_comision_jefe As Integer

    Private fecha As Date
    Private monto As Decimal

    Private facturas_afectadas As factura_afectadas_por_recibo

    Private IsReciboSaved_ As Boolean = False

    Public ReadOnly Property IsReciboSaved As Boolean
        Get
            Return Me.IsReciboSaved_
        End Get
    End Property

    Public Sub New()

        IsReciboSaved_ = False

    End Sub

    Public Sub JustSaveIt(DATOS As builderRecibo)

        id_sucursal = DATOS.id_sucursal
        id_cartera = DATOS.id_cartera
        id_documento = DATOS.id_documento
        id_documento_tipo = DATOS.id_documento_tipo
        id_cliente = DATOS.id_cliente
        id_colector = DATOS.id_colector

        fecha = DATOS.fecha
        monto = monto

        facturas_afectadas.AddRange(DATOS.facturas_afectadas)

        facturas_afectadas.aplicar_abono_en_facturas(Me)

        IsReciboSaved_ = True

    End Sub

    Public Sub JustBringItBackToMeNiggaxD()

        'this will return a builder then set data to recibo so after it will return a objet as recibo. 
        'taking account it's completely saved into database

    End Sub


    Private Class factura_afectadas_por_recibo

        Inherits ArrayList
        Public Sub aplicar_abono_en_facturas(ByRef Recibo As recibo)

            For Each factura As builderRecibo.stru_factura_afectada In Me

                'guardar aqui

            Next

        End Sub

    End Class

    Public Class builderRecibo 'it will be taken to save the recibo

        Public Property id_sucursal As Integer
        Public Property id_cartera As Integer
        Public Property id_documento As String
        Public Property id_documento_tipo As Integer
        Public Property id_cliente As Integer
        Public Property id_colector As Integer

        Public Property fecha As Date
        Public Property total_monto As Decimal

        Public Property facturas_afectadas As List(Of stru_factura_afectada)

        Public Structure stru_factura_afectada 'this is just a structure. it will hold the details from recibo 

            Public id_factura As Integer
            Public monto As Double

        End Structure

    End Class

    Public Class builderReciboDocumento 'its took from database to fill up recibo!

        Inherits builderRecibo

        Public Property id_comision_colector As Integer
        Public Property id_comision_coordinador As Integer
        Public Property id_comision_supervisor As Integer
        Public Property id_comision_oficina As Integer
        Public Property id_comision_jefe As Integer

    End Class

End Class
