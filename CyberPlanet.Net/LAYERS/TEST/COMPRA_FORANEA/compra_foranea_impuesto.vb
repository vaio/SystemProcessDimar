﻿Imports DevExpress.XtraEditors.Controls

Public Class compra_foranea_impuesto

    Private vNumericos As New Validar With {.CantDecimal = 4}

    Private Proveedores As New Dictionary(Of String, Proveedor)

    Private Proveedor_iva As Decimal

    Private sucursal As Integer

    Private numero_de_documento As String

    Private accion As Integer

    Private otros_impuestos As Decimal

    Private impuestos As New Dictionary(Of impuesto.key_identificador, impuesto)

    Dim key As New impuesto.key_identificador

    Private impuesto_actual As New impuesto

    Public Property set_impuestos As Dictionary(Of impuesto.key_identificador, impuesto)
        Get
            Return Me.impuestos
        End Get
        Set(value As Dictionary(Of impuesto.key_identificador, impuesto))
            Me.impuestos = value
        End Set
    End Property
    Public ReadOnly Property curr_impuesto As impuesto
        Get
            Return Me.impuesto_actual
        End Get
    End Property
    Public WriteOnly Property set_otros_impuestos As Decimal
        Set(value As Decimal)
            Me.otros_impuestos = value
        End Set
    End Property

    Public Sub New(sucursal As Integer, numero_de_documento As String, accion As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Me.sucursal = sucursal
        Me.numero_de_documento = numero_de_documento
        Me.accion = accion

        Me.txtNumRefe.Text = Me.numero_de_documento

        cmbTipDocumento.EditValue = Me.accion

        key = New impuesto.key_identificador With {.sucursal = sucursal, .numero_de_documento = numero_de_documento, .tipo_documento = accion}

        AddHandler Me.Load, AddressOf start


    End Sub

    Private Sub start()

        AddHandler cmdAceptar.Click, AddressOf Aplicar
        AddHandler cmdCancelar.Click, AddressOf cancelar
        AddHandler cmbProveedor.EditValueChanged, AddressOf set_valores_de_proveedores

        vNumericos.agregar_control(New List(Of Control)({txtIVA, txtIMI, txtIR, txtMultas, txtNeto, txtOtrosGas, txtOtrosImpuestos, txtSubTotal, txtTotalNeto})).ActivarEventosNumericos()

        get_proveedores()

        set_valores_de_proveedores()

        cargarTipoReferecia()

        cargarTipoDocumento()

        gestionar_accion()

    End Sub
    Private Sub calculo_poliza()

        txtTotalNeto.Text = FormatNumber(ChangeToDouble(txtIVA.Text), 4)

        txtNeto.Text = FormatNumber(ChangeToDouble(txtOtrosImpuestos.Text) + ChangeToDouble(txtTotalNeto.Text), 4)

    End Sub
    Private Sub calculo_agente_aduanero()

        Dim subtotal = ChangeToDouble(txtSubTotal.Text)

        Dim iva = (Proveedor_iva * subtotal)

        Dim otros_gastos = ChangeToDouble(txtOtrosGas.Text)

        txtIVA.Text = FormatNumber(iva, 4)

        txtTotalNeto.Text = FormatNumber(subtotal + iva, 4)

        txtNeto.Text = FormatNumber(ChangeToDouble(txtTotalNeto.Text) + otros_gastos, 4)

    End Sub
    Private Sub calculo_otros_gastos()

        txtNeto.Text = FormatNumber(ChangeToDouble(txtOtrosGas.Text))

    End Sub

    Private Sub gestionar_accion()

        get_impuesto_detalle()

       Dim Result = impuestos.ContainsKey(key)

        Select Case Me.accion

            Case 2021

                AddHandler txtIVA.TextChanged, AddressOf calculo_poliza

                txtIVA.Enabled = True

                If Result = False Then

                    txtOtrosImpuestos.Text = FormatNumber(otros_impuestos, 4)

                    calculo_poliza()

                Else

                    Dim _curr_impuesto_ = impuestos.Item(key)

                    set_datos_cabezera_impuestos(_curr_impuesto_)


                End If

            Case 1021

                AddHandler txtSubTotal.TextChanged, AddressOf calculo_agente_aduanero
                AddHandler txtOtrosGas.TextChanged, AddressOf calculo_agente_aduanero
                AddHandler cmbProveedor.EditValueChanged, AddressOf calculo_agente_aduanero

                txtSubTotal.Enabled = True
                txtOtrosGas.Enabled = True


                If Result = True Then

                    Dim _curr_impuesto_ = impuestos.Item(key)

                    set_datos_cabezera_impuestos(_curr_impuesto_)

                End If

            Case 1022

                AddHandler txtSubTotal.TextChanged, AddressOf calculo_agente_aduanero
                AddHandler cmbProveedor.EditValueChanged, AddressOf calculo_agente_aduanero

                txtSubTotal.Enabled = True

                If Result = True Then

                    Dim _curr_impuesto_ = impuestos.Item(key)

                    set_datos_cabezera_impuestos(_curr_impuesto_)

                End If

            Case 1019

                AddHandler txtSubTotal.TextChanged, AddressOf calculo_agente_aduanero
                AddHandler cmbProveedor.EditValueChanged, AddressOf calculo_agente_aduanero

                txtSubTotal.Enabled = True

                If Result = True Then

                    Dim _curr_impuesto_ = impuestos.Item(key)

                    set_datos_cabezera_impuestos(_curr_impuesto_)

                End If

            Case 2022

                AddHandler txtOtrosGas.TextChanged, AddressOf calculo_otros_gastos

                txtOtrosGas.Enabled = True

                If Result = True Then

                    Dim _curr_impuesto_ = impuestos.Item(key)

                    set_datos_cabezera_impuestos(_curr_impuesto_)

                End If


        End Select

    End Sub
    Private Sub set_datos_cabezera_impuestos(ByRef _curr_impuesto_ As impuesto)

        txtNumDoc.Text = _curr_impuesto_.get_numero_de_documento_digitado

        cmbTipDocumento.EditValue = _curr_impuesto_.get_tipo_documento

        deFechaDoc.Value = _curr_impuesto_.get_Fecha_documento_emitido

        deFechaVenc.Value = _curr_impuesto_.get_Fecha_documento_vencimiento

        txtNumRefe.Text = _curr_impuesto_.get_numero_de_referencia

        cmbReferencia.EditValue = _curr_impuesto_.get_tipo_de_referencia

        cmbProveedor.EditValue = _curr_impuesto_.get_proveedor

        txtSubTotal.Text = FormatNumber(_curr_impuesto_.get_sub_total, 4)

        txtIVA.Text = FormatNumber(_curr_impuesto_.get_iva, 4)

        txtTotalNeto.Text = FormatNumber(_curr_impuesto_.get_total_neto, 4)

        txtOtrosGas.Text = FormatNumber(_curr_impuesto_.get_otros_gastos, 4)

        txtOtrosImpuestos.Text = FormatNumber(_curr_impuesto_.get_otros_impuestos, 4)

        txtNeto.Text = FormatNumber(_curr_impuesto_.get_neto_a_pagar, 4)

    End Sub
    Private Sub get_impuesto_detalle()

        impuestos.Clear()

        impuestos = data_compra_foranea.get_impuestos(Funciones.Param("@sucursal", sucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento", numero_de_documento, SqlDbType.VarChar))

    End Sub

    Private Sub Aplicar()

        get_impuesto_detalle()

        Dim Result = impuestos.ContainsKey(key)

        If Result = False Then


            impuesto_actual = New impuesto(
                                                    sucursal,
                                                    numero_de_documento,
                                                    txtNumDoc.Text,
                                                    cmbTipDocumento.EditValue,
                                                    deFechaDoc.Value,
                                                    deFechaVenc.Value,
                                                    numero_de_documento,
                                                    cmbReferencia.EditValue,
                                                    cmbProveedor.EditValue,
                                                    ChangeToDouble(txtSubTotal.Text),
                                                    ChangeToDouble(txtIR.Text),
                                                    ChangeToDouble(txtIMI.Text),
                                                    ChangeToDouble(txtIVA.Text),
                                                    ChangeToDouble(txtTotalNeto.Text),
                                                    ChangeToDouble(txtOtrosGas.Text),
                                                    ChangeToDouble(txtMultas.Text),
                                                    ChangeToDouble(txtOtrosImpuestos.Text),
                                                    ChangeToDouble(txtNeto.Text)).gestion(1)

        Else

            With impuestos.Item(key)

                .get_sucursal = sucursal
                .get_numero_de_documento = numero_de_documento
                .get_numero_de_documento_digitado = txtNumDoc.Text
                .get_tipo_documento = cmbTipDocumento.EditValue
                .get_Fecha_documento_emitido = deFechaDoc.Value
                .get_Fecha_documento_vencimiento = deFechaVenc.Value
                .get_numero_de_referencia = numero_de_documento
                .get_tipo_de_referencia = cmbReferencia.EditValue
                .get_proveedor = cmbProveedor.EditValue
                .get_sub_total = ChangeToDouble(txtSubTotal.Text)
                .get_ir = ChangeToDouble(txtIR.Text)
                .get_imi = ChangeToDouble(txtIMI.Text)
                .get_iva = ChangeToDouble(txtIVA.Text)
                .get_total_neto = ChangeToDouble(txtTotalNeto.Text)
                .get_otros_gastos = ChangeToDouble(txtOtrosGas.Text)
                .get_multas_y_recargos = ChangeToDouble(txtMultas.Text)
                .get_otros_impuestos = ChangeToDouble(txtOtrosImpuestos.Text)
                .get_neto_a_pagar = ChangeToDouble(txtNeto.Text)

                .gestion(2)

            End With

            impuesto_actual = impuestos.Item(key)

        End If

        Me.Close()

    End Sub
    Private Sub get_proveedores()

        Proveedores = data_proveedor.get_datos_basicos_proveedores

        cmbProveedor.Properties.DataSource = Proveedores.Values

        cmbProveedor.Properties.DisplayMember = "get_Nombre"
        cmbProveedor.Properties.ValueMember = "get_ID"

        cmbProveedor.Properties.Columns.Add(New LookUpColumnInfo("get_ID", "No.", 20))

        cmbProveedor.Properties.Columns.Add(New LookUpColumnInfo("get_Nombre", "Proveedor", 100))

        cmbProveedor.Properties.Columns(0).Visible = False

        cmbProveedor.EditValue = "001"

        cmbProveedor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub
    Private Sub set_valores_de_proveedores()

        Dim Proveedor As New Proveedor

        Proveedor = Proveedores.Item(cmbProveedor.EditValue)

        CkIR.Checked = Proveedor.get_Is_IR

        CkIMI.Checked = Proveedor.get_Is_IMI

        txtPorcIR.Text = FormatNumber(Proveedor.get_Tasa_IR, 2)

        txtPorcIMI.Text = FormatNumber(Proveedor.get_Tasa_IMI, 2)

        Proveedor_iva = IIf(Proveedor.get_Tasa_IVA = 0, 0, Proveedor.get_Tasa_IVA / 100)


    End Sub

    Public Sub cargarTipoReferecia()

        cmbReferencia.Properties.DataSource = SQL("SELECT CatDet.IdCategoriaDetalle, CatDet.Estado FROM Categorias Cat INNER JOIN Categorias_Detalle CatDet ON Cat.IdCategoria = CatDet.IdCategoria where CatDet.IdCategoria = 6 and CatDet.Activo = 1 and Cat.Activo = 1", "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)

        cmbReferencia.Properties.DisplayMember = "Estado"

        cmbReferencia.Properties.ValueMember = "IdCategoriaDetalle"

        cmbReferencia.Properties.Columns.Add(New LookUpColumnInfo("IdCategoriaDetalle", "Tipo de referencia", 100))

        cmbReferencia.Properties.Columns.Add(New LookUpColumnInfo("Estado", "No.", 20))

        cmbReferencia.Properties.Columns(0).Visible = False

        cmbReferencia.EditValue = 20

        cmbReferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub


    Public Sub cargarTipoDocumento()

        cmbTipDocumento.Properties.DataSource = SQL("SELECT CatDet.IdCategoriaDetalle, CatDet.Estado FROM Categorias Cat INNER JOIN Categorias_Detalle CatDet ON Cat.IdCategoria = CatDet.IdCategoria where CatDet.IdCategoria = 7 and CatDet.Activo = 1 and Cat.Activo = 1", "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)

        cmbTipDocumento.Properties.DisplayMember = "Estado"
        cmbTipDocumento.Properties.ValueMember = "IdCategoriaDetalle"

        cmbTipDocumento.Properties.Columns.Add(New LookUpColumnInfo("IdCategoriaDetalle", "Tipo de documento", 100))

        cmbTipDocumento.Properties.Columns.Add(New LookUpColumnInfo("Estado", "No.", 20))

        cmbTipDocumento.Properties.Columns(0).Visible = False

        cmbTipDocumento.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center


    End Sub

    Private Sub cancelar()

        get_impuesto_detalle()

        Dim Result = impuestos.ContainsKey(key)

        If Result = True Then impuesto_actual = impuestos.Item(key)

        Me.Close()

    End Sub


End Class