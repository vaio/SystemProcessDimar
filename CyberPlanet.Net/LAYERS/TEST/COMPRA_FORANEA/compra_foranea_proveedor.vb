﻿Imports DevExpress.XtraEditors.Controls

Public Class compra_foranea_proveedor

    Private numero_de_sucursal As Integer
    Private numero_de_documento As String
    Private numero_de_proveedor As String

    Private accion As Integer

    Private datos_de_proveedores As New DataTable

    Public WriteOnly Property set_numero_de_proveedor As String
        Set(value As String)
            Me.numero_de_proveedor = value
        End Set
    End Property

    Public WriteOnly Property set_all_proveedores_in_detalle As DataTable
        Set(value As DataTable)
            Me.datos_de_proveedores = value
        End Set
    End Property

    Private vcamposNumericos As New Validar With {.CantDecimal = 4}

    Public Sub New(numero_de_sucursal As Integer, numero_de_documento As String, ByRef accion As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        set_ajuste()
        get_proveedores()

        Me.numero_de_sucursal = numero_de_sucursal
        Me.numero_de_documento = numero_de_documento

        Me.accion = accion

        AddHandler Me.Load, AddressOf start

    End Sub

    Private Sub start()

        AddHandler btn_cancelar.Click, AddressOf cancelar
        AddHandler btn_aceptar.Click, Sub(sender, e) aceptar(accion)

        vcamposNumericos.agregar_control(New List(Of Control)({txt_Fob, txt_Flete, txt_Seguro})).ActivarEventosNumericos()


    End Sub
    Private Sub set_ajuste()

        txt_Flete.Text = FormatNumber(0, 4)
        txt_Fob.Text = FormatNumber(0, 4)
        txt_Seguro.Text = FormatNumber(0, 4)

    End Sub
    Private Sub cancelar()

        Me.Close()

    End Sub

    Private Sub get_proveedores()

        cmb_Proveedores.Properties.DataSource = data_proveedor.get_datos_basicos_proveedores.Values

        cmb_Proveedores.Properties.DisplayMember = "get_Nombre"

        cmb_Proveedores.Properties.ValueMember = "get_ID"

        cmb_Proveedores.Properties.Columns.Add(New LookUpColumnInfo("get_ID", "No.", 20))

        cmb_Proveedores.Properties.Columns.Add(New LookUpColumnInfo("get_Nombre", "Proveedor", 100))

        cmb_Proveedores.Properties.Columns(0).Visible = False

        cmb_Proveedores.EditValue = "001"

        cmb_Proveedores.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub

    Private Function validar_datos() As Boolean

        If accion = 1 Then

            If Not datos_de_proveedores.Select("codigo_de_proveedor =" + cmb_Proveedores.EditValue.ToString, " numero_de_documento ASC").Length = 0 Then MsgBox("Ops! Proveedor ya existe en la lista de proveedores", MsgBoxStyle.Exclamation) : Return False

        End If

        If txt_Facturas.Text = String.Empty Or txt_Facturas.TextLength <= 1 Then MsgBox("Ops! no es permitido registrar un proveedor sin un numero de factura valido.", MsgBoxStyle.Exclamation) : Return False

        ' If ChangeToDouble(txt_Fob.Text) = 0 Then MsgBox("Ops! no es permitido registrar un proveedor sin monto FOB.", MsgBoxStyle.Exclamation) : Return False

        Return True

    End Function

    Private Sub aceptar(ByRef Accion As Integer)

        If Not validar_datos() = True Then Exit Sub

        ' Dim Resp = MsgBox("Seguro de proceder con la accion?", MsgBoxStyle.YesNo) : If Resp = MsgBoxResult.No Or Resp = MsgBoxResult.Ignore Then Exit Sub

        Dim Result = data_compra_foranea.set_proveedor(
                                                           Funciones.Param("@numero_de_sucursal", numero_de_sucursal, SqlDbType.Int),
                                                           Funciones.Param("@numero_de_documento", numero_de_documento, SqlDbType.VarChar),
                                                           Funciones.Param("@numero_de_proveedor", cmb_Proveedores.EditValue, SqlDbType.VarChar),
                                                           Funciones.Param("@numero_de_factura", txt_Facturas.Text, SqlDbType.VarChar),
                                                           Funciones.Param("@total_fob", ChangeToDouble(txt_Fob.Text), SqlDbType.Decimal),
                                                           Funciones.Param("@flete", ChangeToDouble(txt_Flete.Text), SqlDbType.Decimal),
                                                           Funciones.Param("@seguro", ChangeToDouble(txt_Seguro.Text), SqlDbType.Decimal),
                                                           Funciones.Param("@observacion", txt_Observacion.Text, SqlDbType.VarChar),
                                                           Funciones.Param("@tipo_de_gestion", Accion, SqlDbType.Int))


        '  If Result = 0 Then MsgBox("Proceso realizado existosamente.", MsgBoxStyle.Information)

        Me.Close()

    End Sub

    Public Sub set_datos_proveedor(ByRef data As DataRow)

        numero_de_sucursal = data.Item(0)
        numero_de_documento = data.Item(1)
        cmb_Proveedores.EditValue = data.Item(2)
        txt_Facturas.Text = data.Item(4)
        txt_Fob.Text = data.Item(5)
        txt_Flete.Text = data.Item(6)
        txt_Seguro.Text = data.Item(7)
        txt_Observacion.Text = data.Item(9)

    End Sub

End Class