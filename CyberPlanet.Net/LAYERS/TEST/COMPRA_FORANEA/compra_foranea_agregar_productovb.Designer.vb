﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class compra_foranea_agregar_producto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(compra_foranea_agregar_producto))
        Me.pBody = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_producto_peso = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_producto_dai = New System.Windows.Forms.TextBox()
        Me.btn_buscar_producto = New System.Windows.Forms.Button()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_producto_costo_total = New System.Windows.Forms.Label()
        Me.txt_producto_costo_unitario = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_producto_codigo = New System.Windows.Forms.TextBox()
        Me.cmb_producto_bodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.fecha_de_expiracion = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_producto_cantidad = New System.Windows.Forms.TextBox()
        Me.txt_producto_descripcion = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.btn_aceptar = New System.Windows.Forms.Button()
        Me.pBody.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_producto_bodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.Label9)
        Me.pBody.Controls.Add(Me.txt_producto_peso)
        Me.pBody.Controls.Add(Me.Label4)
        Me.pBody.Controls.Add(Me.txt_producto_dai)
        Me.pBody.Controls.Add(Me.btn_buscar_producto)
        Me.pBody.Controls.Add(Me.Tabla)
        Me.pBody.Controls.Add(Me.Label12)
        Me.pBody.Controls.Add(Me.txt_producto_costo_total)
        Me.pBody.Controls.Add(Me.txt_producto_costo_unitario)
        Me.pBody.Controls.Add(Me.Label11)
        Me.pBody.Controls.Add(Me.txt_producto_codigo)
        Me.pBody.Controls.Add(Me.cmb_producto_bodega)
        Me.pBody.Controls.Add(Me.Label8)
        Me.pBody.Controls.Add(Me.fecha_de_expiracion)
        Me.pBody.Controls.Add(Me.Label6)
        Me.pBody.Controls.Add(Me.Label1)
        Me.pBody.Controls.Add(Me.Label2)
        Me.pBody.Controls.Add(Me.Label7)
        Me.pBody.Controls.Add(Me.txt_producto_cantidad)
        Me.pBody.Controls.Add(Me.txt_producto_descripcion)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 0)
        Me.pBody.Margin = New System.Windows.Forms.Padding(4)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(675, 397)
        Me.pBody.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 188)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 17)
        Me.Label9.TabIndex = 43
        Me.Label9.Text = "Peso (Kg):"
        '
        'txt_producto_peso
        '
        Me.txt_producto_peso.BackColor = System.Drawing.Color.White
        Me.txt_producto_peso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_producto_peso.Location = New System.Drawing.Point(167, 186)
        Me.txt_producto_peso.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_producto_peso.MaxLength = 12
        Me.txt_producto_peso.Name = "txt_producto_peso"
        Me.txt_producto_peso.Size = New System.Drawing.Size(130, 22)
        Me.txt_producto_peso.TabIndex = 3
        Me.txt_producto_peso.Text = "0.0000"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 158)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 17)
        Me.Label4.TabIndex = 41
        Me.Label4.Text = "DAI:"
        '
        'txt_producto_dai
        '
        Me.txt_producto_dai.BackColor = System.Drawing.Color.White
        Me.txt_producto_dai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_producto_dai.Location = New System.Drawing.Point(167, 156)
        Me.txt_producto_dai.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_producto_dai.MaxLength = 12
        Me.txt_producto_dai.Name = "txt_producto_dai"
        Me.txt_producto_dai.Size = New System.Drawing.Size(130, 22)
        Me.txt_producto_dai.TabIndex = 2
        Me.txt_producto_dai.Text = "0.0000"
        '
        'btn_buscar_producto
        '
        Me.btn_buscar_producto.ForeColor = System.Drawing.Color.Green
        Me.btn_buscar_producto.Location = New System.Drawing.Point(303, 17)
        Me.btn_buscar_producto.Name = "btn_buscar_producto"
        Me.btn_buscar_producto.Size = New System.Drawing.Size(41, 30)
        Me.btn_buscar_producto.TabIndex = 39
        Me.btn_buscar_producto.Text = "+"
        Me.btn_buscar_producto.UseVisualStyleBackColor = True
        '
        'Tabla
        '
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        GridLevelNode1.RelationName = "Level1"
        Me.Tabla.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.Tabla.Location = New System.Drawing.Point(313, 126)
        Me.Tabla.MainView = Me.GridView1
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(335, 256)
        Me.Tabla.TabIndex = 38
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowViewCaption = True
        Me.GridView1.ViewCaption = "Existencias en Bodegas"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(27, 249)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(106, 17)
        Me.Label12.TabIndex = 37
        Me.Label12.Text = "Costo Total ($):"
        '
        'txt_producto_costo_total
        '
        Me.txt_producto_costo_total.BackColor = System.Drawing.Color.Snow
        Me.txt_producto_costo_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_producto_costo_total.Location = New System.Drawing.Point(167, 248)
        Me.txt_producto_costo_total.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.txt_producto_costo_total.Name = "txt_producto_costo_total"
        Me.txt_producto_costo_total.Size = New System.Drawing.Size(130, 24)
        Me.txt_producto_costo_total.TabIndex = 5
        Me.txt_producto_costo_total.Text = "0.0000"
        '
        'txt_producto_costo_unitario
        '
        Me.txt_producto_costo_unitario.BackColor = System.Drawing.Color.White
        Me.txt_producto_costo_unitario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_producto_costo_unitario.Location = New System.Drawing.Point(167, 216)
        Me.txt_producto_costo_unitario.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_producto_costo_unitario.MaxLength = 12
        Me.txt_producto_costo_unitario.Name = "txt_producto_costo_unitario"
        Me.txt_producto_costo_unitario.Size = New System.Drawing.Size(130, 22)
        Me.txt_producto_costo_unitario.TabIndex = 4
        Me.txt_producto_costo_unitario.Text = "0.00000000"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(27, 219)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(123, 17)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Costo Unitario ($):"
        '
        'txt_producto_codigo
        '
        Me.txt_producto_codigo.BackColor = System.Drawing.Color.Snow
        Me.txt_producto_codigo.Enabled = False
        Me.txt_producto_codigo.Location = New System.Drawing.Point(167, 22)
        Me.txt_producto_codigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_producto_codigo.Name = "txt_producto_codigo"
        Me.txt_producto_codigo.Size = New System.Drawing.Size(132, 22)
        Me.txt_producto_codigo.TabIndex = 33
        '
        'cmb_producto_bodega
        '
        Me.cmb_producto_bodega.Location = New System.Drawing.Point(167, 90)
        Me.cmb_producto_bodega.Margin = New System.Windows.Forms.Padding(4)
        Me.cmb_producto_bodega.Name = "cmb_producto_bodega"
        Me.cmb_producto_bodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_producto_bodega.Size = New System.Drawing.Size(486, 23)
        Me.cmb_producto_bodega.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(395, 26)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 17)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Fecha Expiración:"
        Me.Label8.Visible = False
        '
        'fecha_de_expiracion
        '
        Me.fecha_de_expiracion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.fecha_de_expiracion.Location = New System.Drawing.Point(524, 22)
        Me.fecha_de_expiracion.Margin = New System.Windows.Forms.Padding(4)
        Me.fecha_de_expiracion.Name = "fecha_de_expiracion"
        Me.fecha_de_expiracion.Size = New System.Drawing.Size(129, 22)
        Me.fecha_de_expiracion.TabIndex = 4
        Me.fecha_de_expiracion.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 94)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Bodega:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo Producto:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 57)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripción:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 128)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 17)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Cantidad:"
        '
        'txt_producto_cantidad
        '
        Me.txt_producto_cantidad.BackColor = System.Drawing.Color.White
        Me.txt_producto_cantidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_producto_cantidad.Location = New System.Drawing.Point(167, 126)
        Me.txt_producto_cantidad.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_producto_cantidad.MaxLength = 12
        Me.txt_producto_cantidad.Name = "txt_producto_cantidad"
        Me.txt_producto_cantidad.Size = New System.Drawing.Size(130, 22)
        Me.txt_producto_cantidad.TabIndex = 1
        Me.txt_producto_cantidad.Text = "0.0000"
        '
        'txt_producto_descripcion
        '
        Me.txt_producto_descripcion.BackColor = System.Drawing.Color.Snow
        Me.txt_producto_descripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_producto_descripcion.Location = New System.Drawing.Point(167, 55)
        Me.txt_producto_descripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.txt_producto_descripcion.Name = "txt_producto_descripcion"
        Me.txt_producto_descripcion.Size = New System.Drawing.Size(487, 24)
        Me.txt_producto_descripcion.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_cancelar)
        Me.Panel1.Controls.Add(Me.btn_aceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 397)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(675, 60)
        Me.Panel1.TabIndex = 5
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Image = CType(resources.GetObject("btn_cancelar.Image"), System.Drawing.Image)
        Me.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_cancelar.Location = New System.Drawing.Point(545, 10)
        Me.btn_cancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(104, 43)
        Me.btn_cancelar.TabIndex = 1
        Me.btn_cancelar.Text = "&Cancelar"
        Me.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btn_aceptar.Image = CType(resources.GetObject("btn_aceptar.Image"), System.Drawing.Image)
        Me.btn_aceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_aceptar.Location = New System.Drawing.Point(433, 10)
        Me.btn_aceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(104, 43)
        Me.btn_aceptar.TabIndex = 0
        Me.btn_aceptar.Text = "&Aceptar"
        Me.btn_aceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_aceptar.UseVisualStyleBackColor = True
        '
        'compra_foranea_agregar_producto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(675, 457)
        Me.Controls.Add(Me.pBody)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "compra_foranea_agregar_producto"
        Me.Text = "compra_foranea_agregar_productovb"
        Me.pBody.ResumeLayout(False)
        Me.pBody.PerformLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_producto_bodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents btn_buscar_producto As System.Windows.Forms.Button
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_producto_costo_total As System.Windows.Forms.Label
    Friend WithEvents txt_producto_costo_unitario As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_producto_codigo As System.Windows.Forms.TextBox
    Friend WithEvents cmb_producto_bodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents fecha_de_expiracion As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_producto_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents txt_producto_descripcion As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_producto_dai As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_producto_peso As System.Windows.Forms.TextBox
End Class
