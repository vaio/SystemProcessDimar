﻿Imports DevExpress.XtraEditors.Controls

Public Class compra_foranea_agregar_producto

    Private tasa_cabio_de_documento As Decimal

    Private peso As Boolean

    Private vcampos As New Validar With {.CantDecimal = 4}

    Private vcampo_costo_unitario As New Validar With {.CantDecimal = 8}

    Private accion As Integer

    Private curr_producto As New compra_foranea_producto

    Private sucursal As Integer

    Private numero_de_documento As String

    Private msg_error As String

    '''modificado 

    Public WriteOnly Property set_producto As compra_foranea_producto
        Set(value As compra_foranea_producto)
            curr_producto = value
        End Set
    End Property

    Public WriteOnly Property get_tasa_cabio_de_documento
        Set(value)
            tasa_cabio_de_documento = value
        End Set
    End Property

    Public WriteOnly Property get_peso As Boolean
        Set(value As Boolean)
            peso = value
        End Set
    End Property

    Public Sub New(sucursal As Integer, numero_de_documento As String, accion As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Me.sucursal = sucursal
        Me.numero_de_documento = numero_de_documento
        Me.accion = accion

        set_bodegas()

        AddHandler Me.Load, AddressOf start

    End Sub

    Private Sub start()

        vcampos.agregar_control(New List(Of Control)({txt_producto_cantidad, txt_producto_dai, txt_producto_peso})).ActivarEventosNumericos()

        vcampo_costo_unitario.agregar_control(New List(Of Control)({txt_producto_costo_unitario})).ActivarEventosNumericos()

        AddHandler btn_buscar_producto.Click, AddressOf lista_de_productos
        AddHandler cmb_producto_bodega.EditValueChanged, AddressOf get_costo_promedio_por_bodega

        AddHandler txt_producto_cantidad.TextChanged, AddressOf calculo_basico_total
        AddHandler txt_producto_costo_unitario.TextChanged, AddressOf calculo_basico_total

        AddHandler btn_aceptar.Click, AddressOf aceptar
        AddHandler btn_cancelar.Click, AddressOf cancelar

        ajuste_de_componentes()

    End Sub
    Private Sub calculo_basico_total()

        txt_producto_costo_total.Text = FormatNumber(ChangeToDouble(txt_producto_cantidad.Text) * ChangeToDouble(txt_producto_costo_unitario.Text))

    End Sub
    Private Sub ajuste_de_componentes()

        If peso = True Then txt_producto_peso.Enabled = False : txt_producto_peso.BackColor = Color.Snow

        If accion = 2 Then btn_buscar_producto.Enabled = False : cmb_producto_bodega.Enabled = False

        If accion = 3 Then pBody.Enabled = False

    End Sub
    Public Sub set_valores_a_formulario()


        txt_producto_descripcion.Text = curr_producto.get_descripcion
        cmb_producto_bodega.EditValue = curr_producto.get_almacen_codigo.ToString

        txt_producto_codigo.Text = curr_producto.get_producto_codigo
        txt_producto_cantidad.Text = curr_producto.get_producto_cantidad
        txt_producto_dai.Text = curr_producto.get_producto_dai
        txt_producto_peso.Text = curr_producto.get_producto_peso
        txt_producto_costo_unitario.Text = curr_producto.get_producto_precio
        txt_producto_costo_total.Text = curr_producto.get_producto_cantidad * curr_producto.get_producto_precio

    End Sub
    Private Sub lista_de_productos()

        Dim productos As New compra_local_busqueda_producto With {.StartPosition = FormStartPosition.CenterScreen}

        productos.ShowDialog()

        get_datos_de_producto(productos)

    End Sub

    Private Sub get_datos_de_producto(ByRef curr_producto As compra_local_busqueda_producto)

        txt_producto_codigo.Text = curr_producto.get_codigo
        txt_producto_descripcion.Text = curr_producto.get_nombre

        get_costo_promedio_por_bodega()

        Tabla.DataSource = data_producto.get_existencia_en_bodegas(curr_producto.get_codigo)

    End Sub

    Private Sub get_costo_promedio_por_bodega()

        If txt_producto_codigo.Text = String.Empty Then Exit Sub

        Dim costo_cordoba As Decimal = CDec(data_producto.get_existencia_costo_por_bodega_por_producto(Funciones.Param("@cod_bodega", Me.cmb_producto_bodega.EditValue, SqlDbType.Int), Funciones.Param("@cod_producto", txt_producto_codigo.Text, SqlDbType.NChar)).Rows(0).Item(3))

        txt_producto_costo_unitario.Text = FormatNumber(IIf(costo_cordoba = 0, 0, costo_cordoba / tasa_cabio_de_documento), 4)

    End Sub

    Private Sub set_bodegas()

        cmb_producto_bodega.Properties.DataSource = data_fill_up_comb.get_bodegas_M_PT

        cmb_producto_bodega.Properties.DisplayMember = "Nombre_Bodega"
        cmb_producto_bodega.Properties.ValueMember = "Codigo_Bodega"

        cmb_producto_bodega.Properties.Columns.Add(New LookUpColumnInfo("Codigo_Bodega", "No.", 20))

        cmb_producto_bodega.Properties.Columns.Add(New LookUpColumnInfo("Nombre_Bodega", "BODEGA DE DESTINO", 100))

        cmb_producto_bodega.Properties.Columns(0).Visible = False

        cmb_producto_bodega.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        cmb_producto_bodega.EditValue = 2

    End Sub
    Private Function vaplicar() As Boolean

        If txt_producto_codigo.Text = String.Empty Then msg_error = "Producto no seleccionado" : Return False
        If ChangeToDouble(txt_producto_cantidad.Text) = 0 Then msg_error = "Cantidad no digitada" : Return False
        If Not accion = 3 Then If peso = False And ChangeToDouble(txt_producto_peso.Text) = 0 Then msg_error = "Peso no digitado" : Return False
        If ChangeToDouble(txt_producto_costo_total.Text) = 0 Then msg_error = "Costo no digitado" : Return False

        Return True

    End Function
    Private Sub aceptar()

        If vaplicar() = False Then MsgBox(String.Format("Ops! {0} ", msg_error), MsgBoxStyle.Exclamation) : Exit Sub

        curr_producto = New compra_foranea_producto(sucursal,
                                                    numero_de_documento,
                                                    cmb_producto_bodega.EditValue,
                                                    txt_producto_codigo.Text,
                                                    ChangeToDouble(txt_producto_cantidad.Text),
                                                    ChangeToDouble(txt_producto_costo_unitario.Text),
                                                    ChangeToDouble(txt_producto_peso.Text),
                                                    ChangeToDouble(txt_producto_dai.Text),
                                                    fecha_de_expiracion.Value)

        Select Case accion

            Case 1

                curr_producto.guardar()

            Case 2

                curr_producto.actualizar()

            Case 3

                curr_producto.eliminar()

        End Select

        Me.Close()

    End Sub

    Private Sub cancelar()

        Me.Close()

    End Sub


End Class