﻿Imports DevExpress.Utils

Public Class compra_foranea_buscador

    Private curr_compra As New compra

    Public ReadOnly Property get_compra As compra
        Get
            Return Me.curr_compra
        End Get
    End Property

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AddHandler Me.Load, AddressOf start

    End Sub

    Private Sub start()

        AddHandler btn_aceptar.Click, AddressOf set_compra
        AddHandler btn_cancelar.Click, AddressOf cancelar

        Tabla.DataSource = data_compra_foranea.get_lista_de_compra(nSucursal)

        ajuste_de_gridview()

    End Sub

    Private Sub ajuste_de_gridview()

        With GridView1

            If .Columns.Count = 0 Then Exit Sub

            .Columns(0).Visible = False

            .BestFitColumns()

            .Columns(4).DisplayFormat.FormatType = FormatType.Numeric
            .Columns(4).DisplayFormat.FormatString = "N"

        End With

    End Sub

    Private Sub set_compra()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount <= 0 Then Exit Sub

        Dim sucursal As Integer = CInt(GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "sucursal"))
        Dim numero_de_documento As String = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Documento").ToString()

        curr_compra = New compra(sucursal, numero_de_documento).get_compra

        Me.Close()

    End Sub

    Private Sub cancelar()

        Me.Close()

    End Sub

End Class