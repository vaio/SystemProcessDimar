﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterCompraForanea
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterCompraForanea))
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnNacionalizar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.luProveedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.deRecepcionado = New System.Windows.Forms.DateTimePicker()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rbDolares = New System.Windows.Forms.RadioButton()
        Me.btnTasaCambio = New System.Windows.Forms.Button()
        Me.rbCordobas = New System.Windows.Forms.RadioButton()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtTasaCambio = New System.Windows.Forms.TextBox()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtGastosExport = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.grbTransporte = New System.Windows.Forms.GroupBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtOtros = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtSeguro = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtFletes = New System.Windows.Forms.TextBox()
        Me.txtFOB = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbCredito = New System.Windows.Forms.RadioButton()
        Me.rbContado = New System.Windows.Forms.RadioButton()
        Me.txtPlazo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtSSA = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtTSI = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtSPE = New System.Windows.Forms.TextBox()
        Me.txtISC = New System.Windows.Forms.TextBox()
        Me.txtDAI = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtFletesTerrestre = New System.Windows.Forms.TextBox()
        Me.btnPagoTrans = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPagoAdua = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPagoPol = New DevExpress.XtraEditors.SimpleButton()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtAduana = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtIVAPoliza = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btnOtroPago = New DevExpress.XtraEditors.SimpleButton()
        Me.txtOAduana = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtOTransporte = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtOtroPago = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtNumFacturas = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Fecha_Vencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.deFechaVenc = New System.Windows.Forms.DateTimePicker()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNumDocumento = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TblProvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblDetalleDocBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMasterDocBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtPesoKgms = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtImpuestos = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCIFUSS = New System.Windows.Forms.TextBox()
        Me.txtCostoTotal = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtCIFCS = New System.Windows.Forms.TextBox()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.grvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grdDetalle = New DevExpress.XtraGrid.GridControl()
        Me.Panel3.SuspendLayout
        Me.Panel1.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.Panel2.SuspendLayout
        CType(Me.luProveedor.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBox5.SuspendLayout
        Me.GroupBox7.SuspendLayout
        Me.grbTransporte.SuspendLayout
        Me.GroupBox2.SuspendLayout
        Me.GroupBox4.SuspendLayout
        Me.GroupBox3.SuspendLayout
        Me.GroupBox6.SuspendLayout
        CType(Me.TblProvBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblDetalleDocBS,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TblMasterDocBS,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Panel5.SuspendLayout
        Me.Panel4.SuspendLayout
        CType(Me.grvDetalle,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.grdDetalle,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnNacionalizar)
        Me.Panel3.Controls.Add(Me.Panel1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 686)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1655, 64)
        Me.Panel3.TabIndex = 1
        '
        'btnNacionalizar
        '
        Me.btnNacionalizar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnNacionalizar.Image = CType(resources.GetObject("btnNacionalizar.Image"),System.Drawing.Image)
        Me.btnNacionalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNacionalizar.Location = New System.Drawing.Point(1505, 10)
        Me.btnNacionalizar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnNacionalizar.Name = "btnNacionalizar"
        Me.btnNacionalizar.Size = New System.Drawing.Size(132, 43)
        Me.btnNacionalizar.TabIndex = 2
        Me.btnNacionalizar.Text = "&Nacionalizar"
        Me.btnNacionalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNacionalizar.UseVisualStyleBackColor = true
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.btnModificar)
        Me.Panel1.Controls.Add(Me.btnAgregar)
        Me.Panel1.Location = New System.Drawing.Point(3, 1)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(379, 62)
        Me.Panel1.TabIndex = 1
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = false
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"),System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(255, 5)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(111, 49)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = true
        '
        'btnModificar
        '
        Me.btnModificar.Enabled = false
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"),System.Drawing.Image)
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(136, 5)
        Me.btnModificar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(111, 49)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = true
        '
        'btnAgregar
        '
        Me.btnAgregar.Enabled = false
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"),System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(16, 5)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(111, 49)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = true
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(1924, 337)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Datos del Documento"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.luProveedor)
        Me.Panel2.Controls.Add(Me.deRecepcionado)
        Me.Panel2.Controls.Add(Me.Label32)
        Me.Panel2.Controls.Add(Me.GroupBox5)
        Me.Panel2.Controls.Add(Me.cmdAceptar)
        Me.Panel2.Controls.Add(Me.GroupBox7)
        Me.Panel2.Controls.Add(Me.grbTransporte)
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Controls.Add(Me.GroupBox4)
        Me.Panel2.Controls.Add(Me.GroupBox3)
        Me.Panel2.Controls.Add(Me.GroupBox6)
        Me.Panel2.Controls.Add(Me.txtNumFacturas)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Fecha_Vencimiento)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.deFechaVenc)
        Me.Panel2.Controls.Add(Me.txtObservacion)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txtNumDocumento)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(4, 19)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1916, 308)
        Me.Panel2.TabIndex = 0
        '
        'luProveedor
        '
        Me.luProveedor.Location = New System.Drawing.Point(153, 140)
        Me.luProveedor.Name = "luProveedor"
        Me.luProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luProveedor.Size = New System.Drawing.Size(255, 23)
        Me.luProveedor.TabIndex = 65
        '
        'deRecepcionado
        '
        Me.deRecepcionado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deRecepcionado.Location = New System.Drawing.Point(141, 58)
        Me.deRecepcionado.Margin = New System.Windows.Forms.Padding(4)
        Me.deRecepcionado.Name = "deRecepcionado"
        Me.deRecepcionado.Size = New System.Drawing.Size(144, 22)
        Me.deRecepcionado.TabIndex = 63
        '
        'Label32
        '
        Me.Label32.AutoSize = true
        Me.Label32.Location = New System.Drawing.Point(15, 62)
        Me.Label32.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(103, 17)
        Me.Label32.TabIndex = 64
        Me.Label32.Text = "Recepcionado:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rbDolares)
        Me.GroupBox5.Controls.Add(Me.btnTasaCambio)
        Me.GroupBox5.Controls.Add(Me.rbCordobas)
        Me.GroupBox5.Controls.Add(Me.Label31)
        Me.GroupBox5.Controls.Add(Me.txtTasaCambio)
        Me.GroupBox5.Location = New System.Drawing.Point(482, 91)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Size = New System.Drawing.Size(429, 63)
        Me.GroupBox5.TabIndex = 62
        Me.GroupBox5.TabStop = false
        Me.GroupBox5.Text = "Moneda"
        '
        'rbDolares
        '
        Me.rbDolares.AutoSize = true
        Me.rbDolares.Location = New System.Drawing.Point(112, 27)
        Me.rbDolares.Margin = New System.Windows.Forms.Padding(4)
        Me.rbDolares.Name = "rbDolares"
        Me.rbDolares.Size = New System.Drawing.Size(78, 21)
        Me.rbDolares.TabIndex = 1
        Me.rbDolares.Text = "Dolares"
        Me.rbDolares.UseVisualStyleBackColor = true
        '
        'btnTasaCambio
        '
        Me.btnTasaCambio.Enabled = false
        Me.btnTasaCambio.Image = CType(resources.GetObject("btnTasaCambio.Image"),System.Drawing.Image)
        Me.btnTasaCambio.Location = New System.Drawing.Point(387, 23)
        Me.btnTasaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.btnTasaCambio.Name = "btnTasaCambio"
        Me.btnTasaCambio.Size = New System.Drawing.Size(36, 28)
        Me.btnTasaCambio.TabIndex = 61
        Me.btnTasaCambio.UseVisualStyleBackColor = true
        '
        'rbCordobas
        '
        Me.rbCordobas.AutoSize = true
        Me.rbCordobas.Checked = true
        Me.rbCordobas.Location = New System.Drawing.Point(16, 27)
        Me.rbCordobas.Margin = New System.Windows.Forms.Padding(4)
        Me.rbCordobas.Name = "rbCordobas"
        Me.rbCordobas.Size = New System.Drawing.Size(90, 21)
        Me.rbCordobas.TabIndex = 0
        Me.rbCordobas.TabStop = true
        Me.rbCordobas.Text = "Cordobas"
        Me.rbCordobas.UseVisualStyleBackColor = true
        '
        'Label31
        '
        Me.Label31.AutoSize = true
        Me.Label31.Location = New System.Drawing.Point(192, 30)
        Me.Label31.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(95, 17)
        Me.Label31.TabIndex = 25
        Me.Label31.Text = "Taza Cambio:"
        '
        'txtTasaCambio
        '
        Me.txtTasaCambio.BackColor = System.Drawing.Color.White
        Me.txtTasaCambio.Enabled = false
        Me.txtTasaCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtTasaCambio.Location = New System.Drawing.Point(291, 25)
        Me.txtTasaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTasaCambio.Name = "txtTasaCambio"
        Me.txtTasaCambio.ReadOnly = true
        Me.txtTasaCambio.Size = New System.Drawing.Size(87, 24)
        Me.txtTasaCambio.TabIndex = 1
        Me.txtTasaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"),System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(1363, 254)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(211, 32)
        Me.cmdAceptar.TabIndex = 51
        Me.cmdAceptar.Text = "&Ver pagos relacionados"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = true
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtGastosExport)
        Me.GroupBox7.Controls.Add(Me.Label12)
        Me.GroupBox7.Location = New System.Drawing.Point(1075, 236)
        Me.GroupBox7.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox7.Size = New System.Drawing.Size(280, 62)
        Me.GroupBox7.TabIndex = 50
        Me.GroupBox7.TabStop = false
        Me.GroupBox7.Text = "Gastos Aplicables"
        '
        'txtGastosExport
        '
        Me.txtGastosExport.Cursor = System.Windows.Forms.Cursors.No
        Me.txtGastosExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtGastosExport.Location = New System.Drawing.Point(119, 22)
        Me.txtGastosExport.Margin = New System.Windows.Forms.Padding(4)
        Me.txtGastosExport.Name = "txtGastosExport"
        Me.txtGastosExport.ReadOnly = true
        Me.txtGastosExport.Size = New System.Drawing.Size(127, 26)
        Me.txtGastosExport.TabIndex = 1
        Me.txtGastosExport.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = true
        Me.Label12.Location = New System.Drawing.Point(28, 30)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(85, 17)
        Me.Label12.TabIndex = 29
        Me.Label12.Text = "Importación:"
        '
        'grbTransporte
        '
        Me.grbTransporte.Controls.Add(Me.txtTotal)
        Me.grbTransporte.Controls.Add(Me.Label30)
        Me.grbTransporte.Controls.Add(Me.txtOtros)
        Me.grbTransporte.Controls.Add(Me.Label29)
        Me.grbTransporte.Controls.Add(Me.txtSeguro)
        Me.grbTransporte.Controls.Add(Me.Label25)
        Me.grbTransporte.Controls.Add(Me.Label18)
        Me.grbTransporte.Controls.Add(Me.txtFletes)
        Me.grbTransporte.Controls.Add(Me.txtFOB)
        Me.grbTransporte.Controls.Add(Me.Label20)
        Me.grbTransporte.Location = New System.Drawing.Point(773, 167)
        Me.grbTransporte.Margin = New System.Windows.Forms.Padding(4)
        Me.grbTransporte.Name = "grbTransporte"
        Me.grbTransporte.Padding = New System.Windows.Forms.Padding(4)
        Me.grbTransporte.Size = New System.Drawing.Size(815, 62)
        Me.grbTransporte.TabIndex = 46
        Me.grbTransporte.TabStop = false
        Me.grbTransporte.Text = "Factura Proveedor"
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.White
        Me.txtTotal.Cursor = System.Windows.Forms.Cursors.No
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtTotal.Location = New System.Drawing.Point(696, 25)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = true
        Me.txtTotal.Size = New System.Drawing.Size(105, 24)
        Me.txtTotal.TabIndex = 51
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label30
        '
        Me.Label30.AutoSize = true
        Me.Label30.Location = New System.Drawing.Point(653, 30)
        Me.Label30.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(44, 17)
        Me.Label30.TabIndex = 42
        Me.Label30.Text = "Total:"
        '
        'txtOtros
        '
        Me.txtOtros.BackColor = System.Drawing.Color.White
        Me.txtOtros.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txtOtros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtOtros.Location = New System.Drawing.Point(541, 25)
        Me.txtOtros.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOtros.Name = "txtOtros"
        Me.txtOtros.Size = New System.Drawing.Size(105, 24)
        Me.txtOtros.TabIndex = 40
        Me.txtOtros.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label29
        '
        Me.Label29.AutoSize = true
        Me.Label29.Location = New System.Drawing.Point(493, 30)
        Me.Label29.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(47, 17)
        Me.Label29.TabIndex = 41
        Me.Label29.Text = "Otros:"
        '
        'txtSeguro
        '
        Me.txtSeguro.BackColor = System.Drawing.Color.White
        Me.txtSeguro.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txtSeguro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtSeguro.Location = New System.Drawing.Point(381, 25)
        Me.txtSeguro.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSeguro.Name = "txtSeguro"
        Me.txtSeguro.Size = New System.Drawing.Size(105, 24)
        Me.txtSeguro.TabIndex = 38
        Me.txtSeguro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label25
        '
        Me.Label25.AutoSize = true
        Me.Label25.Location = New System.Drawing.Point(321, 30)
        Me.Label25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 17)
        Me.Label25.TabIndex = 39
        Me.Label25.Text = "Seguro:"
        '
        'Label18
        '
        Me.Label18.AutoSize = true
        Me.Label18.Location = New System.Drawing.Point(8, 30)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(40, 17)
        Me.Label18.TabIndex = 33
        Me.Label18.Text = "FOB:"
        '
        'txtFletes
        '
        Me.txtFletes.BackColor = System.Drawing.Color.White
        Me.txtFletes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txtFletes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtFletes.Location = New System.Drawing.Point(205, 25)
        Me.txtFletes.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFletes.Name = "txtFletes"
        Me.txtFletes.Size = New System.Drawing.Size(105, 24)
        Me.txtFletes.TabIndex = 0
        Me.txtFletes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFOB
        '
        Me.txtFOB.BackColor = System.Drawing.Color.White
        Me.txtFOB.Cursor = System.Windows.Forms.Cursors.No
        Me.txtFOB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtFOB.Location = New System.Drawing.Point(51, 25)
        Me.txtFOB.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFOB.Name = "txtFOB"
        Me.txtFOB.ReadOnly = true
        Me.txtFOB.Size = New System.Drawing.Size(108, 24)
        Me.txtFOB.TabIndex = 0
        Me.txtFOB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label20
        '
        Me.Label20.AutoSize = true
        Me.Label20.Location = New System.Drawing.Point(163, 30)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(43, 17)
        Me.Label20.TabIndex = 37
        Me.Label20.Text = "Flete:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbCredito)
        Me.GroupBox2.Controls.Add(Me.rbContado)
        Me.GroupBox2.Controls.Add(Me.txtPlazo)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(482, 11)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(429, 71)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Tipo de Compra"
        '
        'rbCredito
        '
        Me.rbCredito.AutoSize = true
        Me.rbCredito.Location = New System.Drawing.Point(123, 31)
        Me.rbCredito.Margin = New System.Windows.Forms.Padding(4)
        Me.rbCredito.Name = "rbCredito"
        Me.rbCredito.Size = New System.Drawing.Size(74, 21)
        Me.rbCredito.TabIndex = 1
        Me.rbCredito.Text = "Credito"
        Me.rbCredito.UseVisualStyleBackColor = true
        '
        'rbContado
        '
        Me.rbContado.AutoSize = true
        Me.rbContado.Checked = true
        Me.rbContado.Location = New System.Drawing.Point(28, 31)
        Me.rbContado.Margin = New System.Windows.Forms.Padding(4)
        Me.rbContado.Name = "rbContado"
        Me.rbContado.Size = New System.Drawing.Size(82, 21)
        Me.rbContado.TabIndex = 0
        Me.rbContado.TabStop = true
        Me.rbContado.Text = "Contado"
        Me.rbContado.UseVisualStyleBackColor = true
        '
        'txtPlazo
        '
        Me.txtPlazo.BackColor = System.Drawing.Color.White
        Me.txtPlazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtPlazo.Location = New System.Drawing.Point(275, 27)
        Me.txtPlazo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPlazo.MaxLength = 3
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(131, 26)
        Me.txtPlazo.TabIndex = 9
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = true
        Me.Label8.Location = New System.Drawing.Point(219, 33)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 17)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Plazo:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtSSA)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Controls.Add(Me.txtTSI)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.txtSPE)
        Me.GroupBox4.Controls.Add(Me.txtISC)
        Me.GroupBox4.Controls.Add(Me.txtDAI)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Location = New System.Drawing.Point(17, 166)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox4.Size = New System.Drawing.Size(748, 63)
        Me.GroupBox4.TabIndex = 45
        Me.GroupBox4.TabStop = false
        Me.GroupBox4.Text = "Impuesto en Poliza"
        '
        'txtSSA
        '
        Me.txtSSA.BackColor = System.Drawing.Color.White
        Me.txtSSA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtSSA.Location = New System.Drawing.Point(47, 25)
        Me.txtSSA.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSSA.Name = "txtSSA"
        Me.txtSSA.Size = New System.Drawing.Size(105, 24)
        Me.txtSSA.TabIndex = 36
        Me.txtSSA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label28
        '
        Me.Label28.AutoSize = true
        Me.Label28.Location = New System.Drawing.Point(3, 30)
        Me.Label28.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(39, 17)
        Me.Label28.TabIndex = 37
        Me.Label28.Text = "SSA:"
        '
        'txtTSI
        '
        Me.txtTSI.BackColor = System.Drawing.Color.White
        Me.txtTSI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtTSI.Location = New System.Drawing.Point(191, 25)
        Me.txtTSI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTSI.Name = "txtTSI"
        Me.txtTSI.Size = New System.Drawing.Size(105, 24)
        Me.txtTSI.TabIndex = 0
        Me.txtTSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = true
        Me.Label11.Location = New System.Drawing.Point(160, 30)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(33, 17)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "TSI:"
        '
        'txtSPE
        '
        Me.txtSPE.BackColor = System.Drawing.Color.White
        Me.txtSPE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtSPE.Location = New System.Drawing.Point(341, 25)
        Me.txtSPE.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSPE.Name = "txtSPE"
        Me.txtSPE.Size = New System.Drawing.Size(105, 24)
        Me.txtSPE.TabIndex = 1
        Me.txtSPE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtISC
        '
        Me.txtISC.BackColor = System.Drawing.Color.White
        Me.txtISC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtISC.Location = New System.Drawing.Point(487, 25)
        Me.txtISC.Margin = New System.Windows.Forms.Padding(4)
        Me.txtISC.Name = "txtISC"
        Me.txtISC.Size = New System.Drawing.Size(105, 24)
        Me.txtISC.TabIndex = 3
        Me.txtISC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDAI
        '
        Me.txtDAI.BackColor = System.Drawing.Color.White
        Me.txtDAI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtDAI.Location = New System.Drawing.Point(635, 25)
        Me.txtDAI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDAI.Name = "txtDAI"
        Me.txtDAI.Size = New System.Drawing.Size(105, 24)
        Me.txtDAI.TabIndex = 2
        Me.txtDAI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(600, 30)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 17)
        Me.Label14.TabIndex = 33
        Me.Label14.Text = "DAI:"
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Location = New System.Drawing.Point(452, 30)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(33, 17)
        Me.Label15.TabIndex = 35
        Me.Label15.Text = "ISC:"
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(303, 30)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(39, 17)
        Me.Label13.TabIndex = 31
        Me.Label13.Text = "SPE:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtFletesTerrestre)
        Me.GroupBox3.Controls.Add(Me.btnPagoTrans)
        Me.GroupBox3.Controls.Add(Me.btnPagoAdua)
        Me.GroupBox3.Controls.Add(Me.btnPagoPol)
        Me.GroupBox3.Controls.Add(Me.Label27)
        Me.GroupBox3.Controls.Add(Me.txtAduana)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.txtIVAPoliza)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Location = New System.Drawing.Point(919, 11)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox3.Size = New System.Drawing.Size(301, 143)
        Me.GroupBox3.TabIndex = 44
        Me.GroupBox3.TabStop = false
        Me.GroupBox3.Text = "Pago IVA"
        '
        'txtFletesTerrestre
        '
        Me.txtFletesTerrestre.BackColor = System.Drawing.Color.White
        Me.txtFletesTerrestre.Cursor = System.Windows.Forms.Cursors.No
        Me.txtFletesTerrestre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtFletesTerrestre.Location = New System.Drawing.Point(101, 105)
        Me.txtFletesTerrestre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFletesTerrestre.Name = "txtFletesTerrestre"
        Me.txtFletesTerrestre.ReadOnly = true
        Me.txtFletesTerrestre.Size = New System.Drawing.Size(155, 26)
        Me.txtFletesTerrestre.TabIndex = 2
        Me.txtFletesTerrestre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnPagoTrans
        '
        Me.btnPagoTrans.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnPagoTrans.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnPagoTrans.Appearance.Options.UseFont = true
        Me.btnPagoTrans.Appearance.Options.UseForeColor = true
        Me.btnPagoTrans.Location = New System.Drawing.Point(264, 106)
        Me.btnPagoTrans.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPagoTrans.Name = "btnPagoTrans"
        Me.btnPagoTrans.Size = New System.Drawing.Size(27, 23)
        Me.btnPagoTrans.TabIndex = 44
        Me.btnPagoTrans.Text = "+"
        Me.btnPagoTrans.ToolTip = "Agregar un nuevo Departamento"
        '
        'btnPagoAdua
        '
        Me.btnPagoAdua.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnPagoAdua.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnPagoAdua.Appearance.Options.UseFont = true
        Me.btnPagoAdua.Appearance.Options.UseForeColor = true
        Me.btnPagoAdua.Location = New System.Drawing.Point(264, 64)
        Me.btnPagoAdua.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPagoAdua.Name = "btnPagoAdua"
        Me.btnPagoAdua.Size = New System.Drawing.Size(27, 23)
        Me.btnPagoAdua.TabIndex = 43
        Me.btnPagoAdua.Text = "+"
        Me.btnPagoAdua.ToolTip = "Agregar un nuevo Departamento"
        '
        'btnPagoPol
        '
        Me.btnPagoPol.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnPagoPol.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnPagoPol.Appearance.Options.UseFont = true
        Me.btnPagoPol.Appearance.Options.UseForeColor = true
        Me.btnPagoPol.Location = New System.Drawing.Point(264, 23)
        Me.btnPagoPol.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPagoPol.Name = "btnPagoPol"
        Me.btnPagoPol.Size = New System.Drawing.Size(27, 23)
        Me.btnPagoPol.TabIndex = 42
        Me.btnPagoPol.Text = "+"
        Me.btnPagoPol.ToolTip = "Agregar un nuevo Departamento"
        '
        'Label27
        '
        Me.Label27.AutoSize = true
        Me.Label27.Location = New System.Drawing.Point(16, 111)
        Me.Label27.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(82, 17)
        Me.Label27.TabIndex = 41
        Me.Label27.Text = "Transporte:"
        '
        'txtAduana
        '
        Me.txtAduana.BackColor = System.Drawing.Color.White
        Me.txtAduana.Cursor = System.Windows.Forms.Cursors.No
        Me.txtAduana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtAduana.Location = New System.Drawing.Point(101, 63)
        Me.txtAduana.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAduana.Name = "txtAduana"
        Me.txtAduana.ReadOnly = true
        Me.txtAduana.Size = New System.Drawing.Size(155, 26)
        Me.txtAduana.TabIndex = 38
        Me.txtAduana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label26
        '
        Me.Label26.AutoSize = true
        Me.Label26.Location = New System.Drawing.Point(16, 69)
        Me.Label26.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(61, 17)
        Me.Label26.TabIndex = 39
        Me.Label26.Text = "Aduana:"
        '
        'txtIVAPoliza
        '
        Me.txtIVAPoliza.BackColor = System.Drawing.Color.White
        Me.txtIVAPoliza.Cursor = System.Windows.Forms.Cursors.No
        Me.txtIVAPoliza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtIVAPoliza.Location = New System.Drawing.Point(101, 22)
        Me.txtIVAPoliza.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIVAPoliza.Name = "txtIVAPoliza"
        Me.txtIVAPoliza.ReadOnly = true
        Me.txtIVAPoliza.Size = New System.Drawing.Size(155, 26)
        Me.txtIVAPoliza.TabIndex = 0
        Me.txtIVAPoliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = true
        Me.Label16.Location = New System.Drawing.Point(16, 28)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(50, 17)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "Poliza:"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.btnOtroPago)
        Me.GroupBox6.Controls.Add(Me.txtOAduana)
        Me.GroupBox6.Controls.Add(Me.Label3)
        Me.GroupBox6.Controls.Add(Me.txtOTransporte)
        Me.GroupBox6.Controls.Add(Me.Label21)
        Me.GroupBox6.Controls.Add(Me.txtOtroPago)
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Location = New System.Drawing.Point(1227, 11)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox6.Size = New System.Drawing.Size(301, 143)
        Me.GroupBox6.TabIndex = 47
        Me.GroupBox6.TabStop = false
        Me.GroupBox6.Text = "Otros Pagos"
        '
        'btnOtroPago
        '
        Me.btnOtroPago.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnOtroPago.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btnOtroPago.Appearance.Options.UseFont = true
        Me.btnOtroPago.Appearance.Options.UseForeColor = true
        Me.btnOtroPago.Location = New System.Drawing.Point(265, 23)
        Me.btnOtroPago.Margin = New System.Windows.Forms.Padding(4)
        Me.btnOtroPago.Name = "btnOtroPago"
        Me.btnOtroPago.Size = New System.Drawing.Size(27, 23)
        Me.btnOtroPago.TabIndex = 46
        Me.btnOtroPago.Text = "+"
        '
        'txtOAduana
        '
        Me.txtOAduana.BackColor = System.Drawing.Color.White
        Me.txtOAduana.Cursor = System.Windows.Forms.Cursors.No
        Me.txtOAduana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtOAduana.Location = New System.Drawing.Point(101, 63)
        Me.txtOAduana.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOAduana.Name = "txtOAduana"
        Me.txtOAduana.ReadOnly = true
        Me.txtOAduana.Size = New System.Drawing.Size(155, 26)
        Me.txtOAduana.TabIndex = 1
        Me.txtOAduana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(17, 70)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 17)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Aduana:"
        '
        'txtOTransporte
        '
        Me.txtOTransporte.BackColor = System.Drawing.Color.White
        Me.txtOTransporte.Cursor = System.Windows.Forms.Cursors.No
        Me.txtOTransporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtOTransporte.Location = New System.Drawing.Point(101, 103)
        Me.txtOTransporte.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOTransporte.Name = "txtOTransporte"
        Me.txtOTransporte.ReadOnly = true
        Me.txtOTransporte.Size = New System.Drawing.Size(155, 26)
        Me.txtOTransporte.TabIndex = 2
        Me.txtOTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = true
        Me.Label21.Location = New System.Drawing.Point(17, 111)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(82, 17)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "Transporte:"
        '
        'txtOtroPago
        '
        Me.txtOtroPago.BackColor = System.Drawing.Color.White
        Me.txtOtroPago.Cursor = System.Windows.Forms.Cursors.No
        Me.txtOtroPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtOtroPago.Location = New System.Drawing.Point(101, 22)
        Me.txtOtroPago.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOtroPago.Name = "txtOtroPago"
        Me.txtOtroPago.ReadOnly = true
        Me.txtOtroPago.Size = New System.Drawing.Size(155, 26)
        Me.txtOtroPago.TabIndex = 0
        Me.txtOtroPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = true
        Me.Label19.Location = New System.Drawing.Point(17, 30)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(47, 17)
        Me.Label19.TabIndex = 43
        Me.Label19.Text = "Otros:"
        '
        'txtNumFacturas
        '
        Me.txtNumFacturas.BackColor = System.Drawing.Color.White
        Me.txtNumFacturas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumFacturas.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtNumFacturas.Location = New System.Drawing.Point(141, 91)
        Me.txtNumFacturas.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNumFacturas.MaxLength = 50
        Me.txtNumFacturas.Name = "txtNumFacturas"
        Me.txtNumFacturas.Size = New System.Drawing.Size(333, 27)
        Me.txtNumFacturas.TabIndex = 4
        Me.txtNumFacturas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Location = New System.Drawing.Point(13, 97)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 17)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Facturas Compra:"
        '
        'Fecha_Vencimiento
        '
        Me.Fecha_Vencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_Vencimiento.Location = New System.Drawing.Point(346, 58)
        Me.Fecha_Vencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_Vencimiento.Name = "Fecha_Vencimiento"
        Me.Fecha_Vencimiento.Size = New System.Drawing.Size(128, 22)
        Me.Fecha_Vencimiento.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Location = New System.Drawing.Point(294, 62)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 17)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Vence:"
        '
        'deFechaVenc
        '
        Me.deFechaVenc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaVenc.Location = New System.Drawing.Point(346, 16)
        Me.deFechaVenc.Margin = New System.Windows.Forms.Padding(4)
        Me.deFechaVenc.Name = "deFechaVenc"
        Me.deFechaVenc.Size = New System.Drawing.Size(128, 22)
        Me.deFechaVenc.TabIndex = 2
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(127, 236)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multiline = true
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(939, 62)
        Me.txtObservacion.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(15, 239)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 17)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Observaciones:"
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(294, 20)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 17)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Fecha:"
        '
        'txtNumDocumento
        '
        Me.txtNumDocumento.BackColor = System.Drawing.Color.White
        Me.txtNumDocumento.Enabled = false
        Me.txtNumDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtNumDocumento.Location = New System.Drawing.Point(141, 14)
        Me.txtNumDocumento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNumDocumento.Name = "txtNumDocumento"
        Me.txtNumDocumento.Size = New System.Drawing.Size(144, 26)
        Me.txtNumDocumento.TabIndex = 0
        Me.txtNumDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(13, 20)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "N° Documento:"
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.txtPesoKgms)
        Me.Panel5.Controls.Add(Me.Label9)
        Me.Panel5.Controls.Add(Me.txtImpuestos)
        Me.Panel5.Controls.Add(Me.Label22)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.txtCIFUSS)
        Me.Panel5.Controls.Add(Me.txtCostoTotal)
        Me.Panel5.Controls.Add(Me.Label23)
        Me.Panel5.Controls.Add(Me.Label24)
        Me.Panel5.Controls.Add(Me.txtCIFCS)
        Me.Panel5.Location = New System.Drawing.Point(9, 36)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(253, 187)
        Me.Panel5.TabIndex = 80
        '
        'txtPesoKgms
        '
        Me.txtPesoKgms.BackColor = System.Drawing.Color.White
        Me.txtPesoKgms.Cursor = System.Windows.Forms.Cursors.No
        Me.txtPesoKgms.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtPesoKgms.Location = New System.Drawing.Point(104, 11)
        Me.txtPesoKgms.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPesoKgms.Name = "txtPesoKgms"
        Me.txtPesoKgms.ReadOnly = true
        Me.txtPesoKgms.Size = New System.Drawing.Size(136, 26)
        Me.txtPesoKgms.TabIndex = 0
        Me.txtPesoKgms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Location = New System.Drawing.Point(11, 18)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 17)
        Me.Label9.TabIndex = 45
        Me.Label9.Text = "Peso Neto:"
        '
        'txtImpuestos
        '
        Me.txtImpuestos.BackColor = System.Drawing.Color.White
        Me.txtImpuestos.Cursor = System.Windows.Forms.Cursors.No
        Me.txtImpuestos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtImpuestos.Location = New System.Drawing.Point(104, 114)
        Me.txtImpuestos.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImpuestos.Name = "txtImpuestos"
        Me.txtImpuestos.ReadOnly = true
        Me.txtImpuestos.Size = New System.Drawing.Size(136, 26)
        Me.txtImpuestos.TabIndex = 52
        Me.txtImpuestos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label22
        '
        Me.Label22.AutoSize = true
        Me.Label22.Location = New System.Drawing.Point(11, 52)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(63, 17)
        Me.Label22.TabIndex = 47
        Me.Label22.Text = "CIF US$:"
        '
        'Label17
        '
        Me.Label17.AutoSize = true
        Me.Label17.Location = New System.Drawing.Point(11, 121)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(76, 17)
        Me.Label17.TabIndex = 53
        Me.Label17.Text = "Impuestos:"
        '
        'txtCIFUSS
        '
        Me.txtCIFUSS.BackColor = System.Drawing.Color.White
        Me.txtCIFUSS.Cursor = System.Windows.Forms.Cursors.No
        Me.txtCIFUSS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtCIFUSS.Location = New System.Drawing.Point(104, 46)
        Me.txtCIFUSS.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCIFUSS.Name = "txtCIFUSS"
        Me.txtCIFUSS.ReadOnly = true
        Me.txtCIFUSS.Size = New System.Drawing.Size(136, 26)
        Me.txtCIFUSS.TabIndex = 1
        Me.txtCIFUSS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCostoTotal
        '
        Me.txtCostoTotal.BackColor = System.Drawing.Color.White
        Me.txtCostoTotal.Cursor = System.Windows.Forms.Cursors.No
        Me.txtCostoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtCostoTotal.Location = New System.Drawing.Point(104, 148)
        Me.txtCostoTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCostoTotal.Name = "txtCostoTotal"
        Me.txtCostoTotal.ReadOnly = true
        Me.txtCostoTotal.Size = New System.Drawing.Size(136, 26)
        Me.txtCostoTotal.TabIndex = 3
        Me.txtCostoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.AutoSize = true
        Me.Label23.Location = New System.Drawing.Point(11, 87)
        Me.Label23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(53, 17)
        Me.Label23.TabIndex = 49
        Me.Label23.Text = "CIF C$:"
        '
        'Label24
        '
        Me.Label24.AutoSize = true
        Me.Label24.Location = New System.Drawing.Point(11, 154)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(84, 17)
        Me.Label24.TabIndex = 51
        Me.Label24.Text = "Costo Total:"
        '
        'txtCIFCS
        '
        Me.txtCIFCS.BackColor = System.Drawing.Color.White
        Me.txtCIFCS.Cursor = System.Windows.Forms.Cursors.No
        Me.txtCIFCS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtCIFCS.Location = New System.Drawing.Point(104, 80)
        Me.txtCIFCS.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCIFCS.Name = "txtCIFCS"
        Me.txtCIFCS.ReadOnly = true
        Me.txtCIFCS.Size = New System.Drawing.Size(136, 26)
        Me.txtCIFCS.TabIndex = 2
        Me.txtCIFCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEstado
        '
        Me.lblEstado.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblEstado.Location = New System.Drawing.Point(9, 4)
        Me.lblEstado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(252, 25)
        Me.lblEstado.TabIndex = 81
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lblEstado)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel4.Location = New System.Drawing.Point(1655, 337)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(269, 413)
        Me.Panel4.TabIndex = 3
        '
        'grvDetalle
        '
        Me.grvDetalle.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvDetalle.GridControl = Me.grdDetalle
        Me.grvDetalle.GroupFormat = ""
        Me.grvDetalle.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvDetalle.Name = "grvDetalle"
        Me.grvDetalle.OptionsBehavior.Editable = false
        Me.grvDetalle.OptionsBehavior.ReadOnly = true
        Me.grvDetalle.OptionsCustomization.AllowColumnMoving = false
        Me.grvDetalle.OptionsCustomization.AllowColumnResizing = false
        Me.grvDetalle.OptionsCustomization.AllowGroup = false
        Me.grvDetalle.OptionsMenu.EnableGroupPanelMenu = false
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedCell = false
        Me.grvDetalle.OptionsView.ColumnAutoWidth = false
        Me.grvDetalle.OptionsView.ShowFooter = true
        Me.grvDetalle.OptionsView.ShowGroupPanel = false
        '
        'grdDetalle
        '
        Me.grdDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDetalle.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.grdDetalle.Location = New System.Drawing.Point(0, 337)
        Me.grdDetalle.MainView = Me.grvDetalle
        Me.grdDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(1655, 349)
        Me.grdDetalle.TabIndex = 2
        Me.grdDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalle})
        '
        'frmMasterCompraForanea
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1924, 750)
        Me.Controls.Add(Me.grdDetalle)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmMasterCompraForanea"
        Me.Text = "Compras Foranes"
        Me.Panel3.ResumeLayout(false)
        Me.Panel1.ResumeLayout(false)
        Me.GroupBox1.ResumeLayout(false)
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        CType(Me.luProveedor.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox5.ResumeLayout(false)
        Me.GroupBox5.PerformLayout
        Me.GroupBox7.ResumeLayout(false)
        Me.GroupBox7.PerformLayout
        Me.grbTransporte.ResumeLayout(false)
        Me.grbTransporte.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        Me.GroupBox4.ResumeLayout(false)
        Me.GroupBox4.PerformLayout
        Me.GroupBox3.ResumeLayout(false)
        Me.GroupBox3.PerformLayout
        Me.GroupBox6.ResumeLayout(false)
        Me.GroupBox6.PerformLayout
        CType(Me.TblProvBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblDetalleDocBS,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TblMasterDocBS,System.ComponentModel.ISupportInitialize).EndInit
        Me.Panel5.ResumeLayout(false)
        Me.Panel5.PerformLayout
        Me.Panel4.ResumeLayout(false)
        CType(Me.grvDetalle,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.grdDetalle,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TblProvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDetalleDocBS As System.Windows.Forms.BindingSource
    Friend WithEvents TblMasterDocBS As System.Windows.Forms.BindingSource
    Friend WithEvents btnNacionalizar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rbDolares As System.Windows.Forms.RadioButton
    Friend WithEvents btnTasaCambio As System.Windows.Forms.Button
    Friend WithEvents rbCordobas As System.Windows.Forms.RadioButton
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtTasaCambio As System.Windows.Forms.TextBox
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents txtGastosExport As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents grbTransporte As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtOtros As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtSeguro As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtFletes As System.Windows.Forms.TextBox
    Friend WithEvents txtFOB As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rbContado As System.Windows.Forms.RadioButton
    Friend WithEvents txtPlazo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSSA As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtTSI As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSPE As System.Windows.Forms.TextBox
    Friend WithEvents txtISC As System.Windows.Forms.TextBox
    Friend WithEvents txtDAI As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFletesTerrestre As System.Windows.Forms.TextBox
    Friend WithEvents btnPagoTrans As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPagoAdua As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPagoPol As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtAduana As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtIVAPoliza As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents btnOtroPago As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtOAduana As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtOTransporte As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtOtroPago As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtPesoKgms As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtImpuestos As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCIFUSS As System.Windows.Forms.TextBox
    Friend WithEvents txtCostoTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtCIFCS As System.Windows.Forms.TextBox
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents grvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents grdDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents deRecepcionado As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtNumFacturas As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Fecha_Vencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents deFechaVenc As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents luProveedor As DevExpress.XtraEditors.LookUpEdit
End Class
