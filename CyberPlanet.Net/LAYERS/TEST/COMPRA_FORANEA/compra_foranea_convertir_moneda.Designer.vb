﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class compra_foranea_convertir_moneda
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.pBody = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_monto = New DevExpress.XtraEditors.TextEdit()
        Me.txt_capital = New DevExpress.XtraEditors.TextEdit()
        Me.picker_fecha = New DevExpress.XtraEditors.DateEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_tasa_de_cambio = New DevExpress.XtraEditors.TextEdit()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Linea_up = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pButton = New System.Windows.Forms.Panel()
        Me.btn_cancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_aceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.pMain.SuspendLayout()
        Me.pBody.SuspendLayout()
        CType(Me.txt_monto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_capital.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picker_fecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picker_fecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_tasa_de_cambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pButton.SuspendLayout()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.pBody)
        Me.pMain.Controls.Add(Me.pButton)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(451, 353)
        Me.pMain.TabIndex = 0
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.Label4)
        Me.pBody.Controls.Add(Me.txt_monto)
        Me.pBody.Controls.Add(Me.txt_capital)
        Me.pBody.Controls.Add(Me.picker_fecha)
        Me.pBody.Controls.Add(Me.Label3)
        Me.pBody.Controls.Add(Me.txt_tasa_de_cambio)
        Me.pBody.Controls.Add(Me.Label54)
        Me.pBody.Controls.Add(Me.Linea_up)
        Me.pBody.Controls.Add(Me.Label2)
        Me.pBody.Controls.Add(Me.Label1)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 0)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(451, 289)
        Me.pBody.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(92, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 17)
        Me.Label4.TabIndex = 123
        Me.Label4.Text = "Monto (C$):"
        '
        'txt_monto
        '
        Me.txt_monto.Enabled = False
        Me.txt_monto.Location = New System.Drawing.Point(183, 195)
        Me.txt_monto.Name = "txt_monto"
        Me.txt_monto.Properties.Appearance.BackColor = System.Drawing.Color.SeaShell
        Me.txt_monto.Properties.Appearance.Options.UseBackColor = True
        Me.txt_monto.Size = New System.Drawing.Size(204, 22)
        Me.txt_monto.TabIndex = 122
        '
        'txt_capital
        '
        Me.txt_capital.Location = New System.Drawing.Point(183, 158)
        Me.txt_capital.Name = "txt_capital"
        Me.txt_capital.Properties.MaxLength = 10
        Me.txt_capital.Size = New System.Drawing.Size(204, 22)
        Me.txt_capital.TabIndex = 121
        '
        'picker_fecha
        '
        Me.picker_fecha.EditValue = Nothing
        Me.picker_fecha.Location = New System.Drawing.Point(183, 84)
        Me.picker_fecha.Name = "picker_fecha"
        Me.picker_fecha.Properties.Appearance.BackColor = System.Drawing.Color.SeaShell
        Me.picker_fecha.Properties.Appearance.Options.UseBackColor = True
        Me.picker_fecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.picker_fecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.picker_fecha.Size = New System.Drawing.Size(204, 23)
        Me.picker_fecha.TabIndex = 120
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(97, 161)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 17)
        Me.Label3.TabIndex = 119
        Me.Label3.Text = "Capital ($):"
        '
        'txt_tasa_de_cambio
        '
        Me.txt_tasa_de_cambio.Enabled = False
        Me.txt_tasa_de_cambio.Location = New System.Drawing.Point(183, 119)
        Me.txt_tasa_de_cambio.Name = "txt_tasa_de_cambio"
        Me.txt_tasa_de_cambio.Properties.Appearance.BackColor = System.Drawing.Color.SeaShell
        Me.txt_tasa_de_cambio.Properties.Appearance.Options.UseBackColor = True
        Me.txt_tasa_de_cambio.Size = New System.Drawing.Size(204, 22)
        Me.txt_tasa_de_cambio.TabIndex = 118
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label54.Location = New System.Drawing.Point(26, 22)
        Me.Label54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(187, 17)
        Me.Label54.TabIndex = 117
        Me.Label54.Text = "DOLARES A CORDOBAS"
        '
        'Linea_up
        '
        Me.Linea_up.AutoSize = True
        Me.Linea_up.BackColor = System.Drawing.Color.Transparent
        Me.Linea_up.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Linea_up.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Linea_up.Location = New System.Drawing.Point(4, -18)
        Me.Linea_up.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Linea_up.Name = "Linea_up"
        Me.Linea_up.Size = New System.Drawing.Size(855, 54)
        Me.Linea_up.TabIndex = 116
        Me.Linea_up.Text = "________________________________"
        Me.Linea_up.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(151, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha tasa de cambio:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 122)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tasa de cambio:"
        '
        'pButton
        '
        Me.pButton.BackColor = System.Drawing.Color.Snow
        Me.pButton.Controls.Add(Me.btn_cancelar)
        Me.pButton.Controls.Add(Me.btn_aceptar)
        Me.pButton.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pButton.Location = New System.Drawing.Point(0, 289)
        Me.pButton.Name = "pButton"
        Me.pButton.Size = New System.Drawing.Size(451, 64)
        Me.pButton.TabIndex = 0
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.Location = New System.Drawing.Point(339, 18)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(100, 34)
        Me.btn_cancelar.TabIndex = 3
        Me.btn_cancelar.Text = "&Cancelar"
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.Location = New System.Drawing.Point(233, 18)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(100, 34)
        Me.btn_aceptar.TabIndex = 2
        Me.btn_aceptar.Text = "&Aceptar"
        '
        'compra_foranea_convertir_moneda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(451, 353)
        Me.ControlBox = False
        Me.Controls.Add(Me.pMain)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(469, 371)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(469, 371)
        Me.Name = "compra_foranea_convertir_moneda"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pMain.ResumeLayout(False)
        Me.pBody.ResumeLayout(False)
        Me.pBody.PerformLayout()
        CType(Me.txt_monto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_capital.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picker_fecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picker_fecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_tasa_de_cambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pButton.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pButton As System.Windows.Forms.Panel
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Linea_up As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_monto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_capital As DevExpress.XtraEditors.TextEdit
    Friend WithEvents picker_fecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_tasa_de_cambio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_cancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_aceptar As DevExpress.XtraEditors.SimpleButton
End Class
