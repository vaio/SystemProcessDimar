﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class compra_foranea_proveedor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.pDown = New System.Windows.Forms.Panel()
        Me.btn_cancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_aceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.pBody = New System.Windows.Forms.Panel()
        Me.txt_Observacion = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_Seguro = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Flete = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_Fob = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_Facturas = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmb_Proveedores = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Linea_up = New System.Windows.Forms.Label()
        Me.pMain.SuspendLayout()
        Me.pDown.SuspendLayout()
        Me.pBody.SuspendLayout()
        CType(Me.cmb_Proveedores.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.pDown)
        Me.pMain.Controls.Add(Me.pBody)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(508, 587)
        Me.pMain.TabIndex = 0
        '
        'pDown
        '
        Me.pDown.BackColor = System.Drawing.Color.Snow
        Me.pDown.Controls.Add(Me.btn_cancelar)
        Me.pDown.Controls.Add(Me.btn_aceptar)
        Me.pDown.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pDown.Location = New System.Drawing.Point(0, 519)
        Me.pDown.Name = "pDown"
        Me.pDown.Size = New System.Drawing.Size(508, 68)
        Me.pDown.TabIndex = 1
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.Location = New System.Drawing.Point(380, 22)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(100, 34)
        Me.btn_cancelar.TabIndex = 1
        Me.btn_cancelar.Text = "&Cancelar"
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aceptar.Location = New System.Drawing.Point(274, 22)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(100, 34)
        Me.btn_aceptar.TabIndex = 0
        Me.btn_aceptar.Text = "&Aceptar"
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.txt_Observacion)
        Me.pBody.Controls.Add(Me.Label4)
        Me.pBody.Controls.Add(Me.txt_Seguro)
        Me.pBody.Controls.Add(Me.Label3)
        Me.pBody.Controls.Add(Me.txt_Flete)
        Me.pBody.Controls.Add(Me.Label1)
        Me.pBody.Controls.Add(Me.txt_Fob)
        Me.pBody.Controls.Add(Me.Label16)
        Me.pBody.Controls.Add(Me.txt_Facturas)
        Me.pBody.Controls.Add(Me.Label5)
        Me.pBody.Controls.Add(Me.cmb_Proveedores)
        Me.pBody.Controls.Add(Me.Label2)
        Me.pBody.Controls.Add(Me.Label54)
        Me.pBody.Controls.Add(Me.Linea_up)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 0)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(508, 587)
        Me.pBody.TabIndex = 0
        '
        'txt_Observacion
        '
        Me.txt_Observacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Observacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Observacion.Location = New System.Drawing.Point(28, 422)
        Me.txt_Observacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Observacion.MaxLength = 200
        Me.txt_Observacion.Multiline = True
        Me.txt_Observacion.Name = "txt_Observacion"
        Me.txt_Observacion.Size = New System.Drawing.Size(452, 67)
        Me.txt_Observacion.TabIndex = 126
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label4.Location = New System.Drawing.Point(27, 401)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 17)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Observacion:"
        '
        'txt_Seguro
        '
        Me.txt_Seguro.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Seguro.BackColor = System.Drawing.Color.White
        Me.txt_Seguro.Cursor = System.Windows.Forms.Cursors.Default
        Me.txt_Seguro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Seguro.Location = New System.Drawing.Point(28, 363)
        Me.txt_Seguro.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Seguro.MaxLength = 15
        Me.txt_Seguro.Name = "txt_Seguro"
        Me.txt_Seguro.Size = New System.Drawing.Size(452, 26)
        Me.txt_Seguro.TabIndex = 124
        Me.txt_Seguro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label3.Location = New System.Drawing.Point(25, 342)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 17)
        Me.Label3.TabIndex = 125
        Me.Label3.Text = "Seguro:"
        '
        'txt_Flete
        '
        Me.txt_Flete.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Flete.BackColor = System.Drawing.Color.White
        Me.txt_Flete.Cursor = System.Windows.Forms.Cursors.Default
        Me.txt_Flete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Flete.Location = New System.Drawing.Point(28, 296)
        Me.txt_Flete.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Flete.MaxLength = 15
        Me.txt_Flete.Name = "txt_Flete"
        Me.txt_Flete.Size = New System.Drawing.Size(452, 26)
        Me.txt_Flete.TabIndex = 122
        Me.txt_Flete.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Location = New System.Drawing.Point(25, 275)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 17)
        Me.Label1.TabIndex = 123
        Me.Label1.Text = "Flete:"
        '
        'txt_Fob
        '
        Me.txt_Fob.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Fob.BackColor = System.Drawing.Color.White
        Me.txt_Fob.Cursor = System.Windows.Forms.Cursors.Default
        Me.txt_Fob.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Fob.Location = New System.Drawing.Point(28, 233)
        Me.txt_Fob.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Fob.MaxLength = 15
        Me.txt_Fob.Name = "txt_Fob"
        Me.txt_Fob.Size = New System.Drawing.Size(452, 26)
        Me.txt_Fob.TabIndex = 120
        Me.txt_Fob.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label16.Location = New System.Drawing.Point(25, 212)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(40, 17)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "FOB:"
        '
        'txt_Facturas
        '
        Me.txt_Facturas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Facturas.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Facturas.Location = New System.Drawing.Point(28, 131)
        Me.txt_Facturas.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Facturas.MaxLength = 50
        Me.txt_Facturas.Multiline = True
        Me.txt_Facturas.Name = "txt_Facturas"
        Me.txt_Facturas.Size = New System.Drawing.Size(452, 67)
        Me.txt_Facturas.TabIndex = 118
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label5.Location = New System.Drawing.Point(27, 110)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 17)
        Me.Label5.TabIndex = 119
        Me.Label5.Text = "No. Facturas:"
        '
        'cmb_Proveedores
        '
        Me.cmb_Proveedores.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmb_Proveedores.Location = New System.Drawing.Point(28, 70)
        Me.cmb_Proveedores.Margin = New System.Windows.Forms.Padding(4)
        Me.cmb_Proveedores.Name = "cmb_Proveedores"
        Me.cmb_Proveedores.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_Proveedores.Size = New System.Drawing.Size(452, 23)
        Me.cmb_Proveedores.TabIndex = 117
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label2.Location = New System.Drawing.Point(25, 49)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "Proveedor:"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label54.Location = New System.Drawing.Point(25, 14)
        Me.Label54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(163, 17)
        Me.Label54.TabIndex = 115
        Me.Label54.Text = "DATOS PROVEEDOR"
        '
        'Linea_up
        '
        Me.Linea_up.AutoSize = True
        Me.Linea_up.BackColor = System.Drawing.Color.Transparent
        Me.Linea_up.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Linea_up.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Linea_up.Location = New System.Drawing.Point(3, -26)
        Me.Linea_up.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Linea_up.Name = "Linea_up"
        Me.Linea_up.Size = New System.Drawing.Size(855, 54)
        Me.Linea_up.TabIndex = 114
        Me.Linea_up.Text = "________________________________"
        Me.Linea_up.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'compra_foranea_proveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(508, 587)
        Me.ControlBox = False
        Me.Controls.Add(Me.pMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(510, 589)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(510, 589)
        Me.Name = "compra_foranea_proveedor"
        Me.pMain.ResumeLayout(False)
        Me.pDown.ResumeLayout(False)
        Me.pBody.ResumeLayout(False)
        Me.pBody.PerformLayout()
        CType(Me.cmb_Proveedores.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents pDown As System.Windows.Forms.Panel
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents btn_aceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_cancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Linea_up As System.Windows.Forms.Label
    Friend WithEvents cmb_Proveedores As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_Facturas As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_Observacion As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_Seguro As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_Flete As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_Fob As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
End Class
