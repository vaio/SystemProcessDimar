﻿Imports DevExpress.XtraEditors.Controls

Public Class frmMasterCompraForanea
    Dim nTipoLiq As Integer  'Si es 0 se liquida por PESO y si es 1 se liquida por FOB .
    Public isNacionalizado As Boolean = False
    Public TipoPago As Integer = 0
    Dim CodMaxEntrada As String = ""
    Dim CodEntrada As String = ""

    Private Sub frmMasterCompraForanea_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        NumCatalogo = 22
        LimpiarCampos()
        CargarProveedor()
        'Panel2.Enabled = False
        '  CargarDetalleDoc(My.Settings.Sucursal, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
        DesactivarCampos(True)

    End Sub

    Sub LimpiarCampos()

        nTipoEdic = 1
        txtNumDocumento.Text = "00000000"
        txtTasaCambio.Text = TazaCambio()
        '   deFechaDoc.Value = Now.Date
        deFechaVenc.Value = Now.Date
        deRecepcionado.Value = Now.Date
        luProveedor.ItemIndex = 0
        rbContado.Checked = True
        txtPlazo.Text = 0
        txtNumFacturas.Text = ""



        txtFOB.Text = FormatNumber(0, 2)
        txtSSA.Text = FormatNumber(0, 2)
        txtTSI.Text = FormatNumber(0, 2)
        txtSPE.Text = FormatNumber(0, 2)
        txtISC.Text = FormatNumber(0, 2)
        txtDAI.Text = FormatNumber(0, 2)

        txtFletes.Text = FormatNumber(0, 2)
        txtSeguro.Text = FormatNumber(0, 2)
        txtOtros.Text = FormatNumber(0, 2)

        txtIVAPoliza.Text = FormatNumber(0, 2)
        txtAduana.Text = FormatNumber(0, 2)

        txtOtroPago.Text = FormatNumber(0, 2)
        txtOAduana.Text = FormatNumber(0, 2)
        txtOTransporte.Text = FormatNumber(0, 2)
        txtGastosExport.Text = FormatNumber(0, 2)
        txtFletesTerrestre.Text = FormatNumber(0, 2)
        txtObservacion.Text = ""

        txtPesoKgms.Text = FormatNumber(0, 2)
        txtCIFUSS.Text = FormatNumber(0, 2)
        txtCIFCS.Text = FormatNumber(0, 2)
        txtImpuestos.Text = FormatNumber(0, 2)
        txtCostoTotal.Text = FormatNumber(0, 2)
        grdDetalle.DataSource = Nothing
        lblEstado.Text = "SIN NACIONALIZAR"

    End Sub

    Sub CargarProveedor()

        TblProvBindingSource.DataSource = SQL("Select Codigo_Proveedor,Nombre_Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
        luProveedor.Properties.DataSource = TblProvBindingSource
        luProveedor.Properties.DisplayMember = "Nombre_Proveedor"
        luProveedor.Properties.ValueMember = "Codigo_Proveedor"
        luProveedor.ItemIndex = 0

    End Sub

    'Public Sub CargarDetalleDoc(ByVal nSucursal As Integer, ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)
    '    Dim strSqlDoc As String
    '    strSqlDoc = "Select * from Compras_Extranjeras where CodigoSucursal = " & nSucursal & " and Numero_de_Documento = '" & sNumDoc & "' and Tipo_Compra = " & nTipoCompra & " and Codigo_Proveedor = '" & sProveedor & "'"
    '    Dim tblCodProv As DataTable = SQL(strSqlDoc, "tblDatosMasterDoc", My.Settings.SolIndustrialCNX).Tables(0)

    '    If tblCodProv.Rows.Count > 0 Then
    '        txtNumDocumento.Text = tblCodProv.Rows(0).Item("Numero_de_Documento")
    '        luProveedor.EditValue = tblCodProv.Rows(0).Item("Codigo_Proveedor")
    '        '  deFechaDoc.Value = tblCodProv.Rows(0).Item("Fecha")
    '        deFechaVenc.Value = tblCodProv.Rows(0).Item("FechaVencimiento")
    '        deRecepcionado.Value = tblCodProv.Rows(0).Item("FechaRecepcion")
    '        txtPlazo.Text = tblCodProv.Rows(0).Item("Plazo")
    '        txtNumFacturas.Text = tblCodProv.Rows(0).Item("Num_FacturasForaneas")
    '        txtObservacion.Text = tblCodProv.Rows(0).Item("Observaciones")

    '        txtPesoKgms.Text = tblCodProv.Rows(0).Item("Total_PesoKGms")
    '        txtFOB.Text = tblCodProv.Rows(0).Item("Total_FOB")
    '        txtGastosExport.Text = tblCodProv.Rows(0).Item("Total_GastosExport")
    '        txtTasaCambio.Text = tblCodProv.Rows(0).Item("TazaCambio")
    '        txtTSI.Text = tblCodProv.Rows(0).Item("TSI")
    '        txtSPE.Text = tblCodProv.Rows(0).Item("SPE")
    '        txtDAI.Text = tblCodProv.Rows(0).Item("DAI")
    '        txtISC.Text = tblCodProv.Rows(0).Item("ISC")

    '        txtFletes.Text = tblCodProv.Rows(0).Item("Flete")

    '        txtOAduana.Text = tblCodProv.Rows(0).Item("Desaduanaje")
    '        txtOTransporte.Text = tblCodProv.Rows(0).Item("Descargue")
    '        txtOtroPago.Text = tblCodProv.Rows(0).Item("Otros_Gastos")

    '        txtSSA.Text = tblCodProv.Rows(0).Item("SSA")
    '        txtSeguro.Text = tblCodProv.Rows(0).Item("Seguro")
    '        txtOtros.Text = tblCodProv.Rows(0).Item("Otros")

    '        isNacionalizado = tblCodProv.Rows(0).Item("IsNacionalizado")

    '        If tblCodProv.Rows(0).Item("MonedaCordoba") = True Then
    '            rbCordobas.Checked = True
    '        Else
    '            rbDolares.Checked = True
    '        End If

    '        lblEstado.Text = "SIN NACIONALIZAR"

    '        If tblCodProv.Rows(0).Item("IsNacionalizado") = True Then
    '            lblEstado.Text = "COMPRA NACIONALIZADA"

    '            btnAgregar.Enabled = False
    '            btnNacionalizar.Enabled = False
    '            btnModificar.Enabled = False
    '            btnEliminar.Enabled = False
    '        End If

    '        btnPagoPol.Enabled = True
    '        btnPagoAdua.Enabled = True
    '        btnPagoTrans.Enabled = True
    '        btnOtroPago.Enabled = True

    '        CargarDetalleCompra(sNumDoc, nTipoCompra, sProveedor)

    '    Else

    '        btnPagoPol.Enabled = False
    '        btnPagoAdua.Enabled = False
    '        btnPagoTrans.Enabled = False
    '        btnOtroPago.Enabled = False

    '        cmdAceptar.Enabled = False
    '        LimpiarCampos()
    '        txtNumDocumento.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
    '        txtNumDocumento.Focus()
    '        btnAgregar.Enabled = False
    '        btnNacionalizar.Enabled = False
    '        btnModificar.Enabled = False
    '        btnEliminar.Enabled = False
    '    End If

    '    'PermitirBotonesEdicion(True)
    '    'DesactivarCampos(True)
    'End Sub

    Public Sub CargarDetalleCompra(ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)
        Dim strSqlDoc As String
        Dim FOB As Double = 0
        Dim FleSegOtros As Double = 0
        Dim CIFUSS As Double = 0
        Dim CIFCS As Double = 0
        Dim Impuestos As Double = 0
        Dim CostoTotal As Double = 0

        strSqlDoc = "SELECT Codigo, Producto, CodBodega, Recibidas, Medidas, [Costo FOB en US$], [Sub-Total en US$], [Flete $ Seguro], [Sub-Total CIF en US$],  " & _
                                    "[Sub-Total CIF en C$], [Gastos Introducción], [Impto. T.S.I.M], [Impto. Total C$], [Costo Total Cordobas], [Costo Unitario Cordobas], " & _
                                    "[Costo Unitario Dolares USA], [TOTAL Dolares USA] FROM vwComprasNacionalizadas Where "

        strSqlDoc = strSqlDoc & " CodigoSucursal = " & My.Settings.Sucursal & " and "
        strSqlDoc = strSqlDoc & " Numero_de_Documento = '" & sNumDoc & "' "
        strSqlDoc = strSqlDoc & " and Tipo_Compra = " & nTipoCompra & ""
        strSqlDoc = strSqlDoc & " and Codigo_Proveedor = '" & sProveedor & "'"

        'CargareEntrada(20)

        Dim tblDetalle As DataTable = SQL(strSqlDoc, "tblDetalleDoc", My.Settings.SolIndustrialCNX).Tables(0)
        grdDetalle.DataSource = tblDetalle
        grvDetalle.Columns("Codigo").Width = 80
        grvDetalle.Columns("Producto").Width = 250
        grvDetalle.Columns("CodBodega").Visible = False

        grvDetalle.Columns("Recibidas").Width = 80
        grvDetalle.Columns("Recibidas").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Recibidas").DisplayFormat.FormatString = "{0:#,###,##0.00}"

        grvDetalle.Columns("Medidas").Width = 80

        grvDetalle.Columns("Costo FOB en US$").Width = 120
        grvDetalle.Columns("Costo FOB en US$").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Costo FOB en US$").DisplayFormat.FormatString = "{0:#,###,##0.00}"

        grvDetalle.Columns("Sub-Total en US$").Width = 120
        grvDetalle.Columns("Sub-Total en US$").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Sub-Total en US$").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Sub-Total en US$").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Sub-Total en US$").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Flete $ Seguro").Width = 120
        grvDetalle.Columns("Flete $ Seguro").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Flete $ Seguro").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Flete $ Seguro").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Flete $ Seguro").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Sub-Total CIF en US$").Width = 120
        grvDetalle.Columns("Sub-Total CIF en US$").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Sub-Total CIF en US$").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Sub-Total CIF en US$").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Sub-Total CIF en US$").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Sub-Total CIF en C$").Width = 120
        grvDetalle.Columns("Sub-Total CIF en C$").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Sub-Total CIF en C$").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Sub-Total CIF en C$").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Sub-Total CIF en C$").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Gastos Introducción").Width = 120
        grvDetalle.Columns("Gastos Introducción").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Gastos Introducción").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Gastos Introducción").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Gastos Introducción").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Impto. T.S.I.M").Width = 120
        grvDetalle.Columns("Impto. T.S.I.M").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Impto. T.S.I.M").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Impto. T.S.I.M").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Impto. T.S.I.M").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Impto. Total C$").Width = 120
        grvDetalle.Columns("Impto. Total C$").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Impto. Total C$").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Impto. Total C$").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Impto. Total C$").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Costo Total Cordobas").Width = 120
        grvDetalle.Columns("Costo Total Cordobas").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Costo Total Cordobas").DisplayFormat.FormatString = "{0:#,###,##0.00}"
        grvDetalle.Columns("Costo Total Cordobas").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Costo Total Cordobas").SummaryItem.DisplayFormat = "{0:#,###,##0.00}"

        grvDetalle.Columns("Costo Unitario Cordobas").Width = 120
        grvDetalle.Columns("Costo Unitario Cordobas").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Costo Unitario Cordobas").DisplayFormat.FormatString = "{0:#,###,##0.00}"

        grvDetalle.Columns("Costo Unitario Dolares USA").Width = 120
        grvDetalle.Columns("Costo Unitario Dolares USA").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Costo Unitario Dolares USA").DisplayFormat.FormatString = "{0:#,###,##0.0000}"

        grvDetalle.Columns("TOTAL Dolares USA").Width = 120
        grvDetalle.Columns("TOTAL Dolares USA").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("TOTAL Dolares USA").DisplayFormat.FormatString = "{0:#,###,##0.0000}"
        grvDetalle.Columns("TOTAL Dolares USA").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("TOTAL Dolares USA").SummaryItem.DisplayFormat = "{0:#,###,##0.0000}"

        grvDetalle.OptionsView.ShowFooter = True

        If grvDetalle.RowCount > 0 Then
            For Each dr As DataRow In tblDetalle.Rows
                FOB = FOB + CDbl(dr.Item("Sub-Total en US$"))
                FleSegOtros = FleSegOtros + CDbl(dr.Item("Flete $ Seguro"))

                CIFUSS = CIFUSS + CDbl(dr.Item("Sub-Total CIF en US$"))
                CIFCS = CIFCS + CDbl(dr.Item("Sub-Total CIF en C$"))
                Impuestos = Impuestos + CDbl(dr.Item("Impto. Total C$"))
                CostoTotal = CostoTotal + CDbl(dr.Item("Costo Total Cordobas"))
            Next
        End If

        txtFOB.Text = FOB
        txtTotal.Text = (CDbl(FOB) + CDbl(FleSegOtros))
        txtCIFUSS.Text = CIFUSS
        txtCIFCS.Text = CIFCS
        txtImpuestos.Text = Impuestos
        txtCostoTotal.Text = CostoTotal

        cargarMontoPago(1020)
        cargarMontoPago(1021)
        cargarMontoPago(1022)
        cargarMontoPago(2020)

        txtGastosExport.Text = (CDbl(txtOtroPago.Text) + CDbl(txtOAduana.Text) + CDbl(txtOTransporte.Text))

        If Not lblEstado.Text = "COMPRA NACIONALIZADA" Then

            If grvDetalle.RowCount <= 0 Then
                btnAgregar.Enabled = True
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
                btnNacionalizar.Enabled = False
            Else
                btnAgregar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
                btnNacionalizar.Enabled = True
            End If

        End If

        cmdAceptar.Enabled = True
    End Sub

    Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        GroupBox2.Enabled = Not bIsActivo
        GroupBox5.Enabled = Not bIsActivo
        luProveedor.Enabled = Not bIsActivo
        ' deFechaDoc.Enabled = Not bIsActivo
        deFechaVenc.Enabled = Not bIsActivo
        deRecepcionado.Enabled = Not bIsActivo
        btnTasaCambio.Enabled = Not bIsActivo
        txtNumFacturas.Enabled = Not bIsActivo
        txtTSI.Enabled = Not bIsActivo
        txtSPE.Enabled = Not bIsActivo
        txtDAI.Enabled = Not bIsActivo
        txtISC.Enabled = Not bIsActivo
        txtFOB.Enabled = Not bIsActivo
        'txtGastosExport.Enabled = Not bIsActivo
        txtFletesTerrestre.Enabled = Not bIsActivo
        txtFletes.Enabled = Not bIsActivo
        txtIVAPoliza.Enabled = Not bIsActivo
        txtOtroPago.Enabled = Not bIsActivo
        txtOAduana.Enabled = Not bIsActivo
        txtOTransporte.Enabled = Not bIsActivo
        ' cmdAddProveedor.Enabled = Not bIsActivo
        btnPagoPol.Enabled = Not bIsActivo
        btnPagoAdua.Enabled = Not bIsActivo
        btnPagoTrans.Enabled = Not bIsActivo
        btnOtroPago.Enabled = Not bIsActivo
        txtSSA.Enabled = Not bIsActivo
        txtAduana.Enabled = Not bIsActivo
        txtSeguro.Enabled = Not bIsActivo
        txtOtros.Enabled = Not bIsActivo
        txtPlazo.ReadOnly = bIsActivo
        txtObservacion.ReadOnly = bIsActivo
        'cmbTipExoneracion.Enabled = Not bIsActivo
    End Sub

    Sub EncabezadosDetDoc()
        grvDetalle.Columns.Clear()
        grvDetalle.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "Codigo"
        gc0.Caption = "Codigo"
        gc0.FieldName = "Codigo"
        gc0.Width = 6
        grvDetalle.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        grvDetalle.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Descripcion"
        gc1.Caption = "Descripcion"
        gc1.FieldName = "Descripcion"
        gc1.Width = 60
        grvDetalle.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "Cantidad"
        gc2.Caption = "Cantidad"
        gc2.FieldName = "Cantidad"
        gc2.Width = 10
        grvDetalle.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "Precio_Unit"
        gc3.Caption = "Precio_Unit"
        gc3.FieldName = "Precio_Unit"
        gc3.DisplayFormat.FormatString = "{0:$0.00}"
        gc3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc3.Width = 10
        grvDetalle.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3

        grvDetalle.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"
        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "Monto_FOB"
        gc4.Caption = "Monto_FOB"
        gc4.FieldName = "Monto_FOB"
        gc4.DisplayFormat.FormatString = "{0:$0.00}"
        gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc4.Width = 10
        grvDetalle.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4
        grvDetalle.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
        gc5.Name = "Peso_Neto_Kgm"
        gc5.Caption = "Peso_Neto_Kgm"
        gc5.FieldName = "Peso_Neto_Kgm"
        gc5.DisplayFormat.FormatString = "{0:$0.00}"
        gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc5.Width = 10
        grvDetalle.Columns.Add(gc5)
        gc5.Visible = True
        gc5.VisibleIndex = 5
        grvDetalle.Columns(5).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

        grvDetalle.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"
        Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
        gc6.Name = "Gasto_Export"
        gc6.Caption = "Gasto_Export"
        gc6.FieldName = "Gasto_Export"
        gc6.DisplayFormat.FormatString = "{0:$0.00}"
        gc6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc6.Width = 10
        grvDetalle.Columns.Add(gc6)
        gc6.Visible = True
        gc6.VisibleIndex = 6
        grvDetalle.Columns(6).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"

        grvDetalle.Columns(7).SummaryItem.DisplayFormat = "{0:$0.00}"
        Dim gc7 As New DevExpress.XtraGrid.Columns.GridColumn
        gc7.Name = "Flete"
        gc7.Caption = "Flete"
        gc7.FieldName = "Flete"
        gc7.DisplayFormat.FormatString = "{0:$0.00}"
        gc7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc7.Width = 10
        grvDetalle.Columns.Add(gc7)
        gc7.Visible = True
        gc7.VisibleIndex = 7
        grvDetalle.Columns(7).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(7).SummaryItem.DisplayFormat = "{0:$0.00}"
        grvDetalle.Columns(7).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc8 As New DevExpress.XtraGrid.Columns.GridColumn
        gc8.Name = "CIF_USS"
        gc8.Caption = "CIF_USS"
        gc8.FieldName = "CIF_USS"
        gc8.DisplayFormat.FormatString = "{0:$0.00}"
        gc8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc8.Width = 10
        grvDetalle.Columns.Add(gc8)
        gc8.Visible = True
        gc8.VisibleIndex = 8
        grvDetalle.Columns(8).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(8).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc9 As New DevExpress.XtraGrid.Columns.GridColumn
        gc9.Name = "CIF_CS"
        gc9.Caption = "CIF_CS"
        gc9.FieldName = "CIF_CS"
        gc9.DisplayFormat.FormatString = "{0:$0.00}"
        gc9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc9.Width = 10
        grvDetalle.Columns.Add(gc9)
        gc9.Visible = True
        gc9.VisibleIndex = 9
        grvDetalle.Columns(9).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(9).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc10 As New DevExpress.XtraGrid.Columns.GridColumn
        gc10.Name = "Otros_Impuestos"
        gc10.Caption = "Otros_Impuestos"
        gc10.FieldName = "Otros_Impuestos"
        gc10.DisplayFormat.FormatString = "{0:$0.00}"
        gc10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc10.Width = 10
        grvDetalle.Columns.Add(gc10)
        gc10.Visible = True
        gc10.VisibleIndex = 10
        grvDetalle.Columns(10).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(10).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc11 As New DevExpress.XtraGrid.Columns.GridColumn
        gc11.Name = "Desaduanaje"
        gc11.Caption = "Desaduanaje"
        gc11.FieldName = "Desaduanaje"
        gc11.DisplayFormat.FormatString = "{0:$0.00}"
        gc11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc11.Width = 10
        grvDetalle.Columns.Add(gc11)
        gc11.Visible = True
        gc11.VisibleIndex = 11
        grvDetalle.Columns(11).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(11).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc12 As New DevExpress.XtraGrid.Columns.GridColumn
        gc12.Name = "Descargue"
        gc12.Caption = "Descargue"
        gc12.FieldName = "Descargue"
        gc12.DisplayFormat.FormatString = "{0:$0.00}"
        gc12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc12.Width = 10
        grvDetalle.Columns.Add(gc12)
        gc12.Visible = True
        gc12.VisibleIndex = 12
        grvDetalle.Columns(12).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(12).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc13 As New DevExpress.XtraGrid.Columns.GridColumn
        gc13.Name = "OtrosGastos"
        gc13.Caption = "OtrosGastos"
        gc13.FieldName = "OtrosGastos"
        gc13.DisplayFormat.FormatString = "{0:$0.00}"
        gc13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc13.Width = 10
        grvDetalle.Columns.Add(gc13)
        gc13.Visible = True
        gc13.VisibleIndex = 13
        grvDetalle.Columns(13).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(13).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc14 As New DevExpress.XtraGrid.Columns.GridColumn
        gc14.Name = "Costo_Total"
        gc14.Caption = "Costo_Total"
        gc14.FieldName = "Costo_Total"
        gc14.DisplayFormat.FormatString = "{0:$0.00}"
        gc14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc14.Width = 10
        grvDetalle.Columns.Add(gc14)
        gc14.Visible = True
        gc14.VisibleIndex = 14
        grvDetalle.Columns(14).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(14).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc15 As New DevExpress.XtraGrid.Columns.GridColumn
        gc15.Name = "Costo_Unit_CS"
        gc15.Caption = "Costo_Unit_CS"
        gc15.FieldName = "Costo_Unit_CS"
        gc15.DisplayFormat.FormatString = "{0:$0.00}"
        gc15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc15.Width = 10
        grvDetalle.Columns.Add(gc15)
        gc15.Visible = True
        gc15.VisibleIndex = 15
        grvDetalle.Columns(15).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(15).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc16 As New DevExpress.XtraGrid.Columns.GridColumn
        gc16.Name = "Costo_Unit_USS"
        gc16.Caption = "Costo_Unit_USS"
        gc16.FieldName = "Costo_Unit_USS"
        gc16.DisplayFormat.FormatString = "{0:$0.00}"
        gc16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc16.Width = 10
        grvDetalle.Columns.Add(gc16)
        gc16.Visible = True
        gc16.VisibleIndex = 16
        grvDetalle.Columns(16).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(16).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc17 As New DevExpress.XtraGrid.Columns.GridColumn
        gc17.Name = "Precio_Venta"
        gc17.Caption = "Precio_Venta"
        gc17.FieldName = "Precio_Venta"
        gc17.DisplayFormat.FormatString = "{0:$0.00}"
        gc17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc17.Width = 10
        grvDetalle.Columns.Add(gc17)
        gc17.Visible = True
        gc17.VisibleIndex = 17
        grvDetalle.Columns(17).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(17).SummaryItem.DisplayFormat = "{0:$0.00}"

        Dim gc18 As New DevExpress.XtraGrid.Columns.GridColumn
        gc18.Name = "Margen_Utilidad"
        gc18.Caption = "Margen_Utilidad"
        gc18.FieldName = "Margen_Utilidad"
        gc18.DisplayFormat.FormatString = "{0:$0.00}"
        gc18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc18.Width = 10
        grvDetalle.Columns.Add(gc18)
        gc18.Visible = True
        gc18.VisibleIndex = 18
        grvDetalle.Columns(18).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(18).SummaryItem.DisplayFormat = "{0:$0.00}"

    End Sub

    Public Function SigNumDoc()
        Dim strsql As String = ""
        strsql = "SELECT isnull(MAX(Numero_de_Documento)+1,1) AS Maximo FROM Compras_Extranjeras"
        Dim tblRegMax As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)
        SigNumDoc = tblRegMax.Rows(0).Item(0)
    End Function

    Public Function TazaCambio() As Double
        Dim strsql As String = ""
        strsql = "SELECT Tasa_Oficial FROM Tbl_TasasCambio WHERE Fecha = CONVERT(date, GETDATE())"
        Dim tblTaza As DataTable = SQL(strsql, "tblTaza", My.Settings.SolIndustrialCNX).Tables(0)

        If tblTaza.Rows.Count > 0 Then
            TazaCambio = tblTaza.Rows(0).Item(0)
        Else
            TazaCambio = 0
        End If
    End Function

    Sub cargarMontoPago(ByVal tipo_referencia As Integer)
        Dim strsql As String = ""
        strsql = "SELECT IVA, (SubTotal + Otros_Gastos + Multas_Recargos) As Monto_Total, Neto_Pago FROM Tbl_Pagos " & _
                 "WHERE Numero_Referencia = '" & txtNumDocumento.Text & "' and Tipo_Referencia = 20 and Tipo_Documento = " & tipo_referencia.ToString

        Dim tblDatosPago As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

        Select Case tipo_referencia
            Case 1020 'PAGO DE IVA
                If tblDatosPago.Rows.Count > 0 Then
                    txtIVAPoliza.Text = tblDatosPago.Rows(0).Item(0)
                Else
                    txtIVAPoliza.Text = "0.0000"
                End If
            Case 1021 'PAGO DE ADUANA
                If tblDatosPago.Rows.Count > 0 Then
                    txtAduana.Text = tblDatosPago.Rows(0).Item(0)
                    txtOAduana.Text = tblDatosPago.Rows(0).Item(1)
                Else
                    txtAduana.Text = "0.0000"
                End If
            Case 1022 'PAGO DE TRANSPORTE
                If tblDatosPago.Rows.Count > 0 Then
                    txtFletesTerrestre.Text = tblDatosPago.Rows(0).Item(0)
                    txtOTransporte.Text = tblDatosPago.Rows(0).Item(1)
                Else
                    txtFletesTerrestre.Text = "0.0000"
                End If
            Case 2020 'PAGO DE OTROS
                If tblDatosPago.Rows.Count > 0 Then
                    txtOtroPago.Text = tblDatosPago.Rows(0).Item(2)
                Else
                    txtOtroPago.Text = "0.0000"
                End If
        End Select

    End Sub

    Sub Nuevo()
        nTipoEdic = 1
        'Panel2.Enabled = True
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        LimpiarCampos()
        txtNumDocumento.Text = Microsoft.VisualBasic.Right("00000000" & SigNumDoc(), 8)
        txtTasaCambio.Text = TazaCambio()
        '  CargarDetalleDoc(My.Settings.Sucursal, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
    End Sub

    Sub Modificar()
        'Panel2.Enabled = True
        nTipoEdic = 2
        DesactivarCampos(False)
        PermitirBotonesEdicion(False)
        txtNumDocumento.Focus()
    End Sub

    Sub Eliminar()
        DesactivarCampos(True)
        PermitirBotonesEdicion(False)
    End Sub

    Sub Guardar()
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        GuardarMasterDoc()
    End Sub

    'Sub CargareEntrada(ByVal tipo As String)
    '    Dim strsql As String = "SELECT Codigo_Movimiento FROM Movimientos_Almacenes WHERE Tipo_Movimiento = 4 and Codigo_Documento = " & txtNumDocumento.Text & " AND Tipo_Documento = " & tipo
    '    Dim TblDatos As DataTable = SQL(strsql, "tblEntrada", My.Settings.SolIndustrialCNX).Tables(0)
    '    If TblDatos.Rows.Count <> 0 Then
    '        txtCodEntrada.Text = TblDatos.Rows(0).Item(0)
    '    Else
    '        txtCodEntrada.Text = 0
    '    End If
    'End Sub

    Sub GuardarMasterDoc()
        Dim Resum As String = ""
        Dim ResumEntrada As String = ""
        Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try
            'If nTipoEdic = 1 Then
            '    isNacionalizado = False
            'End If

            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            'If result = DialogResult.Yes Then
            '    Resum = MasterDocument.EditarMasterCompraForanea(My.Settings.Sucursal, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2),
            '                                                        luProveedor.EditValue, deFechaDoc.Value, deFechaVenc.Value, deRecepcionado.Value,
            '                                                        CInt(txtPlazo.Text), txtNumFacturas.Text, txtObservacion.Text.ToUpper,
            '                                                        IIf(txtPesoKgms.Text = "", 0, txtPesoKgms.Text),
            '                                                        CDbl(IIf(txtFOB.Text = "", 0, txtFOB.Text)),
            '                                                        CDbl(IIf(txtGastosExport.Text = "", 0, txtGastosExport.Text)),
            '                                                        CDbl(IIf(txtFletesTerrestre.Text = "", 0, txtFletesTerrestre.Text)),
            '                                                        CDbl(IIf(txtTSI.Text = "", 0, txtTSI.Text)),
            '                                                        CDbl(IIf(txtSPE.Text = "", 0, txtSPE.Text)),
            '                                                        CDbl(IIf(txtDAI.Text = "", 0, txtDAI.Text)),
            '                                                        CDbl(IIf(txtISC.Text = "", 0, txtISC.Text)),
            '                                                        CDbl(IIf(txtFletes.Text = "", 0, txtFletes.Text)),
            '                                                        CDbl(IIf(txtIVAPoliza.Text = "", 0, txtIVAPoliza.Text)),
            '                                                        CDbl(IIf(txtOtroPago.Text = "", 0, txtOtroPago.Text)),
            '                                                        CDbl(IIf(txtOAduana.Text = "", 0, txtOAduana.Text)),
            '                                                        CDbl(IIf(txtOTransporte.Text = "", 0, txtOTransporte.Text)),
            '                                                        isNacionalizado, CDbl(txtSSA.Text), CDbl(txtSeguro.Text),
            '                                                        CDbl(txtOtros.Text), IIf(rbCordobas.Checked = True, True, False),
            '                                                        CDbl(IIf(txtTasaCambio.Text = "", 0, txtTasaCambio.Text)), nTipoEdic)

            '    If Resum = "OK" Then
            '        MsgBox("La operación se realizó de forma exitosa", MsgBoxStyle.Information, "Información")

            If nTipoEdic = 3 Then
                LimpiarCampos()
            End If

            PermitirBotonesEdicion(True)
            DesactivarCampos(True)
            '  CargarDetalleDoc(1, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)

            'nTipoEdic = 1
            '    Else
            'MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            '    End If

            'If nTipoEdic = 1 Then
            '    CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(2, 4), 8)
            '    ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, 1, txtNumDocumento.Text, 20, ("ENTRADA A BODEGA DE MATERIA PRIMA"), 0, Date.Now, 4, False, 1, 2)
            'End If
            '  End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            If NumCatalogo_Ant = 0 Then
            End If
        End Try
    End Sub

    Function Cancelar()
        nTipoEdic = 1
        NumCatalogo = 22
        ' CargarDetalleDoc(My.Settings.Sucursal, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
        'LimpiarCampos()
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        If grvDetalle.RowCount <= 0 Then
            frmPrincipal.bbiEliminar.Enabled = True
        Else
            frmPrincipal.bbiEliminar.Enabled = False
        End If
    End Function

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Sub ocultaCompraDatos()
        frmDetalleCompra.Label8.Visible = False
        frmDetalleCompra.deFechaExp.Visible = False
        frmDetalleCompra.txtPorcentaje.Visible = False
        frmDetalleCompra.txtPorcenDescuento.Visible = False
        frmDetalleCompra.Label10.Visible = False
        frmDetalleCompra.txtDescuento.Visible = False
    End Sub

    Private Sub luProveedor_EditValueChanged(sender As Object, e As EventArgs) Handles luProveedor.EditValueChanged
        CargarDatosProveedor(luProveedor.EditValue)
    End Sub

    Public Sub CargarDatosProveedor(ByVal CodigoProv As String)
        Dim strsqlDatProv As String = ""

        strsqlDatProv = "SELECT Plazo,Id_TipoLiquidacion FROM Tbl_Proveedor WHERE Codigo_Proveedor='" & CodigoProv & "'"
        Dim tblDatosProv As DataTable = SQL(strsqlDatProv, "tblDatosProv", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosProv.Rows.Count <> 0 Then
            'txtPlazo.Text = IIf(IsDBNull(tblDatosProv.Rows(0).Item(0)), 0, tblDatosProv.Rows(0).Item(0))
            'deFechaVenc.Value = DateAdd(DateInterval.Day, CInt(txtPlazo.Text), deFechaDoc.Value)
            'nTipoLiq = tblDatosProv.Rows(0).Item(1)
        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdicDet = 1
        ocultaCompraDatos()
        frmDetalleCompra.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdicDet = 2
        ocultaCompraDatos()
        frmDetalleCompra.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        nTipoEdicDet = 3
        ocultaCompraDatos()
        frmDetalleCompra.ShowDialog()
    End Sub

    'Private Sub deFechaDoc_ValueChanged(sender As Object, e As EventArgs) Handles deFechaDoc.ValueChanged
    '    CargarDatosProveedor(luProveedor.EditValue)
    'End Sub

    'Private Sub txtPlazo_TextChanged(sender As Object, e As EventArgs) Handles txtPlazo.TextChanged
    '    deFechaVenc.Value = DateAdd(DateInterval.Day, CInt(txtPlazo.Text), deFechaDoc.Value)
    'End Sub

    Private Sub txtIVAPoliza_DoubleClick(sender As Object, e As EventArgs) Handles txtIVAPoliza.DoubleClick
        'frmOrdenesPago()
    End Sub

    'Private Sub cmdAddProveedor_Click(sender As Object, e As EventArgs) Handles cmdAddProveedor.Click
    '    itemAdd_Ant = luProveedor.EditValue
    '    nTipoEdic_Ant = nTipoEdic
    '    NumCatalogo_Ant = NumCatalogo
    '    nTipoEdic = 1
    '    NumCatalogo = 6
    '    frmCatalogo.AbrirEditorCatalogos()
    '    nTipoEdic = nTipoEdic_Ant
    '    NumCatalogo = NumCatalogo_Ant
    '    nTipoEdic_Ant = 0
    '    NumCatalogo_Ant = 0
    '    CargarProveedor()
    'End Sub

    Private Sub btnNacionalizar_Click(sender As Object, e As EventArgs) Handles btnNacionalizar.Click
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim CodigoProd As String = ""
        Dim Cantidad As Integer = 0
        Dim Bodega As Integer = 0
        Dim Costo As Double = 0
        Dim Resum As String = ""
        Dim ResumEntrada As String = ""
        Dim strsql As String = ""
        Dim Cod_bodega As Integer = 0

        Try
            Dim result As Integer = MessageBox.Show("Desea nacionalizar la compra?", "Información", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then

                If grvDetalle.RowCount > 0 Then
                    For i As Integer = 0 To grvDetalle.DataRowCount - 1
                        CodigoProd = grvDetalle.GetRowCellValue(i, "Codigo")
                        Cantidad = grvDetalle.GetRowCellValue(i, "Recibidas")
                        Costo = grvDetalle.GetRowCellValue(i, "Costo Unitario Cordobas")
                        Cod_bodega = grvDetalle.GetRowCellValue(i, "CodBodega")

                        strsql = "SELECT Codigo_Movimiento FROM Movimientos_Almacenes WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Bodega = " & Cod_bodega & " and Tipo_Movimiento = 4 and Codigo_Documento = " & txtNumDocumento.Text & " and Tipo_Documento = 20"
                        Dim tblCodigo As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

                        If tblCodigo.Rows.Count > 0 Then
                            CodMaxEntrada = tblCodigo.Rows(0).Item(0)
                        Else
                            CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(Cod_bodega, 4), 8)

                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, txtNumDocumento.Text, 20, ("ENTRADA A BODEGA DE MATERIA PRIMA"), 0, Date.Now, 4, False, 1, Cod_bodega)
                        End If

                        Resum = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, CodigoProd, Cantidad, Costo, txtNumFacturas.Text, Cod_bodega, 0, 1, 4)

                        'Resum = Inventario.EditarMovimientoAlmacenDetalle(txtCodEntrada.Text, My.Settings.Sucursal, CodigoProd, Cantidad, Costo, "", Bodega, False, 1, 4)
                    Next
                End If

                If Resum = "OK" Then
                    '     Resum = MasterDocument.EditarMasterCompraForanea(My.Settings.Sucursal, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue, deFechaDoc.Value, deFechaVenc.Value, deRecepcionado.Value, CInt(txtPlazo.Text), txtNumFacturas.Text, txtObservacion.Text.ToUpper, IIf(txtPesoKgms.Text = "", 0, txtPesoKgms.Text), CDbl(IIf(txtFOB.Text = "", 0, txtFOB.Text)), CDbl(IIf(txtGastosExport.Text = "", 0, txtGastosExport.Text)), CDbl(IIf(txtFletesTerrestre.Text = "", 0, txtFletesTerrestre.Text)), CDbl(IIf(txtTSI.Text = "", 0, txtTSI.Text)), CDbl(IIf(txtSPE.Text = "", 0, txtSPE.Text)), CDbl(IIf(txtDAI.Text = "", 0, txtDAI.Text)), CDbl(IIf(txtISC.Text = "", 0, txtISC.Text)), CDbl(IIf(txtFletes.Text = "", 0, txtFletes.Text)), CDbl(IIf(txtIVAPoliza.Text = "", 0, txtIVAPoliza.Text)), CDbl(IIf(txtOtroPago.Text = "", 0, txtOtroPago.Text)), CDbl(IIf(txtOAduana.Text = "", 0, txtOAduana.Text)), CDbl(IIf(txtOTransporte.Text = "", 0, txtOTransporte.Text)), True, CDbl(txtSSA.Text), CDbl(txtSeguro.Text), CDbl(txtOtros.Text), IIf(rbCordobas.Checked = True, True, False), CDbl(IIf(txtTasaCambio.Text = "", 0, txtTasaCambio.Text)), 2)
                End If

                MsgBox("Compra nacionlizada correctamente.", MsgBoxStyle.Information, "Información")
                '  CargarDetalleDoc(1, txtNumDocumento.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)

            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles btnPagoPol.Click
        TipoPago = 1020

        '  FrmPagos.ShowDialog()

        Dim Otros_Impuestos As Double = ChangeToDouble(txtSSA.Text)

        Otros_Impuestos += ChangeToDouble(txtTSI.Text)

        Otros_Impuestos += ChangeToDouble(txtSPE.Text)

        Otros_Impuestos += ChangeToDouble(txtISC.Text)

        Otros_Impuestos += ChangeToDouble(txtDAI.Text)

        '  Dim Set_IVA As New APLICAR_IVA(Otros_Impuestos, txtNumDocumento.Text)

        'Set_IVA.ShowDialog()

        'If Not ChangeToDouble(txtIVAPoliza.Text) = Set_IVA.get_iva And Not Set_IVA.get_iva = 0 Then txtIVAPoliza.Text = FormatNumber(Set_IVA.get_iva, 4)

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles btnPagoAdua.Click
        TipoPago = 1021
        FrmPagos.ShowDialog()
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles btnPagoTrans.Click
        TipoPago = 1022
        FrmPagos.ShowDialog()
    End Sub

    Private Sub cmdAceptar_Click_1(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        frmBusquedaDoc.Text = "Detalle de Pagos"
        frmBusquedaDoc.cmdAceptar.Text = "Editar"
        frmBusquedaDoc.Size = New Size(975, 388)
        frmBusquedaDoc.ShowDialog()
    End Sub

    Private Sub btnOtroPago_Click(sender As Object, e As EventArgs) Handles btnOtroPago.Click
        TipoPago = 2020
        FrmPagos.ShowDialog()
    End Sub

    Private Sub frmMasterCompraForanea_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 22
    End Sub

    Private Sub btnTasaCambio_Click(sender As Object, e As EventArgs) Handles btnTasaCambio.Click
        frmSearch.Text = "Taza de Cambio"
        frmSearch.Size = New Size(364, 324)
        frmSearch.ShowDialog()
    End Sub

End Class