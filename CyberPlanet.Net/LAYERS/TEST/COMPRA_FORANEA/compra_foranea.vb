﻿Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Controls
Imports SIGMA

Public Class compra_foranea

    Inherits Form

    Implements IMenu

    Private ValidarValoresNumericos As New Validar With {.CantDecimal = 4}

    Private vno_poliza As New Validar

    Private datos_de_proveedores As New DataTable

    Private curr_compra As compra

    Private documento_actual_aplicado As Boolean = False

    Private PORCENTAJE_TOTAL_POLIZA_CON_SIN_LIQUIDAR As Decimal
    Private PORCENTAJE_TOTAL_POLIZA_LIQUIDADA As Decimal
    Private PORCENTAJE_TOTAL_POLIZA_ACTUAL_LIQUIDADA As Decimal
    Private PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR As Decimal
    Private PORCENTAJE_TOTAL_POLIZA_RESTANTE_POR_LIQUIDAR As Decimal

    '' modificado 
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AddHandler Me.Load, AddressOf start

    End Sub
    Private Sub start()

        NumCatalogo = 22

        vno_poliza.agregar_control(New List(Of Control)({txt_no_poliza})).PermitirLetrasNumeros()

        ValidarValoresNumericos.agregar_control(New List(Of Control)({txtAduana,
                                                                      txtDAI,
                                                                      txtFletesTerrestre,
                                                                      txtISC,
                                                                      txtIVAPoliza,
                                                                      txtOtroPago,
                                                                      txtOAduana,
                                                                      txtOTransporte,
                                                                      txtSPE,
                                                                      txtSSA,
                                                                      txtTSI,
                                                                      txtAlmacen,
                                                                      txtOtroAlmacen
                                                                    })).ActivarEventosNumericos()

        AddHandler btn_SSA.Click, Sub(sender, e) dolar_a_cordoba("SSA")
        AddHandler btn_SPE.Click, Sub(sender, e) dolar_a_cordoba("SPE")
        AddHandler btn_TSI.Click, Sub(sender, e) dolar_a_cordoba("TSI")
        AddHandler btn_ISC.Click, Sub(sender, e) dolar_a_cordoba("ISC")
        AddHandler btn_DAI.Click, Sub(sender, e) dolar_a_cordoba("DAI")

        AddHandler Fecha_de_recepcion.ValueChanged, AddressOf get_tasa_de_cambio_por_fecha
        AddHandler Fecha_de_documento.ValueChanged, AddressOf validar_fecha_documento_compra
        AddHandler Fecha_de_vencimiento.ValueChanged, AddressOf validar_fecha_documento_vencimiento
        AddHandler txtPlazo.TextChanged, AddressOf validar_plazo_documento

        AddHandler btn_agregar_proveedores.Click, Sub(sender, e) Proveedor(1) ' agrega un proveedor 
        AddHandler btn_modificar_proveedor.Click, Sub(sender, e) Proveedor(2) ' actualizar un proveedor 
        AddHandler btn_remover_proveedor.Click, Sub(sender, e) Proveedor(3) ' eliminar un proveedor 

        AddHandler btn_Poliza.Click, Sub(sender, e) set_impuesto(2021)
        AddHandler btn_aduana.Click, Sub(sender, e) set_impuesto(1021)
        AddHandler btn_transporte.Click, Sub(sender, e) set_impuesto(1022)
        AddHandler btn_almacen.Click, Sub(sender, e) set_impuesto(1019)
        AddHandler btn_otropago.Click, Sub(sender, e) set_impuesto(2022)

        AddHandler btn_producto_agregar.Click, Sub(sender, e) compra_foranea_detalle_producto_gestion(1)
        AddHandler btn_producto_modificar.Click, Sub(sender, e) compra_foranea_detalle_producto_gestion(2)
        AddHandler btn_producto_eliminar.Click, Sub(sender, e) compra_foranea_detalle_producto_gestion(3)

        AddHandler txt_no_poliza.TextChanged, AddressOf verificar_poliza

        AddHandler btn_aplicar.Click, AddressOf aplicar

        get_tasa_de_cambio_por_fecha()

        set_tipo_de_calculo()

        curr_compra = New compra

    End Sub
    Private Sub get_compra_foranea_detalle()

        Tabla.DataSource = Nothing

        If curr_compra.get_numero_de_documento = String.Empty And curr_compra.get_numero_de_documento_poliza = String.Empty Then Exit Sub

        Tabla.DataSource = curr_compra.get_detalle

        Vista.Columns(0).Visible = False
        Vista.Columns(1).Visible = False

        Vista.BestFitColumns()

        PORCENTAJE_TOTAL_POLIZA_CON_SIN_LIQUIDAR = curr_compra.get_total_poliza_total
        PORCENTAJE_TOTAL_POLIZA_LIQUIDADA = curr_compra.get_total_poliza_aplicada
        PORCENTAJE_TOTAL_POLIZA_ACTUAL_LIQUIDADA = curr_compra.get_total_poliza_actual_aplicada
        PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR = curr_compra.get_total_poliza_actual_sin_aplicada
        PORCENTAJE_TOTAL_POLIZA_RESTANTE_POR_LIQUIDAR = (100 - PORCENTAJE_TOTAL_POLIZA_LIQUIDADA)

        porcentaje_poliza_completado_general.Text = PORCENTAJE_TOTAL_POLIZA_LIQUIDADA.ToString + " %"
        porcentaje_poliza_completado_actual.Text = PORCENTAJE_TOTAL_POLIZA_ACTUAL_LIQUIDADA.ToString + " %"
        porcentaje_poliza_completado_restante.Text = PORCENTAJE_TOTAL_POLIZA_RESTANTE_POR_LIQUIDAR.ToString + " %"
        porcentaje_poliza_incompletado_actual.Text = PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR.ToString + " %"

        For i = 10 To 12

            Vista.Columns(i).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Vista.Columns(i).SummaryItem.DisplayFormat = "$ {0:###,###,##0.00}"

        Next

        For i = 13 To 18

            Vista.Columns(i).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Vista.Columns(i).SummaryItem.DisplayFormat = "C$ {0:###,###,##0.00}"


        Next

    End Sub
    Private Sub compra_foranea_detalle_producto_gestion(accion As Integer)

        If datos_de_proveedores.Rows.Count <= 0 Then Exit Sub

        Dim agregar_producto_a_compra As New compra_foranea_agregar_producto(nSucursal, txtNumDocumento.Text, accion) With {.StartPosition = FormStartPosition.CenterScreen, .get_tasa_cabio_de_documento = ChangeToDouble(txtTasaCambio.Text), .get_peso = IIf(cmb_calcular_fob_peso.EditValue = 1, True, False)}

        If Vista.RowCount <= 0 And (accion = 2 Or accion = 3) = True Then Exit Sub ' sino hay registro en el detalle y la accion es actualizar o eliminar entonces que no haga nada 

        If Vista.RowCount > 0 And (accion = 2 Or accion = 3) = True Then

            With Vista

                Dim curr_producto = New compra_foranea_producto(.GetRowCellValue(.GetSelectedRows(0), "sucursal"),
                                                                                     .GetRowCellValue(.GetSelectedRows(0), "numero_de_documento"),
                                                                                     .GetRowCellValue(.GetSelectedRows(0), "Bodega"),
                                                                                     .GetRowCellValue(.GetSelectedRows(0), "Codigo")).get_producto

                curr_producto.get_descripcion = .GetRowCellValue(.GetSelectedRows(0), "Producto")

                agregar_producto_a_compra.set_producto = curr_producto

                agregar_producto_a_compra.set_valores_a_formulario()

            End With

        End If

        agregar_producto_a_compra.ShowDialog()

        get_compra_foranea_detalle()

    End Sub

    Private Sub dolar_a_cordoba(impuesto As String)

        Dim convertir_moneda As New compra_foranea_convertir_moneda(Fecha_de_recepcion.Value, ChangeToDouble(txtTasaCambio.Text), True) With {.StartPosition = FormStartPosition.CenterScreen}

        convertir_moneda.ShowDialog()

        Select Case impuesto

            Case "SSA"

                If Not convertir_moneda.get_estado_de_proceso = False Then txtSSA.Text = FormatNumber(convertir_moneda.get_monto_cordoba, 4)

            Case "SPE"

                If Not convertir_moneda.get_estado_de_proceso = False Then txtSPE.Text = FormatNumber(convertir_moneda.get_monto_cordoba, 4)

            Case "TSI"

                If Not convertir_moneda.get_estado_de_proceso = False Then txtTSI.Text = FormatNumber(convertir_moneda.get_monto_cordoba, 4)

            Case "ISC"

                If Not convertir_moneda.get_estado_de_proceso = False Then txtISC.Text = FormatNumber(convertir_moneda.get_monto_cordoba, 4)

            Case "DAI"

                If Not convertir_moneda.get_estado_de_proceso = False Then txtDAI.Text = FormatNumber(convertir_moneda.get_monto_cordoba, 4)

        End Select

        Dim impuestos = data_compra_foranea.get_impuestos(Funciones.Param("@sucursal", nSucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento", txtNumDocumento.Text, SqlDbType.VarChar))

        If impuestos.ContainsKey(New impuesto.key_identificador With {.sucursal = nSucursal, .numero_de_documento = txtNumDocumento.Text, .tipo_documento = 2021}) Then

            Dim curr_impuesto = impuestos.Item(New impuesto.key_identificador With {.sucursal = nSucursal, .numero_de_documento = txtNumDocumento.Text, .tipo_documento = 2021})

            curr_impuesto.get_otros_impuestos = get_impuestos_en_poliza()

            curr_impuesto.get_neto_a_pagar = curr_impuesto.get_iva + curr_impuesto.get_otros_impuestos

            curr_impuesto.gestion(2) ' me actualiza esta vaina xD LOL troll

        End If

    End Sub
    Private Function get_impuestos_en_poliza() As Decimal

        Return ChangeToDouble(txtSSA.Text) + ChangeToDouble(txtSPE.Text) + ChangeToDouble(txtTSI.Text) + ChangeToDouble(txtISC.Text) + ChangeToDouble(txtDAI.Text)

    End Function
    Private Sub reset_values_to_box_credito_fiscal_subtotales()

        txtFletesTerrestre.Text = FormatNumber(0, 4)
        txtIVAPoliza.Text = FormatNumber(0, 4)
        txtOtroPago.Text = FormatNumber(0, 4)
        txtOAduana.Text = FormatNumber(0, 4)
        txtOTransporte.Text = FormatNumber(0, 4)
        txtAlmacen.Text = FormatNumber(0, 4)
        txtOtroAlmacen.Text = FormatNumber(0, 4)
        txtAduana.Text = FormatNumber(0, 4)


    End Sub
    Private Sub reset_values_to_box()

        Fecha_de_documento.Value = Date.Now.Date
        Fecha_de_recepcion.Value = Date.Now.Date
        Fecha_de_vencimiento.Value = Date.Now.Date
        txtObservacion.Text = String.Empty
        txtDAI.Text = FormatNumber(0, 4)
        txtISC.Text = FormatNumber(0, 4)
        txtSPE.Text = FormatNumber(0, 4)
        txtSSA.Text = FormatNumber(0, 4)
        txtTSI.Text = FormatNumber(0, 4)

        reset_values_to_box_credito_fiscal_subtotales()

        cmb_calcular_fob_peso.EditValue = "1"

        txt_no_poliza.Text = String.Empty

        txt_numero_documento_consecutivo.Text = 1

        documento_multiple.Checked = False
        documento_multiple.Enabled = True
        txt_no_poliza.Enabled = True

        porcentaje_poliza_completado_actual.Text = "0.00 %"
        porcentaje_poliza_completado_general.Text = "0.00 %"
        porcentaje_poliza_completado_restante.Text = "0.00 %"
        porcentaje_poliza_incompletado_actual.Text = "0.00 %"

        documento_actual_aplicado = False

        curr_compra = New compra()

    End Sub

    Public Function nuevo() As Integer Implements IMenu.nuevo

        nTipoEdic = 1

        PermitirBotonesEdicion(False)

        txtNumDocumento.Text = data_compra_foranea.get_the_last_No_document

        reset_values_to_box()

        Ajuste_proveedores(data_compra_foranea.get_proveedores(Funciones.Param("@numero_de_sucursal", nSucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento_poliza", txt_no_poliza.Text, SqlDbType.VarChar)))

        bloqueo(True, False)

        Return 0

    End Function

    Public Function cancelar() As Integer Implements IMenu.cancelar

        If curr_compra.get_aplicado = True And curr_compra.IsMasterSaved = True Then

            bloqueo(False, True)

        ElseIf curr_compra.get_aplicado = False And curr_compra.IsMasterSaved = True Then

            bloqueo(False, False)

        ElseIf curr_compra.get_aplicado = False And curr_compra.IsMasterSaved = False Then

            bloqueo(False, True)

        End If

        PermitirBotonesEdicion(True)

        Return 0

    End Function

    Public Function guardar() As Integer Implements IMenu.guardar

        If curr_compra.get_aplicado = True Or txt_no_poliza.Text = String.Empty Or txt_no_poliza.TextLength < 4 Then Return 1

        If ChangeToDouble(txtTasaCambio.Text) = 0 Then MsgBox("Ops! Es necesario contar con una tasa de cambio.", MsgBoxStyle.Exclamation) : Return 1

        bloqueo(False, False)

        set_datos_de_compra()

        Dim Result = curr_compra.guardar

        If Result = 0 Then get_compra_foranea_detalle()

        Return 0

    End Function

    Public Function modificar() As Integer Implements IMenu.modificar

        If curr_compra.get_aplicado = True Or curr_compra.get_numero_de_documento = String.Empty Then Return 1

        nTipoEdic = 2

        PermitirBotonesEdicion(False)

        bloqueo(True, False)

        Return 0

    End Function
    Public Function eliminar() As Integer Implements IMenu.eliminar

        If curr_compra.get_aplicado = True Or curr_compra.get_numero_de_documento = String.Empty Or curr_compra.get_aplicado = True Then Return 1

        Dim Question = MsgBox("Eliminar la poliza actual?", MsgBoxStyle.YesNo) : If Not Question = MsgBoxResult.Yes Then Return 1

        curr_compra.eliminar_compra()

        nuevo()

        Return 0

    End Function
    Private Sub aplicar()

        If curr_compra.get_aplicado = True Or curr_compra.IsMasterSaved = False Then Exit Sub

        If Not Decimal.Round(curr_compra.get_dai, 2) = Decimal.Round(curr_compra.get_detalle_total_dai, 2) Then

            MsgBox("Ops! DAI distribuido en el detalle no concuerda con el establecido en el campo DAI", MsgBoxStyle.Exclamation)

            Exit Sub

        End If

        If curr_compra.get_documento_multiple = False Then

            If Not PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR = 100 Then

                MsgBox(String.Format("Ops! El porcentaje de poliza requerido a liquidar es del 100 % y usted esta tratando de liquidar {0} % ", PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR), MsgBoxStyle.Exclamation)

                Exit Sub

            End If

        Else


            If PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR > PORCENTAJE_TOTAL_POLIZA_RESTANTE_POR_LIQUIDAR Then

                MsgBox(String.Format("Ops! Sobre pasa el limite permitido por liquidar. el limite es {0} % y usted esta tratando de liquidar el {1} %", PORCENTAJE_TOTAL_POLIZA_RESTANTE_POR_LIQUIDAR, PORCENTAJE_TOTAL_POLIZA_ACTUAL_SIN_LIQUIDAR), MsgBoxStyle.Exclamation)

                Exit Sub

            End If


        End If

        Dim question = MsgBox(String.Format("Seguro de liquidar la poliza {0}. luego de realizar este proceso no hay vuelta a tras! ", curr_compra.get_numero_de_documento_poliza), MsgBoxStyle.YesNo, "Question")

        If Not question = MsgBoxResult.Yes Then Exit Sub

        documento_actual_aplicado = True

        set_datos_de_compra()

        Dim Result = curr_compra.guardar

        If Result = 0 Then

            bloqueo(False, True)

            get_compra_foranea_detalle()

            MsgBox("Poliza liquidada", MsgBoxStyle.Information)

            Dim Reporte_de_movimiento As New reporte_moviemiento_almacen.compra_foranea(nSucursal, txtNumDocumento.Text)

            Reporte_de_movimiento.visualizar()

        End If

    End Sub
    Public Function salir() As Integer Implements IMenu.salir

        Me.Close()

        Return 0

    End Function

    Private Sub set_datos_de_compra()

        Dim Builder As New compra.BuilderCompra

        Builder.sucursal = nSucursal
        Builder.numero_de_documento = txtNumDocumento.Text
        Builder.numero_de_documento_poliza = txt_no_poliza.Text
        Builder.numero_de_documento_consecutivo = ChangeToDouble(txt_numero_documento_consecutivo.Text)
        Builder.documento_multiple = documento_multiple.Checked
        Builder.tipo_de_compra = IIf(rbContado.Checked = True, 1, 0)
        Builder.tipo_cordoba_dolar = IIf(rbCordobas.Checked = True, 1, 0)
        Builder.tipo_calculo_de_documento = cmb_calcular_fob_peso.EditValue
        Builder.tasa_cambio_de_documento = ChangeToDouble(txtTasaCambio.Text)
        Builder.fecha_de_documento = Fecha_de_documento.Value
        Builder.fecha_de_recepcion = Fecha_de_recepcion.Value
        Builder.fecha_de_vencimiento = Fecha_de_vencimiento.Value
        Builder.plazo = ChangeToDouble(txtPlazo.Text)
        Builder.observaciones = txtObservacion.Text
        Builder.ssa = ChangeToDouble(txtSSA.Text)
        Builder.tsi = ChangeToDouble(txtTSI.Text)
        Builder.spe = ChangeToDouble(txtSPE.Text)
        Builder.isc = ChangeToDouble(txtISC.Text)
        Builder.dai = ChangeToDouble(txtDAI.Text)
        Builder.aplicado = documento_actual_aplicado


        curr_compra = New compra(Builder)

    End Sub
    Private Sub get_datos_de_compra()

        documento_multiple.Enabled = False   ' bloqueo el numero de poliza aqui para que no perder integridad de los datos en cuanto a los productos ingresado y el fob de los proveedores
        txt_no_poliza.Enabled = False        ' sino se realiza esta operacion podria verse afectado el documento al cambiar el numero de poliza y vincular los productos a otro proveedor 

        txtNumDocumento.Text = curr_compra.get_numero_de_documento

        txt_no_poliza.Text = curr_compra.get_numero_de_documento_poliza

        documento_multiple.Checked = curr_compra.get_documento_multiple

        txt_numero_documento_consecutivo.Text = curr_compra.get_numero_de_documento_consecutivo

        If curr_compra.get_tipo_de_compra = 1 Then rbContado.Checked = True Else rbCredito.Checked = True
        If curr_compra.get_tipo_cordoba_dolar = 1 Then rbCordobas.Checked = True Else rbDolares.Checked = True
        cmb_calcular_fob_peso.EditValue = curr_compra.get_tipo_calculo_de_documento.ToString
        txtTasaCambio.Text = FormatNumber(curr_compra.get_tasa_cambio_de_documento, 4)
        Fecha_de_documento.Value = curr_compra.get_fecha_de_documento
        Fecha_de_recepcion.Value = curr_compra.get_fecha_de_recepcion
        Fecha_de_vencimiento.Value = curr_compra.get_fecha_de_vencimiento
        txtPlazo.Text = curr_compra.get_plazo
        txtObservacion.Text = curr_compra.get_observaciones
        txtSSA.Text = FormatNumber(curr_compra.get_ssa, 4)
        txtTSI.Text = FormatNumber(curr_compra.get_tsi, 4)
        txtSPE.Text = FormatNumber(curr_compra.get_spe, 4)
        txtISC.Text = FormatNumber(curr_compra.get_isc, 4)
        txtDAI.Text = FormatNumber(curr_compra.get_dai, 4)

        reset_values_to_box_credito_fiscal_subtotales()

        Dim curr_impuestos = data_compra_foranea.get_impuestos(Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.VarChar))

        For Each curr_impuesto As impuesto In curr_impuestos.Values

            set_datos_de_impuestos_a_compra_from_db(curr_impuesto)

        Next

        get_compra_foranea_detalle()

    End Sub

    Private Sub bloqueo(key As Boolean, Save As Boolean)

        If Save = True Then

            pTop_Left.Enabled = False

            g_subtotales.Enabled = False

            g_credito_fiscal.Enabled = False

            p_proveedores_botones.Enabled = False

            g_impuesto_de_poliza.Enabled = False

            pButtons.Enabled = False

            btn_aplicar.Enabled = False

            Exit Sub

        End If

        'compra_foranea_estado_master_guardado = Not key

        pTop.Enabled = True

        pTop_Left.Enabled = key

        g_impuesto_de_poliza.Enabled = key

        cmb_calcular_fob_peso.Enabled = key

        If ChangeToDouble(txt_numero_documento_consecutivo.Text) > 1 Then p_proveedores_botones.Enabled = False : cmb_calcular_fob_peso.Enabled = False Else p_proveedores_botones.Enabled = Not key

        If curr_compra.IsMasterSaved = True Then

            documento_multiple.Enabled = False   ' bloqueo el numero de poliza aqui para que no perder integridad de los datos en cuanto a los productos ingresado y el fob de los proveedores
            txt_no_poliza.Enabled = False        ' sino se realiza esta operacion podria verse afectado el documento al cambiar el numero de poliza y vincular los productos a otro proveedor 

        End If

        g_subtotales.Enabled = Not key

        g_credito_fiscal.Enabled = Not key

        pButtons.Enabled = Not key

        pTop_Right.Enabled = True

        btn_aplicar.Enabled = True


    End Sub

    Private Sub Proveedor(ByRef accion As Integer)

        If accion = 3 And Proveedores_vista.RowCount <= 1 And Vista.RowCount > 0 Then Exit Sub ' no permirit remover proveedor cuando exista productos en detalle y un solo proveedor 

        Dim prov As New compra_foranea_proveedor(nSucursal, txtNumDocumento.Text, accion) With {.StartPosition = FormStartPosition.CenterParent}

        If accion = 2 Or accion = 3 Then

            If Proveedores_vista.RowCount <= 0 Then Exit Sub

            prov.set_datos_proveedor(Proveedores_vista.GetFocusedDataRow())

            If accion = 2 Then prov.cmb_Proveedores.Enabled = False
            If accion = 3 Then prov.pBody.Enabled = False

        End If

        prov.set_all_proveedores_in_detalle = datos_de_proveedores

        prov.ShowDialog()

        Ajuste_proveedores(data_compra_foranea.get_proveedores(Funciones.Param("@numero_de_sucursal", nSucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento_poliza", txt_no_poliza.Text, SqlDbType.VarChar)))

    End Sub
    Private Sub Ajuste_proveedores(ByRef Data As DataTable)

        datos_de_proveedores = Data

        Proveedores.DataSource = datos_de_proveedores

        For i = 0 To 2

            Proveedores_vista.Columns(i).Visible = False

        Next

        Proveedores_vista.Columns(9).Visible = False

        For i = 5 To 8

            Proveedores_vista.Columns(i).DisplayFormat.FormatType = FormatType.Numeric
            Proveedores_vista.Columns(i).DisplayFormat.FormatString = "N2"

            Proveedores_vista.Columns(i).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Proveedores_vista.Columns(i).SummaryItem.DisplayFormat = "$ {0:###,###,##0.0000}"

        Next

        get_compra_foranea_detalle()

    End Sub
    Private Sub get_tasa_de_cambio_por_fecha()

        txtTasaCambio.Text = FormatNumber(data_tasa_de_cambio.get_tasa_de_cambio_actual(Funciones.Param("@Fecha", Fecha_de_recepcion.Value, SqlDbType.Date)), 4)

    End Sub

    Private Sub validar_fecha_documento_compra()

        Dim fecha_de_recibido As Date = Fecha_de_recepcion.Value.Date
        Dim vencimiento As Date = Fecha_de_vencimiento.Value.Date

        If fecha_de_recibido.Date.Date > vencimiento.Date.Date Then

            MsgBox("Fecha de vencimiento no puede ser inferior a la fecha de compra.", MsgBoxStyle.Critical) : Fecha_de_documento.Value = Date.Now.Date

        ElseIf fecha_de_recibido > Date.Now() Then


            MsgBox("Fecha de compra no puede ser mayor a la fecha actual.", MsgBoxStyle.Critical) : Fecha_de_documento.Value = Date.Now.Date


        Else

            txtPlazo.Text = IIf(DateDiff(DateInterval.Day, fecha_de_recibido, vencimiento) = 0, "", DateDiff(DateInterval.Day, fecha_de_recibido, vencimiento))

        End If

    End Sub
    Private Sub validar_fecha_documento_vencimiento()

        Dim fecha_de_recibido As Date = Fecha_de_recepcion.Value.Date
        Dim vencimiento As Date = Fecha_de_vencimiento.Value.Date

        If fecha_de_recibido.Date.Date > vencimiento.Date.Date Then

            MsgBox("Fecha de compra no puede ser mayor a la fecha de vencimiento.", MsgBoxStyle.Critical) : Fecha_de_vencimiento.Value = Date.Now.Date

        Else

            txtPlazo.Text = IIf(DateDiff(DateInterval.Day, fecha_de_recibido, vencimiento) = 0, "", DateDiff(DateInterval.Day, fecha_de_recibido, vencimiento))

        End If

    End Sub
    Private Sub validar_plazo_documento()

        If ChangeToDouble(txtPlazo.Text) = 0 Then Fecha_de_vencimiento.Value = Fecha_de_recepcion.Value

        If Not ChangeToDouble(txtPlazo.Text) = 0 Then Fecha_de_vencimiento.Value = Fecha_de_recepcion.Value.AddDays(ChangeToDouble(txtPlazo.Text))

    End Sub

    Private Sub set_impuesto(ByRef Tipo_de_documento As Integer)

        Dim agregar_impuesto As New compra_foranea_impuesto(nSucursal, txtNumDocumento.Text, Tipo_de_documento) With {.StartPosition = FormStartPosition.CenterParent, .set_otros_impuestos = get_impuestos_en_poliza()}

        agregar_impuesto.ShowDialog()

        set_datos_de_impuestos_a_compra_from_db(agregar_impuesto.curr_impuesto)

        agregar_impuesto.Dispose()

        get_compra_foranea_detalle()

    End Sub

    Private Sub set_datos_de_impuestos_a_compra_from_db(ByRef curr_impuesto As impuesto)

        Select Case curr_impuesto.get_tipo_documento

            Case 2021

                txtIVAPoliza.Text = FormatNumber(curr_impuesto.get_iva, 4)

            Case 1021

                txtAduana.Text = FormatNumber(curr_impuesto.get_iva, 4)
                txtOAduana.Text = FormatNumber(curr_impuesto.get_otros_gastos + curr_impuesto.get_sub_total, 4)

            Case 1022

                txtFletesTerrestre.Text = FormatNumber(curr_impuesto.get_iva, 4)
                txtOTransporte.Text = FormatNumber(curr_impuesto.get_sub_total, 4)

            Case 1019

                txtAlmacen.Text = FormatNumber(curr_impuesto.get_iva, 4)
                txtOtroAlmacen.Text = FormatNumber(curr_impuesto.get_sub_total, 4)

            Case 2022

                txtOtroPago.Text = FormatNumber(curr_impuesto.get_otros_gastos, 4)

        End Select

    End Sub

    Private Sub set_tipo_de_calculo()

        Dim datos = New DataTable

        With datos

            .Columns.Add("id")
            .Columns.Add("Descripcion")

            .Rows.Add(1, "FOB")
            .Rows.Add(2, "PESO KG")

        End With

        cmb_calcular_fob_peso.Properties.DataSource = datos

        cmb_calcular_fob_peso.Properties.DisplayMember = "Descripcion"

        cmb_calcular_fob_peso.Properties.ValueMember = "id"

        cmb_calcular_fob_peso.Properties.Columns.Add(New LookUpColumnInfo("id", "id", 20))

        cmb_calcular_fob_peso.Properties.Columns.Add(New LookUpColumnInfo("Descripcion", "Descripcion", 100))

        cmb_calcular_fob_peso.Properties.Columns(0).Visible = False

        cmb_calcular_fob_peso.EditValue = "1"

        cmb_calcular_fob_peso.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub

    Public Function historial() As Integer Implements IMenu.historial

        Dim curr_historial As New compra_foranea_buscador With {.StartPosition = FormStartPosition.CenterScreen}

        curr_historial.ShowDialog()

        If curr_historial.get_compra.get_numero_de_documento = String.Empty Then Return 1

        curr_compra = curr_historial.get_compra

        get_datos_de_compra()

        Ajuste_proveedores(data_compra_foranea.get_proveedores(Funciones.Param("@numero_de_sucursal", curr_compra.get_sucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.VarChar)))

        If curr_historial.get_compra.get_aplicado = True Then bloqueo(False, True) Else bloqueo(False, False)

        Return 0

    End Function

    Private Sub verificar_poliza()

        If txt_no_poliza.Text = String.Empty Then Exit Sub ' sino hay poliza no validar nada 
        If curr_compra.get_sucursal = nSucursal And curr_compra.get_numero_de_documento_poliza = txt_no_poliza.Text Then set_datos_borrados_multiple() : Exit Sub ' si poliza digitada es identica a la poliza objeto entonces no validar nada y devolver los datos de la poliza objeto

        Dim gran_total_poliza_aplicada = New compra(nSucursal, txt_no_poliza.Text, True, True).get_total_poliza_aplicada

        If gran_total_poliza_aplicada = 100 Then

            txt_no_poliza.Text = String.Empty

            MsgBox("Poliza ya esta liquidad al 100% no esta permitido continuar con el mismo numero de poliza", MsgBoxStyle.Exclamation)

            Exit Sub

        End If

        txt_numero_documento_consecutivo.Text = 1
        documento_multiple.Enabled = True
        cmb_calcular_fob_peso.Enabled = True
        Proveedores.DataSource = Nothing

        Dim poliza_si_aplicada_sin_documento_multiple = New compra(nSucursal, txt_no_poliza.Text, False, True).verificar_poliza() ' Listp
        Dim poliza_no_aplicada_sin_documento_multiple = New compra(nSucursal, txt_no_poliza.Text, False, False).verificar_poliza() ' Listo

        Dim poliza_si_aplicada_con_documento_multiple = New compra(nSucursal, txt_no_poliza.Text, True, True).verificar_poliza() ' Listo
        Dim poliza_no_aplicada_con_documento_multiple = New compra(nSucursal, txt_no_poliza.Text, True, False).verificar_poliza() 'Listo

        If poliza_si_aplicada_sin_documento_multiple = True Then

            MsgBox(String.Format("ya existe una poliza liquidada con este mismo numero de poliza digitado: {0}. Por lo tanto no esta permitido reutilizar el codigo digitado ", txt_no_poliza.Text), MsgBoxStyle.Exclamation)

            reset_multiple()

        ElseIf poliza_no_aplicada_sin_documento_multiple = True Then

            MsgBox(String.Format("ya existe una poliza sin liquidar con este mismo numero de poliza digitado: {0}. Por lo tanto no esta permitido reutilizar el codigo digitado ", txt_no_poliza.Text), MsgBoxStyle.Exclamation)

            reset_multiple()

        ElseIf poliza_no_aplicada_con_documento_multiple = True Then

            MsgBox(String.Format("Ops! Existe una poliza con el mismo codigo de poliza {0} sin aplicar.", txt_no_poliza.Text), MsgBoxStyle.Exclamation)

            reset_multiple()

        ElseIf poliza_si_aplicada_con_documento_multiple = True Then

            Dim question = MsgBox(String.Format("ya existe una poliza multiple con numero de poliza {0}. continuar con el consecutivo de poliza?", txt_no_poliza.Text), MsgBoxStyle.YesNo)

            If Not question = MsgBoxResult.No Or question = MsgBoxResult.Ignore Or question = MsgBoxResult.Cancel Or question = MsgBoxResult.Abort Then

                txt_numero_documento_consecutivo.Text = New compra(nSucursal, txt_no_poliza.Text, True, True).get_ultimo_consecutivo + 1 'indicamos que existe una poliza aplicada y que es multiple por lo tanto obtenemos el ultimo registro consecutivo de ella y le sumamos 1

                cmb_calcular_fob_peso.EditValue = New compra(nSucursal, txt_no_poliza.Text, False, False).get_tipo_de_calculo_de_primer_poliza.ToString ' aqui false y false no hacen nada pero los necesito para rellenar el constructor xD

                Ajuste_proveedores(data_compra_foranea.get_proveedores(Funciones.Param("@numero_de_sucursal", nSucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento_poliza", txt_no_poliza.Text, SqlDbType.VarChar)))


                documento_multiple.Checked = True
                documento_multiple.Enabled = False
                cmb_calcular_fob_peso.Enabled = False

            Else

                txt_no_poliza.Text = String.Empty

            End If

        End If

    End Sub

    Private Sub set_datos_borrados_multiple()

        If curr_compra.get_numero_de_documento_consecutivo > 1 Then documento_multiple.Enabled = False

        documento_multiple.Checked = True

        txt_numero_documento_consecutivo.Text = curr_compra.get_numero_de_documento_consecutivo
        documento_multiple.Checked = curr_compra.get_documento_multiple
        Ajuste_proveedores(data_compra_foranea.get_proveedores(Funciones.Param("@numero_de_sucursal", nSucursal, SqlDbType.Int), Funciones.Param("@numero_de_documento_poliza", txt_no_poliza.Text, SqlDbType.VarChar)))

    End Sub

    Private Sub reset_multiple()

        txt_numero_documento_consecutivo.Text = 1
        documento_multiple.Checked = False
        txt_no_poliza.Text = String.Empty

    End Sub
    Public Function reporte_vista_previa() As Integer Implements IMenu.imprimir

        If curr_compra.get_numero_de_documento = String.Empty Then Return 1

        Dim Previsualizacion As New reporte_modulo.compra_foranea(nSucursal, curr_compra.get_numero_de_documento)

        Previsualizacion.visualizar()

        Return 0

    End Function

End Class