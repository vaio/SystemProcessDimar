﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class compra_foranea

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(compra_foranea))
        Me.pMain = New System.Windows.Forms.Panel()
        Me.pBody = New System.Windows.Forms.Panel()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.Vista = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.pDown = New System.Windows.Forms.Panel()
        Me.btn_aplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.pButtons = New System.Windows.Forms.Panel()
        Me.btn_producto_eliminar = New System.Windows.Forms.Button()
        Me.btn_producto_modificar = New System.Windows.Forms.Button()
        Me.btn_producto_agregar = New System.Windows.Forms.Button()
        Me.pTop = New System.Windows.Forms.Panel()
        Me.pTop_Left = New System.Windows.Forms.Panel()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_numero_documento_consecutivo = New System.Windows.Forms.TextBox()
        Me.documento_multiple = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmb_calcular_fob_peso = New DevExpress.XtraEditors.LookUpEdit()
        Me.txt_no_poliza = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Fecha_de_recepcion = New System.Windows.Forms.DateTimePicker()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rbDolares = New System.Windows.Forms.RadioButton()
        Me.btnTasaCambio = New System.Windows.Forms.Button()
        Me.rbCordobas = New System.Windows.Forms.RadioButton()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtTasaCambio = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbCredito = New System.Windows.Forms.RadioButton()
        Me.rbContado = New System.Windows.Forms.RadioButton()
        Me.txtPlazo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Fecha_de_vencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Fecha_de_documento = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNumDocumento = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pTop_Right = New System.Windows.Forms.Panel()
        Me.porcentaje_poliza_incompletado_actual = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.porcentaje_poliza_completado_restante = New System.Windows.Forms.Label()
        Me.porcentaje_poliza_completado_actual = New System.Windows.Forms.Label()
        Me.porcentaje_poliza_completado_general = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.g_proveedores = New System.Windows.Forms.GroupBox()
        Me.p_proveedores_botones = New System.Windows.Forms.Panel()
        Me.btn_remover_proveedor = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_modificar_proveedor = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_agregar_proveedores = New DevExpress.XtraEditors.SimpleButton()
        Me.Proveedores = New DevExpress.XtraGrid.GridControl()
        Me.Proveedores_vista = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.g_impuesto_de_poliza = New System.Windows.Forms.GroupBox()
        Me.btn_TSI = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_DAI = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_ISC = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_SPE = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_SSA = New DevExpress.XtraEditors.SimpleButton()
        Me.txtSSA = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtTSI = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtSPE = New System.Windows.Forms.TextBox()
        Me.txtISC = New System.Windows.Forms.TextBox()
        Me.txtDAI = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.g_credito_fiscal = New System.Windows.Forms.GroupBox()
        Me.btn_Poliza = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_almacen = New DevExpress.XtraEditors.SimpleButton()
        Me.txtAlmacen = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFletesTerrestre = New System.Windows.Forms.TextBox()
        Me.btn_transporte = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_aduana = New DevExpress.XtraEditors.SimpleButton()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtAduana = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtIVAPoliza = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.g_subtotales = New System.Windows.Forms.GroupBox()
        Me.txtOtroAlmacen = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btn_otropago = New DevExpress.XtraEditors.SimpleButton()
        Me.txtOAduana = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtOTransporte = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtOtroPago = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.pMain.SuspendLayout()
        Me.pBody.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Vista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDown.SuspendLayout()
        Me.pButtons.SuspendLayout()
        Me.pTop.SuspendLayout()
        Me.pTop_Left.SuspendLayout()
        CType(Me.cmb_calcular_fob_peso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.pTop_Right.SuspendLayout()
        Me.g_proveedores.SuspendLayout()
        Me.p_proveedores_botones.SuspendLayout()
        CType(Me.Proveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Proveedores_vista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.g_impuesto_de_poliza.SuspendLayout()
        Me.g_credito_fiscal.SuspendLayout()
        Me.g_subtotales.SuspendLayout()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.pBody)
        Me.pMain.Controls.Add(Me.pDown)
        Me.pMain.Controls.Add(Me.pTop)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1924, 756)
        Me.pMain.TabIndex = 0
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.Tabla)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 377)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(1924, 281)
        Me.pBody.TabIndex = 2
        '
        'Tabla
        '
        Me.Tabla.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Location = New System.Drawing.Point(0, 0)
        Me.Tabla.MainView = Me.Vista
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(1924, 281)
        Me.Tabla.TabIndex = 6
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.Vista})
        '
        'Vista
        '
        Me.Vista.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.Vista.GridControl = Me.Tabla
        Me.Vista.GroupFormat = ""
        Me.Vista.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.Vista.Name = "Vista"
        Me.Vista.OptionsBehavior.Editable = False
        Me.Vista.OptionsBehavior.ReadOnly = True
        Me.Vista.OptionsCustomization.AllowColumnMoving = False
        Me.Vista.OptionsCustomization.AllowColumnResizing = False
        Me.Vista.OptionsCustomization.AllowGroup = False
        Me.Vista.OptionsMenu.EnableGroupPanelMenu = False
        Me.Vista.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.Vista.OptionsView.RowAutoHeight = True
        Me.Vista.OptionsView.ShowFooter = True
        Me.Vista.OptionsView.ShowGroupPanel = False
        '
        'pDown
        '
        Me.pDown.Controls.Add(Me.btn_aplicar)
        Me.pDown.Controls.Add(Me.pButtons)
        Me.pDown.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pDown.Location = New System.Drawing.Point(0, 658)
        Me.pDown.Name = "pDown"
        Me.pDown.Size = New System.Drawing.Size(1924, 98)
        Me.pDown.TabIndex = 1
        '
        'btn_aplicar
        '
        Me.btn_aplicar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aplicar.Location = New System.Drawing.Point(1742, 29)
        Me.btn_aplicar.Name = "btn_aplicar"
        Me.btn_aplicar.Size = New System.Drawing.Size(170, 57)
        Me.btn_aplicar.TabIndex = 24
        Me.btn_aplicar.Text = "&Nacionalizar"
        '
        'pButtons
        '
        Me.pButtons.Controls.Add(Me.btn_producto_eliminar)
        Me.pButtons.Controls.Add(Me.btn_producto_modificar)
        Me.pButtons.Controls.Add(Me.btn_producto_agregar)
        Me.pButtons.Enabled = False
        Me.pButtons.Location = New System.Drawing.Point(8, 7)
        Me.pButtons.Margin = New System.Windows.Forms.Padding(4)
        Me.pButtons.Name = "pButtons"
        Me.pButtons.Size = New System.Drawing.Size(536, 78)
        Me.pButtons.TabIndex = 5
        '
        'btn_producto_eliminar
        '
        Me.btn_producto_eliminar.Image = CType(resources.GetObject("btn_producto_eliminar.Image"), System.Drawing.Image)
        Me.btn_producto_eliminar.Location = New System.Drawing.Point(360, 17)
        Me.btn_producto_eliminar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_producto_eliminar.Name = "btn_producto_eliminar"
        Me.btn_producto_eliminar.Size = New System.Drawing.Size(170, 57)
        Me.btn_producto_eliminar.TabIndex = 2
        Me.btn_producto_eliminar.Text = "&Eliminar"
        Me.btn_producto_eliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_producto_eliminar.UseVisualStyleBackColor = True
        '
        'btn_producto_modificar
        '
        Me.btn_producto_modificar.Image = CType(resources.GetObject("btn_producto_modificar.Image"), System.Drawing.Image)
        Me.btn_producto_modificar.Location = New System.Drawing.Point(182, 17)
        Me.btn_producto_modificar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_producto_modificar.Name = "btn_producto_modificar"
        Me.btn_producto_modificar.Size = New System.Drawing.Size(170, 57)
        Me.btn_producto_modificar.TabIndex = 1
        Me.btn_producto_modificar.Text = "&Modificar"
        Me.btn_producto_modificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_producto_modificar.UseVisualStyleBackColor = True
        '
        'btn_producto_agregar
        '
        Me.btn_producto_agregar.Image = CType(resources.GetObject("btn_producto_agregar.Image"), System.Drawing.Image)
        Me.btn_producto_agregar.Location = New System.Drawing.Point(5, 17)
        Me.btn_producto_agregar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_producto_agregar.Name = "btn_producto_agregar"
        Me.btn_producto_agregar.Size = New System.Drawing.Size(170, 57)
        Me.btn_producto_agregar.TabIndex = 23
        Me.btn_producto_agregar.Text = "&Agregar"
        Me.btn_producto_agregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_producto_agregar.UseVisualStyleBackColor = True
        '
        'pTop
        '
        Me.pTop.Controls.Add(Me.pTop_Left)
        Me.pTop.Controls.Add(Me.pTop_Right)
        Me.pTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pTop.Location = New System.Drawing.Point(0, 0)
        Me.pTop.Name = "pTop"
        Me.pTop.Size = New System.Drawing.Size(1924, 377)
        Me.pTop.TabIndex = 0
        '
        'pTop_Left
        '
        Me.pTop_Left.BackColor = System.Drawing.SystemColors.Control
        Me.pTop_Left.Controls.Add(Me.txtObservacion)
        Me.pTop_Left.Controls.Add(Me.Label10)
        Me.pTop_Left.Controls.Add(Me.txt_numero_documento_consecutivo)
        Me.pTop_Left.Controls.Add(Me.documento_multiple)
        Me.pTop_Left.Controls.Add(Me.Label6)
        Me.pTop_Left.Controls.Add(Me.cmb_calcular_fob_peso)
        Me.pTop_Left.Controls.Add(Me.txt_no_poliza)
        Me.pTop_Left.Controls.Add(Me.Label9)
        Me.pTop_Left.Controls.Add(Me.Fecha_de_recepcion)
        Me.pTop_Left.Controls.Add(Me.Label32)
        Me.pTop_Left.Controls.Add(Me.GroupBox5)
        Me.pTop_Left.Controls.Add(Me.GroupBox2)
        Me.pTop_Left.Controls.Add(Me.Fecha_de_vencimiento)
        Me.pTop_Left.Controls.Add(Me.Label7)
        Me.pTop_Left.Controls.Add(Me.Fecha_de_documento)
        Me.pTop_Left.Controls.Add(Me.Label5)
        Me.pTop_Left.Controls.Add(Me.Label4)
        Me.pTop_Left.Controls.Add(Me.txtNumDocumento)
        Me.pTop_Left.Controls.Add(Me.Label1)
        Me.pTop_Left.Enabled = False
        Me.pTop_Left.Location = New System.Drawing.Point(0, 0)
        Me.pTop_Left.Name = "pTop_Left"
        Me.pTop_Left.Size = New System.Drawing.Size(489, 372)
        Me.pTop_Left.TabIndex = 86
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Location = New System.Drawing.Point(26, 318)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.MaxLength = 200
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(456, 47)
        Me.txtObservacion.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(152, 96)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 17)
        Me.Label10.TabIndex = 105
        Me.Label10.Text = "No. Con:"
        '
        'txt_numero_documento_consecutivo
        '
        Me.txt_numero_documento_consecutivo.BackColor = System.Drawing.Color.Snow
        Me.txt_numero_documento_consecutivo.Enabled = False
        Me.txt_numero_documento_consecutivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_numero_documento_consecutivo.Location = New System.Drawing.Point(219, 91)
        Me.txt_numero_documento_consecutivo.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_numero_documento_consecutivo.MaxLength = 3
        Me.txt_numero_documento_consecutivo.Name = "txt_numero_documento_consecutivo"
        Me.txt_numero_documento_consecutivo.Size = New System.Drawing.Size(53, 26)
        Me.txt_numero_documento_consecutivo.TabIndex = 104
        Me.txt_numero_documento_consecutivo.Text = "1"
        Me.txt_numero_documento_consecutivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'documento_multiple
        '
        Me.documento_multiple.AutoSize = True
        Me.documento_multiple.Location = New System.Drawing.Point(21, 93)
        Me.documento_multiple.Name = "documento_multiple"
        Me.documento_multiple.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.documento_multiple.Size = New System.Drawing.Size(124, 21)
        Me.documento_multiple.TabIndex = 5
        Me.documento_multiple.Text = ":Retiro Multiple"
        Me.documento_multiple.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(35, 129)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 17)
        Me.Label6.TabIndex = 102
        Me.Label6.Text = "Calcular por:"
        '
        'cmb_calcular_fob_peso
        '
        Me.cmb_calcular_fob_peso.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmb_calcular_fob_peso.Location = New System.Drawing.Point(128, 127)
        Me.cmb_calcular_fob_peso.Name = "cmb_calcular_fob_peso"
        Me.cmb_calcular_fob_peso.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_calcular_fob_peso.Size = New System.Drawing.Size(354, 22)
        Me.cmb_calcular_fob_peso.TabIndex = 6
        '
        'txt_no_poliza
        '
        Me.txt_no_poliza.BackColor = System.Drawing.Color.White
        Me.txt_no_poliza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_no_poliza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_poliza.Location = New System.Drawing.Point(128, 59)
        Me.txt_no_poliza.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_no_poliza.MaxLength = 10
        Me.txt_no_poliza.Name = "txt_no_poliza"
        Me.txt_no_poliza.Size = New System.Drawing.Size(144, 26)
        Me.txt_no_poliza.TabIndex = 1
        Me.txt_no_poliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(18, 65)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(102, 17)
        Me.Label9.TabIndex = 99
        Me.Label9.Text = "N° Inportacion:"
        '
        'Fecha_de_recepcion
        '
        Me.Fecha_de_recepcion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_de_recepcion.Location = New System.Drawing.Point(354, 60)
        Me.Fecha_de_recepcion.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_de_recepcion.Name = "Fecha_de_recepcion"
        Me.Fecha_de_recepcion.Size = New System.Drawing.Size(128, 22)
        Me.Fecha_de_recepcion.TabIndex = 3
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(284, 65)
        Me.Label32.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(67, 17)
        Me.Label32.TabIndex = 96
        Me.Label32.Text = "Recibido:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rbDolares)
        Me.GroupBox5.Controls.Add(Me.btnTasaCambio)
        Me.GroupBox5.Controls.Add(Me.rbCordobas)
        Me.GroupBox5.Controls.Add(Me.Label31)
        Me.GroupBox5.Controls.Add(Me.txtTasaCambio)
        Me.GroupBox5.Location = New System.Drawing.Point(24, 230)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox5.Size = New System.Drawing.Size(458, 63)
        Me.GroupBox5.TabIndex = 94
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Moneda"
        '
        'rbDolares
        '
        Me.rbDolares.AutoSize = True
        Me.rbDolares.Checked = True
        Me.rbDolares.Enabled = False
        Me.rbDolares.Location = New System.Drawing.Point(112, 27)
        Me.rbDolares.Margin = New System.Windows.Forms.Padding(4)
        Me.rbDolares.Name = "rbDolares"
        Me.rbDolares.Size = New System.Drawing.Size(78, 21)
        Me.rbDolares.TabIndex = 1
        Me.rbDolares.TabStop = True
        Me.rbDolares.Text = "Dolares"
        Me.rbDolares.UseVisualStyleBackColor = True
        '
        'btnTasaCambio
        '
        Me.btnTasaCambio.Enabled = False
        Me.btnTasaCambio.Image = CType(resources.GetObject("btnTasaCambio.Image"), System.Drawing.Image)
        Me.btnTasaCambio.Location = New System.Drawing.Point(387, 23)
        Me.btnTasaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.btnTasaCambio.Name = "btnTasaCambio"
        Me.btnTasaCambio.Size = New System.Drawing.Size(36, 28)
        Me.btnTasaCambio.TabIndex = 61
        Me.btnTasaCambio.UseVisualStyleBackColor = True
        '
        'rbCordobas
        '
        Me.rbCordobas.AutoSize = True
        Me.rbCordobas.Enabled = False
        Me.rbCordobas.Location = New System.Drawing.Point(16, 27)
        Me.rbCordobas.Margin = New System.Windows.Forms.Padding(4)
        Me.rbCordobas.Name = "rbCordobas"
        Me.rbCordobas.Size = New System.Drawing.Size(90, 21)
        Me.rbCordobas.TabIndex = 0
        Me.rbCordobas.Text = "Cordobas"
        Me.rbCordobas.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(192, 30)
        Me.Label31.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(95, 17)
        Me.Label31.TabIndex = 25
        Me.Label31.Text = "Taza Cambio:"
        '
        'txtTasaCambio
        '
        Me.txtTasaCambio.BackColor = System.Drawing.Color.Snow
        Me.txtTasaCambio.Enabled = False
        Me.txtTasaCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTasaCambio.Location = New System.Drawing.Point(291, 25)
        Me.txtTasaCambio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTasaCambio.Name = "txtTasaCambio"
        Me.txtTasaCambio.ReadOnly = True
        Me.txtTasaCambio.Size = New System.Drawing.Size(87, 24)
        Me.txtTasaCambio.TabIndex = 1
        Me.txtTasaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbCredito)
        Me.GroupBox2.Controls.Add(Me.rbContado)
        Me.GroupBox2.Controls.Add(Me.txtPlazo)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(24, 156)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(458, 71)
        Me.GroupBox2.TabIndex = 90
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Compra"
        '
        'rbCredito
        '
        Me.rbCredito.AutoSize = True
        Me.rbCredito.Location = New System.Drawing.Point(123, 31)
        Me.rbCredito.Margin = New System.Windows.Forms.Padding(4)
        Me.rbCredito.Name = "rbCredito"
        Me.rbCredito.Size = New System.Drawing.Size(74, 21)
        Me.rbCredito.TabIndex = 1
        Me.rbCredito.Text = "Credito"
        Me.rbCredito.UseVisualStyleBackColor = True
        '
        'rbContado
        '
        Me.rbContado.AutoSize = True
        Me.rbContado.Checked = True
        Me.rbContado.Location = New System.Drawing.Point(28, 31)
        Me.rbContado.Margin = New System.Windows.Forms.Padding(4)
        Me.rbContado.Name = "rbContado"
        Me.rbContado.Size = New System.Drawing.Size(82, 21)
        Me.rbContado.TabIndex = 0
        Me.rbContado.TabStop = True
        Me.rbContado.Text = "Contado"
        Me.rbContado.UseVisualStyleBackColor = True
        '
        'txtPlazo
        '
        Me.txtPlazo.BackColor = System.Drawing.Color.White
        Me.txtPlazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlazo.Location = New System.Drawing.Point(275, 27)
        Me.txtPlazo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPlazo.MaxLength = 3
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(131, 26)
        Me.txtPlazo.TabIndex = 7
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(219, 33)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 17)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Plazo:"
        '
        'Fecha_de_vencimiento
        '
        Me.Fecha_de_vencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_de_vencimiento.Location = New System.Drawing.Point(354, 93)
        Me.Fecha_de_vencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_de_vencimiento.Name = "Fecha_de_vencimiento"
        Me.Fecha_de_vencimiento.Size = New System.Drawing.Size(128, 22)
        Me.Fecha_de_vencimiento.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(300, 97)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 17)
        Me.Label7.TabIndex = 93
        Me.Label7.Text = "Vence:"
        '
        'Fecha_de_documento
        '
        Me.Fecha_de_documento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_de_documento.Location = New System.Drawing.Point(354, 29)
        Me.Fecha_de_documento.Margin = New System.Windows.Forms.Padding(4)
        Me.Fecha_de_documento.Name = "Fecha_de_documento"
        Me.Fecha_de_documento.Size = New System.Drawing.Size(128, 22)
        Me.Fecha_de_documento.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(25, 297)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 17)
        Me.Label5.TabIndex = 92
        Me.Label5.Text = "Observaciones:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(302, 33)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 17)
        Me.Label4.TabIndex = 91
        Me.Label4.Text = "Fecha:"
        '
        'txtNumDocumento
        '
        Me.txtNumDocumento.BackColor = System.Drawing.Color.Snow
        Me.txtNumDocumento.Enabled = False
        Me.txtNumDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDocumento.Location = New System.Drawing.Point(128, 27)
        Me.txtNumDocumento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNumDocumento.Name = "txtNumDocumento"
        Me.txtNumDocumento.Size = New System.Drawing.Size(144, 26)
        Me.txtNumDocumento.TabIndex = 86
        Me.txtNumDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 33)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 17)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "N° Documento:"
        '
        'pTop_Right
        '
        Me.pTop_Right.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pTop_Right.Controls.Add(Me.porcentaje_poliza_incompletado_actual)
        Me.pTop_Right.Controls.Add(Me.Label23)
        Me.pTop_Right.Controls.Add(Me.porcentaje_poliza_completado_restante)
        Me.pTop_Right.Controls.Add(Me.porcentaje_poliza_completado_actual)
        Me.pTop_Right.Controls.Add(Me.porcentaje_poliza_completado_general)
        Me.pTop_Right.Controls.Add(Me.Label20)
        Me.pTop_Right.Controls.Add(Me.Label18)
        Me.pTop_Right.Controls.Add(Me.Label17)
        Me.pTop_Right.Controls.Add(Me.g_proveedores)
        Me.pTop_Right.Controls.Add(Me.cmdAceptar)
        Me.pTop_Right.Controls.Add(Me.g_impuesto_de_poliza)
        Me.pTop_Right.Controls.Add(Me.g_credito_fiscal)
        Me.pTop_Right.Controls.Add(Me.g_subtotales)
        Me.pTop_Right.Enabled = False
        Me.pTop_Right.Location = New System.Drawing.Point(489, 0)
        Me.pTop_Right.Name = "pTop_Right"
        Me.pTop_Right.Size = New System.Drawing.Size(1435, 371)
        Me.pTop_Right.TabIndex = 85
        '
        'porcentaje_poliza_incompletado_actual
        '
        Me.porcentaje_poliza_incompletado_actual.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.porcentaje_poliza_incompletado_actual.AutoSize = True
        Me.porcentaje_poliza_incompletado_actual.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.porcentaje_poliza_incompletado_actual.Location = New System.Drawing.Point(1345, 339)
        Me.porcentaje_poliza_incompletado_actual.Name = "porcentaje_poliza_incompletado_actual"
        Me.porcentaje_poliza_incompletado_actual.Size = New System.Drawing.Size(58, 17)
        Me.porcentaje_poliza_incompletado_actual.TabIndex = 99
        Me.porcentaje_poliza_incompletado_actual.Text = "0.00 %"
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.SlateBlue
        Me.Label23.Location = New System.Drawing.Point(977, 339)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(305, 17)
        Me.Label23.TabIndex = 98
        Me.Label23.Text = "Porcentaje no liquidado de poliza actual:"
        '
        'porcentaje_poliza_completado_restante
        '
        Me.porcentaje_poliza_completado_restante.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.porcentaje_poliza_completado_restante.AutoSize = True
        Me.porcentaje_poliza_completado_restante.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.porcentaje_poliza_completado_restante.Location = New System.Drawing.Point(1345, 288)
        Me.porcentaje_poliza_completado_restante.Name = "porcentaje_poliza_completado_restante"
        Me.porcentaje_poliza_completado_restante.Size = New System.Drawing.Size(58, 17)
        Me.porcentaje_poliza_completado_restante.TabIndex = 97
        Me.porcentaje_poliza_completado_restante.Text = "0.00 %"
        '
        'porcentaje_poliza_completado_actual
        '
        Me.porcentaje_poliza_completado_actual.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.porcentaje_poliza_completado_actual.AutoSize = True
        Me.porcentaje_poliza_completado_actual.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.porcentaje_poliza_completado_actual.Location = New System.Drawing.Point(1345, 312)
        Me.porcentaje_poliza_completado_actual.Name = "porcentaje_poliza_completado_actual"
        Me.porcentaje_poliza_completado_actual.Size = New System.Drawing.Size(58, 17)
        Me.porcentaje_poliza_completado_actual.TabIndex = 96
        Me.porcentaje_poliza_completado_actual.Text = "0.00 %"
        '
        'porcentaje_poliza_completado_general
        '
        Me.porcentaje_poliza_completado_general.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.porcentaje_poliza_completado_general.AutoSize = True
        Me.porcentaje_poliza_completado_general.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.porcentaje_poliza_completado_general.Location = New System.Drawing.Point(1345, 261)
        Me.porcentaje_poliza_completado_general.Name = "porcentaje_poliza_completado_general"
        Me.porcentaje_poliza_completado_general.Size = New System.Drawing.Size(58, 17)
        Me.porcentaje_poliza_completado_general.TabIndex = 95
        Me.porcentaje_poliza_completado_general.Text = "0.00 %"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.SlateBlue
        Me.Label20.Location = New System.Drawing.Point(977, 288)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(360, 17)
        Me.Label20.TabIndex = 94
        Me.Label20.Text = "Porcentaje general de poliza restante a liquidar:"
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.SlateBlue
        Me.Label18.Location = New System.Drawing.Point(977, 312)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(282, 17)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "Porcentaje liquidado de poliza actual:"
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.SlateBlue
        Me.Label17.Location = New System.Drawing.Point(977, 262)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(293, 17)
        Me.Label17.TabIndex = 92
        Me.Label17.Text = "Porcentaje general de poliza liquidada:"
        '
        'g_proveedores
        '
        Me.g_proveedores.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.g_proveedores.Controls.Add(Me.p_proveedores_botones)
        Me.g_proveedores.Controls.Add(Me.Proveedores)
        Me.g_proveedores.Location = New System.Drawing.Point(624, 19)
        Me.g_proveedores.Name = "g_proveedores"
        Me.g_proveedores.Size = New System.Drawing.Size(796, 239)
        Me.g_proveedores.TabIndex = 91
        Me.g_proveedores.TabStop = False
        Me.g_proveedores.Text = "Proveedores"
        '
        'p_proveedores_botones
        '
        Me.p_proveedores_botones.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.p_proveedores_botones.Controls.Add(Me.btn_remover_proveedor)
        Me.p_proveedores_botones.Controls.Add(Me.btn_modificar_proveedor)
        Me.p_proveedores_botones.Controls.Add(Me.btn_agregar_proveedores)
        Me.p_proveedores_botones.Location = New System.Drawing.Point(430, 203)
        Me.p_proveedores_botones.Name = "p_proveedores_botones"
        Me.p_proveedores_botones.Size = New System.Drawing.Size(351, 30)
        Me.p_proveedores_botones.TabIndex = 74
        '
        'btn_remover_proveedor
        '
        Me.btn_remover_proveedor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_remover_proveedor.Appearance.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_remover_proveedor.Appearance.ForeColor = System.Drawing.Color.Red
        Me.btn_remover_proveedor.Appearance.Options.UseFont = True
        Me.btn_remover_proveedor.Appearance.Options.UseForeColor = True
        Me.btn_remover_proveedor.Location = New System.Drawing.Point(246, 1)
        Me.btn_remover_proveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_remover_proveedor.Name = "btn_remover_proveedor"
        Me.btn_remover_proveedor.Size = New System.Drawing.Size(100, 27)
        Me.btn_remover_proveedor.TabIndex = 21
        Me.btn_remover_proveedor.Text = "(-) Remover"
        '
        'btn_modificar_proveedor
        '
        Me.btn_modificar_proveedor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_modificar_proveedor.Appearance.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_modificar_proveedor.Appearance.ForeColor = System.Drawing.Color.Green
        Me.btn_modificar_proveedor.Appearance.Options.UseFont = True
        Me.btn_modificar_proveedor.Appearance.Options.UseForeColor = True
        Me.btn_modificar_proveedor.Location = New System.Drawing.Point(132, 2)
        Me.btn_modificar_proveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_modificar_proveedor.Name = "btn_modificar_proveedor"
        Me.btn_modificar_proveedor.Size = New System.Drawing.Size(106, 27)
        Me.btn_modificar_proveedor.TabIndex = 20
        Me.btn_modificar_proveedor.Text = "(!) Modificar  "
        '
        'btn_agregar_proveedores
        '
        Me.btn_agregar_proveedores.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_agregar_proveedores.Appearance.Font = New System.Drawing.Font("Tahoma", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_agregar_proveedores.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_agregar_proveedores.Appearance.Options.UseFont = True
        Me.btn_agregar_proveedores.Appearance.Options.UseForeColor = True
        Me.btn_agregar_proveedores.Location = New System.Drawing.Point(17, 2)
        Me.btn_agregar_proveedores.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_agregar_proveedores.Name = "btn_agregar_proveedores"
        Me.btn_agregar_proveedores.Size = New System.Drawing.Size(107, 27)
        Me.btn_agregar_proveedores.TabIndex = 19
        Me.btn_agregar_proveedores.Text = "(+) Agregar"
        '
        'Proveedores
        '
        Me.Proveedores.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Proveedores.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.Proveedores.Location = New System.Drawing.Point(11, 22)
        Me.Proveedores.MainView = Me.Proveedores_vista
        Me.Proveedores.Margin = New System.Windows.Forms.Padding(4)
        Me.Proveedores.Name = "Proveedores"
        Me.Proveedores.Size = New System.Drawing.Size(770, 179)
        Me.Proveedores.TabIndex = 73
        Me.Proveedores.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.Proveedores_vista})
        '
        'Proveedores_vista
        '
        Me.Proveedores_vista.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.Proveedores_vista.GridControl = Me.Proveedores
        Me.Proveedores_vista.GroupFormat = ""
        Me.Proveedores_vista.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.Proveedores_vista.Name = "Proveedores_vista"
        Me.Proveedores_vista.OptionsBehavior.Editable = False
        Me.Proveedores_vista.OptionsBehavior.ReadOnly = True
        Me.Proveedores_vista.OptionsCustomization.AllowColumnMoving = False
        Me.Proveedores_vista.OptionsCustomization.AllowColumnResizing = False
        Me.Proveedores_vista.OptionsCustomization.AllowGroup = False
        Me.Proveedores_vista.OptionsMenu.EnableGroupPanelMenu = False
        Me.Proveedores_vista.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.Proveedores_vista.OptionsView.ShowFooter = True
        Me.Proveedores_vista.OptionsView.ShowGroupPanel = False
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(726, 324)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(211, 32)
        Me.cmdAceptar.TabIndex = 90
        Me.cmdAceptar.Text = "&Ver pagos relacionados"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        Me.cmdAceptar.Visible = False
        '
        'g_impuesto_de_poliza
        '
        Me.g_impuesto_de_poliza.Controls.Add(Me.btn_TSI)
        Me.g_impuesto_de_poliza.Controls.Add(Me.btn_DAI)
        Me.g_impuesto_de_poliza.Controls.Add(Me.btn_ISC)
        Me.g_impuesto_de_poliza.Controls.Add(Me.btn_SPE)
        Me.g_impuesto_de_poliza.Controls.Add(Me.btn_SSA)
        Me.g_impuesto_de_poliza.Controls.Add(Me.txtSSA)
        Me.g_impuesto_de_poliza.Controls.Add(Me.Label28)
        Me.g_impuesto_de_poliza.Controls.Add(Me.txtTSI)
        Me.g_impuesto_de_poliza.Controls.Add(Me.Label11)
        Me.g_impuesto_de_poliza.Controls.Add(Me.txtSPE)
        Me.g_impuesto_de_poliza.Controls.Add(Me.txtISC)
        Me.g_impuesto_de_poliza.Controls.Add(Me.txtDAI)
        Me.g_impuesto_de_poliza.Controls.Add(Me.Label14)
        Me.g_impuesto_de_poliza.Controls.Add(Me.Label15)
        Me.g_impuesto_de_poliza.Controls.Add(Me.Label13)
        Me.g_impuesto_de_poliza.Location = New System.Drawing.Point(7, 15)
        Me.g_impuesto_de_poliza.Margin = New System.Windows.Forms.Padding(4)
        Me.g_impuesto_de_poliza.Name = "g_impuesto_de_poliza"
        Me.g_impuesto_de_poliza.Padding = New System.Windows.Forms.Padding(4)
        Me.g_impuesto_de_poliza.Size = New System.Drawing.Size(610, 99)
        Me.g_impuesto_de_poliza.TabIndex = 88
        Me.g_impuesto_de_poliza.TabStop = False
        Me.g_impuesto_de_poliza.Text = "Impuesto en Poliza"
        '
        'btn_TSI
        '
        Me.btn_TSI.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_TSI.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_TSI.Appearance.Options.UseFont = True
        Me.btn_TSI.Appearance.Options.UseForeColor = True
        Me.btn_TSI.Location = New System.Drawing.Point(376, 25)
        Me.btn_TSI.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_TSI.Name = "btn_TSI"
        Me.btn_TSI.Size = New System.Drawing.Size(27, 23)
        Me.btn_TSI.TabIndex = 10
        Me.btn_TSI.Text = "+"
        Me.btn_TSI.ToolTip = "Agregar un nuevo Departamento"
        '
        'btn_DAI
        '
        Me.btn_DAI.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_DAI.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_DAI.Appearance.Options.UseFont = True
        Me.btn_DAI.Appearance.Options.UseForeColor = True
        Me.btn_DAI.Location = New System.Drawing.Point(575, 58)
        Me.btn_DAI.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_DAI.Name = "btn_DAI"
        Me.btn_DAI.Size = New System.Drawing.Size(27, 23)
        Me.btn_DAI.TabIndex = 13
        Me.btn_DAI.Text = "+"
        Me.btn_DAI.ToolTip = "Agregar un nuevo Departamento"
        '
        'btn_ISC
        '
        Me.btn_ISC.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ISC.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_ISC.Appearance.Options.UseFont = True
        Me.btn_ISC.Appearance.Options.UseForeColor = True
        Me.btn_ISC.Location = New System.Drawing.Point(376, 59)
        Me.btn_ISC.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_ISC.Name = "btn_ISC"
        Me.btn_ISC.Size = New System.Drawing.Size(27, 23)
        Me.btn_ISC.TabIndex = 12
        Me.btn_ISC.Text = "+"
        Me.btn_ISC.ToolTip = "Agregar un nuevo Departamento"
        '
        'btn_SPE
        '
        Me.btn_SPE.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_SPE.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_SPE.Appearance.Options.UseFont = True
        Me.btn_SPE.Appearance.Options.UseForeColor = True
        Me.btn_SPE.Location = New System.Drawing.Point(176, 58)
        Me.btn_SPE.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_SPE.Name = "btn_SPE"
        Me.btn_SPE.Size = New System.Drawing.Size(27, 23)
        Me.btn_SPE.TabIndex = 11
        Me.btn_SPE.Text = "+"
        Me.btn_SPE.ToolTip = "Agregar un nuevo Departamento"
        '
        'btn_SSA
        '
        Me.btn_SSA.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_SSA.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_SSA.Appearance.Options.UseFont = True
        Me.btn_SSA.Appearance.Options.UseForeColor = True
        Me.btn_SSA.Location = New System.Drawing.Point(176, 26)
        Me.btn_SSA.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_SSA.Name = "btn_SSA"
        Me.btn_SSA.Size = New System.Drawing.Size(27, 23)
        Me.btn_SSA.TabIndex = 9
        Me.btn_SSA.Text = "+"
        Me.btn_SSA.ToolTip = "Agregar un nuevo Departamento"
        '
        'txtSSA
        '
        Me.txtSSA.BackColor = System.Drawing.Color.White
        Me.txtSSA.Enabled = False
        Me.txtSSA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSA.Location = New System.Drawing.Point(47, 25)
        Me.txtSSA.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSSA.MaxLength = 15
        Me.txtSSA.Name = "txtSSA"
        Me.txtSSA.Size = New System.Drawing.Size(121, 24)
        Me.txtSSA.TabIndex = 36
        Me.txtSSA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(3, 30)
        Me.Label28.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(39, 17)
        Me.Label28.TabIndex = 37
        Me.Label28.Text = "SSA:"
        '
        'txtTSI
        '
        Me.txtTSI.BackColor = System.Drawing.Color.White
        Me.txtTSI.Enabled = False
        Me.txtTSI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTSI.Location = New System.Drawing.Point(247, 23)
        Me.txtTSI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTSI.MaxLength = 15
        Me.txtTSI.Name = "txtTSI"
        Me.txtTSI.Size = New System.Drawing.Size(121, 24)
        Me.txtTSI.TabIndex = 0
        Me.txtTSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(216, 28)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(33, 17)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "TSI:"
        '
        'txtSPE
        '
        Me.txtSPE.BackColor = System.Drawing.Color.White
        Me.txtSPE.Enabled = False
        Me.txtSPE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSPE.Location = New System.Drawing.Point(47, 57)
        Me.txtSPE.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSPE.MaxLength = 15
        Me.txtSPE.Name = "txtSPE"
        Me.txtSPE.Size = New System.Drawing.Size(121, 24)
        Me.txtSPE.TabIndex = 1
        Me.txtSPE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtISC
        '
        Me.txtISC.BackColor = System.Drawing.Color.White
        Me.txtISC.Enabled = False
        Me.txtISC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtISC.Location = New System.Drawing.Point(247, 57)
        Me.txtISC.Margin = New System.Windows.Forms.Padding(4)
        Me.txtISC.MaxLength = 15
        Me.txtISC.Name = "txtISC"
        Me.txtISC.Size = New System.Drawing.Size(121, 24)
        Me.txtISC.TabIndex = 3
        Me.txtISC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDAI
        '
        Me.txtDAI.BackColor = System.Drawing.Color.White
        Me.txtDAI.Enabled = False
        Me.txtDAI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDAI.Location = New System.Drawing.Point(446, 57)
        Me.txtDAI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDAI.MaxLength = 15
        Me.txtDAI.Name = "txtDAI"
        Me.txtDAI.Size = New System.Drawing.Size(121, 24)
        Me.txtDAI.TabIndex = 2
        Me.txtDAI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(411, 62)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 17)
        Me.Label14.TabIndex = 33
        Me.Label14.Text = "DAI:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(212, 62)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(33, 17)
        Me.Label15.TabIndex = 35
        Me.Label15.Text = "ISC:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 62)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(39, 17)
        Me.Label13.TabIndex = 31
        Me.Label13.Text = "SPE:"
        '
        'g_credito_fiscal
        '
        Me.g_credito_fiscal.Controls.Add(Me.btn_Poliza)
        Me.g_credito_fiscal.Controls.Add(Me.btn_almacen)
        Me.g_credito_fiscal.Controls.Add(Me.txtAlmacen)
        Me.g_credito_fiscal.Controls.Add(Me.Label2)
        Me.g_credito_fiscal.Controls.Add(Me.txtFletesTerrestre)
        Me.g_credito_fiscal.Controls.Add(Me.btn_transporte)
        Me.g_credito_fiscal.Controls.Add(Me.btn_aduana)
        Me.g_credito_fiscal.Controls.Add(Me.Label27)
        Me.g_credito_fiscal.Controls.Add(Me.txtAduana)
        Me.g_credito_fiscal.Controls.Add(Me.Label26)
        Me.g_credito_fiscal.Controls.Add(Me.txtIVAPoliza)
        Me.g_credito_fiscal.Controls.Add(Me.Label16)
        Me.g_credito_fiscal.Location = New System.Drawing.Point(7, 122)
        Me.g_credito_fiscal.Margin = New System.Windows.Forms.Padding(4)
        Me.g_credito_fiscal.Name = "g_credito_fiscal"
        Me.g_credito_fiscal.Padding = New System.Windows.Forms.Padding(4)
        Me.g_credito_fiscal.Size = New System.Drawing.Size(301, 159)
        Me.g_credito_fiscal.TabIndex = 87
        Me.g_credito_fiscal.TabStop = False
        Me.g_credito_fiscal.Text = "Credito Fiscal"
        '
        'btn_Poliza
        '
        Me.btn_Poliza.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Poliza.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_Poliza.Appearance.Options.UseFont = True
        Me.btn_Poliza.Appearance.Options.UseForeColor = True
        Me.btn_Poliza.Location = New System.Drawing.Point(264, 22)
        Me.btn_Poliza.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Poliza.Name = "btn_Poliza"
        Me.btn_Poliza.Size = New System.Drawing.Size(27, 23)
        Me.btn_Poliza.TabIndex = 14
        Me.btn_Poliza.Text = "+"
        Me.btn_Poliza.ToolTip = "Agregar un nuevo Departamento"
        '
        'btn_almacen
        '
        Me.btn_almacen.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_almacen.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_almacen.Appearance.Options.UseFont = True
        Me.btn_almacen.Appearance.Options.UseForeColor = True
        Me.btn_almacen.Location = New System.Drawing.Point(264, 117)
        Me.btn_almacen.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_almacen.Name = "btn_almacen"
        Me.btn_almacen.Size = New System.Drawing.Size(27, 23)
        Me.btn_almacen.TabIndex = 17
        Me.btn_almacen.Text = "+"
        Me.btn_almacen.ToolTip = "Agregar un nuevo Departamento"
        '
        'txtAlmacen
        '
        Me.txtAlmacen.BackColor = System.Drawing.Color.White
        Me.txtAlmacen.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtAlmacen.Enabled = False
        Me.txtAlmacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlmacen.Location = New System.Drawing.Point(101, 117)
        Me.txtAlmacen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAlmacen.MaxLength = 15
        Me.txtAlmacen.Name = "txtAlmacen"
        Me.txtAlmacen.Size = New System.Drawing.Size(155, 26)
        Me.txtAlmacen.TabIndex = 45
        Me.txtAlmacen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 123)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 17)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Almacen:"
        '
        'txtFletesTerrestre
        '
        Me.txtFletesTerrestre.BackColor = System.Drawing.Color.White
        Me.txtFletesTerrestre.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtFletesTerrestre.Enabled = False
        Me.txtFletesTerrestre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFletesTerrestre.Location = New System.Drawing.Point(101, 85)
        Me.txtFletesTerrestre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFletesTerrestre.MaxLength = 15
        Me.txtFletesTerrestre.Name = "txtFletesTerrestre"
        Me.txtFletesTerrestre.Size = New System.Drawing.Size(155, 26)
        Me.txtFletesTerrestre.TabIndex = 2
        Me.txtFletesTerrestre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btn_transporte
        '
        Me.btn_transporte.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_transporte.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_transporte.Appearance.Options.UseFont = True
        Me.btn_transporte.Appearance.Options.UseForeColor = True
        Me.btn_transporte.Location = New System.Drawing.Point(264, 86)
        Me.btn_transporte.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_transporte.Name = "btn_transporte"
        Me.btn_transporte.Size = New System.Drawing.Size(27, 23)
        Me.btn_transporte.TabIndex = 16
        Me.btn_transporte.Text = "+"
        Me.btn_transporte.ToolTip = "Agregar un nuevo Departamento"
        '
        'btn_aduana
        '
        Me.btn_aduana.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_aduana.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_aduana.Appearance.Options.UseFont = True
        Me.btn_aduana.Appearance.Options.UseForeColor = True
        Me.btn_aduana.Location = New System.Drawing.Point(264, 54)
        Me.btn_aduana.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_aduana.Name = "btn_aduana"
        Me.btn_aduana.Size = New System.Drawing.Size(27, 23)
        Me.btn_aduana.TabIndex = 15
        Me.btn_aduana.Text = "+"
        Me.btn_aduana.ToolTip = "Agregar un nuevo Departamento"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(16, 91)
        Me.Label27.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(82, 17)
        Me.Label27.TabIndex = 41
        Me.Label27.Text = "Transporte:"
        '
        'txtAduana
        '
        Me.txtAduana.BackColor = System.Drawing.Color.White
        Me.txtAduana.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtAduana.Enabled = False
        Me.txtAduana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAduana.Location = New System.Drawing.Point(101, 53)
        Me.txtAduana.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAduana.MaxLength = 15
        Me.txtAduana.Name = "txtAduana"
        Me.txtAduana.Size = New System.Drawing.Size(155, 26)
        Me.txtAduana.TabIndex = 38
        Me.txtAduana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(4, 59)
        Me.Label26.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(94, 17)
        Me.Label26.TabIndex = 39
        Me.Label26.Text = "Agente Adu..:"
        '
        'txtIVAPoliza
        '
        Me.txtIVAPoliza.BackColor = System.Drawing.Color.White
        Me.txtIVAPoliza.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtIVAPoliza.Enabled = False
        Me.txtIVAPoliza.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIVAPoliza.Location = New System.Drawing.Point(101, 22)
        Me.txtIVAPoliza.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIVAPoliza.MaxLength = 15
        Me.txtIVAPoliza.Name = "txtIVAPoliza"
        Me.txtIVAPoliza.Size = New System.Drawing.Size(155, 26)
        Me.txtIVAPoliza.TabIndex = 0
        Me.txtIVAPoliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(4, 28)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(94, 17)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "DGA (Poliza):"
        '
        'g_subtotales
        '
        Me.g_subtotales.Controls.Add(Me.txtOtroAlmacen)
        Me.g_subtotales.Controls.Add(Me.Label12)
        Me.g_subtotales.Controls.Add(Me.btn_otropago)
        Me.g_subtotales.Controls.Add(Me.txtOAduana)
        Me.g_subtotales.Controls.Add(Me.Label3)
        Me.g_subtotales.Controls.Add(Me.txtOTransporte)
        Me.g_subtotales.Controls.Add(Me.Label21)
        Me.g_subtotales.Controls.Add(Me.txtOtroPago)
        Me.g_subtotales.Controls.Add(Me.Label19)
        Me.g_subtotales.Location = New System.Drawing.Point(316, 122)
        Me.g_subtotales.Margin = New System.Windows.Forms.Padding(4)
        Me.g_subtotales.Name = "g_subtotales"
        Me.g_subtotales.Padding = New System.Windows.Forms.Padding(4)
        Me.g_subtotales.Size = New System.Drawing.Size(301, 159)
        Me.g_subtotales.TabIndex = 89
        Me.g_subtotales.TabStop = False
        Me.g_subtotales.Text = "SubTotales"
        '
        'txtOtroAlmacen
        '
        Me.txtOtroAlmacen.BackColor = System.Drawing.Color.White
        Me.txtOtroAlmacen.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtOtroAlmacen.Enabled = False
        Me.txtOtroAlmacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtroAlmacen.Location = New System.Drawing.Point(101, 117)
        Me.txtOtroAlmacen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOtroAlmacen.MaxLength = 15
        Me.txtOtroAlmacen.Name = "txtOtroAlmacen"
        Me.txtOtroAlmacen.Size = New System.Drawing.Size(155, 26)
        Me.txtOtroAlmacen.TabIndex = 49
        Me.txtOtroAlmacen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 123)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(66, 17)
        Me.Label12.TabIndex = 50
        Me.Label12.Text = "Almacen:"
        '
        'btn_otropago
        '
        Me.btn_otropago.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_otropago.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.btn_otropago.Appearance.Options.UseFont = True
        Me.btn_otropago.Appearance.Options.UseForeColor = True
        Me.btn_otropago.Location = New System.Drawing.Point(265, 23)
        Me.btn_otropago.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_otropago.Name = "btn_otropago"
        Me.btn_otropago.Size = New System.Drawing.Size(27, 23)
        Me.btn_otropago.TabIndex = 18
        Me.btn_otropago.Text = "+"
        '
        'txtOAduana
        '
        Me.txtOAduana.BackColor = System.Drawing.Color.White
        Me.txtOAduana.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtOAduana.Enabled = False
        Me.txtOAduana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOAduana.Location = New System.Drawing.Point(101, 53)
        Me.txtOAduana.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOAduana.MaxLength = 15
        Me.txtOAduana.Name = "txtOAduana"
        Me.txtOAduana.Size = New System.Drawing.Size(155, 26)
        Me.txtOAduana.TabIndex = 1
        Me.txtOAduana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 60)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 17)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Agente Adu..:"
        '
        'txtOTransporte
        '
        Me.txtOTransporte.BackColor = System.Drawing.Color.White
        Me.txtOTransporte.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtOTransporte.Enabled = False
        Me.txtOTransporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOTransporte.Location = New System.Drawing.Point(101, 85)
        Me.txtOTransporte.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOTransporte.MaxLength = 15
        Me.txtOTransporte.Name = "txtOTransporte"
        Me.txtOTransporte.Size = New System.Drawing.Size(155, 26)
        Me.txtOTransporte.TabIndex = 2
        Me.txtOTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(17, 93)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(82, 17)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "Transporte:"
        '
        'txtOtroPago
        '
        Me.txtOtroPago.BackColor = System.Drawing.Color.White
        Me.txtOtroPago.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtOtroPago.Enabled = False
        Me.txtOtroPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtroPago.Location = New System.Drawing.Point(101, 22)
        Me.txtOtroPago.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOtroPago.MaxLength = 15
        Me.txtOtroPago.Name = "txtOtroPago"
        Me.txtOtroPago.Size = New System.Drawing.Size(155, 26)
        Me.txtOtroPago.TabIndex = 0
        Me.txtOtroPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(17, 30)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(47, 17)
        Me.Label19.TabIndex = 43
        Me.Label19.Text = "Otros:"
        '
        'compra_foranea
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1924, 756)
        Me.ControlBox = False
        Me.Controls.Add(Me.pMain)
        Me.Name = "compra_foranea"
        Me.Text = "CompForaneas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pMain.ResumeLayout(False)
        Me.pBody.ResumeLayout(False)
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Vista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDown.ResumeLayout(False)
        Me.pButtons.ResumeLayout(False)
        Me.pTop.ResumeLayout(False)
        Me.pTop_Left.ResumeLayout(False)
        Me.pTop_Left.PerformLayout()
        CType(Me.cmb_calcular_fob_peso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.pTop_Right.ResumeLayout(False)
        Me.pTop_Right.PerformLayout()
        Me.g_proveedores.ResumeLayout(False)
        Me.p_proveedores_botones.ResumeLayout(False)
        CType(Me.Proveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Proveedores_vista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.g_impuesto_de_poliza.ResumeLayout(False)
        Me.g_impuesto_de_poliza.PerformLayout()
        Me.g_credito_fiscal.ResumeLayout(False)
        Me.g_credito_fiscal.PerformLayout()
        Me.g_subtotales.ResumeLayout(False)
        Me.g_subtotales.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents pDown As System.Windows.Forms.Panel
    Friend WithEvents pTop As System.Windows.Forms.Panel
    Friend WithEvents pTop_Left As System.Windows.Forms.Panel
    Friend WithEvents Fecha_de_recepcion As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rbDolares As System.Windows.Forms.RadioButton
    Friend WithEvents btnTasaCambio As System.Windows.Forms.Button
    Friend WithEvents rbCordobas As System.Windows.Forms.RadioButton
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtTasaCambio As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rbContado As System.Windows.Forms.RadioButton
    Friend WithEvents txtPlazo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Fecha_de_vencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Fecha_de_documento As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pTop_Right As System.Windows.Forms.Panel
    Friend WithEvents g_proveedores As System.Windows.Forms.GroupBox
    Friend WithEvents Proveedores As DevExpress.XtraGrid.GridControl
    Friend WithEvents Proveedores_vista As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents g_impuesto_de_poliza As System.Windows.Forms.GroupBox
    Friend WithEvents txtSSA As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtTSI As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSPE As System.Windows.Forms.TextBox
    Friend WithEvents txtISC As System.Windows.Forms.TextBox
    Friend WithEvents txtDAI As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents g_credito_fiscal As System.Windows.Forms.GroupBox
    Friend WithEvents btn_almacen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtAlmacen As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFletesTerrestre As System.Windows.Forms.TextBox
    Friend WithEvents btn_transporte As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_aduana As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtAduana As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtIVAPoliza As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents g_subtotales As System.Windows.Forms.GroupBox
    Friend WithEvents txtOtroAlmacen As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btn_otropago As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtOAduana As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtOTransporte As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtOtroPago As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btn_Poliza As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_DAI As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_ISC As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_SPE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_SSA As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_TSI As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pButtons As System.Windows.Forms.Panel
    Friend WithEvents btn_producto_eliminar As System.Windows.Forms.Button
    Friend WithEvents btn_producto_modificar As System.Windows.Forms.Button
    Friend WithEvents btn_producto_agregar As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_numero_documento_consecutivo As System.Windows.Forms.TextBox
    Friend WithEvents documento_multiple As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmb_calcular_fob_peso As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txt_no_poliza As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents p_proveedores_botones As System.Windows.Forms.Panel
    Friend WithEvents btn_remover_proveedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_modificar_proveedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_agregar_proveedores As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents Vista As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents porcentaje_poliza_completado_restante As System.Windows.Forms.Label
    Friend WithEvents porcentaje_poliza_completado_actual As System.Windows.Forms.Label
    Friend WithEvents porcentaje_poliza_completado_general As System.Windows.Forms.Label
    Friend WithEvents porcentaje_poliza_incompletado_actual As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents btn_aplicar As DevExpress.XtraEditors.SimpleButton
End Class
