﻿Public Class compra_foranea_convertir_moneda

    Private convertir As New convertir_moneda

    Private tipo_de_cambio As Boolean

    Private ValidarCapital As New Validar

    Private proceso As Boolean = False

    Public ReadOnly Property get_estado_de_proceso As Boolean
        Get
            Return Me.proceso
        End Get
    End Property

    Public Sub New(fecha As Date, tasa_de_cambio As Decimal, dolar_o_cordoba As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Me.tipo_de_cambio = tipo_de_cambio

        convertir = New convertir_moneda(fecha, tasa_de_cambio, 0)

        start()

    End Sub

    Private Sub start()

        picker_fecha.DateTime = convertir.get_fecha
        txt_tasa_de_cambio.Text = FormatNumber(convertir.get_tasa_de_cambio, 4)
        txt_capital.Text = FormatNumber(convertir.get_capital, 4)
        txt_monto.Text = FormatNumber(convertir.get_monto_cordoba, 4)

        AddHandler txt_capital.TextChanged, AddressOf get_capital
        AddHandler btn_aceptar.Click, AddressOf aceptar
        AddHandler btn_cancelar.Click, AddressOf cancelar
        AddHandler picker_fecha.TextChanged, AddressOf get_tasa_de_cambio_por_fecha

        ValidarCapital.agregar_control(New List(Of Control)({txt_capital})).ActivarEventosNumericos()

        picker_fecha.Focus()

    End Sub
    Private Sub get_tasa_de_cambio_por_fecha()

        txt_tasa_de_cambio.Text = FormatNumber(data_tasa_de_cambio.get_tasa_de_cambio_actual(Funciones.Param("@Fecha", picker_fecha.EditValue, SqlDbType.Date)), 4)

        convertir = New convertir_moneda(picker_fecha.EditValue, ChangeToDouble(txt_tasa_de_cambio.Text), 0)

        get_capital()

    End Sub
    Private Sub aceptar()

        proceso = True

        Me.Close()

    End Sub

    Private Sub cancelar()

        proceso = False

        Me.Close()

    End Sub

    Private Sub get_capital()

        convertir.get_capital = ChangeToDouble(txt_capital.Text)

        set_monto()

    End Sub
    Private Sub set_monto()

        txt_monto.Text = FormatNumber(get_monto_cordoba(), 4)

    End Sub

    Public Function get_monto_cordoba() As Decimal

        Return convertir.get_monto_cordoba

    End Function

    Private MustInherit Class dolares_a_cordobas

        Protected fecha As Date
        Protected tasa_de_cambio As Decimal
        Protected capital As Decimal
        Protected monto As Decimal

        Public Property get_fecha As Date
            Get
                Return Me.fecha
            End Get
            Set(value As Date)
                Me.fecha = value
            End Set
        End Property
        Public Property get_tasa_de_cambio As Decimal
            Get
                Return Me.tasa_de_cambio
            End Get
            Set(value As Decimal)
                Me.tasa_de_cambio = value
            End Set
        End Property
        Public Property get_capital As Decimal
            Get
                Return Me.capital
            End Get
            Set(value As Decimal)
                Me.capital = value
            End Set
        End Property
        Public ReadOnly Property get_monto_cordoba
            Get
                Return IIf(tasa_de_cambio = 0, 0, (Me.capital * Me.tasa_de_cambio))
            End Get
        End Property

    End Class

    Private Class convertir_moneda

        Inherits dolares_a_cordobas

        Public Sub New()

        End Sub

        Public Sub New(fecha As Date, tasa_de_cambio As Decimal, capital As Decimal)

            Me.fecha = fecha
            Me.tasa_de_cambio = tasa_de_cambio
            Me.capital = capital

        End Sub

        Public ReadOnly Property get_monto_dolar
            Get
                Return IIf(Me.tasa_de_cambio = 0, 0, (Me.capital / Me.tasa_de_cambio))
            End Get
        End Property

    End Class
End Class