﻿Public Class impuesto

    Public Structure key_identificador

        Public sucursal As Integer
        Public numero_de_documento As String
        Public tipo_documento As String

    End Structure

    Private identificadores As New key_identificador
    Private numero_de_documento_digitado As String
    Private Fecha_documento_emitido As Date
    Private Fecha_documento_vencimiento As Date
    Private numero_de_referencia As String
    Private tipo_de_referencia As String
    Private proveedor As String

    Private sub_total As Decimal
    Private ir As Decimal
    Private imi As Decimal
    Private iva As Decimal

    Private total_neto As Decimal

    Private otros_gastos As Decimal
    Private multas_y_recargos As Decimal
    Private otros_impuestos As Decimal

    Private neto_a_pagar As Decimal

    Public Sub New()

    End Sub

    Public Sub New(identificador As impuesto.key_identificador)

        identificadores = identificador

    End Sub

    Public Sub New(
                    sucursal As Integer,
                    numero_de_documento As String,
                    numero_de_documento_digitado As String,
                    tipo_documento As String,
                    Fecha_documento_emitido As Date,
                    Fecha_documento_vencimiento As Date,
                    numero_de_referencia As String,
                    tipo_de_referencia As String,
                    proveedor As String,
                    sub_total As Decimal,
                    ir As Decimal,
                    imi As Decimal,
                    iva As Decimal,
                    total_neto As Decimal,
                    otros_gastos As Decimal,
                    multas_y_recargos As Decimal,
                    otros_impuestos As Decimal,
                    neto_a_pagar As Decimal)

        Me.identificadores.sucursal = sucursal
        Me.identificadores.numero_de_documento = numero_de_documento
        Me.numero_de_documento_digitado = numero_de_documento_digitado
        Me.identificadores.tipo_documento = tipo_documento
        Me.Fecha_documento_emitido = Fecha_documento_emitido
        Me.Fecha_documento_vencimiento = Fecha_documento_vencimiento
        Me.numero_de_referencia = numero_de_referencia
        Me.tipo_de_referencia = tipo_de_referencia
        Me.proveedor = proveedor
        Me.sub_total = sub_total
        Me.ir = ir
        Me.imi = imi
        Me.iva = iva
        Me.total_neto = total_neto
        Me.otros_gastos = otros_gastos
        Me.multas_y_recargos = multas_y_recargos
        Me.otros_impuestos = otros_impuestos
        Me.neto_a_pagar = neto_a_pagar


    End Sub

    Public Property get_sucursal As Integer
        Get
            Return Me.identificadores.sucursal
        End Get
        Set(value As Integer)
            Me.identificadores.sucursal = value
        End Set
    End Property
    Public Property get_numero_de_documento As String
        Get
            Return Me.identificadores.numero_de_documento
        End Get
        Set(value As String)
            Me.identificadores.numero_de_documento = value
        End Set
    End Property
    Public Property get_tipo_documento As String
        Get
            Return Me.identificadores.tipo_documento
        End Get
        Set(value As String)
            Me.identificadores.tipo_documento = value
        End Set
    End Property
    Public Property get_numero_de_documento_digitado As String
        Get
            Return Me.numero_de_documento_digitado
        End Get
        Set(value As String)
            numero_de_documento_digitado = value
        End Set
    End Property
    Public Property get_Fecha_documento_emitido As Date
        Get
            Return Me.Fecha_documento_emitido
        End Get
        Set(value As Date)
            Me.Fecha_documento_emitido = value
        End Set
    End Property
    Public Property get_Fecha_documento_vencimiento As Date
        Get
            Return Me.Fecha_documento_vencimiento
        End Get
        Set(value As Date)
            Me.Fecha_documento_vencimiento = value
        End Set
    End Property
    Public Property get_numero_de_referencia As String
        Get
            Return Me.numero_de_referencia
        End Get
        Set(value As String)
            Me.numero_de_referencia = value
        End Set
    End Property
    Public Property get_tipo_de_referencia As String
        Get
            Return Me.tipo_de_referencia
        End Get
        Set(value As String)
            Me.tipo_de_referencia = value
        End Set
    End Property
    Public Property get_proveedor As String
        Get
            Return Me.proveedor
        End Get
        Set(value As String)
            Me.proveedor = value
        End Set
    End Property
    Public Property get_sub_total As Decimal
        Get
            Return Me.sub_total
        End Get
        Set(value As Decimal)
            Me.sub_total = value
        End Set
    End Property
    Public Property get_ir As Decimal
        Get
            Return Me.ir
        End Get
        Set(value As Decimal)
            Me.ir = value
        End Set
    End Property
    Public Property get_imi As Decimal
        Get
            Return Me.imi
        End Get
        Set(value As Decimal)
            Me.imi = value
        End Set
    End Property
    Public Property get_iva As Decimal
        Get
            Return Me.iva
        End Get
        Set(value As Decimal)
            Me.iva = value
        End Set
    End Property
    Public Property get_total_neto As Decimal
        Get
            Return Me.total_neto
        End Get
        Set(value As Decimal)
            Me.total_neto = value
        End Set
    End Property
    Public Property get_otros_gastos As Decimal
        Get
            Return Me.otros_gastos
        End Get
        Set(value As Decimal)
            Me.otros_gastos = value
        End Set
    End Property
    Public Property get_multas_y_recargos As Decimal
        Get
            Return Me.multas_y_recargos
        End Get
        Set(value As Decimal)
            Me.multas_y_recargos = value
        End Set
    End Property
    Public Property get_otros_impuestos As Decimal
        Get
            Return Me.otros_impuestos
        End Get
        Set(value As Decimal)
            Me.otros_impuestos = value
        End Set
    End Property
    Public Property get_neto_a_pagar As Decimal
        Get
            Return Me.neto_a_pagar
        End Get
        Set(value As Decimal)
            Me.neto_a_pagar = value
        End Set
    End Property


    Public Function gestion(ByRef accion As Integer) As impuesto   '1 = insertar, 2 = actualizar y 3 = eliminar

        data_compra_foranea.set_impuesto(
                                            Funciones.Param("@sucursal", Me.identificadores.sucursal, SqlDbType.Int),
                                            Funciones.Param("@numero_de_documento", Me.identificadores.numero_de_documento, SqlDbType.NVarChar),
                                            Funciones.Param("@numero_de_documento_digitado", Me.numero_de_documento_digitado, SqlDbType.NVarChar),
                                            Funciones.Param("@tipo_documento", Me.identificadores.tipo_documento, SqlDbType.NVarChar),
                                            Funciones.Param("@Fecha_documento_emitido", Me.Fecha_documento_emitido, SqlDbType.DateTime),
                                            Funciones.Param("@Fecha_documento_vencimiento", Me.Fecha_documento_vencimiento, SqlDbType.DateTime),
                                            Funciones.Param("@numero_de_referencia", Me.numero_de_referencia, SqlDbType.NVarChar),
                                            Funciones.Param("@tipo_de_referencia", Me.tipo_de_referencia, SqlDbType.NVarChar),
                                            Funciones.Param("@proveedor", Me.proveedor, SqlDbType.NVarChar),
                                            Funciones.Param("@sub_total", Me.sub_total, SqlDbType.Decimal),
                                            Funciones.Param("@ir", Me.ir, SqlDbType.Decimal),
                                            Funciones.Param("@imi", Me.imi, SqlDbType.Decimal),
                                            Funciones.Param("@iva", Me.iva, SqlDbType.Decimal),
                                            Funciones.Param("@total_neto", Me.total_neto, SqlDbType.Decimal),
                                            Funciones.Param("@otros_gastos", Me.otros_gastos, SqlDbType.Decimal),
                                            Funciones.Param("@multas_y_recargos", Me.multas_y_recargos, SqlDbType.Decimal),
                                            Funciones.Param("@otros_impuestos", Me.otros_impuestos, SqlDbType.Decimal),
                                            Funciones.Param("@neto_a_pagar", Me.neto_a_pagar, SqlDbType.Decimal),
                                            Funciones.Param("@accion", accion, SqlDbType.Int))

        Return Me

    End Function

    Public Function get_impuestos() As Dictionary(Of impuesto.key_identificador, impuesto)

        Return data_compra_foranea.get_impuestos(
                                                    Funciones.Param("@sucursal", identificadores.sucursal, SqlDbType.Int),
                                                    Funciones.Param("@numero_de_documento", identificadores.numero_de_documento, SqlDbType.Int))

    End Function

End Class