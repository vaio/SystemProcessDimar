﻿Public Class compra_foranea_producto

    Private sucursal As Integer
    Private numero_de_documento As String
    Private almacen_codigo As Integer
    Private producto_codigo As String
    Private producto_cantidad As Decimal
    Private producto_precio As Decimal
    Private producto_peso As Decimal
    Private producto_dai As Decimal
    Private fecha_de_expiracion As Date

    Private descripcion As String

    Public ReadOnly Property get_sucursal As Integer
        Get
            Return Me.sucursal
        End Get
    End Property
    Public ReadOnly Property get_numero_de_documento As String
        Get
            Return Me.numero_de_documento
        End Get
    End Property

    Public Property get_almacen_codigo As Integer
        Get
            Return Me.almacen_codigo
        End Get
        Set(value As Integer)
            Me.almacen_codigo = value
        End Set
    End Property

    Public Property get_producto_codigo As String
        Get
            Return Me.producto_codigo
        End Get
        Set(value As String)
            Me.producto_codigo = value
        End Set
    End Property

    Public Property get_producto_cantidad As Decimal
        Get
            Return Me.producto_cantidad
        End Get
        Set(value As Decimal)
            Me.producto_cantidad = value
        End Set
    End Property

    Public Property get_producto_precio As Decimal
        Get
            Return Me.producto_precio
        End Get
        Set(value As Decimal)
            Me.producto_precio = value
        End Set
    End Property

    Public Property get_producto_peso As Decimal
        Get
            Return Me.producto_peso
        End Get
        Set(value As Decimal)
            Me.producto_peso = value
        End Set
    End Property

    Public Property get_producto_dai As Decimal
        Get
            Return Me.producto_dai
        End Get
        Set(value As Decimal)
            Me.producto_dai = value
        End Set
    End Property
    Public Property get_fecha_de_expiracion As Date
        Get
            Return Me.fecha_de_expiracion
        End Get
        Set(value As Date)
            Me.fecha_de_expiracion = value
        End Set
    End Property

    Public Property get_descripcion As String
        Get
            Return Me.descripcion
        End Get
        Set(value As String)
            Me.descripcion = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(
                     sucursal As Integer,
                     numero_de_documento As String,
                     almacen_codigo As Integer,
                     producto_codigo As String)

        Me.sucursal = sucursal
        Me.numero_de_documento = numero_de_documento
        Me.almacen_codigo = almacen_codigo
        Me.producto_codigo = producto_codigo

    End Sub
    Public Sub New(
                     sucursal As Integer,
                     numero_de_documento As String,
                     almacen_codigo As Integer,
                     producto_codigo As String,
                     producto_cantidad As Decimal,
                     producto_precio As Decimal,
                     producto_peso As Decimal,
                     producto_dai As Decimal,
                     fecha_de_expiracion As Date
                  )


        Me.sucursal = sucursal
        Me.numero_de_documento = numero_de_documento
        Me.almacen_codigo = almacen_codigo
        Me.producto_codigo = producto_codigo
        Me.producto_cantidad = producto_cantidad
        Me.producto_precio = producto_precio
        Me.producto_peso = producto_peso
        Me.producto_dai = producto_dai
        Me.fecha_de_expiracion = fecha_de_expiracion

    End Sub

    Public Function guardar() As Integer

        Return data_compra_foranea.set_producto(Me, 1) ' ingresa y actualiza

    End Function

    Public Function actualizar() As Integer

        Return data_compra_foranea.set_producto(Me, 2) ' actualiza

    End Function

    Public Function eliminar() As Integer

        Return data_compra_foranea.set_producto(Me, 3) ' elimina

    End Function

    Public Function get_producto() As compra_foranea_producto

        Return data_compra_foranea.get_producto(Me)

    End Function

End Class
