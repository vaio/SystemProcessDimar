﻿Public Class compra

    Private sucursal As Integer
    Private numero_de_documento As String
    Private numero_de_documento_poliza As String
    Private numero_de_documento_consecutivo As Integer
    Private documento_multiple As Boolean
    Private tipo_de_compra As Integer
    Private tipo_cordoba_dolar As Integer
    Private tipo_calculo_de_documento As Integer
    Private tasa_cambio_de_documento As Decimal
    Private fecha_de_documento As Date
    Private fecha_de_recepcion As Date
    Private fecha_de_vencimiento As Date
    Private plazo As Integer
    Private observaciones As String
    Private ssa As Decimal
    Private tsi As Decimal
    Private spe As Decimal
    Private isc As Decimal
    Private dai As Decimal
    Private aplicado As Boolean
    Private comprobante As Boolean

    Private guardado As Boolean = False
    Public ReadOnly Property IsMasterSaved As Boolean
        Get
            Return Me.guardado
        End Get
    End Property

    Public ReadOnly Property get_sucursal As Integer
        Get
            Return Me.sucursal
        End Get
    End Property
    Public ReadOnly Property get_numero_de_documento As String
        Get
            Return Me.numero_de_documento
        End Get
    End Property
    Public ReadOnly Property get_numero_de_documento_poliza As String
        Get
            Return Me.numero_de_documento_poliza
        End Get
    End Property
    Public ReadOnly Property get_numero_de_documento_consecutivo As Integer
        Get
            Return Me.numero_de_documento_consecutivo
        End Get
    End Property
    Public ReadOnly Property get_documento_multiple As Boolean
        Get
            Return Me.documento_multiple
        End Get
    End Property
    Public Property get_tipo_de_compra As Integer
        Get
            Return Me.tipo_de_compra
        End Get
        Set(value As Integer)
            Me.tipo_de_compra = value
        End Set
    End Property
    Public Property get_tipo_cordoba_dolar As Integer
        Get
            Return Me.tipo_cordoba_dolar
        End Get
        Set(value As Integer)
            Me.tipo_cordoba_dolar = value
        End Set
    End Property
    Public Property get_tipo_calculo_de_documento As Integer
        Get
            Return Me.tipo_calculo_de_documento
        End Get
        Set(value As Integer)
            Me.tipo_calculo_de_documento = value
        End Set
    End Property
    Public Property get_tasa_cambio_de_documento As Decimal
        Get
            Return Me.tasa_cambio_de_documento
        End Get
        Set(value As Decimal)
            Me.tasa_cambio_de_documento = value
        End Set
    End Property

    Public Property get_fecha_de_documento As Date
        Get
            Return Me.fecha_de_documento
        End Get
        Set(value As Date)
            Me.fecha_de_documento = value
        End Set
    End Property

    Public Property get_fecha_de_recepcion As Date
        Get
            Return Me.fecha_de_recepcion
        End Get
        Set(value As Date)
            Me.fecha_de_recepcion = value
        End Set
    End Property

    Public Property get_fecha_de_vencimiento As Date
        Get
            Return Me.fecha_de_vencimiento
        End Get
        Set(value As Date)
            Me.fecha_de_vencimiento = value
        End Set
    End Property

    Public Property get_plazo As Integer
        Get
            Return Me.plazo
        End Get
        Set(value As Integer)
            Me.plazo = value
        End Set
    End Property

    Public Property get_observaciones As String
        Get
            Return Me.observaciones
        End Get
        Set(value As String)
            Me.observaciones = value
        End Set
    End Property
    Public Property get_ssa As Decimal
        Get
            Return Me.ssa
        End Get
        Set(value As Decimal)
            Me.ssa = value
        End Set
    End Property

    Public Property get_tsi As Decimal
        Get
            Return Me.tsi
        End Get
        Set(value As Decimal)
            Me.tsi = value
        End Set
    End Property
    Public Property get_spe As Decimal
        Get
            Return Me.spe
        End Get
        Set(value As Decimal)
            Me.spe = value
        End Set
    End Property
    Public Property get_isc As Decimal
        Get
            Return Me.isc
        End Get
        Set(value As Decimal)
            Me.isc = value
        End Set
    End Property
    Public Property get_dai As Decimal
        Get
            Return Me.dai
        End Get
        Set(value As Decimal)
            Me.dai = value
        End Set
    End Property

    Public ReadOnly Property get_aplicado As Boolean
        Get
            Return Me.aplicado
        End Get
    End Property

    Public ReadOnly Property get_comporbante As Boolean
        Get
            Return Me.comprobante
        End Get
    End Property

    Public Sub New()

    End Sub

    Public Sub New(sucursal As Integer, numero_de_documento As String)

        Me.sucursal = sucursal
        Me.numero_de_documento = numero_de_documento

    End Sub

    Public Sub New(sucursal As Integer, numero_de_documento_poliza As String, documento_multiple As Boolean, aplicado As Boolean)


        Me.sucursal = sucursal
        Me.numero_de_documento_poliza = numero_de_documento_poliza
        Me.documento_multiple = documento_multiple
        Me.aplicado = aplicado

    End Sub

    Public Sub New(sucursal As Integer, numero_de_documento As String, numero_de_documento_poliza As String, numero_de_documento_consecutivo As Integer)

        Me.sucursal = sucursal
        Me.numero_de_documento = numero_de_documento
        Me.numero_de_documento_poliza = numero_de_documento_poliza
        Me.numero_de_documento_consecutivo = numero_de_documento_consecutivo

    End Sub

    Public Sub New(Builder As BuilderCompra)

        Me.sucursal = Builder.sucursal
        Me.numero_de_documento = Builder.numero_de_documento
        Me.numero_de_documento_poliza = Builder.numero_de_documento_poliza
        Me.numero_de_documento_consecutivo = Builder.numero_de_documento_consecutivo
        Me.documento_multiple = Builder.documento_multiple
        Me.tipo_de_compra = Builder.tipo_de_compra
        Me.tipo_cordoba_dolar = Builder.tipo_cordoba_dolar
        Me.tipo_calculo_de_documento = Builder.tipo_calculo_de_documento
        Me.tasa_cambio_de_documento = Builder.tasa_cambio_de_documento
        Me.fecha_de_documento = Builder.fecha_de_documento
        Me.fecha_de_recepcion = Builder.fecha_de_recepcion
        Me.fecha_de_vencimiento = Builder.fecha_de_vencimiento
        Me.plazo = Builder.plazo
        Me.observaciones = Builder.observaciones
        Me.ssa = Builder.ssa
        Me.tsi = Builder.tsi
        Me.spe = Builder.spe
        Me.isc = Builder.isc
        Me.dai = Builder.dai

        Me.aplicado = Builder.aplicado

    End Sub
    Public Function guardar() As Integer

        Dim result = data_compra_foranea.set_compra(Me)

        If result = 0 Then guardado = True

        Return result

    End Function

    Public Function get_compra() As compra

        Dim Builder = data_compra_foranea.get_compra(Me)

        Me.sucursal = Builder.sucursal
        Me.numero_de_documento = Builder.numero_de_documento
        Me.numero_de_documento_poliza = Builder.numero_de_documento_poliza
        Me.numero_de_documento_consecutivo = Builder.numero_de_documento_consecutivo
        Me.documento_multiple = Builder.documento_multiple
        Me.tipo_de_compra = Builder.tipo_de_compra
        Me.tipo_cordoba_dolar = Builder.tipo_cordoba_dolar
        Me.tipo_calculo_de_documento = Builder.tipo_calculo_de_documento
        Me.tasa_cambio_de_documento = Builder.tasa_cambio_de_documento
        Me.fecha_de_documento = Builder.fecha_de_documento
        Me.fecha_de_recepcion = Builder.fecha_de_recepcion
        Me.fecha_de_vencimiento = Builder.fecha_de_vencimiento
        Me.plazo = Builder.plazo
        Me.observaciones = Builder.observaciones
        Me.ssa = Builder.ssa
        Me.tsi = Builder.tsi
        Me.spe = Builder.spe
        Me.isc = Builder.isc
        Me.dai = Builder.dai

        Me.aplicado = Builder.aplicado

        Me.guardado = True

        Return Me

    End Function

    Public Function verificar_poliza() As Boolean

        Return data_compra_foranea.get_documento_multiple(Me)

    End Function

    Public Function get_ultimo_consecutivo() As Integer

        Return data_compra_foranea.get_documento_multiple_consecutivo(Me)

    End Function

    Public Function get_tipo_de_calculo_de_primer_poliza() As Integer

        Return data_compra_foranea.get_documento_multiple_tipo_de_calculo(Me)

    End Function

    Public Function get_detalle() As DataTable

        Return data_compra_foranea.get_detalle(Me)

    End Function

    Public Function get_total_poliza_aplicada() As Decimal

        Return data_compra_foranea.get_total_poliza_aplicada(Me)

    End Function
    Public Function get_total_poliza_actual_aplicada() As Decimal

        Return data_compra_foranea.get_total_poliza_actual_aplicada(Me)

    End Function
    Public Function get_total_poliza_actual_sin_aplicada() As Decimal

        Return data_compra_foranea.get_total_poliza_actual_sin_aplicada(Me)

    End Function

    Public Function get_total_poliza_total() As Decimal

        Return data_compra_foranea.get_total_poliza_total(Me)

    End Function

    Public Function get_detalle_total_dai() As Decimal

        Return data_compra_foranea.get_detalle_total_dai(Me)

    End Function

    Public Function eliminar_compra() As Integer

        Return data_compra_foranea.eliminar_compra(Me)

    End Function

    Public Class BuilderCompra

        Public sucursal As Integer
        Public numero_de_documento As String
        Public numero_de_documento_poliza As String
        Public numero_de_documento_consecutivo As Integer
        Public documento_multiple As Boolean
        Public tipo_de_compra As Integer
        Public tipo_cordoba_dolar As Integer
        Public tipo_calculo_de_documento As Integer
        Public tasa_cambio_de_documento As Decimal
        Public fecha_de_documento As Date
        Public fecha_de_recepcion As Date
        Public fecha_de_vencimiento As Date
        Public plazo As Integer
        Public observaciones As String
        Public ssa As Decimal
        Public tsi As Decimal
        Public spe As Decimal
        Public isc As Decimal
        Public dai As Decimal
        Public aplicado As Boolean

    End Class


End Class
