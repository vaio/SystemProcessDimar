﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class compra_foranea_impuesto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(compra_foranea_impuesto))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmbReferencia = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmbTipDocumento = New DevExpress.XtraEditors.LookUpEdit()
        Me.cmbProveedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtOtrosImpuestos = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtIMI = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtIR = New System.Windows.Forms.TextBox()
        Me.txtMultas = New System.Windows.Forms.TextBox()
        Me.txtSubTotal = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTotalNeto = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.txtOtrosGas = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNeto = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtPorcIR = New System.Windows.Forms.TextBox()
        Me.txtPorcIMI = New System.Windows.Forms.TextBox()
        Me.CkIR = New System.Windows.Forms.CheckBox()
        Me.CkIMI = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNumRefe = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.deFechaVenc = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.deFechaDoc = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNumDoc = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.cmbReferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbTipDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdCancelar)
        Me.Panel2.Controls.Add(Me.cmdAceptar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 343)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(737, 60)
        Me.Panel2.TabIndex = 15
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(612, 9)
        Me.cmdCancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(104, 43)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(500, 9)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(104, 43)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmbReferencia)
        Me.Panel1.Controls.Add(Me.cmbTipDocumento)
        Me.Panel1.Controls.Add(Me.cmbProveedor)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtNumRefe)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.deFechaVenc)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.deFechaDoc)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtNumDoc)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(737, 403)
        Me.Panel1.TabIndex = 14
        '
        'cmbReferencia
        '
        Me.cmbReferencia.Enabled = False
        Me.cmbReferencia.Location = New System.Drawing.Point(130, 199)
        Me.cmbReferencia.Name = "cmbReferencia"
        Me.cmbReferencia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbReferencia.Size = New System.Drawing.Size(300, 23)
        Me.cmbReferencia.TabIndex = 76
        '
        'cmbTipDocumento
        '
        Me.cmbTipDocumento.Enabled = False
        Me.cmbTipDocumento.Location = New System.Drawing.Point(132, 66)
        Me.cmbTipDocumento.Name = "cmbTipDocumento"
        Me.cmbTipDocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbTipDocumento.Size = New System.Drawing.Size(300, 23)
        Me.cmbTipDocumento.TabIndex = 75
        '
        'cmbProveedor
        '
        Me.cmbProveedor.Location = New System.Drawing.Point(132, 231)
        Me.cmbProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbProveedor.Name = "cmbProveedor"
        Me.cmbProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbProveedor.Size = New System.Drawing.Size(297, 23)
        Me.cmbProveedor.TabIndex = 74
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 202)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(113, 17)
        Me.Label15.TabIndex = 72
        Me.Label15.Text = "Tipo Referencia:"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.txtOtrosImpuestos)
        Me.Panel5.Controls.Add(Me.Label16)
        Me.Panel5.Controls.Add(Me.Label14)
        Me.Panel5.Controls.Add(Me.txtIMI)
        Me.Panel5.Controls.Add(Me.Label13)
        Me.Panel5.Controls.Add(Me.txtIR)
        Me.Panel5.Controls.Add(Me.txtMultas)
        Me.Panel5.Controls.Add(Me.txtSubTotal)
        Me.Panel5.Controls.Add(Me.Label12)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Controls.Add(Me.txtTotalNeto)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.Label11)
        Me.Panel5.Controls.Add(Me.txtIVA)
        Me.Panel5.Controls.Add(Me.txtOtrosGas)
        Me.Panel5.Controls.Add(Me.Label9)
        Me.Panel5.Controls.Add(Me.Label10)
        Me.Panel5.Controls.Add(Me.txtNeto)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel5.Location = New System.Drawing.Point(437, 0)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(300, 403)
        Me.Panel5.TabIndex = 14
        '
        'txtOtrosImpuestos
        '
        Me.txtOtrosImpuestos.BackColor = System.Drawing.Color.Snow
        Me.txtOtrosImpuestos.Enabled = False
        Me.txtOtrosImpuestos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtrosImpuestos.Location = New System.Drawing.Point(136, 258)
        Me.txtOtrosImpuestos.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOtrosImpuestos.MaxLength = 10
        Me.txtOtrosImpuestos.Name = "txtOtrosImpuestos"
        Me.txtOtrosImpuestos.Size = New System.Drawing.Size(144, 26)
        Me.txtOtrosImpuestos.TabIndex = 89
        Me.txtOtrosImpuestos.Text = "0.0000"
        Me.txtOtrosImpuestos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(5, 264)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(115, 17)
        Me.Label16.TabIndex = 88
        Me.Label16.Text = "Otros impuestos:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 100)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 17)
        Me.Label14.TabIndex = 87
        Me.Label14.Text = "IMI:"
        '
        'txtIMI
        '
        Me.txtIMI.BackColor = System.Drawing.Color.Snow
        Me.txtIMI.Enabled = False
        Me.txtIMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIMI.Location = New System.Drawing.Point(136, 94)
        Me.txtIMI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIMI.MaxLength = 10
        Me.txtIMI.Name = "txtIMI"
        Me.txtIMI.Size = New System.Drawing.Size(144, 26)
        Me.txtIMI.TabIndex = 86
        Me.txtIMI.Text = "0.0000"
        Me.txtIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 65)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(25, 17)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "IR:"
        '
        'txtIR
        '
        Me.txtIR.BackColor = System.Drawing.Color.Snow
        Me.txtIR.Enabled = False
        Me.txtIR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIR.Location = New System.Drawing.Point(136, 59)
        Me.txtIR.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIR.MaxLength = 10
        Me.txtIR.Name = "txtIR"
        Me.txtIR.Size = New System.Drawing.Size(144, 26)
        Me.txtIR.TabIndex = 84
        Me.txtIR.Text = "0.0000"
        Me.txtIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtMultas
        '
        Me.txtMultas.BackColor = System.Drawing.Color.Snow
        Me.txtMultas.Enabled = False
        Me.txtMultas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMultas.Location = New System.Drawing.Point(136, 224)
        Me.txtMultas.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMultas.MaxLength = 10
        Me.txtMultas.Name = "txtMultas"
        Me.txtMultas.Size = New System.Drawing.Size(144, 26)
        Me.txtMultas.TabIndex = 83
        Me.txtMultas.Text = "0.0000"
        Me.txtMultas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSubTotal
        '
        Me.txtSubTotal.BackColor = System.Drawing.Color.Snow
        Me.txtSubTotal.Enabled = False
        Me.txtSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotal.Location = New System.Drawing.Point(136, 23)
        Me.txtSubTotal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSubTotal.MaxLength = 10
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.Size = New System.Drawing.Size(144, 26)
        Me.txtSubTotal.TabIndex = 72
        Me.txtSubTotal.Text = "0.0000"
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(5, 230)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(129, 17)
        Me.Label12.TabIndex = 82
        Me.Label12.Text = "Multas y Recargos:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 27)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 17)
        Me.Label7.TabIndex = 73
        Me.Label7.Text = "Sub-Total:"
        '
        'txtTotalNeto
        '
        Me.txtTotalNeto.BackColor = System.Drawing.Color.Snow
        Me.txtTotalNeto.Enabled = False
        Me.txtTotalNeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalNeto.Location = New System.Drawing.Point(136, 159)
        Me.txtTotalNeto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalNeto.MaxLength = 10
        Me.txtTotalNeto.Name = "txtTotalNeto"
        Me.txtTotalNeto.Size = New System.Drawing.Size(144, 26)
        Me.txtTotalNeto.TabIndex = 80
        Me.txtTotalNeto.Text = "0.0000"
        Me.txtTotalNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(5, 132)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 17)
        Me.Label8.TabIndex = 75
        Me.Label8.Text = "IVA:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(5, 165)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 17)
        Me.Label11.TabIndex = 81
        Me.Label11.Text = "Total Neto:"
        '
        'txtIVA
        '
        Me.txtIVA.BackColor = System.Drawing.Color.Snow
        Me.txtIVA.Enabled = False
        Me.txtIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIVA.Location = New System.Drawing.Point(136, 126)
        Me.txtIVA.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIVA.MaxLength = 10
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.Size = New System.Drawing.Size(144, 26)
        Me.txtIVA.TabIndex = 74
        Me.txtIVA.Text = "0.0000"
        Me.txtIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtOtrosGas
        '
        Me.txtOtrosGas.BackColor = System.Drawing.Color.Snow
        Me.txtOtrosGas.Enabled = False
        Me.txtOtrosGas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtrosGas.Location = New System.Drawing.Point(136, 191)
        Me.txtOtrosGas.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOtrosGas.MaxLength = 10
        Me.txtOtrosGas.Name = "txtOtrosGas"
        Me.txtOtrosGas.Size = New System.Drawing.Size(144, 26)
        Me.txtOtrosGas.TabIndex = 78
        Me.txtOtrosGas.Text = "0.0000"
        Me.txtOtrosGas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 298)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 17)
        Me.Label9.TabIndex = 77
        Me.Label9.Text = "Neto a Pagar:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 197)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 17)
        Me.Label10.TabIndex = 79
        Me.Label10.Text = "Otros Gastos:"
        '
        'txtNeto
        '
        Me.txtNeto.BackColor = System.Drawing.Color.Snow
        Me.txtNeto.Enabled = False
        Me.txtNeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNeto.Location = New System.Drawing.Point(136, 292)
        Me.txtNeto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNeto.MaxLength = 10
        Me.txtNeto.Name = "txtNeto"
        Me.txtNeto.ReadOnly = True
        Me.txtNeto.Size = New System.Drawing.Size(144, 26)
        Me.txtNeto.TabIndex = 76
        Me.txtNeto.Text = "0.0000"
        Me.txtNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPorcIR)
        Me.GroupBox1.Controls.Add(Me.txtPorcIMI)
        Me.GroupBox1.Controls.Add(Me.CkIR)
        Me.GroupBox1.Controls.Add(Me.CkIMI)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(132, 269)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(293, 57)
        Me.GroupBox1.TabIndex = 71
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Retenciones"
        '
        'txtPorcIR
        '
        Me.txtPorcIR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcIR.Location = New System.Drawing.Point(65, 22)
        Me.txtPorcIR.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcIR.Name = "txtPorcIR"
        Me.txtPorcIR.ReadOnly = True
        Me.txtPorcIR.Size = New System.Drawing.Size(48, 26)
        Me.txtPorcIR.TabIndex = 1
        Me.txtPorcIR.Text = "0"
        Me.txtPorcIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPorcIMI
        '
        Me.txtPorcIMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcIMI.Location = New System.Drawing.Point(199, 22)
        Me.txtPorcIMI.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcIMI.Name = "txtPorcIMI"
        Me.txtPorcIMI.ReadOnly = True
        Me.txtPorcIMI.Size = New System.Drawing.Size(48, 26)
        Me.txtPorcIMI.TabIndex = 3
        Me.txtPorcIMI.Text = "0"
        Me.txtPorcIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CkIR
        '
        Me.CkIR.AutoSize = True
        Me.CkIR.Location = New System.Drawing.Point(20, 26)
        Me.CkIR.Margin = New System.Windows.Forms.Padding(4)
        Me.CkIR.Name = "CkIR"
        Me.CkIR.Size = New System.Drawing.Size(115, 21)
        Me.CkIR.TabIndex = 0
        Me.CkIR.Text = "IR               %"
        Me.CkIR.UseVisualStyleBackColor = True
        '
        'CkIMI
        '
        Me.CkIMI.AutoSize = True
        Me.CkIMI.Location = New System.Drawing.Point(148, 25)
        Me.CkIMI.Margin = New System.Windows.Forms.Padding(4)
        Me.CkIMI.Name = "CkIMI"
        Me.CkIMI.Size = New System.Drawing.Size(119, 21)
        Me.CkIMI.TabIndex = 2
        Me.CkIMI.Text = "IMI               %"
        Me.CkIMI.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 234)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Proveedor:"
        '
        'txtNumRefe
        '
        Me.txtNumRefe.Enabled = False
        Me.txtNumRefe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumRefe.Location = New System.Drawing.Point(132, 164)
        Me.txtNumRefe.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNumRefe.Name = "txtNumRefe"
        Me.txtNumRefe.Size = New System.Drawing.Size(297, 26)
        Me.txtNumRefe.TabIndex = 67
        Me.txtNumRefe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 170)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 17)
        Me.Label6.TabIndex = 68
        Me.Label6.Text = "N° Referencia:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 70)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(116, 17)
        Me.Label5.TabIndex = 64
        Me.Label5.Text = "Tipo Documento:"
        '
        'deFechaVenc
        '
        Me.deFechaVenc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaVenc.Location = New System.Drawing.Point(132, 129)
        Me.deFechaVenc.Margin = New System.Windows.Forms.Padding(4)
        Me.deFechaVenc.Name = "deFechaVenc"
        Me.deFechaVenc.Size = New System.Drawing.Size(165, 22)
        Me.deFechaVenc.TabIndex = 15
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 132)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 17)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Vencimiento:"
        '
        'deFechaDoc
        '
        Me.deFechaDoc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaDoc.Location = New System.Drawing.Point(132, 97)
        Me.deFechaDoc.Margin = New System.Windows.Forms.Padding(4)
        Me.deFechaDoc.Name = "deFechaDoc"
        Me.deFechaDoc.Size = New System.Drawing.Size(165, 22)
        Me.deFechaDoc.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 105)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 17)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Fecha:"
        '
        'txtNumDoc
        '
        Me.txtNumDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDoc.Location = New System.Drawing.Point(132, 28)
        Me.txtNumDoc.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(300, 26)
        Me.txtNumDoc.TabIndex = 9
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 34)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 17)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "N° Documento:"
        '
        'compra_foranea_impuesto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(737, 403)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(755, 450)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(755, 450)
        Me.Name = "compra_foranea_impuesto"
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.cmbReferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbTipDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtIMI As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtIR As System.Windows.Forms.TextBox
    Friend WithEvents txtMultas As System.Windows.Forms.TextBox
    Friend WithEvents txtSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTotalNeto As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents txtOtrosGas As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtNeto As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPorcIR As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcIMI As System.Windows.Forms.TextBox
    Friend WithEvents CkIR As System.Windows.Forms.CheckBox
    Friend WithEvents CkIMI As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNumRefe As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents deFechaVenc As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents deFechaDoc As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtOtrosImpuestos As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmbProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmbTipDocumento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmbReferencia As DevExpress.XtraEditors.LookUpEdit
End Class
