﻿Public Class linea

    Implements IMenu, ICatalogo

    Private CODIGO_LINEA As Integer
    Private NOMBRE_LINEA As String
    Private ACCION As Integer = 1 '1 = guardar , 2 = actualizar, 3 = eliminar
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        set_data()

        Titulo.Text = "-> Catalogo de lineas"

        AddHandler tabla_de_datos.DoubleClick, AddressOf btnBuscar
        AddHandler MyBase.Resize, AddressOf responsive
        AddHandler btn_cancelar.Click, AddressOf btnAtras
        AddHandler btn_aceptar.Click, AddressOf btnAceptar

    End Sub
    Public Sub set_data() Implements ICatalogo.set_data

        tabla_de_datos.DataSource = get_data()

    End Sub
    Public Function get_data() As DataTable Implements ICatalogo.get_data

        Return conexion_to_db.GetVista("Select Codigo as 'ID', Nombre_Linea as 'LINEA' From vwVerLineasProd")

    End Function
    Public Function nuevo() As Integer Implements IMenu.nuevo

        NavigationFrame1.SelectedPage = NavigationPage2

        limpiar()

        ACCION = 1

        CODIGO_LINEA = Integer.Parse(conexion_to_db.ExecuteSql("SELECT isnull(MAX(Codigo_linea)+1,1) AS Maximo FROM Linea_Producto"))

        asignar_datos_a_controles(CODIGO_LINEA, String.Empty)

        activacion_controles(True)

        Return 0

    End Function
    Public Function guardar() As Integer Implements IMenu.guardar

        Dim Resum As String = ""
        Dim CatalogoBodegas As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try

            asignar_datos_de_controles_a_variables(ChangeToDouble(box_codigo.Text), box_nombre.Text)

            If Not validar_campos() = False Then Return 1

            Dim Question = MsgBox("Aplicar accion?", MsgBoxStyle.OkCancel) : If Not Question = MsgBoxResult.Ok Then Return 1

            Resum = CatalogoBodegas.EditarLinea(CODIGO_LINEA, NOMBRE_LINEA, ACCION)

            If Not Resum = "OK" Then

                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")

            Else

                ACCION = 2

                activacion_controles(False)

                MsgBox("Listo! accion aplicada", MsgBoxStyle.Information)

                set_data()

            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally

        End Try

        Return 0

    End Function
    Public Function cancelar() As Integer Implements IMenu.cancelar

        If Not ACCION = 1 Then

            ManipularMenu.buscar(NUEVO_MENU)

            Return 1

        End If

        activacion_controles(False)

        Return 0

    End Function
    Public Function eliminar() As Integer Implements IMenu.eliminar

        Return 1

    End Function
    Public Function historial() As Integer Implements IMenu.historial

        NavigationFrame1.SelectedPage = NavigationPage2

        asignar_datos_de_controles_a_variables(Integer.Parse(datos.GetRowCellValue(datos.GetSelectedRows(0), "ID")), datos.GetRowCellValue(datos.GetSelectedRows(0), "LINEA"))

        asignar_datos_a_controles(CODIGO_LINEA, NOMBRE_LINEA)

        activacion_controles(False)

        Return 0

    End Function
    Public Function imprimir() As Integer Implements IMenu.imprimir

        Return 1

    End Function
    Public Function modificar() As Integer Implements IMenu.modificar

        ACCION = 2

        activacion_controles(True)

        Return 0

    End Function
    Public Function salir() As Integer Implements IMenu.salir

        Me.Close()

        Return 0

    End Function

    Private Sub btnBuscar()

        ManipularMenu.buscar(NUEVO_MENU)

    End Sub
    Private Sub btnAtras()

        ACCION = 1

        ManipularMenu.cancelar(NUEVO_MENU)

        NavigationFrame1.SelectedPage = NavigationPage1

    End Sub
    Private Sub btnAceptar()

        ManipularMenu.guardar(NUEVO_MENU)

    End Sub

    Private Sub responsive()

        Me.panel_cuerpo_editar.Location = New Point(Me.ClientSize.Width / 2 - Me.panel_cuerpo_editar.Size.Width / 2, (Me.ClientSize.Height - 100) / 2 - Me.panel_cuerpo_editar.Size.Height / 2)

    End Sub

    Private Sub asignar_datos_a_controles(ByRef CODIGO, ByRef NOMBRE)

        box_codigo.Text = Integer.Parse(CODIGO)

        box_nombre.Text = NOMBRE.ToString

    End Sub
    Private Sub asignar_datos_de_controles_a_variables(ByRef CODIGO As Integer, ByRef NOMBRE As String)

        CODIGO_LINEA = CODIGO

        NOMBRE_LINEA = NOMBRE

    End Sub

    Private Sub limpiar()

        box_codigo.Text = String.Empty

        box_nombre.Text = String.Empty

    End Sub
    Private Function validar_campos() As Boolean

        If CODIGO_LINEA = 0 Then MsgBox("Vamos! tio! no se como llegates a este error! pero no puedes ingresar una linea sin codigo") : Return 1
        If NOMBRE_LINEA = String.Empty Then MsgBox("Vaya! te quieres pasar de listo ingresando una linea sin nombre! vuelve atras e ingresa un nombre", MsgBoxStyle.Exclamation) : Return 1

        Return 0

    End Function
    Private Sub activacion_controles(KEY As Boolean)

        box_nombre.Enabled = KEY

        btn_aceptar.Enabled = KEY

    End Sub

End Class