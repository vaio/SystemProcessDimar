﻿Imports DevExpress.XtraEditors.Controls

Public Class categoria
    Implements IMenu, ICatalogo

    Private CODIGO_CATEGORIA As Integer
    Private NOMBRE_CATEGORIA As String
    Private CODIGO_LINEA As Integer
    Private ACCION As Integer = 1 '1 = guardar , 2 = actualizar, 3 = eliminar
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        set_data()
        setup_tabla_de_datos()
        setup_lineas()

        Titulo.Text = "-> Catalogo de categorias"

        AddHandler look_linea.EditValueChanged, AddressOf get_new_id
        AddHandler tabla_de_datos.DoubleClick, AddressOf btnBuscar
        AddHandler MyBase.Resize, AddressOf responsive
        AddHandler btn_cancelar.Click, AddressOf btnAtras
        AddHandler btn_aceptar.Click, AddressOf btnAceptar

    End Sub
    Private Sub setup_lineas()

        look_linea.Properties.DataSource = SQL("select Codigo_Linea,Nombre_Linea from Linea_Producto order by Codigo_Linea", "tblMarcas", My.Settings.SolIndustrialCNX).Tables(0)
        look_linea.Properties.DisplayMember = "Nombre_Linea"
        look_linea.Properties.ValueMember = "Codigo_Linea"

        look_linea.Properties.Columns.Add(New LookUpColumnInfo("Codigo_Linea", "No.", 20))

        look_linea.Properties.Columns.Add(New LookUpColumnInfo("Nombre_Linea", "Linea", 100))

        look_linea.Properties.Columns(0).Visible = False

        look_linea.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        look_linea.EditValue = 0

    End Sub
    Private Sub get_new_id()

        If ACCION = 1 Then

            CODIGO_CATEGORIA = Integer.Parse(conexion_to_db.ExecuteSql(String.Format("SELECT isnull(MAX(Id_Modelo)+1,1) AS Maximo FROM Tbl_Modelo Where Id_Marca = {0}", look_linea.EditValue)))

            asignar_datos_a_controles(CODIGO_CATEGORIA, box_nombre.Text, look_linea.EditValue)

        End If

    End Sub
    Private Sub setup_tabla_de_datos() ' solo oculta el grid. 

        datos.Columns(3).Visible = False

    End Sub
    Public Sub set_data() Implements ICatalogo.set_data

        tabla_de_datos.DataSource = get_data()

    End Sub
    Public Function get_data() As DataTable Implements ICatalogo.get_data

        Return conexion_to_db.GetVista("Select Codigo as 'ID', Nombre_Modelo as 'MODELO', Nombre_Marca as'MARCA', Codigo_Marca as 'ID_MARCA'  From vwVerModelos")

    End Function
    Public Function nuevo() As Integer Implements IMenu.nuevo

        NavigationFrame1.SelectedPage = NavigationPage2

        limpiar()

        ACCION = 1

        CODIGO_CATEGORIA = Integer.Parse(conexion_to_db.ExecuteSql(String.Format("SELECT isnull(MAX(Id_Modelo)+1,1) AS Maximo FROM Tbl_Modelo Where Id_Marca = {0}", look_linea.EditValue)))

        asignar_datos_a_controles(CODIGO_CATEGORIA, String.Empty, 0)

        activacion_controles(True)

        Return 0

    End Function
    Public Function guardar() As Integer Implements IMenu.guardar

        Dim Resum As String = ""
        Dim CatalogoModelos As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try

            asignar_datos_de_controles_a_variables(ChangeToDouble(box_codigo.Text), box_nombre.Text, look_linea.EditValue)

            If Not validar_campos() = False Then Return 1

            Dim Question = MsgBox("Aplicar accion?", MsgBoxStyle.OkCancel) : If Not Question = MsgBoxResult.Ok Then Return 1

            Resum = CatalogoModelos.EditarModelos(CODIGO_CATEGORIA, NOMBRE_CATEGORIA, CODIGO_LINEA, ACCION, 0, 0) 'dejo ambos en cero porque estoy modificando y a la vez reutilizando logica para ahorrar tiempo. pero esto se puede escribir mejor

            If Not Resum = "OK" Then

                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")

            Else

                ACCION = 2

                activacion_controles(False)

                MsgBox("Listo! accion aplicada", MsgBoxStyle.Information)

                set_data()
                setup_tabla_de_datos()

            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally

        End Try

        Return 0

    End Function
    Public Function cancelar() As Integer Implements IMenu.cancelar

        If Not ACCION = 1 Then

            ManipularMenu.buscar(NUEVO_MENU)

            Return 1

        End If

        activacion_controles(False)

        Return 0

    End Function
    Public Function eliminar() As Integer Implements IMenu.eliminar

        Return 1

    End Function
    Public Function historial() As Integer Implements IMenu.historial

        NavigationFrame1.SelectedPage = NavigationPage2

        ACCION = 2

        asignar_datos_de_controles_a_variables(Integer.Parse(datos.GetRowCellValue(datos.GetSelectedRows(0), "ID")), datos.GetRowCellValue(datos.GetSelectedRows(0), "MODELO"), Integer.Parse(datos.GetRowCellValue(datos.GetSelectedRows(0), "ID_MARCA")))

        asignar_datos_a_controles(CODIGO_CATEGORIA, NOMBRE_CATEGORIA, CODIGO_LINEA)

        activacion_controles(False)

        Return 0

    End Function
    Public Function imprimir() As Integer Implements IMenu.imprimir

        Return 1

    End Function
    Public Function modificar() As Integer Implements IMenu.modificar

        ACCION = 2

        activacion_controles(True)

        Return 0

    End Function
    Public Function salir() As Integer Implements IMenu.salir

        Me.Close()

        Return 0

    End Function
    Private Sub btnBuscar()

        ManipularMenu.buscar(NUEVO_MENU)

    End Sub
    Private Sub btnAtras()

        ACCION = 1

        ManipularMenu.cancelar(NUEVO_MENU)

        NavigationFrame1.SelectedPage = NavigationPage1

    End Sub
    Private Sub btnAceptar()

        ManipularMenu.guardar(NUEVO_MENU)

    End Sub
    Private Sub responsive()

        Me.panel_cuerpo_editar.Location = New Point(Me.ClientSize.Width / 2 - Me.panel_cuerpo_editar.Size.Width / 2, (Me.ClientSize.Height - 100) / 2 - Me.panel_cuerpo_editar.Size.Height / 2)

    End Sub
    Private Sub asignar_datos_a_controles(ByRef CODIGO, ByRef MODELO, ByRef MARCA)

        box_codigo.Text = Integer.Parse(CODIGO)

        box_nombre.Text = MODELO.ToString

        look_linea.EditValue = Integer.Parse(MARCA)

    End Sub
    Private Sub asignar_datos_de_controles_a_variables(ByRef CODIGO As Integer, ByRef NOMBRE As String, ByRef MARCA As Integer)

        CODIGO_CATEGORIA = CODIGO

        NOMBRE_CATEGORIA = NOMBRE

        CODIGO_LINEA = MARCA

    End Sub

    Private Sub limpiar()

        box_codigo.Text = String.Empty

        box_nombre.Text = String.Empty

    End Sub
    Private Function validar_campos() As Boolean

        If CODIGO_CATEGORIA = 0 Then MsgBox("Vamos! tio! no se como llegates a este error! pero no puedes ingresar una categoria sin codigo") : Return 1
        If NOMBRE_CATEGORIA = String.Empty Then MsgBox("Vaya! te quieres pasar de listo ingresando una categoria sin nombre! vuelve atras e ingresa un nombre", MsgBoxStyle.Exclamation) : Return 1

        Return 0

    End Function
    Private Sub activacion_controles(KEY As Boolean)

        box_nombre.Enabled = KEY

        btn_aceptar.Enabled = KEY

        If ACCION = 2 Then look_linea.Enabled = False Else look_linea.Enabled = True

    End Sub

End Class