﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class marca
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NavigationFrame1 = New DevExpress.XtraBars.Navigation.NavigationFrame()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.Titulo = New DevExpress.XtraEditors.LabelControl()
        Me.tabla_de_datos = New DevExpress.XtraGrid.GridControl()
        Me.datos = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.NavigationPage2 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.panel_principal_editar = New System.Windows.Forms.Panel()
        Me.panel_cuerpo_editar = New System.Windows.Forms.Panel()
        Me.btn_cancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_aceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.box_nombre = New System.Windows.Forms.TextBox()
        Me.box_codigo = New System.Windows.Forms.TextBox()
        Me.lblNomCatalogo = New System.Windows.Forms.Label()
        Me.LblCod = New System.Windows.Forms.Label()
        CType(Me.NavigationFrame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationFrame1.SuspendLayout()
        Me.NavigationPage1.SuspendLayout()
        CType(Me.tabla_de_datos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPage2.SuspendLayout()
        Me.panel_principal_editar.SuspendLayout()
        Me.panel_cuerpo_editar.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavigationFrame1
        '
        Me.NavigationFrame1.Appearance.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.NavigationFrame1.Appearance.Options.UseBackColor = True
        Me.NavigationFrame1.Controls.Add(Me.NavigationPage1)
        Me.NavigationFrame1.Controls.Add(Me.NavigationPage2)
        Me.NavigationFrame1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationFrame1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationFrame1.Name = "NavigationFrame1"
        Me.NavigationFrame1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPage1, Me.NavigationPage2})
        Me.NavigationFrame1.SelectedPage = Me.NavigationPage1
        Me.NavigationFrame1.Size = New System.Drawing.Size(1175, 622)
        Me.NavigationFrame1.TabIndex = 1
        Me.NavigationFrame1.Text = "NavigationFrame1"
        Me.NavigationFrame1.TransitionType = DevExpress.Utils.Animation.Transitions.Push
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "NavigationPage1"
        Me.NavigationPage1.Controls.Add(Me.Titulo)
        Me.NavigationPage1.Controls.Add(Me.tabla_de_datos)
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.Size = New System.Drawing.Size(1175, 622)
        '
        'Titulo
        '
        Me.Titulo.Appearance.Font = New System.Drawing.Font("Consolas", 11.0!)
        Me.Titulo.Location = New System.Drawing.Point(12, 24)
        Me.Titulo.Name = "Titulo"
        Me.Titulo.Size = New System.Drawing.Size(60, 22)
        Me.Titulo.TabIndex = 1
        Me.Titulo.Text = "Titulo"
        '
        'tabla_de_datos
        '
        Me.tabla_de_datos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabla_de_datos.Location = New System.Drawing.Point(12, 50)
        Me.tabla_de_datos.MainView = Me.datos
        Me.tabla_de_datos.Name = "tabla_de_datos"
        Me.tabla_de_datos.Size = New System.Drawing.Size(1151, 560)
        Me.tabla_de_datos.TabIndex = 0
        Me.tabla_de_datos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.datos})
        '
        'datos
        '
        Me.datos.GridControl = Me.tabla_de_datos
        Me.datos.Name = "datos"
        Me.datos.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.datos.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.datos.OptionsBehavior.Editable = False
        Me.datos.OptionsBehavior.ReadOnly = True
        '
        'NavigationPage2
        '
        Me.NavigationPage2.Caption = "NavigationPage2"
        Me.NavigationPage2.Controls.Add(Me.panel_principal_editar)
        Me.NavigationPage2.Name = "NavigationPage2"
        Me.NavigationPage2.Size = New System.Drawing.Size(1175, 622)
        '
        'panel_principal_editar
        '
        Me.panel_principal_editar.Controls.Add(Me.panel_cuerpo_editar)
        Me.panel_principal_editar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panel_principal_editar.Location = New System.Drawing.Point(0, 0)
        Me.panel_principal_editar.Name = "panel_principal_editar"
        Me.panel_principal_editar.Size = New System.Drawing.Size(1175, 622)
        Me.panel_principal_editar.TabIndex = 0
        '
        'panel_cuerpo_editar
        '
        Me.panel_cuerpo_editar.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(249, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.panel_cuerpo_editar.Controls.Add(Me.btn_cancelar)
        Me.panel_cuerpo_editar.Controls.Add(Me.btn_aceptar)
        Me.panel_cuerpo_editar.Controls.Add(Me.box_nombre)
        Me.panel_cuerpo_editar.Controls.Add(Me.box_codigo)
        Me.panel_cuerpo_editar.Controls.Add(Me.lblNomCatalogo)
        Me.panel_cuerpo_editar.Controls.Add(Me.LblCod)
        Me.panel_cuerpo_editar.Location = New System.Drawing.Point(96, 35)
        Me.panel_cuerpo_editar.Name = "panel_cuerpo_editar"
        Me.panel_cuerpo_editar.Size = New System.Drawing.Size(825, 444)
        Me.panel_cuerpo_editar.TabIndex = 0
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Location = New System.Drawing.Point(417, 199)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(153, 42)
        Me.btn_cancelar.TabIndex = 23
        Me.btn_cancelar.Text = "Atras"
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Location = New System.Drawing.Point(258, 199)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(153, 42)
        Me.btn_aceptar.TabIndex = 22
        Me.btn_aceptar.Text = "Aceptar"
        '
        'box_nombre
        '
        Me.box_nombre.BackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.box_nombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.box_nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.box_nombre.Font = New System.Drawing.Font("Consolas", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.box_nombre.ForeColor = System.Drawing.Color.FromArgb(CType(CType(86, Byte), Integer), CType(CType(101, Byte), Integer), CType(CType(115, Byte), Integer))
        Me.box_nombre.Location = New System.Drawing.Point(137, 112)
        Me.box_nombre.MaxLength = 50
        Me.box_nombre.Multiline = True
        Me.box_nombre.Name = "box_nombre"
        Me.box_nombre.Size = New System.Drawing.Size(640, 34)
        Me.box_nombre.TabIndex = 21
        Me.box_nombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'box_codigo
        '
        Me.box_codigo.BackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(63, Byte), Integer))
        Me.box_codigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.box_codigo.Enabled = False
        Me.box_codigo.Font = New System.Drawing.Font("Consolas", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.box_codigo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(234, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(237, Byte), Integer))
        Me.box_codigo.Location = New System.Drawing.Point(137, 69)
        Me.box_codigo.MaxLength = 50
        Me.box_codigo.Multiline = True
        Me.box_codigo.Name = "box_codigo"
        Me.box_codigo.Size = New System.Drawing.Size(241, 34)
        Me.box_codigo.TabIndex = 20
        Me.box_codigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblNomCatalogo
        '
        Me.lblNomCatalogo.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblNomCatalogo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.lblNomCatalogo.Location = New System.Drawing.Point(35, 114)
        Me.lblNomCatalogo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblNomCatalogo.Name = "lblNomCatalogo"
        Me.lblNomCatalogo.Size = New System.Drawing.Size(95, 25)
        Me.lblNomCatalogo.TabIndex = 19
        Me.lblNomCatalogo.Text = "Nombre:"
        Me.lblNomCatalogo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblCod
        '
        Me.LblCod.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.LblCod.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.LblCod.Location = New System.Drawing.Point(70, 78)
        Me.LblCod.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LblCod.Name = "LblCod"
        Me.LblCod.Size = New System.Drawing.Size(60, 25)
        Me.LblCod.TabIndex = 17
        Me.LblCod.Text = "ID:"
        Me.LblCod.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'marca
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1175, 622)
        Me.ControlBox = False
        Me.Controls.Add(Me.NavigationFrame1)
        Me.Name = "marca"
        Me.Text = "Catalogo de marcas"
        CType(Me.NavigationFrame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationFrame1.ResumeLayout(False)
        Me.NavigationPage1.ResumeLayout(False)
        Me.NavigationPage1.PerformLayout()
        CType(Me.tabla_de_datos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPage2.ResumeLayout(False)
        Me.panel_principal_editar.ResumeLayout(False)
        Me.panel_cuerpo_editar.ResumeLayout(False)
        Me.panel_cuerpo_editar.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents NavigationFrame1 As DevExpress.XtraBars.Navigation.NavigationFrame
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents Titulo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tabla_de_datos As DevExpress.XtraGrid.GridControl
    Friend WithEvents datos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents NavigationPage2 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents panel_principal_editar As Panel
    Friend WithEvents panel_cuerpo_editar As Panel
    Friend WithEvents btn_cancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_aceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents box_nombre As TextBox
    Friend WithEvents box_codigo As TextBox
    Friend WithEvents lblNomCatalogo As Label
    Friend WithEvents LblCod As Label
End Class
