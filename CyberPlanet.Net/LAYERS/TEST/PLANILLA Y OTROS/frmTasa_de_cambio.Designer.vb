﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTasa_de_cambio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTasa_de_cambio))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btn_Cancelar = New System.Windows.Forms.Button()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.grvBuscar = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Panel2.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btn_Cancelar)
        Me.Panel2.Controls.Add(Me.btn_Aceptar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 342)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(457, 60)
        Me.Panel2.TabIndex = 18
        '
        'btn_Cancelar
        '
        Me.btn_Cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Cancelar.Image = CType(resources.GetObject("btn_Cancelar.Image"), System.Drawing.Image)
        Me.btn_Cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Cancelar.Location = New System.Drawing.Point(331, 10)
        Me.btn_Cancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Cancelar.Name = "btn_Cancelar"
        Me.btn_Cancelar.Size = New System.Drawing.Size(104, 43)
        Me.btn_Cancelar.TabIndex = 1
        Me.btn_Cancelar.Text = "&Cancelar"
        Me.btn_Cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Cancelar.UseVisualStyleBackColor = True
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btn_Aceptar.Image = CType(resources.GetObject("btn_Aceptar.Image"), System.Drawing.Image)
        Me.btn_Aceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_Aceptar.Location = New System.Drawing.Point(219, 10)
        Me.btn_Aceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(104, 43)
        Me.btn_Aceptar.TabIndex = 0
        Me.btn_Aceptar.Text = "&Aceptar"
        Me.btn_Aceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_Aceptar.UseVisualStyleBackColor = True
        '
        'Tabla
        '
        Me.Tabla.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Location = New System.Drawing.Point(0, 0)
        Me.Tabla.MainView = Me.grvBuscar
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(457, 402)
        Me.Tabla.TabIndex = 17
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvBuscar})
        '
        'grvBuscar
        '
        Me.grvBuscar.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.grvBuscar.Appearance.RowSeparator.Options.UseBackColor = True
        Me.grvBuscar.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvBuscar.GridControl = Me.Tabla
        Me.grvBuscar.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.grvBuscar.Name = "grvBuscar"
        Me.grvBuscar.OptionsBehavior.Editable = False
        Me.grvBuscar.OptionsBehavior.ReadOnly = True
        Me.grvBuscar.OptionsCustomization.AllowColumnMoving = False
        Me.grvBuscar.OptionsFind.AllowFindPanel = False
        Me.grvBuscar.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvBuscar.OptionsView.RowAutoHeight = True
        Me.grvBuscar.OptionsView.ShowAutoFilterRow = True
        Me.grvBuscar.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.grvBuscar.OptionsView.ShowGroupPanel = False
        '
        'frmTasa_de_cambio_V2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(457, 402)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Tabla)
        Me.Name = "frmTasa_de_cambio_V2"
        Me.Text = "frmTasa_de_cambio"
        Me.Panel2.ResumeLayout(False)
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btn_Cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_Aceptar As System.Windows.Forms.Button
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvBuscar As DevExpress.XtraGrid.Views.Grid.GridView
End Class
