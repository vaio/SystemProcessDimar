﻿Public Class frmDeduccion_fija_programada

    Public Sub New()

        InitializeComponent()

        start()

    End Sub

    Private Sub start()

        AddHandler Me.Load, AddressOf responsive

    End Sub

    Private Sub responsive()

        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2) : Me.pMain.Anchor = AnchorStyles.None

    End Sub

End Class