﻿Imports System.Data.SqlClient

Public Class frmHoras_extras

    Private cmd As New SqlCommand
    Private con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Private sqlstring As String
    Private adapter As SqlDataAdapter
    Private VCantidades As New Validar With {.CantDecimal = 2}

    Private sucursal As Integer
    Private estado_documento As Boolean
    Private estado_planilla As Boolean


    Private Sub CargaI()

        Dim dt As DataTable = New DataTable()

        sqlstring = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(id_documento as int))+1,1))),8)as 'Codigo'  from Tbl_Horas_Extras where Id_sucursal=@sucursal"

        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)

            cmd.Parameters.AddWithValue("@sucursal", My.Settings.Sucursal)
            cmd.ExecuteNonQuery()


            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)

            For Each row0 In dt.Rows
                txtCodigo.Text = row0(0)

            Next

            con.Close()
            dt.Rows.Clear()

        Catch ex As Exception

            MsgBox(String.Format("Error al mostrar datos.{0}Contactar administrador de sistema{0}Error: {1}", vbNewLine, ex.Message))
            con.Close()

        End Try

    End Sub
    Private Sub Estado(ByVal my_estado As String)

        Select Case my_estado

            Case "--"

                pMaster.Enabled = False
                pmasterDeco.ForeColor = SystemColors.AppWorkspace
                pdetalleDeco.ForeColor = SystemColors.AppWorkspace
                'pDetalle.Enabled = False
                pMaster.Enabled = False
                lbEstado.Visible = False
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = False
                clear()

            Case "PENDIENTE"

                CargaI()
                pMaster.Enabled = True
                pmasterDeco.ForeColor = Color.DodgerBlue
                pdetalleDeco.ForeColor = Color.DodgerBlue
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = True

                clear()

            Case "GUARDADO"

                pMaster.Enabled = False
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = True
                btn_guardar.Enabled = False

            Case "ANULADO"

                pMaster.Enabled = False
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = False

            Case "APLICADO EN PLANILLA"

                pMaster.Enabled = False
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = False

        End Select



    End Sub
    Private Sub clear()

        txtCantHoras.Text = String.Empty
        txtTotal.Text = String.Empty
        Fecha.Value = CDate(Date.Now.Date)
        txtDescripcion.Text = String.Empty

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AddHandler Me.Resize, AddressOf responsive
        AddHandler btn_nuevo.Click, AddressOf nuevo
        AddHandler btn_eliminar.Click, AddressOf eliminar
        AddHandler btn_guardar.Click, AddressOf guardar
        AddHandler txtCantHoras.TextChanged, AddressOf calculo_basico_horas_extras
        AddHandler txtPrecioHora.TextChanged, AddressOf calculo_basico_horas_extras
        AddHandler Tabla.DoubleClick, AddressOf seleccionar_comision
        AddHandler txtNombre.Click, AddressOf get_empleado

        responsive()

        CargaI()

        Estado("--")

        Me.CenterToScreen()

        VCantidades.agregar_control(New List(Of Control)({txtCantHoras})).SoloEnterosConDecimales()

        set_historial()

    End Sub
    Private Sub responsive()


        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None

    End Sub

    Private Sub nuevo()

        Estado("PENDIENTE")

    End Sub

    Private Sub guardar()

        If ChangeToDouble(txtTotal.Text) = 0 Or ChangeToDouble(txt_cod_empleado.Text) = 0 Then Exit Sub

        Dim Question = MsgBox("Almacenar horas extras?", MsgBoxStyle.YesNo) : If Question = MsgBoxResult.No Then Exit Sub

        Dim Result = data_horas_extras.set_horas_extras(
            Funciones.Param("@id_sucursal", nSucursal, SqlDbType.Int),
            Funciones.Param("@id_documento", txtCodigo.Text, SqlDbType.NVarChar),
            Funciones.Param("@id_empleado", CInt(txt_cod_empleado.Text), SqlDbType.Int),
            Funciones.Param("@cantidad", ChangeToDouble(txtCantHoras.Text), SqlDbType.Float),
            Funciones.Param("@precio", ChangeToDouble(txtPrecioHora.Text), SqlDbType.Float),
            Funciones.Param("@total", ChangeToDouble(txtTotal.Text), SqlDbType.Float),
            Funciones.Param("@tipo_edicion", "1", SqlDbType.Int),
            Funciones.Param("@fecha", CDate(Fecha.Value), SqlDbType.Date),
            Funciones.Param("@descripcion", txtDescripcion.Text, SqlDbType.NVarChar))

        If Result = 0 Then MsgBox("Horas Extras Almacenada", MsgBoxStyle.Information) : Estado("GUARDADO") : set_historial()

    End Sub
    Private Sub eliminar()

        If ChangeToDouble(txtTotal.Text) = 0 Or ChangeToDouble(txt_cod_empleado.Text) = 0 Then Exit Sub

        If Not Me.estado_documento = True And Me.estado_planilla = True Then MsgBox("No esta permitido eliminar documentos ya tomados en planilla", MsgBoxStyle.Critical) : Exit Sub

        Dim Question = MsgBox("Eliminar Horas extras?", MsgBoxStyle.YesNo) : If Question = MsgBoxResult.No Then Exit Sub

        Dim Result = data_horas_extras.set_horas_extras(
           Funciones.Param("@id_sucursal", nSucursal, SqlDbType.Int),
           Funciones.Param("@id_documento", txtCodigo.Text, SqlDbType.NVarChar),
           Funciones.Param("@id_empleado", CInt(txt_cod_empleado.Text), SqlDbType.Int),
           Funciones.Param("@cantidad", ChangeToDouble(txtCantHoras.Text), SqlDbType.Float),
           Funciones.Param("@precio", ChangeToDouble(txtPrecioHora.Text), SqlDbType.Float),
           Funciones.Param("@total", ChangeToDouble(txtTotal.Text), SqlDbType.Float),
           Funciones.Param("@tipo_edicion", "2", SqlDbType.Bit),
           Funciones.Param("@fecha", CDate(Fecha.Value), SqlDbType.Date),
           Funciones.Param("@descripcion", txtDescripcion.Text, SqlDbType.NVarChar))

        If Result = 0 Then MsgBox("Horas extras eliminas") : Estado("--") : set_historial()

    End Sub

    Private Sub calculo_basico_horas_extras()

        txtTotal.Text = FormatNumber((ChangeToDouble(txtCantHoras.Text) * ChangeToDouble(txtPrecioHora.Text)), 2)

    End Sub

    Private Sub set_historial()

        Tabla.DataSource = data_horas_extras.get_horas_extras_por_empleado(ChangeToDouble(txt_cod_empleado.Text))

        If Not GridView1.RowCount = 0 Then

            GridView1.Columns(0).Visible = False
            GridView1.Columns(2).Visible = False
            'GridView1.Columns(6).Visible = False

            GridView1.BestFitColumns()
        Else

            GridView1.Columns.Clear()

        End If

    End Sub

    Private Sub seleccionar_comision()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        With GridView1

            Me.sucursal = .GetRowCellValue(.GetSelectedRows(0), "id_sucursal")
            Me.estado_documento = .GetRowCellValue(.GetSelectedRows(0), "estado_documento")
            Me.estado_planilla = .GetRowCellValue(.GetSelectedRows(0), "estado_planilla")
            Me.txtCodigo.Text = .GetRowCellValue(.GetSelectedRows(0), "id_documento")
            Me.Fecha.Value = CDate(.GetRowCellValue(.GetSelectedRows(0), "Fecha"))
            If Not IsDBNull(.GetRowCellValue(.GetSelectedRows(0), "Descripcion")) Then Me.txtDescripcion.Text = .GetRowCellValue(.GetSelectedRows(0), "Descripcion")
            Me.txtCantHoras.Text = .GetRowCellValue(.GetSelectedRows(0), "Cantidad")
            Me.txtPrecioHora.Text = FormatNumber(.GetRowCellValue(.GetSelectedRows(0), "Precio"), 2)
            Me.txtTotal.Text = FormatNumber(.GetRowCellValue(.GetSelectedRows(0), "Total"), 2)
            Me.txtDescripcion.Text = .GetRowCellValue(.GetSelectedRows(0), "Descripcion")

        End With

        If estado_documento = False Then Estado("ANULADO") Else Estado("GUARDADO")
        If estado_documento = True And estado_planilla = True Then Estado("APLICADO EN PLANILLA")

    End Sub

    Private Sub get_empleado()

        Dim Empleados As New frmBusqueda_empleado() With {.StartPosition = FormStartPosition.CenterScreen}

        Empleados.ShowDialog()

        set_data_empleado(Empleados)

    End Sub

    Private Sub set_data_empleado(ByRef emp As frmBusqueda_empleado)

        If emp.get_id = String.Empty Then Exit Sub

        txt_cod_empleado.Text = emp.get_id
        txtNombre.Text = emp.get_nombre + emp.get_apellido

        txtPrecioHora.Text = ((emp.get_salario / 30) / 4)

        set_historial()

    End Sub

End Class