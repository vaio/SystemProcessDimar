﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrestamos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_eliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_historial = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_guardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_nuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pdetalleDeco = New System.Windows.Forms.Label()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.RichTextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmbModalidad = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCapital = New System.Windows.Forms.TextBox()
        Me.txtTasa = New System.Windows.Forms.TextBox()
        Me.txtplazo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txt_cod_empleado = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.pMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pDetalle.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMaster.SuspendLayout()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.Panel1)
        Me.pMain.Controls.Add(Me.pDetalle)
        Me.pMain.Controls.Add(Me.pMaster)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Margin = New System.Windows.Forms.Padding(4)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1477, 761)
        Me.pMain.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_eliminar)
        Me.Panel1.Controls.Add(Me.btn_historial)
        Me.Panel1.Controls.Add(Me.btn_guardar)
        Me.Panel1.Controls.Add(Me.btn_nuevo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 694)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1477, 67)
        Me.Panel1.TabIndex = 2
        '
        'btn_eliminar
        '
        Me.btn_eliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_eliminar.Location = New System.Drawing.Point(1123, 16)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(109, 39)
        Me.btn_eliminar.TabIndex = 212
        Me.btn_eliminar.Text = "Eliminar"
        '
        'btn_historial
        '
        Me.btn_historial.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_historial.Location = New System.Drawing.Point(1008, 16)
        Me.btn_historial.Name = "btn_historial"
        Me.btn_historial.Size = New System.Drawing.Size(109, 39)
        Me.btn_historial.TabIndex = 211
        Me.btn_historial.Text = "Historial"
        '
        'btn_guardar
        '
        Me.btn_guardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_guardar.Location = New System.Drawing.Point(1238, 16)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(109, 39)
        Me.btn_guardar.TabIndex = 210
        Me.btn_guardar.Text = "Guardar"
        '
        'btn_nuevo
        '
        Me.btn_nuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_nuevo.Location = New System.Drawing.Point(1353, 16)
        Me.btn_nuevo.Name = "btn_nuevo"
        Me.btn_nuevo.Size = New System.Drawing.Size(109, 39)
        Me.btn_nuevo.TabIndex = 209
        Me.btn_nuevo.Text = "Nuevo"
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.Tabla)
        Me.pDetalle.Controls.Add(Me.Label10)
        Me.pDetalle.Controls.Add(Me.pdetalleDeco)
        Me.pDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pDetalle.Location = New System.Drawing.Point(0, 286)
        Me.pDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(1477, 475)
        Me.pDetalle.TabIndex = 1
        '
        'Tabla
        '
        Me.Tabla.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Location = New System.Drawing.Point(19, 17)
        Me.Tabla.MainView = Me.GridView1
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(1443, 385)
        Me.Tabla.TabIndex = 204
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(29, -3)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 17)
        Me.Label10.TabIndex = 203
        Me.Label10.Text = "DETALLE"
        '
        'pdetalleDeco
        '
        Me.pdetalleDeco.AutoSize = True
        Me.pdetalleDeco.BackColor = System.Drawing.Color.Transparent
        Me.pdetalleDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdetalleDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pdetalleDeco.Location = New System.Drawing.Point(1, -44)
        Me.pdetalleDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pdetalleDeco.Name = "pdetalleDeco"
        Me.pdetalleDeco.Size = New System.Drawing.Size(1973, 54)
        Me.pdetalleDeco.TabIndex = 202
        Me.pdetalleDeco.Text = "___________________________________________________________________________"
        Me.pdetalleDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.Label12)
        Me.pMaster.Controls.Add(Me.txtDescripcion)
        Me.pMaster.Controls.Add(Me.Label9)
        Me.pMaster.Controls.Add(Me.cmbModalidad)
        Me.pMaster.Controls.Add(Me.Label8)
        Me.pMaster.Controls.Add(Me.txtMonto)
        Me.pMaster.Controls.Add(Me.Label11)
        Me.pMaster.Controls.Add(Me.lbEstado)
        Me.pMaster.Controls.Add(Me.Label7)
        Me.pMaster.Controls.Add(Me.txtCodigo)
        Me.pMaster.Controls.Add(Me.dtpFechaI)
        Me.pMaster.Controls.Add(Me.Label1)
        Me.pMaster.Controls.Add(Me.Label5)
        Me.pMaster.Controls.Add(Me.txtCapital)
        Me.pMaster.Controls.Add(Me.txtTasa)
        Me.pMaster.Controls.Add(Me.txtplazo)
        Me.pMaster.Controls.Add(Me.Label6)
        Me.pMaster.Controls.Add(Me.cbTipo)
        Me.pMaster.Controls.Add(Me.txt_cod_empleado)
        Me.pMaster.Controls.Add(Me.Label3)
        Me.pMaster.Controls.Add(Me.Label4)
        Me.pMaster.Controls.Add(Me.Label54)
        Me.pMaster.Controls.Add(Me.pmasterDeco)
        Me.pMaster.Controls.Add(Me.Label2)
        Me.pMaster.Controls.Add(Me.txtNombre)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Top
        Me.pMaster.Location = New System.Drawing.Point(0, 0)
        Me.pMaster.Margin = New System.Windows.Forms.Padding(4)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(1477, 286)
        Me.pMaster.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(34, 173)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 17)
        Me.Label12.TabIndex = 214
        Me.Label12.Text = "Descripcion:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(129, 170)
        Me.txtDescripcion.MaxLength = 150
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(448, 81)
        Me.txtDescripcion.TabIndex = 213
        Me.txtDescripcion.Text = ""
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(576, 130)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 17)
        Me.Label9.TabIndex = 207
        Me.Label9.Text = "Modalidad:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbModalidad
        '
        Me.cmbModalidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModalidad.FormattingEnabled = True
        Me.cmbModalidad.Location = New System.Drawing.Point(662, 125)
        Me.cmbModalidad.Name = "cmbModalidad"
        Me.cmbModalidad.Size = New System.Drawing.Size(170, 24)
        Me.cmbModalidad.TabIndex = 206
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(601, 227)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 17)
        Me.Label8.TabIndex = 205
        Me.Label8.Text = "Monto:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMonto
        '
        Me.txtMonto.BackColor = System.Drawing.Color.White
        Me.txtMonto.Enabled = False
        Me.txtMonto.Location = New System.Drawing.Point(662, 224)
        Me.txtMonto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(170, 22)
        Me.txtMonto.TabIndex = 204
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(840, 97)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(20, 17)
        Me.Label11.TabIndex = 203
        Me.Label11.Text = "%"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbEstado
        '
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEstado.ForeColor = System.Drawing.Color.Navy
        Me.lbEstado.Location = New System.Drawing.Point(1178, 5)
        Me.lbEstado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(23, 20)
        Me.lbEstado.TabIndex = 201
        Me.lbEstado.Text = "--"
        Me.lbEstado.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(597, 197)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 17)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Capital:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(129, 45)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(104, 22)
        Me.txtCodigo.TabIndex = 0
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaI.Location = New System.Drawing.Point(129, 139)
        Me.dtpFechaI.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(109, 22)
        Me.dtpFechaI.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 49)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Codigo:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(68, 143)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Fecha:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCapital
        '
        Me.txtCapital.Location = New System.Drawing.Point(662, 194)
        Me.txtCapital.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCapital.MaxLength = 10
        Me.txtCapital.Name = "txtCapital"
        Me.txtCapital.Size = New System.Drawing.Size(170, 22)
        Me.txtCapital.TabIndex = 7
        '
        'txtTasa
        '
        Me.txtTasa.Location = New System.Drawing.Point(662, 95)
        Me.txtTasa.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTasa.MaxLength = 5
        Me.txtTasa.Name = "txtTasa"
        Me.txtTasa.Size = New System.Drawing.Size(170, 22)
        Me.txtTasa.TabIndex = 5
        '
        'txtplazo
        '
        Me.txtplazo.Location = New System.Drawing.Point(662, 158)
        Me.txtplazo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtplazo.MaxLength = 2
        Me.txtplazo.Name = "txtplazo"
        Me.txtplazo.Size = New System.Drawing.Size(170, 22)
        Me.txtplazo.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(605, 162)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 17)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Plazo:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbTipo
        '
        Me.cbTipo.Location = New System.Drawing.Point(129, 75)
        Me.cbTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.BorderColor = System.Drawing.SystemColors.AppWorkspace
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBorderColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(267, 22)
        Me.cbTipo.TabIndex = 1
        '
        'txt_cod_empleado
        '
        Me.txt_cod_empleado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txt_cod_empleado.Cursor = System.Windows.Forms.Cursors.Default
        Me.txt_cod_empleado.Enabled = False
        Me.txt_cod_empleado.Location = New System.Drawing.Point(502, 108)
        Me.txt_cod_empleado.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_cod_empleado.Name = "txt_cod_empleado"
        Me.txt_cod_empleado.ReadOnly = True
        Me.txt_cod_empleado.Size = New System.Drawing.Size(48, 22)
        Me.txt_cod_empleado.TabIndex = 116
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(80, 79)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 17)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Tipo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(609, 99)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 17)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Tasa:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(29, 5)
        Me.Label54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(146, 17)
        Me.Label54.TabIndex = 113
        Me.Label54.Text = "MASTER PRESTAMO"
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(1, -34)
        Me.pmasterDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(2493, 54)
        Me.pmasterDeco.TabIndex = 112
        Me.pmasterDeco.Text = "_________________________________________________________________________________" & _
    "______________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 111)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 17)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Nombre:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtNombre.Location = New System.Drawing.Point(129, 107)
        Me.txtNombre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(365, 22)
        Me.txtNombre.TabIndex = 2
        '
        'frmPrestamos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1477, 761)
        Me.Controls.Add(Me.pMain)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmPrestamos"
        Me.Text = "Prestamos de Empleados"
        Me.pMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCapital As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtplazo As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTasa As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txt_cod_empleado As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pdetalleDeco As System.Windows.Forms.Label
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_eliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_historial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_guardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_nuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbModalidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.RichTextBox
End Class
