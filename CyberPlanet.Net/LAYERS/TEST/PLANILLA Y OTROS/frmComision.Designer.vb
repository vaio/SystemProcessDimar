﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComision
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_eliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_guardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_nuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pdetalleDeco = New System.Windows.Forms.Label()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_cod_empleado = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.pDetalle.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMaster.SuspendLayout()
        Me.SuspendLayout()
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(1, -34)
        Me.pmasterDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(855, 54)
        Me.pmasterDeco.TabIndex = 112
        Me.pmasterDeco.Text = "________________________________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_eliminar)
        Me.Panel1.Controls.Add(Me.btn_guardar)
        Me.Panel1.Controls.Add(Me.btn_nuevo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 616)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(775, 67)
        Me.Panel1.TabIndex = 5
        '
        'btn_eliminar
        '
        Me.btn_eliminar.Location = New System.Drawing.Point(419, 12)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(109, 39)
        Me.btn_eliminar.TabIndex = 212
        Me.btn_eliminar.Text = "Eliminar"
        '
        'btn_guardar
        '
        Me.btn_guardar.Location = New System.Drawing.Point(534, 12)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(109, 39)
        Me.btn_guardar.TabIndex = 210
        Me.btn_guardar.Text = "Guardar"
        '
        'btn_nuevo
        '
        Me.btn_nuevo.Location = New System.Drawing.Point(649, 12)
        Me.btn_nuevo.Name = "btn_nuevo"
        Me.btn_nuevo.Size = New System.Drawing.Size(109, 39)
        Me.btn_nuevo.TabIndex = 209
        Me.btn_nuevo.Text = "Nuevo"
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.Tabla)
        Me.pDetalle.Controls.Add(Me.Label10)
        Me.pDetalle.Controls.Add(Me.pdetalleDeco)
        Me.pDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pDetalle.Location = New System.Drawing.Point(0, 165)
        Me.pDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(775, 518)
        Me.pDetalle.TabIndex = 4
        '
        'Tabla
        '
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Location = New System.Drawing.Point(19, 24)
        Me.Tabla.MainView = Me.GridView1
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(739, 416)
        Me.Tabla.TabIndex = 204
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(29, 3)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 17)
        Me.Label10.TabIndex = 203
        Me.Label10.Text = "HISTORIAL"
        '
        'pdetalleDeco
        '
        Me.pdetalleDeco.AutoSize = True
        Me.pdetalleDeco.BackColor = System.Drawing.Color.Transparent
        Me.pdetalleDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdetalleDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pdetalleDeco.Location = New System.Drawing.Point(1, -37)
        Me.pdetalleDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pdetalleDeco.Name = "pdetalleDeco"
        Me.pdetalleDeco.Size = New System.Drawing.Size(855, 54)
        Me.pdetalleDeco.TabIndex = 202
        Me.pdetalleDeco.Text = "________________________________"
        Me.pdetalleDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.Label8)
        Me.pMaster.Controls.Add(Me.txtMonto)
        Me.pMaster.Controls.Add(Me.lbEstado)
        Me.pMaster.Controls.Add(Me.txtCodigo)
        Me.pMaster.Controls.Add(Me.dtpFechaI)
        Me.pMaster.Controls.Add(Me.Label1)
        Me.pMaster.Controls.Add(Me.Label5)
        Me.pMaster.Controls.Add(Me.txt_cod_empleado)
        Me.pMaster.Controls.Add(Me.Label54)
        Me.pMaster.Controls.Add(Me.pmasterDeco)
        Me.pMaster.Controls.Add(Me.Label2)
        Me.pMaster.Controls.Add(Me.txtNombre)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Top
        Me.pMaster.Location = New System.Drawing.Point(0, 0)
        Me.pMaster.Margin = New System.Windows.Forms.Padding(4)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(775, 165)
        Me.pMaster.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(23, 133)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 17)
        Me.Label8.TabIndex = 205
        Me.Label8.Text = "Monto:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtMonto
        '
        Me.txtMonto.BackColor = System.Drawing.Color.White
        Me.txtMonto.Location = New System.Drawing.Point(85, 130)
        Me.txtMonto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(109, 22)
        Me.txtMonto.TabIndex = 204
        '
        'lbEstado
        '
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEstado.ForeColor = System.Drawing.Color.Navy
        Me.lbEstado.Location = New System.Drawing.Point(510, 5)
        Me.lbEstado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(23, 20)
        Me.lbEstado.TabIndex = 201
        Me.lbEstado.Text = "--"
        Me.lbEstado.Visible = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(85, 36)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(104, 22)
        Me.txtCodigo.TabIndex = 0
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaI.Location = New System.Drawing.Point(85, 101)
        Me.dtpFechaI.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(109, 22)
        Me.dtpFechaI.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 40)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Codigo:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(24, 105)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Fecha:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txt_cod_empleado
        '
        Me.txt_cod_empleado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txt_cod_empleado.Cursor = System.Windows.Forms.Cursors.Default
        Me.txt_cod_empleado.Enabled = False
        Me.txt_cod_empleado.Location = New System.Drawing.Point(485, 69)
        Me.txt_cod_empleado.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_cod_empleado.Name = "txt_cod_empleado"
        Me.txt_cod_empleado.ReadOnly = True
        Me.txt_cod_empleado.Size = New System.Drawing.Size(48, 22)
        Me.txt_cod_empleado.TabIndex = 116
        Me.txt_cod_empleado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(29, 5)
        Me.Label54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(136, 17)
        Me.Label54.TabIndex = 113
        Me.Label54.Text = "MASTER COMISION"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 73)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 17)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Nombre:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.BackColor = System.Drawing.Color.White
        Me.txtNombre.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtNombre.Location = New System.Drawing.Point(85, 69)
        Me.txtNombre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(391, 22)
        Me.txtNombre.TabIndex = 2
        '
        'frmComision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 683)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pDetalle)
        Me.Controls.Add(Me.pMaster)
        Me.Name = "frmComision"
        Me.Text = "frmComision"
        Me.Panel1.ResumeLayout(False)
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_eliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_guardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_nuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pdetalleDeco As System.Windows.Forms.Label
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_cod_empleado As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
End Class
