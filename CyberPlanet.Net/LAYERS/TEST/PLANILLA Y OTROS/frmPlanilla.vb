﻿Public Class frmPlanilla

    Public Sub New()

        InitializeComponent()

        AddHandler btn_generar.Click, AddressOf generar_planilla_quincenal
        AddHandler Tabla.DoubleClick, AddressOf set_data_planilla

    End Sub

    Private Sub generar_planilla_quincenal()

        Tabla.DataSource = data_planilla_quincenal.get_planilla_quicenal

        GridView1.Columns(0).Visible = False

        GridView1.BestFitColumns()

    End Sub

    Private Sub set_data_planilla()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        Dim emp_id As Integer
        Dim emp_nombre As String

        emp_id = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo")
        emp_nombre = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre")

        Dim Alimentador As New frmAlimentador_planilla() With {.get_codigo = emp_id, .get_Nombre = emp_nombre}

        Alimentador.ShowDialog()

        btn_generar.PerformClick()

    End Sub

End Class