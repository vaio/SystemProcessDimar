﻿
Imports DevExpress.XtraNavBar
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class frmPrestamos

    Dim cmd As New SqlCommand
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Public Tipo As Integer

    Private EnteroConDecimales As New Validar With {.CantDecimal = 2}
    Private Enteros As New Validar
    Private NumerosConDecimales As New Validar With {.CantDecimal = 2}
    Private EventosNumericos As New Validar With {.CantDecimal = 2}

    Private tabla_cuotas As New DataTable

    Private Sub frmPrestamos_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize

        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None

    End Sub
    Private Sub frmPrestamos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.CenterToScreen()

        Carga()

        If NumCatalogo = 44 Then cbTipo.SelectedIndex = 0 Else cbTipo.SelectedIndex = 1

        'Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None

        Dim Modalidad As New DataTable

        Modalidad.Columns.Add("id")
        Modalidad.Columns.Add("Descripcion")

        Modalidad.Rows.Add(0, "Quincenal")
        Modalidad.Rows.Add(0, "Mensual")

        cmbModalidad.DataSource = Modalidad

        cmbModalidad.DisplayMember = "Descripcion"
        cmbModalidad.ValueMember = "id"

        AddHandler btn_nuevo.Click, AddressOf Nuevo
        AddHandler txtCapital.TextChanged, AddressOf calculo_basico_monto
        AddHandler dtpFechaI.ValueChanged, AddressOf calculo_basico_monto
        AddHandler txtTasa.TextChanged, AddressOf calculo_basico_monto
        AddHandler txtplazo.TextChanged, AddressOf calculo_basico_monto
        AddHandler cmbModalidad.SelectedIndexChanged, AddressOf calculo_basico_monto
        AddHandler txtNombre.MouseClick, AddressOf get_empleado
        AddHandler btn_guardar.Click, AddressOf guardar
        AddHandler cbTipo.EditValueChanged, AddressOf CargaI
        AddHandler btn_historial.Click, AddressOf historial
        AddHandler btn_eliminar.Click, AddressOf eliminar

        EnteroConDecimales.agregar_control(New List(Of Control)({txtTasa})).SoloEnterosConDecimales()

        Enteros.agregar_control(New List(Of Control)({txtplazo})).SoloEnteros()

        EventosNumericos.agregar_control(New List(Of Control)({txtMonto})).ActivarEventosNumericos()

        NumerosConDecimales.agregar_control(New List(Of Control)({txtCapital})).ActivarEventosNumericos()

        ajuste_tabla()

        Estado("--")

    End Sub
    Private Sub Carga()
        dtpFechaI.Value = New Date(Now.Year, Now.Month, 1)

        Dim dt As DataTable = New DataTable()
        sqlstring = "select * from [Tbl_Prestamo.Tipo] where Activo=1"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbTipo.Properties.Items.Add(row0(1))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox(String.Format("Error al mostrar datos.{0}Contactar administrador de sistema{0}Error: {1}", vbNewLine, ex.Message))
            con.Close()
        End Try
    End Sub
    Private Sub CargaI()

        Dim dt As DataTable = New DataTable()

        sqlstring = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(Id_Prestamo as int))+1,1))),8)as 'Codigo', (select Val2 from vw_Parametros_Valores where Id_Parametro=9) as 'Default'  from Tbl_Prestamo where Id_sucursal=@sucursal and Id_Tipo=@tipo"

        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@tipo", cbTipo.SelectedIndex + 1)
            cmd.Parameters.AddWithValue("@sucursal", My.Settings.Sucursal)
            cmd.ExecuteNonQuery()


            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtCodigo.Text = row0(0)
                txtTasa.Text = FormatNumber(row0(1))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox(String.Format("Error al mostrar datos.{0}Contactar administrador de sistema{0}Error: {1}", vbNewLine, ex.Message))
            con.Close()
        End Try



    End Sub
    Private Sub Estado(ByVal my_estado As String)

        Select Case my_estado

            Case "--"
                pMaster.Enabled = False
                pmasterDeco.ForeColor = SystemColors.AppWorkspace
                pdetalleDeco.ForeColor = SystemColors.AppWorkspace
                pDetalle.Enabled = False
                pMaster.Enabled = False
                lbEstado.Visible = False
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = False
                btn_historial.Enabled = False
                clear()


            Case "PENDIENTE"

                clear()
                CargaI()
                pMaster.Enabled = True
                pDetalle.Enabled = True
                pmasterDeco.ForeColor = Color.DodgerBlue
                pdetalleDeco.ForeColor = SystemColors.AppWorkspace
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = True
                btn_historial.Enabled = True

            Case "GUARDADO"

                pMaster.Enabled = False
                pDetalle.Enabled = True
                pMaster.ForeColor = SystemColors.AppWorkspace
                pdetalleDeco.ForeColor = SystemColors.AppWorkspace
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = True
                btn_guardar.Enabled = False
                btn_historial.Enabled = True

            Case "GUARDADO Y CON MOVIMIENTO"

                pMaster.Enabled = False
                pDetalle.Enabled = True
                pMaster.ForeColor = SystemColors.AppWorkspace
                pdetalleDeco.ForeColor = SystemColors.AppWorkspace
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = False

            Case "ANULADO"

                pMaster.Enabled = False
                pDetalle.Enabled = False
                pMaster.ForeColor = SystemColors.HotTrack
                pdetalleDeco.ForeColor = SystemColors.HotTrack
                lbEstado.Visible = True
                lbEstado.Text = my_estado
                btn_eliminar.Enabled = False
                btn_guardar.Enabled = False

        End Select
    End Sub

    Sub Nuevo()

        Estado("PENDIENTE")

    End Sub

    Private Sub clear()

        txtCodigo.Text = String.Empty
        cbTipo.SelectedIndex = 0
        txtTasa.Text = FormatNumber(5, 2)
        txtplazo.Text = String.Empty
        txtCapital.Text = String.Empty
        txtMonto.Text = String.Empty
        cmbModalidad.SelectedIndex = 0
        txtDescripcion.Text = String.Empty

        ajuste_tabla()

    End Sub
    Private Sub get_empleado()

        Dim Empleados As New frmBusqueda_empleado() With {.StartPosition = FormStartPosition.CenterScreen}

        Empleados.ShowDialog()

        set_data_empleado(Empleados)

    End Sub

    Private Sub set_data_empleado(ByRef emp As frmBusqueda_empleado)

        If emp.get_id = String.Empty Then Exit Sub

        txt_cod_empleado.Text = emp.get_id
        txtNombre.Text = emp.get_nombre + emp.get_apellido

    End Sub

    Private Sub calculo_basico_monto()

        If ChangeToDouble(txtTasa.Text) = 0 Or ChangeToDouble(txtplazo.Text) = 0 Or ChangeToDouble(txtCapital.Text) = 0 Then Tabla.DataSource = tabla_cuotas : Exit Sub

        Dim i As Double = (ChangeToDouble(txtTasa.Text) / 100)
        Dim capital As Double = ChangeToDouble(txtCapital.Text)
        Dim n As Integer
        Dim fecha_de_prestamo As Date = dtpFechaI.Value
        Dim Dias As Integer = 0

        If cmbModalidad.SelectedIndex = 0 Then n = ChangeToDouble(txtplazo.Text) * 2 : Dias = 15 Else n = ChangeToDouble(txtplazo.Text) * 1 : Dias = 30

        Dim interes_por_cuota As Double = ((capital * i))
        Dim Cuota As Double = (capital / n)

        txtMonto.Text = FormatNumber(((capital * (i * n)) + capital), 2)

        tabla_cuotas.Rows.Clear()

        For i = 0 To n - 1

            tabla_cuotas.Rows.Add((1 + i), fecha_de_prestamo.ToShortDateString, FormatNumber(Cuota, 2), FormatNumber(interes_por_cuota, 2), FormatNumber((Cuota + interes_por_cuota), 2), False)

            fecha_de_prestamo = fecha_de_prestamo.AddDays(Dias)

        Next

        Tabla.DataSource = tabla_cuotas

        ocultar_estado_cuotas()

        GridView1.BestFitColumns()

    End Sub

    Private Sub ajuste_tabla()

        tabla_cuotas = New DataTable

        tabla_cuotas.Columns.Add("ID")
        tabla_cuotas.Columns.Add("Fecha de pago")
        tabla_cuotas.Columns.Add("Cuota")
        tabla_cuotas.Columns.Add("Interes (%)")
        tabla_cuotas.Columns.Add("Sub Total")
        tabla_cuotas.Columns.Add("estado")

        Tabla.DataSource = tabla_cuotas

        ocultar_estado_cuotas()

    End Sub

    Private Sub ocultar_estado_cuotas()

        GridView1.Columns(5).Visible = False

        '---------------- ademas de ocultar centra las columnas - Bob estuvo aqui xD  -------------------------------

        For i = 0 To GridView1.Columns.Count - 1

            GridView1.Columns(i).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

        Next

    End Sub

    Private Sub guardar()

        If ChangeToDouble(txtMonto.Text) = 0 Or ChangeToDouble(txt_cod_empleado.Text) = 0 Then Exit Sub

        Dim Question = MsgBox("Seguro desea almacenar el prestamo?", MsgBoxStyle.YesNo) : If Question = MsgBoxResult.No Then Exit Sub

        Dim Result = data_prestamos.set_prestamo(
            Funciones.Param("@id_sucursal", nSucursal, SqlDbType.Int),
            Funciones.Param("@id_prestamo", txtCodigo.Text, SqlDbType.NVarChar),
            Funciones.Param("@id_tipo", cbTipo.SelectedIndex + 1, SqlDbType.Int),
            Funciones.Param("@id_empleado", CInt(txt_cod_empleado.Text), SqlDbType.Int),
            Funciones.Param("@fecha", CDate(dtpFechaI.Value), SqlDbType.Date),
            Funciones.Param("@descripcion", txtDescripcion.Text, SqlDbType.NVarChar),
            Funciones.Param("@tasa", ChangeToDouble(txtTasa.Text), SqlDbType.Decimal),
            Funciones.Param("@modalidad", cmbModalidad.SelectedIndex, SqlDbType.Int),
            Funciones.Param("@plazo", ChangeToDouble(txtplazo.Text), SqlDbType.Int),
            Funciones.Param("@capital", ChangeToDouble(txtCapital.Text), SqlDbType.Money),
            Funciones.Param("@estado", "1", SqlDbType.Bit),
            Funciones.Param("@tipo_edicion", "1", SqlDbType.Int))

        If Result = 0 Then MsgBox("Prestamo Almacenado") : Estado("GUARDADO")

    End Sub
    Private Sub set_datos_de_busqueda(ByRef datos As frmBusqueda_prestamo)

        If datos.get_codigo_prestamo = String.Empty Then Exit Sub

        txt_cod_empleado.Text = datos.get_id_empleado
        txtCodigo.Text = datos.get_codigo_prestamo
        txtNombre.Text = datos.get_nombre
        dtpFechaI.Value = datos.get_fecha
        txtTasa.Text = FormatNumber(datos.get_tasa, 2)
        cmbModalidad.SelectedIndex = datos.get_modalidad
        txtplazo.Text = datos.get_cuotas
        txtCapital.Text = FormatNumber(datos.get_capital, 2)
        txtDescripcion.Text = datos.get_descripcion

        If datos.get_monto_cancelado = 0 Then Estado("GUARDADO") Else Estado("GUARDADO Y CON MOVIMIENTO")

    End Sub
    Private Sub historial()

        Dim buscar_prestamos As New frmBusqueda_prestamo(ChangeToDouble(txt_cod_empleado.Text)) With {.StartPosition = FormStartPosition.CenterScreen}

        buscar_prestamos.ShowDialog()

        set_datos_de_busqueda(buscar_prestamos)

    End Sub

    Private Sub eliminar()

        If ChangeToDouble(txtMonto.Text) = 0 Or ChangeToDouble(txt_cod_empleado.Text) = 0 Then Exit Sub

        Dim Question = MsgBox("Seguro desea eliminar el prestamo?", MsgBoxStyle.YesNo) : If Question = MsgBoxResult.No Then Exit Sub



        Dim Result = data_prestamos.set_prestamo(
            Funciones.Param("@id_sucursal", nSucursal, SqlDbType.Int),
            Funciones.Param("@id_prestamo", txtCodigo.Text, SqlDbType.NVarChar),
            Funciones.Param("@id_tipo", cbTipo.SelectedIndex + 1, SqlDbType.Int),
            Funciones.Param("@id_empleado", CInt(txt_cod_empleado.Text), SqlDbType.Int),
            Funciones.Param("@fecha", CDate(dtpFechaI.Value), SqlDbType.Date),
            Funciones.Param("@descripcion", txtDescripcion.Text, SqlDbType.NVarChar),
            Funciones.Param("@tasa", ChangeToDouble(txtTasa.Text), SqlDbType.Decimal),
            Funciones.Param("@modalidad", cmbModalidad.SelectedIndex, SqlDbType.Int),
            Funciones.Param("@plazo", ChangeToDouble(txtplazo.Text), SqlDbType.Int),
            Funciones.Param("@capital", ChangeToDouble(txtCapital.Text), SqlDbType.Money),
            Funciones.Param("@estado", "1", SqlDbType.Bit),
            Funciones.Param("@tipo_edicion", "2", SqlDbType.Int))

        If Result = 0 Then MsgBox("Prestamo eliminado", MsgBoxStyle.Information) : Estado("--") Else MsgBox("El prestamo ya presenta movimiento en planilla. (No se elimino)", MsgBoxStyle.Critical)

    End Sub

End Class