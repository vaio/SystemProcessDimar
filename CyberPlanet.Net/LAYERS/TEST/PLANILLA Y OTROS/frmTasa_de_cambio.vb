﻿Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository

Public Class frmTasa_de_cambio

    Private Tasa As Double = 0

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        start()

    End Sub

    Private Sub start()

        AddHandler btn_Aceptar.Click, AddressOf aceptar
        AddHandler btn_Cancelar.Click, AddressOf cancelar

        Tabla.DataSource = data_tasa_de_cambio.get_historial_tasa_de_cambio

    End Sub

    Private Sub get_tasa_from_historial()

        If grvBuscar.SelectedRowsCount = 0 Then Exit Sub

        Tasa = grvBuscar.GetRowCellValue(grvBuscar.GetSelectedRows(0), "Tasa_Oficial").ToString()

    End Sub

    Public ReadOnly Property get_tasa As Double
        Get
            Return Me.Tasa
        End Get
    End Property

    Private Sub aceptar()

        get_tasa_from_historial()

        Me.Close()

    End Sub

    Private Sub cancelar()

        Me.Tasa = 0

        Me.Close()

    End Sub



End Class