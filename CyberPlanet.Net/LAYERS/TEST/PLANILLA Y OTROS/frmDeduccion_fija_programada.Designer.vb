﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeduccion_fija_programada
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_eliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_guardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_nuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.Tabla = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pdetalleDeco = New System.Windows.Forms.Label()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_cod_empleado = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pEditar = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmbModalidad = New System.Windows.Forms.ComboBox()
        Me.txtplazo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.pMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pDetalle.SuspendLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMaster.SuspendLayout()
        Me.pEditar.SuspendLayout()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.Panel1)
        Me.pMain.Controls.Add(Me.pDetalle)
        Me.pMain.Controls.Add(Me.pMaster)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Margin = New System.Windows.Forms.Padding(4)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1325, 642)
        Me.pMain.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_eliminar)
        Me.Panel1.Controls.Add(Me.btn_guardar)
        Me.Panel1.Controls.Add(Me.btn_nuevo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 575)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1325, 67)
        Me.Panel1.TabIndex = 2
        '
        'btn_eliminar
        '
        Me.btn_eliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_eliminar.Location = New System.Drawing.Point(971, 16)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(109, 39)
        Me.btn_eliminar.TabIndex = 212
        Me.btn_eliminar.Text = "Eliminar"
        '
        'btn_guardar
        '
        Me.btn_guardar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_guardar.Location = New System.Drawing.Point(1086, 16)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(109, 39)
        Me.btn_guardar.TabIndex = 210
        Me.btn_guardar.Text = "Guardar"
        '
        'btn_nuevo
        '
        Me.btn_nuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_nuevo.Location = New System.Drawing.Point(1201, 16)
        Me.btn_nuevo.Name = "btn_nuevo"
        Me.btn_nuevo.Size = New System.Drawing.Size(109, 39)
        Me.btn_nuevo.TabIndex = 209
        Me.btn_nuevo.Text = "Nuevo"
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.Tabla)
        Me.pDetalle.Controls.Add(Me.Label10)
        Me.pDetalle.Controls.Add(Me.pdetalleDeco)
        Me.pDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pDetalle.Location = New System.Drawing.Point(0, 188)
        Me.pDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(1325, 454)
        Me.pDetalle.TabIndex = 1
        '
        'Tabla
        '
        Me.Tabla.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tabla.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Location = New System.Drawing.Point(19, 17)
        Me.Tabla.MainView = Me.GridView1
        Me.Tabla.Margin = New System.Windows.Forms.Padding(4)
        Me.Tabla.Name = "Tabla"
        Me.Tabla.Size = New System.Drawing.Size(1291, 364)
        Me.Tabla.TabIndex = 204
        Me.Tabla.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(29, -3)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 17)
        Me.Label10.TabIndex = 203
        Me.Label10.Text = "DETALLE"
        '
        'pdetalleDeco
        '
        Me.pdetalleDeco.AutoSize = True
        Me.pdetalleDeco.BackColor = System.Drawing.Color.Transparent
        Me.pdetalleDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdetalleDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pdetalleDeco.Location = New System.Drawing.Point(1, -44)
        Me.pdetalleDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pdetalleDeco.Name = "pdetalleDeco"
        Me.pdetalleDeco.Size = New System.Drawing.Size(1973, 54)
        Me.pdetalleDeco.TabIndex = 202
        Me.pdetalleDeco.Text = "___________________________________________________________________________"
        Me.pdetalleDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.pEditar)
        Me.pMaster.Controls.Add(Me.Label3)
        Me.pMaster.Controls.Add(Me.lbEstado)
        Me.pMaster.Controls.Add(Me.txtCodigo)
        Me.pMaster.Controls.Add(Me.dtpFechaI)
        Me.pMaster.Controls.Add(Me.Label1)
        Me.pMaster.Controls.Add(Me.Label5)
        Me.pMaster.Controls.Add(Me.txt_cod_empleado)
        Me.pMaster.Controls.Add(Me.Label54)
        Me.pMaster.Controls.Add(Me.pmasterDeco)
        Me.pMaster.Controls.Add(Me.Label2)
        Me.pMaster.Controls.Add(Me.txtNombre)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Top
        Me.pMaster.Location = New System.Drawing.Point(0, 0)
        Me.pMaster.Margin = New System.Windows.Forms.Padding(4)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(1325, 188)
        Me.pMaster.TabIndex = 0
        '
        'lbEstado
        '
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEstado.ForeColor = System.Drawing.Color.Navy
        Me.lbEstado.Location = New System.Drawing.Point(1178, 5)
        Me.lbEstado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(23, 20)
        Me.lbEstado.TabIndex = 201
        Me.lbEstado.Text = "--"
        Me.lbEstado.Visible = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(129, 34)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(170, 22)
        Me.txtCodigo.TabIndex = 0
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaI.Location = New System.Drawing.Point(129, 64)
        Me.dtpFechaI.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(170, 22)
        Me.dtpFechaI.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 38)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Codigo:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(68, 68)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Fecha:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txt_cod_empleado
        '
        Me.txt_cod_empleado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txt_cod_empleado.Cursor = System.Windows.Forms.Cursors.Default
        Me.txt_cod_empleado.Enabled = False
        Me.txt_cod_empleado.Location = New System.Drawing.Point(611, 98)
        Me.txt_cod_empleado.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_cod_empleado.Name = "txt_cod_empleado"
        Me.txt_cod_empleado.ReadOnly = True
        Me.txt_cod_empleado.Size = New System.Drawing.Size(75, 22)
        Me.txt_cod_empleado.TabIndex = 116
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(29, 5)
        Me.Label54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(199, 17)
        Me.Label54.TabIndex = 113
        Me.Label54.Text = "MASTER DEDUCCIONES FIJA"
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(1, -34)
        Me.pmasterDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(2493, 54)
        Me.pmasterDeco.TabIndex = 112
        Me.pmasterDeco.Text = "_________________________________________________________________________________" & _
    "______________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 102)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 17)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Nombre:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNombre
        '
        Me.txtNombre.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtNombre.Location = New System.Drawing.Point(129, 98)
        Me.txtNombre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(365, 22)
        Me.txtNombre.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(502, 101)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(101, 17)
        Me.Label3.TabIndex = 208
        Me.Label3.Text = "No. Empleado:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pEditar
        '
        Me.pEditar.Controls.Add(Me.Label9)
        Me.pEditar.Controls.Add(Me.cmbModalidad)
        Me.pEditar.Controls.Add(Me.txtplazo)
        Me.pEditar.Controls.Add(Me.Label6)
        Me.pEditar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pEditar.Location = New System.Drawing.Point(0, 127)
        Me.pEditar.Name = "pEditar"
        Me.pEditar.Size = New System.Drawing.Size(1325, 61)
        Me.pEditar.TabIndex = 209
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(36, 8)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 17)
        Me.Label9.TabIndex = 211
        Me.Label9.Text = "Descripcion:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbModalidad
        '
        Me.cmbModalidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModalidad.FormattingEnabled = True
        Me.cmbModalidad.Location = New System.Drawing.Point(129, 5)
        Me.cmbModalidad.Name = "cmbModalidad"
        Me.cmbModalidad.Size = New System.Drawing.Size(365, 24)
        Me.cmbModalidad.TabIndex = 210
        '
        'txtplazo
        '
        Me.txtplazo.Location = New System.Drawing.Point(611, 3)
        Me.txtplazo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtplazo.MaxLength = 2
        Me.txtplazo.Name = "txtplazo"
        Me.txtplazo.Size = New System.Drawing.Size(164, 22)
        Me.txtplazo.TabIndex = 208
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(520, 3)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 17)
        Me.Label6.TabIndex = 209
        Me.Label6.Text = "Monto (C$):"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmDeduccion_fija_programada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1325, 642)
        Me.Controls.Add(Me.pMain)
        Me.Name = "frmDeduccion_fija_programada"
        Me.Text = "frmDeduccion_fija_programada"
        Me.pMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        CType(Me.Tabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        Me.pEditar.ResumeLayout(False)
        Me.pEditar.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_eliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_guardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_nuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents Tabla As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents pdetalleDeco As System.Windows.Forms.Label
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_cod_empleado As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pEditar As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbModalidad As System.Windows.Forms.ComboBox
    Friend WithEvents txtplazo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
