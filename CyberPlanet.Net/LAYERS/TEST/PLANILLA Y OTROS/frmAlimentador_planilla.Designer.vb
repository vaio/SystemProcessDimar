﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAlimentador_planilla
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.pBody = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.pButtom = New System.Windows.Forms.Panel()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.pTop = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtNoDocumento = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Fecha = New DevExpress.XtraEditors.DateEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEmpNombre = New System.Windows.Forms.TextBox()
        Me.txtEmpCod = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.pMaster.SuspendLayout()
        Me.pBody.SuspendLayout()
        Me.pButtom.SuspendLayout()
        Me.pTop.SuspendLayout()
        CType(Me.Fecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Fecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.pBody)
        Me.pMaster.Controls.Add(Me.pButtom)
        Me.pMaster.Controls.Add(Me.pTop)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMaster.Location = New System.Drawing.Point(0, 0)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(529, 830)
        Me.pMaster.TabIndex = 0
        '
        'pBody
        '
        Me.pBody.Controls.Add(Me.Label16)
        Me.pBody.Controls.Add(Me.TextBox13)
        Me.pBody.Controls.Add(Me.Label17)
        Me.pBody.Controls.Add(Me.TextBox14)
        Me.pBody.Controls.Add(Me.Label15)
        Me.pBody.Controls.Add(Me.TextBox12)
        Me.pBody.Controls.Add(Me.Label12)
        Me.pBody.Controls.Add(Me.TextBox9)
        Me.pBody.Controls.Add(Me.Label14)
        Me.pBody.Controls.Add(Me.TextBox11)
        Me.pBody.Controls.Add(Me.Label10)
        Me.pBody.Controls.Add(Me.TextBox7)
        Me.pBody.Controls.Add(Me.Label11)
        Me.pBody.Controls.Add(Me.TextBox8)
        Me.pBody.Controls.Add(Me.Label8)
        Me.pBody.Controls.Add(Me.TextBox5)
        Me.pBody.Controls.Add(Me.Label7)
        Me.pBody.Controls.Add(Me.TextBox4)
        Me.pBody.Controls.Add(Me.Label6)
        Me.pBody.Controls.Add(Me.TextBox3)
        Me.pBody.Controls.Add(Me.Label3)
        Me.pBody.Controls.Add(Me.Label4)
        Me.pBody.Controls.Add(Me.Label18)
        Me.pBody.Controls.Add(Me.Label19)
        Me.pBody.Controls.Add(Me.Label20)
        Me.pBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBody.Location = New System.Drawing.Point(0, 121)
        Me.pBody.Name = "pBody"
        Me.pBody.Size = New System.Drawing.Size(529, 655)
        Me.pBody.TabIndex = 2
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(23, 596)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(81, 17)
        Me.Label16.TabIndex = 147
        Me.Label16.Text = "(-) Anticipo:"
        '
        'TextBox13
        '
        Me.TextBox13.BackColor = System.Drawing.Color.White
        Me.TextBox13.Location = New System.Drawing.Point(26, 616)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(473, 22)
        Me.TextBox13.TabIndex = 146
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(23, 548)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(131, 17)
        Me.Label17.TabIndex = 145
        Me.Label17.Text = "Otras deducciones:"
        '
        'TextBox14
        '
        Me.TextBox14.BackColor = System.Drawing.Color.White
        Me.TextBox14.Location = New System.Drawing.Point(26, 568)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(473, 22)
        Me.TextBox14.TabIndex = 144
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(23, 499)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(25, 17)
        Me.Label15.TabIndex = 143
        Me.Label15.Text = "IR:"
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.Color.White
        Me.TextBox12.Location = New System.Drawing.Point(26, 519)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(473, 22)
        Me.TextBox12.TabIndex = 142
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(23, 425)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 17)
        Me.Label12.TabIndex = 141
        Me.Label12.Text = "(+) Anticipo:"
        '
        'TextBox9
        '
        Me.TextBox9.BackColor = System.Drawing.Color.White
        Me.TextBox9.Location = New System.Drawing.Point(26, 445)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(473, 22)
        Me.TextBox9.TabIndex = 140
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 326)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(105, 17)
        Me.Label14.TabIndex = 137
        Me.Label14.Text = "Otros ingresos:"
        '
        'TextBox11
        '
        Me.TextBox11.BackColor = System.Drawing.Color.White
        Me.TextBox11.Location = New System.Drawing.Point(26, 346)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(473, 22)
        Me.TextBox11.TabIndex = 136
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(23, 225)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 17)
        Me.Label10.TabIndex = 133
        Me.Label10.Text = "Subsidio:"
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.Color.White
        Me.TextBox7.Location = New System.Drawing.Point(26, 245)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(473, 22)
        Me.TextBox7.TabIndex = 132
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(23, 176)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 17)
        Me.Label11.TabIndex = 131
        Me.Label11.Text = "Feriados:"
        '
        'TextBox8
        '
        Me.TextBox8.BackColor = System.Drawing.Color.White
        Me.TextBox8.Location = New System.Drawing.Point(26, 196)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(473, 22)
        Me.TextBox8.TabIndex = 130
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(23, 127)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 17)
        Me.Label8.TabIndex = 129
        Me.Label8.Text = "Vacaciones:"
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.White
        Me.TextBox5.Location = New System.Drawing.Point(26, 147)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(473, 22)
        Me.TextBox5.TabIndex = 128
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 17)
        Me.Label7.TabIndex = 127
        Me.Label7.Text = "Septimo dia:"
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.White
        Me.TextBox4.Location = New System.Drawing.Point(26, 97)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(473, 22)
        Me.TextBox4.TabIndex = 126
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 28)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(111, 17)
        Me.Label6.TabIndex = 125
        Me.Label6.Text = "Dias trabajados:"
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.White
        Me.TextBox3.Location = New System.Drawing.Point(26, 48)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(473, 22)
        Me.TextBox3.TabIndex = 124
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label3.Location = New System.Drawing.Point(28, 4)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 17)
        Me.Label3.TabIndex = 121
        Me.Label3.Text = "INGRESOS"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.Location = New System.Drawing.Point(6, -36)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(855, 54)
        Me.Label4.TabIndex = 120
        Me.Label4.Text = "________________________________"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label18.Location = New System.Drawing.Point(28, 476)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 17)
        Me.Label18.TabIndex = 149
        Me.Label18.Text = "EGRESOS"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label19.Location = New System.Drawing.Point(6, 436)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(855, 54)
        Me.Label19.TabIndex = 148
        Me.Label19.Text = "________________________________"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label20.Location = New System.Drawing.Point(-12, 602)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(855, 54)
        Me.Label20.TabIndex = 150
        Me.Label20.Text = "________________________________"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pButtom
        '
        Me.pButtom.Controls.Add(Me.SimpleButton3)
        Me.pButtom.Controls.Add(Me.SimpleButton2)
        Me.pButtom.Controls.Add(Me.SimpleButton1)
        Me.pButtom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pButtom.Location = New System.Drawing.Point(0, 776)
        Me.pButtom.Name = "pButtom"
        Me.pButtom.Size = New System.Drawing.Size(529, 54)
        Me.pButtom.TabIndex = 1
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(112, 10)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(100, 26)
        Me.SimpleButton3.TabIndex = 2
        Me.SimpleButton3.Text = "&Historial"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(218, 10)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(100, 26)
        Me.SimpleButton2.TabIndex = 1
        Me.SimpleButton2.Text = "&Eliminar"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(324, 10)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(100, 26)
        Me.SimpleButton1.TabIndex = 0
        Me.SimpleButton1.Text = "&Guardar"
        '
        'pTop
        '
        Me.pTop.Controls.Add(Me.Label21)
        Me.pTop.Controls.Add(Me.txtNoDocumento)
        Me.pTop.Controls.Add(Me.Label5)
        Me.pTop.Controls.Add(Me.Fecha)
        Me.pTop.Controls.Add(Me.Label2)
        Me.pTop.Controls.Add(Me.Label1)
        Me.pTop.Controls.Add(Me.txtEmpNombre)
        Me.pTop.Controls.Add(Me.txtEmpCod)
        Me.pTop.Controls.Add(Me.Label54)
        Me.pTop.Controls.Add(Me.pmasterDeco)
        Me.pTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.pTop.Location = New System.Drawing.Point(0, 0)
        Me.pTop.Name = "pTop"
        Me.pTop.Size = New System.Drawing.Size(529, 121)
        Me.pTop.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(23, 29)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(110, 17)
        Me.Label21.TabIndex = 127
        Me.Label21.Text = "No. Documento:"
        '
        'txtNoDocumento
        '
        Me.txtNoDocumento.BackColor = System.Drawing.Color.White
        Me.txtNoDocumento.Enabled = False
        Me.txtNoDocumento.Location = New System.Drawing.Point(26, 49)
        Me.txtNoDocumento.Name = "txtNoDocumento"
        Me.txtNoDocumento.Size = New System.Drawing.Size(161, 22)
        Me.txtNoDocumento.TabIndex = 126
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(195, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 17)
        Me.Label5.TabIndex = 125
        Me.Label5.Text = "Fecha:"
        '
        'Fecha
        '
        Me.Fecha.EditValue = Nothing
        Me.Fecha.Location = New System.Drawing.Point(193, 49)
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Fecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.Fecha.Size = New System.Drawing.Size(156, 22)
        Me.Fecha.TabIndex = 124
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(95, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 17)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "empleado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 17)
        Me.Label1.TabIndex = 118
        Me.Label1.Text = "codigo:"
        '
        'txtEmpNombre
        '
        Me.txtEmpNombre.BackColor = System.Drawing.Color.White
        Me.txtEmpNombre.Enabled = False
        Me.txtEmpNombre.Location = New System.Drawing.Point(98, 95)
        Me.txtEmpNombre.Name = "txtEmpNombre"
        Me.txtEmpNombre.Size = New System.Drawing.Size(401, 22)
        Me.txtEmpNombre.TabIndex = 117
        '
        'txtEmpCod
        '
        Me.txtEmpCod.BackColor = System.Drawing.Color.White
        Me.txtEmpCod.Enabled = False
        Me.txtEmpCod.Location = New System.Drawing.Point(26, 95)
        Me.txtEmpCod.Name = "txtEmpCod"
        Me.txtEmpCod.Size = New System.Drawing.Size(66, 22)
        Me.txtEmpCod.TabIndex = 116
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label54.Location = New System.Drawing.Point(23, 9)
        Me.Label54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(198, 34)
        Me.Label54.TabIndex = 115
        Me.Label54.Text = "ALIMENTADOR DE PLANILLA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(1, -31)
        Me.pmasterDeco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(855, 54)
        Me.pmasterDeco.TabIndex = 114
        Me.pmasterDeco.Text = "________________________________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmAlimentador_planilla
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(529, 830)
        Me.Controls.Add(Me.pMaster)
        Me.Name = "frmAlimentador_planilla"
        Me.pMaster.ResumeLayout(False)
        Me.pBody.ResumeLayout(False)
        Me.pBody.PerformLayout()
        Me.pButtom.ResumeLayout(False)
        Me.pTop.ResumeLayout(False)
        Me.pTop.PerformLayout()
        CType(Me.Fecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Fecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents pBody As System.Windows.Forms.Panel
    Friend WithEvents pButtom As System.Windows.Forms.Panel
    Friend WithEvents pTop As System.Windows.Forms.Panel
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtEmpNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpCod As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Fecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtNoDocumento As System.Windows.Forms.TextBox
End Class
