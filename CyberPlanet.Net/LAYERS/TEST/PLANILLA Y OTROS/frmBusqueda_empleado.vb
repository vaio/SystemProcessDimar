﻿Public Class frmBusqueda_empleado

    Private id As String
    Private cedula As String
    Private nombre As String
    Private apellido As String
    Private ingreso As Date
    Private salario As Double
    Private area As String
    Private cargo As String
    Private activo As Boolean

    Public ReadOnly Property get_id As String
        Get
            Return Me.id
        End Get
    End Property
    Public ReadOnly Property get_cedula As String
        Get
            Return Me.cedula
        End Get
    End Property
    Public ReadOnly Property get_nombre As String
        Get
            Return Me.nombre
        End Get
    End Property
    Public ReadOnly Property get_apellido As String
        Get
            Return Me.apellido
        End Get
    End Property
    Public ReadOnly Property get_ingreso As Date
        Get
            Return Me.ingreso
        End Get
    End Property
    Public ReadOnly Property get_salario As Double
        Get
            Return Me.salario
        End Get
    End Property
    Public ReadOnly Property get_area As String
        Get
            Return Me.area
        End Get
    End Property
    Public ReadOnly Property get_cargo As String
        Get
            Return Me.cargo
        End Get
    End Property
    Public ReadOnly Property get_activo As String
        Get
            Return Me.activo
        End Get
    End Property
   
    Public Sub New()

        InitializeComponent()

        AddHandler btn_cancelar.Click, AddressOf cancelar
        AddHandler Tabla.DoubleClick, AddressOf aceptar
        AddHandler btn_aceptar.Click, AddressOf aceptar

        Tabla.DataSource = data_empleado.get_data_buscador()

        ajuste_tabla()

    End Sub

    Private Sub ajuste_tabla()

        If GridView1.Columns.Count = 0 Then Exit Sub

        GridView1.Columns(0).Visible = False
        GridView1.Columns(4).Visible = False
        GridView1.Columns(5).Visible = False
        GridView1.Columns(8).Visible = False

    End Sub

    Private Sub aceptar()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        With GridView1

            Me.id = .GetRowCellValue(.GetSelectedRows(0), "id")
            Me.cedula = .GetRowCellValue(.GetSelectedRows(0), "Cedula")
            Me.nombre = .GetRowCellValue(.GetSelectedRows(0), "Nombre")
            Me.apellido = .GetRowCellValue(.GetSelectedRows(0), "Apellido")
            Me.ingreso = .GetRowCellValue(.GetSelectedRows(0), "Ingreso")
            Me.salario = .GetRowCellValue(.GetSelectedRows(0), "Salario")
            Me.area = .GetRowCellValue(.GetSelectedRows(0), "Area")
            Me.cargo = .GetRowCellValue(.GetSelectedRows(0), "Cargo")
            Me.activo = .GetRowCellValue(.GetSelectedRows(0), "Activo")

        End With

        Me.Close()

    End Sub

    Private Sub cancelar()

        Me.id = String.Empty
        Me.cedula = String.Empty
        Me.nombre = String.Empty
        Me.apellido = String.Empty
        Me.ingreso = Date.Now
        Me.salario = 0
        Me.area = String.Empty
        Me.cargo = String.Empty
        Me.activo = 0

        Me.Close()

    End Sub

End Class