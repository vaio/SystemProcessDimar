﻿Public Class frmBusqueda_prestamo

    Private empleado As Integer

    Private id_sucursal As Integer
    Private nombre As String
    Private tipo As Integer
    Private fecha As Date
    Private codigo_prestamo As String
    Private tasa As Double
    Private cuotas As Double
    Private capital As Double
    Private estado As Integer
    Private monto_cancelado As Double
    Private modalidad As Integer
    Private descripcion As String

    Public ReadOnly Property get_id_empleado As Integer
        Get
            Return Me.empleado
        End Get
    End Property
    Public ReadOnly Property get_sucursal As Integer
        Get
            Return Me.id_sucursal
        End Get
    End Property
    Public ReadOnly Property get_nombre As String
        Get
            Return Me.nombre
        End Get
    End Property
    Public ReadOnly Property get_tipo As Integer
        Get
            Return Me.tipo
        End Get
    End Property
    Public ReadOnly Property get_fecha As Date
        Get
            Return Me.fecha
        End Get
    End Property
    Public ReadOnly Property get_codigo_prestamo As String
        Get
            Return Me.codigo_prestamo
        End Get
    End Property
    Public ReadOnly Property get_tasa As Double
        Get
            Return Me.tasa
        End Get
    End Property
    Public ReadOnly Property get_cuotas As Double
        Get
            Return Me.cuotas
        End Get
    End Property
    Public ReadOnly Property get_capital As Double
        Get
            Return Me.capital
        End Get
    End Property
    Public ReadOnly Property get_estado
        Get
            Return Me.estado
        End Get
    End Property
    Public ReadOnly Property get_monto_cancelado As Double
        Get
            Return Me.monto_cancelado
        End Get
    End Property
    Public ReadOnly Property get_modalidad As Integer
        Get
            Return Me.modalidad
        End Get
    End Property
    Public ReadOnly Property get_descripcion As String
        Get
            Return Me.descripcion
        End Get
    End Property
    Public Sub New(ByRef id_empleado As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.


        AddHandler btn_aceptar.Click, AddressOf aceptar
        AddHandler btn_cancelar.Click, AddressOf cancelar
        AddHandler Tabla.Click, AddressOf aceptar

        empleado = id_empleado

        Tabla.DataSource = data_prestamos.get_prestamos_por_empleado(empleado)


        If Not GridView1.Columns.Count = 0 Then

            GridView1.BestFitColumns()

            GridView1.Columns(0).Visible = False
            GridView1.Columns(13).Visible = False
            GridView1.Columns(14).Visible = False
            GridView1.Columns(15).Visible = False
            GridView1.Columns(16).Visible = False

        End If

    End Sub

    Private Sub aceptar()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        With GridView1

            Me.id_sucursal = .GetRowCellValue(.GetSelectedRows(0), "Id_Sucursal")
            Me.nombre = .GetRowCellValue(.GetSelectedRows(0), "Nombre")
            Me.tipo = .GetRowCellValue(.GetSelectedRows(0), "Id_Tipo")
            Me.fecha = .GetRowCellValue(.GetSelectedRows(0), "Fecha")
            Me.codigo_prestamo = .GetRowCellValue(.GetSelectedRows(0), "Id_Prestamo")
            Me.tasa = .GetRowCellValue(.GetSelectedRows(0), "Tasa")
            Me.modalidad = .GetRowCellValue(.GetSelectedRows(0), "id_Modalidad")
            Me.cuotas = .GetRowCellValue(.GetSelectedRows(0), "Plazo")
            Me.capital = .GetRowCellValue(.GetSelectedRows(0), "Capital")
            Me.estado = .GetRowCellValue(.GetSelectedRows(0), "estado")
            Me.monto_cancelado = .GetRowCellValue(.GetSelectedRows(0), "Cancelado")
            If Not IsDBNull(.GetRowCellValue(.GetSelectedRows(0), "Descripcion")) Then Me.descripcion = .GetRowCellValue(.GetSelectedRows(0), "Descripcion")

        End With

        Me.Close()

    End Sub

    Private Sub cancelar()

        Me.nombre = String.Empty
        Me.tipo = 0
        Me.fecha = Date.Now
        Me.codigo_prestamo = String.Empty
        Me.tasa = 0
        Me.cuotas = 0
        Me.capital = 0
        Me.estado = 0
        Me.id_sucursal = 0
        Me.monto_cancelado = 0

        Me.Close()

    End Sub
 
End Class