﻿Imports System.Data.SqlTypes
Imports System.Data.SqlClient

Public Class frmAlimentador_planilla

    Private cmd As New SqlCommand
    Private con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Private sqlstring As String
    Private adapter As SqlDataAdapter

    Private emp_id As Integer
    Private emp_nombre As String

    Public WriteOnly Property get_codigo As Integer
        Set(value As Integer)
            emp_id = value
        End Set
    End Property
    Public WriteOnly Property get_Nombre As String
        Set(value As String)
            emp_nombre = value
        End Set
    End Property

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AddHandler Me.Load, AddressOf set_inicializacion

    End Sub

    Private Sub set_inicializacion()

        Me.CenterToParent()

        txtEmpCod.Text = emp_id

        txtEmpNombre.Text = emp_nombre

        Fecha.EditValue = Date.Now.Date

        CargaI()

    End Sub
    Private Sub CargaI()

        Dim dt As DataTable = New DataTable()

        Dim SqlString = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(id_documento as int))+1,1))),8)as 'Codigo'  from Tbl_datos_de_planilla where id_sucursal=@sucursal"

        Try
            con.Open()
            cmd = New SqlCommand(SqlString, con)

            cmd.Parameters.AddWithValue("@sucursal", My.Settings.Sucursal)
            cmd.ExecuteNonQuery()


            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)

            For Each row0 In dt.Rows

                txtNoDocumento.Text = row0(0)

            Next

            con.Close()
            dt.Rows.Clear()

        Catch ex As Exception

            MsgBox(String.Format("Error al mostrar datos.{0}Contactar administrador de sistema{0}Error: {1}", vbNewLine, ex.Message))
            con.Close()

        End Try

    End Sub

End Class