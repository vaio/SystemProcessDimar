﻿Public Class Cliente


    Private codigo As String
    Private cedula As String
    Private nombres As String
    Private apellidos As String
    Private Domicilio As String
    Private Telefono_1 As String
    Private Telefono_2 As String
    Private Correo As String
    Private Id_tipo As Integer

    Public Sub New()

    End Sub

    Public Sub New(codigo As String, cedula As String, nombres As String, apellidos As String, Domicilio As String, Telefono_1 As String, Telefono_2 As String, Correo As String,Id_tipo As Integer)

        Me.codigo = codigo
        Me.cedula = cedula
        Me.nombres = nombres
        Me.apellidos = apellidos
        Me.Domicilio = Domicilio
        Me.Telefono_1 = Telefono_1
        Me.Telefono_2 = Telefono_2
        Me.Correo = Correo
        Me.Id_tipo = Id_tipo

    End Sub

    Public Property get_codigo As String
        Get
            Return Me.codigo
        End Get
        Set(value As String)
            Me.codigo = value
        End Set
    End Property
    Public Property get_cedula As String
        Get
            Return Me.cedula
        End Get
        Set(value As String)
            Me.cedula = value
        End Set
    End Property
    Public Property get_nombres As String
        Get
            Return Me.nombres
        End Get
        Set(value As String)
            Me.nombres = value
        End Set
    End Property
    Public Property get_apellidos As String
        Get
            Return Me.apellidos
        End Get
        Set(value As String)
            Me.apellidos = value
        End Set
    End Property
    Public Property get_Domicilio As String
        Get
            Return Me.Domicilio
        End Get
        Set(value As String)
            Me.Domicilio = value
        End Set
    End Property
    Public Property get_Telefono_1 As String
        Get
            Return Me.Telefono_1
        End Get
        Set(value As String)
            Me.Telefono_1 = value
        End Set
    End Property
    Public Property get_Telefono_2 As String
        Get
            Return Me.Telefono_2
        End Get
        Set(value As String)
            Me.Telefono_2 = value
        End Set
    End Property
    Public Property get_Correo As String
        Get
            Return Me.Correo
        End Get
        Set(value As String)
            Me.Correo = value
        End Set
    End Property
    Public Property get_Id_tipo As Integer
        Get
            Return Me.Id_tipo
        End Get
        Set(value As Integer)
            Me.Id_tipo = value
        End Set
    End Property
End Class
