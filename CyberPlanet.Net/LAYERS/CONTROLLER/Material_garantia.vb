﻿Imports System.ComponentModel

Public Class Material_garantia

    Inherits frmBusqueda_Prod.Data

    Private cantidad As Double
    Private costo_total As Double
    Private adicional As Boolean
    Private autorizado As Boolean
    Private activo As Boolean

    Public Sub New()

    End Sub

    Public Sub New(codigo As String, materia_prima As String, medida As String, cantidad As Double, costo_unitario As Double, costo_total As Double, Optional adicional As Boolean = False, Optional autorizado As Boolean = False, Optional activo As Boolean = False)

        Me.codigo = codigo
        Me.descripcion = materia_prima
        Me.unidad_medida = medida
        Me.cantidad = cantidad
        Me.costo = costo_unitario
        Me.costo_total = costo_total
        Me.adicional = adicional
        Me.autorizado = autorizado
        Me.activo = activo

    End Sub
    <DisplayName("Cantidad")>
    Public Property get_cantidad() As Double
        Get
            Return Me.cantidad
        End Get
        Set(ByVal value As Double)
            cantidad = value
        End Set
    End Property
    <DisplayName("Costo Total")>
     Public Property get_costo_total() As Double
        Get
            Return Me.costo_total
        End Get
        Set(ByVal value As Double)
            costo_total = value
        End Set
    End Property
    <DisplayName("Adicional")>
    Public Property get_adicional() As Boolean
        Get
            Return Me.adicional
        End Get
        Set(ByVal value As Boolean)
            adicional = value
        End Set
    End Property

    <DisplayName("Autorizado")>
    Public Property get_autorizado() As Boolean
        Get
            Return Me.autorizado
        End Get
        Set(ByVal value As Boolean)
            autorizado = value
        End Set
    End Property
    <DisplayName("Activo")>
    Public Property get_activo() As Boolean
        Get
            Return Me.activo
        End Get
        Set(ByVal value As Boolean)
            activo = value
        End Set
    End Property
End Class
