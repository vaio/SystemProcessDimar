﻿Public Class Proveedor


    Private ID As String
    Private RUC As String
    Private Nombre As String
    Private Plazo As Double
    Private Is_IR As Double
    Private Is_IMI As Double
    Private Is_IVA As Double
    Private Tasa_IR As Double
    Private Tasa_IMI As Double
    Private Tasa_IVA As Double

    Public Sub New()

    End Sub

    Public Sub New(
         ID As String, RUC As String,
         Nombre As String, Plazo As Double,
         Is_IR As Double, Is_IMI As Double,
         Is_IVA As Double, Tasa_IR As Double,
         Tasa_IMI As Double, Tasa_IVA As Double
                  )

        Me.ID = ID
        Me.RUC = RUC
        Me.Nombre = Nombre
        Me.Plazo = Plazo
        Me.Is_IR = Is_IR
        Me.Is_IMI = Is_IMI
        Me.Is_IVA = Is_IVA
        Me.Tasa_IR = Tasa_IR
        Me.Tasa_IMI = Tasa_IMI
        Me.Tasa_IVA = Tasa_IVA

    End Sub

    Public ReadOnly Property get_ID As String
        Get
            Return Me.ID
        End Get
    End Property
    Public ReadOnly Property get_RUC As String
        Get
            Return Me.RUC
        End Get
    End Property
    Public ReadOnly Property get_Nombre As String
        Get
            Return Me.Nombre
        End Get
    End Property
    Public ReadOnly Property get_Plazo As Double
        Get
            Return Me.Plazo
        End Get
    End Property
    Public ReadOnly Property get_Is_IR As Boolean
        Get
            Return Me.Is_IR
        End Get
    End Property
    Public ReadOnly Property get_Is_IMI As Boolean
        Get
            Return Me.Is_IMI
        End Get
    End Property
    Public ReadOnly Property get_Is_IVA As Boolean
        Get
            Return Me.Is_IVA
        End Get
    End Property
    Public ReadOnly Property get_Tasa_IR As Double
        Get
            Return Me.Tasa_IR
        End Get
    End Property
    Public ReadOnly Property get_Tasa_IMI As Double
        Get
            Return Me.Tasa_IMI
        End Get
    End Property
    Public ReadOnly Property get_Tasa_IVA As Double
        Get
            Return Me.Tasa_IVA
        End Get
    End Property

End Class

