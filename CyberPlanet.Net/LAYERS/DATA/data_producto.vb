﻿Imports System.Data.SqlClient

Public NotInheritable Class data_producto

    Private Sub New()

    End Sub


    Public Shared Function get_existencia_bodega(ParamArray Parametros() As SqlParameter) As DataTable

        Return conexion_to_db.GetData("SP_GET_Producto_Por_Bodega_Cod", Parametros)

    End Function

    Public Shared Function get_existencia_costo_por_bodega_por_producto(ParamArray Parametros() As SqlParameter) As DataTable

        Return conexion_to_db.GetData("SP_AME_get_Producto_costo_existencia_por_bodega", Parametros)

    End Function

    Public Shared Function get_existencia_en_bodegas(ByRef cod_producto As String) As DataTable

        Dim sql_string = String.Format("SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '{0}')", cod_producto)

        Return SQL(sql_string, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)

    End Function

End Class

