﻿Public NotInheritable Class data_compra_foranea

    Private Sub New()

    End Sub

    Public Shared Function set_proveedor(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Compra_extranjera_proveedor", Parametros)

    End Function

    Public Shared Function get_the_last_No_document() As String

        Dim tblRegMax As DataTable = SQL("SELECT isnull(MAX(Numero_de_documento)+1,1) AS Maximo FROM compra_extranjera WHERE sucursal = " & My.Settings.Sucursal, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

        Return Microsoft.VisualBasic.Right("00000000" & tblRegMax.Rows(0).Item(0), 8)

    End Function

    Public Shared Function get_proveedores(ParamArray Parametros() As Data.SqlClient.SqlParameter) As DataTable

        Return conexion_to_db.GetData("SP_AME_get_compra_extranjera_proveedores", Parametros)

    End Function

    Public Shared Function set_impuesto(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Integer 'insert, update and delete u can do whatever u want

        Return conexion_to_db.SendData("SP_AME_Compra_extranjera_detalle_impuestos", Parametros)

    End Function

    Public Shared Function get_impuestos(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Dictionary(Of impuesto.key_identificador, impuesto)

        get_impuestos = New Dictionary(Of impuesto.key_identificador, impuesto)

        Dim impuestos = conexion_to_db.GetData("SP_AME_get_compra_foranea_impuestos", Parametros)

        For Each curr_impuesto As DataRow In impuestos.Rows

            Dim key As New impuesto.key_identificador With {.sucursal = curr_impuesto.Item(0), .numero_de_documento = curr_impuesto.Item(1), .tipo_documento = curr_impuesto.Item(3)}


            Dim impuesto As New impuesto(
                                                          curr_impuesto.Item(0),
                                                          curr_impuesto.Item(1),
                                                          curr_impuesto.Item(2),
                                                          curr_impuesto.Item(3),
                                                          curr_impuesto.Item(4),
                                                          curr_impuesto.Item(5),
                                                          curr_impuesto.Item(6),
                                                          curr_impuesto.Item(7),
                                                          curr_impuesto.Item(8),
                                                          curr_impuesto.Item(9),
                                                          curr_impuesto.Item(10),
                                                          curr_impuesto.Item(11),
                                                          curr_impuesto.Item(12),
                                                          curr_impuesto.Item(13),
                                                          curr_impuesto.Item(14),
                                                          curr_impuesto.Item(15),
                                                          curr_impuesto.Item(16),
                                                          curr_impuesto.Item(17))

            get_impuestos.Add(key, impuesto)

        Next

        Return get_impuestos

    End Function

    Public Shared Function get_lista_de_compra(ByRef curr_sucursal As Integer) As DataTable

        Return conexion_to_db.GetVista(String.Format("SELECT * FROM vw_compra_extranjera WHERE sucursal = {0} ORDER BY Poliza, Consecutivo", curr_sucursal.ToString))

    End Function

    Public Shared Function set_compra(curr_compra As compra) As Integer

        set_compra = conexion_to_db.SendData("SP_AME_gestion_master_compra_extranjera",
                                               Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                               Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.VarChar),
                                               Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.VarChar),
                                               Funciones.Param("@numero_de_documento_consecutivo", curr_compra.get_numero_de_documento_consecutivo, SqlDbType.Int),
                                               Funciones.Param("@documento_multiple", curr_compra.get_documento_multiple, SqlDbType.Bit),
                                               Funciones.Param("@tipo_de_compra", curr_compra.get_tipo_de_compra, SqlDbType.Int),
                                               Funciones.Param("@tipo_cordoba_dolar", curr_compra.get_tipo_cordoba_dolar, SqlDbType.Int),
                                               Funciones.Param("@tipo_calculo_de_documento", curr_compra.get_tipo_calculo_de_documento, SqlDbType.Int),
                                               Funciones.Param("@tasa_cambio_de_documento", curr_compra.get_tasa_cambio_de_documento, SqlDbType.Decimal),
                                               Funciones.Param("@fecha_de_documento", curr_compra.get_fecha_de_documento, SqlDbType.DateTime),
                                               Funciones.Param("@fecha_de_recepcion", curr_compra.get_fecha_de_recepcion, SqlDbType.DateTime),
                                               Funciones.Param("@fecha_de_vencimiento", curr_compra.get_fecha_de_vencimiento, SqlDbType.DateTime),
                                               Funciones.Param("@plazo", curr_compra.get_plazo, SqlDbType.Int),
                                               Funciones.Param("@observaciones", curr_compra.get_observaciones, SqlDbType.VarChar),
                                               Funciones.Param("@ssa", curr_compra.get_ssa, SqlDbType.Decimal),
                                               Funciones.Param("@tsi", curr_compra.get_tsi, SqlDbType.Decimal),
                                               Funciones.Param("@spe", curr_compra.get_spe, SqlDbType.Decimal),
                                               Funciones.Param("@isc", curr_compra.get_isc, SqlDbType.Decimal),
                                               Funciones.Param("@dai", curr_compra.get_dai, SqlDbType.Decimal),
                                               Funciones.Param("@aplicado", curr_compra.get_aplicado, SqlDbType.Bit),
                                               Funciones.Param("@accion", 1, SqlDbType.Int))


    End Function
    Public Shared Function eliminar_compra(curr_compra As compra) As Integer

        Return eliminar_compra = conexion_to_db.SendData("SP_AME_gestion_master_compra_extranjera",
                                               Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                               Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.VarChar),
                                               Funciones.Param("@numero_de_documento_consecutivo", curr_compra.get_numero_de_documento_consecutivo, SqlDbType.Int),
                                               Funciones.Param("@aplicado", curr_compra.get_aplicado, SqlDbType.Bit),
                                               Funciones.Param("@accion", 3, SqlDbType.Int))

    End Function
    Public Shared Function get_compra(curr_compra As compra) As compra.BuilderCompra

        get_compra = New compra.BuilderCompra

        Dim data = conexion_to_db.GetData("SP_AME_gestion_master_compra_extranjera",
                                                 Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                 Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.VarChar),
                                                 Funciones.Param("@accion", 4, SqlDbType.Int))


        With get_compra

            .sucursal = CInt(data.Rows(0).Item(0).ToString)
            .numero_de_documento = data.Rows(0).Item(1).ToString
            .numero_de_documento_poliza = data.Rows(0).Item(2).ToString.Trim
            .numero_de_documento_consecutivo = CInt(data.Rows(0).Item(3).ToString)
            .documento_multiple = CBool(data.Rows(0).Item(4).ToString)
            .tipo_de_compra = CInt(data.Rows(0).Item(5).ToString)
            .tipo_cordoba_dolar = CInt(data.Rows(0).Item(6).ToString)
            .tipo_calculo_de_documento = CInt(data.Rows(0).Item(7).ToString)
            .tasa_cambio_de_documento = CDec(data.Rows(0).Item(8).ToString)
            .fecha_de_documento = CDate(data.Rows(0).Item(9).ToString)
            .fecha_de_recepcion = CDate(data.Rows(0).Item(10).ToString)
            .fecha_de_vencimiento = CDate(data.Rows(0).Item(11).ToString)
            .plazo = CInt(data.Rows(0).Item(12).ToString)
            .observaciones = data.Rows(0).Item(13).ToString.Trim
            .ssa = CDec(data.Rows(0).Item(14).ToString)
            .tsi = CDec(data.Rows(0).Item(15).ToString)
            .spe = CDec(data.Rows(0).Item(16).ToString)
            .isc = CDec(data.Rows(0).Item(17).ToString)
            .dai = CDec(data.Rows(0).Item(18).ToString)
            .aplicado = CBool(data.Rows(0).Item(19).ToString)

        End With

    End Function

    Public Shared Function set_producto(compra_detalle_producto As compra_foranea_producto, accion As Integer) As Integer

        With compra_detalle_producto

            set_producto = conexion_to_db.SendData("SP_AME_gestion_detalle_compra_extranjera",
                                                  Funciones.Param("@sucursal", .get_sucursal, SqlDbType.Int),
                                                  Funciones.Param("@numero_de_documento", .get_numero_de_documento, SqlDbType.NVarChar),
                                                  Funciones.Param("@almacen_codigo", .get_almacen_codigo, SqlDbType.Int),
                                                  Funciones.Param("@producto_codigo", .get_producto_codigo, SqlDbType.NVarChar),
                                                  Funciones.Param("@producto_cantidad", .get_producto_cantidad, SqlDbType.Decimal),
                                                  Funciones.Param("@producto_precio", .get_producto_precio, SqlDbType.Decimal),
                                                  Funciones.Param("@producto_peso", .get_producto_peso, SqlDbType.Decimal),
                                                  Funciones.Param("@produto_dai", .get_producto_dai, SqlDbType.Decimal),
                                                  Funciones.Param("@fecha_de_expiracion", .get_fecha_de_expiracion, SqlDbType.Date),
                                                  Funciones.Param("@accion", accion, SqlDbType.Int))

        End With

    End Function
    Public Shared Function get_producto(compra_detalle_producto As compra_foranea_producto) As compra_foranea_producto

        With compra_detalle_producto

            Dim DATOS = conexion_to_db.GetData("SP_AME_gestion_detalle_compra_extranjera",
                                                  Funciones.Param("@sucursal", .get_sucursal, SqlDbType.Int),
                                                  Funciones.Param("@numero_de_documento", .get_numero_de_documento, SqlDbType.NVarChar),
                                                  Funciones.Param("@almacen_codigo", .get_almacen_codigo, SqlDbType.Int),
                                                  Funciones.Param("@producto_codigo", .get_producto_codigo, SqlDbType.NVarChar),
                                                  Funciones.Param("@accion", 4, SqlDbType.Int))

            With DATOS.Rows(0)

                get_producto = New compra_foranea_producto(CInt(.Item(0)), .Item(1).ToString.Trim, CInt(.Item(2)), .Item(3).ToString.Trim, CDec(.Item(4)), CDec(.Item(5)), CDec(.Item(6)), CDec(.Item(7)), CDate(.Item(8)))

            End With

        End With

    End Function

    Public Shared Function get_documento_multiple(curr_compra As compra) As Boolean

        Return CBool(conexion_to_db.GetData("SP_AME_get_compra_foranea_existe_multiple_poliza",
                                                  Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                  Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.VarChar),
                                                  Funciones.Param("@documento_multiple", curr_compra.get_documento_multiple, SqlDbType.Bit),
                                                  Funciones.Param("@aplicado", curr_compra.get_aplicado, SqlDbType.Bit),
                                                  Funciones.Param("@accion", 1, SqlDbType.Int)).Rows(0).Item(0))


    End Function

    Public Shared Function get_documento_multiple_consecutivo(curr_compra As compra) As Integer

        Return CInt(conexion_to_db.GetData("SP_AME_get_compra_foranea_existe_multiple_poliza",
                                                Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.VarChar),
                                                Funciones.Param("@documento_multiple", curr_compra.get_documento_multiple, SqlDbType.Bit),
                                                Funciones.Param("@aplicado", curr_compra.get_aplicado, SqlDbType.Bit),
                                                Funciones.Param("@accion", 2, SqlDbType.Int)).Rows(0).Item(0))

    End Function

    Public Shared Function get_documento_multiple_tipo_de_calculo(curr_compra As compra) As Integer

        Return CInt(conexion_to_db.GetData("SP_AME_get_compra_foranea_existe_multiple_poliza",
                                                Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.VarChar),
                                                Funciones.Param("@accion", 3, SqlDbType.Int)).Rows(0).Item(0))

    End Function

    Public Shared Function get_detalle(curr_compra As compra) As DataTable

        Return conexion_to_db.GetData("SP_AME_get_compra_foranea_detalle",
                                      Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                      Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.NVarChar),
                                      Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.NChar))

    End Function

    Public Shared Function get_total_poliza_actual_aplicada(curr_compra As compra) As Decimal

        Return CDec(conexion_to_db.GetData("SP_AME_get_compra_foranea_tota_porcentaje",
                                                              Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                              Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.NChar),
                                                              Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.NChar),
                                                              Funciones.Param("@aplicada", 1, SqlDbType.Int)).Rows(0).Item(0))

    End Function

    Public Shared Function get_total_poliza_actual_sin_aplicada(curr_compra As compra) As Decimal

        Return CDec(conexion_to_db.GetData("SP_AME_get_compra_foranea_tota_porcentaje",
                                                              Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                              Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.NChar),
                                                              Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.NChar),
                                                              Funciones.Param("@aplicada", 0, SqlDbType.Int)).Rows(0).Item(0))

    End Function
    Public Shared Function get_total_poliza_aplicada(curr_compra As compra) As Decimal

        Return CDec(conexion_to_db.GetData("SP_AME_get_compra_foranea_tota_porcentaje",
                                                              Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                              Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.NChar),
                                                              Funciones.Param("@aplicada", 1, SqlDbType.Int)).Rows(0).Item(0))

    End Function
    Public Shared Function get_total_poliza_sin_aplicada(curr_compra As compra) As Decimal

        Return CDec(conexion_to_db.GetData("SP_AME_get_compra_foranea_tota_porcentaje",
                                                              Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                              Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.NChar),
                                                              Funciones.Param("@aplicada", 0, SqlDbType.Int)).Rows(0).Item(0))

    End Function

    Public Shared Function get_total_poliza_total(curr_compra As compra) As Decimal ' LISTO

        Return CDec(conexion_to_db.GetData("SP_AME_get_compra_foranea_tota_porcentaje",
                                                              Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                              Funciones.Param("@numero_de_documento_poliza", curr_compra.get_numero_de_documento_poliza, SqlDbType.NChar)).Rows(0).Item(0))

    End Function

    Public Shared Function get_detalle_total_dai(curr_compra As compra) As Decimal

        Return CDec(conexion_to_db.GetData("SP_AME_get_compra_foranea_detalle_total_dai",
                                                              Funciones.Param("@sucursal", curr_compra.get_sucursal, SqlDbType.Int),
                                                              Funciones.Param("@numero_de_documento", curr_compra.get_numero_de_documento, SqlDbType.NChar)).Rows(0).Item(0))


    End Function
    Public Shared Function get_reporte_master_movimiento(sucursal_ As Integer, numero_documento_ As String)

        Return conexion_to_db.GetData("SP_AME_get_compra_foranea_master_reporte",
                                                              Funciones.Param("@sucursal", sucursal_, SqlDbType.Int),
                                                              Funciones.Param("@numero_documento_compra", numero_documento_, SqlDbType.NChar))

    End Function

End Class
