﻿Public NotInheritable Class data_tasa_de_cambio

    Private Sub New()

    End Sub

    Public Shared Function get_tasa_de_cambio_actual(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Double

        Try

            Return Double.Parse(conexion_to_db.GetData("SP_AME_get_tasa_cambio_actual", Parametros).Rows(0).Item(0))

        Catch ex As Exception

            MsgBox(String.Format("Algo anda mal con: {0}", ex.Message))

        End Try

    End Function

    Public Shared Function get_historial_tasa_de_cambio() As DataTable

        Return SQL("SELECT Fecha, Tasa_Oficial FROM Tbl_TasasCambio ORDER BY Fecha DESC", "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

    End Function

End Class
