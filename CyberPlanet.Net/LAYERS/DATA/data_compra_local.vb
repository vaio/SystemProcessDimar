﻿Public NotInheritable Class data_compra_local

    Private Sub New()

    End Sub

    Public Shared Function get_the_last_No_document() As String

        Dim tblRegMax As DataTable = SQL("SELECT isnull(MAX(Numero_de_Compra)+1,1) AS Maximo FROM Compras WHERE CodigoSucursal = " & My.Settings.Sucursal, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

        Return Microsoft.VisualBasic.Right("00000000" & tblRegMax.Rows(0).Item(0), 8)

    End Function

    Public Shared Function set_master(ParamArray Parametro() As Data.SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_Master_CompraLocal", Parametro)

    End Function

    Public Shared Function set_detalle(ParamArray Parametro() As Data.SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_Detalle_CompraLocal", Parametro)

    End Function

    Public Shared Function get_master_reporte(ParamArray Parametro() As Data.SqlClient.SqlParameter) As DataTable

        Return conexion_to_db.GetData("SP_AME_get_compra_loca_master_reporte", Parametro)

    End Function

    Public Shared Function get_movimiento_detalle_reporte(ParamArray Parametro() As Data.SqlClient.SqlParameter) As DataTable

        Return conexion_to_db.GetData("SP_AME_get_reporte_detalle_movimiento", Parametro)

    End Function

End Class
