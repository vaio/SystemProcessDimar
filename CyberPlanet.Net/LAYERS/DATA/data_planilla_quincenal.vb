﻿Public NotInheritable Class data_planilla_quincenal

    Private Sub New()

    End Sub

    Public Shared Function get_planilla_quicenal() As DataTable

        Return conexion_to_db.GetData("SP_AME_generar_planilla_quincenal")

    End Function

    Public Shared Function set_alimentador(ParamArray Parametros() As SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Gestion_Alimentador_Planilla", Parametros)

    End Function

End Class
