﻿Public NotInheritable Class data_prestamos

    Private Sub New()

    End Sub

    Public Shared Function set_prestamo(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Prestamo_Master", Parametros)

    End Function
    Public Shared Function delete_prestamo(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Prestamo_Master", Parametros)

    End Function

    Public Shared Function get_prestamos_por_empleado(ByRef id_empleado As Integer) As DataTable

        Return conexion_to_db.GetVista(String.Format("select * from vw_Prestamo where Id_Empleado = {0} and estado=1", id_empleado))

    End Function

End Class
