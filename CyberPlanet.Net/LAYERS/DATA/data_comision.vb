﻿Public NotInheritable Class data_comision

    Private Sub New()

    End Sub

    Public Shared Function set_comision(ParamArray Parametros() As SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Comision", Parametros)

    End Function

    Public Shared Function set_delete(ParamArray Parametros() As SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Comision", Parametros)

    End Function

    Public Shared Function get_comisiones_por_usuario(ByRef id_empleado As Integer) As DataTable

        Return conexion_to_db.GetVista(String.Format("Select * from vw_Comision where id_empleado = {0}", id_empleado))

    End Function

End Class
