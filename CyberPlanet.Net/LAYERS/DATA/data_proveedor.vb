﻿Public NotInheritable Class data_proveedor

   
    Private Sub New()

    End Sub

    Public Shared Function get_datos_basicos_proveedores() As Dictionary(Of String, Proveedor)

        get_datos_basicos_proveedores = New Dictionary(Of String, Proveedor)

        Dim Proveedores = conexion_to_db.GetVista("SELECT * FROM vw_Proveedores_v2")

        For Each dato As DataRow In Proveedores.Rows

            Dim P As New Proveedor(dato.Item(0), dato.Item(1), dato.Item(2), dato.Item(3), dato.Item(4), dato.Item(5), dato.Item(6), dato.Item(7), dato.Item(8), dato.Item(9))

            get_datos_basicos_proveedores.Add(P.get_ID, P)

        Next

    End Function



End Class
