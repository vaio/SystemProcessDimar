﻿Imports System.Data.SqlClient

Public NotInheritable Class data_fill_up_comb

    Private Sub New()
        'No crear objeto de esta clase.
    End Sub

    Public Shared Function get_tipos_de_ordenes() As DataTable

        Return conexion_to_db.GetVista("Select * from vw_TipoServicio")

    End Function

    Public Shared Function get_tipos_de_prioridades() As DataTable

        Return conexion_to_db.GetVista("select * from vw_Prioridad")

    End Function

    Public Shared Function get_tipos_de_clientes() As DataTable

        Return conexion_to_db.GetVista("select * from vw_TipoCliente")

    End Function

    Public Shared Function get_tipos_condiciones_del_articulo() As DataTable

        Return conexion_to_db.GetVista("select * from vw_TipoCondicionArticulo")

    End Function

    Public Shared Function get_bodegas_M_PT() As DataTable

        Return SQL("SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes where Almacenes.Codigo_Bodega in (2,4)  ", "tblBodega", My.Settings.SolIndustrialCNX).Tables(0)

    End Function

End Class
