﻿Imports System.Data.SqlClient

Public NotInheritable Class data_orden_de_servicio

    Private Sub New()

    End Sub

    Public Shared Function get_codigo() As String

        Return Microsoft.VisualBasic.Right("00000000" & CInt(conexion_to_db.GetData("SP_AME_CODIGO_Orden_de_servicio").Rows(0).Item(0)), 8)

    End Function

    Public Shared Function get_tipos_de_estados() As DataTable

        Return conexion_to_db.GetVista("SELECT * FROM vw_tipo_de_estado")

    End Function

    Public Shared Function insert_master(ParamArray Parametros() As SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_Orden_de_servicio_master", Parametros)

    End Function

End Class
