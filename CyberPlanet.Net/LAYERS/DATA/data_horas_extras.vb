﻿Public NotInheritable Class data_horas_extras

    Private Sub New()

    End Sub

    Public Shared Function set_horas_extras(ParamArray Parametros() As Data.SqlClient.SqlParameter) As Integer

        Return conexion_to_db.SendData("SP_AME_HorasExtras", Parametros)

    End Function

    Public Shared Function get_horas_extras_por_empleado(ByRef id_empleado As Integer) As DataTable

        Return conexion_to_db.GetVista(String.Format("select * from vw_HorasExtras where id_empleado = {0} and estado_documento=1", id_empleado))

    End Function

End Class
