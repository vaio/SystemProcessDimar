﻿Public Interface IMenu

    Function nuevo() As Integer

    Function guardar() As Integer

    Function eliminar() As Integer

    Function cancelar() As Integer

    Function modificar() As Integer

    Function historial() As Integer

    Function salir() As Integer

    Function imprimir() As Integer

End Interface
