﻿Public Class Validar

    Public Property CantDecimal As Integer = 4

    Protected Control As List(Of Control)

    Public Function agregar_control(ByRef my_control As List(Of Control)) As procesar

        Control = my_control

        Return New procesar(Control, CantDecimal)

    End Function

    Public Class procesar

        Inherits Validar

        Public Sub New(ByRef my_control As List(Of Control), ByRef Dec As Integer)

            Control = my_control

            CantDecimal = Dec

        End Sub

        Public Function ActivarEventosNumericos()

            For Each my_control As Control In Control

                AddHandler my_control.LostFocus, Sub(sender, e) AddDecimal(my_control)
                AddHandler my_control.GotFocus, Sub(sender, e) FormatiarOrigen(my_control)
                AddHandler my_control.KeyPress, Sub(sender, e) BloquoLetrasPuntoFlotante(my_control, e, True)

                Formatiar(my_control)

            Next

            Return True

        End Function

        Public Sub SoloEnteros()

            For Each my_control As Control In Control

                AddHandler my_control.KeyPress, Sub(sender, e) BloquoLetrasPuntoFlotante(my_control, e, False)

            Next

        End Sub
        Public Sub SoloEnterosConDecimales()

            For Each my_control As Control In Control

                AddHandler my_control.LostFocus, Sub(sender, e) AddDecimal(my_control)
                AddHandler my_control.GotFocus, Sub(sender, e) FormatiarOrigen(my_control)
                AddHandler my_control.KeyPress, Sub(sender, e) BloquoLetrasPuntoFlotante(my_control, e, True)

            Next

        End Sub
        Public Sub PermitirLetrasNumeros()

            For Each my_control As Control In Control

                AddHandler my_control.KeyPress, Sub(sender, e) PermitirSoloNumLetras(my_control, e)

            Next

        End Sub
        Private Sub AddDecimal(ByRef my_control As Control)

            If my_control.Text = Nothing Then my_control.Text = FormatNumber(0, CantDecimal) : Exit Sub

            my_control.Text = FormatNumber(ChangeToDouble(my_control.Text), CantDecimal)

        End Sub

        Private Sub FormatiarOrigen(ByRef my_control As Control)

            If my_control.Text = Nothing Then Exit Sub

            my_control.Text = ChangeToDouble(my_control.Text)

            If ChangeToDouble(my_control.Text) = 0 Then my_control.Text = Nothing

        End Sub


        Private Sub BloqueoTotal(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

            e.Handled = True

        End Sub

        Private Function PuntoFlotante(ByRef txtControl As Control, ByVal caracter As Char, ByVal decimales As Boolean) As Boolean

            If (Char.IsNumber(caracter, 0) = True) Or caracter = Convert.ToChar(8) Or caracter = "." Then
                If caracter = "." Then
                    If decimales = True Then
                        If txtControl.Text.IndexOf(".") <> -1 Then Return True
                    Else : Return True
                    End If
                End If
                Return False
            Else
                Return True
            End If

        End Function

        Private Sub BloquoLetrasPuntoFlotante(ByRef my_control As Control, e As System.Windows.Forms.KeyPressEventArgs, ByRef Flotante As Boolean)

            e.Handled = PuntoFlotante(my_control, e.KeyChar, Flotante)

        End Sub

        Private Sub Formatiar(ByRef my_control As Control)

            If my_control.Text = Nothing Then Exit Sub Else my_control.Text = FormatNumber(ChangeToDouble(my_control.Text), CantDecimal)

        End Sub

        Private Sub PermitirSoloNumLetras(sender As Object, e As KeyPressEventArgs)

            Const allows As String = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789-/"

            If allows.Contains(e.KeyChar) Or e.KeyChar = Chr(Keys.Back) Then e.Handled = False Else e.Handled = True

        End Sub

    End Class

End Class

