﻿Public NotInheritable Class Funciones

    Private Sub New()

    End Sub

    Public Shared Function Param(Name As String, Value As String, Type As SqlDbType) As SqlClient.SqlParameter

        Return New Param(Name, Value, Type).myparam()

    End Function

    Public Shared Sub Limpiar(ByVal Controls As System.Windows.Forms.Control.ControlCollection)

        For Each c As Control In Controls

            If Not c.Controls Is Nothing AndAlso c.Controls.Count > 0 Then
                Limpiar(c.Controls)
            ElseIf c.GetType().Equals(GetType(TextBox)) Then
                CType(c, TextBox).Text = ""
            ElseIf c.GetType().Equals(GetType(RichTextBox)) Then
                CType(c, RichTextBox).Text = ""
            ElseIf c.GetType().Equals(GetType(DateTimePicker)) Then
                CType(c, DateTimePicker).Value = Date.Now
            ElseIf c.GetType().Equals(GetType(MaskedTextBox)) Then
                CType(c, MaskedTextBox).Text = ""
            ElseIf c.GetType().Equals(GetType(ComboBox)) Then
                CType(c, ComboBox).SelectedItem = Nothing
            ElseIf c.GetType.Equals(GetType(DataGridView)) Then
                CType(c, DataGridView).DataSource = Nothing
                CType(c, DataGridView).Rows.Clear()
            End If

        Next

    End Sub

End Class
