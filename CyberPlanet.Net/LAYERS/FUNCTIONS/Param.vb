﻿Imports System.Data.SqlClient

Public Class Param

    Private Parametro As SqlParameter

    Public Sub New(Name As String, Value As String, Tipo As SqlDbType)

        Parametro = New SqlParameter(Name, Tipo) With {.Direction = ParameterDirection.Input, .Value = conver_parame(Value, Tipo)}
        
    End Sub

    Public Function myparam() As SqlParameter

        Return Parametro

    End Function

    Private Function conver_parame(Value As String, Tipo As SqlDbType)

        Select Case Tipo

            Case SqlDbType.Text Or SqlDbType.NVarChar

                Return Value

            Case SqlDbType.Int

                Return Int32.Parse(CInt(Value))

            Case SqlDbType.Bit

                Return CBool(Value)

            Case SqlDbType.Date

                Return Date.Parse(Value)

            Case SqlDbType.Money

                Return Double.Parse(Value)

            Case Else

                Return Value

        End Select


    End Function

End Class
