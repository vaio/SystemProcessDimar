﻿Imports System.ComponentModel

Public Class frmBusqueda_Prod

    Private nombre As String
    Private bodega As Integer
    Private tipo As SqlDbType

    Private my_data As New Data

    Public Sub New(nombre As String, bodega As Integer, tipo As SqlDbType)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.


        Me.nombre = nombre
        Me.bodega = bodega
        Me.tipo = tipo

        AddHandler Tabla.MouseDoubleClick, AddressOf aceptar
        AddHandler btnAceptar.Click, AddressOf aceptar
        AddHandler btnCancelar.Click, AddressOf cancelar

    End Sub

    Public Sub mostrar_por_bodega(bodega As Integer)

        Me.bodega = bodega

        set_data()

        Me.ShowDialog()


    End Sub
    Public Sub mostrar(Optional Column As String = Nothing)

        set_data()

        ocultar_columna(Column)

        Me.ShowDialog()

    End Sub
    Private Sub ocultar_columna(ByRef Column As String)

        If Not Column Is Nothing Then GridView1.Columns(CInt(Column)).Visible = False

    End Sub

    Private Sub set_data()

        Tabla.DataSource = conexion_to_db.GetData("SP_GET_Producto_Por_Bodega_Cod", Funciones.Param(nombre, bodega, tipo))
        GridView1.BestFitColumns()

    End Sub

    Private Sub aceptar()

        get_data()

        Me.Close()

    End Sub
    Private Sub cancelar()

        my_data = New Data

        Me.Close()

    End Sub
    Private Sub get_data()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        With GridView1


            my_data = New Data(.GetRowCellValue(.GetSelectedRows(0), "Codigo"), .GetRowCellValue(.GetSelectedRows(0), "Descripcion"), .GetRowCellValue(.GetSelectedRows(0), "C. Promedio"), .GetRowCellValue(.GetSelectedRows(0), "Existencias"), .GetRowCellValue(.GetSelectedRows(0), "Unidad Medida"))

        End With


    End Sub
    Public Function get_info() As Data

        Return my_data

    End Function

    '-----------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Class Data

        Protected codigo As String
        Protected descripcion As String
        Protected costo As Double
        Protected existencia As Double
        Protected unidad_medida As String

        Public Sub New()

        End Sub

        Public Sub New(codigo As String, descripcion As String, costo As Double, existencia As Double, unidad_medida As String)

            Me.codigo = codigo
            Me.descripcion = descripcion
            Me.costo = costo
            Me.existencia = existencia
            Me.unidad_medida = unidad_medida

        End Sub
        <DisplayName("Codigo")>
        Public Property get_codigo As String
            Get
                Return Me.codigo
            End Get
            Set(value As String)
                Me.codigo = value
            End Set
        End Property
        <DisplayName("Materia prima")>
        Public Property get_descripcion As String
            Get
                Return Me.descripcion
            End Get
            Set(value As String)
                Me.descripcion = value
            End Set
        End Property
        <DisplayName("Costo Unitario")>
        Public Property get_costo As Double
            Get
                Return Me.costo
            End Get
            Set(value As Double)
                Me.costo = value
            End Set
        End Property
        <DisplayName("Existencia")>
        Public Property get_existencia As Double
            Get
                Return Me.existencia
            End Get
            Set(value As Double)
                Me.existencia = value
            End Set
        End Property
        <DisplayName("Medida")>
        Public Property get_unidad_medida As String
            Get
                Return Me.unidad_medida
            End Get
            Set(value As String)
                Me.unidad_medida = value
            End Set
        End Property
    End Class
    '-----------------------------------------------------------------------------------------------------------------------------------------------------------
End Class