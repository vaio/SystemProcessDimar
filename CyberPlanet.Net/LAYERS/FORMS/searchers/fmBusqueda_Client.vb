﻿Public Class fmBusqueda_Client

    Private Cliente As New Cliente

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        set_data()

        AddHandler btnAceptar.Click, AddressOf Aceptar
        AddHandler btnCancelar.Click, AddressOf Cancelar

    End Sub

    Private Sub set_data()

        Tabla.DataSource = data_cliente.get_Clientes()
        GridView1.BestFitColumns()

    End Sub

    Private Sub Aceptar()

        If GridView1.RowCount = 0 Then Exit Sub
        If GridView1.SelectedRowsCount = 0 Then Exit Sub

        With GridView1


            Cliente = New Cliente(.GetRowCellValue(.GetSelectedRows(0), "Codigo"), .GetRowCellValue(.GetSelectedRows(0), "Cedula"), .GetRowCellValue(.GetSelectedRows(0), "Nombre"), .GetRowCellValue(.GetSelectedRows(0), "Apellido"), .GetRowCellValue(.GetSelectedRows(0), "Domicilio"), .GetRowCellValue(.GetSelectedRows(0), "Tel Personal 1"), .GetRowCellValue(.GetSelectedRows(0), "Tel Personal 2"), .GetRowCellValue(.GetSelectedRows(0), "Correo"), .GetRowCellValue(.GetSelectedRows(0), "Tipo"))

        End With

        Me.Close()

    End Sub

    Private Sub Cancelar()

        Cliente = New Cliente

        Me.Close()

    End Sub

    Public Function Curr_Cliente() As Cliente

        Return Cliente

    End Function
End Class