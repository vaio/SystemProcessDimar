﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProductoAdicional
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProductoAdicional))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.btn_aceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_materiales = New DevExpress.XtraEditors.SimpleButton()
        Me.txt_material_codigo = New System.Windows.Forms.TextBox()
        Me.txt_material_costo_unitario = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_material_costo_total = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_material_cantidad = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_material_existencia = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_material_descripcion = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btn_cancelar)
        Me.Panel2.Controls.Add(Me.btn_aceptar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 159)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(454, 49)
        Me.Panel2.TabIndex = 2
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancelar.Image = CType(resources.GetObject("btn_cancelar.Image"), System.Drawing.Image)
        Me.btn_cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_cancelar.Location = New System.Drawing.Point(356, 8)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(78, 35)
        Me.btn_cancelar.TabIndex = 1
        Me.btn_cancelar.Text = "&Cancelar"
        Me.btn_cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'btn_aceptar
        '
        Me.btn_aceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btn_aceptar.Image = CType(resources.GetObject("btn_aceptar.Image"), System.Drawing.Image)
        Me.btn_aceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_aceptar.Location = New System.Drawing.Point(272, 8)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(78, 35)
        Me.btn_aceptar.TabIndex = 0
        Me.btn_aceptar.Text = "&Aceptar"
        Me.btn_aceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_aceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_materiales)
        Me.Panel1.Controls.Add(Me.txt_material_codigo)
        Me.Panel1.Controls.Add(Me.txt_material_costo_unitario)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txt_material_costo_total)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txt_material_cantidad)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_material_existencia)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txt_material_descripcion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(454, 208)
        Me.Panel1.TabIndex = 1
        '
        'btn_materiales
        '
        Me.btn_materiales.Location = New System.Drawing.Point(204, 15)
        Me.btn_materiales.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_materiales.Name = "btn_materiales"
        Me.btn_materiales.Size = New System.Drawing.Size(76, 19)
        Me.btn_materiales.TabIndex = 22
        Me.btn_materiales.Text = "Materiales"
        '
        'txt_material_codigo
        '
        Me.txt_material_codigo.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_material_codigo.Enabled = False
        Me.txt_material_codigo.Location = New System.Drawing.Point(100, 16)
        Me.txt_material_codigo.Name = "txt_material_codigo"
        Me.txt_material_codigo.Size = New System.Drawing.Size(100, 20)
        Me.txt_material_codigo.TabIndex = 21
        '
        'txt_material_costo_unitario
        '
        Me.txt_material_costo_unitario.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_material_costo_unitario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txt_material_costo_unitario.Location = New System.Drawing.Point(100, 105)
        Me.txt_material_costo_unitario.Name = "txt_material_costo_unitario"
        Me.txt_material_costo_unitario.Size = New System.Drawing.Size(98, 20)
        Me.txt_material_costo_unitario.TabIndex = 20
        Me.txt_material_costo_unitario.Text = "0.0000"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(267, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Costo Total:"
        '
        'txt_material_costo_total
        '
        Me.txt_material_costo_total.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_material_costo_total.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txt_material_costo_total.Location = New System.Drawing.Point(340, 105)
        Me.txt_material_costo_total.Name = "txt_material_costo_total"
        Me.txt_material_costo_total.Size = New System.Drawing.Size(98, 20)
        Me.txt_material_costo_total.TabIndex = 18
        Me.txt_material_costo_total.Text = "0.0000"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Costo Unitario:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 76)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Cantidad:"
        '
        'txt_material_cantidad
        '
        Me.txt_material_cantidad.Location = New System.Drawing.Point(100, 74)
        Me.txt_material_cantidad.Name = "txt_material_cantidad"
        Me.txt_material_cantidad.Size = New System.Drawing.Size(98, 20)
        Me.txt_material_cantidad.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(267, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Existencia:"
        '
        'txt_material_existencia
        '
        Me.txt_material_existencia.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_material_existencia.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txt_material_existencia.Location = New System.Drawing.Point(340, 75)
        Me.txt_material_existencia.Name = "txt_material_existencia"
        Me.txt_material_existencia.Size = New System.Drawing.Size(98, 20)
        Me.txt_material_existencia.TabIndex = 12
        Me.txt_material_existencia.Text = "0.0000"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Descripción:"
        '
        'txt_material_descripcion
        '
        Me.txt_material_descripcion.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_material_descripcion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txt_material_descripcion.Location = New System.Drawing.Point(100, 44)
        Me.txt_material_descripcion.Name = "txt_material_descripcion"
        Me.txt_material_descripcion.Size = New System.Drawing.Size(338, 20)
        Me.txt_material_descripcion.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Codigo Material:"
        '
        'frmProductoAdicional
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 208)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmProductoAdicional"
        Me.Text = "frmProductoAdicional"
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_material_codigo As System.Windows.Forms.TextBox
    Friend WithEvents txt_material_costo_unitario As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_material_costo_total As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_material_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_material_existencia As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_material_descripcion As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_materiales As DevExpress.XtraEditors.SimpleButton
End Class
