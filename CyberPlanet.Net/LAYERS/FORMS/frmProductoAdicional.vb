﻿Public Class frmProductoAdicional

    Private Material As New Material_garantia

    Private validar_campos As New Validar

    Private BuscarProducto As New frmBusqueda_Prod("@cod_bodega", 2, SqlDbType.Int)
    ' Public Material As New produc

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AddHandler btn_materiales.Click, AddressOf Materiales
        AddHandler txt_material_cantidad.TextChanged, AddressOf calcular_costo_total
        AddHandler btn_aceptar.Click, AddressOf aceptar
        AddHandler btn_cancelar.Click, AddressOf cancelar

        validar_campos.agregar_control(New List(Of Control)({txt_material_cantidad})).ActivarEventosNumericos()

    End Sub

    Private Sub Materiales()

        BuscarProducto.StartPosition = FormStartPosition.CenterParent
        BuscarProducto.mostrar()

        If BuscarProducto.get_info.get_codigo = String.Empty Then Exit Sub

        set_data_to_controls(BuscarProducto.get_info)

    End Sub

    Private Sub set_data_to_controls(ByRef DATA As frmBusqueda_Prod.Data)

        txt_material_cantidad.Text = String.Empty

        Material.get_codigo = DATA.get_codigo
        Material.get_costo = FormatNumber(DATA.get_costo, 2)
        Material.get_descripcion = DATA.get_descripcion
        Material.get_existencia = DATA.get_existencia
        Material.get_unidad_medida = DATA.get_unidad_medida
        Material.get_adicional = 0
        Material.get_autorizado = 1
        Material.get_activo = 1

        txt_material_codigo.Text = Material.get_codigo
        txt_material_costo_unitario.Text = FormatNumber(Material.get_costo, 2)
        txt_material_descripcion.Text = Material.get_descripcion
        txt_material_existencia.Text = Material.get_existencia

    End Sub

    Private Sub calcular_costo_total()

        Material.get_cantidad = ChangeToDouble(txt_material_cantidad.Text)

        Material.get_costo_total = FormatNumber(Material.get_costo * Material.get_cantidad, 4)

        txt_material_costo_total.Text = FormatNumber(Material.get_costo_total, 2)

    End Sub

    Private Sub aceptar()

        If Material.get_codigo = String.Empty Then MsgBox("Seleccione un material", MsgBoxStyle.Critical) : Exit Sub
        If Material.get_cantidad = 0 Then MsgBox("Digite una cantidad ", MsgBoxStyle.Critical) : Exit Sub

        calcular_costo_total()

        Me.Close()

    End Sub

    Private Sub cancelar()

        Me.Material = New Material_garantia

        Me.Close()

    End Sub

    Public Sub get_material_modificar(material_from_orden As Material_garantia)

        Material = material_from_orden

        txt_material_cantidad.Text = Material.get_cantidad
        txt_material_codigo.Text = Material.get_codigo
        txt_material_costo_unitario.Text = FormatNumber(Material.get_costo, 2)
        txt_material_descripcion.Text = Material.get_descripcion
        txt_material_existencia.Text = Material.get_existencia

        calcular_costo_total()

    End Sub

    Public ReadOnly Property my_material As Material_garantia
        Get
            Return Me.Material
        End Get
    End Property
End Class