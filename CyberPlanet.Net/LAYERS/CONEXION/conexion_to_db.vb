﻿
Imports System.Data.SqlClient

Public NotInheritable Class conexion_to_db


    Private Shared ConfigToServer = My.Settings.SolIndustrialCNX
    Private Shared Conexion As New SqlConnection(ConfigToServer)
    Private Shared Comando As New SqlCommand

    Private Sub New()

    End Sub
    Private Shared Sub Open_Conexion()

        Try

            If Not Conexion.State = ConnectionState.Open Then Conexion.Open()

        Catch ex As Exception

            MsgBox("Error: Ha ocurrido un problema al tratar de conectar al servidor")

        End Try

    End Sub


    Public Shared Function GetData(ByVal Procedimiento As String, ParamArray Parametros() As SqlParameter) As DataTable

        GetData = Nothing

        Try

            Open_Conexion()

            Dim DATOS As New DataTable, adp As New SqlDataAdapter, cmd As New SqlCommand() With {.Connection = Conexion, .CommandText = Procedimiento}

            adp.SelectCommand = cmd

            cmd.CommandType = CommandType.StoredProcedure

            SqlCommandBuilder.DeriveParameters(cmd)

            cmd.Parameters.Clear()

            For Each Parametro As SqlParameter In Parametros

                Parametro.Direction = ParameterDirection.Input
                cmd.Parameters.Add(Parametro)

            Next

            adp.Fill(DATOS)

            cmd.Dispose()

            adp.Dispose()

            Conexion.Close()

            Return DATOS


        Catch ex As Exception

            MsgBox("Error: " + ex.Message)

        End Try

    End Function

    Public Shared Function SendData(ByVal tabla As String, ParamArray Parametros() As SqlParameter) As Integer

        SendData = Nothing
        Try

            Open_Conexion()

            Dim MyComando As New SqlCommand() With {.Connection = Conexion, .CommandText = tabla, .CommandType = CommandType.StoredProcedure} ' inline

            SqlCommandBuilder.DeriveParameters(MyComando)

            MyComando.Parameters.Clear()

            For Each Parametro As SqlParameter In Parametros

                Parametro.Direction = ParameterDirection.Input
                MyComando.Parameters.Add(Parametro)

            Next

            SendData = MyComando.ExecuteScalar()

            MyComando.Dispose()

            Conexion.Close()

        Catch ex As Exception

            MsgBox("Error: " + ex.Message)

        End Try

    End Function
    Public Shared Function GetVista(Sql As String) As DataTable

        GetVista = Nothing

        Try

            Open_Conexion()

            Dim DATOS As New DataTable, adp As New SqlDataAdapter

            Comando.Connection = Conexion

            Comando.CommandText = Sql

            adp.SelectCommand = Comando

            Comando.CommandType = CommandType.Text

            adp.Fill(DATOS)

            Comando.Dispose()

            adp.Dispose()

            Conexion.Close()

            Return DATOS

        Catch ex As Exception

            MsgBox("Error: " + ex.Message)

        End Try


    End Function

    Public Shared Function ExecuteSql(Sql As String) As String

        ExecuteSql = String.Empty

        Try

            Open_Conexion()

            Comando.Connection = Conexion

            Comando.CommandText = Sql

            Comando.CommandType = CommandType.Text

            ExecuteSql = Comando.ExecuteScalar()

            Comando.Dispose()

            Conexion.Close()

            Return ExecuteSql

        Catch ex As Exception

            MsgBox("Error: " + ex.Message)

        End Try


    End Function

End Class
