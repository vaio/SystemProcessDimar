﻿Public Class FrmOdometro
    Private Sub FrmOdometro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbPlaca.Text = FrmRemision.txtVehiculo.Text
        lbFecha.Text = FrmRemision.dtpFecha.Text
        lbRemision.Text = FrmRemision.lbRemision.Text
        txtOdoI.Text = FrmRemision.txtOdoI.Text
        txtOdoF.Focus()
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim OdoI As Decimal = txtOdoI.Text
        Dim OdoF As Decimal = txtOdoF.Text
        If OdoF <= OdoI Then
            MsgBox("El Kilometraje no es validdo")
            txtOdoF.Focus()
            txtOdoF.SelectAll()
        Else
            FrmRemision.OdoF = OdoF
            FrmRemision.txtOdoF.Text = FrmRemision.OdoF
            Me.Close()
        End If
    End Sub
    Private Sub txtOdoF_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOdoF.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub
End Class