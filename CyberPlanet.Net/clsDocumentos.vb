﻿Imports System.Data.SqlClient

Public Class clsDocumentos

    Implements IDisposable

    Dim mconexion As String
    Dim strSql As String
    Dim isWebService As Boolean = False

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private disposedValue As Boolean = False    ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#End Region

    Public Sub New(ByVal ConextionString As String)
        mconexion = ConextionString
    End Sub

    Public Function Sql(ByVal strSql As String) As DataSet
        Dim cn As New SqlConnection
        cn.ConnectionString = mconexion
        Try
            cn.Open()
            Dim sqlda As New SqlDataAdapter(strSql, cn)
            Dim ds As New DataSet
            sqlda.Fill(ds, "tblNotas")
            Sql = ds
        Catch ex As Exception
      Call MsgBox(ex.Message & vbNewLine & "Consulte al Administrador del Sistema", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, "FINANZAS.NET")
            Sql = Nothing
        Finally
            cn.Close()
        End Try
    End Function

    Public Function EditarMasterCompraLocal(ByVal Sucursal As Integer, ByVal NumDoc As String, ByVal TipoCompra As Integer, ByVal NumProv As String, ByVal TipoDoc As Integer, ByVal Fecha As Date,
                                            ByVal FechaVenc As Date, ByVal FechaRecep As Date, ByVal Plazo As Integer, ByVal Porcentaje_RetIR As Double, ByVal Porcentaje_RetIMI As Double, ByVal Observ As String,
                                            ByVal TipoExon As Integer, ByVal NoFactura As String, ByVal IsPorcentaje As Boolean, ByVal Descuento As Double, ByVal Aplicado As Boolean, ByVal MonedaCordoba As Boolean,
                                            ByVal TazaCambio As Double, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_Master_CompraLocal", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@Sucursal", SqlDbType.Int).Value = Sucursal
            cmdDocument.Parameters.Add("@NumCompra", SqlDbType.VarChar).Value = NumDoc
            cmdDocument.Parameters.Add("@TipoCompra", SqlDbType.Int).Value = TipoCompra
            cmdDocument.Parameters.Add("@NumProv", SqlDbType.VarChar).Value = NumProv
            cmdDocument.Parameters.Add("@TipoDoc", SqlDbType.Int).Value = TipoDoc
            cmdDocument.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha
            cmdDocument.Parameters.Add("@FechaVenc", SqlDbType.Date).Value = FechaVenc
            cmdDocument.Parameters.Add("@FechaRecep", SqlDbType.Date).Value = FechaRecep
            cmdDocument.Parameters.Add("@Plazo", SqlDbType.Int).Value = Plazo
            cmdDocument.Parameters.Add("@Porcentaje_RetIR", SqlDbType.Decimal).Value = Porcentaje_RetIR
            cmdDocument.Parameters.Add("@Porcentaje_RetIMI", SqlDbType.Decimal).Value = Porcentaje_RetIMI
            cmdDocument.Parameters.Add("@Observ", SqlDbType.NVarChar).Value = Observ
            cmdDocument.Parameters.Add("@TipoExon", SqlDbType.Int).Value = TipoExon

            cmdDocument.Parameters.Add("@NoFactura", SqlDbType.NVarChar).Value = NoFactura
            cmdDocument.Parameters.Add("@IsPorcentaje", SqlDbType.Bit).Value = IsPorcentaje
            cmdDocument.Parameters.Add("@Descuento", SqlDbType.Money).Value = Descuento
            cmdDocument.Parameters.Add("@Aplicado", SqlDbType.Bit).Value = Aplicado
            cmdDocument.Parameters.Add("@MonedaCordoba", SqlDbType.Bit).Value = MonedaCordoba
            cmdDocument.Parameters.Add("@TazaCambio", SqlDbType.Decimal).Value = TazaCambio

            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocument.ExecuteReader()
            EditarMasterCompraLocal = "OK"
        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMasterCompraLocal = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarDetalleCompraLocal(ByVal nSucursal As Integer, ByVal sNumDoc As String, ByVal TipoCompra As Integer, ByVal sNumProv As String, ByVal IdBod As Integer,
    ByVal IdProd As String, ByVal FechaExp As DateTime, ByVal Cant As Integer, ByVal CantBonif As Integer, ByVal CantDev As Integer, ByVal PrecUnit As Double,
    ByVal PrecUnitUS As Double, ByVal Desc As Double, ByVal NewExistUnd As Integer, ByVal NewExistMon As Double, ByVal ExistUndTotal As Integer, ByVal ExistMonTotal As Double,
    ByVal NewCostoProm As Double, ByVal nSumSubTotal As Double, ByVal nSumDesc As Double, ByVal nIR As Double, ByVal nIMI As Double, ByVal nIVA As Double, ByVal PorcIR As Integer, ByVal PorcIMI As Integer, ByVal dFechaDoc As DateTime, ByVal dFechaVenc As DateTime, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocumentDet As New SqlCommand("SP_Detalle_CompraLocal", cnConec)
            cmdDocumentDet.CommandType = CommandType.StoredProcedure
            cmdDocumentDet.Parameters.Add("@Sucursal", SqlDbType.Int).Value = nSucursal
            cmdDocumentDet.Parameters.Add("@NumCompra", SqlDbType.VarChar).Value = sNumDoc
            cmdDocumentDet.Parameters.Add("@TipoCompra", SqlDbType.Int).Value = TipoCompra
            cmdDocumentDet.Parameters.Add("@NumProv", SqlDbType.VarChar).Value = sNumProv
            cmdDocumentDet.Parameters.Add("@Bodega", SqlDbType.Int).Value = IdBod
            cmdDocumentDet.Parameters.Add("@CodProd", SqlDbType.VarChar).Value = IdProd
            cmdDocumentDet.Parameters.Add("@FechaExp", SqlDbType.DateTime).Value = FechaExp
            cmdDocumentDet.Parameters.Add("@Cant", SqlDbType.Int).Value = Cant
            cmdDocumentDet.Parameters.Add("@CantBonif", SqlDbType.Int).Value = CantBonif
            cmdDocumentDet.Parameters.Add("@CantDev", SqlDbType.Int).Value = CantDev
            cmdDocumentDet.Parameters.Add("@PrecUnit", SqlDbType.Money).Value = PrecUnit
            cmdDocumentDet.Parameters.Add("@PrecUnitUS", SqlDbType.Money).Value = PrecUnitUS
            cmdDocumentDet.Parameters.Add("@Desc", SqlDbType.Money).Value = Desc

            cmdDocumentDet.Parameters.Add("@NewExistUnd", SqlDbType.Int).Value = NewExistUnd
            cmdDocumentDet.Parameters.Add("@NewExistMon", SqlDbType.Money).Value = NewExistMon
            cmdDocumentDet.Parameters.Add("@ExistUndTotal", SqlDbType.Int).Value = ExistUndTotal
            cmdDocumentDet.Parameters.Add("@ExistMonTotal", SqlDbType.Money).Value = ExistMonTotal
            cmdDocumentDet.Parameters.Add("@NewCostoProm", SqlDbType.Money).Value = NewCostoProm

            cmdDocumentDet.Parameters.Add("@SumSubTotal", SqlDbType.Money).Value = nSumSubTotal
            cmdDocumentDet.Parameters.Add("@SumDesc", SqlDbType.Money).Value = nSumDesc
            cmdDocumentDet.Parameters.Add("@IR", SqlDbType.Money).Value = nIR
            cmdDocumentDet.Parameters.Add("@IMI", SqlDbType.Money).Value = nIMI
            cmdDocumentDet.Parameters.Add("@IVA", SqlDbType.Money).Value = nIVA
            cmdDocumentDet.Parameters.Add("@PorcIR", SqlDbType.Int).Value = PorcIR
            cmdDocumentDet.Parameters.Add("@PorcIMI", SqlDbType.Int).Value = PorcIMI

            cmdDocumentDet.Parameters.Add("@FechaDoc", SqlDbType.DateTime).Value = dFechaDoc
            cmdDocumentDet.Parameters.Add("@FechaVenc", SqlDbType.DateTime).Value = dFechaVenc

            cmdDocumentDet.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocumentDet.ExecuteReader()

            EditarDetalleCompraLocal = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarDetalleCompraLocal = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarMasterDoc(ByVal Sucursal As Integer, ByVal IdDoc As String, ByVal Serie As String, ByVal Concepto As Integer, ByVal Fecha As Date,
    ByVal Observ As String, ByVal TipoDoc As Integer, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_AME_DocInvent_Master", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@Sucursal", SqlDbType.Int).Value = Sucursal
            cmdDocument.Parameters.Add("@IdDoc", SqlDbType.NVarChar).Value = IdDoc
            cmdDocument.Parameters.Add("@Serie", SqlDbType.NVarChar).Value = Serie
            cmdDocument.Parameters.Add("@Concepto", SqlDbType.Int).Value = Concepto
            cmdDocument.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha
            cmdDocument.Parameters.Add("@Observ", SqlDbType.NVarChar).Value = Observ
            cmdDocument.Parameters.Add("@TipoDoc", SqlDbType.Int).Value = TipoDoc
            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocument.ExecuteReader()

            EditarMasterDoc = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMasterDoc = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarDetalleDoc(ByVal Sucursal As Integer, ByVal IdDoc As String, ByVal Serie As String, ByVal IdBod As Integer, ByVal IdBodDest As Integer,
    ByVal IdProd As Integer, ByVal Cant As Integer, ByVal CostoUnit As Double, ByVal TipoDoc As Integer, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocumentDet As New SqlCommand("SP_AME_DocInvent_Detalle", cnConec)
            cmdDocumentDet.CommandType = CommandType.StoredProcedure
            cmdDocumentDet.Parameters.Add("@Sucursal", SqlDbType.Int).Value = Sucursal
            cmdDocumentDet.Parameters.Add("@IdDoc", SqlDbType.NVarChar).Value = IdDoc
            cmdDocumentDet.Parameters.Add("@Serie", SqlDbType.NVarChar).Value = Serie
            cmdDocumentDet.Parameters.Add("@IdBod_Orig", SqlDbType.Int).Value = IdBod
            cmdDocumentDet.Parameters.Add("@IdBod_Dest", SqlDbType.Int).Value = IdBodDest
            cmdDocumentDet.Parameters.Add("@IdProd", SqlDbType.Int).Value = IdProd
            cmdDocumentDet.Parameters.Add("@Cant", SqlDbType.Int).Value = Cant
            cmdDocumentDet.Parameters.Add("@CostoUnit", SqlDbType.Decimal).Value = CostoUnit
            cmdDocumentDet.Parameters.Add("@TipoDoc", SqlDbType.Int).Value = TipoDoc
            cmdDocumentDet.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocumentDet.ExecuteReader()

            EditarDetalleDoc = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarDetalleDoc = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarMasterCompraForanea(ByVal Sucursal As Integer, ByVal NumDoc As String, ByVal TipoCompra As Integer, ByVal CodProv As String, ByVal Fecha As Date, ByVal FechaVenc As Date, ByVal FechaRecep As Date, ByVal Plazo As Integer, ByVal Num_FacturasForaneas As String, ByVal Observ As String, ByVal Pesokgms As Double,
    ByVal Total_FOB As Double, ByVal Total_GastosExport As Double, ByVal Total_FletesTer As Double, ByVal TSI As Double, ByVal SPE As Double, ByVal DAI As Double, ByVal ISC As Double, ByVal Flete As Double, ByVal IVA_Poliza As Double, ByVal Desaduanaje As Double,
    ByVal Descargue As Double, ByVal Otros_Gastos As Double, ByVal IsNacionalizado As Boolean, ByVal SSA As Double, ByVal Seguro As Double, ByVal Otros As Double, ByVal MonedaCordoba As Boolean, ByVal Tipo_de_Cambio As Double, ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_Master_CompraForanea", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@Sucursal", SqlDbType.Int).Value = Sucursal
            cmdDocument.Parameters.Add("@NumDoc", SqlDbType.VarChar).Value = NumDoc
            cmdDocument.Parameters.Add("@TipoCompra", SqlDbType.Int).Value = TipoCompra
            cmdDocument.Parameters.Add("@CodProv", SqlDbType.VarChar).Value = CodProv
            cmdDocument.Parameters.Add("@Fecha", SqlDbType.Date).Value = Fecha
            cmdDocument.Parameters.Add("@FechaVenc", SqlDbType.Date).Value = FechaVenc
            cmdDocument.Parameters.Add("@FechaRecep", SqlDbType.Date).Value = FechaRecep
            cmdDocument.Parameters.Add("@Plazo", SqlDbType.Int).Value = Plazo
            cmdDocument.Parameters.Add("@NumFacts", SqlDbType.NVarChar).Value = Num_FacturasForaneas
            cmdDocument.Parameters.Add("@Observ", SqlDbType.NVarChar).Value = Observ
            cmdDocument.Parameters.Add("@PesoKgms", SqlDbType.Float).Value = Pesokgms
            cmdDocument.Parameters.Add("@TotalFOB", SqlDbType.Money).Value = Total_FOB
            cmdDocument.Parameters.Add("@TotalGasExp", SqlDbType.Money).Value = Total_GastosExport
            cmdDocument.Parameters.Add("@TotalFleTerr", SqlDbType.Money).Value = Total_FletesTer
            cmdDocument.Parameters.Add("@TSI", SqlDbType.Money).Value = TSI
            cmdDocument.Parameters.Add("@SPE", SqlDbType.Money).Value = SPE
            cmdDocument.Parameters.Add("@DAI", SqlDbType.Money).Value = DAI
            cmdDocument.Parameters.Add("@ISC", SqlDbType.Money).Value = ISC
            cmdDocument.Parameters.Add("@Flete", SqlDbType.Money).Value = Flete
            cmdDocument.Parameters.Add("@IVAPoliza", SqlDbType.Money).Value = IVA_Poliza
            cmdDocument.Parameters.Add("@Desaduanaje", SqlDbType.Money).Value = Desaduanaje
            cmdDocument.Parameters.Add("@Descargue", SqlDbType.Money).Value = Descargue
            cmdDocument.Parameters.Add("@OtrosGastos", SqlDbType.Money).Value = Otros_Gastos
            cmdDocument.Parameters.Add("@IsNacionalizado", SqlDbType.Bit).Value = IsNacionalizado
            cmdDocument.Parameters.Add("@SSA", SqlDbType.Money).Value = SSA
            cmdDocument.Parameters.Add("@Seguro", SqlDbType.Money).Value = Seguro
            cmdDocument.Parameters.Add("@Otros", SqlDbType.Money).Value = Otros
            cmdDocument.Parameters.Add("@MonedaCordoba", SqlDbType.Bit).Value = MonedaCordoba
            cmdDocument.Parameters.Add("@TazaCambio", SqlDbType.Money).Value = Tipo_de_Cambio
            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion
            cmdDocument.ExecuteReader()
            EditarMasterCompraForanea = "OK"
        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMasterCompraForanea = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

    Public Function EditarDetalleCompraForanea(ByVal CodigoSucursal As Integer, ByVal Numero_de_Documento As String, ByVal Tipo_Compra As Integer, ByVal Codigo_Proveedor As String, ByVal Bodega As Integer,
                                            ByVal Codigo_Producto As String, ByVal Cantidad As Integer, ByVal Precio_Unit As Double, ByVal Peso_Neto_Kgm As Double, ByVal Otros_Impuestos As Double,
                                            ByVal Desaduanaje As Double, ByVal Descargue As Double, ByVal OtrosGastos As Double, ByVal Fecha_Expiracion As Date, ByVal nTipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocumentDet As New SqlCommand("SP_Detalle_CompraForanea", cnConec)
            cmdDocumentDet.CommandType = CommandType.StoredProcedure
            cmdDocumentDet.Parameters.Add("@CodigoSucursal", SqlDbType.Int).Value = CodigoSucursal
            cmdDocumentDet.Parameters.Add("@Numero_de_Documento", SqlDbType.VarChar).Value = Numero_de_Documento
            cmdDocumentDet.Parameters.Add("@Tipo_Compra", SqlDbType.Int).Value = Tipo_Compra
            cmdDocumentDet.Parameters.Add("@Codigo_Proveedor", SqlDbType.VarChar).Value = Codigo_Proveedor
            cmdDocumentDet.Parameters.Add("@Bodega", SqlDbType.Int).Value = Bodega
            cmdDocumentDet.Parameters.Add("@Codigo_Producto", SqlDbType.VarChar).Value = Codigo_Producto

            cmdDocumentDet.Parameters.Add("@Cantidad", SqlDbType.Int).Value = Cantidad
            cmdDocumentDet.Parameters.Add("@Precio_Unit", SqlDbType.Money).Value = Precio_Unit
            cmdDocumentDet.Parameters.Add("@Peso_Neto_Kgm", SqlDbType.Float).Value = Peso_Neto_Kgm
            cmdDocumentDet.Parameters.Add("@Otros_Impuestos", SqlDbType.Money).Value = Otros_Impuestos
            cmdDocumentDet.Parameters.Add("@Desaduanaje", SqlDbType.Money).Value = Desaduanaje
            cmdDocumentDet.Parameters.Add("@Descargue", SqlDbType.Money).Value = Descargue
            cmdDocumentDet.Parameters.Add("@OtrosGastos", SqlDbType.Money).Value = OtrosGastos

            cmdDocumentDet.Parameters.Add("@Fecha_Expiracion", SqlDbType.DateTime).Value = Fecha_Expiracion
            cmdDocumentDet.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = nTipoEdicion
            cmdDocumentDet.ExecuteReader()

            EditarDetalleCompraForanea = "OK"

        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarDetalleCompraForanea = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try
    End Function

    Public Function EditarMasterPagos(Nsucursal_ As Integer, ByVal Codigo_Proveedor As String, ByVal Tipo_Documento As Integer, ByVal Numero_Documento As String, ByVal Fecha_Documento As Date,
                                      ByVal Fecha_Vencimiento As Date, ByVal Numero_Referencia As String, ByVal Tipo_Referencia As Integer, numero_referencia_fisico As String, ByVal SubTotal As Double, ByVal IR As Double, ByVal IMI As Double,
                                      ByVal IVA As Double, ByVal Otros_Impuestos As Double, ByVal Otros_Gastos As Double, ByVal Multas_Recargos As Double, ByVal Fecha_Registro As Date, ByVal Valido As Boolean,
                                      ByVal TipoEdicion As Integer)
        Dim ID As String = ""
        Dim cnConec As New SqlConnection

        With cnConec
            .ConnectionString = mconexion
            .Open()
        End With

        Try
            Dim cmdDocument As New SqlCommand("SP_AME_Pago", cnConec)
            cmdDocument.CommandType = CommandType.StoredProcedure
            cmdDocument.Parameters.Add("@sucursal", SqlDbType.Int).Value = Nsucursal_
            cmdDocument.Parameters.Add("@Codigo_Proveedor", SqlDbType.VarChar).Value = Codigo_Proveedor
            cmdDocument.Parameters.Add("@Tipo_Documento", SqlDbType.Int).Value = Tipo_Documento
            cmdDocument.Parameters.Add("@Numero_Documento", SqlDbType.VarChar).Value = Numero_Documento
            cmdDocument.Parameters.Add("@Fecha_Documento", SqlDbType.Date).Value = Fecha_Documento

            cmdDocument.Parameters.Add("@Fecha_Vencimiento", SqlDbType.Date).Value = Fecha_Vencimiento
            cmdDocument.Parameters.Add("@Numero_Referencia", SqlDbType.VarChar).Value = Numero_Referencia
            cmdDocument.Parameters.Add("@Tipo_Referencia", SqlDbType.Int).Value = Tipo_Referencia
            cmdDocument.Parameters.Add("@numero_referencia_fisico", SqlDbType.NChar).Value = numero_referencia_fisico
            cmdDocument.Parameters.Add("@SubTotal", SqlDbType.Money).Value = SubTotal
            cmdDocument.Parameters.Add("@IR", SqlDbType.Money).Value = IR
            cmdDocument.Parameters.Add("@IMI", SqlDbType.Money).Value = IMI

            cmdDocument.Parameters.Add("@IVA", SqlDbType.Money).Value = IVA
            cmdDocument.Parameters.Add("@Otros_Impuestos", SqlDbType.Money).Value = Otros_Impuestos

            cmdDocument.Parameters.Add("@Otros_Gastos", SqlDbType.Money).Value = Otros_Gastos
            cmdDocument.Parameters.Add("@Multas_Recargos", SqlDbType.Money).Value = Multas_Recargos
            cmdDocument.Parameters.Add("@Fecha_Registro", SqlDbType.Date).Value = Fecha_Registro
            cmdDocument.Parameters.Add("@Valido", SqlDbType.Bit).Value = Valido
            cmdDocument.Parameters.Add("@nTipoEdicion", SqlDbType.Int).Value = TipoEdicion

            cmdDocument.ExecuteReader()
            EditarMasterPagos = "OK"
        Catch ex As SqlException
            MsgBox("Error: " + ex.Message)
            EditarMasterPagos = "ERROR"
        Finally
            cnConec.Close()
            cnConec.Dispose()
        End Try

    End Function

End Class
