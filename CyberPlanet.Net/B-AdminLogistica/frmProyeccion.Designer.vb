﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProyeccion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProyeccion))
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.grdDetProyeccion = New DevExpress.XtraGrid.GridControl()
        Me.grvDetProyeccion = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lbRequerido = New System.Windows.Forms.Label()
        Me.lblTraslado = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblExist = New System.Windows.Forms.Label()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.grdProyeccion = New DevExpress.XtraGrid.GridControl()
        Me.grvProyeccion = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.pMaximo = New System.Windows.Forms.Panel()
        Me.lbMaximo = New System.Windows.Forms.Label()
        Me.ckbMaterial = New System.Windows.Forms.CheckBox()
        Me.ckbProceso = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbFechaF = New System.Windows.Forms.Label()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.CmbTipo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCodigoOrden = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CmbSolicitante = New System.Windows.Forms.ComboBox()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.daCerrado = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.deRegistrado = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.mainPanel.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.grdDetProyeccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetProyeccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.grdProyeccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvProyeccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMaximo.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.Panel2)
        Me.mainPanel.Controls.Add(Me.Panel3)
        Me.mainPanel.Controls.Add(Me.GroupBox1)
        Me.mainPanel.Location = New System.Drawing.Point(0, 0)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1160, 493)
        Me.mainPanel.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.grdDetProyeccion)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(399, 66)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(761, 427)
        Me.Panel2.TabIndex = 84
        '
        'grdDetProyeccion
        '
        Me.grdDetProyeccion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdDetProyeccion.Location = New System.Drawing.Point(0, 37)
        Me.grdDetProyeccion.MainView = Me.grvDetProyeccion
        Me.grdDetProyeccion.Name = "grdDetProyeccion"
        Me.grdDetProyeccion.Size = New System.Drawing.Size(761, 390)
        Me.grdDetProyeccion.TabIndex = 15
        Me.grdDetProyeccion.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetProyeccion})
        '
        'grvDetProyeccion
        '
        Me.grvDetProyeccion.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvDetProyeccion.GridControl = Me.grdDetProyeccion
        Me.grvDetProyeccion.Name = "grvDetProyeccion"
        Me.grvDetProyeccion.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetProyeccion.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetProyeccion.OptionsBehavior.Editable = False
        Me.grvDetProyeccion.OptionsBehavior.ReadOnly = True
        Me.grvDetProyeccion.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetProyeccion.OptionsCustomization.AllowColumnResizing = False
        Me.grvDetProyeccion.OptionsCustomization.AllowGroup = False
        Me.grvDetProyeccion.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetProyeccion.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvDetProyeccion.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.grvDetProyeccion.OptionsView.AllowHtmlDrawGroups = False
        Me.grvDetProyeccion.OptionsView.ColumnAutoWidth = False
        Me.grvDetProyeccion.OptionsView.ShowAutoFilterRow = True
        Me.grvDetProyeccion.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetProyeccion.OptionsView.ShowFooter = True
        Me.grvDetProyeccion.OptionsView.ShowGroupPanel = False
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label10)
        Me.Panel5.Controls.Add(Me.Label11)
        Me.Panel5.Controls.Add(Me.lbRequerido)
        Me.Panel5.Controls.Add(Me.lblTraslado)
        Me.Panel5.Controls.Add(Me.Label15)
        Me.Panel5.Controls.Add(Me.lblExist)
        Me.Panel5.Controls.Add(Me.btnGenerar)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(761, 37)
        Me.Panel5.TabIndex = 13
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(343, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(82, 13)
        Me.Label10.TabIndex = 90
        Me.Label10.Text = "Con Existencias"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Location = New System.Drawing.Point(329, 11)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(14, 14)
        Me.Label11.TabIndex = 89
        '
        'lbRequerido
        '
        Me.lbRequerido.AutoSize = True
        Me.lbRequerido.Location = New System.Drawing.Point(455, 11)
        Me.lbRequerido.Name = "lbRequerido"
        Me.lbRequerido.Size = New System.Drawing.Size(48, 13)
        Me.lbRequerido.TabIndex = 88
        Me.lbRequerido.Text = "Traslado"
        '
        'lblTraslado
        '
        Me.lblTraslado.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblTraslado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTraslado.Location = New System.Drawing.Point(441, 11)
        Me.lblTraslado.Name = "lblTraslado"
        Me.lblTraslado.Size = New System.Drawing.Size(14, 14)
        Me.lblTraslado.TabIndex = 87
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(539, 11)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 13)
        Me.Label15.TabIndex = 86
        Me.Label15.Text = "Sin Existencia"
        '
        'lblExist
        '
        Me.lblExist.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblExist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblExist.Location = New System.Drawing.Point(525, 11)
        Me.lblExist.Name = "lblExist"
        Me.lblExist.Size = New System.Drawing.Size(14, 14)
        Me.lblExist.TabIndex = 85
        '
        'btnGenerar
        '
        Me.btnGenerar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnGenerar.Enabled = False
        Me.btnGenerar.Image = CType(resources.GetObject("btnGenerar.Image"), System.Drawing.Image)
        Me.btnGenerar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerar.Location = New System.Drawing.Point(628, 4)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(128, 27)
        Me.btnGenerar.TabIndex = 80
        Me.btnGenerar.Text = "Generar Pedido"
        Me.btnGenerar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Navy
        Me.Label7.Location = New System.Drawing.Point(9, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(225, 15)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Materiales Requeridos en la Proyección"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.grdProyeccion)
        Me.Panel3.Controls.Add(Me.pMaximo)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Location = New System.Drawing.Point(0, 66)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(398, 427)
        Me.Panel3.TabIndex = 83
        '
        'grdProyeccion
        '
        Me.grdProyeccion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdProyeccion.Location = New System.Drawing.Point(0, 37)
        Me.grdProyeccion.MainView = Me.grvProyeccion
        Me.grdProyeccion.Name = "grdProyeccion"
        Me.grdProyeccion.Size = New System.Drawing.Size(398, 332)
        Me.grdProyeccion.TabIndex = 15
        Me.grdProyeccion.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvProyeccion})
        '
        'grvProyeccion
        '
        Me.grvProyeccion.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.grvProyeccion.GridControl = Me.grdProyeccion
        Me.grvProyeccion.Name = "grvProyeccion"
        Me.grvProyeccion.OptionsBehavior.Editable = False
        Me.grvProyeccion.OptionsBehavior.ReadOnly = True
        Me.grvProyeccion.OptionsCustomization.AllowColumnMoving = False
        Me.grvProyeccion.OptionsCustomization.AllowColumnResizing = False
        Me.grvProyeccion.OptionsCustomization.AllowGroup = False
        Me.grvProyeccion.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvProyeccion.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvProyeccion.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.grvProyeccion.OptionsView.ColumnAutoWidth = False
        Me.grvProyeccion.OptionsView.ShowAutoFilterRow = True
        Me.grvProyeccion.OptionsView.ShowGroupPanel = False
        '
        'pMaximo
        '
        Me.pMaximo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pMaximo.Controls.Add(Me.lbMaximo)
        Me.pMaximo.Controls.Add(Me.ckbMaterial)
        Me.pMaximo.Controls.Add(Me.ckbProceso)
        Me.pMaximo.Controls.Add(Me.Label4)
        Me.pMaximo.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pMaximo.Location = New System.Drawing.Point(0, 369)
        Me.pMaximo.Name = "pMaximo"
        Me.pMaximo.Size = New System.Drawing.Size(398, 58)
        Me.pMaximo.TabIndex = 82
        Me.pMaximo.Visible = False
        '
        'lbMaximo
        '
        Me.lbMaximo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbMaximo.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMaximo.Location = New System.Drawing.Point(147, 9)
        Me.lbMaximo.Name = "lbMaximo"
        Me.lbMaximo.Size = New System.Drawing.Size(156, 37)
        Me.lbMaximo.TabIndex = 59
        Me.lbMaximo.Text = "0.00"
        Me.lbMaximo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ckbMaterial
        '
        Me.ckbMaterial.AutoSize = True
        Me.ckbMaterial.Location = New System.Drawing.Point(310, 29)
        Me.ckbMaterial.Name = "ckbMaterial"
        Me.ckbMaterial.Size = New System.Drawing.Size(74, 17)
        Me.ckbMaterial.TabIndex = 58
        Me.ckbMaterial.Text = "Materiales"
        Me.ckbMaterial.UseVisualStyleBackColor = True
        '
        'ckbProceso
        '
        Me.ckbProceso.AutoSize = True
        Me.ckbProceso.Location = New System.Drawing.Point(310, 10)
        Me.ckbProceso.Name = "ckbProceso"
        Me.ckbProceso.Size = New System.Drawing.Size(70, 17)
        Me.ckbProceso.TabIndex = 57
        Me.ckbProceso.Text = "Procesos"
        Me.ckbProceso.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.Location = New System.Drawing.Point(9, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 15)
        Me.Label4.TabIndex = 56
        Me.Label4.Text = "Maxima Produccion:"
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.btnQuitar)
        Me.Panel4.Controls.Add(Me.btnAgregar)
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(398, 37)
        Me.Panel4.TabIndex = 13
        '
        'btnQuitar
        '
        Me.btnQuitar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnQuitar.Image = CType(resources.GetObject("btnQuitar.Image"), System.Drawing.Image)
        Me.btnQuitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnQuitar.Location = New System.Drawing.Point(315, 4)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(77, 27)
        Me.btnQuitar.TabIndex = 81
        Me.btnQuitar.Text = "Quitar"
        Me.btnQuitar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(232, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(77, 27)
        Me.btnAgregar.TabIndex = 80
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Navy
        Me.Label9.Location = New System.Drawing.Point(9, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(132, 15)
        Me.Label9.TabIndex = 56
        Me.Label9.Text = "Productos Proyectados"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lbFechaF)
        Me.GroupBox1.Controls.Add(Me.lbEstado)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.CmbTipo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtCodigoOrden)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.CmbSolicitante)
        Me.GroupBox1.Controls.Add(Me.ckIsActivo)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.daCerrado)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.deRegistrado)
        Me.GroupBox1.Controls.Add(Me.lblFechaIngreso)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1160, 66)
        Me.GroupBox1.TabIndex = 82
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Proyeccion de Requerimientos de Materiales"
        '
        'lbFechaF
        '
        Me.lbFechaF.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbFechaF.ForeColor = System.Drawing.SystemColors.Desktop
        Me.lbFechaF.Location = New System.Drawing.Point(1037, -2)
        Me.lbFechaF.Name = "lbFechaF"
        Me.lbFechaF.Size = New System.Drawing.Size(112, 20)
        Me.lbFechaF.TabIndex = 85
        Me.lbFechaF.Text = "PENDIENTE"
        Me.lbFechaF.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbFechaF.Visible = False
        '
        'lbEstado
        '
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEstado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbEstado.Location = New System.Drawing.Point(888, -2)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(149, 20)
        Me.lbEstado.TabIndex = 84
        Me.lbEstado.Text = "Traslado Realizado:"
        Me.lbEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbEstado.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(482, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "Observacion:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Location = New System.Drawing.Point(558, 25)
        Me.txtObservacion.MaxLength = 100
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(292, 20)
        Me.txtObservacion.TabIndex = 3
        '
        'CmbTipo
        '
        Me.CmbTipo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CmbTipo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbTipo.FormattingEnabled = True
        Me.CmbTipo.Location = New System.Drawing.Point(910, 24)
        Me.CmbTipo.Name = "CmbTipo"
        Me.CmbTipo.Size = New System.Drawing.Size(178, 21)
        Me.CmbTipo.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Codigo:"
        '
        'txtCodigoOrden
        '
        Me.txtCodigoOrden.BackColor = System.Drawing.Color.White
        Me.txtCodigoOrden.Enabled = False
        Me.txtCodigoOrden.Location = New System.Drawing.Point(50, 25)
        Me.txtCodigoOrden.Name = "txtCodigoOrden"
        Me.txtCodigoOrden.Size = New System.Drawing.Size(92, 20)
        Me.txtCodigoOrden.TabIndex = 1
        Me.txtCodigoOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(151, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Solicitante:"
        '
        'CmbSolicitante
        '
        Me.CmbSolicitante.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CmbSolicitante.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbSolicitante.FormattingEnabled = True
        Me.CmbSolicitante.Location = New System.Drawing.Point(212, 25)
        Me.CmbSolicitante.Name = "CmbSolicitante"
        Me.CmbSolicitante.Size = New System.Drawing.Size(251, 21)
        Me.CmbSolicitante.TabIndex = 2
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Location = New System.Drawing.Point(1094, 25)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Activo"
        Me.ckIsActivo.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.ckIsActivo.Size = New System.Drawing.Size(60, 18)
        Me.ckIsActivo.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(878, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 16)
        Me.Label2.TabIndex = 81
        Me.Label2.Text = "Tipo:"
        '
        'daCerrado
        '
        Me.daCerrado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.daCerrado.Location = New System.Drawing.Point(498, 0)
        Me.daCerrado.Name = "daCerrado"
        Me.daCerrado.Size = New System.Drawing.Size(95, 20)
        Me.daCerrado.TabIndex = 79
        Me.daCerrado.Visible = False
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(471, 3)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(33, 19)
        Me.Label12.TabIndex = 77
        Me.Label12.Text = "Fin:"
        Me.Label12.Visible = False
        '
        'deRegistrado
        '
        Me.deRegistrado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deRegistrado.Location = New System.Drawing.Point(362, 0)
        Me.deRegistrado.Name = "deRegistrado"
        Me.deRegistrado.Size = New System.Drawing.Size(95, 20)
        Me.deRegistrado.TabIndex = 78
        Me.deRegistrado.Visible = False
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(325, 2)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(52, 16)
        Me.lblFechaIngreso.TabIndex = 63
        Me.lblFechaIngreso.Text = "Inicio:"
        Me.lblFechaIngreso.Visible = False
        '
        'frmProyeccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1161, 494)
        Me.Controls.Add(Me.mainPanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProyeccion"
        Me.Text = "Proyección de Materiales"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mainPanel.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.grdDetProyeccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetProyeccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        CType(Me.grdProyeccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvProyeccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMaximo.ResumeLayout(False)
        Me.pMaximo.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents CmbSolicitante As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents deRegistrado As System.Windows.Forms.DateTimePicker
    Friend WithEvents daCerrado As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents grdProyeccion As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvProyeccion As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents grdDetProyeccion As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetProyeccion As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents CmbTipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pMaximo As System.Windows.Forms.Panel
    Friend WithEvents ckbMaterial As System.Windows.Forms.CheckBox
    Friend WithEvents ckbProceso As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents lbMaximo As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lbRequerido As System.Windows.Forms.Label
    Friend WithEvents lblTraslado As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblExist As System.Windows.Forms.Label
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents lbFechaF As System.Windows.Forms.Label
End Class
