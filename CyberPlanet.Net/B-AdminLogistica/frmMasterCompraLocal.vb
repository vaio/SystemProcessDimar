﻿Public Class frmMasterCompraLocal

    Public CodigoCompra As String = ""
    Public PorcenDes As Double = 0
    Dim CodMaxEntrada As String = ""
    Dim CodEntrada As String = ""

    '-------------------- written by bob ----------------------------------

    Private ValidarCampoSoloEnteros As New Validar
    Private ValidarTextFactura As New Validar
    Private ValidarCampoDescuento As New Validar With {.CantDecimal = 2}

    Private IsIVA As Boolean = 0
    Private TasaIva As Double = 0

    '---------------------------------------------------------------------

    Private Sub frmMasterCompraLocal_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        NumCatalogo = 21
        LimpiarCampos()
        CargarProveedor()
        cmbTipExoneracion.SelectedIndex = 0
        CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
        DesactivarCampos(True)

        ValidarTextFactura.agregar_control(New List(Of Control)({txtFactura})).PermitirLetrasNumeros()
        ValidarCampoSoloEnteros.agregar_control(New List(Of Control)({txtPlazo})).SoloEnteros()
        ValidarCampoDescuento.agregar_control(New List(Of Control)({txtDescuentoDos})).ActivarEventosNumericos()

        AddHandler Fecha_Recepcionado.ValueChanged, AddressOf validar_fecha_documento
        AddHandler Fecha_Vencimiento.ValueChanged, AddressOf validar_fecha_documento
        AddHandler txtDescuentoDos.TextChanged, AddressOf validar_descuento_documento


    End Sub

    Sub CargarDetalleDoc(ByVal nSucursal As Integer, ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)
        Dim strSqlDoc As String
        strSqlDoc = "Select * from vwComprasData where CodigoSucursal = " & My.Settings.Sucursal & " and Numero_de_Compra = '" & sNumDoc & "' and Tipo_Compra = " & nTipoCompra & " and Codigo_Proveedor = '" & sProveedor & "'"
        Dim tblCodProv As DataTable = SQL(strSqlDoc, "tblDatosMasterDoc", My.Settings.SolIndustrialCNX).Tables(0)

        If tblCodProv.Rows.Count > 0 Then
            txtNumDoc.Text = tblCodProv.Rows(0).Item("Numero_de_Compra")
            CodigoCompra = tblCodProv.Rows(0).Item("Numero_de_Compra")
            luProveedor.EditValue = tblCodProv.Rows(0).Item("Codigo_Proveedor")
            deFechaDoc.Value = tblCodProv.Rows(0).Item("Fecha")
            Fecha_Vencimiento.Value = tblCodProv.Rows(0).Item("FechaVencimiento")
            Fecha_Recepcionado.Value = IIf(tblCodProv.Rows(0).Item("FechaRecepcion") Is DBNull.Value, Date.Now, tblCodProv.Rows(0).Item("FechaRecepcion"))
            txtPlazo.Text = tblCodProv.Rows(0).Item("Plazo")
            txtObservacion.Text = tblCodProv.Rows(0).Item("Observaciones")
            cmbTipExoneracion.SelectedIndex = tblCodProv.Rows(0).Item("TipoExoneracion")
            txtSubtotal.Text = FormatNumber(tblCodProv.Rows(0).Item("Total_Compra"), 4)
            txtDescuento.Text = FormatNumber(tblCodProv.Rows(0).Item("Total_Descuentos"), 4)
            txtIR.Text = FormatNumber(IIf(tblCodProv.Rows(0).Item("Total_RetencionIR") Is DBNull.Value, 0, tblCodProv.Rows(0).Item("Total_RetencionIR")), 4)
            txtIMI.Text = FormatNumber(IIf(tblCodProv.Rows(0).Item("Total_RetencionIMI") Is DBNull.Value, 0, tblCodProv.Rows(0).Item("Total_RetencionIMI")), 4)
            txtIVA.Text = FormatNumber(IIf(tblCodProv.Rows(0).Item("Total_ImpuestoIVA") Is DBNull.Value, 0, tblCodProv.Rows(0).Item("Total_ImpuestoIVA")), 4)
            txtTotalNeto.Text = FormatNumber(tblCodProv.Rows(0).Item("Total_Neto"), 4)
            txtFactura.Text = tblCodProv.Rows(0).Item("NoFactura")
            txtPorcentaje.Checked = tblCodProv.Rows(0).Item("IsPorcentaje")
            txtDescuentoDos.Text = FormatNumber(tblCodProv.Rows(0).Item("Descuento"), 4)
            txtTazaCambio.Text = tblCodProv.Rows(0).Item("TazaCambio")
            txtTotalbruto.Text = FormatNumber(IIf(tblCodProv.Rows(0).Item("Total_Bruto") Is DBNull.Value, 0, tblCodProv.Rows(0).Item("Total_Bruto")), 4)
            txtIVAExcento.Text = FormatNumber(IIf(tblCodProv.Rows(0).Item("Total_IVA_Excento") Is DBNull.Value, 0, tblCodProv.Rows(0).Item("Total_IVA_Excento")), 4)

            If tblCodProv.Rows(0).Item("MonedaCordoba") = True Then
                rbCordobas.Checked = True
            Else
                rbDolares.Checked = True
            End If

            PorcenDes = CDbl(tblCodProv.Rows(0).Item("Descuento"))

            lblEstado.Text = "SIN APLICAR"

            If tblCodProv.Rows(0).Item("Aplicado") = True Then
                lblEstado.Text = "COMPRA APLICADA"

                btnAgregar.Enabled = False
                btnAplicar.Enabled = False
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
            End If

            CargarDetalleCompra(sNumDoc, nTipoCompra, sProveedor)

        Else
            LimpiarCampos()
            txtNumDoc.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
            txtNumDoc.Focus()
            btnAgregar.Enabled = False
            btnAplicar.Enabled = False
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Sub CargarDetalleCompra(ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)
        Dim StrsqlDetDoc As String = ""
        Dim TableDatos As DataTable

        StrsqlDetDoc = "Select Codigo,Descripcion,CodBodega,Bodega,Entrada,Fecha_Expiracion,Excento_IVA,Cantidad,[Cant. Bonificada],Costo_Unitario,[Sub-Total],Descuento,Total from vwDetalleComprasLoc where "
        StrsqlDetDoc = StrsqlDetDoc & " CodigoSucursal = " & My.Settings.Sucursal & " and " & " Numero_de_Compra = '" & sNumDoc & "' "
        StrsqlDetDoc = StrsqlDetDoc & " and Tipo_Compra = " & nTipoCompra & " and Codigo_Proveedor = '" & sProveedor & "'"

        TableDatos = SQL(StrsqlDetDoc, "tblDetalleDoc", My.Settings.SolIndustrialCNX).Tables(0)

        grdDetalle.DataSource = TableDatos
        grvDetalle.Columns("Codigo").Width = 75
        grvDetalle.Columns("Descripcion").Width = 400
        grvDetalle.Columns("CodBodega").Visible = False
        grvDetalle.Columns("Bodega").Width = 200
        grvDetalle.Columns("Entrada").Width = 90
        grvDetalle.Columns("Fecha_Expiracion").Width = 100
        grvDetalle.Columns("Excento_IVA").Width = 70
        grvDetalle.Columns("Cantidad").Width = 80
        grvDetalle.Columns("Cant. Bonificada").Width = 100
        grvDetalle.Columns("Costo_Unitario").Width = 100
        grvDetalle.Columns("Sub-Total").Width = 100
        grvDetalle.Columns("Descuento").Width = 100
        grvDetalle.Columns("Total").Width = 100

        If Not lblEstado.Text = "COMPRA APLICADA" Then

            If grvDetalle.RowCount <= 0 Then
                txtPorcentaje.Enabled = True
                txtDescuentoDos.Enabled = True
                frmPrincipal.bbiEliminar.Enabled = True
                btnAgregar.Enabled = True
                btnAplicar.Enabled = True
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
                btnAplicar.Enabled = False
            Else
                txtPorcentaje.Enabled = False
                txtDescuentoDos.Enabled = False
                frmPrincipal.bbiEliminar.Enabled = False
                btnAgregar.Enabled = True
                btnAplicar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
                btnAplicar.Enabled = True
            End If

        End If
    End Sub

    Sub EncabezadosDetDoc()
        grvDetalle.Columns.Clear()
        grvDetalle.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "Codigo"
        gc0.Caption = "Codigo"
        gc0.FieldName = "Codigo"
        gc0.Width = 6
        grvDetalle.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        grvDetalle.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Descripcion"
        gc1.Caption = "Descripcion"
        gc1.FieldName = "Descripcion"
        gc1.Width = 60
        grvDetalle.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "Cantidad"
        gc2.Caption = "Cantidad"
        gc2.FieldName = "Cantidad"
        gc2.Width = 10
        grvDetalle.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "Precio_Compra"
        gc3.Caption = "Costo_Unitario"
        gc3.FieldName = "Precio_Compra"
        gc3.DisplayFormat.FormatString = "{0:$0.00}"
        gc3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc3.Width = 10
        grvDetalle.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3

        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "Descuento"
        gc4.Caption = "Descuento"
        gc4.FieldName = "Descuento"
        gc4.DisplayFormat.FormatString = "{0:$0.00}"
        gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc4.Width = 10
        grvDetalle.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4
        grvDetalle.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

        grvDetalle.Columns(5).SummaryItem.DisplayFormat = "{0:$0.00}"
        Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
        gc5.Name = "Subtotales"
        gc5.Caption = "Subtotales"
        gc5.FieldName = "Subtotales"
        gc5.DisplayFormat.FormatString = "{0:$0.00}"
        gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc5.Width = 10
        grvDetalle.Columns.Add(gc5)
        gc5.Visible = True
        gc5.VisibleIndex = 5
        grvDetalle.Columns(5).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(5).SummaryItem.DisplayFormat = "{0:$0.00}"

        grvDetalle.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"
        Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
        gc6.Name = "Subtotal_Neto"
        gc6.Caption = "Subtotal_Neto"
        gc6.FieldName = "Subtotal_Neto"
        gc6.DisplayFormat.FormatString = "{0:$0.00}"
        gc6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc6.Width = 10
        grvDetalle.Columns.Add(gc6)
        gc6.Visible = True
        gc6.VisibleIndex = 6
        grvDetalle.Columns(6).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"
    End Sub

    Sub CargarProveedor()
        TblProvBindingSource.DataSource = SQL("Select Codigo_Proveedor,Nombre_Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
        luProveedor.Properties.DataSource = TblProvBindingSource
        luProveedor.Properties.DisplayMember = "Nombre_Proveedor"
        luProveedor.Properties.ValueMember = "Codigo_Proveedor"
        luProveedor.ItemIndex = 0
    End Sub

    Public Sub TazaActual()
        Dim strsql As String = ""

        strsql = "SELECT Tasa_Oficial FROM Tbl_TasasCambio Where fecha = convert(datetime,convert(varchar(8),getdate(),112))"
        Dim tblTaza As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

        If tblTaza.Rows.Count > 0 Then
            txtTazaCambio.Text = tblTaza.Rows(0).Item(0)
        Else
            txtTazaCambio.Text = Format(1, "###,###,###.00")
        End If

    End Sub

    Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        'txtNumDoc.ReadOnly = bIsActivo
        GroupBox2.Enabled = Not bIsActivo
        GroupBox3.Enabled = Not bIsActivo
        luProveedor.Enabled = Not bIsActivo
        deFechaDoc.Enabled = Not bIsActivo
        Fecha_Vencimiento.Enabled = Not bIsActivo
        Fecha_Recepcionado.Enabled = Not bIsActivo
        txtFactura.Enabled = Not bIsActivo
        CkIR.Enabled = Not bIsActivo
        CkIMI.Enabled = Not bIsActivo
        txtPorcIR.Enabled = Not bIsActivo
        txtPorcIMI.Enabled = Not bIsActivo
        cmdAddProveedor.Enabled = Not bIsActivo
        txtDescuentoDos.Enabled = Not bIsActivo
        txtPorcentaje.Enabled = Not bIsActivo
        txtPlazo.Enabled = Not bIsActivo
        txtObservacion.Enabled = Not bIsActivo
        cmbTipExoneracion.Enabled = Not bIsActivo
    End Sub

    Sub LimpiarCampos()
        txtNumDoc.Text = "00000000"
        txtFactura.Text = ""
        rbContado.Checked = True
        deFechaDoc.Value = Now.Date
        Fecha_Vencimiento.Value = Now.Date
        Fecha_Recepcionado.Value = Now.Date
        txtPlazo.Text = 0
        luProveedor.ItemIndex = 0
        cmbTipExoneracion.SelectedIndex = 0
        txtObservacion.Text = ""
        txtSubtotal.Text = FormatNumber(0, 2)
        txtDescuento.Text = FormatNumber(0, 2)
        txtIR.Text = FormatNumber(0, 2)
        txtIMI.Text = FormatNumber(0, 2)
        txtIVA.Text = FormatNumber(0, 2)
        txtTotalNeto.Text = FormatNumber(0, 2)
        txtTotalbruto.Text = FormatNumber(0, 2)
        txtIVAExcento.Text = FormatNumber(0, 2)
        txtDescuentoDos.Text = FormatNumber(0, 2)
        TazaActual()
        grdDetalle.DataSource = Nothing
        lblEstado.Text = "SIN APLICAR"
    End Sub

    Function Nuevo()
        nTipoEdic = 1
        'Panel2.Enabled = True
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        LimpiarCampos()
        txtNumDoc.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        txtNumDoc.Focus()
        CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
    End Function

    Function Modificar()
        If Not lblEstado.Text = "COMPRA APLICADA" Then
            nTipoEdic = 2
            PermitirBotonesEdicion(False)
            DesactivarCampos(False)
            txtNumDoc.Focus()

            If grvDetalle.RowCount <= 0 Then
                txtPorcentaje.Enabled = True
                txtDescuentoDos.Enabled = True
            Else
                txtPorcentaje.Enabled = False
                txtDescuentoDos.Enabled = False
            End If
        Else
            MsgBox("No se puede modificar una compra aplicada.", MsgBoxStyle.Information, "Información")
        End If

    End Function

    Function Eliminar()
        nTipoEdic = 3
        DesactivarCampos(True)
        PermitirBotonesEdicion(False)
    End Function

    Function Guardar()
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        GuardarMasterDoc()
    End Function

    Sub GuardarMasterDoc()
        Dim Resum As String = ""
        Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try

            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            If result = DialogResult.Yes Then
                Resum = MasterDocument.EditarMasterCompraLocal(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue, 1, deFechaDoc.Value, Fecha_Vencimiento.Value, Fecha_Recepcionado.Value, CInt(txtPlazo.Text), CDbl(txtPorcIR.Text), CDbl(txtPorcIMI.Text), txtObservacion.Text.ToUpper, cmbTipExoneracion.SelectedIndex, txtFactura.Text, txtPorcentaje.Checked, txtDescuentoDos.Text, False, IIf(rbCordobas.Checked = True, True, False), CDbl(txtTazaCambio.Text), nTipoEdic)

                If Resum = "OK" Then
                    MsgBox("La operación se realizó de forma exitosa", MsgBoxStyle.Information, "Información")

                    If nTipoEdic = 3 Then
                        LimpiarCampos()
                    End If

                    PermitirBotonesEdicion(True)
                    DesactivarCampos(True)
                    CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
            If NumCatalogo_Ant = 0 Then
            End If
        End Try
    End Sub

    Function Cancelar()
        nTipoEdic = 1
        NumCatalogo = 21
        CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        If grvDetalle.RowCount <= 0 Then
            frmPrincipal.bbiEliminar.Enabled = True
        Else
            frmPrincipal.bbiEliminar.Enabled = False
        End If
    End Function

    Function Buscar()
        frmBusquedaDoc.ShowDialog()
    End Function

    Public Sub Cerrar()
        NumCatalogo = 0
        Me.Dispose()
        Close()
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Sub mostrarCompraDatos()
        frmDetalleCompra.Label8.Visible = True
        frmDetalleCompra.deFechaExp.Visible = True
        frmDetalleCompra.txtPorcentaje.Visible = True
        frmDetalleCompra.txtPorcenDescuento.Visible = True
        frmDetalleCompra.Label10.Visible = True
        frmDetalleCompra.txtDescuento.Visible = True
    End Sub

    Private Sub luProveedor_EditValueChanged(sender As Object, e As EventArgs) Handles luProveedor.EditValueChanged
        CargarDatosProveedor(luProveedor.EditValue)
    End Sub

    Public Sub CargarDatosProveedor(ByVal CodigoProv As String)
        Dim strsqlDatProv As String = ""

        strsqlDatProv = String.Format("SELECT Plazo, Is_IR, Is_IMI, Is_IVA, TasaIR, TasaIMI, TasaIVA FROM Tbl_Proveedor WHERE Codigo_Proveedor='{0}'", CodigoProv)
        Dim tblDatosProdServ As DataTable = SQL(strsqlDatProv, "tblDatosProv", My.Settings.SolIndustrialCNX).Tables(0)
        If tblDatosProdServ.Rows.Count <> 0 Then
            txtPlazo.Text = IIf(IsDBNull(tblDatosProdServ.Rows(0).Item(0)), 0, tblDatosProdServ.Rows(0).Item(0))
            Fecha_Vencimiento.Value = DateAdd(DateInterval.Day, CInt(txtPlazo.Text), deFechaDoc.Value)
            If (IIf(IsDBNull(tblDatosProdServ.Rows(0).Item(1)), 0, tblDatosProdServ.Rows(0).Item(1))) <> 0 Then
                CkIR.Checked = True
                txtPorcIR.Text = tblDatosProdServ.Rows(0).Item(4)
            Else
                CkIR.Checked = False
                txtPorcIR.Text = 0
            End If
            If (IIf(IsDBNull(tblDatosProdServ.Rows(0).Item(2)), 0, tblDatosProdServ.Rows(0).Item(2))) <> 0 Then
                CkIMI.Checked = True
                txtPorcIMI.Text = tblDatosProdServ.Rows(0).Item(5)
            Else
                CkIMI.Checked = False
                txtPorcIMI.Text = 0
            End If

            '------------------- written by bob LOL ----------------------------------3 y 6 

            IsIVA = CBool(tblDatosProdServ.Rows(0).Item(3))
            TasaIva = CDbl(tblDatosProdServ.Rows(0).Item(6))

            '------------------------------------------------------------------------

        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdicDet = 1
        mostrarCompraDatos()
        frmDetalleCompra.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdicDet = 2
        mostrarCompraDatos()
        frmDetalleCompra.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        nTipoEdicDet = 3
        mostrarCompraDatos()
        frmDetalleCompra.ShowDialog()
    End Sub

    Private Sub CkIR_CheckedChanged(sender As Object, e As EventArgs) Handles CkIR.CheckedChanged
        If CkIR.Checked = True Then
            txtPorcIR.ReadOnly = False
            txtPorcIR.Focus()
            txtPorcIR.SelectionStart = 0
            txtPorcIR.SelectionLength = txtPorcIR.TextLength
        Else
            txtPorcIR.Text = 0
            txtPorcIR.ReadOnly = True
        End If
    End Sub

    Private Sub CkIMI_CheckedChanged(sender As Object, e As EventArgs) Handles CkIMI.CheckedChanged
        If CkIMI.Checked = True Then
            txtPorcIMI.ReadOnly = False
            txtPorcIMI.Focus()
            txtPorcIMI.SelectionStart = 0
            txtPorcIMI.SelectionLength = txtPorcIMI.TextLength
        Else
            txtPorcIMI.Text = 0
            txtPorcIMI.ReadOnly = True
        End If
    End Sub

    Private Sub deFechaDoc_ValueChanged(sender As Object, e As EventArgs) Handles deFechaDoc.ValueChanged
        CargarDatosProveedor(luProveedor.EditValue)
    End Sub

    Private Sub txtPlazo_TextChanged(sender As Object, e As EventArgs) Handles txtPlazo.TextChanged
        Fecha_Vencimiento.Value = DateAdd(DateInterval.Day, ChangeToDouble(txtPlazo.Text), deFechaDoc.Value)
    End Sub

    Private Sub cmdAddProveedor_Click(sender As Object, e As EventArgs) Handles cmdAddProveedor.Click
        itemAdd_Ant = luProveedor.EditValue
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 6
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        CargarProveedor()
    End Sub

    Private Sub frmMasterCompraLocal_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 21
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click

        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim Resum As String = ""
        Dim ResumEntrada As String = ""
        Dim CodigoProd As String = ""
        Dim strsql As String = ""
        Dim Cantidad As Integer = 0
        Dim Costo As Double = 0
        Dim Cod_bodega As Integer = 0
        Dim bodega As String = String.Empty

        Try
            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            If result = DialogResult.Yes Then
                If grvDetalle.RowCount > 0 Then
                    For i As Integer = 0 To grvDetalle.DataRowCount - 1

                        CodigoProd = grvDetalle.GetRowCellValue(i, "Codigo")
                        Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                        Costo = grvDetalle.GetRowCellValue(i, "Costo_Unitario")
                        Cod_bodega = grvDetalle.GetRowCellValue(i, "CodBodega")
                        bodega = grvDetalle.GetRowCellValue(i, "Bodega")

                        strsql = String.Format("SELECT Codigo_Movimiento FROM Movimientos_Almacenes WHERE CodigoSucursal = {0} and Bodega = {1} and Tipo_Movimiento = 4 and Codigo_Documento = {2} and Tipo_Documento = 18", My.Settings.Sucursal, Cod_bodega, txtNumDoc.Text)
                        Dim tblCodigo As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

                        If tblCodigo.Rows.Count > 0 Then

                            CodMaxEntrada = tblCodigo.Rows(0).Item(0)

                        Else
                            CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(Cod_bodega, 4), 8)

                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, txtNumDoc.Text, 18, ("ENTRADA A BODEGA DE " + bodega.ToUpper), 0, Date.Now, 4, False, 1, Cod_bodega)
                        End If

                        ''''''''''''''''''''''''''''''''''''''' written by bob ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        Dim bonificacion As Double = grvDetalle.GetRowCellValue(i, "Cant. Bonificada")

                        Dim cantidad_real As Double = bonificacion + Cantidad

                        Dim costo_real As Double = ((Cantidad * Costo) / cantidad_real)

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        Resum = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, CodigoProd, cantidad_real, costo_real, txtFactura.Text, Cod_bodega, 0, 1, 4)

                        '----------------------------------------------------------------------------------------------------------------------------------

                    Next

                    If Resum = "OK" Then
                        Resum = MasterDocument.EditarMasterCompraLocal(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue, 1, deFechaDoc.Value, Fecha_Vencimiento.Value, Fecha_Recepcionado.Value, CInt(txtPlazo.Text), CDbl(txtPorcIR.Text), CDbl(txtPorcIMI.Text), txtObservacion.Text.ToUpper, cmbTipExoneracion.SelectedIndex, txtFactura.Text, txtPorcentaje.Checked, txtDescuentoDos.Text, True, IIf(rbCordobas.Checked = True, True, False), CDbl(txtTazaCambio.Text), 2)
                    End If

                    ' Resum = MasterDocument.EditarMasterPagos(luProveedor.EditValue, 18, txtNumDoc.Text, deFechaDoc.Value, Fecha_Vencimiento.Value, txtNumDoc.Text, 2021, txtSubtotal.Text, txtIR.Text, txtIMI.Text, txtIVA.Text, 0, 0, 0, Date.Now, True, 1)

                    MsgBox("Compra aplicada correctamente", MsgBoxStyle.Information, "Información")
                    CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)

                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    'Private Sub TazaCambio_CheckedChanged(sender As Object, e As EventArgs) Handles rbCordobas.CheckedChanged, rbDolares.CheckedChanged
    '    Dim strsql As String = ""

    '    If rbCordobas.Checked = True Then
    '        txtTazaCambio.Text = Format(1, "###,###,###.00")
    '    Else
    '        strsql = "SELECT Tasa_Oficial FROM Tbl_TasasCambio Where fecha = convert(datetime,convert(varchar(8),getdate(),112))"
    '        Dim tblTaza As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)
    '        txtTazaCambio.Text = tblTaza.Rows(0).Item(0)
    '    End If
    'End Sub

    Private Sub btnTasaCambio_Click(sender As Object, e As EventArgs) Handles btnTasaCambio.Click
        frmSearch.Text = "Taza de Cambio"
        frmSearch.Size = New Size(364, 324)
        frmSearch.ShowDialog()
    End Sub

    '---------------------------------- written by bob ------------------------------------------

    Private Sub validar_fecha_documento()

        Dim recepcion As Date = Fecha_Recepcionado.Value
        Dim vencimiento As Date = Fecha_Vencimiento.Value


        If recepcion.Date > vencimiento.Date Then

            MsgBox("Fecha de vencimiento no puede ser inferior a la fecha de recepcion.", MsgBoxStyle.Critical)

            Fecha_Recepcionado.Value = Date.Now.Date
            Fecha_Vencimiento.Value = Date.Now.Date

        End If


    End Sub

    Private Sub validar_descuento_documento()

        Dim decuento As Double = ChangeToDouble(txtDescuentoDos.Text)

        If decuento > 100 Then txtDescuentoDos.Text = String.Empty


    End Sub

    '--------------------------------------------------------------------------------------------
End Class