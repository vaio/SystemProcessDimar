﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrdenesPago
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrdenesPago))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.gbRetenciones = New System.Windows.Forms.GroupBox()
        Me.txtPorcIR = New System.Windows.Forms.TextBox()
        Me.txtPorcIMI = New System.Windows.Forms.TextBox()
        Me.CkIR = New System.Windows.Forms.CheckBox()
        Me.CkIMI = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbCredito = New System.Windows.Forms.RadioButton()
        Me.rbContado = New System.Windows.Forms.RadioButton()
        Me.txtPlazo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmdAddProveedor = New DevExpress.XtraEditors.SimpleButton()
        Me.deFechaVenc = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.deFechaDoc = New System.Windows.Forms.DateTimePicker()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.luProveedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNumDoc = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.txtSubtotal = New System.Windows.Forms.TextBox()
        Me.Lbl_IR = New System.Windows.Forms.Label()
        Me.txtIMI = New System.Windows.Forms.TextBox()
        Me.txtIR = New System.Windows.Forms.TextBox()
        Me.Lbl_IVA = New System.Windows.Forms.Label()
        Me.Lbl_IMI = New System.Windows.Forms.Label()
        Me.txtTotalNeto = New System.Windows.Forms.TextBox()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.TblProvBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblDetalleDocBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2.SuspendLayout()
        Me.gbRetenciones.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.luProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDetalleDocBS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TextBox1)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.gbRetenciones)
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Controls.Add(Me.txtPlazo)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.cmdAddProveedor)
        Me.Panel2.Controls.Add(Me.deFechaVenc)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.deFechaDoc)
        Me.Panel2.Controls.Add(Me.txtObservacion)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.luProveedor)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtNumDoc)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1113, 146)
        Me.Panel2.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(110, 36)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(106, 22)
        Me.TextBox1.TabIndex = 26
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 42)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(95, 13)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "N° de Documento:"
        '
        'gbRetenciones
        '
        Me.gbRetenciones.Controls.Add(Me.txtPorcIR)
        Me.gbRetenciones.Controls.Add(Me.txtPorcIMI)
        Me.gbRetenciones.Controls.Add(Me.CkIR)
        Me.gbRetenciones.Controls.Add(Me.CkIMI)
        Me.gbRetenciones.Location = New System.Drawing.Point(188, 66)
        Me.gbRetenciones.Name = "gbRetenciones"
        Me.gbRetenciones.Size = New System.Drawing.Size(220, 46)
        Me.gbRetenciones.TabIndex = 6
        Me.gbRetenciones.TabStop = False
        Me.gbRetenciones.Text = "Retenciones"
        '
        'txtPorcIR
        '
        Me.txtPorcIR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcIR.Location = New System.Drawing.Point(49, 18)
        Me.txtPorcIR.Name = "txtPorcIR"
        Me.txtPorcIR.ReadOnly = True
        Me.txtPorcIR.Size = New System.Drawing.Size(37, 22)
        Me.txtPorcIR.TabIndex = 2
        Me.txtPorcIR.Text = "0"
        Me.txtPorcIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPorcIMI
        '
        Me.txtPorcIMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcIMI.Location = New System.Drawing.Point(149, 18)
        Me.txtPorcIMI.Name = "txtPorcIMI"
        Me.txtPorcIMI.ReadOnly = True
        Me.txtPorcIMI.Size = New System.Drawing.Size(37, 22)
        Me.txtPorcIMI.TabIndex = 3
        Me.txtPorcIMI.Text = "0"
        Me.txtPorcIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CkIR
        '
        Me.CkIR.AutoSize = True
        Me.CkIR.Location = New System.Drawing.Point(15, 21)
        Me.CkIR.Name = "CkIR"
        Me.CkIR.Size = New System.Drawing.Size(90, 17)
        Me.CkIR.TabIndex = 0
        Me.CkIR.Text = "IR               %"
        Me.CkIR.UseVisualStyleBackColor = True
        '
        'CkIMI
        '
        Me.CkIMI.AutoSize = True
        Me.CkIMI.Location = New System.Drawing.Point(111, 20)
        Me.CkIMI.Name = "CkIMI"
        Me.CkIMI.Size = New System.Drawing.Size(94, 17)
        Me.CkIMI.TabIndex = 1
        Me.CkIMI.Text = "IMI               %"
        Me.CkIMI.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbCredito)
        Me.GroupBox2.Controls.Add(Me.rbContado)
        Me.GroupBox2.Location = New System.Drawing.Point(17, 66)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(161, 46)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tipo de Compra"
        '
        'rbCredito
        '
        Me.rbCredito.AutoSize = True
        Me.rbCredito.Location = New System.Drawing.Point(93, 19)
        Me.rbCredito.Name = "rbCredito"
        Me.rbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rbCredito.TabIndex = 1
        Me.rbCredito.Text = "Credito"
        Me.rbCredito.UseVisualStyleBackColor = True
        '
        'rbContado
        '
        Me.rbContado.AutoSize = True
        Me.rbContado.Checked = True
        Me.rbContado.Location = New System.Drawing.Point(21, 19)
        Me.rbContado.Name = "rbContado"
        Me.rbContado.Size = New System.Drawing.Size(65, 17)
        Me.rbContado.TabIndex = 0
        Me.rbContado.TabStop = True
        Me.rbContado.Text = "Contado"
        Me.rbContado.UseVisualStyleBackColor = True
        '
        'txtPlazo
        '
        Me.txtPlazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlazo.Location = New System.Drawing.Point(422, 116)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(97, 22)
        Me.txtPlazo.TabIndex = 5
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(419, 99)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Plazo Doc.:"
        '
        'cmdAddProveedor
        '
        Me.cmdAddProveedor.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddProveedor.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmdAddProveedor.Appearance.Options.UseFont = True
        Me.cmdAddProveedor.Appearance.Options.UseForeColor = True
        Me.cmdAddProveedor.Location = New System.Drawing.Point(378, 118)
        Me.cmdAddProveedor.Name = "cmdAddProveedor"
        Me.cmdAddProveedor.Size = New System.Drawing.Size(20, 19)
        Me.cmdAddProveedor.TabIndex = 24
        Me.cmdAddProveedor.Text = "+"
        Me.cmdAddProveedor.ToolTip = "Agregar un nuevo Departamento"
        '
        'deFechaVenc
        '
        Me.deFechaVenc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaVenc.Location = New System.Drawing.Point(301, 36)
        Me.deFechaVenc.Name = "deFechaVenc"
        Me.deFechaVenc.Size = New System.Drawing.Size(97, 20)
        Me.deFechaVenc.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(231, 42)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Vencimiento:"
        '
        'deFechaDoc
        '
        Me.deFechaDoc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaDoc.Location = New System.Drawing.Point(301, 5)
        Me.deFechaDoc.Name = "deFechaDoc"
        Me.deFechaDoc.Size = New System.Drawing.Size(97, 20)
        Me.deFechaDoc.TabIndex = 3
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(422, 22)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(332, 68)
        Me.txtObservacion.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(425, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Observaciones:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(231, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Fecha:"
        '
        'luProveedor
        '
        Me.luProveedor.Location = New System.Drawing.Point(73, 118)
        Me.luProveedor.Name = "luProveedor"
        Me.luProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luProveedor.Size = New System.Drawing.Size(304, 20)
        Me.luProveedor.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 121)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Proveedor:"
        '
        'txtNumDoc
        '
        Me.txtNumDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumDoc.Location = New System.Drawing.Point(109, 6)
        Me.txtNumDoc.Name = "txtNumDoc"
        Me.txtNumDoc.Size = New System.Drawing.Size(107, 22)
        Me.txtNumDoc.TabIndex = 0
        Me.txtNumDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "N° Oden de Pago:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 532)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1113, 49)
        Me.Panel1.TabIndex = 20
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(1015, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(931, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel4.Location = New System.Drawing.Point(937, 146)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(176, 386)
        Me.Panel4.TabIndex = 23
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.Controls.Add(Me.txtSubtotal)
        Me.Panel5.Controls.Add(Me.Lbl_IR)
        Me.Panel5.Controls.Add(Me.txtIMI)
        Me.Panel5.Controls.Add(Me.txtIR)
        Me.Panel5.Controls.Add(Me.Lbl_IVA)
        Me.Panel5.Controls.Add(Me.Lbl_IMI)
        Me.Panel5.Controls.Add(Me.txtTotalNeto)
        Me.Panel5.Controls.Add(Me.txtIVA)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.txtDescuento)
        Me.Panel5.Controls.Add(Me.Label12)
        Me.Panel5.Controls.Add(Me.Label9)
        Me.Panel5.Location = New System.Drawing.Point(3, 231)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(164, 152)
        Me.Panel5.TabIndex = 13
        '
        'txtSubtotal
        '
        Me.txtSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubtotal.Location = New System.Drawing.Point(73, 3)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.ReadOnly = True
        Me.txtSubtotal.Size = New System.Drawing.Size(87, 22)
        Me.txtSubtotal.TabIndex = 1
        Me.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Lbl_IR
        '
        Me.Lbl_IR.AutoSize = True
        Me.Lbl_IR.Location = New System.Drawing.Point(3, 56)
        Me.Lbl_IR.Name = "Lbl_IR"
        Me.Lbl_IR.Size = New System.Drawing.Size(21, 13)
        Me.Lbl_IR.TabIndex = 12
        Me.Lbl_IR.Text = "IR:"
        '
        'txtIMI
        '
        Me.txtIMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIMI.Location = New System.Drawing.Point(73, 76)
        Me.txtIMI.Name = "txtIMI"
        Me.txtIMI.ReadOnly = True
        Me.txtIMI.Size = New System.Drawing.Size(87, 22)
        Me.txtIMI.TabIndex = 5
        Me.txtIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIR
        '
        Me.txtIR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIR.Location = New System.Drawing.Point(73, 51)
        Me.txtIR.Name = "txtIR"
        Me.txtIR.ReadOnly = True
        Me.txtIR.Size = New System.Drawing.Size(87, 22)
        Me.txtIR.TabIndex = 11
        Me.txtIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Lbl_IVA
        '
        Me.Lbl_IVA.AutoSize = True
        Me.Lbl_IVA.Location = New System.Drawing.Point(3, 106)
        Me.Lbl_IVA.Name = "Lbl_IVA"
        Me.Lbl_IVA.Size = New System.Drawing.Size(27, 13)
        Me.Lbl_IVA.TabIndex = 8
        Me.Lbl_IVA.Text = "IVA:"
        '
        'Lbl_IMI
        '
        Me.Lbl_IMI.AutoSize = True
        Me.Lbl_IMI.Location = New System.Drawing.Point(3, 81)
        Me.Lbl_IMI.Name = "Lbl_IMI"
        Me.Lbl_IMI.Size = New System.Drawing.Size(25, 13)
        Me.Lbl_IMI.TabIndex = 6
        Me.Lbl_IMI.Text = "IMI:"
        '
        'txtTotalNeto
        '
        Me.txtTotalNeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalNeto.Location = New System.Drawing.Point(73, 126)
        Me.txtTotalNeto.Name = "txtTotalNeto"
        Me.txtTotalNeto.ReadOnly = True
        Me.txtTotalNeto.Size = New System.Drawing.Size(87, 22)
        Me.txtTotalNeto.TabIndex = 9
        Me.txtTotalNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIVA
        '
        Me.txtIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIVA.Location = New System.Drawing.Point(73, 101)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.ReadOnly = True
        Me.txtIVA.Size = New System.Drawing.Size(87, 22)
        Me.txtIVA.TabIndex = 7
        Me.txtIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Subtotal:"
        '
        'txtDescuento
        '
        Me.txtDescuento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescuento.Location = New System.Drawing.Point(73, 27)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.ReadOnly = True
        Me.txtDescuento.Size = New System.Drawing.Size(87, 22)
        Me.txtDescuento.TabIndex = 3
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(3, 131)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 13)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Total Neto:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Descuento:"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 146)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(937, 386)
        Me.GridControl1.TabIndex = 25
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsCustomization.AllowColumnMoving = False
        Me.GridView1.OptionsCustomization.AllowColumnResizing = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'frmOrdenesPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1113, 581)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "frmOrdenesPago"
        Me.Text = ".:::.  Ordenes de Pago  .:::."
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.gbRetenciones.ResumeLayout(False)
        Me.gbRetenciones.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.luProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblProvBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDetalleDocBS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents gbRetenciones As System.Windows.Forms.GroupBox
    Friend WithEvents txtPorcIR As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcIMI As System.Windows.Forms.TextBox
    Friend WithEvents CkIR As System.Windows.Forms.CheckBox
    Friend WithEvents CkIMI As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rbContado As System.Windows.Forms.RadioButton
    Friend WithEvents txtPlazo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmdAddProveedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFechaVenc As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents deFechaDoc As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents luProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNumDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_IR As System.Windows.Forms.Label
    Friend WithEvents txtIMI As System.Windows.Forms.TextBox
    Friend WithEvents txtIR As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_IVA As System.Windows.Forms.Label
    Friend WithEvents Lbl_IMI As System.Windows.Forms.Label
    Friend WithEvents txtTotalNeto As System.Windows.Forms.TextBox
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblProvBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDetalleDocBS As System.Windows.Forms.BindingSource
End Class
