﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors.Controls

Public Class FrmVasignacion
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    '-----
    Dim Codigo As Integer
    Dim Id_Coordinador As Integer = 0
    Dim Id_Supervisor As Integer = 0
    Dim Id_Vehiculo As Integer = 0
    Dim FechaI As DateTime

    Private Sub FrmVasignacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        With frmVehiculo.GridView1
            Codigo = .GetRowCellValue(.GetSelectedRows(0), "Codigo").ToString()
            txtcodigo.Text = Codigo
        End With

        Carga_Coordinador()
        Carga_Supervisor()
        Carga_Vehiculo()

        Cargar_Datos()

    End Sub
    Sub Cargar_Datos()

        Dim dt As DataTable = New DataTable()

        sqlstring = "Select * from vw_Vehiculo_Asignacion_Inicial where Id_Ruta= " & Codigo & ""
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows

                If row0("Codigo") <> "-" Then Id_Vehiculo = row0("Codigo") : luVehiculo.EditValue = Id_Vehiculo
                If row0("Id_Coordinador") <> "-" Then Id_Coordinador = row0("Id_Coordinador") : luCoordinador.EditValue = Id_Coordinador
                If row0("Id_Supervisor") <> "-" Then Id_Supervisor = row0("Id_Supervisor") : luSupervisor.EditValue = Id_Supervisor
                If row0("Activo") = True Then txtEstado.Text = ("Activo") : ckActivo.Checked = True Else txtEstado.Text = ("No Activo")
                'If row0("Fecha") <> "-" Then FechaI = row0("Fecha")

                txtNombre.Text = row0("Ruta")
                txtRuta.Text = row0("Ruta")
                'txtcodigo.Text = Codigo
                txtSucursal.Text = row0("Sucursal")
                txtCoordinador.Text = row0("Coordinador")
                txtsupervisor.Text = row0("Supervisor")

            Next
            con.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            con.Close()
        End Try

    End Sub
    Sub Carga_Coordinador()

        Try
            luCoordinador.Properties.DataSource = SQL("select Id_Empleado as 'Codigo',Nombres+' '+Apellidos as 'Nombre' from Tbl_Empleados where Id_Cargo=21 and Activo=1", "tbl_Coordinador", My.Settings.SolIndustrialCNX).Tables(0)

            luCoordinador.Properties.DisplayMember = "Nombre"
            luCoordinador.Properties.ValueMember = "Codigo"
            luCoordinador.Properties.PopulateColumns()
            luCoordinador.Properties.Columns("Codigo").Visible = False

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try

    End Sub
    Sub Carga_Supervisor()

        Try
            luSupervisor.Properties.DataSource = SQL("select Id_Empleado as 'Codigo', Nombres+' '+Apellidos  as 'Nombre' from Tbl_Empleados where Id_Cargo=6 and Activo=1", "tbl_Supervisor", My.Settings.SolIndustrialCNX).Tables(0)

            luSupervisor.Properties.DisplayMember = "Nombre"
            luSupervisor.Properties.ValueMember = "Codigo"
            luSupervisor.Properties.PopulateColumns()
            luSupervisor.Properties.Columns("Codigo").Visible = False

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try

    End Sub
    Sub Carga_Vehiculo()

        Try
            luVehiculo.Properties.DataSource = SQL("select Id_Vehiculo as 'Codigo', Placa, Marca, Modelo from Tbl_Vehiculo where Estado=1 and Tipo=1", "Tbl_Vehiculo", My.Settings.SolIndustrialCNX).Tables(0)
            luVehiculo.Properties.DisplayMember = "Placa"
            luVehiculo.Properties.ValueMember = "Codigo"
            luVehiculo.Properties.PopulateColumns()
            luVehiculo.Properties.Columns("Codigo").Visible = False

        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim resum As String
        resum = frmVehiculo.EditarAsignacion(frmVehiculo.Edicion, luVehiculo.EditValue, luCoordinador.EditValue, luSupervisor.EditValue, txtcodigo.Text, txtNombre.Text, ckActivo.Checked)
        If resum = "OK" Then

            MsgBox("Cambios guardados")
            con.Close()
            Me.Dispose()
            Me.Close()

        End If

    End Sub

    Private Sub FrmVasignacion_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Dispose()
        Me.Close()
    End Sub


    Private Sub luVehiculo_EditValueChanged(sender As Object, e As EventArgs) Handles luVehiculo.EditValueChanged

        Codigo = luVehiculo.EditValue

    End Sub
End Class