﻿
Imports System.Data.SqlClient

Public Class frmBusquedaProd

    Private Sub frmBusquedaProd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarProductos()
    End Sub

    Sub CargarProductos()
        Dim strsql As String = ""
        
        If NumCatalogo = 27 Then
            strsql = "Select Codigo_Producto,Nombre_Producto,dbo.getCostoProducto(Codigo_Producto) As Costo_Promedio from Productos where Descontinuado=0 and Id_ProductosTipo in (1,2)"
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            GridView1.Columns("Codigo_Producto").Width = 100
            GridView1.Columns("Nombre_Producto").Width = 400
            GridView1.Columns("Costo_Promedio").Width = 100
            GridControl1.BringToFront()

        ElseIf NumCatalogo = 23 Then
            strsql = "select INV.Codigo_Producto 'Codigo',PRD.Nombre_Producto  'Descripcion', cast(INV.Costo_Promedio_Actual as decimal (18,4)) 'C. Promedio', (INV.UndTotal_Existencia- isnull(OPD.Cantidad_Requerida,0)) 'Existencia', MED.Descripcion_Unidad 'Unidad Medida' from Inventario INV inner join Productos PRD on PRD.Codigo_Producto=INV.Codigo_Producto and INV.Codigo_Bodega=3 inner join Unidades_Medida MED on MED.Codigo_UnidadMed=PRD.Codigo_UnidMedida left join Orden_Produccion_Detalle OPD on OPD.Cod_Producto_Detalle=INV.Codigo_Producto left join Orden_Produccion OP on OP.CodigoSucursal=OPD.CodigoSucursal and OP.Codigo_Orden_Produc=OPD.Codigo_Orden_Produc and OP.Estado='2024'"
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            GridControl1.BringToFront()
            GridView1.BestFitColumns()
            GridView1.Columns("Descripcion").BestFit()

        ElseIf NumCatalogo = 32 Then
            strsql = "select * from vvRemision_Producto"
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            With GridView1
                'EN EL DISEÑADOR, FEATURE BROWSER. COLUMN LAYOUT: AUTO =TRUE    
                .Columns("Nombre").BestFit()
            End With
        ElseIf NumCatalogo = 38 Then
            strsql = "Select Codigo_Producto,Nombre_Producto,dbo.getCostoProducto(Codigo_Producto) As Costo_Promedio from Productos where Descontinuado=0 and Id_ProductosTipo in (0,1)"
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            GridView1.Columns("Codigo_Producto").Width = 100
            GridView1.Columns("Nombre_Producto").Width = 400
            GridView1.Columns("Costo_Promedio").Width = 100
            GridControl1.BringToFront()
        ElseIf NumCatalogo = 39 Then
            strsql = "SELECT Prd.Codigo_Producto, Prd.Nombre_Producto, dbo.getCostoProducto(Prd.Codigo_Producto) As Costo_Promedio FROM Productos Prd "
            strsql = strsql & "INNER JOIN Inventario Inv ON Prd.Codigo_Producto = Inv.Codigo_Producto WHERE Inv.UndTotal_Existencia > 0 and Inv.Codigo_Bodega = " & frmTraslados.cmbBdOrigen.SelectedValue
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            GridView1.Columns("Codigo_Producto").Width = 100
            GridView1.Columns("Nombre_Producto").Width = 400
            GridView1.Columns("Costo_Promedio").Width = 100
            GridControl1.BringToFront()
        ElseIf NumCatalogo = 23 Then
            strsql = "Select Codigo_Producto,Nombre_Producto,dbo.getCostoProducto(Codigo_Producto) As Costo_Promedio from Productos where Descontinuado=0 "
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            GridView1.Columns("Codigo_Producto").Width = 100
            GridView1.Columns("Nombre_Producto").Width = 400
            GridView1.Columns("Costo_Promedio").Width = 100
            GridControl1.BringToFront()
        Else
            strsql = "Select Codigo_Producto,Nombre_Producto,dbo.getCostoProducto(Codigo_Producto) As Costo_Promedio from Productos where Descontinuado=0 "
            GridControl1.DataSource = SQL(strsql, "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
            GridView1.Columns("Codigo_Producto").Width = 100
            GridView1.Columns("Nombre_Producto").Width = 400
            GridView1.Columns("Costo_Promedio").Width = 100
            GridControl1.BringToFront()
        End If
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        'pendiente
        Try
            FrmRemision.codigo = ""
        Catch ex As Exception
        End Try
        Cerrar()
    End Sub
    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub
    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Aceptar()
    End Sub

    Sub Aceptar()
        Dim Codigo As String = ""
        Try
            If GridView1.RowCount > 0 Then
                If NumCatalogo = 21 Or NumCatalogo = 22 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
                    frmDetalleCompra.txtCodigoProd.Text = Codigo
                ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
                    frmDetalleEntradaSalida.txtCodigoProd.Text = Codigo
                    frmDetalleEntradaSalida.CargarDatosProducto(Codigo)
                ElseIf NumCatalogo = 23 Then
                    'Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
                    'With frmAdicional
                    '    .txtCodigoProd.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                    '    .lblDescripcion.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Descripcion").ToString()
                    '    .lblCostoUnitario.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "C. Promedio").ToString()
                    '    .lblExistencia.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Existencia").ToString()
                    'End With
                    'frmAdicional.txtCodigoProd.Text = Codigo
                ElseIf NumCatalogo = 27 Then
                    'Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
                    With frmAdicional
                        'GridView1.Columns("Codigo_Producto").Width = 100
                        'GridView1.Columns("Nombre_Producto").Width = 400
                        'GridView1.Columns("Costo_Promedio").Width = 100

                        .txtCodigoProd.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
                        .lblDescripcion.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre_Producto").ToString()
                        .lblCostoUnitario.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Costo_Promedio").ToString()
                        '.lblExistencia.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Existencia").ToString()
                    End With
                ElseIf NumCatalogo = 32 Then
                    With FrmRemision
                        .codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                        .Producto = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre").ToString()
                        .Costo_Promedio = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "C. Promedio").ToString()
                        .Existencia = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Existencias").ToString()
                        If GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Precio") > 0 Then
                            .btnRmas.BackColor = Color.LightGreen
                            .btnRmenos.Text = "Canelar"
                            .btnRmenos.BackColor = Color.MistyRose
                            .txtCantidad.Enabled = True
                            .txtCantidad.Focus()
                            .txtCantidad.Text = 1
                            .txtCantidad.SelectAll()
                            .Producto_Nuevo = True
                        Else
                            MsgBox("El Articulo no tiene precio")
                            Exit Sub
                        End If
                    End With
                ElseIf NumCatalogo = 38 Or NumCatalogo = 39 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo_Producto").ToString()
                    frmAdicional.txtCodigoProd.Text = Codigo
                End If

                Cerrar()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick, gridRemision.DoubleClick
        Aceptar()
    End Sub
End Class