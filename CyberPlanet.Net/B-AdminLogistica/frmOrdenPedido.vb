﻿Imports System.Xml
Public Class frmOrdenPedido
    'Public CodigoCompra As String = ""
    Public PorcenDes As Double = 0
    Dim CodMaxEntrada As String = ""
    Dim CodEntrada As String = ""
    Dim Referencia As String

    Private Sub frmMasterCompraLocal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NumCatalogo = 38
        CargarProveedor()
        CargarEstados()
        DesactivarCampos(True)
        txtNumDoc.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        LimpiarCampos()
    End Sub

    Sub CargarDatos(ByVal Codigo As String)
        Dim strSqlDoc As String

        strSqlDoc = "SELECT Codigo_Proveedor, Concepto, cast(Fecha as date) as 'Fecha', Taza_Cambio, Estado, isnull(Referencia,'') as 'Referencia' FROM Orden_Pedido WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Pedido = " & Codigo
        Dim tblDatos As DataTable = SQL(strSqlDoc, "tblDatosMasterDoc", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatos.Rows.Count > 0 Then
            txtNumDoc.Text = Codigo
            luProveedor.EditValue = tblDatos.Rows(0).Item("Codigo_Proveedor")
            deFechaDoc.Value = tblDatos.Rows(0).Item("Fecha")
            txtObservacion.Text = tblDatos.Rows(0).Item("Concepto")
            txtTazaCambio.Text = tblDatos.Rows(0).Item("Taza_Cambio")
            Referencia = tblDatos.Rows(0).Item("Referencia")
            cmbEstado.SelectedValue = tblDatos.Rows(0).Item("Estado")

            If Not tblDatos.Rows(0).Item("Estado") = 3042 And Not tblDatos.Rows(0).Item("Estado") = 3048 Then
                Panel1.Enabled = False
            Else
                Panel1.Enabled = True
            End If

            CargarDetalle(Codigo)

        Else
            LimpiarCampos()
            txtNumDoc.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
            txtNumDoc.Focus()
            btnAgregar.Enabled = False
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If

        'If Referencia <> "" Then Panel1.Enabled = False : lbEstado.Visible = True : lbReferencia.Text = Referencia : lbReferencia.Visible = True Else Panel1.Enabled = True : lbEstado.Visible = False : lbReferencia.Text = Referencia : lbReferencia.Visible = False
    End Sub

    Sub CargarDetalle(ByVal sNumDoc As String)
        Dim strsql As String = ""
        Dim SubTotal As Double = 0

        strsql = "SELECT Det.Codigo_Producto As 'Codigo', Prd.Nombre_Producto As 'Producto', Und.Descripcion_Unidad As 'Medida', Det.Cant_Requerida As 'Cantidad', Det.Costo_Unitario As 'Costo Unitario', " & _
                 "Det.Costo_Total As 'Costo Total' FROM Orden_Pedido_Detalle Det INNER JOIN Productos Prd ON Det.Codigo_Producto = Prd.Codigo_Producto INNER JOIN Unidades_Medida Und ON Prd.Codigo_UnidMedida = Und.Codigo_UnidadMed " & _
                 "WHERE CodigoSucursal = " & My.Settings.Sucursal & " AND Codigo_Pedido = " & sNumDoc

        Dim TableDatos As DataTable = SQL(strsql, "tblDatosDetalle", My.Settings.SolIndustrialCNX).Tables(0)
        grdDetalle.DataSource = TableDatos

        'grvDetalle.Columns("Codigo").Width = 75
        'grvDetalle.Columns("Producto").Width = 520

        'grvDetalle.Columns("Medida").Width = 90
        'grvDetalle.Columns("Cantidad").Width = 90

        'grvDetalle.Columns("Costo Unitario").Width = 110
        grvDetalle.Columns("Costo Unitario").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Costo Unitario").DisplayFormat.FormatString = "{0:C$ 0.0000}"

        'grvDetalle.Columns("Costo Total").Width = 110
        grvDetalle.Columns("Costo Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvDetalle.Columns("Costo Total").DisplayFormat.FormatString = "{0:C$ 0.0000}"

        grvDetalle.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        grvDetalle.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:C$ 0.0000}"
        grvDetalle.OptionsView.ShowFooter = True

        grvDetalle.BestFitColumns()

        If grvDetalle.RowCount > 0 Then
            For Each dr As DataRow In TableDatos.Rows
                SubTotal = SubTotal + CDbl(dr.Item("Costo Total"))
            Next
        End If

        txtSubtotal.Text = SubTotal
        txtSubtotalDol.Text = Math.Round(SubTotal / CDbl(txtTazaCambio.Text), 4)

        If grvDetalle.RowCount <= 0 Then
            frmPrincipal.bbiEliminar.Enabled = True
            btnAgregar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        Else
            frmPrincipal.bbiEliminar.Enabled = False
            btnAgregar.Enabled = True
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If

    End Sub

    Sub CargarProveedor()
        TblProvBindingSource.DataSource = SQL("Select Codigo_Proveedor,Nombre_Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
        luProveedor.Properties.DataSource = TblProvBindingSource
        luProveedor.Properties.DisplayMember = "Nombre_Proveedor"
        luProveedor.Properties.ValueMember = "Codigo_Proveedor"
        luProveedor.ItemIndex = 0
    End Sub

    Sub CargarEstados()
        cmbEstado.DataSource = SQL("SELECT  IdCategoriaDetalle, Estado FROM Categorias_Detalle WHERE CodCategoria = '08' and Activo = 1 ORDER BY Orden", "tblEstado", My.Settings.SolIndustrialCNX).Tables(0)
        cmbEstado.DisplayMember = "Estado"
        cmbEstado.ValueMember = "IdCategoriaDetalle"
        cmbEstado.SelectedIndex = 0
    End Sub

    Public Sub TazaActual()
        Dim strsql = "SELECT Tasa_Oficial FROM Tbl_TasasCambio Where fecha = cast('" + deFechaDoc.Value + "' as date)"

        Dim tblTaza As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

        With txtTazaCambio

            If tblTaza.Rows.Count > 0 Then

                txtTazaCambio.Text = tblTaza.Rows(0).Item(0)
                .BackColor = Color.White

            Else

                txtTazaCambio.Text = "0.0"
                .BackColor = Color.Coral

            End If

        End With

    End Sub

    Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        'txtNumDoc.ReadOnly = bIsActivo
        ' If cmbEstado.SelectedValue = "3048" Then luProveedor.Enabled = True Else luProveedor.Enabled = False
        luProveedor.Enabled = Not bIsActivo
        deFechaDoc.Enabled = Not bIsActivo
        cmdAddProveedor.Enabled = Not bIsActivo
        txtObservacion.Enabled = Not bIsActivo
        cmbEstado.Enabled = Not bIsActivo
    End Sub

    Sub LimpiarCampos()
        deFechaDoc.Value = Now.Date
        luProveedor.ItemIndex = 0
        txtObservacion.Text = ""
        TazaActual()
        cmbEstado.SelectedIndex = 0
        grdDetalle.DataSource = Nothing
        txtSubtotal.Text = Format(0, "###,###,##0.00")
    End Sub

    Function Nuevo()
        nTipoEdic = 1
        'Panel2.Enabled = True
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        LimpiarCampos()
        txtNumDoc.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        txtNumDoc.Focus()
        'CargarDetalleDoc(txtNumDoc.Text)
    End Function

    Function Modificar()
        'If Not lblEstado.Text = "COMPRA APLICADA" Then
        nTipoEdic = 2
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        txtNumDoc.Focus()
        'Else
        '    MsgBox("No se puede modificar una compra aplicada.", MsgBoxStyle.Information, "Información")
        'End If
    End Function

    Function Eliminar()
        nTipoEdic = 3
        DesactivarCampos(True)
        PermitirBotonesEdicion(False)
    End Function

    Function Guardar()
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        GuardarMasterDoc()
    End Function

    Sub GuardarMasterDoc()
        Dim Resum As String = ""
        Dim produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try

            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            If result = DialogResult.Yes Then
                Resum = Inventario.EditarPedido(My.Settings.Sucursal, txtNumDoc.Text, luProveedor.EditValue, txtObservacion.Text, deFechaDoc.Value, CDbl(txtTazaCambio.Text), cmbEstado.SelectedValue, nTipoEdic, Nothing)

                If Resum = "OK" Then
                    MsgBox("La operación se realizó de forma exitosa", MsgBoxStyle.Information, "Información")

                    If nTipoEdic = 3 Then
                        LimpiarCampos()
                    End If

                    PermitirBotonesEdicion(True)
                    DesactivarCampos(True)

                    CargarDatos(txtNumDoc.Text)
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If
            End If
        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)

        Finally
            If NumCatalogo_Ant = 0 Then
            End If
        End Try
    End Sub

    Function Cancelar()
        nTipoEdic = 1
        NumCatalogo = 38
        'CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, luProveedor.EditValue)
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
        If grvDetalle.RowCount <= 0 Then
            frmPrincipal.bbiEliminar.Enabled = True
        Else
            frmPrincipal.bbiEliminar.Enabled = False
        End If
    End Function

    Public Sub Cerrar()
        NumCatalogo = 0
        Me.Dispose()
        Close()
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
        frmPrincipal.bbiCerrar.Enabled = bEstado
    End Sub

    'Sub mostrarCompraDatos()
    '    frmDetalleCompra.Label8.Visible = True
    '    frmDetalleCompra.deFechaExp.Visible = True
    '    frmDetalleCompra.txtPorcentaje.Visible = True
    '    frmDetalleCompra.txtPorcenDescuento.Visible = True
    '    frmDetalleCompra.Label10.Visible = True
    '    frmDetalleCompra.txtDescuento.Visible = True
    'End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdicDet = 1
        frmAdicional.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdicDet = 2
        frmAdicional.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim Resum As String = ""
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        nTipoEdicDet = 3

        Try
            Dim result As Integer = MessageBox.Show("Desea eliminar el registro seleccionado?", "Confirmación de Eliminación", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Dim CodigoProducto As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
                Dim Cantidad As Integer = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad")
                Dim Costo As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Unitario")
                Dim CostoTotal As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Total")

                Resum = Inventario.EditarPedidoDeta(My.Settings.Sucursal, txtNumDoc.Text, CodigoProducto, 0, CDbl(Cantidad), CDbl(Costo), CDbl(CostoTotal), nTipoEdicDet)

                If Resum = "OK" Then
                    CargarDetalle(txtNumDoc.Text)
                End If

            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    'Private Sub txtPlazo_TextChanged(sender As Object, e As EventArgs) Handles txtPlazo.TextChanged
    '    deFechaVenc.Value = DateAdd(DateInterval.Day, CInt(txtPlazo.Text), deFechaDoc.Value)
    'End Sub

    Private Sub cmdAddProveedor_Click(sender As Object, e As EventArgs) Handles cmdAddProveedor.Click
        itemAdd_Ant = luProveedor.EditValue
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 6
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
        CargarProveedor()
    End Sub

    Private Sub frmMasterCompraLocal_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 38
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim MasterDocument As New clsDocumentos(My.Settings.SolIndustrialCNX)
        Dim Resum As String = ""
        Dim ResumEntrada As String = ""
        Dim CodigoProd As String = ""
        Dim strsql As String = ""
        Dim Cantidad As Integer = 0
        Dim Costo As Double = 0
        Dim Cod_bodega As Integer = 0

        Try
            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

            If result = DialogResult.Yes Then
                If grvDetalle.RowCount > 0 Then
                    For i As Integer = 0 To grvDetalle.DataRowCount - 1
                        CodigoProd = grvDetalle.GetRowCellValue(i, "Codigo")
                        Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                        Costo = grvDetalle.GetRowCellValue(i, "Costo_Unitario")
                        Cod_bodega = grvDetalle.GetRowCellValue(i, "CodBodega")

                        strsql = "SELECT Codigo_Movimiento FROM Movimientos_Almacenes WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Bodega = " & Cod_bodega & " and Tipo_Movimiento = 4 and Codigo_Documento = " & txtNumDoc.Text & " and Tipo_Documento = 18"
                        Dim tblCodigo As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

                        If tblCodigo.Rows.Count > 0 Then
                            CodMaxEntrada = tblCodigo.Rows(0).Item(0)
                        Else
                            CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(Cod_bodega, 4), 8)

                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, txtNumDoc.Text, 18, ("ENTRADA A BODEGA DE MATERIA PRIMA"), 0, Date.Now, 4, False, 1, Cod_bodega)
                        End If

                        'Resum = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, CodigoProd, Cantidad, Costo, txtFactura.Text, Cod_bodega, 0, 1, 4)

                    Next

                    If Resum = "OK" Then
                        'Resum = MasterDocument.EditarMasterCompraLocal(My.Settings.Sucursal, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue, 1, deFechaDoc.Value, deFechaVenc.Value, deRecepcionado.Value, CInt(txtPlazo.Text), CDbl(txtPorcIR.Text), CDbl(txtPorcIMI.Text), txtObservacion.Text.ToUpper, cmbTipExoneracion.SelectedIndex, txtFactura.Text, txtPorcentaje.Checked, txtDescuentoDos.Text, True, IIf(rbCordobas.Checked = True, True, False), CDbl(txtTazaCambio.Text), 2)
                    End If

                    'Resum = MasterDocument.EditarMasterPagos(luProveedor.EditValue, 18, txtNumDoc.Text, deFechaDoc.Value, deFechaVenc.Value, txtNumDoc.Text, 2021, txtSubtotal.Text, txtIR.Text, txtIMI.Text, txtIVA.Text, 0, 0, Date.Now, True, 1)

                    MsgBox("Compra aplicada correctamente", MsgBoxStyle.Information, "Información")
                    'CargarDetalleDoc(My.Settings.Sucursal, txtNumDoc.Text, luProveedor.EditValue)

                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    'Private Sub TazaCambio_CheckedChanged(sender As Object, e As EventArgs) Handles rbCordobas.CheckedChanged, rbDolares.CheckedChanged
    '    Dim strsql As String = ""

    '    If rbCordobas.Checked = True Then
    '        txtTazaCambio.Text = Format(1, "###,###,###.00")
    '    Else
    '        strsql = "SELECT Tasa_Oficial FROM Tbl_TasasCambio Where fecha = convert(datetime,convert(varchar(8),getdate(),112))"
    '        Dim tblTaza As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)
    '        txtTazaCambio.Text = tblTaza.Rows(0).Item(0)
    '    End If
    'End Sub

    Private Sub btnTasaCambio_Click(sender As Object, e As EventArgs)
        frmSearch.Text = "Taza de Cambio"
        frmSearch.Size = New Size(364, 324)
        frmSearch.ShowDialog()
    End Sub

    Private Sub txtSubtotal_TextChanged(sender As Object, e As EventArgs) Handles txtSubtotal.TextChanged
        If IsNumeric(txtSubtotal.Text) = True Then
            txtSubtotalDol.Text = Format((txtSubtotal.Text / txtTazaCambio.Text), "###,###,##0.00")
        End If
    End Sub

    Private Sub luProveedor_EditValueChanged(sender As Object, e As EventArgs) Handles luProveedor.EditValueChanged
        Remitente = luProveedor.Text

    End Sub

    Private Sub deFechaDoc_ValueChanged(sender As Object, e As EventArgs) Handles deFechaDoc.ValueChanged
        TazaActual()
    End Sub
End Class