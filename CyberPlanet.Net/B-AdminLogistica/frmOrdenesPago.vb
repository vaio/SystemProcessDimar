﻿Public Class frmOrdenesPago


Private Sub frmOrdenesPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
      LimpiarCampos()
      CargarProveedor()
      'Panel2.Enabled = False
      CargarDetalleDoc(1, txtNumDoc.Text, IIf(rbContado.Checked = True, 1, 2), luProveedor.EditValue)
      DesactivarCampos(True)
End Sub

    Sub LimpiarCampos()
      txtNumDoc.Text = "0000000000"
      rbContado.Checked = True
      deFechaDoc.Value = Now.Date
      deFechaVenc.Value = Now.Date
      txtPlazo.Text = 0
      luProveedor.ItemIndex = 0
      txtObservacion.Text = ""
      txtSubtotal.Text = Format(0, "###,###,###.00")
      txtDescuento.Text = Format(0, "###,###,###.00")
      txtIR.Text = Format(0, "###,###,###.00")
      txtIMI.Text = Format(0, "###,###,###.00")
      txtIVA.Text = Format(0, "###,###,###.00")
      txtTotalNeto.Text = Format(0, "###,###,###.00")
    End Sub

    Sub CargarProveedor()
        TblProvBindingSource.DataSource = SQL("Select Codigo_Proveedor,Nombre_Proveedor from Tbl_Proveedor order by Codigo_Proveedor", "tblProveedor", My.Settings.SolIndustrialCNX).Tables(0)
        luProveedor.Properties.DataSource = TblProvBindingSource
        luProveedor.Properties.DisplayMember = "Nombre_Proveedor"
        luProveedor.Properties.ValueMember = "Codigo_Proveedor"
        luProveedor.ItemIndex = 0
    End Sub

    Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        txtNumDoc.ReadOnly = bIsActivo
        GroupBox2.Enabled = Not bIsActivo
        luProveedor.Enabled = Not bIsActivo
        deFechaDoc.Enabled = Not bIsActivo
        deFechaVenc.Enabled = Not bIsActivo
        txtPlazo.ReadOnly = bIsActivo
        txtObservacion.ReadOnly = bIsActivo
    End Sub

    Sub CargarDetalleDoc(ByVal nSucursal As Integer, ByVal sNumDoc As String, ByVal nTipoCompra As Integer, ByVal sProveedor As String)
        Dim strSqlDoc As String
        strSqlDoc = "Select * from Compras where CodigoSucursal = " & nSucursal & " and Numero_de_Compra = '" & sNumDoc & "' and Tipo_Compra = " & nTipoCompra & " and Codigo_Proveedor = '" & sProveedor & "'"
        Dim tblCodProv As DataTable = SQL(strSqlDoc, "tblDatosMasterDoc", My.Settings.SolIndustrialCNX).Tables(0)

        If tblCodProv.Rows.Count > 0 Then
              txtNumDoc.Text = tblCodProv.Rows(0).Item(1)
              If tblCodProv.Rows(0).Item(2) = 1 Then : rbContado.Checked = True
              Else : rbCredito.Checked = True
              End If
              luProveedor.EditValue = tblCodProv.Rows(0).Item(3)
              deFechaDoc.Value = tblCodProv.Rows(0).Item(5)
              deFechaVenc.Value = tblCodProv.Rows(0).Item(6)
              txtPlazo.Text = tblCodProv.Rows(0).Item(7)
              txtObservacion.Text = tblCodProv.Rows(0).Item(21)
              txtSubtotal.Text = tblCodProv.Rows(0).Item(8)
              txtDescuento.Text = tblCodProv.Rows(0).Item(9)
              txtIR.Text = tblCodProv.Rows(0).Item(12)
              txtIMI.Text = tblCodProv.Rows(0).Item(13)
              txtIVA.Text = tblCodProv.Rows(0).Item(14)
              txtTotalNeto.Text = tblCodProv.Rows(0).Item(15)

              Dim StrsqlDetDoc As String = "Select Codigo,Descripcion,Bodega,Fecha_Expiracion,Cantidad,CantidadBonif,Costo_Unitario,Descuento,Subtotal from vwDetalleComprasLoc where "

              StrsqlDetDoc = StrsqlDetDoc & " CodigoSucursal = " & nSucursal & " and "
              StrsqlDetDoc = StrsqlDetDoc & " Numero_de_Compra = '" & sNumDoc & "' "
              StrsqlDetDoc = StrsqlDetDoc & " and Tipo_Compra = " & nTipoCompra & ""
              StrsqlDetDoc = StrsqlDetDoc & " and Codigo_Proveedor = '" & sProveedor & "'"
              'StrsqlDetDoc = StrsqlDetDoc & " order by Documento desc"

            TblDetalleDocBS.DataSource = SQL(StrsqlDetDoc, "tblDetalleDoc", My.Settings.SolIndustrialCNX).Tables(0)

            'EncabezadosDetDoc()
              If TblDetalleDocBS.Count <= 0 Then
                  'btnAgregar.Enabled = True
                  'btnModificar.Enabled = False
                  'btnEliminar.Enabled = False
              Else
                  'btnAgregar.Enabled = True
                  'btnModificar.Enabled = True
                  'btnEliminar.Enabled = True
              End If
        Else
            'btnAgregar.Enabled = False
            'btnModificar.Enabled = False
            'btnEliminar.Enabled = False
        End If
    End Sub

End Class