﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntradaSalida
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntradaSalida))
        Me.gpbEncabezado = New System.Windows.Forms.GroupBox()
        Me.deRegistrado = New System.Windows.Forms.DateTimePicker()
        Me.cmbBodega = New System.Windows.Forms.ComboBox()
        Me.txtBodega = New System.Windows.Forms.Label()
        Me.txtNoFactura = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ckIsAnulada = New DevExpress.XtraEditors.CheckEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbTipDocumento = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.richComentario = New System.Windows.Forms.RichTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDocumento = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.grbDetalleProd = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.grdDetalle = New DevExpress.XtraGrid.GridControl()
        Me.grvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.lblCosto = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.gpbEncabezado.SuspendLayout()
        CType(Me.ckIsAnulada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDetalleProd.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mainPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbEncabezado
        '
        Me.gpbEncabezado.Controls.Add(Me.lblCosto)
        Me.gpbEncabezado.Controls.Add(Me.txtCosto)
        Me.gpbEncabezado.Controls.Add(Me.deRegistrado)
        Me.gpbEncabezado.Controls.Add(Me.cmbBodega)
        Me.gpbEncabezado.Controls.Add(Me.txtBodega)
        Me.gpbEncabezado.Controls.Add(Me.txtNoFactura)
        Me.gpbEncabezado.Controls.Add(Me.Label1)
        Me.gpbEncabezado.Controls.Add(Me.ckIsAnulada)
        Me.gpbEncabezado.Controls.Add(Me.Label8)
        Me.gpbEncabezado.Controls.Add(Me.txtCodigo)
        Me.gpbEncabezado.Controls.Add(Me.Label6)
        Me.gpbEncabezado.Controls.Add(Me.cmbTipDocumento)
        Me.gpbEncabezado.Controls.Add(Me.Label5)
        Me.gpbEncabezado.Controls.Add(Me.richComentario)
        Me.gpbEncabezado.Controls.Add(Me.Label4)
        Me.gpbEncabezado.Controls.Add(Me.txtDocumento)
        Me.gpbEncabezado.Controls.Add(Me.Label3)
        Me.gpbEncabezado.Location = New System.Drawing.Point(3, 3)
        Me.gpbEncabezado.Name = "gpbEncabezado"
        Me.gpbEncabezado.Size = New System.Drawing.Size(1006, 125)
        Me.gpbEncabezado.TabIndex = 47
        Me.gpbEncabezado.TabStop = False
        Me.gpbEncabezado.Text = "Encabezado - Datos Generales"
        '
        'deRegistrado
        '
        Me.deRegistrado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deRegistrado.Location = New System.Drawing.Point(813, 21)
        Me.deRegistrado.Name = "deRegistrado"
        Me.deRegistrado.Size = New System.Drawing.Size(98, 20)
        Me.deRegistrado.TabIndex = 57
        '
        'cmbBodega
        '
        Me.cmbBodega.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBodega.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBodega.BackColor = System.Drawing.Color.White
        Me.cmbBodega.FormattingEnabled = True
        Me.cmbBodega.Location = New System.Drawing.Point(587, 54)
        Me.cmbBodega.Name = "cmbBodega"
        Me.cmbBodega.Size = New System.Drawing.Size(179, 21)
        Me.cmbBodega.TabIndex = 74
        '
        'txtBodega
        '
        Me.txtBodega.AutoSize = True
        Me.txtBodega.Location = New System.Drawing.Point(492, 57)
        Me.txtBodega.Name = "txtBodega"
        Me.txtBodega.Size = New System.Drawing.Size(47, 13)
        Me.txtBodega.TabIndex = 73
        Me.txtBodega.Text = "Bodega:"
        '
        'txtNoFactura
        '
        Me.txtNoFactura.BackColor = System.Drawing.Color.White
        Me.txtNoFactura.Location = New System.Drawing.Point(588, 88)
        Me.txtNoFactura.Name = "txtNoFactura"
        Me.txtNoFactura.Size = New System.Drawing.Size(178, 20)
        Me.txtNoFactura.TabIndex = 72
        Me.txtNoFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(493, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "No. Factura:"
        '
        'ckIsAnulada
        '
        Me.ckIsAnulada.Location = New System.Drawing.Point(927, 21)
        Me.ckIsAnulada.Name = "ckIsAnulada"
        Me.ckIsAnulada.Properties.Caption = "Anulada"
        Me.ckIsAnulada.Size = New System.Drawing.Size(69, 19)
        Me.ckIsAnulada.TabIndex = 35
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(30, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 69
        Me.Label8.Text = "Codigo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.White
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(74, 21)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(130, 20)
        Me.txtCodigo.TabIndex = 68
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(772, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "Fecha:"
        '
        'cmbTipDocumento
        '
        Me.cmbTipDocumento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbTipDocumento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbTipDocumento.BackColor = System.Drawing.Color.White
        Me.cmbTipDocumento.FormattingEnabled = True
        Me.cmbTipDocumento.Location = New System.Drawing.Point(305, 21)
        Me.cmbTipDocumento.Name = "cmbTipDocumento"
        Me.cmbTipDocumento.Size = New System.Drawing.Size(178, 21)
        Me.cmbTipDocumento.TabIndex = 63
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(209, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 13)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "Tipo Documento:"
        '
        'richComentario
        '
        Me.richComentario.BackColor = System.Drawing.Color.White
        Me.richComentario.Location = New System.Drawing.Point(74, 54)
        Me.richComentario.Name = "richComentario"
        Me.richComentario.Size = New System.Drawing.Size(409, 54)
        Me.richComentario.TabIndex = 34
        Me.richComentario.Text = ""
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 17)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Comentario:"
        '
        'txtDocumento
        '
        Me.txtDocumento.BackColor = System.Drawing.Color.White
        Me.txtDocumento.Location = New System.Drawing.Point(587, 21)
        Me.txtDocumento.Name = "txtDocumento"
        Me.txtDocumento.Size = New System.Drawing.Size(178, 20)
        Me.txtDocumento.TabIndex = 39
        Me.txtDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(492, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "No. Documento:"
        '
        'grbDetalleProd
        '
        Me.grbDetalleProd.Controls.Add(Me.Panel3)
        Me.grbDetalleProd.Controls.Add(Me.grdDetalle)
        Me.grbDetalleProd.Location = New System.Drawing.Point(3, 134)
        Me.grbDetalleProd.Name = "grbDetalleProd"
        Me.grbDetalleProd.Size = New System.Drawing.Size(1009, 368)
        Me.grbDetalleProd.TabIndex = 48
        Me.grbDetalleProd.TabStop = False
        Me.grbDetalleProd.Text = "Detalle de Entrada"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 313)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1003, 52)
        Me.Panel3.TabIndex = 57
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.btnModificar)
        Me.Panel1.Controls.Add(Me.btnAgregar)
        Me.Panel1.Location = New System.Drawing.Point(2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(284, 50)
        Me.Panel1.TabIndex = 1
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(191, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(83, 40)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Enabled = False
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(102, 4)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(83, 40)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Enabled = False
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(12, 4)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(83, 40)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'grdDetalle
        '
        Me.grdDetalle.Location = New System.Drawing.Point(15, 19)
        Me.grdDetalle.MainView = Me.grvDetalle
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(981, 293)
        Me.grdDetalle.TabIndex = 52
        Me.grdDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalle, Me.GridView1})
        '
        'grvDetalle
        '
        Me.grvDetalle.GridControl = Me.grdDetalle
        Me.grvDetalle.Name = "grvDetalle"
        Me.grvDetalle.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalle.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalle.OptionsBehavior.Editable = False
        Me.grvDetalle.OptionsBehavior.ReadOnly = True
        Me.grvDetalle.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetalle.OptionsCustomization.AllowColumnResizing = False
        Me.grvDetalle.OptionsCustomization.AllowGroup = False
        Me.grvDetalle.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetalle.OptionsView.AllowHtmlDrawGroups = False
        Me.grvDetalle.OptionsView.ColumnAutoWidth = False
        Me.grvDetalle.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetalle.OptionsView.ShowFooter = True
        Me.grvDetalle.OptionsView.ShowGroupPanel = False
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.grdDetalle
        Me.GridView1.Name = "GridView1"
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.grbDetalleProd)
        Me.mainPanel.Controls.Add(Me.gpbEncabezado)
        Me.mainPanel.Location = New System.Drawing.Point(-2, -2)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1012, 505)
        Me.mainPanel.TabIndex = 48
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(772, 57)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(77, 13)
        Me.lblCosto.TabIndex = 76
        Me.lblCosto.Text = "Costo Entrada:"
        '
        'txtCosto
        '
        Me.txtCosto.BackColor = System.Drawing.Color.White
        Me.txtCosto.Enabled = False
        Me.txtCosto.Location = New System.Drawing.Point(869, 54)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(127, 20)
        Me.txtCosto.TabIndex = 75
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'frmEntradaSalida
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1009, 500)
        Me.Controls.Add(Me.mainPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEntradaSalida"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entradas/Salidas de Inventario"
        Me.gpbEncabezado.ResumeLayout(False)
        Me.gpbEncabezado.PerformLayout()
        CType(Me.ckIsAnulada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDetalleProd.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mainPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gpbEncabezado As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ckIsAnulada As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cmbTipDocumento As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents richComentario As System.Windows.Forms.RichTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grbDetalleProd As System.Windows.Forms.GroupBox
    Friend WithEvents grdDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents cmbBodega As System.Windows.Forms.ComboBox
    Friend WithEvents txtBodega As System.Windows.Forms.Label
    Friend WithEvents txtNoFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents deRegistrado As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblCosto As System.Windows.Forms.Label
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
End Class
