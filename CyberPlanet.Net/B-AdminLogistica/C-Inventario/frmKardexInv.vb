﻿Imports System.Xml
Imports System.Text.RegularExpressions
Public Class frmKardexInv

    Public Sub CargarDatos(ByVal Codigo As String)

        Dim strsql As String = ""

        Try
            strsql = "SELECT PRD.Codigo_Producto, PRD.Nombre_Producto, Tbl_Proveedor.Nombre_Proveedor, PRD.Existencia_Minima, PRD.Existencia_Maxima " & _
                     "FROM Productos PRD INNER JOIN Tbl_Proveedor ON PRD.Codigo_Proveedor = Tbl_Proveedor.Codigo_Proveedor where PRD.Codigo_Producto = '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            lblCodigo.Text = tblDatos.Rows(0).Item(0)
            lblNombreProd.Text = tblDatos.Rows(0).Item(1)
            lblProveedor.Text = tblDatos.Rows(0).Item(2)
            lblMinimo.Text = tblDatos.Rows(0).Item(3)
            lblMaximo.Text = tblDatos.Rows(0).Item(4)

            deInicio.Enabled = True
            deFin.Enabled = True
            cmbBodega.Enabled = True
            btnGenerar.Enabled = True

            Bodega(Codigo)

        Catch ex As Exception
        End Try
    End Sub

    Public Sub Bodega(ByVal Codigo As String)
        Dim strsql As String = ""
        Dim valor As String = ""

        strsql = " SELECT Alm.Codigo_Bodega, Alm.Nombre_Bodega FROM Almacenes Alm INNER JOIN Inventario_SaldoInicial Inv "
        strsql = strsql & " ON Alm.Codigo_Sucursal = Inv.Codigo_Sucursal and Alm.Codigo_Bodega = Inv.Codigo_Bodega "
        strsql = strsql & " WHERE Inv.Codigo_Sucursal = 1 and Inv.Codigo_Producto = '" & Codigo & "' "

        Dim Data As DataTable = SQL(strsql, "tblData", My.Settings.SolIndustrialCNX).Tables(0)

        cmbBodega.DisplayMember = "Nombre_Bodega"
        cmbBodega.ValueMember = "Codigo_Bodega"
        cmbBodega.DataSource = Data

        If Data.Rows.Count > 0 Then
            btnGenerar.Enabled = True
        Else
            btnGenerar.Enabled = False
            cmbBodega.Text = ""
            grdKardex.DataSource = Nothing
        End If
    End Sub

    Public Sub cargarKardex(ByVal Codigo As String)
        Dim strsql As String = ""
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        grdKardex.DataSource = Nothing

        strsql = "exec [dbo].[SP_GET_Kardex_Inventario] '" & Codigo & "'," & cmbBodega.SelectedValue.ToString & ",'" & deInicio.DateTime.Date & "','" & deFin.DateTime.Date & "'"

        grdKardex.DataSource = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
        grvKardex.Columns(0).Width = 30
        grvKardex.Columns(1).Width = 70
        grvKardex.Columns(2).Width = 90
        grvKardex.Columns(3).Width = 300
        grvKardex.Columns(4).Width = 80
        grvKardex.Columns(5).Width = 80
        grvKardex.Columns(6).Width = 80
        grvKardex.Columns(7).Width = 100
        grvKardex.Columns(7).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns(7).DisplayFormat.FormatString = "{0:$ #,##0.0000}"
        grvKardex.Columns(8).Width = 100
        grvKardex.Columns(8).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns(8).DisplayFormat.FormatString = "{0:$ #,##0.0000}"
        grvKardex.Columns(9).Width = 100
        grvKardex.Columns(9).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns(9).DisplayFormat.FormatString = "{0:$ #,##0.0000}"
        grvKardex.Columns(10).Width = 85
        grvKardex.Columns(10).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns(10).DisplayFormat.FormatString = "{0:$ #,##0.0000}"
    End Sub

    Public Sub nuevo()
        lblCodigo.Text = ""
        lblNombreProd.Text = ""

        Dim Fecha As Date = Date.Now
        Fecha = DateSerial(Year(Fecha), Month(Fecha) + 0, 1)
        deInicio.DateTime = Fecha

        deFin.DateTime = Date.Now
        grdKardex.DataSource = Nothing
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmKardexInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Fecha As Date = Date.Now
        Fecha = DateSerial(Year(Fecha), Month(Fecha) + 0, 1)
        deInicio.DateTime = Fecha
        deFin.DateTime = Date.Now

        deInicio.Enabled = False
        deFin.Enabled = False
        cmbBodega.Enabled = False
        btnGenerar.Enabled = False
    End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        cargarKardex(lblCodigo.Text)
    End Sub

    Private Sub frmKardexInv_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 24
    End Sub

    Private Sub cmbBodega_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbBodega.SelectedValueChanged
        grdKardex.DataSource = Nothing
    End Sub
End Class