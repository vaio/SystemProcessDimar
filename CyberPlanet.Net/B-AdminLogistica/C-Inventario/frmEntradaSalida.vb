﻿Imports System.Xml

Public Class frmEntradaSalida

    Public CodMaxEntrada As String = ""
    Public CodMaxSalida As String = ""
    Public ResumEntrada As String = ""
    Public ResumSalida As String = ""
    Public ResumEntradaDeta As String = ""

    Public Sub CargarDatos(ByVal Codigo As String, ByVal CodDocumento As String)
        Dim strsql As String = ""
        Dim strsql2 As String = ""

        Try

            strsql = "SELECT CAST(Mov.Fecha AS Date) AS Fecha, Mov.Codigo_Movimiento As Codigo, Mov.Codigo_Documento As 'No. Documento', Categorias_Detalle.IdCategoriaDetalle As 'Tipo Documento', " & _
                     "Mov.Comentario, Mov.Total_Neto As 'Costo Total',Mov.Anulada FROM Movimientos_Almacenes AS Mov INNER JOIN Categorias_Detalle ON Mov.Tipo_Documento = Categorias_Detalle.IdCategoriaDetalle " & _
                     "WHERE (Mov.Tipo_Movimiento = " & TipoMovimiento & ") and Mov.Codigo_Documento = '" & CodDocumento & "' AND Mov.Anulada = 0 and Mov.Codigo_Movimiento = '" & Codigo & "' and CodigoSucursal = " & My.Settings.Sucursal & " ORDER BY Fecha DESC "

            Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count <> 0 Then

                deRegistrado.Value = tblDatos.Rows(0).Item("Fecha")
                txtCodigo.Text = tblDatos.Rows(0).Item("Codigo")
                txtDocumento.Text = tblDatos.Rows(0).Item("No. Documento")
                cmbTipDocumento.SelectedValue = tblDatos.Rows(0).Item("Tipo Documento")
                richComentario.Text = tblDatos.Rows(0).Item("Comentario")
                txtCosto.Text = tblDatos.Rows(0).Item("Costo Total")
                ckIsAnulada.Checked = tblDatos.Rows(0).Item("Anulada")

                DesactivarCampos(True)
                CargarDetalle(txtCodigo.Text)
            Else
                LimpiarCampos()
                btnAgregar.Enabled = False
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CargarDetalle(ByVal CodigoMovimiento As String)
        Dim strsql As String = ""
        Dim strsql2 As String = ""

        Try
            strsql = "SELECT Prd.Codigo_Producto AS 'Codigo', Prd.Nombre_Producto AS 'Producto', MovDet.Cantidad AS 'Cantidad', MovDet.Precio_Unitario AS 'Costo Unitario', " & _
                     "CAST((MovDet.Cantidad * MovDet.Precio_Unitario) AS DECIMAL(18,4)) AS 'Costo Total' FROM Movimientos_Almaneces_Detalle MovDet  " & _
                     "INNER JOIN Productos Prd ON MovDet.Codigo_Producto = Prd.Codigo_Producto " & _
                     "WHERE MovDet.Codigo_Movimiento = '" & CodigoMovimiento & "'"

            grdDetalle.DataSource = SQL(strsql, "tblDatosDetalle", My.Settings.SolIndustrialCNX).Tables(0)

            grvDetalle.Columns("Codigo").Width = 75
            grvDetalle.Columns("Producto").Width = 400
            grvDetalle.Columns("Cantidad").Width = 80
            grvDetalle.Columns("Costo Unitario").Width = 100
            grvDetalle.Columns("Costo Total").Width = 100

            grvDetalle.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvDetalle.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:#.####}"
            grvDetalle.OptionsView.ShowFooter = True

            If grvDetalle.RowCount <= 0 Then
                btnAgregar.Enabled = True
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
            Else
                btnAgregar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

        Catch ex As Exception
        End Try
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Private Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        txtDocumento.Enabled = not bIsActivo
        cmbTipDocumento.Enabled = Not bIsActivo
        deRegistrado.Enabled = Not bIsActivo
        richComentario.Enabled = Not bIsActivo
        cmbBodega.Enabled = Not bIsActivo
        txtNoFactura.Enabled = Not bIsActivo
        ckIsAnulada.Enabled = Not bIsActivo
    End Sub

    Public Sub Nuevo()
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        nTipoEdic = 1
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        LimpiarCampos()
        txtCodigo.Text = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
        CargarDatos(txtCodigo.Text, txtDocumento.Text)
    End Sub

    Public Sub Modificar()
        nTipoEdic = 2
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
    End Sub

    Public Sub Eliminar()
        nTipoEdic = 3
        DesactivarCampos(True)
        PermitirBotonesEdicion(False)
    End Sub

    Function Cancelar()
        nTipoEdic = 1
        CargarDatos(txtCodigo.Text, txtDocumento.Text)
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
    End Function

    Sub LimpiarCampos()
        txtDocumento.Text = ""
        richComentario.Text = ""
        deRegistrado.Value = Now.Date
        cmbTipDocumento.SelectedIndex = 0
        cmbBodega.SelectedIndex = 0
        txtNoFactura.Text = ""
        ckIsAnulada.Checked = False
        grvDetalle.Columns.Clear()
    End Sub

    Private Sub frmEntradaSalida_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        DesactivarCampos(True)

        txtCodigo.Text = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
        cargarBodega()
        cargarTipoDocumento()
        deRegistrado.Value = Date.Now

        If TipoMovimiento = 4 Then
            gpbEncabezado.Text = "Encabezado - Entrada de Inventario"
            grbDetalleProd.Text = "Detalle de Entrada"
            txtBodega.Text = "Bodega Entrada"
            lblCosto.Text = "Costo Entrada C$:"
        Else
            gpbEncabezado.Text = "Encabezado - Salida de Inventario"
            grbDetalleProd.Text = "Detalle de Salida"
            txtBodega.Text = "Bodega Salida"
            lblCosto.Text = "Costo Salida C$:"
        End If

        txtCosto.Text = 0
        cmbBodega.SelectedIndex = 0
        cmbTipDocumento.SelectedIndex = 0

    End Sub

    Public Sub cargarBodega()
        cmbBodega.DataSource = SQL("SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes order by Codigo_Bodega", "tblBodega", My.Settings.SolIndustrialCNX).Tables(0)
        cmbBodega.DisplayMember = "Nombre_Bodega"
        cmbBodega.ValueMember = "Codigo_Bodega"
    End Sub

    Public Sub cargarTipoDocumento()
        cmbTipDocumento.DataSource = SQL("SELECT CatDet.IdCategoriaDetalle, CatDet.Estado FROM Categorias Cat INNER JOIN Categorias_Detalle CatDet ON Cat.IdCategoria = CatDet.IdCategoria where CatDet.IdCategoria = 6 and CatDet.Activo = 1 and Cat.Activo = 1 Order by Orden", "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)
        cmbTipDocumento.DisplayMember = "Estado"
        cmbTipDocumento.ValueMember = "IdCategoriaDetalle"
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Public Sub GuardarMasterDoc()
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try

            If TipoMovimiento = 4 Then
                ResumEntrada = Inventario.EditarMovimientoAlmacen(txtCodigo.Text, My.Settings.Sucursal, txtDocumento.Text, cmbTipDocumento.SelectedValue, richComentario.Text.ToUpper, 0, deRegistrado.Value, 4, ckIsAnulada.EditValue, nTipoEdic)
            Else
                ResumSalida = Inventario.EditarMovimientoAlmacen(txtCodigo.Text, My.Settings.Sucursal, txtDocumento.Text, cmbTipDocumento.SelectedValue, richComentario.Text.ToUpper, 0, deRegistrado.Value, 5, ckIsAnulada.EditValue, nTipoEdic)
            End If

            If ResumEntrada = "OK" Or ResumSalida = "OK" Then
                MsgBox("Proceso realizado correctamente.", MsgBoxStyle.Information, "CyberPlanet.Net")
                Cancelar()
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",") Then
            e.Handled = True
        ElseIf e.KeyChar = "," Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub frmRecetas_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Dim strsql As String = "Select * from vwRecetas"
        Dim tblDatosCatalogo As DataTable = Nothing
        frmCatalogo.GridView1.Columns.Clear()
        frmCatalogo.TblCatalogosBS.DataSource = SQL(strsql, "tblDatosCatalogo", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Private Sub cmdAddProv_Click(sender As Object, e As EventArgs)
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 9
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdicDet = 1
        frmDetalleEntradaSalida.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdicDet = 2
        frmDetalleEntradaSalida.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        nTipoEdicDet = 3
        frmDetalleEntradaSalida.ShowDialog()
    End Sub

    Private Sub frmEntradaSalida_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        frmCatalogo.Close()

        If TipoMovimiento = 4 Then
            NumCatalogo = 25
        ElseIf TipoMovimiento = 5 Then
            NumCatalogo = 26
        End If
    End Sub

    Private Sub frmEntradaSalida_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub

    Private Sub txtDocumento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocumento.KeyPress
        NumerosyDecimal(txtDocumento, e)
    End Sub
End Class