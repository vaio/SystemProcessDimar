﻿Public Class frmDetalleEntradaSalida

    Dim NewExistUnd As Integer = 0
    Dim LastExistUnd As Integer = 0
    Dim NewExistMon As Double = 0
    Dim LastExistMon As Double = 0
    Dim ExistMon As Double = 0
    Dim ExistUndTotal As Integer = 0
    Dim ExistMonTotal As Double = 0
    Dim NewCostoProm As Double = 0

    Dim SumSubtotal As Double = 0
    Dim SumDesc As Double = 0

    Dim nIR As Double = 0
    Dim nIMI As Double = 0
    Dim nIVA As Double = 0

    Dim ResumEntrada As String = ""
    Dim CodMaxEntrada As String = ""
    Dim ResumEntradaDeta As String = ""

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        nTipoEdicDet = 0
        LastExistUnd = 0
        LastExistMon = 0
        Me.Dispose()
        Close()
    End Sub

    Sub CargarDatosProducto(ByVal nNumProd As String)
        Dim strsql As String = "SELECT Codigo_Producto,Nombre_Producto,Costo_Promedio,Existencia_Actual from Productos where Codigo_Producto = '" & nNumProd & "'"
        Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)
        If TblDatosProd.Rows.Count <> 0 Then
            lblDescripcion.Text = TblDatosProd.Rows(0).Item(1)
            lblExistencia.Text = TblDatosProd.Rows(0).Item(3)
            txtCostoUnit.Text = Format(TblDatosProd.Rows(0).Item(2), "###,###,###.00")
            CargarExistProd()
            txtCantidad.Focus()
        Else
            MsgBox("No existe ningún producto existente con el código ingresado.", MsgBoxStyle.Information, "Información")
            txtCodigoProd.Text = ""
            txtCodigoProd.Focus()
        End If
    End Sub

    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged
        CalcularCostoTotal()
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        If lblDescripcion.Text <> "" Then
            Dim Resum As String = ""
            Dim Div As Double = 0
            Dim DetalleDoc As New clsDocumentos(My.Settings.SolIndustrialCNX)
            Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

            Try

                ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(frmEntradaSalida.txtCodigo.Text, My.Settings.Sucursal, txtCodigoProd.Text, txtCantidad.Text, txtCostoUnit.Text, "", frmEntradaSalida.cmbBodega.SelectedValue.ToString(), False, nTipoEdicDet)

                If ResumEntradaDeta = "OK" Then
                    frmEntradaSalida.CargarDatos(frmEntradaSalida.txtCodigo.Text, frmEntradaSalida.txtDocumento.Text)
                    Cerrar()
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "Información")
                End If

            Catch ex As Exception
                Call MsgBox("Error: " + ex.Message)
            Finally

            End Try
        End If
    End Sub

    Private Sub frmDetalleDocInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If nTipoEdicDet <> 1 Then
            CargarCurrentDetalle()
        End If
        If nTipoEdicDet = 3 Then
            Panel2.Enabled = False
        End If
        txtCodigoProd.Focus()
    End Sub

    Sub CargarCurrentDetalle()
        Dim strsql As String = ""
        Dim CodigoProducto As String = frmEntradaSalida.grvDetalle.GetRowCellValue(frmEntradaSalida.grvDetalle.GetSelectedRows(0), "Codigo").ToString()

        strsql = "SELECT TOP 1 Prd.Codigo_Producto, Prd.Nombre_Producto, MovDet.Bodega, MovDet.Cantidad, MovDet.Precio_Unitario FROM Movimientos_Almaneces_Detalle MovDet " & _
                 "INNER JOIN Productos Prd ON MovDet.Codigo_Producto = Prd.Codigo_Producto WHERE  MovDet.Codigo_Movimiento = '" & frmEntradaSalida.txtCodigo.Text & "' and Prd.Codigo_Producto = '" & CodigoProducto & "'"

        Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatos.Rows.Count <> 0 Then
            txtCodigoProd.Text = tblDatos.Rows(0).Item(0)
            lblDescripcion.Text = tblDatos.Rows(0).Item(1)
            txtCantidad.Text = tblDatos.Rows(0).Item(3)
            LastExistUnd = tblDatos.Rows(0).Item(3)
            txtCostoUnit.Text = tblDatos.Rows(0).Item(4)
            LastExistMon = (txtCantidad.Text * txtCostoUnit.Text)
            CargarExistProd()
            CargarExistProdBod(tblDatos.Rows(0).Item(2))
        End If

    End Sub

    Function ExistinBodega()
        Dim strExistBodega As String = "SELECT dbo.Almacenes.Codigo_Bodega, dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & frmEntradaSalida.cmbBodega.ToString() & ")"
        Dim tblExistinBod As DataTable = SQL(strExistBodega, "tblExistinBod", My.Settings.SolIndustrialCNX).Tables(0) ', dbo.Inventario.UndTotal_Existencia as Existencia
        ExistinBodega = 0
        If tblExistinBod.Rows.Count > 0 Then
            ExistinBodega = tblExistinBod.Rows.Count
        End If
    End Function


    Function NomBodeg(ByVal nNumBodega As Integer)
        Dim strsqlNomBod As String
        strsqlNomBod = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & nNumBodega & ")"
        Dim tblNomBod As DataTable = SQL(strsqlNomBod, "tblNomBod", My.Settings.SolIndustrialCNX).Tables(0) ', dbo.Inventario.UndTotal_Existencia as Existencia
        If tblNomBod.Rows.Count > 0 Then
            NomBodeg = tblNomBod.Rows(0).Item(0)
        End If
    End Function

    Private Sub txtCostoUnit_TextChanged(sender As Object, e As EventArgs) Handles txtCostoUnit.TextChanged
        CalcularCostoTotal()
    End Sub

    Sub CalcularCostoTotal()
        lblCostoTotal.Text = Format(CDbl(IIf(txtCostoUnit.Text = "", 0, txtCostoUnit.Text)) * CInt(IIf(txtCantidad.Text = "", 0, txtCantidad.Text)), "###,###,###.00")
    End Sub

    Sub CargarExistProd()
        If txtCodigoProd.Text = "" Then
            Exit Sub
        End If
        Dim strsqlExistProd As String
        strsqlExistProd = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "')"
        Tbl_ExistBodegasBS.DataSource = SQL(strsqlExistProd, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Sub CargarExistProdBod(ByVal nNumBodega As Integer)
        If txtCodigoProd.Text = "" Then
            Exit Sub
        End If
        Dim strsqlExistProdBod As String
        strsqlExistProdBod = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & nNumBodega & ")"
        Dim tblExistProdBod As DataTable = SQL(strsqlExistProdBod, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)
        If tblExistProdBod.Rows.Count > 0 Then
            lblExistencia.Text = tblExistProdBod.Rows(0).Item(1)
        End If
    End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs)
        Dim sNomBodega As String
        If Tbl_ExistBodegasBS.Count > 0 Then
            Dim registro As DataRowView = Tbl_ExistBodegasBS.Current
            sNomBodega = registro.Item(0)
        End If
    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim DecSeparator As String = Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
        Dim dig As Integer = Len(CajaTexto.Text & e.KeyChar)

        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = DecSeparator And Not CajaTexto.Text.IndexOf(DecSeparator) Then
            e.Handled = True
        ElseIf e.KeyChar = DecSeparator Then
            e.Handled = False
        Else
            e.Handled = True
        End If

        If CajaTexto.SelectedText <> DecSeparator Then
            If e.KeyChar = DecSeparator Then
                e.Handled = True
            End If
        End If

        If dig = 1 And e.KeyChar = DecSeparator Then
            e.Handled = True
        End If

    End Sub

    Private Sub txtCodigoProd_KeyUp(sender As Object, e As KeyEventArgs) Handles txtCodigoProd.KeyUp
        If e.KeyCode = 13 Then
            If txtCodigoProd.Text = "" Then
                frmBusquedaProd.ShowDialog()
            Else
                CargarDatosProducto(txtCodigoProd.Text)
            End If
        End If
    End Sub

    Private Sub txtCodigoProd_Click(sender As Object, e As EventArgs) Handles txtCodigoProd.Click
        If txtCodigoProd.Text = "" Then
            frmBusquedaProd.ShowDialog()
        End If
    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        NumerosyDecimal(txtCantidad, e)
    End Sub

    Private Sub txtCostoUnit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCostoUnit.KeyPress
        NumerosyDecimal(txtCostoUnit, e)
    End Sub
End Class