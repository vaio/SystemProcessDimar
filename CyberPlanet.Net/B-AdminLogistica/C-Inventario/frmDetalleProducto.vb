﻿Public Class frmDetalleProducto

    Dim NewExistUnd As Integer = 0
    Dim LastExistUnd As Integer = 0
    Dim NewExistMon As Double = 0
    Dim LastExistMon As Double = 0
    Dim ExistMon As Double = 0
    Dim ExistUndTotal As Integer = 0
    Dim ExistMonTotal As Double = 0
    Dim NewCostoProm As Double = 0

    Dim SumSubtotal As Double = 0
    Dim SumDesc As Double = 0

    Dim nIR As Double = 0
    Dim nIMI As Double = 0
    Dim nIVA As Double = 0

    Dim ResumEntrada As String = ""
    Dim CodMaxEntrada As String = ""
    Dim ResumEntradaDeta As String = ""

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        nTipoEdicDet = 0
        LastExistUnd = 0
        LastExistMon = 0
        Me.Dispose()
        Close()
    End Sub

    Sub CargarDatosProducto(ByVal nNumProd As String)
        Dim strsql As String = "SELECT Codigo_Producto,Nombre_Producto,Costo_Promedio,Existencia_Actual from Productos where Codigo_Producto = '" & nNumProd & "'"
        Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)
        If TblDatosProd.Rows.Count <> 0 Then
            lblDescripcion.Text = TblDatosProd.Rows(0).Item(1)
            lblExistencia.Text = TblDatosProd.Rows(0).Item(3)
            txtCostoUnit.Text = Format(TblDatosProd.Rows(0).Item(2), "###,###,###.00")
            CargarExistProd()
            CargarBodegas()
            txtCantidad.Focus()
        Else
            MsgBox("No existe ningún producto existente con el código ingresado.", MsgBoxStyle.Information, "Información")
            txtCodigoProd.Text = ""
            txtCodigoProd.Focus()
        End If
    End Sub

    Private Sub txtCodigoProd_KeyUp(sender As Object, e As KeyEventArgs) Handles txtCodigoProd.KeyUp
        If e.KeyCode = 13 Then
            If txtCodigoProd.Text = "" Then
                frmBusquedaProd.ShowDialog()
            Else
                CargarDatosProducto(txtCodigoProd.Text)
            End If
        End If
    End Sub

    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged
        CalcularCostoTotal()
    End Sub

    Sub TotalExistenciasProd()
        Dim StrSqlExisTotalProd As String
        Dim StrSqlExistBodProd As String
        Dim StrSqlSumDetalle As String

        StrSqlExisTotalProd = "SELECT SUM(UndTotal_Existencia) AS SumUnd, SUM(MonTotal_Existencia) AS SumMon, Codigo_Producto FROM dbo.Inventario WHERE (Codigo_Producto = '" & txtCodigoProd.Text & "') GROUP BY Codigo_Producto"
        StrSqlExistBodProd = "SELECT MonTotal_Existencia FROM dbo.Inventario WHERE (Codigo_Bodega = " & luBodegas.EditValue & ") AND (Codigo_Producto = '" & txtCodigoProd.Text & "')"
        StrSqlSumDetalle = "SELECT SUM(Cantidad * Precio_Unitario) AS SumSubtotal, SUM(Descuento) AS SumDesc, CodigoSucursal, Numero_de_Compra, Tipo_Compra, Codigo_Proveedor FROM dbo.Detalle_Compras GROUP BY CodigoSucursal, Numero_de_Compra, Tipo_Compra, Codigo_Proveedor HAVING (CodigoSucursal = " & nSucursal & ") AND (Numero_de_Compra = '" & frmMasterCompraLocal.txtNumDoc.Text & "') AND (Tipo_Compra = " & IIf(frmMasterCompraLocal.rbContado.Checked = True, 1, 2) & ") AND (Codigo_Proveedor = '" & frmMasterCompraLocal.luProveedor.EditValue & "')"

        Dim TblExistTotal As DataTable = SQL(StrSqlExisTotalProd, "tblExisTotalProd", My.Settings.SolIndustrialCNX).Tables(0)
        If TblExistTotal.Rows.Count <> 0 Then
            ExistUndTotal = TblExistTotal.Rows(0).Item(0)
            ExistMonTotal = Format(TblExistTotal.Rows(0).Item(1), "###,###,###.00")
        Else
            MsgBox("No existe ningún producto existente con el código ingresado.", MsgBoxStyle.Information, "Información")
            txtCodigoProd.Focus()
        End If

        Dim TblExistBodProd As DataTable = SQL(StrSqlExistBodProd, "tblExistBodProd", My.Settings.SolIndustrialCNX).Tables(0)
        If TblExistBodProd.Rows.Count <> 0 Then
            ExistMon = Format(TblExistBodProd.Rows(0).Item(0), "###,###,###.00")
        End If

        Dim TblSumDetalle As DataTable = SQL(StrSqlSumDetalle, "tblSumDetalle", My.Settings.SolIndustrialCNX).Tables(0)
        If TblSumDetalle.Rows.Count <> 0 Then
            SumSubtotal = Format(TblSumDetalle.Rows(0).Item(0), "###,###,###.00")
            SumDesc = Format(TblSumDetalle.Rows(0).Item(1), "###,###,###.00")
        End If

    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        If lblDescripcion.Text <> "" Then
            Dim Resum As String = ""
            Dim Div As Double = 0
            Dim DetalleDoc As New clsDocumentos(My.Settings.SolIndustrialCNX)
            Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

            'nSucursal = 1

            ''TotalExistenciasProd()
            'If nTipoEdicDet = 1 Then
            '    NewExistUnd = CInt(lblExistencia.Text) + CInt(txtCantidad.Text)
            '    NewExistMon = ExistMon + CDbl(lblCostoTotal.Text)
            '    ExistUndTotal = ExistUndTotal + CInt(txtCantidad.Text)
            '    ExistMonTotal = ExistMonTotal + CDbl(lblCostoTotal.Text)
            '    SumSubtotal = SumSubtotal + CDbl(lblCostoTotal.Text)
            '    'SumDesc = SumDesc + CDbl(txtDescuento.Text)
            'ElseIf nTipoEdicDet = 2 Then
            '    NewExistUnd = CInt(lblExistencia.Text) - LastExistUnd + CInt(txtCantidad.Text)
            '    NewExistMon = ExistMon - LastExistMon + CDbl(lblCostoTotal.Text)
            '    ExistUndTotal = ExistUndTotal - LastExistUnd + CInt(txtCantidad.Text)
            '    ExistMonTotal = ExistMonTotal - LastExistMon + CDbl(lblCostoTotal.Text)
            '    SumSubtotal = SumSubtotal - LastExistMon + CDbl(lblCostoTotal.Text)
            '    'SumDesc = SumDesc + CDbl(txtDescuento.Text)
            'ElseIf nTipoEdicDet = 3 Then
            '    NewExistUnd = CInt(IIf(lblExistencia.Text = "", 0, lblExistencia.Text)) - LastExistUnd
            '    NewExistMon = ExistMon - LastExistMon
            '    ExistUndTotal = ExistUndTotal - LastExistUnd
            '    ExistMonTotal = ExistMonTotal - LastExistMon
            '    SumSubtotal = SumSubtotal - LastExistMon
            '    'SumDesc = SumDesc + CDbl(txtDescuento.Text)
            'End If
            'Div = IIf(ExistUndTotal = 0, 0, CDbl(ExistMonTotal / ExistUndTotal))
            'NewCostoProm = Format(IIf(ExistUndTotal = 0, 0, ExistMonTotal / ExistUndTotal), "###,###,###.00")

            'nIR = (SumSubtotal - SumDesc) * (CDbl(frmMasterCompraLocal.txtPorcIR.Text) / 100)
            'nIMI = (SumSubtotal - SumDesc) * (CInt(frmMasterCompraLocal.txtPorcIMI.Text) / 100)
            'nIVA = (SumSubtotal - SumDesc) * (IIf(frmMasterCompraLocal.cmbTipExoneracion.SelectedIndex = 0, 15, 0) / 100)

            'If ExistinBodega() = 0 Then
            '    MsgBox("Este producto no esta agregado en la bodega seleccionada. Seleccione otra bodega", MsgBoxStyle.Information, "Información")
            '    luBodegas.Focus()
            '    Exit Sub
            'End If

            Try

                ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(frmEntradaSalida.txtCodigo.Text, My.Settings.Sucursal, txtCodigoProd.Text, txtCantidad.Text, txtCostoUnit.Text, "", luBodegas.EditValue, False, nTipoEdicDet)

                If ResumEntradaDeta = "OK" Then
                    frmEntradaSalida.CargarDatos(frmEntradaSalida.txtCodigo.Text, frmEntradaSalida.txtDocumento.Text)
                    Cerrar()
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "Información")
                End If

            Catch ex As Exception
                Call MsgBox("Error: " + ex.Message)
            Finally

            End Try
        End If
    End Sub

    Private Sub frmDetalleDocInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarBodegas()
        'txtCantBonif.Text = 0
        'txtDescuento.Text = 0
        'deFechaExp.Value = Now.Date
        If nTipoEdicDet <> 1 Then
            CargarCurrentDetalle()
        End If
        If nTipoEdicDet = 3 Then
            Panel2.Enabled = False
        End If
        txtCodigoProd.Focus()
    End Sub

    Sub CargarCurrentDetalle()
        Dim strsql As String = ""
        Dim CodigoProducto As String = frmEntradaSalida.grvDetalle.GetRowCellValue(frmEntradaSalida.grvDetalle.GetSelectedRows(0), "Codigo").ToString()

        strsql = "SELECT TOP 1 Prd.Codigo_Producto, Prd.Nombre_Producto, MovDet.Bodega, MovDet.Cantidad, MovDet.Precio_Unitario FROM Movimientos_Almaneces_Detalle MovDet " & _
                 "INNER JOIN Productos Prd ON MovDet.Codigo_Producto = Prd.Codigo_Producto WHERE  MovDet.Codigo_Movimiento = '" & frmEntradaSalida.txtCodigo.Text & "' and Prd.Codigo_Producto = '" & CodigoProducto & "'"

        Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

        If tblDatos.Rows.Count <> 0 Then
            txtCodigoProd.Text = tblDatos.Rows(0).Item(0)
            lblDescripcion.Text = tblDatos.Rows(0).Item(1)
            luBodegas.EditValue = tblDatos.Rows(0).Item(2)
            'deFechaExp.Value = tblDatos.Rows(0).Item(3)
            txtCantidad.Text = tblDatos.Rows(0).Item(3)
            LastExistUnd = tblDatos.Rows(0).Item(3)
            txtCostoUnit.Text = tblDatos.Rows(0).Item(4)
            LastExistMon = (txtCantidad.Text * txtCostoUnit.Text)
            CargarExistProd()
            CargarExistProdBod(tblDatos.Rows(0).Item(2))
        End If

    End Sub

    Function ExistinBodega()
        Dim strExistBodega As String = "SELECT dbo.Almacenes.Codigo_Bodega, dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & luBodegas.EditValue & ")"
        Dim tblExistinBod As DataTable = SQL(strExistBodega, "tblExistinBod", My.Settings.SolIndustrialCNX).Tables(0) ', dbo.Inventario.UndTotal_Existencia as Existencia
        ExistinBodega = 0
        If tblExistinBod.Rows.Count > 0 Then
            ExistinBodega = tblExistinBod.Rows.Count
        End If
    End Function


    Function NomBodeg(ByVal nNumBodega As Integer)
        Dim strsqlNomBod As String
        strsqlNomBod = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & nNumBodega & ")"
        Dim tblNomBod As DataTable = SQL(strsqlNomBod, "tblNomBod", My.Settings.SolIndustrialCNX).Tables(0) ', dbo.Inventario.UndTotal_Existencia as Existencia
        If tblNomBod.Rows.Count > 0 Then
            NomBodeg = tblNomBod.Rows(0).Item(0)
        End If
    End Function

    Private Sub txtCostoUnit_TextChanged(sender As Object, e As EventArgs) Handles txtCostoUnit.TextChanged
        CalcularCostoTotal()
    End Sub

    Sub CalcularCostoTotal()
        lblCostoTotal.Text = Format(CDbl(IIf(txtCostoUnit.Text = "", 0, txtCostoUnit.Text)) * CInt(IIf(txtCantidad.Text = "", 0, txtCantidad.Text)), "###,###,###.00")
    End Sub

    Public Sub CargarBodegas()
        Dim strsqlBodega As String = "SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes"

        TblBodegaBS.DataSource = SQL(strsqlBodega, "tblBodega", My.Settings.SolIndustrialCNX).Tables(0)
        luBodegas.Properties.DataSource = TblBodegaBS
        luBodegas.Properties.DisplayMember = "Nombre_Bodega"
        luBodegas.Properties.ValueMember = "Codigo_Bodega"
        luBodegas.ItemIndex = 0
    End Sub

    Sub CargarExistProd()
        If txtCodigoProd.Text = "" Then
            Exit Sub
        End If
        Dim strsqlExistProd As String
        strsqlExistProd = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "')"
        Tbl_ExistBodegasBS.DataSource = SQL(strsqlExistProd, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)
        EncabezadosDetDoc()
    End Sub

    Sub CargarExistProdBod(ByVal nNumBodega As Integer)
        If txtCodigoProd.Text = "" Then
            Exit Sub
        End If
        Dim strsqlExistProdBod As String
        strsqlExistProdBod = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & nNumBodega & ")"
        Dim tblExistProdBod As DataTable = SQL(strsqlExistProdBod, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)
        If tblExistProdBod.Rows.Count > 0 Then
            lblExistencia.Text = tblExistProdBod.Rows(0).Item(1)
        End If
    End Sub

    Sub EncabezadosDetDoc()
        GridView1.Columns.Clear()
        GridView1.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "Bodega"
        gc0.Caption = "Bodega"
        gc0.FieldName = "Bodega"
        gc0.Width = 50
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Existencia"
        gc1.Caption = "Existencia"
        gc1.FieldName = "Existencia"
        gc1.DisplayFormat.FormatString = "{0:0.00}"
        gc1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc1.Width = 1
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1
        GridView1.Columns(1).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(1).SummaryItem.DisplayFormat = "{0:0.00}"
    End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick
        Dim sNomBodega As String
        If Tbl_ExistBodegasBS.Count > 0 Then
            Dim registro As DataRowView = Tbl_ExistBodegasBS.Current
            sNomBodega = registro.Item(0)
            luBodegas.Text = sNomBodega
            luBodegas.ClosePopup()
            'deFechaExp.Focus()
        End If
    End Sub

    Private Sub luBodegas_EditValueChanged(sender As Object, e As EventArgs) Handles luBodegas.EditValueChanged
        CargarExistProdBod(luBodegas.EditValue)
    End Sub

End Class