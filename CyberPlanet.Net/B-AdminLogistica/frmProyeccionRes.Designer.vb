﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProyeccionRes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.daCerrado = New DevExpress.XtraEditors.DateEdit()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmbProducProducir = New System.Windows.Forms.ComboBox()
        Me.txtCostoTotal = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCostoUnd = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCantidadPro = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.deRegistrado = New DevExpress.XtraEditors.DateEdit()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.CmbSolicitante = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCodigoOrden = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.grdDetalleProy = New DevExpress.XtraGrid.GridControl()
        Me.grvDetalleProy = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.mainPanel.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.daCerrado.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.daCerrado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deRegistrado.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deRegistrado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.grdDetalleProy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalleProy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.Label11)
        Me.mainPanel.Controls.Add(Me.Panel4)
        Me.mainPanel.Controls.Add(Me.Panel1)
        Me.mainPanel.Location = New System.Drawing.Point(0, 0)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1160, 493)
        Me.mainPanel.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Navy
        Me.Label11.Location = New System.Drawing.Point(2, 1)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(358, 15)
        Me.Label11.TabIndex = 80
        Me.Label11.Text = "PROYECCIÓN DE REQUERIMIENTOS DE MATERIALES"
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.daCerrado)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Controls.Add(Me.cmbProducProducir)
        Me.Panel4.Controls.Add(Me.txtCostoTotal)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.txtCostoUnd)
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.txtCantidadPro)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.Label2)
        Me.Panel4.Controls.Add(Me.ckIsActivo)
        Me.Panel4.Controls.Add(Me.deRegistrado)
        Me.Panel4.Controls.Add(Me.lblFechaIngreso)
        Me.Panel4.Controls.Add(Me.CmbSolicitante)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.txtCodigoOrden)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(3, 20)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1157, 77)
        Me.Panel4.TabIndex = 78
        '
        'daCerrado
        '
        Me.daCerrado.EditValue = Nothing
        Me.daCerrado.Location = New System.Drawing.Point(959, 11)
        Me.daCerrado.Name = "daCerrado"
        Me.daCerrado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.daCerrado.Properties.Mask.EditMask = "g"
        Me.daCerrado.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.daCerrado.Size = New System.Drawing.Size(170, 20)
        Me.daCerrado.TabIndex = 76
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(932, 14)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(33, 19)
        Me.Label12.TabIndex = 77
        Me.Label12.Text = "Fin:"
        '
        'cmbProducProducir
        '
        Me.cmbProducProducir.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbProducProducir.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbProducProducir.FormattingEnabled = True
        Me.cmbProducProducir.Location = New System.Drawing.Point(97, 43)
        Me.cmbProducProducir.Name = "cmbProducProducir"
        Me.cmbProducProducir.Size = New System.Drawing.Size(528, 21)
        Me.cmbProducProducir.TabIndex = 73
        '
        'txtCostoTotal
        '
        Me.txtCostoTotal.Enabled = False
        Me.txtCostoTotal.Location = New System.Drawing.Point(959, 43)
        Me.txtCostoTotal.Name = "txtCostoTotal"
        Me.txtCostoTotal.Size = New System.Drawing.Size(68, 20)
        Me.txtCostoTotal.TabIndex = 72
        Me.txtCostoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(910, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "C. Total:"
        '
        'txtCostoUnd
        '
        Me.txtCostoUnd.Enabled = False
        Me.txtCostoUnd.Location = New System.Drawing.Point(829, 44)
        Me.txtCostoUnd.Name = "txtCostoUnd"
        Me.txtCostoUnd.Size = New System.Drawing.Size(68, 20)
        Me.txtCostoUnd.TabIndex = 70
        Me.txtCostoUnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(778, 47)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "Costo U:"
        '
        'txtCantidadPro
        '
        Me.txtCantidadPro.Location = New System.Drawing.Point(696, 44)
        Me.txtCantidadPro.Name = "txtCantidadPro"
        Me.txtCantidadPro.Size = New System.Drawing.Size(68, 20)
        Me.txtCantidadPro.TabIndex = 68
        Me.txtCantidadPro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(640, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Cantidad:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(42, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 65
        Me.Label2.Text = "Producto:"
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Location = New System.Drawing.Point(1069, 44)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Activo"
        Me.ckIsActivo.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.ckIsActivo.Size = New System.Drawing.Size(60, 18)
        Me.ckIsActivo.TabIndex = 64
        '
        'deRegistrado
        '
        Me.deRegistrado.EditValue = Nothing
        Me.deRegistrado.Location = New System.Drawing.Point(696, 12)
        Me.deRegistrado.Name = "deRegistrado"
        Me.deRegistrado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deRegistrado.Properties.Mask.EditMask = "g"
        Me.deRegistrado.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deRegistrado.Size = New System.Drawing.Size(200, 20)
        Me.deRegistrado.TabIndex = 62
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(645, 14)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(52, 16)
        Me.lblFechaIngreso.TabIndex = 63
        Me.lblFechaIngreso.Text = "Inicio:"
        '
        'CmbSolicitante
        '
        Me.CmbSolicitante.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CmbSolicitante.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbSolicitante.FormattingEnabled = True
        Me.CmbSolicitante.Location = New System.Drawing.Point(259, 11)
        Me.CmbSolicitante.Name = "CmbSolicitante"
        Me.CmbSolicitante.Size = New System.Drawing.Size(366, 21)
        Me.CmbSolicitante.TabIndex = 60
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(198, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Solicitante:"
        '
        'txtCodigoOrden
        '
        Me.txtCodigoOrden.Enabled = False
        Me.txtCodigoOrden.Location = New System.Drawing.Point(97, 12)
        Me.txtCodigoOrden.Name = "txtCodigoOrden"
        Me.txtCodigoOrden.Size = New System.Drawing.Size(92, 20)
        Me.txtCodigoOrden.TabIndex = 14
        Me.txtCodigoOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Cod. Proyección:"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.grdDetalleProy)
        Me.Panel1.Location = New System.Drawing.Point(3, 99)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1157, 394)
        Me.Panel1.TabIndex = 75
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.Location = New System.Drawing.Point(3, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(327, 15)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "MATERIALES REQUERIDOS PARA EL PRODUCTO"
        '
        'grdDetalleProy
        '
        Me.grdDetalleProy.Location = New System.Drawing.Point(-1, 27)
        Me.grdDetalleProy.MainView = Me.grvDetalleProy
        Me.grdDetalleProy.Name = "grdDetalleProy"
        Me.grdDetalleProy.Size = New System.Drawing.Size(1157, 363)
        Me.grdDetalleProy.TabIndex = 53
        Me.grdDetalleProy.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalleProy})
        '
        'grvDetalleProy
        '
        Me.grvDetalleProy.GridControl = Me.grdDetalleProy
        Me.grvDetalleProy.Name = "grvDetalleProy"
        Me.grvDetalleProy.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalleProy.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalleProy.OptionsBehavior.Editable = False
        Me.grvDetalleProy.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetalleProy.OptionsView.AllowHtmlDrawGroups = False
        Me.grvDetalleProy.OptionsView.ShowAutoFilterRow = True
        Me.grvDetalleProy.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetalleProy.OptionsView.ShowFooter = True
        Me.grvDetalleProy.OptionsView.ShowGroupPanel = False
        Me.grvDetalleProy.PaintStyleName = "UltraFlat"
        '
        'frmProyeccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1161, 494)
        Me.Controls.Add(Me.mainPanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProyeccion"
        Me.Text = "Proyección de Materiales"
        Me.mainPanel.ResumeLayout(False)
        Me.mainPanel.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.daCerrado.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.daCerrado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deRegistrado.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deRegistrado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdDetalleProy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalleProy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCostoTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnd As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadPro As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents deRegistrado As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents CmbSolicitante As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents grdDetalleProy As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetalleProy As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cmbProducProducir As System.Windows.Forms.ComboBox
    Friend WithEvents daCerrado As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
