﻿Public Class frmBusquedaDoc

    Private Sub frmBusquedaDoc_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            CargarDocumentos()
        Catch ex As Exception

        End Try

    End Sub

    Sub CargarDocumentos()
        Dim strsql As String = ""

        GridView1.Columns.Clear()
        GridControl1.DataSource = Nothing

        If NumCatalogo = 20 Then
            strsql = "Select * from vwComprasLocales"

        ElseIf NumCatalogo = 21 Then
                strsql = "Select * from vwComprasLocales where Anulada=0"

        ElseIf NumCatalogo = 22 Then
            If Me.Text = "Detalle de Pagos" Then
                Dim codigo As String = ""

                codigo = frmMasterCompraForanea.txtNumDocumento.Text
                strsql = "SELECT [Codigo], [Tipo Documento], Fecha, Vencimiento, [Sub-Total], IR, IMI, IVA, [Total Neto], [Otros Gastos], [Multas/Recargos], [Neto Pago] FROM vwDetallePago PAG " & _
                         "WHERE [PAG].[Referencia] = '" & codigo & "' and [PAG].[Tipo Referencia] = 20"

            Else
                strsql = "Select * from vwComprasExtranjeras where Anulada=0"
            End If

        ElseIf NumCatalogo = 23 Then
            strsql = "SELECT * FROM [dbo].[vwVerOrdenProduccion] ODP WHERE [ODP].CodigoSucursal = " & My.Settings.Sucursal & "   order by Orden desc"

        ElseIf NumCatalogo = 24 Then
            strsql = "SELECT Productos.Codigo_Producto AS Codigo, Productos.Nombre_Producto AS Nombre, Linea.Nombre_Linea As Linea, Catego.Nombre_Categoria As Categoria, Marca.Nombre_Marca As Marca, Modelo.Nombre_Modelo As Modelo, "
            strsql = strsql & "(CASE WHEN EXISTS(select TOP 1 Productos.Codigo_Producto from Movimientos_Almaneces_Detalle Mov where CodigoSucursal = 1 and Mov.Codigo_Producto = Productos.Codigo_Producto) THEN 'SI' ELSE 'NO' END) As TieneMov "
            strsql = strsql & "FROM Productos INNER JOIN Linea_Producto Linea ON Productos.Codigo_Linea = Linea.Codigo_Linea INNER JOIN Categoria_Producto Catego ON Productos.Codigo_Linea = Catego.Codigo_Linea AND "
            strsql = strsql & "Productos.Codigo_Categoria = catego.Codigo_Categoria INNER JOIN Tbl_Modelo Modelo ON Productos.Codigo_Marca = Modelo.Id_Marca AND Productos.Codigo_Modelo = Modelo.Id_Modelo INNER JOIN "
            strsql = strsql & "Tbl_Marca Marca ON Productos.Codigo_Marca = Marca.Codigo_Marca ORDER BY Codigo "

        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            Dim Tipo As Integer = 0
            Dim Nombre As String = ""

            If NumCatalogo = 25 Then
                Tipo = 4
                Nombre = "Entrada"
            Else
                Tipo = 5
                Nombre = "Salida"
            End If

            strsql = "SELECT CAST(Mov.Fecha AS Date) AS Fecha,Mov.Bodega 'CodBodega',Alm.Nombre_Bodega 'Bodega',Mov.Codigo_Movimiento As 'Cod. " & Nombre & "',Categorias_Detalle.Descripcion As 'Tipo Referencia', Mov.Codigo_Documento As 'No. Referencia', " & _
                     "Mov.Comentario, Mov.Total_Neto As 'Costo Total' FROM Movimientos_Almacenes AS Mov INNER JOIN Categorias_Detalle ON Mov.Tipo_Documento = Categorias_Detalle.IdCategoriaDetalle " & _
                     "INNER JOIN Almacenes As Alm ON Mov.Bodega = Alm.Codigo_Bodega " & _
                     "WHERE (Mov.Tipo_Movimiento = " & Tipo & ") AND Mov.Anulada = 0 ORDER BY Fecha,Mov.Bodega,Mov.Codigo_Movimiento DESC "
        ElseIf NumCatalogo = 27 Then

            strsql = "SELECT PRY.Codigo_Proyeccion As Codigo,PRY.FechaInicio Fecha, (Tbl_Empleados.Nombres + ' ' + Tbl_Empleados.Apellidos) As Solicitante,  pry.Observacion, (case when PRY.Activo=1 then 'NO' when PRY.Activo=0 then 'SI' end) Generado, isnull(CAST(FechaFin as varchar(20)), 'PENDIENTE') as Aplicado FROM Proyeccion PRY INNER JOIN Tbl_Empleados ON PRY.Solicitante = Tbl_Empleados.Id_Empleado"


        ElseIf NumCatalogo = 29 Then
            strsql = "SELECT CAST(Registrado AS DATE) As 'Fecha', COUNT(Codigo_Orden_Produc) As 'Cant. Ordenes', sum(Costo_Elaborado) As 'Costo Total' FROM Orden_Produccion WHERE Estado = 2029 Group by CAST(Registrado AS DATE)"

        ElseIf NumCatalogo = 30 Then
            strsql = "Select * from vwVerCIFBuscar Order By Anio,Mes DESC"

        ElseIf NumCatalogo = 32 Then
            Me.Text = "REMISION"
            strsql = "select * from vw_Remision"
        ElseIf NumCatalogo = 33 Then 'FACTURACION/ solicitud de factura
            If Nuevo = 1 Then
                Me.Text = "SOLICITUD DE FACTURACION PENDIENTE"

                strsql = "select distinct FACT.Id_Factura_Solicitud, DFACT.Id_Remision as 'Remision', FACT.Fecha, isnull(fact.Id_Cliente,'-') as 'Id Cliente', FACT.Cliente as 'Nombre Cliente' from Tbl_Factura_Solicitud FACT inner join [Tbl_Factura_Solicitud.Detalle] DFACT on DFACT.Id_Factura_Solicitud=FACT.Id_Factura_Solicitud and FACT.Id_Sucursal=DFACT.Id_Sucursal and FACT.Id_Remision=DFACT.Id_Remision"

                'strsql = "select SFD.Id_Remision as 'Remision', SF.Fecha, SF.Cliente from [Tbl_Factura_Solicitud.Detalle] SFD inner join Tbl_Factura_Solicitud SF on  sf.Id_Factura_Solicitud=SFD.Id_Factura_Solicitud and SF.Id_Sucursal=SFD.Id_Sucursal and SF.Id_Remision=SFD.Id_Remision where SFD.Id_Factura is null order by SFD.Id_Remision"
            ElseIf Nuevo = 0 Then
                Me.Text = "Buscar Facturas Registradas"
                strsql = "select Codigo_Factura as 'Factura', Serie_Factura as 'Serie', Facturas.Id_Remision as 'Remision', Configuracion.Sucursales.Nombre as 'Sucursal', Facturas.Fecha_Factura as 'Fecha', isnull(cast(Facturas.Fecha_Venc_Factura as varchar (10)),'PENDIENTE') as 'Fecha Vencimiento', (case when Estado_Factura = 1 then 'RESERVADA' when Estado_Factura=2 then 'EMITIDA' when Estado_Factura=3 then 'IMPRESA' when Estado_Factura=4 then 'ANULADA' end) as Estado from Facturas inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Facturas.Codigo_Sucursal"
            End If
        ElseIf NumCatalogo = 35 Then
            strsql = "SELECT  Id_Empleado 'Codigo', (Nombres + ' ' +  Apellidos) Empleado, NombreCargo 'Cargo',  Nombre_Area 'Area' FROM vwVerEmpleados Where Activo = 1"

        ElseIf NumCatalogo = 34 Then
            strsql = "select distinct cast(cr.Fecha as date) as 'Fecha', (select count(Id_Cliente) from Tbl_TransUnion CANT where cast(cant.Fecha as date)=cast(cr.Fecha as date)) as 'Clientes Verificados', (select count(Id_Cliente) from Tbl_TransUnion CANT where cast(cant.Fecha as date)=cast(cr.Fecha as date) and cant.Estado=0) as 'Clientes Pendientes', (select count(Id_Cliente) from Tbl_TransUnion CANT where cast(cant.Fecha as date)=cast(cr.Fecha as date) and cant.Estado=2) as 'Clientes Aprobados', (select count(Id_Cliente) from Tbl_TransUnion CANT where cast(cant.Fecha as date)=cast(cr.Fecha as date) and cant.Estado=1) as 'Clientes Denegados' from Tbl_TransUnion CR order by cast(cr.Fecha as date) desc"

        ElseIf NumCatalogo = 38 Then

            strsql = "SELECT Ped.Codigo_Pedido AS Codigo, suc.Nombre as  'Sucursal', cast(Ped.Fecha as date) as 'Fecha', (Tbl_Proveedor.Nombre_Proveedor) Proveedor, Ped.Concepto, Ped.Taza_Cambio, Det.Estado, isnull(Ped.Referencia,'--') as 'Referencia' FROM Orden_Pedido AS Ped inner join Configuracion.Sucursales Suc on Suc.Key_Sucursal=Ped.CodigoSucursal INNER JOIN Categorias_Detalle AS Det ON Ped.Estado = Det.IdCategoriaDetalle and det.CodCategoria='08' INNER JOIN Tbl_Proveedor ON Ped.Codigo_Proveedor = Tbl_Proveedor.Codigo_Proveedor " & _
                     "where Ped.CodigoSucursal= " & My.Settings.Sucursal & ""

        ElseIf NumCatalogo = 39 Then
            strsql = "SELECT Trasl.Codigo_Traslado AS Codigo, cast(Trasl.Fecha as date) as Fecha, isnull(Trasl.Referencia, '--') as Referencia, Alm.Nombre_Bodega AS 'Bodega Origen', AlmDest.Nombre_Bodega AS 'Bodega Destino', Trasl.Comentario, (case when Trasl.Activo=1 then 'SI' else 'NO' end) as 'Activo', (CASE WHEN Aplicado = 1 THEN 'SI' ELSE 'NO' END) AS Aplicado FROM Orden_Traslado Trasl INNER JOIN Almacenes Alm ON Trasl.CodigoSucursal = Alm.Codigo_Sucursal AND Trasl.CodigoSucursal = Alm.Codigo_Sucursal AND Trasl.Bodega_Origen = Alm.Codigo_Bodega INNER JOIN Almacenes AS AlmDest ON Trasl.CodigoSucursal = AlmDest.Codigo_Sucursal AND Trasl.CodigoSucursal = AlmDest.Codigo_Sucursal AND Trasl.Bodega_Destino = AlmDest.Codigo_Bodega WHERE (Trasl.CodigoSucursal = " & My.Settings.Sucursal & ") ORDER BY Codigo DESC"
        ElseIf NumCatalogo = 40 Then
            strsql = "select * from vw_EndoseMaster"
        ElseIf NumCatalogo = 41 Then
            strsql = "Select * from vw_VerificacionMaster"
        ElseIf NumCatalogo = 51 Then
            strsql = "Select * from vw_MasterComprobantes"
        End If

        Dim TableDatos As DataTable = SQL(strsql, "tblDocumentos", My.Settings.SolIndustrialCNX).Tables(0)
        GridControl1.DataSource = TableDatos

        If NumCatalogo = 23 Then
            EncabezadoOrdenProd()
        ElseIf NumCatalogo = 24 Then
            EncabezadoKardex()
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            EncabezadoEntradaSalida()
        ElseIf NumCatalogo = 51 Then
            EncabezadoComprob()
        ElseIf NumCatalogo = 32 Then
            'GridView1.Columns("Entrada").Visible = False
            'GridView1.Columns("Salida").Visible = False
            GridView1.Columns("Pre").Visible = False
            GridView1.Columns("Id_Ruta").Visible = False
            GridView1.Columns("Id_Coordinador").Visible = False
            GridView1.Columns("Id_Supervisor").Visible = False
            GridView1.Columns("Id_Vehiculo").Visible = False
            GridView1.Columns("Id_Estado").Visible = False
            GridView1.Columns("Id_Bodega").Visible = False
        End If
        GridView1.BestFitColumns()

    End Sub
    Sub EncabezadoOrdenProd()
        GridView1.Columns("CodigoSucursal").Visible = False
    End Sub

    Sub EncabezadoKardex()
        GridView1.Columns("TieneMov").Visible = False
    End Sub

    Sub EncabezadoEntradaSalida()
        GridView1.Columns("Fecha").Width = 50

        If NumCatalogo = 25 Then
            GridView1.Columns("Cod. Entrada").Width = 50
        Else
            GridView1.Columns("Cod. Salida").Width = 50
        End If

        GridView1.Columns("CodBodega").Visible = False
        GridView1.Columns("Bodega").Width = 100
        GridView1.Columns("No. Referencia").Width = 60
        GridView1.Columns("Tipo Referencia").Width = 85
        GridView1.Columns("Comentario").Width = 200

        GridView1.Columns("Costo Total").Width = 80
        GridView1.Columns("Costo Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        GridView1.Columns("Costo Total").DisplayFormat.FormatString = "{0:C$ #,##0.0000}"
    End Sub

    Sub EncabezadoComprob()
        GridView1.Columns.Clear()
        GridView1.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "Num_Comprobante"
        gc0.Caption = "Num_Comprobante"
        gc0.FieldName = "Num_Comprobante"
        gc0.Width = 15
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Fecha"
        gc1.Caption = "Fecha"
        gc1.FieldName = "Fecha"
        gc1.Width = 15
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1

        Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
        gc2.Name = "Descripcion"
        gc2.Caption = "Descripcion"
        gc2.FieldName = "Descripcion"
        gc2.Width = 90
        GridView1.Columns.Add(gc2)
        gc2.Visible = True
        gc2.VisibleIndex = 2

        Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
        gc3.Name = "Monto"
        gc3.Caption = "Monto"
        gc3.FieldName = "Monto"
        gc3.DisplayFormat.FormatString = "{0:$0.00}"
        gc3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc3.Width = 15
        GridView1.Columns.Add(gc3)
        gc3.Visible = True
        gc3.VisibleIndex = 3
        GridView1.Columns(3).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

        Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
        gc4.Name = "Tipo_Comprobante"
        gc4.Caption = "Tipo_Comprobante"
        gc4.FieldName = "Tipo_Comprobante"
        gc4.Width = 15
        GridView1.Columns.Add(gc4)
        gc4.Visible = True
        gc4.VisibleIndex = 4

    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        Aceptar()
    End Sub
    Sub Aceptar()
        Try
            If GridView1.RowCount > 0 Then
                'Dim registro As DataRowView = Tbl_BusqDocBS.Current
                Dim Codigo As String = ""
                Dim Dato1 As String = ""
                Dim Dato2 As String = ""

                If NumCatalogo = 21 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Numero_de_Compra").ToString()
                    Dato1 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Tipo_Operacion").ToString()
                    Dato2 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre_Proveedor").ToString()

                    '  frmMasterCompraLocal.CargarDetalleDoc(My.Settings.Sucursal, Codigo, IIf(Dato1 = "Contado", 1, 2), GetCodigoProveedor(Dato2))

                    '------------------------------- Aqui estuvo bob!  -----------------------------------------------------------------------

                    compra_local.CargarDetalleDoc(My.Settings.Sucursal, Codigo, IIf(Dato1 = "Contado", 1, 2), GetCodigoProveedor(Dato2))

                    '--------------------------------------------------------------------------------------------------------------------------

                    Cerrar()
                ElseIf NumCatalogo = 22 Then
                    If Me.Text = "Detalle de Pagos" Then
                        FrmPagos.ShowDialog()
                    Else
                        Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Numero_de_Documento").ToString()
                        Dato1 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Tipo_Operacion").ToString()
                        Dato2 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre_Proveedor").ToString()
                        '  frmMasterCompraForanea.CargarDetalleDoc(My.Settings.Sucursal, Codigo, IIf(Dato1 = "Contado", 1, 2), GetCodigoProveedor(Dato2))
                        Cerrar()
                    End If
                ElseIf NumCatalogo = 23 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Orden").ToString()
                    frmOrdenProd.CargarDatos(Codigo)
                    Cerrar()
                ElseIf NumCatalogo = 24 Then
                    Try
                        Dato1 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "TieneMov").ToString()
                    Catch ex As Exception
                    End Try

                    If Dato1 = "SI" Then
                        Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                        frmKardexInv.CargarDatos(Codigo)
                        Cerrar()
                    Else
                        frmKardexInv.Mensaje()
                    End If

                ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then

                    If NumCatalogo = 25 Then
                        Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Cod. Entrada").ToString()
                    Else
                        Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Cod. Salida").ToString()
                    End If

                    Dato1 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "CodBodega").ToString()
                    frmEntradaSalida.CargarDatos(Codigo, Dato1)
                    Cerrar()
                ElseIf NumCatalogo = 27 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                    frmProyeccion.CargarDatos(Codigo)
                    Cerrar()

                ElseIf NumCatalogo = 29 Then
                    frmCierreOrden.deRegistrado.Value = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Fecha").ToString()
                    Cerrar()
                ElseIf NumCatalogo = 30 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                    Dato1 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Mes").ToString()
                    Dato2 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Anio").ToString()
                    frmAdminCIF.CargarDatos(Codigo, Dato2, Dato1)
                    Cerrar()
                ElseIf NumCatalogo = 32 Then
                    FrmRemision.CargaF()
                    Me.Close()
                ElseIf NumCatalogo = 33 Then
                    If Nuevo = 1 Then
                        With FrmFactura
                            .txtRemision.Text = ""
                            .txtCnombre.Text = ""
                            .txtCapellido.Text = ""
                            .dgvDetalle.Rows.Clear()

                            If GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Id Cliente").ToString() <> "-" Then
                                .txtCcodigo.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Id Cliente").ToString()

                            Else

                                Try
                                    Dim space1 As Integer = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre Cliente").ToString().IndexOf(" ")
                                    Dim space2 As Integer = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre Cliente").ToString().IndexOf(" ", space1 + 1)
                                    .txtCnombre.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre Cliente").ToString().Substring(0, space2)
                                    .txtCapellido.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre Cliente").ToString().Substring(space2 + 1)
                                Catch ex As Exception
                                    .txtCnombre.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre Cliente").ToString()
                                End Try

                            End If

                            .txtRemision.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Remision").ToString()
                            .cbEstado.SelectedIndex = 0
                            .CodFactura()

                        End With
                    ElseIf Nuevo = 0 Then
                        With FrmFactura
                            Try
                                .Cargar(GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Factura").ToString(), GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Serie").ToString())
                            Catch ex As Exception
                            End Try
                        End With
                    End If
                ElseIf NumCatalogo = 34 Then

                    Dim strsql = "select CR.Id_Cliente as 'Codigo' , cast(cr.Fecha as date) as 'Fecha', CAST(CR.Fecha AS time) AS 'Hora'  , cr.Nombre , cr.Apellido , cr.Cedula , isnull(ALM.Nombre_Bodega, 'PENDIENTE') as 'Ruta' , BAR.Nombre_Barrio as 'Barrio' , cr.Observacion , (case when cr.Dimar=1 then 'SI' when cr.Dimar=0 then '' end) as 'Dimar' , (case when cr.Inficsa=1 then 'SI' when cr.Inficsa=0 then '' end) as 'Inficsa' , (case when Estado=0 then 'PENDIENTE' when Estado=1 then 'NO APROBADO' when Estado=2 then 'APROBADO' end) as 'Estado' from Tbl_TransUnion CR left join Almacenes ALM on ALM.Codigo_Bodega=cr.Id_Ruta left join Tbl_Barrios BAR on BAR.Id_Barrio=CR.Id_Barrio where cast(cr.Fecha as date)='" & GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Fecha") & "'"

                    frmCatalogo.GridView1.BestFitColumns()
                    ' Dim criterio As String = frmCatalogo.GridView1.ActiveFilterString
                    'pendiente restablecer text de filtro aplicado

                    Dim tblDatosCatalogo As DataTable = Nothing
                    frmCatalogo.GridView1.Columns.Clear()
                    frmCatalogo.TblCatalogosBS.DataSource = SQL(strsql, "tblDatosCatalogo", My.Settings.SolIndustrialCNX).Tables(0)

                    Cerrar()

                ElseIf NumCatalogo = 35 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                    frmSeguridadUsuariosAlta.cargarEmpleado(Codigo)
                    Cerrar()

                ElseIf NumCatalogo = 38 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                    frmOrdenPedido.CargarDatos(Codigo)
                    Cerrar()

                ElseIf NumCatalogo = 39 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString()
                    frmTraslados.CargarDatos(Codigo)
                    Cerrar()
                ElseIf NumCatalogo = 40 Or NumCatalogo = 41 Then
                    With frmEndoseFactura
                        .txtCodigo.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Codigo").ToString
                        .dtpFecha.Value = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Fecha").ToString
                        .txtConcepto.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Concepto").ToString
                        .txtMonto.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Monto").ToString
                        .txtnFactura.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Cant. Facturas").ToString
                        .txtObservacion.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Observacion").ToString
                        .lbEstado.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Estado").ToString
                    End With
                ElseIf NumCatalogo = 51 Then
                    Codigo = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Num_Comprobante").ToString()
                    Dato1 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Tipo_Comprobante").ToString()
                    Dato2 = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Fecha").ToString()
                    frmEditorComprobantes.CargarDetalleComprob(Codigo, GetTipoComprob(Dato1), Dato2)
                    Cerrar()
                End If
            End If
            Me.Dispose()
            Me.Close()
        Catch ex As Exception
        End Try
    End Sub

Function GetTipoComprob(ByVal sTipoComprob As String)
      Dim strsqlTipComp As String = ""
      strsqlTipComp = "SELECT Id_TipoComprobante FROM Tbl_TipoComprobante WHERE Tipo_Comprobante='" & sTipoComprob & "'"
      Dim tblTipComp As DataTable = SQL(strsqlTipComp, "tblTipComp", My.Settings.SolIndustrialCNX).Tables(0)
      If tblTipComp.Rows.Count <> 0 Then
          GetTipoComprob = tblTipComp.Rows(0).Item(0)
      End If
End Function

Function GetCodigoProveedor(ByVal sNomProv As String)
      Dim strsqlProv As String = ""
      strsqlProv = "SELECT Codigo_Proveedor FROM Tbl_Proveedor WHERE Nombre_Proveedor='" & sNomProv & "'"
      Dim tblCodProv As DataTable = SQL(strsqlProv, "tblCodProv", My.Settings.SolIndustrialCNX).Tables(0)
      If tblCodProv.Rows.Count <> 0 Then
          GetCodigoProveedor = tblCodProv.Rows(0).Item(0)
      End If
End Function

Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs)
  Aceptar()
End Sub
    Private Sub GridControl1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles GridControl1.MouseDoubleClick
        Aceptar()
    End Sub
    Private Sub frmBusquedaDoc_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Dispose()
        Close()
    End Sub
End Class