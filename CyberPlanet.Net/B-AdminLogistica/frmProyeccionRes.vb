﻿Imports System.Xml
Imports DevExpress.XtraGrid.Columns
Imports System.Threading

Public Class frmProyeccionRes
    Public IdEstado As Integer
    Public ResumMaster As String = ""
    Public ResumMDetalle As String = ""
    Public ResumEntrada As String = ""
    Public ResumSalida As String = ""
    Public ResumEntradaDeta As String = ""
    Public ResumSalidaDeta As String = ""
    Public CodMaxEntrada As String = ""
    Public CodMaxSalida As String = ""
    Public CodTipo As Integer = 0

    Public Sub CargarDatos(ByVal Codigo As String)
        Dim strsql As String = ""

        Try
            strsql = "SELECT Codigo_Orden_Produc, Solicitante, Registrado, Cerrado, Activo, Codigo_Producto, Cantidad_Requerida, " & _
                     "Cantidad_Elaborado FROM Orden_Produccion where Codigo_Orden_Produc = '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatosOrden", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count <> 0 Then

                txtCodigoOrden.Text = tblDatos.Rows(0).Item(0)
                CmbSolicitante.SelectedValue = tblDatos.Rows(0).Item(1)
                deRegistrado.Text = tblDatos.Rows(0).Item(2)
                daCerrado.Text = IIf(tblDatos.Rows(0).Item(3) Is DBNull.Value, "", tblDatos.Rows(0).Item(3))
                ckIsActivo.Checked = tblDatos.Rows(0).Item(4)
                cmbProducProducir.SelectedValue = tblDatos.Rows(0).Item(5)
                txtCantidadPro.Text = tblDatos.Rows(0).Item(6)
                'txtElaborado.Text = tblDatos.Rows(0).Item(7)

                activarControles(2)
                'obtenerEstado(Codigo)
                'CargarRemision(Codigo)
                'CargarFinal(Codigo)
                'cambiarTexto()

            End If
        Catch ex As Exception
        End Try

    End Sub

    Public Sub cargarSolicitante()
        CmbSolicitante.DataSource = SQL("SELECT  Codigo_Supervisor AS 'Codigo', Nombre_Supervisor AS 'Nombre Completo' FROM  Supervisores WHERE (Activo = 1) ORDER BY 'Codigo'", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
        CmbSolicitante.DisplayMember = "Nombre Completo"
        CmbSolicitante.ValueMember = "Codigo"
        'CmbSolicitante.SelectedIndex = 0
    End Sub

    Private Sub cargarProductoFinal()
        cmbProducProducir.DataSource = SQL("select Codigo_Producto AS Codigo, (Codigo_Producto + ' - ' + Nombre_Producto) AS Nombre from [dbo].[Productos] where Id_ProductosTipo in (1,2) order by Codigo_Producto asc", "tblProductos", My.Settings.SolIndustrialCNX).Tables(0)
        cmbProducProducir.DisplayMember = "Nombre"
        cmbProducProducir.ValueMember = "Codigo"
    End Sub

    'Public Sub CargarFinal(ByVal Codigo As String)
    '    Dim strsql As String = ""

    '    Try
    '        strsql = "SELECT [Cod. Producto], [Nombre Producto], Requerido, Elaborado, Medida, [C. Estimado], [C. No Utilizado], [C. Real] " & _
    '                 "FROM  vwVerOrdenProductoFinal where [Cod. OrdenProd] =  '" & Codigo & "'"

    '        'grdFinal.DataSource = SQL(strsql, "tblDatosOrden", My.Settings.SolIndustrialCNX).Tables(0)

    '    Catch ex As Exception
    '    End Try
    'End Sub

    'Public Sub cambiarTexto()
    '    Dim strsql As String = "SELECT PRDT.Id_ProductosTipo, PRDT.Descripion, Unidades_Medida.Descripcion_Unidad As Medida FROM Productos AS PRD " & _
    '                           "INNER JOIN Productos_Tipo AS PRDT ON PRD.Id_ProductosTipo = PRDT.Id_ProductosTipo INNER JOIN Unidades_Medida ON " & _
    '                           "PRD.Codigo_UnidMedida = Unidades_Medida.Codigo_UnidadMed WHERE (PRD.Codigo_Producto = '" + cmbProducProducir.SelectedValue.ToString + "')"

    '    Dim tblDatos As DataTable = SQL(strsql, "tblDato", My.Settings.SolIndustrialCNX).Tables(0)

    '    If tblDatos.Rows.Count <> 0 Then
    '        txtUnidadMed.Text = tblDatos.Rows(0).Item(2)
    '        CodTipo = tblDatos.Rows(0).Item(0)

    '        Select Case CodTipo
    '            Case 0
    '                lblTextoFinal.Text = "MATERIA PRIMA ELABORADO"
    '            Case 1
    '                lblTextoFinal.Text = "PRODUCTO SEMI-PROCESADO ELABORADO"
    '            Case 2
    '                lblTextoFinal.Text = "PRODUCTO TERMINADO ELABORADO"
    '            Case Else
    '                lblTextoFinal.Text = "SIN ESPECIFICAR"
    '        End Select
    '    End If
    'End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",") Then
            e.Handled = True
        ElseIf e.KeyChar = "," Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub CargarLista()
        Dim strsql As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        grvDetalleProy.Columns.Clear()

        If (Convert.ToInt32(cmbProducProducir.SelectedValue(0)) > 0) Then
            strsql = "exec [dbo].[SP_GET_Proyeccion_MaterialesMod] '" & cmbProducProducir.SelectedValue.ToString & "', '" & txtCantidadPro.Text & "'"

            grdDetalleProy.DataSource = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
            'grvDetalleProy.Columns(0).Width = 30
            'grvDetalleProy.Columns(1).Width = 30
            'grvDetalleProy.Columns(2).Width = 30
            'grvDetalleProy.Columns(3).Width = 30
            'grvDetalleProy.Columns(4).Width = 40
            'grvDetalleProy.Columns(5).Width = 40
            grvDetalleProy.Columns(6).SummaryItem.DisplayFormat = "{0:$ #.####}"
            grvDetalleProy.Columns(8).SummaryItem.DisplayFormat = "{0:$ #.####}"

            grvDetalleProy.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvDetalleProy.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:$ #.####}"
            grvDetalleProy.Columns("Costo Requerimiento").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            grvDetalleProy.Columns("Costo Requerimiento").SummaryItem.DisplayFormat = "{0:$ #.####}"
            grvDetalleProy.OptionsView.ShowFooter = True

            strsql = "SELECT [dbo].[getCostoProducto](Codigo_Producto) As Costo FROM Productos WHERE Codigo_Producto = '" & cmbProducProducir.SelectedValue.ToString & "'"
            Dim Data As DataTable = SQL(strsql, "tblEstado", My.Settings.SolIndustrialCNX).Tables(0)

            If Data.Rows.Count > 0 Then
                txtCostoUnd.Text = Data.Rows(0).Item(0).ToString
            Else
                txtCostoUnd.Text = 0
            End If

            txtCostoTotal.Text = (Convert.ToInt32(IIf(txtCantidadPro.Text = "", "0", txtCantidadPro.Text)) * Convert.ToDecimal(txtCostoUnd.Text)).ToString
        End If
    End Sub

    'Private Sub CargarListaNoUtilizada()
    '    Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
    '    Dim strsql As String = ""
    '    Dim Cantidad As Integer = 0
    '    Dim Codigo As String = ""
    '    Dim CantidadDet As String = ""
    '    Dim Costo As String = ""

    '    grvRemision.Columns.Clear()
    '    Cantidad = (CInt(txtCantidadPro.Text) - CInt(txtElaborado.Text))

    '    If (Convert.ToInt32(cmbProducProducir.SelectedValue(0)) > 0) And Cantidad >= 0 And IdEstado = 1 Then

    '        Try

    '            strsql = "exec [dbo].[SP_GET_Receta_MateriaPrima] '" & cmbProducProducir.SelectedValue.ToString & "', '" & Cantidad & "'"
    '            Dim Table As DataTable = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)

    '            For i As Integer = 0 To Table.Rows.Count - 1
    '                Codigo = Table.Rows(i).Item(0).ToString
    '                CantidadDet = Table.Rows(i).Item(1).ToString
    '                Costo = Table.Rows(i).Item(4).ToString

    '                ResumMDetalle = Produccion.EditarOrdenDetalle(txtCodigoOrden.Text, Codigo, 0, CantidadDet, 4, Costo, 2)
    '            Next i

    '            CargarRemision(txtCodigoOrden.Text)
    '        Catch ex As Exception
    '        End Try
    '    ElseIf Cantidad < 0 Then
    '        MsgBox("La cantidad elaborada no puede ser mayor que: " & txtCantidadPro.Text)
    '        txtElaborado.Text = txtCantidadPro.Text
    '        txtElaborado.Select()
    '    End If
    'End Sub

    'Sub updateNoUtilizado()
    '    Dim Resum As String = ""
    '    Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
    '    Dim Codigo As String = ""
    '    Dim Cantidad As String = ""
    '    Dim Costo As String = ""

    '    Try
    '        For i As Integer = 0 To grvRemision.DataRowCount - 1
    '            Codigo = grvRemision.GetRowCellValue(i, "Codigo")
    '            Cantidad = grvRemision.GetRowCellValue(i, "Cant.")
    '            Costo = grvRemision.GetRowCellValue(i, "Costo Und")

    '            ResumMDetalle = Produccion.EditarOrdenDetalle(txtCodigoOrden.Text, Codigo, 0, Cantidad, 4, Costo, 2)
    '        Next i

    '        CargarRemision(txtCodigoOrden.Text)
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub activarControles(ByVal nTipoEdic As Integer)

        If nTipoEdic = 1 Or nTipoEdic = 0 Then
            deRegistrado.Enabled = True
            daCerrado.Enabled = True

            'daCerrado.Enabled = False
            'btnRegistrar.Enabled = True

            'pnlAgregar.Enabled = False
            'pnlRemision.Enabled = False
            'pnlTerminados.Enabled = False

            CmbSolicitante.Enabled = True
            cmbProducProducir.Enabled = True
            'btnRegistrar.Enabled = True
            txtCantidadPro.Enabled = True

        Else
            If nTipoEdic = 2 Then
                deRegistrado.Enabled = False
                daCerrado.Enabled = True
                CmbSolicitante.Enabled = False
                cmbProducProducir.Enabled = False
                'btnRegistrar.Enabled = False
                txtCantidadPro.Enabled = False

                'pnlAgregar.Enabled = True
                'pnlRemision.Enabled = True
                'pnlTerminados.Enabled = True

                daCerrado.DateTime = Now

            End If
        End If

    End Sub

    'Private Sub obtenerEstado(ByVal Codigo As String)
    '    Dim strsql As String = ""
    '    Dim valor As String = ""

    '    strsql = "SELECT TOP 1 CD.IdCategoriaDetalle, CD.Estado FROM Orden_Produccion OP INNER JOIN Categorias_Detalle CD ON OP.Estado = CD.IdCategoriaDetalle WHERE (OP.Activo = 1) AND (OP.Codigo_Orden_Produc = '" & Codigo & "')"
    '    Dim Data As DataTable = SQL(strsql, "tblEstado", My.Settings.SolIndustrialCNX).Tables(0)

    '    If Data.Rows.Count > 0 Then
    '        IdEstado = Data.Rows(0).Item(0)
    '        lblEstado.Text = Data.Rows(0).Item(1).ToString
    '    Else
    '        lblEstado.Text = "SIN REGISTRAR"
    '    End If

    '    Select Case IdEstado
    '        Case 1
    '            pnlAgregar.Enabled = True
    '            btnCerrarOrden.Enabled = False
    '            txtElaborado.Enabled = True
    '            btnLiquidar.Enabled = True
    '        Case 3
    '            pnlAgregar.Enabled = False
    '            btnCerrarOrden.Enabled = True
    '            txtElaborado.Enabled = False
    '            btnLiquidar.Enabled = False
    '        Case 19
    '            pnlAgregar.Enabled = False
    '            btnCerrarOrden.Enabled = False
    '            txtElaborado.Enabled = False
    '            btnLiquidar.Enabled = False
    '    End Select

    'End Sub

    'Private Sub CargarRemision(ByVal CodigoOrden As String)
    '    Dim strsql As String = ""
    '    Dim strsql2 As String = ""

    '    If txtElaborado.Text <= 0 Then
    '        CodigoOrden = "NO EXISTE"
    '    End If

    '    Try
    '        strsql = "SELECT PRD.Codigo_Producto As 'Codigo', ORD.Cantidad_NoUtilizada AS 'Cant.', PRD.Nombre_Producto As 'Materia Prima', UMD.Descripcion_Unidad As Medida, " & _
    '                 "[dbo].[getCostoProducto](PRD.Codigo_Producto) AS 'Costo Und.' , (ORD.Cantidad_NoUtilizada * [dbo].[getCostoProducto](PRD.Codigo_Producto)) AS 'Costo Total' " & _
    '                 "FROM Orden_Produccion_Detalle ORD INNER JOIN Productos PRD ON ORD.Codigo_Producto = PRD.Codigo_Producto INNER JOIN Unidades_Medida UMD ON PRD.Codigo_UnidMedida = UMD.Codigo_UnidadMed " & _
    '                 "where ORD.Tipo_Detalle = 4 and ORD.Cantidad_NoUtilizada > 0 and ORD.Codigo_Orden_Produc = '" & CodigoOrden & "' order by PRD.Codigo_Producto "

    '        grdRemision.DataSource = SQL(strsql, "tblDatosRemision", My.Settings.SolIndustrialCNX).Tables(0)
    '        grvRemision.Columns(0).Width = 30
    '        grvRemision.Columns(1).Width = 30
    '        grvRemision.Columns(3).Width = 30
    '        grvRemision.Columns(4).Width = 40
    '        grvRemision.Columns(5).Width = 40

    '        grvRemision.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
    '        grvRemision.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:#.####}"
    '        grvRemision.OptionsView.ShowFooter = True

    '    Catch ex As Exception
    '    End Try


    'End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Public Sub nuevo()
        nTipoEdic = 1
        activarControles(nTipoEdic)
        cargarSolicitante()
        cargarProductoFinal()

        txtCodigoOrden.Text = Microsoft.VisualBasic.Right("000000" & RegistroMaximo(), 6)
        'obtenerEstado(txtCodigoOrden.Text)
        ckIsActivo.Checked = True
        deRegistrado.DateTime = Now
        'txtCantidad.Text = 0
        'txtElaborado.Text = 0
        txtCostoUnd.Text = 0
        txtCostoTotal.Text = 0
        txtCantidadPro.Text = 1

        'CargarFinal(txtCodigoOrden.Text)
    End Sub

    Private Sub frmOrdenProd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strsql As String = ""

        activarControles(nTipoEdic)
        cargarSolicitante()
        cargarProductoFinal()

        txtCodigoOrden.Text = Microsoft.VisualBasic.Right("000000" & RegistroMaximo(), 6)
        'obtenerEstado(txtCodigoOrden.Text)
        ckIsActivo.Checked = True
        deRegistrado.DateTime = Now
        'txtCantidad.Text = 0
        txtCantidadPro.Text = 1
        'txtElaborado.Text = 0
        txtCostoUnd.Text = 0
        txtCostoTotal.Text = 0

    End Sub

    Private Sub cmbProducProducir_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProducProducir.SelectedIndexChanged
        CargarLista()
    End Sub

    Private Sub txtCantidadPro_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidadPro.KeyPress
        NumerosyDecimal(txtCantidadPro, e)
    End Sub

    Private Sub txtCantidadPro_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadPro.TextChanged
        CargarLista()
    End Sub

    Private Sub btnRegistrar_Click(sender As Object, e As EventArgs)

        'Dim Codigo As String = ""
        'Dim Cantidad As String = ""
        'Dim Costo As String = ""
        'Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        'Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        'Try
        '    If CmbSolicitante.SelectedValue > 0 Then

        '        Dim result As Integer = MessageBox.Show("Desea registrar la orden de producción?", "Confirmación de Registro", MessageBoxButtons.YesNo)
        '        If result = DialogResult.Yes Then

        '            ResumMaster = Produccion.EditarOrdenProduccion(txtCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), txtCostoTotal.Text, 0, 0, deRegistrado.DateTime, CType(Nothing, DateTime?), 1, ckIsActivo.Checked, 1)

        '            CodMaxSalida = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
        '            ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, 1, txtCodigoOrden.Text, 17, "SALIDA DE BODEGA DE MATERIA PRIMA", txtCostoTotal.Text, deRegistrado.DateTime, 5, False, 1)

        '            CodMaxEntrada = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
        '            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, 1, txtCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS", txtCostoTotal.Text, deRegistrado.DateTime, 4, False, 1)

        '            If ResumMaster = "OK" Then

        '                For i As Integer = 0 To grvDetalle.DataRowCount - 1
        '                    Codigo = grvDetalle.GetRowCellValue(i, "Codigo")
        '                    Cantidad = grvDetalle.GetRowCellValue(i, "Cant.")
        '                    Costo = grvDetalle.GetRowCellValue(i, "Costo Und")

        '                    ResumMDetalle = Produccion.EditarOrdenDetalle(txtCodigoOrden.Text, Codigo, Cantidad, 0, 4, Costo, 1)

        '                    ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, Codigo, Cantidad, Costo, "", 2, False, 1)
        '                    ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, Codigo, Cantidad, Costo, "", 3, False, 1)

        '                Next i
        '            Else
        '                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
        '            End If

        '            activarControles(2)
        '            'obtenerEstado(txtCodigoOrden.Text)
        '            'CargarRemision(txtCodigoOrden.Text)
        '        End If
        '    Else
        '        Dim mensaje As String = ("Debe seleccionar los siguientes datos:" + Chr(13) + Chr(13) + "- Solicitante" + Chr(13) + "- Producto a elaborar")
        '        MsgBox(mensaje, MsgBoxStyle.Information, "Información")
        '    End If

        'Catch ex As Exception
        '    Call MsgBox("Error: " + ex.Message)
        'Finally
        'End Try
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs)
        'Dim Resum As String = ""
        'Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        'Try
        '    Dim CodigoProducto As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
        '    Dim Cantidad As Integer = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cant.")
        '    Dim Costo As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Und")

        '    If txtCantidad.Text > 0 And txtCantidad.Text <= Cantidad Then
        '        Dim CantUtilizada As Integer = Convert.ToInt32(txtCantidad.Text)
        '        Resum = Produccion.EditarOrdenDetalle(txtCodigoOrden.Text, CodigoProducto, 0, CantUtilizada, 4, Costo, 1)
        '        If Resum = "OK" Then
        '            txtCantidad.Text = 0
        '            CargarRemision(txtCodigoOrden.Text)
        '        Else
        '            MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
        '        End If
        '    Else
        '        MsgBox(("La cantidad debe ser mayor a 0 y menor o igual que " & Cantidad.ToString & "."), MsgBoxStyle.Information, "SIGCA")
        '        txtCantidad.Text = 0
        '        txtCantidad.Focus()
        '    End If
        'Catch ex As Exception
        '    Call MsgBox("Error: " + ex.Message)
        'Finally
        'End Try
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs)
        'Dim Resum As String = ""
        'Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        'Try
        '    Dim CodigoProducto As String = grvRemision.GetRowCellValue(grvRemision.GetSelectedRows(0), "Codigo").ToString()
        '    Dim Cantidad As Integer = grvRemision.GetRowCellValue(grvRemision.GetSelectedRows(0), "Cant.")
        '    Dim Costo As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Und")

        '    If txtCantidad.Text > 0 Then
        '        Dim CantUtilizada As Integer = Convert.ToInt32(txtCantidad.Text)

        '        If Cantidad > txtCantidad.Text Then
        '            Resum = Produccion.EditarOrdenDetalle(txtCodigoOrden.Text, CodigoProducto, 0, (Cantidad - CantUtilizada), 4, Costo, 2)
        '        Else
        '            Resum = Produccion.EditarOrdenDetalle(txtCodigoOrden.Text, CodigoProducto, 0, 0, 4, Costo, 2)
        '        End If

        '        If Resum = "OK" Then
        '            txtCantidad.Text = 0
        '            CargarRemision(txtCodigoOrden.Text)
        '        Else
        '            MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
        '        End If
        '    Else
        '        MsgBox("La cantidad debe ser mayor a 0.", MsgBoxStyle.Information, "Información")
        '        txtCantidad.Text = 0
        '    End If

        'Catch ex As Exception
        '    Call MsgBox("Error: " + ex.Message)
        'Finally
        'End Try
    End Sub

    'Private Sub grdDetalle_DoubleClick(sender As Object, e As EventArgs) Handles grdDetalle.DoubleClick
    '    txtCantidad.Focus()
    '    txtCantidad.SelectAll()
    'End Sub

    'Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    NumerosyDecimal(txtCantidad, e)
    'End Sub

    'Private Sub btnLiquidar_Click(sender As Object, e As EventArgs)
    '    Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
    '    Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

    '    If IdEstado = 1 Then
    '        Dim result As Integer = MessageBox.Show("Desea liquidar la orden de producción?", "Confirmación de Liquidación", MessageBoxButtons.YesNo)
    '        If result = DialogResult.Yes Then
    '            Try
    '                Dim Codigo As String = ""
    '                Dim Cantidad As String = ""
    '                Dim Costo As String = ""
    '                Dim CostoTotal As Double = 0

    '                If Integer.Parse(txtElaborado.Text) > 0 And Integer.Parse(txtElaborado.Text) <= Integer.Parse(txtCantidadPro.Text) Then
    '                    If grvRemision.RowCount <= 0 And Integer.Parse(txtElaborado.Text) < Integer.Parse(txtCantidadPro.Text) Then
    '                        MsgBox("Debe detallar los materiales no utilizados.", MsgBoxStyle.Information, "Información")
    '                    Else

    '                        ResumMaster = Produccion.EditarOrdenProduccion(txtCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), txtCostoTotal.Text, Convert.ToInt32(txtElaborado.Text), (txtCostoTotal.Text - CostoTotal), deRegistrado.DateTime, CType(Nothing, DateTime?), 3, ckIsActivo.Checked, 2)
    '                        CargarFinal(txtCodigoOrden.Text)

    '                        If grvRemision.RowCount > 0 Then

    '                            For i As Integer = 0 To grvRemision.DataRowCount - 1
    '                                CostoTotal = CostoTotal + grvRemision.GetRowCellValue(i, "Costo Total")
    '                            Next

    '                            CodMaxSalida = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
    '                            ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, 1, txtCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCTOS EN PROCESOS", CostoTotal, deRegistrado.DateTime, 5, False, 1)

    '                            CodMaxEntrada = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
    '                            ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, 1, txtCodigoOrden.Text, 17, "ENTRADA A BODEGA DE MATERIA PRIMA", CostoTotal, deRegistrado.DateTime, 4, False, 1)

    '                            If ResumMaster = "OK" Then

    '                                For i As Integer = 0 To grvRemision.DataRowCount - 1
    '                                    Codigo = grvRemision.GetRowCellValue(i, "Codigo")
    '                                    Cantidad = grvRemision.GetRowCellValue(i, "Cant.")
    '                                    Costo = grvRemision.GetRowCellValue(i, "Costo Und.")

    '                                    ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, Codigo, Cantidad, Costo, "", 3, False, 1)
    '                                    ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, Codigo, Cantidad, Costo, "", 2, False, 1)

    '                                Next i

    '                            End If

    '                            If grvFinal.RowCount > 0 Then
    '                                Costo = grvFinal.GetRowCellValue(0, "C. Real")

    '                                CodMaxEntrada = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
    '                                ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, 1, txtCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS EN PROCESOS", Costo, Date.Now, 4, False, 1)

    '                                ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, Codigo, Cantidad, Costo, "", 3, False, 1)
    '                            End If

    '                        End If

    '                        If ResumMaster = "OK" Then
    '                            MsgBox("Liquidación de Orden Realizada.", MsgBoxStyle.Information, "Operacion Finalizada")
    '                            CargarDatos(txtCodigoOrden.Text)
    '                            'txtElaborado.Text = 0
    '                        End If

    '                    End If
    '                Else
    '                    MsgBox("La cantidad elaborada debe ser mayor a 0 y menor o igual que " + txtCantidadPro.Text, MsgBoxStyle.Information, "Información")
    '                    'txtElaborado.Text = 0
    '                    txtElaborado.Focus()
    '                    txtElaborado.SelectAll()
    '                End If

    '            Catch ex As Exception

    '            End Try
    '        End If
    '    Else
    '        MsgBox("Solo se pueden liquidar ordenes en proceso.", MsgBoxStyle.Information, "Información")
    '    End If
    'End Sub

    'Private Sub txtElaborado_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    NumerosyDecimal(txtElaborado, e)
    'End Sub

    'Private Sub btnCerrarOrden_Click(sender As Object, e As EventArgs)
    '    Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
    '    Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
    '    Dim Codigo As String = ""
    '    Dim Cantidad As String = ""
    '    Dim Costo As String = ""

    '    If IdEstado = 3 Then
    '        Dim result As Integer = MessageBox.Show("Desea cerrar la orden de producción?", "Confirmación de Cierre", MessageBoxButtons.YesNo)
    '        If result = DialogResult.Yes Then
    '            Try
    '                If grvFinal.RowCount > 0 Then
    '                    ResumMaster = Produccion.EditarOrdenProduccion(txtCodigoOrden.Text, CmbSolicitante.SelectedValue, cmbProducProducir.SelectedValue.ToString, Convert.ToInt32(txtCantidadPro.Text), grvFinal.GetRowCellValue(0, "C. Estimado"), Convert.ToInt32(grvFinal.GetRowCellValue(0, "Elaborado")), grvFinal.GetRowCellValue(0, "C. Real"), deRegistrado.DateTime, daCerrado.DateTime, 19, ckIsActivo.Checked, 2)

    '                    For i As Integer = 0 To grvFinal.DataRowCount - 1
    '                        Codigo = grvFinal.GetRowCellValue(i, "Cod. Producto")
    '                        Cantidad = grvFinal.GetRowCellValue(i, "Elaborado")
    '                        Costo = grvFinal.GetRowCellValue(i, "C. Real")

    '                        CodMaxSalida = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
    '                        ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, 1, txtCodigoOrden.Text, 17, "SALIDA DE BODEGA DE PRODUCTOS EN PROCESOS", Costo, deRegistrado.DateTime, 5, False, 1)

    '                        CodMaxEntrada = Microsoft.VisualBasic.Right("000000" & Inventario.RegMaxMovimiento(), 6)
    '                        ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, 1, txtCodigoOrden.Text, 17, "ENTRADA A BODEGA DE PRODUCTOS TERMINADOS", Costo, deRegistrado.DateTime, 4, False, 1)

    '                        ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, Codigo, Cantidad, (Costo / Cantidad), "", 3, False, 1)

    '                        Select Case CodTipo
    '                            Case 1
    '                                ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, Codigo, Cantidad, (Costo / Cantidad), "", 2, False, 1)
    '                            Case 2
    '                                ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, Codigo, Cantidad, (Costo / Cantidad), "", 4, False, 1)
    '                        End Select

    '                    Next i

    '                    If ResumMaster = "OK" Then
    '                        MsgBox("Cierre de Orden Realizada.", MsgBoxStyle.Information, "Operacion Finalizada")
    '                        CargarDatos(txtCodigoOrden.Text)
    '                    End If
    '                End If
    '            Catch ex As Exception
    '            End Try
    '        End If
    '    Else
    '        MsgBox("Solo se pueden cerrar ordenes liquidadas.", MsgBoxStyle.Information, "Información")
    '    End If
    'End Sub

    'Private Sub txtElaborado_TextChanged(sender As Object, e As EventArgs)
    '    CargarListaNoUtilizada()
    'End Sub
End Class