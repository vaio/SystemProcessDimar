﻿Imports System.Data.SqlClient

Public Class Asignacion_Vendedor

    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)

    Dim Codigo As Integer
    Dim dt As DataRow


    Private Sub Asignacion_Vendedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        With frmVehiculo.GridView1
            Codigo = .GetRowCellValue(.GetSelectedRows(0), "Codigo").ToString()
        End With

        Carga_Datos()
        Carga_Vendedor()
        Carga_Vendedor_Ruta()

    End Sub
    Sub Carga_Datos()

        Dim dt As DataTable = New DataTable()

        dt = SQL("select * from vw_Vehiculo_Asignacion_Inicial where Id_Ruta=" & Codigo & "", "tbl_Datos", My.Settings.SolIndustrialCNX).Tables(0)

        For Each row0 In dt.Rows

            txtcodigo.Text = row0("Id_Ruta")
            txtRuta.Text = row0("Ruta")
            txtSucursal.Text = row0("Sucursal")
            txtCoordinador.Text = row0("Coordinador")
            txtsupervisor.Text = row0("Supervisor")

            If row0("Activo") = True Then txtEstado.Text = ("Activo") Else txtEstado.Text = ("No Activo")

        Next

    End Sub

    Sub Carga_Vendedor()

        GridControl1.DataSource = vbNull
        GridView1.Columns.Clear()
        GridControl1.DataSource = SQL("select * from vw_Asignacion_Vendedor_Inicial where not Id_Ruta=" & Codigo & "", "tbl_Vendedor", My.Settings.SolIndustrialCNX).Tables(0)
        GridView1.BestFitColumns()
        GridView1.GroupPanelText = "Vendedores Activos"

        GridView1.Columns("Codigo").Visible = False
        GridView1.Columns("Id_Ruta").Visible = False

    End Sub

    Sub Carga_Vendedor_Ruta()

        GridControl2.DataSource = vbNull
        GridView2.Columns.Clear()
        GridControl2.DataSource = SQL("select * from vw_Asignacion_Vendedor_Inicial where Id_Ruta=" & Codigo & "", "tbl_Vendedor", My.Settings.SolIndustrialCNX).Tables(0)
        GridView2.BestFitColumns()
        GridView2.GroupPanelText = "Vendedores en Ruta"

        GridView2.Columns("Codigo").Visible = False
        GridView2.Columns("Id_Ruta").Visible = False

    End Sub

    Public Sub AddRow(ByVal view1 As DevExpress.XtraGrid.Views.Grid.GridView, ByVal grid1 As DevExpress.XtraGrid.GridControl, view2 As DevExpress.XtraGrid.Views.Grid.GridView, ByVal grid2 As DevExpress.XtraGrid.GridControl)

        Dim currentRow = view1.FocusedRowHandle

        Dim newRow As DataRow = (TryCast(grid2.DataSource, DataTable)).NewRow()

        newRow("Codigo") = view1.GetRowCellValue(currentRow, "Codigo")
        newRow("Nombre") = view1.GetRowCellValue(currentRow, "Nombre")
        newRow("Id_Ruta") = view1.GetRowCellValue(currentRow, "Id_Ruta")
        newRow("Ruta Actual") = view1.GetRowCellValue(currentRow, "Ruta Actual")
        newRow("Ultima Fecha") = view1.GetRowCellValue(currentRow, "Ultima Fecha")

        view1.DeleteRow(currentRow)

        TryCast(grid2.DataSource, DataTable).Rows.Add(newRow)

        grid2.RefreshDataSource()

    End Sub


    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick

        AddRow(GridView1, GridControl1, GridView2, GridControl2)

    End Sub

    Private Sub GridControl2_DoubleClick(sender As Object, e As EventArgs) Handles GridControl2.DoubleClick

        AddRow(GridView2, GridControl2, GridView1, GridControl1)

    End Sub

    Private Sub Aplicar(ByVal edicion As Integer, ruta As Integer, empleado As Integer)

        Try
            con.Open()

            Dim cmd As New SqlCommand("SP_Ruta_Vendedor", con)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd.Parameters

                .AddWithValue("@edicion", SqlDbType.Int).Value = edicion
                .AddWithValue("@sucursal", SqlDbType.Int).Value = My.Settings.Sucursal
                .AddWithValue("@ruta", SqlDbType.Int).Value = ruta
                .AddWithValue("@empleado", SqlDbType.Int).Value = empleado
                .AddWithValue("@usuario", SqlDbType.Int).Value = UserID

            End With
            cmd.ExecuteReader()
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        For i = 0 To GridView2.RowCount - 1

            Aplicar(nTipoEdic, Codigo, GridView2.GetRowCellValue(i, "Codigo"))

        Next

        Dim CodigoS As String = Codigo

        For i = 0 To GridView1.RowCount - 1     'MEJORAR ESTO

            Dim id_ruta As String = GridView1.GetRowCellValue(i, "Id_Ruta").ToString

            If id_ruta = CodigoS Then

                Aplicar(3, Codigo, GridView1.GetRowCellValue(i, "Codigo"))

            End If

        Next

        Me.Dispose()
        Me.Close()

    End Sub
End Class