﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBusquedaProd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBusquedaProd))
        Me.Tbl_BusqProdBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gridRemision = New DevExpress.XtraGrid.GridControl()
        Me.VvRemisionProductoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.B_WorkSystemDataSet2 = New SIGMA.B_WorkSystemDataSet2()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCodigo_Producto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombre_Producto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUndTotal_Existencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExistencia_Minima = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.B_WorkSystemDataSet1 = New SIGMA.B_WorkSystemDataSet1()
        Me.B_WorkSystemDataSet = New SIGMA.B_WorkSystemDataSet()
        Me.VvRemision_ProductoTableAdapter = New SIGMA.B_WorkSystemDataSet2TableAdapters.vvRemision_ProductoTableAdapter()
        CType(Me.Tbl_BusqProdBS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VvRemisionProductoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B_WorkSystemDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B_WorkSystemDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B_WorkSystemDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 328)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(637, 49)
        Me.Panel1.TabIndex = 12
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(542, 8)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(78, 35)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(458, 8)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(78, 35)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(637, 377)
        Me.GridControl1.TabIndex = 17
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.GridView1.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowViewCaption = True
        Me.GridView1.ViewCaption = "Catalogo de Productos"
        '
        'gridRemision
        '
        Me.gridRemision.DataSource = Me.VvRemisionProductoBindingSource
        Me.gridRemision.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridRemision.Location = New System.Drawing.Point(0, 0)
        Me.gridRemision.MainView = Me.GridView2
        Me.gridRemision.Name = "gridRemision"
        Me.gridRemision.Size = New System.Drawing.Size(637, 377)
        Me.gridRemision.TabIndex = 18
        Me.gridRemision.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'VvRemisionProductoBindingSource
        '
        Me.VvRemisionProductoBindingSource.DataMember = "vvRemision_Producto"
        Me.VvRemisionProductoBindingSource.DataSource = Me.B_WorkSystemDataSet2
        '
        'B_WorkSystemDataSet2
        '
        Me.B_WorkSystemDataSet2.DataSetName = "B_WorkSystemDataSet2"
        Me.B_WorkSystemDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCodigo_Producto, Me.colNombre_Producto, Me.colUndTotal_Existencia, Me.colExistencia_Minima})
        Me.GridView2.GridControl = Me.gridRemision
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowColumnMoving = False
        Me.GridView2.OptionsCustomization.AllowColumnResizing = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        '
        'colCodigo_Producto
        '
        Me.colCodigo_Producto.FieldName = "Codigo_Producto"
        Me.colCodigo_Producto.Name = "colCodigo_Producto"
        Me.colCodigo_Producto.Visible = True
        Me.colCodigo_Producto.VisibleIndex = 0
        '
        'colNombre_Producto
        '
        Me.colNombre_Producto.FieldName = "Nombre_Producto"
        Me.colNombre_Producto.Name = "colNombre_Producto"
        Me.colNombre_Producto.Visible = True
        Me.colNombre_Producto.VisibleIndex = 1
        '
        'colUndTotal_Existencia
        '
        Me.colUndTotal_Existencia.FieldName = "UndTotal_Existencia"
        Me.colUndTotal_Existencia.Name = "colUndTotal_Existencia"
        Me.colUndTotal_Existencia.Visible = True
        Me.colUndTotal_Existencia.VisibleIndex = 2
        '
        'colExistencia_Minima
        '
        Me.colExistencia_Minima.FieldName = "Existencia_Minima"
        Me.colExistencia_Minima.Name = "colExistencia_Minima"
        Me.colExistencia_Minima.Visible = True
        Me.colExistencia_Minima.VisibleIndex = 3
        '
        'B_WorkSystemDataSet1
        '
        Me.B_WorkSystemDataSet1.DataSetName = "B_WorkSystemDataSet1"
        Me.B_WorkSystemDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'B_WorkSystemDataSet
        '
        Me.B_WorkSystemDataSet.DataSetName = "B_WorkSystemDataSet"
        Me.B_WorkSystemDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VvRemision_ProductoTableAdapter
        '
        Me.VvRemision_ProductoTableAdapter.ClearBeforeFill = True
        '
        'frmBusquedaProd
        '
        Me.AcceptButton = Me.cmdAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(637, 377)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.gridRemision)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBusquedaProd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Busqueda de Documentos"
        CType(Me.Tbl_BusqProdBS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridRemision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VvRemisionProductoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B_WorkSystemDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B_WorkSystemDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B_WorkSystemDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Tbl_BusqProdBS As System.Windows.Forms.BindingSource
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gridRemision As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents B_WorkSystemDataSet As SIGMA.B_WorkSystemDataSet
    Friend WithEvents colCodigo_Producto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombre_Producto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUndTotal_Existencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExistencia_Minima As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents B_WorkSystemDataSet1 As SIGMA.B_WorkSystemDataSet1
    Friend WithEvents B_WorkSystemDataSet2 As SIGMA.B_WorkSystemDataSet2
    Friend WithEvents VvRemisionProductoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VvRemision_ProductoTableAdapter As SIGMA.B_WorkSystemDataSet2TableAdapters.vvRemision_ProductoTableAdapter
End Class
