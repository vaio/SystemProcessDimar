﻿Public Class FrmSolicitud_Garantia
    Public Sub FrmSolicitud_Garantia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If FrmRemision.cbTipo.SelectedIndex = 0 And FrmRemision.cbvTipo.SelectedIndex = 0 Then
            txtSolicitud.Enabled = True
        Else
            txtSolicitud.Enabled = False
        End If
        lbProducto.Text = FrmRemision.dgvEntrada.Rows(FrmRemision.dgvEntrada.CurrentRow.Index).Cells(0).Value
        lbCliente.Text = FrmRemision.luCliente.Text
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim bandera1 As Boolean = False
        If txtSolicitud.Enabled = True Then
            If txtGarantia.Text <> "" And txtSolicitud.Text <> "" Then
                If FrmRemision.dgvVenta.RowCount > 0 Then
                    For i = 0 To FrmRemision.dgvVenta.RowCount - 1
                        If txtSolicitud.Text = FrmRemision.dgvVenta.Rows(i).Cells(3).Value Then
                            bandera1 = True
                        End If
                        If txtGarantia.Text = FrmRemision.dgvVenta.Rows(i).Cells(14).Value Then
                            bandera1 = True
                        End If
                    Next
                End If
                If bandera1 = False Then
                    FrmRemision.Solicitud = txtSolicitud.Text
                    FrmRemision.Garantia = txtGarantia.Text
                    txtSolicitud.Text = ""
                    txtGarantia.Text = ""
                    txtSolicitud.Focus()
                    Me.Close()
                Else
                    MsgBox("Datos invalidos")
                End If
            End If
        Else
            If txtGarantia.Text <> "" Then
                If FrmRemision.dgvVenta.RowCount > 0 Then
                    For i = 0 To FrmRemision.dgvVenta.RowCount - 1
                        If txtGarantia.Text = FrmRemision.dgvVenta.Rows(i).Cells(14).Value Then
                            bandera1 = True
                        End If
                    Next
                End If
                If bandera1 = False Then
                    FrmRemision.Garantia = txtGarantia.Text
                    txtGarantia.Text = ""
                    txtGarantia.Focus()
                    Me.Close()
                End If
            End If
        End If
    End Sub

End Class