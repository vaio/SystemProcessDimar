﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmVehiculo_Edicion
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Private Sub frmVehiculo_Edicion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If frmVehiculo.Edicion = 1 Then
            txtOdoF.ReadOnly = True
            txtOdoF.TabStop = False
        Else
            txtOdoI.ReadOnly = True
            txtOdoF.ReadOnly = True
            'txtOdoF.TabStop = True
            Cargar_Datos()
        End If
    End Sub
    Sub Cargar_Datos()
        With frmVehiculo.GridView1
            txtcodigo.Text = .GetRowCellValue(.GetSelectedRows(0), "Codigo").ToString()
            txtPlaca.Text = .GetRowCellValue(.GetSelectedRows(0), "Placa").ToString()
            txtMarca.Text = .GetRowCellValue(.GetSelectedRows(0), "Marca").ToString()
            txtModelo.Text = .GetRowCellValue(.GetSelectedRows(0), "Modelo").ToString()
            dtpFecha.Value = .GetRowCellValue(.GetSelectedRows(0), "Adquisicion").ToString()
            txtOdoI.Text = .GetRowCellValue(.GetSelectedRows(0), "KM inicial").ToString()
            txtOdoF.Text = .GetRowCellValue(.GetSelectedRows(0), "KM Actual").ToString()
            cbEstado.SelectedItem = .GetRowCellValue(.GetSelectedRows(0), "Estado").ToString()
            cbTipo.SelectedItem = .GetRowCellValue(.GetSelectedRows(0), "Tipo").ToString()
        End With
    End Sub
    Private Sub txtOdoI_TextChanged(sender As Object, e As EventArgs) Handles txtOdoI.TextChanged
        If frmVehiculo.Edicion = 1 Then
            txtOdoF.Text = txtOdoI.Text
        End If
    End Sub
    Private Sub txtOdoF_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOdoI.KeyPress, txtOdoF.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim Resum As String
        Resum = frmVehiculo.EditarVehiculo(frmVehiculo.Edicion, txtcodigo.Text, txtPlaca.Text, txtMarca.Text, txtModelo.Text, dtpFecha.Text, txtOdoI.Text, txtOdoF.Text, cbEstado.SelectedIndex + 1, cbTipo.SelectedIndex + 1)
        If Resum = "OK" Then
            MsgBox("Registro Guardado")
            con.Close()
            Me.Dispose()
            Me.Close()
        End If
    End Sub
End Class