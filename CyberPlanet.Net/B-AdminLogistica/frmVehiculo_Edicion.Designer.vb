﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVehiculo_Edicion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVehiculo_Edicion))
        Me.Pcontrol = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbEstado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.gbDatos = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtOdoI = New System.Windows.Forms.TextBox()
        Me.txtOdoF = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPlaca = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtcodigo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.txtModelo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TblCatalogosBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Pcontrol.SuspendLayout()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbDatos.SuspendLayout()
        CType(Me.TblCatalogosBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Pcontrol
        '
        Me.Pcontrol.Controls.Add(Me.btnCancelar)
        Me.Pcontrol.Controls.Add(Me.btnAceptar)
        Me.Pcontrol.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Pcontrol.Location = New System.Drawing.Point(0, 146)
        Me.Pcontrol.Name = "Pcontrol"
        Me.Pcontrol.Size = New System.Drawing.Size(493, 49)
        Me.Pcontrol.TabIndex = 17
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Image = CType(resources.GetObject("btnCancelar.Image"), System.Drawing.Image)
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(398, 8)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(78, 35)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAceptar.Location = New System.Drawing.Point(314, 8)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(78, 35)
        Me.btnAceptar.TabIndex = 7
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(302, 51)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(31, 13)
        Me.Label9.TabIndex = 143
        Me.Label9.Text = "Tipo:"
        '
        'cbEstado
        '
        Me.cbEstado.EditValue = "ACTIVO"
        Me.cbEstado.Location = New System.Drawing.Point(339, 25)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.cbEstado.Properties.Appearance.Options.UseForeColor = True
        Me.cbEstado.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbEstado.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbEstado.Properties.AppearanceDisabled.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cbEstado.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbEstado.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEstado.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbEstado.Properties.Items.AddRange(New Object() {"ACTIVO", "BAJA", "REPARACION", "MANTENIMIENTO"})
        Me.cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbEstado.Size = New System.Drawing.Size(110, 20)
        Me.cbEstado.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(290, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Estado:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(127, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Fecha:"
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "dd/MM/yy"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFecha.Location = New System.Drawing.Point(173, 22)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(69, 20)
        Me.dtpFecha.TabIndex = 6
        Me.dtpFecha.TabStop = False
        '
        'gbDatos
        '
        Me.gbDatos.Controls.Add(Me.Label10)
        Me.gbDatos.Controls.Add(Me.txtOdoI)
        Me.gbDatos.Controls.Add(Me.txtOdoF)
        Me.gbDatos.Controls.Add(Me.Label8)
        Me.gbDatos.Controls.Add(Me.txtPlaca)
        Me.gbDatos.Controls.Add(Me.Label6)
        Me.gbDatos.Controls.Add(Me.cbTipo)
        Me.gbDatos.Controls.Add(Me.Label9)
        Me.gbDatos.Controls.Add(Me.cbEstado)
        Me.gbDatos.Controls.Add(Me.Label5)
        Me.gbDatos.Controls.Add(Me.Label7)
        Me.gbDatos.Controls.Add(Me.dtpFecha)
        Me.gbDatos.Controls.Add(Me.txtcodigo)
        Me.gbDatos.Controls.Add(Me.Label1)
        Me.gbDatos.Controls.Add(Me.txtMarca)
        Me.gbDatos.Controls.Add(Me.txtModelo)
        Me.gbDatos.Controls.Add(Me.Label3)
        Me.gbDatos.Controls.Add(Me.Label2)
        Me.gbDatos.Location = New System.Drawing.Point(12, 6)
        Me.gbDatos.Name = "gbDatos"
        Me.gbDatos.Size = New System.Drawing.Size(467, 134)
        Me.gbDatos.TabIndex = 16
        Me.gbDatos.TabStop = False
        Me.gbDatos.Text = "Nuevo Vehiculo"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(283, 103)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 13)
        Me.Label10.TabIndex = 149
        Me.Label10.Text = "Km Final:"
        '
        'txtOdoI
        '
        Me.txtOdoI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOdoI.Location = New System.Drawing.Point(339, 74)
        Me.txtOdoI.MaxLength = 50
        Me.txtOdoI.Name = "txtOdoI"
        Me.txtOdoI.Size = New System.Drawing.Size(109, 20)
        Me.txtOdoI.TabIndex = 7
        Me.txtOdoI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOdoF
        '
        Me.txtOdoF.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOdoF.Location = New System.Drawing.Point(339, 100)
        Me.txtOdoF.MaxLength = 50
        Me.txtOdoF.Name = "txtOdoF"
        Me.txtOdoF.Size = New System.Drawing.Size(109, 20)
        Me.txtOdoF.TabIndex = 8
        Me.txtOdoF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(278, 77)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 13)
        Me.Label8.TabIndex = 147
        Me.Label8.Text = "Km Inicial:"
        '
        'txtPlaca
        '
        Me.txtPlaca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPlaca.Location = New System.Drawing.Point(69, 100)
        Me.txtPlaca.MaxLength = 50
        Me.txtPlaca.Name = "txtPlaca"
        Me.txtPlaca.Size = New System.Drawing.Size(173, 20)
        Me.txtPlaca.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(26, 103)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 145
        Me.Label6.Text = "Placa:"
        '
        'txtcodigo
        '
        Me.txtcodigo.Location = New System.Drawing.Point(69, 22)
        Me.txtcodigo.Name = "txtcodigo"
        Me.txtcodigo.ReadOnly = True
        Me.txtcodigo.Size = New System.Drawing.Size(48, 20)
        Me.txtcodigo.TabIndex = 0
        Me.txtcodigo.TabStop = False
        Me.txtcodigo.Text = "NUEVO"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Codigo:"
        '
        'txtMarca
        '
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.Location = New System.Drawing.Point(69, 48)
        Me.txtMarca.MaxLength = 50
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Size = New System.Drawing.Size(173, 20)
        Me.txtMarca.TabIndex = 1
        '
        'txtModelo
        '
        Me.txtModelo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelo.Location = New System.Drawing.Point(69, 74)
        Me.txtModelo.MaxLength = 50
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Size = New System.Drawing.Size(173, 20)
        Me.txtModelo.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Modelo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Marca:"
        '
        'cbTipo
        '
        Me.cbTipo.EditValue = "OPERATIVO"
        Me.cbTipo.Location = New System.Drawing.Point(339, 48)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.cbTipo.Properties.Appearance.Options.UseForeColor = True
        Me.cbTipo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbTipo.Properties.AppearanceDisabled.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbTipo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTipo.Properties.Items.AddRange(New Object() {"OPERATIVO", "ADMINISTRATIVO"})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(110, 20)
        Me.cbTipo.TabIndex = 5
        '
        'frmVehiculo_Edicion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(493, 195)
        Me.Controls.Add(Me.Pcontrol)
        Me.Controls.Add(Me.gbDatos)
        Me.Name = "frmVehiculo_Edicion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmVehiculo_Edicion"
        Me.Pcontrol.ResumeLayout(False)
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbDatos.ResumeLayout(False)
        Me.gbDatos.PerformLayout()
        CType(Me.TblCatalogosBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Pcontrol As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents TblCatalogosBS As System.Windows.Forms.BindingSource
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbEstado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbDatos As System.Windows.Forms.GroupBox
    Friend WithEvents txtcodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents txtModelo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtOdoF As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtOdoI As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPlaca As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
End Class
