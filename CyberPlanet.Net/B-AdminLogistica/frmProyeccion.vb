﻿Imports System.Xml
Imports DevExpress.XtraGrid.Columns
Imports System.Threading
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmProyeccion
    Public IdEstado As Integer
    Public ResumMaster As String = ""
    Public ResumMDetalle As String = ""
    Public ResumEntrada As String = ""
    Public ResumSalida As String = ""
    Public ResumEntradaDeta As String = ""
    Public ResumSalidaDeta As String = ""
    Public CodMaxEntrada As String = ""
    Public CodMaxSalida As String = ""
    Public CodTipo As Integer = 0
    Dim C_requerido As Color = Color.Gold
    Dim C_existencias As Color = Color.Coral

    '-----------
    'Private Articulos As New frmBusqueda_Prod("cod_bodega", 2, SqlDbType.Int)|
    '------

    Public Sub CargarDatos(ByVal Codigo As String)
        Dim strsql As String = ""
        Dim FechaFin As String
        Try
            strsql = "SELECT Codigo_Proyeccion, Solicitante, FechaInicio, isnull(cast(FechaFin as varchar(20)),0) as 'FechaFin', Tipo, Activo, Observacion FROM Proyeccion where Codigo_Proyeccion =  '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatosOrden", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count <> 0 Then

                txtCodigoOrden.Text = tblDatos.Rows(0).Item("Codigo_Proyeccion")
                CmbSolicitante.SelectedValue = tblDatos.Rows(0).Item("Solicitante")
                deRegistrado.Text = tblDatos.Rows(0).Item("FechaInicio")
                FechaFin = tblDatos.Rows(0).Item("FechaFin")
                CmbTipo.SelectedValue = tblDatos.Rows(0).Item("Tipo")
                ckIsActivo.Checked = tblDatos.Rows(0).Item("Activo")
                txtObservacion.Text = tblDatos.Rows(0).Item("Observacion")

                If FechaFin <> "0" Then
                    daCerrado.Text = FechaFin
                    lbEstado.Visible = True
                    lbFechaF.Visible = True
                    lbFechaF.Text = Convert.ToDateTime(FechaFin).ToString
                    lbFechaF.ForeColor = Color.Blue
                Else
                    lbFechaF.Text = "PENDIENTE"
                    lbFechaF.Visible = True
                    lbEstado.Visible = False
                    lbFechaF.ForeColor = C_existencias
                End If

                CargarDetalle(Codigo)

                activarControles(2)

                'If ckIsActivo.Checked = False Then btnAgregar.Enabled = False : btnQuitar.Enabled = False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Public Sub CargarDetalle(ByVal Codigo As String)
        Dim strsql As String = ""

        Try

            strsql = "SELECT Proy.Codigo_Producto As 'Codigo', Prod.Nombre_Producto As 'Nombre Producto', Proy.Cantidad, Proy.Costo_Undidad As 'Costo Und', Proy.Costo_Total As 'Costo Total' " & _
                     "FROM Proyeccion_Detalle Proy INNER JOIN Productos Prod ON Proy.Codigo_Producto = Prod.Codigo_Producto WHERE Codigo_Proyeccion = '" & Codigo & "' and Sucursal='" & My.Settings.Sucursal & "'"


            Dim Detalle1 As DataTable = SQL(strsql, "tblProyectado", My.Settings.SolIndustrialCNX).Tables(0)
            grdProyeccion.DataSource = Detalle1

            'grvProyeccion.Columns("Codigo").Width = 75
            'grvProyeccion.Columns("Nombre Producto").Width = 250
            'grvProyeccion.Columns("Cantidad").Width = 70

            'grvProyeccion.Columns("Costo Und").Width = 100
            'grvProyeccion.Columns("Costo Und").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            'grvProyeccion.Columns("Costo Und").DisplayFormat.FormatString = "{0:###,###,##0.0000}"

            'grvProyeccion.Columns("Costo Total").Width = 100
            'grvProyeccion.Columns("Costo Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            'grvProyeccion.Columns("Costo Total").DisplayFormat.FormatString = "{0:###,###,##0.0000}"
            'grvProyeccion.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            'grvProyeccion.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:###,###.0000}"

            With grvProyeccion
                .Columns("Codigo").BestFit()
                .Columns("Nombre Producto").BestFit()
                .Columns("Cantidad").BestFit()
                .Columns("Costo Und").BestFit()
                .Columns("Costo Total").BestFit()

            End With

            grvProyeccion.OptionsView.ShowFooter = True

            CargaExistencias(Codigo)

        Catch ex As Exception

        End Try
    End Sub
    Public Sub CargaExistencias(ByVal Codigo As String)

        Dim strsql = "exec [dbo].[SP_GET_Proyeccion] '" + Codigo + "', '" & My.Settings.Sucursal & "'"

        Dim Detalle2 As DataTable = SQL(strsql, "tblMateriales", My.Settings.SolIndustrialCNX).Tables(0)
        grdDetProyeccion.DataSource = Detalle2

        'grvDetProyeccion.Columns("Codigo").Width = 75
        'grvDetProyeccion.Columns("Producto").Width = 350
        'grvDetProyeccion.Columns("Unidad").Width = 55

        'grvDetProyeccion.Columns("Existencia").Width = 100
        'grvDetProyeccion.Columns("Existencia").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        'grvDetProyeccion.Columns("Existencia").DisplayFormat.FormatString = "{0:###,###,##0.0000}"

        'grvDetProyeccion.Columns("Cantidad").Width = 100
        'grvDetProyeccion.Columns("Cantidad").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        'grvDetProyeccion.Columns("Cantidad").DisplayFormat.FormatString = "{0:###,###,##0.0000}"

        'grvDetProyeccion.Columns("Costo Unitario").Width = 100
        'grvDetProyeccion.Columns("Costo Unitario").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        'grvDetProyeccion.Columns("Costo Unitario").DisplayFormat.FormatString = "{0:###,###,##0.0000}"

        'grvDetProyeccion.Columns("Costo Total").Width = 100
        'grvDetProyeccion.Columns("Costo Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        'grvDetProyeccion.Columns("Costo Total").DisplayFormat.FormatString = "{0:###,###,##0.0000}"
        'grvDetProyeccion.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'grvDetProyeccion.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:###,###,##0.0000}"

        'grvDetProyeccion.Columns("Requerido").Width = 100
        'grvDetProyeccion.Columns("Requerido").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        'grvDetProyeccion.Columns("Requerido").DisplayFormat.FormatString = "{0:###,###,##0.0000}"

        'grvDetProyeccion.Columns("Cost Requerido").Width = 100
        'grvDetProyeccion.Columns("Cost Requerido").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        'grvDetProyeccion.Columns("Cost Requerido").DisplayFormat.FormatString = "{0:###,###,##0.0000}"
        'grvDetProyeccion.Columns("Cost Requerido").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'grvDetProyeccion.Columns("Cost Requerido").SummaryItem.DisplayFormat = "{0:###,###,##0.0000}"

        With grvDetProyeccion

            .Columns("C. Unid").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("C. Unid").DisplayFormat.FormatString = Format("C4")
            .Columns("C. Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("C. Total").DisplayFormat.FormatString = Format("C4")
            .Columns("C. Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("C. Total").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"
            .Columns("C. Requerido").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("C. Requerido").DisplayFormat.FormatString = Format("C4")
            .Columns("C. Requerido").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("C. Requerido").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

            .Columns("Codigo").Fixed = FixedStyle.Left
            .Columns("Producto").Fixed = FixedStyle.Left
            .Columns("Producto").BestFit()
            .Columns("B. Procesos").BestFit()
            .Columns("B. Materiales").BestFit()
            .Columns("Cantidad").BestFit()
            .Columns("C. Unid").BestFit()
            .Columns("C. Total").BestFit()
            .Columns("Requerido").BestFit()
            .Columns("C. Requerido").BestFit()
            .Columns("Restante").Visible = False

            If CmbTipo.SelectedValue = 6 Then
                .Columns("B. Procesos").Visible = False
            Else
                .Columns("B. Procesos").Visible = True
            End If

            .OptionsView.ShowFooter = True
        End With

    End Sub

    Public Sub cargarSolicitante(ByVal cargo As Integer)
        CmbSolicitante.DataSource = SQL("SELECT  Id_Empleado AS 'Codigo', (Nombres + ' ' + Apellidos) AS 'Nombre Completo' FROM  Tbl_Empleados where Id_Cargo = " & cargo & " and Activo = 1 ORDER BY 'Codigo'", "tblSolicitante", My.Settings.SolIndustrialCNX).Tables(0)
        CmbSolicitante.DisplayMember = "Nombre Completo"
        CmbSolicitante.ValueMember = "Codigo"
    End Sub

    Public Sub cargarTipo()
        CmbTipo.DisplayMember = "Estado"
        CmbTipo.ValueMember = "IdCategoriaDetalle"
        CmbTipo.DataSource = SQL("SELECT IdCategoriaDetalle, Estado FROM Categorias_Detalle WHERE IdCategoria = 3 and Activo = 1", "tblTipo", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",") Then
            e.Handled = True
        ElseIf e.KeyChar = "," Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    'Private Sub CargarLista()
    '    Dim strsql As String = ""
    '    Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
    '    Dim prueba As String = ""

    '    grvDetProyeccion.Columns.Clear()

    '    grdDetProyeccion.DataSource = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)

    '    grvDetProyeccion.Columns("Codigo").Width = 75
    '    grvDetProyeccion.Columns("Materia Prima").Width = 250

    '    grvDetProyeccion.Columns("Costo Und").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
    '    grvDetProyeccion.Columns("Costo Und").DisplayFormat.FormatString = "{0:###,###,##0.00}"

    '    grvDetProyeccion.Columns("Costo Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
    '    grvDetProyeccion.Columns("Costo Total").DisplayFormat.FormatString = "{0:###,###,##0.0000}"
    '    grvDetProyeccion.Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
    '    grvDetProyeccion.Columns("Costo Total").SummaryItem.DisplayFormat = "{0:###,###,##0.0000}"

    '    grvDetProyeccion.Columns("Costo Requerimiento").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
    '    grvDetProyeccion.Columns("Costo Requerimiento").DisplayFormat.FormatString = "{0:###,###,##0.0000}"
    '    grvDetProyeccion.Columns("Costo Requerimiento").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
    '    grvDetProyeccion.Columns("Costo Requerimiento").SummaryItem.DisplayFormat = "{0:###,###,##0.0000}"
    '    grvDetProyeccion.OptionsView.ShowFooter = True

    'End Sub

    Private Sub activarControles(ByVal nTipoEdic As Integer)

        If nTipoEdic = 0 Then
            deRegistrado.Enabled = False
            daCerrado.Enabled = False
            CmbSolicitante.Enabled = False
            CmbTipo.Enabled = False
            btnAgregar.Enabled = False
            btnQuitar.Enabled = False
            txtObservacion.Enabled = False
            ckIsActivo.Enabled = False

        ElseIf nTipoEdic = 1 Then
            deRegistrado.Enabled = True
            daCerrado.Enabled = True
            CmbSolicitante.Enabled = True
            CmbTipo.Enabled = True
            btnAgregar.Enabled = False
            btnQuitar.Enabled = False
            txtObservacion.Enabled = True
            'ckIsActivo.Enabled = True


        ElseIf nTipoEdic = 2 Or nTipoEdic = 3 Then
            deRegistrado.Enabled = False
            daCerrado.Enabled = False
            CmbSolicitante.Enabled = False
            CmbTipo.Enabled = False
            btnAgregar.Enabled = True
            btnQuitar.Enabled = True
            txtObservacion.Enabled = False
            ckIsActivo.Enabled = False
        End If

        If CmbTipo.SelectedValue = 6 Then
            ckbMaterial.Checked = True
            ckbProceso.Enabled = False
        Else
            ckbProceso.Checked = True
            ckbMaterial.Checked = False
        End If

    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = False
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = False
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Me.Close()
    End Sub

    Public Sub nuevo()
        nTipoEdic = 1
        txtCodigoOrden.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)

        CmbSolicitante.SelectedIndex = 0
        CmbTipo.SelectedIndex = 0
        ckIsActivo.Checked = True
        deRegistrado.Value = Now
        daCerrado.Value = Now
        ckIsActivo.Checked = True
        txtObservacion.Text = ""

        grvProyeccion.Columns.Clear()
        grdProyeccion.DataSource = Nothing
        grvDetProyeccion.Columns.Clear()
        grdDetProyeccion.DataSource = Nothing

        activarControles(nTipoEdic)

        PermitirBotonesEdicion(False)
        CmbSolicitante.Focus()
    End Sub

    Function Modificar()
        If ckIsActivo.Checked = True Then

            deRegistrado.Enabled = True
            daCerrado.Enabled = True
            CmbSolicitante.Enabled = True
            CmbTipo.Enabled = True
            'ckIsActivo.Enabled = True
            btnAgregar.Enabled = True
            btnQuitar.Enabled = True
            PermitirBotonesEdicion(False)
        Else
            MsgBox("La proyeccion ya ha sido aplicada.")
        End If
    End Function

    Function Cancelar()
        activarControles(0)
        PermitirBotonesEdicion(True)
    End Function

    Private Sub frmOrdenProd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strsql As String = ""

        PermitirBotonesEdicion(True)

        activarControles(0)
        cargarTipo()
        'cargarSolicitante()
        txtCodigoOrden.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        ckIsActivo.Checked = True
        deRegistrado.Value = Now


        lblExist.BackColor = C_existencias
        lblTraslado.BackColor = C_requerido

        'AddHandler btnAgregar.Click, AddressOf agregar_producto --USARE EL CODIGO ANTERIOR

    End Sub

    Private AgregarProducto As New frmProductoAdicional
    Private OrdenServDetalle As New Dictionary(Of Llaves, Material_garantia)

    Private Sub agregar_producto()

        AgregarProducto = New frmProductoAdicional() With {.StartPosition = FormStartPosition.CenterParent}
        AgregarProducto.ShowDialog()

        With AgregarProducto.my_material

            If Not .get_codigo = String.Empty Then

                Dim key As New Llaves With {._adicional = AgregarProducto.my_material.get_adicional, ._autorizado = AgregarProducto.my_material.get_autorizado, ._cod_producto = AgregarProducto.my_material.get_codigo, ._activo = AgregarProducto.my_material.get_activo}

                If OrdenServDetalle.ContainsKey(key) = True Then

                    OrdenServDetalle.Item(key).get_cantidad += .get_cantidad

                    OrdenServDetalle.Item(key).get_costo_total += (OrdenServDetalle.Item(key).get_cantidad * .get_costo)

                Else

                    OrdenServDetalle.Add(key, AgregarProducto.my_material)

                End If

            End If

        End With

        'If OrdenServDetalle.Count = 0 Then Tabla.DataSource = Nothing Else Tabla.DataSource = Nothing : Tabla.DataSource = OrdenServDetalle.Values : ajuste_tabla()

    End Sub

    Private Structure Llaves

        Public _cod_producto As String
        Public _adicional As Boolean
        Public _autorizado As Boolean
        Public _activo As Boolean

    End Structure

    'END---------------------------------------------------------------------------------------------------------------------------


    Private Sub grvDetProyeccion_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grvDetProyeccion.RowStyle
        Dim view As ColumnView = DirectCast(sender, ColumnView)

        If view.IsValidRowHandle(e.RowHandle) Then

            'Dim Existencia As Double = view.GetRowCellValue(e.RowHandle, "Existencia")
            'Dim Cantidad As Double = view.GetRowCellValue(e.RowHandle, "Cantidad")

            'If Cantidad > Existencia Then
            '    e.Appearance.BackColor = Color.Tomato
            'End If
            '------------------------------NUEVO----------------------------
            Dim Materiales As Decimal = view.GetRowCellValue(e.RowHandle, "B. Materiales")
            Dim Requerido As Decimal = view.GetRowCellValue(e.RowHandle, "Requerido")
            Dim Result As Decimal = Materiales - Requerido

            If CmbTipo.SelectedValue = 6 Then
                If Requerido > 0 Then e.Appearance.BackColor = C_requerido
            Else

                If Requerido > 0 Then
                    Select Case Result

                        Case 0

                            e.Appearance.BackColor = C_requerido

                        Case Is < 0

                            e.Appearance.BackColor = C_existencias
                            'btnGenerar.Enabled = False

                        Case Is > 0

                            e.Appearance.BackColor = C_requerido

                    End Select

                End If
                ' If Requerido > 0 And Requerido > Materiales Then e.Appearance.BackColor = Color.Coral : btnGenerar.Enabled = False Else If Requerido > 0 And Requerido < Materiales Then e.Appearance.BackColor = Color.Gold

            End If

            '------------------------------NUEVO----------------------------
        End If
    End Sub

    Private Sub frmProyeccion_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub

    Private Sub frmProyeccion_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 27
    End Sub

    Private Sub btnAgregarMat_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        frmAdicional.ShowDialog()
        'frmProductoAdicional.Show()
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        Dim Resum As String = ""
        Dim Codigo, Cantidad, CostoUnd, CostoTot As String
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            If grvProyeccion.RowCount > 0 Then

                Dim result As Integer = MessageBox.Show("Desea remover el producto de la lista?", "Confirmación de Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If result = DialogResult.Yes Then
                    Codigo = grvProyeccion.GetRowCellValue(grvProyeccion.GetSelectedRows(0), "Codigo").ToString()
                    Cantidad = grvProyeccion.GetRowCellValue(grvProyeccion.GetSelectedRows(0), "Cantidad").ToString()
                    CostoUnd = grvProyeccion.GetRowCellValue(grvProyeccion.GetSelectedRows(0), "Costo Und").ToString()
                    CostoTot = grvProyeccion.GetRowCellValue(grvProyeccion.GetSelectedRows(0), "Costo Total").ToString()

                    Resum = Produccion.EditProyDetalle(txtCodigoOrden.Text, Codigo, Cantidad, CostoUnd, CostoTot, 3)

                    If Resum = "OK" Then
                        CargarDetalle(txtCodigoOrden.Text)
                    End If
                End If

            Else
                MsgBox("No hay registros que pueda eliminar.", MsgBoxStyle.Critical, "Información")
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub grdDetProyeccion_DataSourceChanged(sender As Object, e As EventArgs) Handles grdDetProyeccion.DataSourceChanged
        If grvDetProyeccion.RowCount > 0 Then 'And ckIsActivo.Checked = True Then
            btnGenerar.Enabled = True
            'btnAgregar.Enabled = True
            'btnQuitar.Enabled = True
        Else
            'btnGenerar.Enabled = False
            'btnAgregar.Enabled = False
            'btnQuitar.Enabled = False
        End If
    End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        Dim Codigo, TipoOrden As String
        Dim Message As String = ""
        Dim Resum As String = "OK"
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        Dim Bandera As Boolean = False
        Dim strsql As String

        Dim tblFilter As DataTable = DirectCast(grvDetProyeccion.DataSource, DataView).Table.Clone()
        For i As Integer = 0 To grvDetProyeccion.DataRowCount - 1

            tblFilter.ImportRow(grvDetProyeccion.GetDataRow(i))

        Next

        Try
            TipoOrden = ""
            If CmbTipo.SelectedValue = 6 Then


                For i = 0 To tblFilter.Rows.Count - 1

                    '---------
                    If tblFilter.Rows(i).Item("Requerido") > 0 Then
                        Bandera = True
                    End If
                    '-----------------
                Next

                Message = "Desea generar la orden de pedido?"
                TipoOrden = "Orden de Pedido"

            ElseIf CmbTipo.SelectedValue = 7 Then


                For i = 0 To tblFilter.Rows.Count - 1

                    '---------
                    If tblFilter.Rows(i).Item("Requerido") > 0 And tblFilter.Rows(i).Item("B. Materiales") >= tblFilter.Rows(i).Item("Requerido") Then
                        Bandera = True
                    End If

                Next

                Message = "Desea generar la orden de traslado?"
                TipoOrden = "Orden de Traslado"

            End If

            If Bandera = True Then

                Dim result As Integer = MessageBox.Show(Message, "Confirmación de Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If result = DialogResult.No Then Exit Sub

                If CmbTipo.SelectedValue = 6 Then

                    strsql = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(Codigo_Pedido as int))+1,1))),8) as 'Codigo' from Orden_Pedido"

                    Dim tblRegMax As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

                    Codigo = tblRegMax.Rows(0).Item(0)

                    Resum = Inventario.EditarPedido(My.Settings.Sucursal, Codigo, "000", "ORDEN DE PEDIDO GENERADA DESDE PROYECCIÓN.", Date.Now, 0, 3048, 1, txtCodigoOrden.Text)


                ElseIf CmbTipo.SelectedValue = 7 Then

                    strsql = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(Codigo_Traslado as int))+1,1))),8) as 'Codigo' from Orden_Traslado"

                    Dim tblRegMax As DataTable = SQL(strsql, "tblRegMax", My.Settings.SolIndustrialCNX).Tables(0)

                    Codigo = tblRegMax.Rows(0).Item(0)

                    Resum = Inventario.EditarTraslado(My.Settings.Sucursal, Codigo, 2, 3, "ORDEN DE TRASLADO GENERADA DESDE PROYECCIÓN.", Date.Now, False, True, 1, txtCodigoOrden.Text)

                End If

                If Resum = "OK" Then

                    For i As Integer = 0 To tblFilter.Rows.Count - 1

                        Dim CodigoProducto As String = tblFilter.Rows(i).Item("Codigo").ToString
                        'Dim Cantidad As Integer = tblFilter.Rows(i).Item("Cantidad").ToString   NO SE TOMA LA CANTIDAD TOTAL, SOLO LA CANTIDAD REQUERIDA PARA TRASLADO O COMPRA
                        Dim Requerido As Decimal = tblFilter.Rows(i).Item("Requerido").ToString
                        Dim Costo As Double = tblFilter.Rows(i).Item("C. Unid").ToString
                        Dim CostoTotal As Double = tblFilter.Rows(i).Item("C. Requerido").ToString

                        If CmbTipo.SelectedValue = 6 Then

                            If Requerido > 0 Then

                                Resum = Inventario.EditarPedidoDeta(My.Settings.Sucursal, Codigo, CodigoProducto, 0, Requerido, Costo, CostoTotal, 1)

                            End If

                        ElseIf CmbTipo.SelectedValue = 7 Then

                            If Requerido > 0 Then

                                Resum = Inventario.EditarTrasladoDeta(My.Settings.Sucursal, Codigo, CodigoProducto, Requerido, Costo, 1)

                            End If
                        End If
                    Next

                    Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

                    ckIsActivo.Checked = False

                    Produccion.EditProyeccion(txtCodigoOrden.Text, CmbSolicitante.SelectedValue, deRegistrado.Value, daCerrado.Value, Date.Now, CmbTipo.SelectedValue, ckIsActivo.Checked, 2, txtObservacion.Text)

                End If

                MsgBox("Proyeccion Guardad.")

                Dim abrirOrden As Integer = MessageBox.Show(((TipoOrden & " registrada con No. " & Codigo) & " Desea abrir el documento?"), "Confirmación de Acción", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If abrirOrden = DialogResult.Yes Then

                    If CmbTipo.SelectedValue = 6 Then
                        Me.Hide()
                        frmOrdenPedido.MdiParent = frmPrincipal
                        frmOrdenPedido.WindowState = FormWindowState.Maximized
                        frmOrdenPedido.Show()
                        NumCatalogo = 38
                        frmOrdenPedido.CargarDatos(Codigo)
                        Me.Close()

                    ElseIf CmbTipo.SelectedValue = 7 Then
                        Me.Hide()
                        frmTraslados.MdiParent = frmPrincipal
                        frmTraslados.WindowState = FormWindowState.Maximized
                        NumCatalogo = 39
                        frmTraslados.Show()
                        frmTraslados.CargarDatos(Codigo)
                        Me.Close()

                    End If
                Else
                    CargarDatos(txtCodigoOrden.Text)
                    btnAgregar.Enabled = False
                    btnQuitar.Enabled = False
                End If


            Else
                Select Case CmbTipo.SelectedValue
                    Case 6

                        MsgBox("Orden de pedido no generado." & vbNewLine & "No existen materiales a solicitar.")

                    Case 7

                        MsgBox("Orden traslado no generado." & vbNewLine & "No existen materiales a trasladar.")

                End Select
            End If

        Catch ex As Exception
        End Try
    End Sub

    Sub GuardarMasterDoc()
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)

        Try
            If CmbSolicitante.SelectedValue > 0 Then

                Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)

                If result = DialogResult.Yes Then

                    ResumMaster = Produccion.EditProyeccion(txtCodigoOrden.Text, CmbSolicitante.SelectedValue, deRegistrado.Value, daCerrado.Value, Date.Now, CmbTipo.SelectedValue, ckIsActivo.Checked, nTipoEdic, txtObservacion.Text)

                    If ResumMaster = "OK" Then
                        MsgBox("Proyeccion registrada.", MsgBoxStyle.Information, "Información")
                    Else
                        MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "Información")
                    End If

                    PermitirBotonesEdicion(True)
                    nTipoEdic = 2
                    CargarDatos(txtCodigoOrden.Text)
                End If
            Else
                MsgBox("Debe selecccionar el solicitante.", MsgBoxStyle.Information, "Información")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Private Sub CmbTipo_SelectedValueChanged(sender As Object, e As EventArgs) Handles CmbTipo.SelectedValueChanged
        Dim Valor As String = ""

        If CmbTipo.SelectedValue = 6 Then
            btnGenerar.Text = "Generar el Pedido"
            lbRequerido.Text = "Compra"
            cargarSolicitante(15)

        ElseIf CmbTipo.SelectedValue = 7 Then
            btnGenerar.Text = "Generar el Traslado"
            lbRequerido.Text = "Traslado"
            cargarSolicitante(8)
        End If



    End Sub

    Public Sub ckbProceso_CheckedChanged(sender As Object, e As EventArgs) Handles ckbProceso.CheckedChanged, ckbMaterial.CheckedChanged
        CargaExistencias(txtCodigoOrden.Text)
    End Sub
End Class