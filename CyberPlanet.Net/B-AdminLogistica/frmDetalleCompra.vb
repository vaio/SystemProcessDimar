﻿Imports DevExpress.XtraEditors.Controls

Public Class frmDetalleCompra

    '------------------------------- Written by bob ---------------------------------------- 

    Private existencia As Double = 0

    '---------------------------------------------------------------------------------------

    Dim NewExistUnd As Integer = 0
    Dim LastExistUnd As Integer = 0
    Dim NewExistMon As Double = 0
    Dim LastExistMon As Double = 0
    Dim ExistMon As Double = 0
    Dim ExistUndTotal As Integer = 0
    Dim ExistMonTotal As Double = 0
    Dim NewCostoProm As Double = 0

    Dim SumSubtotal As Double = 0
    Dim SumDesc As Double = 0

    Dim nIR As Double = 0
    Dim nIMI As Double = 0
    Dim nIVA As Double = 0

    Dim ResumEntrada As String = ""
    Dim CodMaxEntrada As String = ""
    Dim ResumEntradaDeta As String = ""
    Dim descuento As Double
    Dim CodEntrada As String = ""

    Private ValidarCampoCantidad As New Validar With {.CantDecimal = 2}
    Private ValidarCampoDescuento As New Validar With {.CantDecimal = 0}
    Private ValidarCamposMoneda As New Validar With {.CantDecimal = 4}

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 2
        nTipoEdicDet = 0
        LastExistUnd = 0
        LastExistMon = 0
        Me.Dispose()
        Close()
    End Sub

    Sub CargarDatosProducto(ByVal nNumProd As String)
        Dim strsql As String = "SELECT Codigo_Producto,Nombre_Producto,Costo_Promedio,Existencia_Actual from Productos where Codigo_Producto = '" & nNumProd & "'"
        Dim TblDatosProd As DataTable = SQL(strsql, "tblProducto", My.Settings.SolIndustrialCNX).Tables(0)
        If TblDatosProd.Rows.Count <> 0 Then
            lblDescripcion.Text = TblDatosProd.Rows(0).Item(1)
            existencia = TblDatosProd.Rows(0).Item(3)
            txtCostoUnit.Text = Format(TblDatosProd.Rows(0).Item(2), "###,###,##0.0000")

            If NumCatalogo = 21 Then
                txtCostoUnitUSS.Text = Format((CDbl(txtCostoUnit.Text) / CDbl(frmMasterCompraLocal.txtTazaCambio.Text)), "###,###,##0.0000")
            ElseIf NumCatalogo = 22 Then
                txtCostoUnitUSS.Text = Format((CDbl(txtCostoUnit.Text) / CDbl(frmMasterCompraForanea.txtTasaCambio.Text)), "###,###,##0.0000")
            End If

            txtCantidad.Text = FormatNumber(1, 2)
            CargarExistProd()
            CargarBodegas()
            txtCantidad.Focus()
        Else
            MsgBox("No existe ningún producto existente con el código ingresado.", MsgBoxStyle.Information, "SIGCA")
            txtCodigoProd.Text = ""
            txtCodigoProd.Focus()
        End If
    End Sub

    Sub CargareEntrada(ByVal tipo As String)
        Dim strsql As String = "SELECT Codigo_Movimiento FROM Movimientos_Almacenes WHERE Tipo_Movimiento = 4 and Codigo_Documento = " & frmMasterCompraLocal.txtNumDoc.Text & " AND Tipo_Documento = " & tipo
        Dim TblDatos As DataTable = SQL(strsql, "tblEntrada", My.Settings.SolIndustrialCNX).Tables(0)
        If TblDatos.Rows.Count <> 0 Then
            CodEntrada = TblDatos.Rows(0).Item(0)
        End If
    End Sub
    Private Sub txtCodigoProd_MouseClick(sender As Object, e As MouseEventArgs) Handles txtCodigoProd.MouseClick
        frmBusquedaProd.ShowDialog()
    End Sub

    Private Sub txtCantidad_TextChanged(sender As Object, e As EventArgs) Handles txtCantidad.TextChanged
        CalcularCostoTotal()
    End Sub

    Sub TotalExistenciasProd()
        Dim StrSqlExisTotalProd As String
        Dim StrSqlExistBodProd As String
        Dim StrSqlSumDetalle As String

        StrSqlExisTotalProd = "SELECT SUM(UndTotal_Existencia) AS SumUnd, SUM(MonTotal_Existencia) AS SumMon, Codigo_Producto FROM dbo.Inventario WHERE (Codigo_Producto = '" & txtCodigoProd.Text & "') GROUP BY Codigo_Producto"
        StrSqlExistBodProd = "SELECT MonTotal_Existencia FROM dbo.Inventario WHERE (Codigo_Bodega = " & luBodegas.EditValue & ") AND (Codigo_Producto = '" & txtCodigoProd.Text & "')"
        StrSqlSumDetalle = "SELECT SUM(Cantidad * Precio_Unitario) AS SumSubtotal, SUM(Descuento) AS SumDesc, CodigoSucursal, Numero_de_Compra, Tipo_Compra, Codigo_Proveedor FROM dbo.Detalle_Compras GROUP BY CodigoSucursal, Numero_de_Compra, Tipo_Compra, Codigo_Proveedor HAVING (CodigoSucursal = " & nSucursal & ") AND (Numero_de_Compra = '" & frmMasterCompraLocal.txtNumDoc.Text & "') AND (Tipo_Compra = " & IIf(frmMasterCompraLocal.rbContado.Checked = True, 1, 2) & ") AND (Codigo_Proveedor = '" & frmMasterCompraLocal.luProveedor.EditValue & "')"

        Dim TblExistTotal As DataTable = SQL(StrSqlExisTotalProd, "tblExisTotalProd", My.Settings.SolIndustrialCNX).Tables(0)
        If TblExistTotal.Rows.Count <> 0 Then
            ExistUndTotal = TblExistTotal.Rows(0).Item(0)
            ExistMonTotal = Format(TblExistTotal.Rows(0).Item(1), "###,###,##0.0000")
        Else

            MsgBox("No existe ningún producto existente con el código ingresado.", MsgBoxStyle.Information, "SIGCA")
            txtCodigoProd.Focus()
        End If

        Dim TblExistBodProd As DataTable = SQL(StrSqlExistBodProd, "tblExistBodProd", My.Settings.SolIndustrialCNX).Tables(0)
        If TblExistBodProd.Rows.Count <> 0 Then
            ExistMon = Format(TblExistBodProd.Rows(0).Item(0), "###,###,##0.0000")
        End If

        Dim TblSumDetalle As DataTable = SQL(StrSqlSumDetalle, "tblSumDetalle", My.Settings.SolIndustrialCNX).Tables(0)
        If TblSumDetalle.Rows.Count <> 0 Then
            SumSubtotal = Format(TblSumDetalle.Rows(0).Item(0), "###,###,##0.0000")
            SumDesc = Format(TblSumDetalle.Rows(0).Item(1), "###,###,##0.0000")
        End If

    End Sub

    Private Sub cmdAceptar_Click(sender As Object, e As EventArgs) Handles cmdAceptar.Click
        If lblDescripcion.Text <> "" Then
            Dim Resum As String = ""
            Dim Div As Double = 0
            Dim DetalleDoc As New clsDocumentos(My.Settings.SolIndustrialCNX)
            Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

            NewExistUnd = 0
            NewExistMon = 0
            ExistUndTotal = 0
            ExistMonTotal = 0
            SumSubtotal = 0
            SumDesc = 0
            nSucursal = My.Settings.Sucursal

            If nTipoEdicDet = 1 Then
                NewExistUnd = existencia + ChangeToDouble(txtCantidad.Text)
                NewExistMon = ExistMon + ChangeToDouble(lblCostoTotal.Text)
                ExistUndTotal = ExistUndTotal + ChangeToDouble(txtCantidad.Text)
                ExistMonTotal = ExistMonTotal + ChangeToDouble(lblCostoTotal.Text)
                SumSubtotal = SumSubtotal + ChangeToDouble(lblCostoTotal.Text)
                SumDesc = SumDesc + ChangeToDouble(txtDescuento.Text)
            ElseIf nTipoEdicDet = 2 Then
                NewExistUnd = existencia - LastExistUnd + CInt(txtCantidad.Text)
                NewExistMon = ExistMon - LastExistMon + CDbl(lblCostoTotal.Text)
                ExistUndTotal = ExistUndTotal - LastExistUnd + CInt(txtCantidad.Text)
                ExistMonTotal = ExistMonTotal - LastExistMon + CDbl(lblCostoTotal.Text)
                SumSubtotal = SumSubtotal - LastExistMon + CDbl(lblCostoTotal.Text)
                SumDesc = SumDesc + CDbl(txtDescuento.Text)
            ElseIf nTipoEdicDet = 3 Then
                NewExistUnd = existencia - LastExistUnd
                NewExistMon = ExistMon - LastExistMon
                ExistUndTotal = ExistUndTotal - LastExistUnd
                ExistMonTotal = ExistMonTotal - LastExistMon
                SumSubtotal = SumSubtotal - LastExistMon
                SumDesc = SumDesc + CDbl(txtDescuento.Text)
            End If

            Div = IIf(ExistUndTotal = 0, 0, CDbl(ExistMonTotal / ExistUndTotal))
            NewCostoProm = Format(IIf(ExistUndTotal = 0, 0, ExistMonTotal / ExistUndTotal), "###,###,##0.0000")

            If NumCatalogo = 21 Then
                nIR = (SumSubtotal - SumDesc) * (ChangeToDouble(frmMasterCompraLocal.txtPorcIR.Text) / 100)
                nIMI = (SumSubtotal - SumDesc) * (ChangeToDouble(frmMasterCompraLocal.txtPorcIMI.Text) / 100)
                nIVA = (SumSubtotal - SumDesc) * (IIf(frmMasterCompraLocal.cmbTipExoneracion.SelectedIndex = 0, 15, 0) / 100)
            End If

            Try
                If NumCatalogo = 21 Then

                    Resum = DetalleDoc.EditarDetalleCompraLocal(My.Settings.Sucursal, frmMasterCompraLocal.txtNumDoc.Text, IIf(frmMasterCompraLocal.rbContado.Checked = True, 1, 2), frmMasterCompraLocal.luProveedor.EditValue,
                        luBodegas.EditValue, txtCodigoProd.Text, deFechaExp.Value, ChangeToDouble(txtCantidad.Text), ChangeToDouble(txtCantBonif.Text), CInt(0), ChangeToDouble(txtCostoUnit.Text), CDbl(0), ChangeToDouble(txtDescuento.Text), NewExistUnd, NewExistMon, ExistUndTotal, ExistMonTotal, NewCostoProm,
                         SumSubtotal, SumDesc, nIR, nIMI, nIVA, ChangeToDouble(frmMasterCompraLocal.txtPorcIR.Text), ChangeToDouble(frmMasterCompraLocal.txtPorcIMI.Text), frmMasterCompraLocal.deFechaDoc.Value, frmMasterCompraLocal.Fecha_Vencimiento.Value, nTipoEdicDet)

                ElseIf NumCatalogo = 22 Then

                    '   Resum = DetalleDoc.EditarDetalleCompraForanea(My.Settings.Sucursal, frmMasterCompraForanea.txtNumDocumento.Text, IIf(frmMasterCompraForanea.rbContado.Checked = True, 1, 2), frmMasterCompraForanea.cmbProveedor.EditValue,
                    '   luBodegas.EditValue, txtCodigoProd.Text, txtCantidad.Text, txtCostoUnit.Text, 0, 0, 0, 0, 0, frmMasterCompraForanea.Fecha_de_documento.Value, nTipoEdicDet)

                End If

                If Resum = "OK" Then
                    If NumCatalogo = 21 Then
                        frmMasterCompraLocal.CargarDetalleDoc(My.Settings.Sucursal, frmMasterCompraLocal.txtNumDoc.Text, IIf(frmMasterCompraLocal.rbContado.Checked = True, 1, 2), frmMasterCompraLocal.luProveedor.EditValue)
                    ElseIf NumCatalogo = 22 Then
                        '  frmMasterCompraForanea.CargarDetalleDoc(My.Settings.Sucursal, frmMasterCompraForanea.txtNumDocumento.Text, IIf(frmMasterCompraForanea.rbContado.Checked = True, 1, 2), frmMasterCompraForanea.luProveedor.EditValue)
                    End If

                    Cerrar()
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
                End If

            Catch ex As Exception
                Call MsgBox("Error: " + ex.Message)
            Finally

            End Try
        End If
    End Sub

    Private Sub frmDetalleDocInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ValidarCampoCantidad.agregar_control(New List(Of Control)({txtCantidad})).ActivarEventosNumericos()
        ValidarCampoDescuento.agregar_control(New List(Of Control)({txtPorcenDescuento})).SoloEnteros()
        ValidarCamposMoneda.agregar_control(New List(Of Control)({txtCantBonif, txtCostoUnit, txtDescuento, txtCostoUnitUSS})).ActivarEventosNumericos()

        CargarBodegas()

        If NumCatalogo = 21 Then
            If frmMasterCompraLocal.rbCordobas.Checked = True Then
                txtCostoUnitUSS.Enabled = False
                txtCostoUnit.Enabled = True
            Else
                txtCostoUnit.Enabled = False
                txtCostoUnitUSS.Enabled = True
            End If
        ElseIf NumCatalogo = 22 Then
            If frmMasterCompraForanea.rbCordobas.Checked = True Then
                txtCostoUnitUSS.Enabled = False
                txtCostoUnit.Enabled = True
            Else
                txtCostoUnit.Enabled = False
                txtCostoUnitUSS.Enabled = True
            End If
        End If

        If nTipoEdicDet = 1 Then
            txtCodigoProd.Text = ""
            txtCantidad.Text = String.Empty
            lblDescripcion.Text = ""
            existencia = 0
            txtCantBonif.Text = String.Empty
            txtDescuento.Text = FormatNumber(0, 4)
            txtCostoUnitUSS.Text = FormatNumber(0, 4)
            txtCostoUnit.Text = FormatNumber(0, 4)
            lblCostoTotalUSS.Text = FormatNumber(0, 4)
            lblCostoTotal.Text = FormatNumber(0, 4)
            deFechaExp.Value = Now.Date
            txtPorcenDescuento.Text = String.Empty

        ElseIf nTipoEdicDet = 2 Then
            CargarCurrentDetalle()

        ElseIf nTipoEdicDet = 3 Then
            CargarCurrentDetalle()
            Panel2.Enabled = False

        End If

        txtCodigoProd.Focus()
        descuento = frmMasterCompraLocal.PorcenDes

        If Convert.ToDouble(descuento) > 0 Then
            txtPorcentaje.Enabled = False
            txtDescuento.Enabled = False
            txtPorcenDescuento.Enabled = False
            txtPorcentaje.Checked = frmMasterCompraLocal.txtPorcentaje.Checked
            txtPorcenDescuento.Text = descuento
        Else
            txtPorcentaje.Enabled = True
            txtPorcenDescuento.Enabled = False
        End If

    End Sub

    Sub CargarCurrentDetalle()

        If NumCatalogo = 21 Then
            If frmMasterCompraLocal.grvDetalle.RowCount > 0 Then
                txtCodigoProd.Text = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Codigo").ToString()
                lblDescripcion.Text = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Descripcion").ToString()
                luBodegas.EditValue = CInt(frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "CodBodega").ToString())
                deFechaExp.Value = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Fecha_Expiracion").ToString()
                txtCantidad.Text = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Cantidad").ToString()
                txtCantBonif.Text = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Cant. Bonificada").ToString()
                txtCostoUnit.Text = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Costo_Unitario").ToString()
                txtDescuento.Text = frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "Descuento").ToString()
                CargarExistProd()
                CargarExistProdBod(frmMasterCompraLocal.grvDetalle.GetRowCellValue(frmMasterCompraLocal.grvDetalle.GetSelectedRows(0), "CodBodega").ToString())
            End If
        ElseIf NumCatalogo = 22 Then
            If frmMasterCompraForanea.grvDetalle.RowCount > 0 Then
                txtCodigoProd.Text = frmMasterCompraForanea.grvDetalle.GetRowCellValue(frmMasterCompraForanea.grvDetalle.GetSelectedRows(0), "Codigo").ToString()
                lblDescripcion.Text = frmMasterCompraForanea.grvDetalle.GetRowCellValue(frmMasterCompraForanea.grvDetalle.GetSelectedRows(0), "Producto").ToString()
                luBodegas.EditValue = CInt(frmMasterCompraForanea.grvDetalle.GetRowCellValue(frmMasterCompraForanea.grvDetalle.GetSelectedRows(0), "CodBodega").ToString())
                deFechaExp.Value = Date.Now
                txtCantidad.Text = frmMasterCompraForanea.grvDetalle.GetRowCellValue(frmMasterCompraForanea.grvDetalle.GetSelectedRows(0), "Recibidas").ToString()
                txtCantBonif.Text = Format(0, "###,###,##0.0000")
                txtCostoUnit.Text = frmMasterCompraForanea.grvDetalle.GetRowCellValue(frmMasterCompraForanea.grvDetalle.GetSelectedRows(0), "Costo FOB en US$").ToString()
                txtDescuento.Text = Format(0, "###,###,##0.0000")
                CargarExistProd()
                CargarExistProdBod(frmMasterCompraForanea.grvDetalle.GetRowCellValue(frmMasterCompraForanea.grvDetalle.GetSelectedRows(0), "CodBodega").ToString())
            End If
        End If

    End Sub

    Function ExistinBodega()
        Dim strExistBodega As String = "SELECT dbo.Almacenes.Codigo_Bodega, dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & luBodegas.EditValue & ")"
        Dim tblExistinBod As DataTable = SQL(strExistBodega, "tblExistinBod", My.Settings.SolIndustrialCNX).Tables(0) ', dbo.Inventario.UndTotal_Existencia as Existencia
        ExistinBodega = 0
        If tblExistinBod.Rows.Count > 0 Then
            ExistinBodega = tblExistinBod.Rows.Count
        End If
    End Function


    Function NomBodeg(ByVal nNumBodega As Integer)
        Dim strsqlNomBod As String
        strsqlNomBod = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & nNumBodega & ")"
        Dim tblNomBod As DataTable = SQL(strsqlNomBod, "tblNomBod", My.Settings.SolIndustrialCNX).Tables(0) ', dbo.Inventario.UndTotal_Existencia as Existencia
        If tblNomBod.Rows.Count > 0 Then
            NomBodeg = tblNomBod.Rows(0).Item(0)
        End If
    End Function

    Private Sub txtCostoUnit_TextChanged(sender As Object, e As EventArgs) Handles txtCostoUnit.TextChanged

        If txtCostoUnit.Enabled = True Then
            If NumCatalogo = 21 Then
                txtCostoUnitUSS.Text = Format((ChangeToDouble(txtCostoUnit.Text) / ChangeToDouble(frmMasterCompraLocal.txtTazaCambio.Text)), "###,###,##0.0000")
            ElseIf NumCatalogo = 22 Then
                txtCostoUnitUSS.Text = Format((ChangeToDouble(txtCostoUnit.Text) / ChangeToDouble(frmMasterCompraForanea.txtTasaCambio.Text)), "###,###,##0.0000")
            End If
        End If

        CalcularCostoTotal()
    End Sub

    Private Sub txtCostoUnitUSS_TextChanged(sender As Object, e As EventArgs) Handles txtCostoUnitUSS.TextChanged

        If txtCostoUnitUSS.Enabled = True Then
            If NumCatalogo = 21 Then
                txtCostoUnit.Text = Format((ChangeToDouble(txtCostoUnitUSS.Text) * ChangeToDouble(frmMasterCompraLocal.txtTazaCambio.Text)), "###,###,##0.0000")
            ElseIf NumCatalogo = 22 Then
                txtCostoUnit.Text = Format((ChangeToDouble(txtCostoUnitUSS.Text) * ChangeToDouble(frmMasterCompraForanea.txtTasaCambio.Text)), "###,###,##0.0000")
            End If
        End If

        CalcularCostoTotal()
    End Sub

    Sub CalcularCostoTotal()
        lblCostoTotal.Text = Format(CDbl(IIf(txtCostoUnit.Text = "", 0, txtCostoUnit.Text)) * CInt(IIf(txtCantidad.Text = "", 0, txtCantidad.Text)), "###,###,##0.0000")
        lblCostoTotalUSS.Text = Format(CDbl(IIf(txtCostoUnitUSS.Text = "", 0, txtCostoUnitUSS.Text)) * CInt(IIf(txtCantidad.Text = "", 0, txtCantidad.Text)), "###,###,##0.0000")
        CalcularPorcDescuento()
    End Sub

    Sub CalcularPorcDescuento()
        If CDbl(IIf(txtPorcenDescuento.Text = "", 0, txtPorcenDescuento.Text)) > 0 Then
            txtDescuento.Text = Format((CDbl(lblCostoTotal.Text) * (CDbl(txtPorcenDescuento.Text) / 100)), "###,###,###.00")
        End If
    End Sub

    Public Sub CargarBodegas()
        Dim strsqlBodega As String = "SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes where Almacenes.Codigo_Bodega in (2,4)  "

        luBodegas.Properties.DataSource = SQL(strsqlBodega, "tblBodega", My.Settings.SolIndustrialCNX).Tables(0)
        luBodegas.Properties.DisplayMember = "Nombre_Bodega"
        luBodegas.Properties.ValueMember = "Codigo_Bodega"


        luBodegas.Properties.Columns.Add(New LookUpColumnInfo("Codigo_Bodega", "No.", 20))

        luBodegas.Properties.Columns.Add(New LookUpColumnInfo("Nombre_Bodega", "BODEGA DE DESTINO", 100))

        luBodegas.ItemIndex = 0

        luBodegas.Properties.Columns(0).Visible = False

        luBodegas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center

    End Sub

    Sub CargarExistProd()
        If txtCodigoProd.Text = "" Then
            Exit Sub
        End If
        Dim strsqlExistProd As String
        strsqlExistProd = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "')"
        Tbl_ExistBodegasBS.DataSource = SQL(strsqlExistProd, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)
        EncabezadosDetDoc()
    End Sub

    Sub CargarExistProdBod(ByVal nNumBodega As Integer)
        If txtCodigoProd.Text = "" Then
            Exit Sub
        End If
        Dim strsqlExistProdBod As String
        strsqlExistProdBod = "SELECT dbo.Almacenes.Nombre_Bodega as Bodega, dbo.Inventario.UndTotal_Existencia as Existencia FROM dbo.Almacenes INNER JOIN dbo.Inventario ON dbo.Almacenes.Codigo_Sucursal = dbo.Inventario.Codigo_Sucursal AND dbo.Almacenes.Codigo_Bodega = dbo.Inventario.Codigo_Bodega WHERE (dbo.Inventario.Codigo_Producto = '" & txtCodigoProd.Text & "') and (dbo.Almacenes.Codigo_Bodega=" & nNumBodega & ")"
        Dim tblExistProdBod As DataTable = SQL(strsqlExistProdBod, "tblExistProd", My.Settings.SolIndustrialCNX).Tables(0)
        If tblExistProdBod.Rows.Count > 0 Then
            existencia = tblExistProdBod.Rows(0).Item(1)
        End If
    End Sub

    Sub EncabezadosDetDoc()
        GridView1.Columns.Clear()
        GridView1.OptionsView.ShowFooter = True

        Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
        gc0.Name = "Bodega"
        gc0.Caption = "Bodega"
        gc0.FieldName = "Bodega"
        gc0.Width = 50
        GridView1.Columns.Add(gc0)
        gc0.Visible = True
        gc0.VisibleIndex = 0
        GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

        Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
        gc1.Name = "Existencia"
        gc1.Caption = "Existencia"
        gc1.FieldName = "Existencia"
        gc1.DisplayFormat.FormatString = "{0:0.00}"
        gc1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        gc1.Width = 1
        GridView1.Columns.Add(gc1)
        gc1.Visible = True
        gc1.VisibleIndex = 1
        GridView1.Columns(1).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView1.Columns(1).SummaryItem.DisplayFormat = "{0:0.00}"
    End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick
        Dim sNomBodega As String
        If Tbl_ExistBodegasBS.Count > 0 Then
            Dim registro As DataRowView = Tbl_ExistBodegasBS.Current
            sNomBodega = registro.Item(0)
            luBodegas.Text = sNomBodega
            luBodegas.ClosePopup()
            deFechaExp.Focus()
        End If
    End Sub

    Private Sub luBodegas_EditValueChanged(sender As Object, e As EventArgs) Handles luBodegas.EditValueChanged
        CargarExistProdBod(luBodegas.EditValue)
    End Sub

    Private Sub txtPorcentaje_CheckedChanged() Handles txtPorcentaje.CheckedChanged
        If Convert.ToDouble(descuento) <= 0 Then

            If txtPorcentaje.Checked = True Then

                txtDescuento.Text = String.Empty
                txtDescuento.Enabled = False
                txtPorcenDescuento.Enabled = True

            Else

                txtDescuento.Text = String.Empty
                txtDescuento.Enabled = True
                txtPorcenDescuento.Enabled = False
                txtPorcenDescuento.Text = String.Empty

            End If
        End If
    End Sub

    Private Sub txtPorcenDescuento_TextChanged(sender As Object, e As EventArgs) Handles txtPorcenDescuento.TextChanged
        CalcularPorcDescuento()
    End Sub

    Private Sub txtCodigoProd_TextChanged(sender As Object, e As EventArgs) Handles txtCodigoProd.TextChanged
        CargarDatosProducto(txtCodigoProd.Text)
    End Sub

    Private Sub Tbl_ExistBodegasBS_CurrentChanged(sender As Object, e As EventArgs) Handles Tbl_ExistBodegasBS.CurrentChanged

    End Sub
End Class