﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDetalleCompra
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDetalleCompra))
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdAceptar = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblCostoTotalUSS = New System.Windows.Forms.Label()
        Me.txtCostoUnitUSS = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCodigoProd = New System.Windows.Forms.TextBox()
        Me.txtPorcenDescuento = New System.Windows.Forms.TextBox()
        Me.txtPorcentaje = New System.Windows.Forms.CheckBox()
        Me.luBodegas = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCantBonif = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.deFechaExp = New System.Windows.Forms.DateTimePicker()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Tbl_ExistBodegasBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCostoUnit = New System.Windows.Forms.TextBox()
        Me.lblCostoTotal = New System.Windows.Forms.Label()
        Me.TblBodegaBS = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.luBodegas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tbl_ExistBodegasBS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBodegaBS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo Producto:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 57)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripción:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(27, 226)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Costo Unitario (C$):"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDescripcion.Location = New System.Drawing.Point(167, 55)
        Me.lblDescripcion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(487, 24)
        Me.lblDescripcion.TabIndex = 1
        '
        'txtCantidad
        '
        Me.txtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCantidad.Location = New System.Drawing.Point(167, 126)
        Me.txtCantidad.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidad.MaxLength = 12
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(130, 22)
        Me.txtCantidad.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(27, 128)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 17)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Cantidad:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdCancelar)
        Me.Panel1.Controls.Add(Me.cmdAceptar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 414)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(681, 60)
        Me.Panel1.TabIndex = 1
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(551, 10)
        Me.cmdCancelar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(104, 43)
        Me.cmdCancelar.TabIndex = 1
        Me.cmdCancelar.Text = "&Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(439, 10)
        Me.cmdAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(104, 43)
        Me.cmdAceptar.TabIndex = 0
        Me.cmdAceptar.Text = "&Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.lblCostoTotalUSS)
        Me.Panel2.Controls.Add(Me.txtCostoUnitUSS)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.txtCodigoProd)
        Me.Panel2.Controls.Add(Me.txtPorcenDescuento)
        Me.Panel2.Controls.Add(Me.txtPorcentaje)
        Me.Panel2.Controls.Add(Me.luBodegas)
        Me.Panel2.Controls.Add(Me.txtDescuento)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.txtCantBonif)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.deFechaExp)
        Me.Panel2.Controls.Add(Me.GridControl1)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtCostoUnit)
        Me.Panel2.Controls.Add(Me.lblCostoTotal)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.txtCantidad)
        Me.Panel2.Controls.Add(Me.lblDescripcion)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(681, 414)
        Me.Panel2.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(27, 257)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(106, 17)
        Me.Label12.TabIndex = 37
        Me.Label12.Text = "Costo Total ($):"
        '
        'lblCostoTotalUSS
        '
        Me.lblCostoTotalUSS.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCostoTotalUSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCostoTotalUSS.Location = New System.Drawing.Point(167, 256)
        Me.lblCostoTotalUSS.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCostoTotalUSS.Name = "lblCostoTotalUSS"
        Me.lblCostoTotalUSS.Size = New System.Drawing.Size(130, 24)
        Me.lblCostoTotalUSS.TabIndex = 36
        '
        'txtCostoUnitUSS
        '
        Me.txtCostoUnitUSS.BackColor = System.Drawing.Color.White
        Me.txtCostoUnitUSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCostoUnitUSS.Location = New System.Drawing.Point(167, 190)
        Me.txtCostoUnitUSS.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCostoUnitUSS.MaxLength = 12
        Me.txtCostoUnitUSS.Name = "txtCostoUnitUSS"
        Me.txtCostoUnitUSS.Size = New System.Drawing.Size(130, 22)
        Me.txtCostoUnitUSS.TabIndex = 35
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(27, 193)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(123, 17)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Costo Unitario ($):"
        '
        'txtCodigoProd
        '
        Me.txtCodigoProd.Location = New System.Drawing.Point(167, 22)
        Me.txtCodigoProd.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigoProd.Name = "txtCodigoProd"
        Me.txtCodigoProd.Size = New System.Drawing.Size(132, 22)
        Me.txtCodigoProd.TabIndex = 33
        '
        'txtPorcenDescuento
        '
        Me.txtPorcenDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPorcenDescuento.Location = New System.Drawing.Point(167, 324)
        Me.txtPorcenDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcenDescuento.MaxLength = 2
        Me.txtPorcenDescuento.Name = "txtPorcenDescuento"
        Me.txtPorcenDescuento.Size = New System.Drawing.Size(130, 22)
        Me.txtPorcenDescuento.TabIndex = 32
        Me.txtPorcenDescuento.Visible = False
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.AutoSize = True
        Me.txtPorcentaje.Location = New System.Drawing.Point(31, 325)
        Me.txtPorcentaje.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.Size = New System.Drawing.Size(118, 21)
        Me.txtPorcentaje.TabIndex = 31
        Me.txtPorcentaje.Text = " % Descuento"
        Me.txtPorcentaje.UseVisualStyleBackColor = True
        Me.txtPorcentaje.Visible = False
        '
        'luBodegas
        '
        Me.luBodegas.Location = New System.Drawing.Point(167, 90)
        Me.luBodegas.Margin = New System.Windows.Forms.Padding(4)
        Me.luBodegas.Name = "luBodegas"
        Me.luBodegas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luBodegas.Size = New System.Drawing.Size(486, 22)
        Me.luBodegas.TabIndex = 2
        '
        'txtDescuento
        '
        Me.txtDescuento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescuento.Location = New System.Drawing.Point(167, 357)
        Me.txtDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescuento.MaxLength = 12
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(130, 22)
        Me.txtDescuento.TabIndex = 9
        Me.txtDescuento.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(27, 361)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 17)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Descuento:"
        Me.Label10.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 160)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(138, 17)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Cantidad Bonificada:"
        '
        'txtCantBonif
        '
        Me.txtCantBonif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCantBonif.Location = New System.Drawing.Point(167, 158)
        Me.txtCantBonif.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantBonif.MaxLength = 12
        Me.txtCantBonif.Name = "txtCantBonif"
        Me.txtCantBonif.Size = New System.Drawing.Size(130, 22)
        Me.txtCantBonif.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(395, 26)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 17)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Fecha Expiración:"
        Me.Label8.Visible = False
        '
        'deFechaExp
        '
        Me.deFechaExp.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deFechaExp.Location = New System.Drawing.Point(524, 22)
        Me.deFechaExp.Margin = New System.Windows.Forms.Padding(4)
        Me.deFechaExp.Name = "deFechaExp"
        Me.deFechaExp.Size = New System.Drawing.Size(129, 22)
        Me.deFechaExp.TabIndex = 4
        Me.deFechaExp.Visible = False
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.Tbl_ExistBodegasBS
        Me.GridControl1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(320, 126)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(335, 256)
        Me.GridControl1.TabIndex = 10
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Tbl_ExistBodegasBS
        '
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowGroupExpandCollapseButtons = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowViewCaption = True
        Me.GridView1.ViewCaption = "Existencias en Bodegas"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(27, 94)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Bodega:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(27, 290)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(115, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Costo Total (C$):"
        '
        'txtCostoUnit
        '
        Me.txtCostoUnit.BackColor = System.Drawing.Color.White
        Me.txtCostoUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCostoUnit.Location = New System.Drawing.Point(167, 223)
        Me.txtCostoUnit.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCostoUnit.MaxLength = 12
        Me.txtCostoUnit.Name = "txtCostoUnit"
        Me.txtCostoUnit.Size = New System.Drawing.Size(130, 22)
        Me.txtCostoUnit.TabIndex = 7
        '
        'lblCostoTotal
        '
        Me.lblCostoTotal.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCostoTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCostoTotal.Location = New System.Drawing.Point(167, 289)
        Me.lblCostoTotal.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCostoTotal.Name = "lblCostoTotal"
        Me.lblCostoTotal.Size = New System.Drawing.Size(130, 24)
        Me.lblCostoTotal.TabIndex = 8
        '
        'frmDetalleCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(681, 474)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDetalleCompra"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Agregar Producto"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.luBodegas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tbl_ExistBodegasBS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBodegaBS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCostoTotal As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnit As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Tbl_ExistBodegasBS As System.Windows.Forms.BindingSource
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents deFechaExp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCantBonif As System.Windows.Forms.TextBox
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents luBodegas As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblBodegaBS As System.Windows.Forms.BindingSource
    Friend WithEvents txtPorcentaje As System.Windows.Forms.CheckBox
    Friend WithEvents txtPorcenDescuento As System.Windows.Forms.TextBox
    Friend WithEvents txtCodigoProd As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblCostoTotalUSS As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnitUSS As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
