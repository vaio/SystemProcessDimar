﻿Imports DevExpress.XtraNavBar
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class frmEndoseFactura
    Dim cmd As New SqlCommand
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Dim monto As Decimal = 0
    Dim count As Integer = 0
    Public Categoria As Integer = 0
    Public SubCat = 0
    Public Edicion As Integer = 0
    Public Edicion2 As Integer = 0
    Private Sub frmEndoseFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpFechaI.Value = New Date(Now.Year, Now.Month, 1)
        dtpFechaF.Value = New Date(Now.Year, Now.Month, Date.DaysInMonth(Now.Year, Now.Month))
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Select Case NumCatalogo
            Case 40 'endose
                lbMaster.Text = "MASTER ENDOSE"
                Me.Text = "Endose Facturas"
                nbiEndosada.Caption = "Endosadas"
                Categoria = 1
                btnMarcar.Visible = True
            Case 41 'verificacion
                lbMaster.Text = "MASTER VERIFICACION"
                Me.Text = "Verificacion Facturas"
                nbiEndosada.Caption = "Verificadas"
                Categoria = 2
                btnMarcar.Visible = False
        End Select
        Estado()
    End Sub
    Sub Estado()
        With frmPrincipal
            Select Case lbEstado.Text
                Case "--"
                    pMaster.Enabled = False
                    pmasterDeco.ForeColor = SystemColors.AppWorkspace
                    GridControl1.Enabled = False
                    NavBarControl1.Enabled = False
                    btnAplicar.Enabled = False
                    btnMarcar.Enabled = False
                    Label2.Enabled = False
                    dtpFechaI.Enabled = False
                    pdeco.Enabled = False
                    dtpFechaF.Enabled = False
                    .bbiNuevo.Enabled = True
                    .bbiModificar.Enabled = False
                    .bbiEliminar.Enabled = False
                    lbEstado.Visible = False
                Case "RESERVADA"
                    pMaster.Enabled = True
                    pmasterDeco.ForeColor = Color.DodgerBlue
                    GridControl1.Enabled = False
                    NavBarControl1.Enabled = False
                    btnAplicar.Enabled = False
                    btnMarcar.Enabled = False
                    Label2.Enabled = True
                    dtpFechaI.Enabled = True
                    pdeco.Enabled = True
                    dtpFechaF.Enabled = True
                    .bbiNuevo.Enabled = False
                    .bbiModificar.Enabled = False
                    .bbiEliminar.Enabled = True
                    .bbiGuardar.Enabled = True
                    .bbiCancelar.Enabled = True
                    lbEstado.Visible = True
                    Edicion = 0
                Case "PENDIENTE"
                    pMaster.Enabled = False
                    pmasterDeco.ForeColor = SystemColors.AppWorkspace
                    GridControl1.Enabled = True
                    NavBarControl1.Enabled = True
                    btnAplicar.Enabled = True
                    btnMarcar.Enabled = True
                    Label2.Enabled = True
                    dtpFechaI.Enabled = True
                    pdeco.Enabled = True
                    dtpFechaF.Enabled = True
                    .bbiNuevo.Enabled = False
                    .bbiModificar.Enabled = False
                    .bbiGuardar.Enabled = True
                    .bbiCancelar.Enabled = True
                    .bbiEliminar.Enabled = True
                    Edicion = 1
                    Edicion2 = 0
                    SubCat = 2
                    If Categoria = 1 Then
                        RetriveA()
                    Else
                        RetriveB()
                    End If
                    lbEstado.Visible = True
                Case "EMITIDO"
                    pMaster.Enabled = False
                    pmasterDeco.ForeColor = SystemColors.AppWorkspace
                    GridControl1.Enabled = False
                    NavBarControl1.Enabled = False
                    btnAplicar.Enabled = False
                    btnMarcar.Enabled = False
                    Label2.Enabled = False
                    dtpFechaI.Enabled = False
                    pdeco.Enabled = False
                    dtpFechaF.Enabled = False
                    .bbiNuevo.Enabled = True
                    .bbiModificar.Enabled = True
                    .bbiEliminar.Enabled = True
                    lbEstado.Visible = True
                    Edicion = 1
                    Edicion2 = 1
                    SubCat = 1
                    If Categoria = 1 Then
                        RetriveA()
                    Else
                        RetriveB()
                    End If
                Case "ANULADO"
                    pMaster.Enabled = False
                    pmasterDeco.ForeColor = SystemColors.AppWorkspace
                    GridControl1.Enabled = False
                    NavBarControl1.Enabled = False
                    btnAplicar.Enabled = False
                    btnMarcar.Enabled = False
                    Label2.Enabled = False
                    dtpFechaI.Enabled = False
                    pdeco.Enabled = False
                    dtpFechaF.Enabled = False
                    .bbiNuevo.Enabled = True
                    .bbiModificar.Enabled = False
                    .bbiEliminar.Enabled = False
                    lbEstado.Visible = True
                    Edicion = 2
                    Edicion2 = 2
            End Select
        End With
    End Sub
    Private Sub gridview1_cellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        monto = 0
        count = 0
        Select Case Categoria
            Case 1
                For i = 0 To GridView1.RowCount - 1
                    If GridView1.GetRowCellValue(i, "Endosado") = True Then
                        monto += GridView1.GetRowCellValue(i, "Monto")
                        count += 1
                    Else
                        'monto -= GridView1.GetRowCellValue(i, "Monto")
                        'count -= 1
                    End If
                Next
            Case 2
                For i = 0 To GridView1.RowCount - 1
                    If GridView1.GetRowCellValue(i, "Verificado") = True Then
                        monto += GridView1.GetRowCellValue(i, "Monto")
                        count += 1
                    Else
                        'monto -= GridView1.GetRowCellValue(i, "Monto")
                        'count -= 1
                    End If
                Next

        End Select
        txtMonto.Text = String.Format("{0:n2}", monto)
        txtnFactura.Text = count
    End Sub
    Private Sub lbEstado_TextChanged(sender As Object, e As EventArgs) Handles lbEstado.TextChanged
        Estado()
    End Sub
    Sub Nuevo()
        lbEstado.Text = "RESERVADA"
        CargaI()
        txtConcepto.Text = ""
        txtMonto.Text = "0.00"
        txtnFactura.Text = "0"
        txtObservacion.Text = ""
        GridControl1.DataSource = vbNull
        GridView1.Columns.Clear()
        txtConcepto.Focus()
    End Sub
    Sub CargaI()
        Dim dt As DataTable = New DataTable()
        Select Case NumCatalogo
            Case 40
                sqlstring = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(Id_Endoso as int))+1,1))),8)as 'Codigo' from Tbl_Endoso where Id_Sucursal='" & My.Settings.Sucursal & "'"
            Case 41
                sqlstring = "select right('00000000'+Ltrim(Rtrim(isnull(max(cast(Id_Verificacion as int))+1,1))),8)as 'Codigo' from Tbl_Verificacion where Id_Sucursal='" & My.Settings.Sucursal & "'"
        End Select
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            'cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Text))
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtCodigo.Text = row0(0)
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Modificar()
        pMaster.Enabled = False
        GridControl1.Enabled = True
        NavBarControl1.Enabled = True
        btnAplicar.Enabled = True
        btnMarcar.Enabled = True
        Label2.Enabled = True
        dtpFechaI.Enabled = True
        pdeco.Enabled = True
        dtpFechaF.Enabled = True
        With frmPrincipal
            .bbiNuevo.Enabled = False
            .bbiModificar.Enabled = False
            .bbiGuardar.Enabled = True
            .bbiCancelar.Enabled = True
            .bbiEliminar.Enabled = True
        End With
    End Sub
    Sub Cancelar()
        Estado()
    End Sub
    Sub RetriveA() 'ENDOSE
        btnMarcar.Text = "Marcar Todo"
        Select Case SubCat
            Case 1 'Detalle
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Codigo, Factura, Serie, Fecha,Cliente, [Cliente Nombre], Articulo, Monto, Endosado from vw_EndosoDetalle where master='" + txtCodigo.Text + "' order by Fecha"
                Dim tabledatos As DataTable = SQL(sqlstring, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Detalle de Endose"
                dtpFechaI.Value = GridView1.GetRowCellValue(0, "Fecha")
                dtpFechaF.Value = GridView1.GetRowCellValue(GridView1.RowCount - 1, "Fecha")
                GridControl1.Dock = DockStyle.Fill
            Case 2 'Pendiente
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Factura_Detalle.Cantidad,Fecha, Factura, Serie, tFinanciado as 'Monto', [Codigo Cliente], Cliente, upper(Productos.Nombre_Producto) as 'Articulo', Verificado, Contabilizada as 'Endosado' from vw_FacturaCargaF inner join Factura_Detalle on Factura_Detalle.Id_Factura=vw_FacturaCargaF.Factura inner join Productos on Productos.Codigo_Producto=Factura_Detalle.Id_Producto where Tipo=1 and Estado=3 and Financiada=1 and Contabilizada=0 and Fecha >= '" + Convert.ToDateTime(dtpFechaI.Value) + "' and Fecha <='" + Convert.ToDateTime(dtpFechaF.Value) + "'"
                Dim tabledatos As DataTable = SQL(sqlstring, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "vehiculos operativos"
                GridControl1.Dock = DockStyle.Fill
            Case 3 'Endosada
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Factura_Detalle.Cantidad,Fecha, Factura, Serie, tFinanciado as 'Monto', [Codigo Cliente], Cliente, upper(Productos.Nombre_Producto) as 'Articulo', Contabilizada as 'Endosado' from vw_FacturaCargaF inner join Factura_Detalle on Factura_Detalle.Id_Factura=vw_FacturaCargaF.Factura inner join Productos on Productos.Codigo_Producto=Factura_Detalle.Id_Producto where Tipo=1 and Estado=3 and Financiada=1 and Contabilizada=1"
                Dim tabledatos As DataTable = SQL(sqlstring, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "vehiculos administrativos"
                dtpFechaI.Value = GridView1.GetRowCellValue(0, "Fecha")
                dtpFechaF.Value = GridView1.GetRowCellValue(GridView1.RowCount - 1, "Fecha")
                GridControl1.Dock = DockStyle.Fill
        End Select
        frmPrincipal.bbiNuevo.Enabled = True
    End Sub
    Sub RetriveB()  'VERIFICACION
        btnMarcar.Text = "Marcar Todo"
        Select Case SubCat
            Case 1 'Detalle
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Codigo, Factura, Serie, Fecha,Cliente, [Cliente Nombre], Articulo, Monto, Endosado from vw_VerificacionDetalle where master='" + txtCodigo.Text + "' order by Fecha"
                Dim tabledatos As DataTable = SQL(sqlstring, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "Detalle de Endose"
                dtpFechaI.Value = GridView1.GetRowCellValue(0, "Fecha")
                dtpFechaF.Value = GridView1.GetRowCellValue(GridView1.RowCount - 1, "Fecha")
                GridControl1.Dock = DockStyle.Fill
            Case 2 'Pendiente
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Factura_Detalle.Cantidad,Fecha, Factura, Serie, tFinanciado as 'Monto', [Codigo Cliente], Cliente, upper(Productos.Nombre_Producto) as 'Articulo', Contabilizada as 'Endosado', Verificado from vw_FacturaCargaF inner join Factura_Detalle on Factura_Detalle.Id_Factura=vw_FacturaCargaF.Factura inner join Productos on Productos.Codigo_Producto=Factura_Detalle.Id_Producto where Tipo=1 and Estado=3 and Financiada=1 and Contabilizada=0 and Fecha >= '" + Convert.ToDateTime(dtpFechaI.Value) + "' and Fecha <='" + Convert.ToDateTime(dtpFechaF.Value) + "'"
                Dim tabledatos As DataTable = SQL(sqlstring, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "vehiculos operativos"
                GridControl1.Dock = DockStyle.Fill
            Case 3 'Endosada
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                sqlstring = "select Factura_Detalle.Cantidad,Fecha, Factura, Serie, tFinanciado as 'Monto', [Codigo Cliente], Cliente, upper(Productos.Nombre_Producto) as 'Articulo', Contabilizada as 'Endosado' from vw_FacturaCargaF inner join Factura_Detalle on Factura_Detalle.Id_Factura=vw_FacturaCargaF.Factura inner join Productos on Productos.Codigo_Producto=Factura_Detalle.Id_Producto where Tipo=1 and Estado=3 and Financiada=1 and Contabilizada=1"
                Dim tabledatos As DataTable = SQL(sqlstring, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                GridView1.GroupPanelText = "vehiculos administrativos"
                dtpFechaI.Value = GridView1.GetRowCellValue(0, "Fecha")
                dtpFechaF.Value = GridView1.GetRowCellValue(GridView1.RowCount - 1, "Fecha")
                GridControl1.Dock = DockStyle.Fill
        End Select
        frmPrincipal.bbiNuevo.Enabled = True
    End Sub
    Sub Guardar()
        Try
            con.Open()
            Dim cmd As New SqlCommand("SP_Endose", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@edicion", Edicion)
                .AddWithValue("@codigo", txtCodigo.Text)
                .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Value))
                .AddWithValue("@concepto", txtConcepto.Text)
                .AddWithValue("@monto", txtMonto.Text)
                .AddWithValue("@nfactura", txtnFactura.Text)
                .AddWithValue("@observacion", txtObservacion.Text)
                .AddWithValue("@usuario", frmPrincipal.LblNombreUsuario.Text)
                .AddWithValue("@sucursal", My.Settings.Sucursal)
                .AddWithValue("@estado", Edicion + 1)
            End With
            cmd.ExecuteReader()
            con.Close()
            MsgBox("Endose Guardado")
            Select Case lbEstado.Text
                Case "RESERVADA"
                    lbEstado.Text = "PENDIENTE"
                Case "PENDIENTE"
                    lbEstado.Text = "EMITIDO"
                Case "EMITIDO"
                    lbEstado.Text = "EMITIDO"
                Case "ANULADO"
                    lbEstado.Text = "ANULADO"
            End Select
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub GuardarB()
        Try
            con.Open()
            Dim cmd As New SqlCommand("SP_Verificacion", con)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .AddWithValue("@edicion", Edicion)
                .AddWithValue("@codigo", txtCodigo.Text)
                .AddWithValue("@fecha", Convert.ToDateTime(dtpFecha.Value))
                .AddWithValue("@concepto", txtConcepto.Text)
                .AddWithValue("@monto", txtMonto.Text)
                .AddWithValue("@nfactura", txtnFactura.Text)
                .AddWithValue("@observacion", txtObservacion.Text)
                .AddWithValue("@usuario", frmPrincipal.LblNombreUsuario.Text)
                .AddWithValue("@sucursal", My.Settings.Sucursal)
                .AddWithValue("@estado", Edicion + 1)
            End With
            cmd.ExecuteReader()
            con.Close()
            MsgBox("Verificacion Guardada")
            Select Case lbEstado.Text
                Case "RESERVADA"
                    lbEstado.Text = "PENDIENTE"
                Case "PENDIENTE"
                    lbEstado.Text = "EMITIDO"
                Case "EMITIDO"
                    lbEstado.Text = "EMITIDO"
                Case "ANULADO"
                    lbEstado.Text = "ANULADO"
            End Select
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Guardar_Detalle()
        Dim bandera As Boolean = False
        Select Case Categoria
            Case 1
                For i = 0 To GridView1.RowCount - 1
                    If GridView1.GetRowCellValue(i, "Endosado") = True Then
                        Try
                            con.Open()
                            Dim cmd As New SqlCommand("SP_Endose_Detalle", con)
                            cmd.CommandType = CommandType.StoredProcedure
                            With cmd.Parameters
                                .AddWithValue("@edicion", Edicion2)
                                .AddWithValue("@codigoEndoso", txtCodigo.Text)
                                .AddWithValue("@sucursal", My.Settings.Sucursal)
                                .AddWithValue("@codigo", GridView1.GetRowCellValue(i, "Codigo")) 'editar
                                .AddWithValue("@factura", GridView1.GetRowCellValue(i, "Factura"))
                                .AddWithValue("@serie", GridView1.GetRowCellValue(i, "Serie"))
                                .AddWithValue("@Monto", GridView1.GetRowCellValue(i, "Monto"))
                            End With
                            cmd.ExecuteReader()
                            con.Close()
                        Catch ex As Exception
                            MsgBox("Error: " + ex.Message)
                            con.Close()
                        End Try
                        bandera = True
                    End If
                Next
                If bandera = True Then
                    MsgBox("Facturas Endosadas")
                    lbEstado.Text = "EMITIDO"
                    Guardar()
                Else
                    MsgBox("No se ha selecionado facturas")
                End If
            Case 2
                For i = 0 To GridView1.RowCount - 1
                    If GridView1.GetRowCellValue(i, "Endosado") = True Then
                        Try
                            con.Open()
                            Dim cmd As New SqlCommand("SP_Endose_Detalle", con)
                            cmd.CommandType = CommandType.StoredProcedure
                            With cmd.Parameters
                                .AddWithValue("@edicion", Edicion2)
                                .AddWithValue("@codigoEndoso", txtCodigo.Text)
                                .AddWithValue("@sucursal", My.Settings.Sucursal)
                                .AddWithValue("@codigo", GridView1.GetRowCellValue(i, "Codigo")) 'editar
                                .AddWithValue("@factura", GridView1.GetRowCellValue(i, "Factura"))
                                .AddWithValue("@serie", GridView1.GetRowCellValue(i, "Serie"))
                                .AddWithValue("@Monto", GridView1.GetRowCellValue(i, "Monto"))
                            End With
                            cmd.ExecuteReader()
                            con.Close()
                        Catch ex As Exception
                            MsgBox("Error: " + ex.Message)
                            con.Close()
                        End Try
                        bandera = True
                    End If
                Next
                If bandera = True Then
                    MsgBox("Facturas Endosadas")
                    lbEstado.Text = "EMITIDO"
                    Guardar()
                Else
                    MsgBox("No se ha selecionado facturas")
                End If
        End Select
        
    End Sub
    
    Private Sub nbiTodo_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiDetalle.LinkPressed
        SubCat = 1
        RetriveA()
    End Sub

    Private Sub nbiPendiente_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiPendiente.LinkPressed
        SubCat = 2
        RetriveA()
    End Sub

    Private Sub nbiEndosada_LinkPressed(sender As Object, e As NavBarLinkEventArgs) Handles nbiEndosada.LinkPressed
        SubCat = 3
        RetriveA()
    End Sub
    Private Sub btnMarcar_Click(sender As Object, e As EventArgs) Handles btnMarcar.Click
        Select Case Categoria
            Case 1
                If btnMarcar.Text = "Marcar Todo" Then
                    For i = 0 To GridView1.RowCount - 1
                        If GridView1.GetRowCellValue(i, "Verificado") = 1 Then
                            GridView1.SetRowCellValue(i, "Endosado", 1)
                        End If
                    Next
                    btnMarcar.Text = "Desmarcar Todo"
                Else
                    For i = 0 To GridView1.RowCount - 1
                        GridView1.SetRowCellValue(i, "Endosado", 0)
                    Next
                    btnMarcar.Text = "Marcar Todo"
                End If
            Case 2
                If btnMarcar.Text = "Marcar Todo" Then
                    For i = 0 To GridView1.RowCount - 1
                        GridView1.SetRowCellValue(i, "Verificado", 1)
                    Next
                    btnMarcar.Text = "Desmarcar Todo"
                Else
                    For i = 0 To GridView1.RowCount - 1
                        GridView1.SetRowCellValue(i, "Verificado", 0)
                    Next
                    btnMarcar.Text = "Marcar Todo"
                End If
        End Select
        GridView1.ActiveFilter.Clear()
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click
        Guardar_Detalle()
    End Sub
    Private Sub dtpFechaI_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaI.ValueChanged, dtpFechaF.ValueChanged
        If Categoria = 1 Then
            RetriveA()
        Else
            RetriveB()
        End If
    End Sub
End Class
