﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
Public Class FrmFacturacion
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    '-----------
    Dim codCliente As String = ""
    Private Sub FrmFacturacion_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None
    End Sub
    Private Sub FrmFacturacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        TabPrincipal.Width = 405
        Cargar_Datos()
    End Sub
    Sub Cargar_Datos()
        cbCaja.Properties.Items.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Codigo_Caja from Cajas where Activo = 1 and Id_Sucursal='" & My.Settings.Sucursal & "'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbCaja.Properties.Items.Add(row0(0))
            Next
            con.Close()
            cbCaja.SelectedIndex = 0
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Sub Nuevo()
        txtRemision.Text = ""
        txtSolicitud.Text = ""
        txtSucursal.Text = ""
        txtSupervisor.Text = ""
        txtTasaCambio.Text = ""
        dtpFechaI.Value = Today
        cbTipo.SelectedIndex = 0
        txtCliente.Text = ""
        txtCodigoCliente.Text = ""
        txtVendedor.Text = ""
        txtColector.Text = ""
        cbModalidad.SelectedIndex = 0
        ckbIVA.Checked = False
        ckbIR.Checked = False
        ckbIMI.Checked = False
        ckbFinanciado.Checked = False
        ckbPlazoMensual.Checked = False
        txtSaldo.Text = ""
        dtpTurnoTransaccion.Value = Today
        txtObservacion.Text = ""
        txtTcosto.Text = ""
        txtTventa.Text = ""
        txtTdescuento.Text = ""
        txtSubtotal.Text = ""
        txtTiva.Text = ""
        txtTir.Text = ""
        txtTimi.Text = ""
        txtTneto.Text = ""
        TabPrincipal.SelectedIndex = 0
        cbEstado.SelectedIndex = 5
        Control_Enable()
    End Sub
    Sub Borrar()
        sqlstring = "delete from Facturas where Codigo_Factura='" + lbFactura.Text + "' and Serie_Factura='" + lbSerie.Text + "' and Codigo_Sucursal='" & My.Settings.Sucursal & "'"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.ExecuteNonQuery()
            con.Close()
            MsgBox("Factura Eliminada")
            Nuevo()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Sub cargarInicial()
        dtpFechaI.Value = Today
        Tasa_Cambio(dtpFechaI.Value)
        sqlstring = "insert into Facturas (Codigo_Sucursal, Codigo_Factura, Serie_Factura, Tipo_Venta, Fecha_Factura, Fecha_Venc_Factura, Estado_Factura) values (@sucursal, @factura, @serie,@tipo, @fechai, @fechaf, @estado) "
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            With cmd.Parameters
                .AddWithValue("@sucursal", My.Settings.Sucursal)
                .AddWithValue("@factura", lbFactura.Text)
                .AddWithValue("@serie", lbSerie.Text)
                .AddWithValue("@tipo", cbTipo.SelectedIndex + 1)
                .AddWithValue("@fechai", dtpFechaI.Value)
                .AddWithValue("@fechaf", dtpFechaF.Value)
                .AddWithValue("@estado", cbEstado.SelectedIndex + 1)
            End With
            cmd.ExecuteNonQuery()
            con.Close()
            pMaster.Enabled = True
            frmPrincipal.bbiGuardar.Enabled = True
            frmPrincipal.bbiCancelar.Enabled = True
            cbEstado.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Control_Enable()
        pmasterDeco.ForeColor = SystemColors.AppWorkspace
        pdetalleDeco.ForeColor = SystemColors.AppWorkspace
        ppagoDeco.ForeColor = SystemColors.AppWorkspace
        ptotalDeco.ForeColor = SystemColors.AppWorkspace
        pMaster.Enabled = False
        pDetalle.Enabled = False
        pPago.Enabled = False
        pTotal.Enabled = False
        TabPrincipal.Enabled = False
        Select Case cbEstado.SelectedIndex
            Case 0 'Reservada
                pMaster.Enabled = True
                pmasterDeco.ForeColor = Color.DodgerBlue
            Case 1 'Activada
                pDetalle.Enabled = True
                pdetalleDeco.ForeColor = Color.DodgerBlue
                TabPrincipal.Enabled = True
            Case 2 'Cancelada
            Case 3 'Devolucion
            Case 4 'Anulada
            Case 5 'nueva
        End Select
    End Sub
    Sub Guardar()
        cbEstado.SelectedIndex = 1
        sqlstring = "update Facturas set [Tipo_Venta]=@tipo, [Estado_Factura]=@estado, [Codigo_Supervisor]=(select Codigo_Supervisor from Supervisores where Nombre_Supervisor=@supervisor), [Fecha_Factura]=@fechai, [Fecha_Venc_Factura]=@fechaf, [Tipo_Cambio]=@tasa, [Numero_Caja]=@caja where Codigo_Sucursal=@sucursal and Codigo_Factura=@factura and Serie_Factura=@serie"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            With cmd.Parameters
                .AddWithValue("@tipo", cbTipo.SelectedIndex + 1)
                .AddWithValue("@estado", cbEstado.SelectedIndex + 1)
                .AddWithValue("@supervisor", txtSupervisor.Text)
                .AddWithValue("@fechai", dtpFechaI.Value)
                .AddWithValue("@fechaf", dtpFechaF.Value)
                .AddWithValue("@tasa", txtTasaCambio.Text)
                .AddWithValue("@caja", cbCaja.Text)
                .AddWithValue("@sucursal", My.Settings.Sucursal)
                .AddWithValue("@factura", lbFactura.Text)
                .AddWithValue("@serie", lbSerie.Text)
            End With
            cmd.ExecuteNonQuery()
            con.Close()
            Carga_Cliente(txtRemision.Text)
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
            cbEstado.SelectedIndex = 0
        End Try
    End Sub
    Sub Tasa_Cambio(ByVal fecha As Date)
        Dim dt As DataTable = New DataTable
        sqlstring = "select Tasa_Oficial,(select isnull(MAX(cast(Facturas.Codigo_Factura as int))+1,1) from Facturas where Codigo_Sucursal='" & My.Settings.Sucursal & "' and Serie_Factura=(select max(Serie_Factura)from Facturas where Codigo_Sucursal='" & My.Settings.Sucursal & "')) as Factura, (select MAX(Serie_Factura) from Facturas where Codigo_Sucursal='" & My.Settings.Sucursal & "') as Serie from Tbl_TasasCambio where Fecha='" + fecha + "'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtTasaCambio.Text = row0(0)
                If row0(1) = 100000 Then
                    lbFactura.Text = Microsoft.VisualBasic.Right("00000" & 1, 5)
                    lbSerie.Text = Chr(Asc(row0(2)) + 1)
                Else
                    lbFactura.Text = Microsoft.VisualBasic.Right("00000" & row0(1), 5)
                    lbSerie.Text = row0(2)
                End If
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            con.Close()
        End Try
    End Sub
    Sub Carga_Cliente(ByVal remision As String)
        dgvCliente.Rows.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Cliente,(select count(Id_producto) from [Tbl_Factura_Solicitud.Detalle] where Id_Sucursal='" & My.Settings.Sucursal & "' and Id_Remision='" + txtRemision.Text + "'), (select Nombre_Vendedor from Vendedores where Codigo_Vendedor=Tbl_Factura_Solicitud.Id_Vendedor) from Tbl_Factura_Solicitud where Id_Sucursal='" & My.Settings.Sucursal & "' and Id_Remision='" + txtRemision.Text + "'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvCliente.Rows.Add(row0(1), row0(0), row0(2))
            Next
            con.Close()
            dgvCliente.ClearSelection()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Private Sub dtpFechaI_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaI.ValueChanged
        dtpFechaF.Value = dtpFechaI.Value.AddMonths(1)
    End Sub
    Private Sub cbEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbEstado.SelectedIndexChanged
        Control_Enable()
    End Sub
    Private Sub txtCodigoCliente_MouseClick(sender As Object, e As MouseEventArgs) Handles txtCodigoCliente.MouseClick, txtCliente.MouseClick
        TabPrincipal.SelectedIndex = 1
    End Sub
    Private Sub pMaster_MouseClick(sender As Object, e As MouseEventArgs) Handles pTotal.MouseClick, pPago.MouseClick, pMaster.MouseClick, pMain.MouseClick, pDetalle.MouseClick
        TabPrincipal.SelectedIndex = 0
    End Sub
    Private Sub dgvCliente_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCliente.CellMouseClick
        dgvCliente2.Rows.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Codigo_Cliente, Nombre_Cliente from Clientes where Nombre_Cliente like '%" + dgvCliente.Rows(dgvCliente.CurrentRow.Index).Cells(1).Value + "%'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvCliente2.Rows.Add(row0(0), row0(1))
            Next
            con.Close()
            dgvCliente2.ClearSelection()
            If dgvCliente2.Rows.Count = 0 Then
                tabPrincipa_A()
                txtCnombre.Focus()
                Carga_Departamento()
            Else
                tabPrincipal_B()
            End If
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Private Sub dgvCliente2_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCliente2.CellMouseDoubleClick
        If dgvCliente2.Rows.Count > 0 Then
            txtCodigoCliente.Text = dgvCliente2.Rows(dgvCliente2.CurrentRow.Index).Cells(0).Value
            txtCliente.Text = dgvCliente2.Rows(dgvCliente2.CurrentRow.Index).Cells(1).Value
            txtVendedor.Text = dgvCliente.Rows(dgvCliente.CurrentRow.Index).Cells(2).Value
        End If
    End Sub
    Sub tabPrincipa_A()
        TabPrincipal.Width = 1330
        dtpTurnoTransaccion.Visible = False
        txtObservacion.Visible = False
        lbCcoincidencia.Visible = True
    End Sub
    Sub tabPrincipal_B()
        TabPrincipal.Width = 405
        dtpTurnoTransaccion.Visible = True
        txtObservacion.Visible = True
        lbCcoincidencia.Visible = False
    End Sub
    Sub Carga_Departamento()
        cbCdepartamento.Properties.Items.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Nombre from Catalogo.Departamento"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbCdepartamento.Properties.Items.Add(row0(0))
            Next
            con.Close()
            cbCdepartamento.SelectedIndex = 9
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Private Sub btnCcancelar_Click(sender As Object, e As EventArgs) Handles btnCcancelar.Click
        TabPrincipal.Width = 405
        dtpTurnoTransaccion.Visible = True
        txtObservacion.Visible = True
        lbCcoincidencia.Visible = False
    End Sub
    Private Sub cbCtipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCtipo.SelectedIndexChanged
        If cbCtipo.SelectedIndex = 0 Then
            lbCcedula.Text = "Cedula:"
        Else
            lbCcedula.Text = "RUC:"
        End If
    End Sub

    Private Sub cbCdepartamento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCdepartamento.SelectedIndexChanged
        cbCmunicipio.Properties.Items.Clear()
        cbCbarrio.Properties.Items.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Nombre_Municipio from Tbl_Municipio where Id_Departamento=(select Id_Departamento from Catalogo.Departamento where Nombre='" + cbCdepartamento.Text + "')"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbCmunicipio.Properties.Items.Add(row0(0))
            Next
            con.Close()
            cbCmunicipio.SelectedIndex = 0
            cbCbarrio.SelectedIndex = 0
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Private Sub cbCmunicipio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCmunicipio.SelectedIndexChanged
        cbCbarrio.Properties.Items.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Nombre_Barrio from Tbl_Barrios where Id_Zona_Distrito=(select Id_Zona_Distrito from Tbl_Zonas_Distritos where Id_Municipio=(select Id_Municipio from Tbl_Municipio where Nombre_Municipio='" + cbCmunicipio.Text + "'))"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbCbarrio.Properties.Items.Add(row0(0))
            Next
            con.Close()
            cbCbarrio.SelectedIndex = 0
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Private Sub txtCnombre_TextChanged(sender As Object, e As EventArgs) Handles txtCnombre.TextChanged, txtCcedula.TextChanged
        dgvCliente3.Rows.Clear()
        Dim dt As DataTable = New DataTable
        sqlstring = "select Cedula_Cliente, Nombre_Cliente from Clientes where Nombre_Cliente like '%" + txtCnombre.Text + "%' and Cedula_Cliente like '" + txtCcedula.Text + "%'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvCliente3.Rows.Add(row0(0), row0(1))
            Next
            If txtCnombre.Text <> "" And txtCcedula.Text <> "" Then

            End If
            If dgvCliente3.Rows.Count > 0 Then
                dgvCliente3.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Else
                dgvCliente3.Columns(1).Width = 164
            End If
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
    Private Sub btnCaceptar_Click(sender As Object, e As EventArgs) Handles btnCaceptar.Click
        If dgvCliente3.Rows.Count = 0 Then
            Codigo_Cliente()
            'condicion si  tomo el dato del cliente??
            NuevoCliente(codCliente, txtCnombre.Text, txtCcedula.Text, txtCdireccion1.Text, txtCdireccion2.Text,
                         txtCtelefono1.Text, txtCtelefono2.Text, txtCcorreo1.Text, txtCcorreo2.Text, dtpCfechaI.Value,
                         txtCdirecion3.Text, txtCtelefono3.Text, txtCcorreo3.Text, cbCdepartamento.Text, cbCmunicipio.Text, cbCbarrio.Text)
        End If
    End Sub
    Sub Codigo_Cliente()
        Dim dt As DataTable = New DataTable
        sqlstring = "select isnull(MAX (CAST(Codigo_Cliente as int))+1,1) as Maximo from Clientes "
        Dim tblRegMax As DataTable = dt
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            con.Close()
            codCliente = Microsoft.VisualBasic.Right("0000000" & tblRegMax.Rows(0).Item(0), 8)
        Catch ex As Exception
            con.Close()
        End Try
    End Sub
    Function NuevoCliente(ByVal codigo As String, nombre As String, cedula As String, direccion1 As String,
                          ByVal direccion2 As String, tel1 As String, tel2 As String, email1 As String,
                          ByVal email2 As String, fecha As Date, direccion3 As String, tel3 As String, email3 As String, departamento As String, municipio As String, barrio As String)
        sqlstring = "insert into Clientes (Codigo_Cliente, Nombre_Cliente, Cedula_Cliente,Direccion1_Cliente,Direccion2_Cliente,Telefono1_Cliente,Telefono2_Cliente,Email1_Cliente,Email2_Cliente, Fecha_Ingreso, DirTrabajo_Cliente, TelTrabajo_Cliente,EmailTrab_Cliente, Departamento_Cliente, Municipio_Cliente, Barrio_Cliente) values(@codcliente, @nombre, @cedula, @dir1,@dir2, @tel1, @tel2, @email1,@email2, @fecha, @dir3, @tel3, @email3, (select Id_Departamento from Catalogo.Departamento where Nombre= @departamento), (select Id_Municipio from Tbl_Municipio where Nombre_Municipio= @municipio), (select Id_Barrio from Tbl_Barrios where Nombre_Barrio=@barrio))"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            With cmd.Parameters
                .AddWithValue("@codcliente", codigo)
                .AddWithValue("@nombre", nombre)
                .AddWithValue("@cedula", cedula)
                .AddWithValue("@dir1", direccion1)
                .AddWithValue("@dir2", direccion2)
                .AddWithValue("@tel1", tel1)
                .AddWithValue("@tel2", tel2)
                .AddWithValue("@email1", email1)
                .AddWithValue("@email2", email2)
                .AddWithValue("@fecha", fecha)
                .AddWithValue("@dir3", direccion3)
                .AddWithValue("@tel3", tel3)
                .AddWithValue("@email3", email3)
                .AddWithValue("@departamento", cbCdepartamento.Text)
                .AddWithValue("@municipio", cbCmunicipio.Text)
                .AddWithValue("@barrio", cbCbarrio.Text)
            End With
            cmd.ExecuteNonQuery()
            con.Close()
            dgvCliente.Rows(dgvCliente.CurrentRow.Index).Cells(1).Value = txtCnombre.Text
            tabPrincipal_B()
            '-------CAMBIAR: LLAMAR EVENTO DGVCLIENTE2 MOUSECLICK
            dgvCliente2.Rows.Clear()
            Dim dt As DataTable = New DataTable
            sqlstring = "select Codigo_Cliente, Nombre_Cliente from Clientes where Nombre_Cliente like '%" + dgvCliente.Rows(dgvCliente.CurrentRow.Index).Cells(1).Value + "%'"
            cmd = New SqlCommand(sqlstring, con)
            Try
                con.Open()
                adapter = New SqlDataAdapter(cmd)
                adapter.Fill(dt)
                For Each row0 In dt.Rows
                    dgvCliente2.Rows.Add(row0(0), row0(1))
                Next
                con.Close()
                dgvCliente2.ClearSelection()
                If dgvCliente2.Rows.Count = 0 Then
                    tabPrincipa_A()
                    txtCnombre.Focus()
                    Carga_Departamento()
                End If
            Catch ex As Exception
                con.Close()
                MsgBox("Error: " + ex.Message)
            End Try
            '----------------------------------------------------
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
            con.Close()
        End Try
    End Function
    Private Sub cbCbarrio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCbarrio.SelectedIndexChanged
        Dim dt As DataTable = New DataTable
        sqlstring = "select distinct Tbl_Asignacion_CobradorSerie.Id_Serie, Tbl_Empleados.Nombres from Tbl_Empleados inner join Tbl_Asignacion_CobradorSerie on Tbl_Asignacion_CobradorSerie.Id_Colector=Tbl_Empleados.Id_Empleado inner join Tbl_Asignacion_BarrioSerie on Tbl_Asignacion_BarrioSerie.Id_Serie=Tbl_Asignacion_CobradorSerie.Id_Serie inner join Tbl_Barrios on Tbl_Asignacion_BarrioSerie.Id_Barrio=Tbl_Barrios.Id_Barrio where Tbl_Barrios.Nombre_Barrio='" + cbCbarrio.Text + "'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtCcolector.Text = (row0(1))
            Next
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error: " + ex.Message)
        End Try
    End Sub
End Class