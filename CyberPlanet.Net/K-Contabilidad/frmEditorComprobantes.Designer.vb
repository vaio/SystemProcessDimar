﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditorComprobantes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditorComprobantes))
    Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
    Me.Tbl_DetalleComprobBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.luTipoComprob = New DevExpress.XtraEditors.LookUpEdit()
    Me.Tbl_TipoComprobBS = New System.Windows.Forms.BindingSource(Me.components)
    Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
    Me.txtMonto = New System.Windows.Forms.TextBox()
    Me.txtConceptoComprob = New System.Windows.Forms.TextBox()
    Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
    Me.dtpFechaComprob = New System.Windows.Forms.DateTimePicker()
    Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
    Me.txtNumComprob = New System.Windows.Forms.TextBox()
    Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
    Me.LblTitulo = New System.Windows.Forms.Label()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.btnEliminar = New System.Windows.Forms.Button()
    Me.btnModificar = New System.Windows.Forms.Button()
    Me.btnAgregar = New System.Windows.Forms.Button()
    Me.txtSaldoDebito = New System.Windows.Forms.TextBox()
    Me.txtSaldoCredito = New System.Windows.Forms.TextBox()
    Me.txtDiferencia = New System.Windows.Forms.TextBox()
    Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
    Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Tbl_DetalleComprobBS, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel2.SuspendLayout()
    CType(Me.luTipoComprob.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Tbl_TipoComprobBS, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.SuspendLayout()
    '
    'GridControl1
    '
    Me.GridControl1.DataSource = Me.Tbl_DetalleComprobBS
    Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GridControl1.Location = New System.Drawing.Point(0, 178)
    Me.GridControl1.MainView = Me.GridView1
    Me.GridControl1.Name = "GridControl1"
    Me.GridControl1.Size = New System.Drawing.Size(1122, 273)
    Me.GridControl1.TabIndex = 14
    Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
    '
    'GridView1
    '
    Me.GridView1.GridControl = Me.GridControl1
    Me.GridView1.Name = "GridView1"
    Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
    Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
    Me.GridView1.OptionsBehavior.Editable = False
    Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
    Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
    Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
    Me.GridView1.OptionsView.ShowFooter = True
    Me.GridView1.OptionsView.ShowGroupPanel = False
    Me.GridView1.PaintStyleName = "UltraFlat"
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.luTipoComprob)
    Me.Panel2.Controls.Add(Me.LabelControl1)
    Me.Panel2.Controls.Add(Me.txtMonto)
    Me.Panel2.Controls.Add(Me.txtConceptoComprob)
    Me.Panel2.Controls.Add(Me.LabelControl11)
    Me.Panel2.Controls.Add(Me.LabelControl10)
    Me.Panel2.Controls.Add(Me.dtpFechaComprob)
    Me.Panel2.Controls.Add(Me.LabelControl9)
    Me.Panel2.Controls.Add(Me.txtNumComprob)
    Me.Panel2.Controls.Add(Me.LabelControl7)
    Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel2.Location = New System.Drawing.Point(0, 48)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(1122, 130)
    Me.Panel2.TabIndex = 1
    '
    'luTipoComprob
    '
    Me.luTipoComprob.Location = New System.Drawing.Point(136, 20)
    Me.luTipoComprob.Name = "luTipoComprob"
    Me.luTipoComprob.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
    Me.luTipoComprob.Properties.DataSource = Me.Tbl_TipoComprobBS
    Me.luTipoComprob.Size = New System.Drawing.Size(128, 20)
    Me.luTipoComprob.TabIndex = 1
    '
    'LabelControl1
    '
    Me.LabelControl1.Location = New System.Drawing.Point(678, 20)
    Me.LabelControl1.Name = "LabelControl1"
    Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
    Me.LabelControl1.TabIndex = 23
    Me.LabelControl1.Text = "Monto:"
    '
    'txtMonto
    '
    Me.txtMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtMonto.Location = New System.Drawing.Point(718, 17)
    Me.txtMonto.Name = "txtMonto"
    Me.txtMonto.ReadOnly = True
    Me.txtMonto.Size = New System.Drawing.Size(100, 20)
    Me.txtMonto.TabIndex = 0
    Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtConceptoComprob
    '
    Me.txtConceptoComprob.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtConceptoComprob.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtConceptoComprob.Location = New System.Drawing.Point(136, 53)
    Me.txtConceptoComprob.Multiline = True
    Me.txtConceptoComprob.Name = "txtConceptoComprob"
    Me.txtConceptoComprob.Size = New System.Drawing.Size(682, 62)
    Me.txtConceptoComprob.TabIndex = 4
    '
    'LabelControl11
    '
    Me.LabelControl11.Location = New System.Drawing.Point(23, 56)
    Me.LabelControl11.Name = "LabelControl11"
    Me.LabelControl11.Size = New System.Drawing.Size(107, 13)
    Me.LabelControl11.TabIndex = 20
    Me.LabelControl11.Text = "Concepto Movimiento:"
    '
    'LabelControl10
    '
    Me.LabelControl10.Location = New System.Drawing.Point(280, 23)
    Me.LabelControl10.Name = "LabelControl10"
    Me.LabelControl10.Size = New System.Drawing.Size(124, 13)
    Me.LabelControl10.TabIndex = 19
    Me.LabelControl10.Text = "Numero de Comprobante:"
    '
    'dtpFechaComprob
    '
    Me.dtpFechaComprob.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpFechaComprob.Location = New System.Drawing.Point(560, 17)
    Me.dtpFechaComprob.Name = "dtpFechaComprob"
    Me.dtpFechaComprob.Size = New System.Drawing.Size(98, 20)
    Me.dtpFechaComprob.TabIndex = 3
    '
    'LabelControl9
    '
    Me.LabelControl9.Location = New System.Drawing.Point(23, 23)
    Me.LabelControl9.Name = "LabelControl9"
    Me.LabelControl9.Size = New System.Drawing.Size(107, 13)
    Me.LabelControl9.TabIndex = 16
    Me.LabelControl9.Text = "Tipo de Comprobante:"
    '
    'txtNumComprob
    '
    Me.txtNumComprob.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtNumComprob.Location = New System.Drawing.Point(410, 20)
    Me.txtNumComprob.Name = "txtNumComprob"
    Me.txtNumComprob.Size = New System.Drawing.Size(90, 20)
    Me.txtNumComprob.TabIndex = 2
    Me.txtNumComprob.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'LabelControl7
    '
    Me.LabelControl7.Location = New System.Drawing.Point(521, 20)
    Me.LabelControl7.Name = "LabelControl7"
    Me.LabelControl7.Size = New System.Drawing.Size(33, 13)
    Me.LabelControl7.TabIndex = 13
    Me.LabelControl7.Text = "Fecha:"
    '
    'LblTitulo
    '
    Me.LblTitulo.BackColor = System.Drawing.SystemColors.GradientActiveCaption
    Me.LblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.LblTitulo.Dock = System.Windows.Forms.DockStyle.Top
    Me.LblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.LblTitulo.ForeColor = System.Drawing.SystemColors.Highlight
    Me.LblTitulo.Location = New System.Drawing.Point(0, 0)
    Me.LblTitulo.Name = "LblTitulo"
    Me.LblTitulo.Size = New System.Drawing.Size(1122, 48)
    Me.LblTitulo.TabIndex = 0
    Me.LblTitulo.Text = "Edición de Comprobantes"
    Me.LblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Panel3)
    Me.Panel1.Controls.Add(Me.txtSaldoDebito)
    Me.Panel1.Controls.Add(Me.txtSaldoCredito)
    Me.Panel1.Controls.Add(Me.txtDiferencia)
    Me.Panel1.Controls.Add(Me.LabelControl15)
    Me.Panel1.Controls.Add(Me.LabelControl16)
    Me.Panel1.Controls.Add(Me.LabelControl17)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.Panel1.Location = New System.Drawing.Point(0, 358)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(1122, 93)
    Me.Panel1.TabIndex = 2
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.btnEliminar)
    Me.Panel3.Controls.Add(Me.btnModificar)
    Me.Panel3.Controls.Add(Me.btnAgregar)
    Me.Panel3.Location = New System.Drawing.Point(12, 8)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(284, 50)
    Me.Panel3.TabIndex = 10
    '
    'btnEliminar
    '
    Me.btnEliminar.Enabled = False
    Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
    Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.btnEliminar.Location = New System.Drawing.Point(191, 4)
    Me.btnEliminar.Name = "btnEliminar"
    Me.btnEliminar.Size = New System.Drawing.Size(83, 40)
    Me.btnEliminar.TabIndex = 2
    Me.btnEliminar.Text = "&Eliminar"
    Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.btnEliminar.UseVisualStyleBackColor = True
    '
    'btnModificar
    '
    Me.btnModificar.Enabled = False
    Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
    Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.btnModificar.Location = New System.Drawing.Point(102, 4)
    Me.btnModificar.Name = "btnModificar"
    Me.btnModificar.Size = New System.Drawing.Size(83, 40)
    Me.btnModificar.TabIndex = 1
    Me.btnModificar.Text = "&Modificar"
    Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.btnModificar.UseVisualStyleBackColor = True
    '
    'btnAgregar
    '
    Me.btnAgregar.Enabled = False
    Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
    Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.btnAgregar.Location = New System.Drawing.Point(12, 4)
    Me.btnAgregar.Name = "btnAgregar"
    Me.btnAgregar.Size = New System.Drawing.Size(83, 40)
    Me.btnAgregar.TabIndex = 0
    Me.btnAgregar.Text = "&Agregar"
    Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.btnAgregar.UseVisualStyleBackColor = True
    '
    'txtSaldoDebito
    '
    Me.txtSaldoDebito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSaldoDebito.Location = New System.Drawing.Point(824, 12)
    Me.txtSaldoDebito.Name = "txtSaldoDebito"
    Me.txtSaldoDebito.ReadOnly = True
    Me.txtSaldoDebito.Size = New System.Drawing.Size(105, 20)
    Me.txtSaldoDebito.TabIndex = 0
    Me.txtSaldoDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtSaldoCredito
    '
    Me.txtSaldoCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtSaldoCredito.Location = New System.Drawing.Point(1005, 12)
    Me.txtSaldoCredito.Name = "txtSaldoCredito"
    Me.txtSaldoCredito.ReadOnly = True
    Me.txtSaldoCredito.Size = New System.Drawing.Size(105, 20)
    Me.txtSaldoCredito.TabIndex = 1
    Me.txtSaldoCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'txtDiferencia
    '
    Me.txtDiferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtDiferencia.Location = New System.Drawing.Point(1005, 38)
    Me.txtDiferencia.Name = "txtDiferencia"
    Me.txtDiferencia.ReadOnly = True
    Me.txtDiferencia.Size = New System.Drawing.Size(105, 20)
    Me.txtDiferencia.TabIndex = 2
    Me.txtDiferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    '
    'LabelControl15
    '
    Me.LabelControl15.Location = New System.Drawing.Point(945, 38)
    Me.LabelControl15.Name = "LabelControl15"
    Me.LabelControl15.Size = New System.Drawing.Size(52, 13)
    Me.LabelControl15.TabIndex = 9
    Me.LabelControl15.Text = "Diferencia:"
    '
    'LabelControl16
    '
    Me.LabelControl16.Location = New System.Drawing.Point(945, 15)
    Me.LabelControl16.Name = "LabelControl16"
    Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
    Me.LabelControl16.TabIndex = 7
    Me.LabelControl16.Text = "Créditos:"
    '
    'LabelControl17
    '
    Me.LabelControl17.Location = New System.Drawing.Point(778, 15)
    Me.LabelControl17.Name = "LabelControl17"
    Me.LabelControl17.Size = New System.Drawing.Size(40, 13)
    Me.LabelControl17.TabIndex = 5
    Me.LabelControl17.Text = "Débitos:"
    '
    'frmEditorComprobantes
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1122, 451)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.GridControl1)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.LblTitulo)
    Me.Name = "frmEditorComprobantes"
    Me.Text = "Editor Comprobantes"
    CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Tbl_DetalleComprobBS, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    CType(Me.luTipoComprob.Properties, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Tbl_TipoComprobBS, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    Me.ResumeLayout(False)

End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtNumComprob As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LblTitulo As System.Windows.Forms.Label
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtpFechaComprob As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtConceptoComprob As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtSaldoDebito As System.Windows.Forms.TextBox
    Friend WithEvents txtSaldoCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtDiferencia As System.Windows.Forms.TextBox
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents Tbl_DetalleComprobBS As System.Windows.Forms.BindingSource
    Friend WithEvents Tbl_TipoComprobBS As System.Windows.Forms.BindingSource
    Friend WithEvents luTipoComprob As DevExpress.XtraEditors.LookUpEdit
End Class
