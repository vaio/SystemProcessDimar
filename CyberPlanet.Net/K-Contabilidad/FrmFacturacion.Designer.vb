﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFacturacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.TabPrincipal = New System.Windows.Forms.TabControl()
        Me.tabDimar = New System.Windows.Forms.TabPage()
        Me.tabCliente = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtCcolector = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbCtipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.btnCcancelar = New System.Windows.Forms.Button()
        Me.btnCaceptar = New System.Windows.Forms.Button()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.txtCtelefono3 = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txtCcorreo3 = New System.Windows.Forms.TextBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.txtCdirecion3 = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.dgvCliente3 = New System.Windows.Forms.DataGridView()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbCbarrio = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.cbCmunicipio = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.cbCdepartamento = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.dtpCfechaI = New System.Windows.Forms.DateTimePicker()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.txtCdireccion2 = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.txtCdireccion1 = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.txtCcorreo2 = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.txtCcorreo1 = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.txtCtelefono2 = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.txtCtelefono1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCcedula = New System.Windows.Forms.TextBox()
        Me.lbCcedula = New System.Windows.Forms.Label()
        Me.txtCnombre = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.lbCcoincidencia = New System.Windows.Forms.Label()
        Me.dgvCliente2 = New System.Windows.Forms.DataGridView()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lbCremision = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvCliente = New System.Windows.Forms.DataGridView()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabProductos = New System.Windows.Forms.TabPage()
        Me.dgvProducto = New System.Windows.Forms.DataGridView()
        Me.TextBox47 = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.TextBox48 = New System.Windows.Forms.TextBox()
        Me.tabFinanciamiento = New System.Windows.Forms.TabPage()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.tabAbono = New System.Windows.Forms.TabPage()
        Me.TextBox32 = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.TextBox33 = New System.Windows.Forms.TextBox()
        Me.tabMora = New System.Windows.Forms.TabPage()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.tabPago = New System.Windows.Forms.TabPage()
        Me.TextBox39 = New System.Windows.Forms.TextBox()
        Me.TextBox34 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.TextBox35 = New System.Windows.Forms.TextBox()
        Me.TextBox40 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.TextBox36 = New System.Windows.Forms.TextBox()
        Me.TextBox41 = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.TextBox38 = New System.Windows.Forms.TextBox()
        Me.TextBox42 = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.TextBox43 = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.pTotal = New System.Windows.Forms.Panel()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtSubtotal = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtTiva = New System.Windows.Forms.TextBox()
        Me.txtTcosto = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtTir = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtTventa = New System.Windows.Forms.TextBox()
        Me.txtTimi = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.ckbEstado = New System.Windows.Forms.CheckBox()
        Me.txtTneto = New System.Windows.Forms.TextBox()
        Me.txtTdescuento = New System.Windows.Forms.TextBox()
        Me.ckbAnulado = New System.Windows.Forms.CheckBox()
        Me.ckbAprobacion = New System.Windows.Forms.CheckBox()
        Me.ckbComprobante = New System.Windows.Forms.CheckBox()
        Me.pPago = New System.Windows.Forms.Panel()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtSaldo = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.dtpTurnoTransaccion = New System.Windows.Forms.DateTimePicker()
        Me.cbModalidad = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ckbIVA = New System.Windows.Forms.CheckBox()
        Me.ckbIR = New System.Windows.Forms.CheckBox()
        Me.ckbIMI = New System.Windows.Forms.CheckBox()
        Me.ckbFinanciado = New System.Windows.Forms.CheckBox()
        Me.ckbPlazoMensual = New System.Windows.Forms.CheckBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.pDetalle = New System.Windows.Forms.Panel()
        Me.txtCodigoCliente = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtColector = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.lbSerie = New System.Windows.Forms.TextBox()
        Me.cbCaja = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.lbFactura = New System.Windows.Forms.Label()
        Me.cbEstado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.cbTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txtTasaCambio = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dtpFechaF = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtSolicitud = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtRemision = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSupervisor = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.ppagoDeco = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.pdetalleDeco = New System.Windows.Forms.Label()
        Me.ptotalDeco = New System.Windows.Forms.Label()
        Me.pMain.SuspendLayout()
        Me.TabPrincipal.SuspendLayout()
        Me.tabCliente.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cbCtipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCliente3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCbarrio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCmunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCdepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCliente2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProductos.SuspendLayout()
        CType(Me.dgvProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabFinanciamiento.SuspendLayout()
        Me.tabAbono.SuspendLayout()
        Me.tabMora.SuspendLayout()
        Me.tabPago.SuspendLayout()
        Me.pTotal.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pPago.SuspendLayout()
        CType(Me.cbModalidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pDetalle.SuspendLayout()
        Me.pMaster.SuspendLayout()
        CType(Me.cbCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.TabPrincipal)
        Me.pMain.Controls.Add(Me.pTotal)
        Me.pMain.Controls.Add(Me.pPago)
        Me.pMain.Controls.Add(Me.pDetalle)
        Me.pMain.Controls.Add(Me.pMaster)
        Me.pMain.Controls.Add(Me.Label62)
        Me.pMain.Controls.Add(Me.Label55)
        Me.pMain.Controls.Add(Me.Label54)
        Me.pMain.Controls.Add(Me.pmasterDeco)
        Me.pMain.Controls.Add(Me.Label56)
        Me.pMain.Controls.Add(Me.ppagoDeco)
        Me.pMain.Controls.Add(Me.Label63)
        Me.pMain.Controls.Add(Me.pdetalleDeco)
        Me.pMain.Controls.Add(Me.ptotalDeco)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1339, 581)
        Me.pMain.TabIndex = 0
        '
        'TabPrincipal
        '
        Me.TabPrincipal.Controls.Add(Me.tabDimar)
        Me.TabPrincipal.Controls.Add(Me.tabCliente)
        Me.TabPrincipal.Controls.Add(Me.tabProductos)
        Me.TabPrincipal.Controls.Add(Me.tabFinanciamiento)
        Me.TabPrincipal.Controls.Add(Me.tabAbono)
        Me.TabPrincipal.Controls.Add(Me.tabMora)
        Me.TabPrincipal.Controls.Add(Me.tabPago)
        Me.TabPrincipal.Enabled = False
        Me.TabPrincipal.Location = New System.Drawing.Point(9, 182)
        Me.TabPrincipal.Name = "TabPrincipal"
        Me.TabPrincipal.SelectedIndex = 0
        Me.TabPrincipal.Size = New System.Drawing.Size(387, 343)
        Me.TabPrincipal.TabIndex = 111
        '
        'tabDimar
        '
        Me.tabDimar.BackgroundImage = Global.CyberPlanet.Net.My.Resources.Resources.logo
        Me.tabDimar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.tabDimar.Location = New System.Drawing.Point(4, 22)
        Me.tabDimar.Name = "tabDimar"
        Me.tabDimar.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDimar.Size = New System.Drawing.Size(379, 317)
        Me.tabDimar.TabIndex = 2
        Me.tabDimar.Text = "DIMAR S.A."
        Me.tabDimar.UseVisualStyleBackColor = True
        '
        'tabCliente
        '
        Me.tabCliente.BackColor = System.Drawing.Color.White
        Me.tabCliente.Controls.Add(Me.GroupBox1)
        Me.tabCliente.Controls.Add(Me.lbCcoincidencia)
        Me.tabCliente.Controls.Add(Me.dgvCliente2)
        Me.tabCliente.Controls.Add(Me.lbCremision)
        Me.tabCliente.Controls.Add(Me.Label1)
        Me.tabCliente.Controls.Add(Me.dgvCliente)
        Me.tabCliente.Location = New System.Drawing.Point(4, 22)
        Me.tabCliente.Name = "tabCliente"
        Me.tabCliente.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCliente.Size = New System.Drawing.Size(1322, 317)
        Me.tabCliente.TabIndex = 0
        Me.tabCliente.Text = "CLIENTES"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtCcolector)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cbCtipo)
        Me.GroupBox1.Controls.Add(Me.btnCcancelar)
        Me.GroupBox1.Controls.Add(Me.btnCaceptar)
        Me.GroupBox1.Controls.Add(Me.Label78)
        Me.GroupBox1.Controls.Add(Me.Label79)
        Me.GroupBox1.Controls.Add(Me.txtCtelefono3)
        Me.GroupBox1.Controls.Add(Me.Label77)
        Me.GroupBox1.Controls.Add(Me.txtCcorreo3)
        Me.GroupBox1.Controls.Add(Me.Label76)
        Me.GroupBox1.Controls.Add(Me.txtCdirecion3)
        Me.GroupBox1.Controls.Add(Me.Label75)
        Me.GroupBox1.Controls.Add(Me.Label74)
        Me.GroupBox1.Controls.Add(Me.dgvCliente3)
        Me.GroupBox1.Controls.Add(Me.cbCbarrio)
        Me.GroupBox1.Controls.Add(Me.Label72)
        Me.GroupBox1.Controls.Add(Me.cbCmunicipio)
        Me.GroupBox1.Controls.Add(Me.Label71)
        Me.GroupBox1.Controls.Add(Me.cbCdepartamento)
        Me.GroupBox1.Controls.Add(Me.Label70)
        Me.GroupBox1.Controls.Add(Me.dtpCfechaI)
        Me.GroupBox1.Controls.Add(Me.Label69)
        Me.GroupBox1.Controls.Add(Me.txtCdireccion2)
        Me.GroupBox1.Controls.Add(Me.Label64)
        Me.GroupBox1.Controls.Add(Me.txtCdireccion1)
        Me.GroupBox1.Controls.Add(Me.Label67)
        Me.GroupBox1.Controls.Add(Me.txtCcorreo2)
        Me.GroupBox1.Controls.Add(Me.Label58)
        Me.GroupBox1.Controls.Add(Me.txtCcorreo1)
        Me.GroupBox1.Controls.Add(Me.Label59)
        Me.GroupBox1.Controls.Add(Me.txtCtelefono2)
        Me.GroupBox1.Controls.Add(Me.Label57)
        Me.GroupBox1.Controls.Add(Me.txtCtelefono1)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtCcedula)
        Me.GroupBox1.Controls.Add(Me.lbCcedula)
        Me.GroupBox1.Controls.Add(Me.txtCnombre)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label73)
        Me.GroupBox1.Location = New System.Drawing.Point(404, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(906, 301)
        Me.GroupBox1.TabIndex = 200
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "NUEVO CLIENTE / BUSQUEDA MANUAL"
        '
        'txtCcolector
        '
        Me.txtCcolector.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcolector.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCcolector.Location = New System.Drawing.Point(662, 107)
        Me.txtCcolector.MaxLength = 14
        Me.txtCcolector.Name = "txtCcolector"
        Me.txtCcolector.Size = New System.Drawing.Size(219, 20)
        Me.txtCcolector.TabIndex = 210
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(607, 110)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 211
        Me.Label3.Text = "Colector:"
        '
        'cbCtipo
        '
        Me.cbCtipo.EditValue = "DETALLE"
        Me.cbCtipo.Location = New System.Drawing.Point(488, 47)
        Me.cbCtipo.Name = "cbCtipo"
        Me.cbCtipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbCtipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbCtipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCtipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbCtipo.Properties.Items.AddRange(New Object() {"DETALLE", "MAYORISTA"})
        Me.cbCtipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCtipo.Size = New System.Drawing.Size(95, 20)
        Me.cbCtipo.TabIndex = 2
        '
        'btnCcancelar
        '
        Me.btnCcancelar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnCcancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCcancelar.Location = New System.Drawing.Point(508, 250)
        Me.btnCcancelar.Name = "btnCcancelar"
        Me.btnCcancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCcancelar.TabIndex = 18
        Me.btnCcancelar.Text = "Cancelar"
        Me.btnCcancelar.UseVisualStyleBackColor = False
        '
        'btnCaceptar
        '
        Me.btnCaceptar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnCaceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCaceptar.Location = New System.Drawing.Point(427, 250)
        Me.btnCaceptar.Name = "btnCaceptar"
        Me.btnCaceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnCaceptar.TabIndex = 17
        Me.btnCaceptar.Text = "Aceptar"
        Me.btnCaceptar.UseVisualStyleBackColor = False
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(23, 29)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(65, 13)
        Me.Label78.TabIndex = 208
        Me.Label78.Text = "PERSONAL"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.BackColor = System.Drawing.Color.Transparent
        Me.Label79.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label79.Location = New System.Drawing.Point(17, -5)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(578, 42)
        Me.Label79.TabIndex = 209
        Me.Label79.Text = "____________________________"
        Me.Label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCtelefono3
        '
        Me.txtCtelefono3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCtelefono3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCtelefono3.Location = New System.Drawing.Point(87, 254)
        Me.txtCtelefono3.MaxLength = 15
        Me.txtCtelefono3.Name = "txtCtelefono3"
        Me.txtCtelefono3.Size = New System.Drawing.Size(210, 20)
        Me.txtCtelefono3.TabIndex = 15
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(56, 257)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(25, 13)
        Me.Label77.TabIndex = 206
        Me.Label77.Text = "Tel:"
        '
        'txtCcorreo3
        '
        Me.txtCcorreo3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcorreo3.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtCcorreo3.Location = New System.Drawing.Point(373, 210)
        Me.txtCcorreo3.MaxLength = 30
        Me.txtCcorreo3.Name = "txtCcorreo3"
        Me.txtCcorreo3.Size = New System.Drawing.Size(210, 20)
        Me.txtCcorreo3.TabIndex = 16
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(326, 213)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(41, 13)
        Me.Label76.TabIndex = 204
        Me.Label76.Text = "Correo:"
        '
        'txtCdirecion3
        '
        Me.txtCdirecion3.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCdirecion3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCdirecion3.Location = New System.Drawing.Point(87, 210)
        Me.txtCdirecion3.MaxLength = 255
        Me.txtCdirecion3.Multiline = True
        Me.txtCdirecion3.Name = "txtCdirecion3"
        Me.txtCdirecion3.Size = New System.Drawing.Size(210, 37)
        Me.txtCdirecion3.TabIndex = 14
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(26, 213)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(55, 13)
        Me.Label75.TabIndex = 202
        Me.Label75.Text = "Direccion:"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(21, 187)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(56, 13)
        Me.Label74.TabIndex = 198
        Me.Label74.Text = "TRABAJO"
        '
        'dgvCliente3
        '
        Me.dgvCliente3.AllowUserToAddRows = False
        Me.dgvCliente3.AllowUserToDeleteRows = False
        Me.dgvCliente3.AllowUserToResizeColumns = False
        Me.dgvCliente3.AllowUserToResizeRows = False
        Me.dgvCliente3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvCliente3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCliente3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column10, Me.Column9})
        Me.dgvCliente3.Location = New System.Drawing.Point(614, 133)
        Me.dgvCliente3.Name = "dgvCliente3"
        Me.dgvCliente3.ReadOnly = True
        Me.dgvCliente3.RowHeadersVisible = False
        Me.dgvCliente3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCliente3.Size = New System.Drawing.Size(267, 161)
        Me.dgvCliente3.TabIndex = 201
        '
        'Column10
        '
        Me.Column10.HeaderText = "Cedula"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.HeaderText = "Nombre"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 164
        '
        'cbCbarrio
        '
        Me.cbCbarrio.Location = New System.Drawing.Point(414, 159)
        Me.cbCbarrio.Name = "cbCbarrio"
        Me.cbCbarrio.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbCbarrio.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbCbarrio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCbarrio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbCbarrio.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCbarrio.Size = New System.Drawing.Size(169, 20)
        Me.cbCbarrio.TabIndex = 10
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(371, 162)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(37, 13)
        Me.Label72.TabIndex = 169
        Me.Label72.Text = "Barrio:"
        '
        'cbCmunicipio
        '
        Me.cbCmunicipio.Location = New System.Drawing.Point(414, 133)
        Me.cbCmunicipio.Name = "cbCmunicipio"
        Me.cbCmunicipio.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbCmunicipio.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbCmunicipio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCmunicipio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbCmunicipio.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCmunicipio.Size = New System.Drawing.Size(169, 20)
        Me.cbCmunicipio.TabIndex = 9
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(353, 136)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(55, 13)
        Me.Label71.TabIndex = 167
        Me.Label71.Text = "Municipio:"
        '
        'cbCdepartamento
        '
        Me.cbCdepartamento.Location = New System.Drawing.Point(414, 107)
        Me.cbCdepartamento.Name = "cbCdepartamento"
        Me.cbCdepartamento.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbCdepartamento.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbCdepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCdepartamento.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbCdepartamento.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCdepartamento.Size = New System.Drawing.Size(169, 20)
        Me.cbCdepartamento.TabIndex = 8
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(331, 110)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(77, 13)
        Me.Label70.TabIndex = 146
        Me.Label70.Text = "Departamento:"
        '
        'dtpCfechaI
        '
        Me.dtpCfechaI.Location = New System.Drawing.Point(662, 80)
        Me.dtpCfechaI.Name = "dtpCfechaI"
        Me.dtpCfechaI.Size = New System.Drawing.Size(219, 20)
        Me.dtpCfechaI.TabIndex = 13
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(611, 84)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(45, 13)
        Me.Label69.TabIndex = 165
        Me.Label69.Text = "Ingreso:"
        '
        'txtCdireccion2
        '
        Me.txtCdireccion2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCdireccion2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCdireccion2.Location = New System.Drawing.Point(87, 142)
        Me.txtCdireccion2.MaxLength = 255
        Me.txtCdireccion2.Multiline = True
        Me.txtCdireccion2.Name = "txtCdireccion2"
        Me.txtCdireccion2.Size = New System.Drawing.Size(210, 37)
        Me.txtCdireccion2.TabIndex = 7
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(26, 145)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(55, 13)
        Me.Label64.TabIndex = 163
        Me.Label64.Text = "Direccion:"
        '
        'txtCdireccion1
        '
        Me.txtCdireccion1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCdireccion1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCdireccion1.Location = New System.Drawing.Point(87, 99)
        Me.txtCdireccion1.MaxLength = 255
        Me.txtCdireccion1.Multiline = True
        Me.txtCdireccion1.Name = "txtCdireccion1"
        Me.txtCdireccion1.Size = New System.Drawing.Size(210, 37)
        Me.txtCdireccion1.TabIndex = 6
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(26, 102)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(55, 13)
        Me.Label67.TabIndex = 161
        Me.Label67.Text = "Direccion:"
        '
        'txtCcorreo2
        '
        Me.txtCcorreo2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcorreo2.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtCcorreo2.Location = New System.Drawing.Point(662, 53)
        Me.txtCcorreo2.MaxLength = 30
        Me.txtCcorreo2.Name = "txtCcorreo2"
        Me.txtCcorreo2.Size = New System.Drawing.Size(219, 20)
        Me.txtCcorreo2.TabIndex = 12
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(615, 56)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(41, 13)
        Me.Label58.TabIndex = 159
        Me.Label58.Text = "Correo:"
        '
        'txtCcorreo1
        '
        Me.txtCcorreo1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcorreo1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtCcorreo1.Location = New System.Drawing.Point(662, 27)
        Me.txtCcorreo1.MaxLength = 30
        Me.txtCcorreo1.Name = "txtCcorreo1"
        Me.txtCcorreo1.Size = New System.Drawing.Size(219, 20)
        Me.txtCcorreo1.TabIndex = 11
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(615, 30)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(41, 13)
        Me.Label59.TabIndex = 157
        Me.Label59.Text = "Correo:"
        '
        'txtCtelefono2
        '
        Me.txtCtelefono2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCtelefono2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCtelefono2.Location = New System.Drawing.Point(488, 73)
        Me.txtCtelefono2.MaxLength = 15
        Me.txtCtelefono2.Name = "txtCtelefono2"
        Me.txtCtelefono2.Size = New System.Drawing.Size(95, 20)
        Me.txtCtelefono2.TabIndex = 5
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(457, 76)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(25, 13)
        Me.Label57.TabIndex = 155
        Me.Label57.Text = "Tel:"
        '
        'txtCtelefono1
        '
        Me.txtCtelefono1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCtelefono1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCtelefono1.Location = New System.Drawing.Point(348, 73)
        Me.txtCtelefono1.MaxLength = 15
        Me.txtCtelefono1.Name = "txtCtelefono1"
        Me.txtCtelefono1.Size = New System.Drawing.Size(95, 20)
        Me.txtCtelefono1.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(317, 76)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(25, 13)
        Me.Label12.TabIndex = 153
        Me.Label12.Text = "Tel:"
        '
        'txtCcedula
        '
        Me.txtCcedula.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCcedula.Location = New System.Drawing.Point(87, 73)
        Me.txtCcedula.MaxLength = 14
        Me.txtCcedula.Name = "txtCcedula"
        Me.txtCcedula.Size = New System.Drawing.Size(210, 20)
        Me.txtCcedula.TabIndex = 3
        '
        'lbCcedula
        '
        Me.lbCcedula.AutoSize = True
        Me.lbCcedula.Location = New System.Drawing.Point(38, 76)
        Me.lbCcedula.Name = "lbCcedula"
        Me.lbCcedula.Size = New System.Drawing.Size(43, 13)
        Me.lbCcedula.TabIndex = 150
        Me.lbCcedula.Text = "Cedula:"
        '
        'txtCnombre
        '
        Me.txtCnombre.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCnombre.Location = New System.Drawing.Point(87, 47)
        Me.txtCnombre.MaxLength = 75
        Me.txtCnombre.Name = "txtCnombre"
        Me.txtCnombre.Size = New System.Drawing.Size(395, 20)
        Me.txtCnombre.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(34, 50)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 148
        Me.Label7.Text = "Nombre:"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.BackColor = System.Drawing.Color.Transparent
        Me.Label73.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label73.Location = New System.Drawing.Point(15, 153)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(578, 42)
        Me.Label73.TabIndex = 198
        Me.Label73.Text = "____________________________"
        Me.Label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbCcoincidencia
        '
        Me.lbCcoincidencia.AutoSize = True
        Me.lbCcoincidencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCcoincidencia.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.lbCcoincidencia.Location = New System.Drawing.Point(101, 262)
        Me.lbCcoincidencia.Name = "lbCcoincidencia"
        Me.lbCcoincidencia.Size = New System.Drawing.Size(188, 13)
        Me.lbCcoincidencia.TabIndex = 198
        Me.lbCcoincidencia.Text = "*** NO EXISTEN COINCIDENCIAS ***"
        Me.lbCcoincidencia.Visible = False
        '
        'dgvCliente2
        '
        Me.dgvCliente2.AllowUserToAddRows = False
        Me.dgvCliente2.AllowUserToDeleteRows = False
        Me.dgvCliente2.AllowUserToResizeColumns = False
        Me.dgvCliente2.AllowUserToResizeRows = False
        Me.dgvCliente2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvCliente2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCliente2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column6, Me.Column7, Me.Column13})
        Me.dgvCliente2.Location = New System.Drawing.Point(9, 213)
        Me.dgvCliente2.Name = "dgvCliente2"
        Me.dgvCliente2.ReadOnly = True
        Me.dgvCliente2.RowHeadersVisible = False
        Me.dgvCliente2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCliente2.Size = New System.Drawing.Size(377, 96)
        Me.dgvCliente2.TabIndex = 118
        '
        'Column6
        '
        Me.Column6.HeaderText = "Codigo"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 60
        '
        'Column7
        '
        Me.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column7.HeaderText = "Cliente"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Column13
        '
        Me.Column13.HeaderText = "Colector"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        '
        'lbCremision
        '
        Me.lbCremision.AutoSize = True
        Me.lbCremision.Location = New System.Drawing.Point(10, 26)
        Me.lbCremision.Name = "lbCremision"
        Me.lbCremision.Size = New System.Drawing.Size(60, 13)
        Me.lbCremision.TabIndex = 199
        Me.lbCremision.Text = "REMISION"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 197)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 198
        Me.Label1.Text = "COINCIDENCIAS"
        '
        'dgvCliente
        '
        Me.dgvCliente.AllowUserToAddRows = False
        Me.dgvCliente.AllowUserToDeleteRows = False
        Me.dgvCliente.AllowUserToResizeColumns = False
        Me.dgvCliente.AllowUserToResizeRows = False
        Me.dgvCliente.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCliente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column8, Me.DataGridViewTextBoxColumn2, Me.Column11})
        Me.dgvCliente.Location = New System.Drawing.Point(9, 42)
        Me.dgvCliente.Name = "dgvCliente"
        Me.dgvCliente.ReadOnly = True
        Me.dgvCliente.RowHeadersVisible = False
        Me.dgvCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCliente.Size = New System.Drawing.Size(377, 139)
        Me.dgvCliente.TabIndex = 119
        '
        'Column8
        '
        Me.Column8.HeaderText = "Articulos"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 60
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Cliente"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'Column11
        '
        Me.Column11.HeaderText = "Vendedor"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Visible = False
        '
        'tabProductos
        '
        Me.tabProductos.BackColor = System.Drawing.Color.White
        Me.tabProductos.Controls.Add(Me.dgvProducto)
        Me.tabProductos.Controls.Add(Me.TextBox47)
        Me.tabProductos.Controls.Add(Me.Label60)
        Me.tabProductos.Controls.Add(Me.Label61)
        Me.tabProductos.Controls.Add(Me.TextBox48)
        Me.tabProductos.Location = New System.Drawing.Point(4, 22)
        Me.tabProductos.Name = "tabProductos"
        Me.tabProductos.Padding = New System.Windows.Forms.Padding(3)
        Me.tabProductos.Size = New System.Drawing.Size(1322, 317)
        Me.tabProductos.TabIndex = 1
        Me.tabProductos.Text = "PRODUCTO"
        '
        'dgvProducto
        '
        Me.dgvProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProducto.Location = New System.Drawing.Point(9, 45)
        Me.dgvProducto.Name = "dgvProducto"
        Me.dgvProducto.Size = New System.Drawing.Size(377, 264)
        Me.dgvProducto.TabIndex = 123
        '
        'TextBox47
        '
        Me.TextBox47.Location = New System.Drawing.Point(235, 11)
        Me.TextBox47.Name = "TextBox47"
        Me.TextBox47.Size = New System.Drawing.Size(149, 20)
        Me.TextBox47.TabIndex = 122
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(169, 14)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(60, 13)
        Me.Label60.TabIndex = 121
        Me.Label60.Text = "Supervisor:"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(15, 15)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(43, 13)
        Me.Label61.TabIndex = 119
        Me.Label61.Text = "Codigo:"
        '
        'TextBox48
        '
        Me.TextBox48.Location = New System.Drawing.Point(64, 12)
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.Size = New System.Drawing.Size(72, 20)
        Me.TextBox48.TabIndex = 120
        '
        'tabFinanciamiento
        '
        Me.tabFinanciamiento.Controls.Add(Me.TextBox26)
        Me.tabFinanciamiento.Controls.Add(Me.Label27)
        Me.tabFinanciamiento.Controls.Add(Me.Label26)
        Me.tabFinanciamiento.Controls.Add(Me.TextBox25)
        Me.tabFinanciamiento.Controls.Add(Me.Label25)
        Me.tabFinanciamiento.Controls.Add(Me.TextBox24)
        Me.tabFinanciamiento.Location = New System.Drawing.Point(4, 22)
        Me.tabFinanciamiento.Name = "tabFinanciamiento"
        Me.tabFinanciamiento.Padding = New System.Windows.Forms.Padding(3)
        Me.tabFinanciamiento.Size = New System.Drawing.Size(1322, 317)
        Me.tabFinanciamiento.TabIndex = 5
        Me.tabFinanciamiento.Text = "FINAN."
        Me.tabFinanciamiento.UseVisualStyleBackColor = True
        '
        'TextBox26
        '
        Me.TextBox26.Location = New System.Drawing.Point(181, 20)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(78, 20)
        Me.TextBox26.TabIndex = 138
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(112, 23)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(63, 13)
        Me.Label27.TabIndex = 137
        Me.Label27.Text = "Total Prima:"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(98, 49)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(77, 13)
        Me.Label26.TabIndex = 139
        Me.Label26.Text = "Total Principal:"
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(181, 46)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(78, 20)
        Me.TextBox25.TabIndex = 140
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(95, 75)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(80, 13)
        Me.Label25.TabIndex = 141
        Me.Label25.Text = "Total Intereses:"
        '
        'TextBox24
        '
        Me.TextBox24.Location = New System.Drawing.Point(181, 72)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(78, 20)
        Me.TextBox24.TabIndex = 142
        '
        'tabAbono
        '
        Me.tabAbono.Controls.Add(Me.TextBox32)
        Me.tabAbono.Controls.Add(Me.Label32)
        Me.tabAbono.Controls.Add(Me.TextBox29)
        Me.tabAbono.Controls.Add(Me.Label35)
        Me.tabAbono.Controls.Add(Me.Label34)
        Me.tabAbono.Controls.Add(Me.TextBox31)
        Me.tabAbono.Controls.Add(Me.Label33)
        Me.tabAbono.Controls.Add(Me.TextBox30)
        Me.tabAbono.Controls.Add(Me.Label36)
        Me.tabAbono.Controls.Add(Me.DateTimePicker2)
        Me.tabAbono.Controls.Add(Me.Label37)
        Me.tabAbono.Controls.Add(Me.TextBox33)
        Me.tabAbono.Location = New System.Drawing.Point(4, 22)
        Me.tabAbono.Name = "tabAbono"
        Me.tabAbono.Padding = New System.Windows.Forms.Padding(3)
        Me.tabAbono.Size = New System.Drawing.Size(1322, 317)
        Me.tabAbono.TabIndex = 6
        Me.tabAbono.Text = "ABONO"
        Me.tabAbono.UseVisualStyleBackColor = True
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(198, 44)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.Size = New System.Drawing.Size(78, 20)
        Me.TextBox32.TabIndex = 157
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(124, 125)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(68, 13)
        Me.Label32.TabIndex = 154
        Me.Label32.Text = "Abono Mora:"
        '
        'TextBox29
        '
        Me.TextBox29.Location = New System.Drawing.Point(198, 122)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.Size = New System.Drawing.Size(78, 20)
        Me.TextBox29.TabIndex = 155
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(122, 47)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(70, 13)
        Me.Label35.TabIndex = 156
        Me.Label35.Text = "Abono Prima:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(108, 73)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(84, 13)
        Me.Label34.TabIndex = 158
        Me.Label34.Text = "Abono Principal:"
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(198, 70)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.Size = New System.Drawing.Size(78, 20)
        Me.TextBox31.TabIndex = 159
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(105, 99)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(87, 13)
        Me.Label33.TabIndex = 160
        Me.Label33.Text = "Abono Intereses:"
        '
        'TextBox30
        '
        Me.TextBox30.Location = New System.Drawing.Point(198, 96)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New System.Drawing.Size(78, 20)
        Me.TextBox30.TabIndex = 161
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(119, 24)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(73, 13)
        Me.Label36.TabIndex = 162
        Me.Label36.Text = "Ultimo Abono:"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(198, 18)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(78, 20)
        Me.DateTimePicker2.TabIndex = 163
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(124, 151)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(68, 13)
        Me.Label37.TabIndex = 164
        Me.Label37.Text = "Total Abono:"
        '
        'TextBox33
        '
        Me.TextBox33.Location = New System.Drawing.Point(198, 148)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.Size = New System.Drawing.Size(78, 20)
        Me.TextBox33.TabIndex = 165
        '
        'tabMora
        '
        Me.tabMora.Controls.Add(Me.DateTimePicker1)
        Me.tabMora.Controls.Add(Me.Label24)
        Me.tabMora.Controls.Add(Me.Label29)
        Me.tabMora.Controls.Add(Me.TextBox27)
        Me.tabMora.Controls.Add(Me.Label28)
        Me.tabMora.Controls.Add(Me.TextBox23)
        Me.tabMora.Controls.Add(Me.Label23)
        Me.tabMora.Controls.Add(Me.TextBox22)
        Me.tabMora.Controls.Add(Me.Label30)
        Me.tabMora.Controls.Add(Me.TextBox28)
        Me.tabMora.Location = New System.Drawing.Point(4, 22)
        Me.tabMora.Name = "tabMora"
        Me.tabMora.Padding = New System.Windows.Forms.Padding(3)
        Me.tabMora.Size = New System.Drawing.Size(1322, 317)
        Me.tabMora.TabIndex = 7
        Me.tabMora.Text = "MORA"
        Me.tabMora.UseVisualStyleBackColor = True
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(209, 22)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(79, 20)
        Me.DateTimePicker1.TabIndex = 144
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(84, 28)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(119, 13)
        Me.Label24.TabIndex = 143
        Me.Label24.Text = "Ultimo Calculo de Mora:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(119, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(85, 13)
        Me.Label29.TabIndex = 145
        Me.Label29.Text = "Mora Pendiente:"
        '
        'TextBox27
        '
        Me.TextBox27.Location = New System.Drawing.Point(209, 48)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New System.Drawing.Size(78, 20)
        Me.TextBox27.TabIndex = 146
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(114, 78)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(90, 13)
        Me.Label28.TabIndex = 147
        Me.Label28.Text = "Mora Acumulada:"
        '
        'TextBox23
        '
        Me.TextBox23.Location = New System.Drawing.Point(209, 74)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(78, 20)
        Me.TextBox23.TabIndex = 148
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(123, 103)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(69, 13)
        Me.Label23.TabIndex = 149
        Me.Label23.Text = "Total Deuda:"
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(209, 100)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(78, 20)
        Me.TextBox22.TabIndex = 150
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(124, 129)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(71, 13)
        Me.Label30.TabIndex = 151
        Me.Label30.Text = "Monto Cuota:"
        '
        'TextBox28
        '
        Me.TextBox28.Location = New System.Drawing.Point(210, 126)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New System.Drawing.Size(78, 20)
        Me.TextBox28.TabIndex = 152
        '
        'tabPago
        '
        Me.tabPago.Controls.Add(Me.TextBox39)
        Me.tabPago.Controls.Add(Me.TextBox34)
        Me.tabPago.Controls.Add(Me.Label38)
        Me.tabPago.Controls.Add(Me.Label45)
        Me.tabPago.Controls.Add(Me.TextBox35)
        Me.tabPago.Controls.Add(Me.TextBox40)
        Me.tabPago.Controls.Add(Me.Label39)
        Me.tabPago.Controls.Add(Me.Label46)
        Me.tabPago.Controls.Add(Me.TextBox36)
        Me.tabPago.Controls.Add(Me.TextBox41)
        Me.tabPago.Controls.Add(Me.Label42)
        Me.tabPago.Controls.Add(Me.Label47)
        Me.tabPago.Controls.Add(Me.TextBox38)
        Me.tabPago.Controls.Add(Me.TextBox42)
        Me.tabPago.Controls.Add(Me.Label44)
        Me.tabPago.Controls.Add(Me.Label43)
        Me.tabPago.Controls.Add(Me.TextBox43)
        Me.tabPago.Controls.Add(Me.Label48)
        Me.tabPago.Location = New System.Drawing.Point(4, 22)
        Me.tabPago.Name = "tabPago"
        Me.tabPago.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPago.Size = New System.Drawing.Size(1322, 317)
        Me.tabPago.TabIndex = 8
        Me.tabPago.Text = "PAGO"
        Me.tabPago.UseVisualStyleBackColor = True
        '
        'TextBox39
        '
        Me.TextBox39.Location = New System.Drawing.Point(192, 37)
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New System.Drawing.Size(78, 20)
        Me.TextBox39.TabIndex = 167
        '
        'TextBox34
        '
        Me.TextBox34.Location = New System.Drawing.Point(192, 141)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Size = New System.Drawing.Size(78, 20)
        Me.TextBox34.TabIndex = 175
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(124, 144)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(57, 13)
        Me.Label38.TabIndex = 174
        Me.Label38.Text = "CS Tarjeta"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(124, 170)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(58, 13)
        Me.Label45.TabIndex = 176
        Me.Label45.Text = "US Tarjeta"
        '
        'TextBox35
        '
        Me.TextBox35.Location = New System.Drawing.Point(192, 115)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New System.Drawing.Size(78, 20)
        Me.TextBox35.TabIndex = 173
        '
        'TextBox40
        '
        Me.TextBox40.Location = New System.Drawing.Point(192, 167)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.Size = New System.Drawing.Size(78, 20)
        Me.TextBox40.TabIndex = 177
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(124, 118)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(62, 13)
        Me.Label39.TabIndex = 172
        Me.Label39.Text = "US Cheque"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(111, 196)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(76, 13)
        Me.Label46.TabIndex = 178
        Me.Label46.Text = "Codigo Tarjeta"
        '
        'TextBox36
        '
        Me.TextBox36.Location = New System.Drawing.Point(192, 89)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New System.Drawing.Size(78, 20)
        Me.TextBox36.TabIndex = 171
        '
        'TextBox41
        '
        Me.TextBox41.Location = New System.Drawing.Point(192, 193)
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.Size = New System.Drawing.Size(78, 20)
        Me.TextBox41.TabIndex = 179
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(129, 92)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(61, 13)
        Me.Label42.TabIndex = 170
        Me.Label42.Text = "CS Cheque"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(129, 222)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(54, 13)
        Me.Label47.TabIndex = 180
        Me.Label47.Text = "N. Tarjeta"
        '
        'TextBox38
        '
        Me.TextBox38.Location = New System.Drawing.Point(192, 63)
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New System.Drawing.Size(78, 20)
        Me.TextBox38.TabIndex = 169
        '
        'TextBox42
        '
        Me.TextBox42.Location = New System.Drawing.Point(192, 219)
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.Size = New System.Drawing.Size(78, 20)
        Me.TextBox42.TabIndex = 181
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(115, 40)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(63, 13)
        Me.Label44.TabIndex = 166
        Me.Label44.Text = "CS Efectivo"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(122, 69)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(64, 13)
        Me.Label43.TabIndex = 168
        Me.Label43.Text = "US Efectivo"
        '
        'TextBox43
        '
        Me.TextBox43.Location = New System.Drawing.Point(192, 245)
        Me.TextBox43.Name = "TextBox43"
        Me.TextBox43.Size = New System.Drawing.Size(78, 20)
        Me.TextBox43.TabIndex = 183
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(129, 248)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(58, 13)
        Me.Label48.TabIndex = 182
        Me.Label48.Text = "N. Cheque"
        '
        'pTotal
        '
        Me.pTotal.Controls.Add(Me.dgvDetalle)
        Me.pTotal.Controls.Add(Me.Label20)
        Me.pTotal.Controls.Add(Me.txtSubtotal)
        Me.pTotal.Controls.Add(Me.Label19)
        Me.pTotal.Controls.Add(Me.txtTiva)
        Me.pTotal.Controls.Add(Me.txtTcosto)
        Me.pTotal.Controls.Add(Me.Label18)
        Me.pTotal.Controls.Add(Me.Label15)
        Me.pTotal.Controls.Add(Me.txtTir)
        Me.pTotal.Controls.Add(Me.Label16)
        Me.pTotal.Controls.Add(Me.Label21)
        Me.pTotal.Controls.Add(Me.txtTventa)
        Me.pTotal.Controls.Add(Me.txtTimi)
        Me.pTotal.Controls.Add(Me.Label17)
        Me.pTotal.Controls.Add(Me.Label22)
        Me.pTotal.Controls.Add(Me.ckbEstado)
        Me.pTotal.Controls.Add(Me.txtTneto)
        Me.pTotal.Controls.Add(Me.txtTdescuento)
        Me.pTotal.Controls.Add(Me.ckbAnulado)
        Me.pTotal.Controls.Add(Me.ckbAprobacion)
        Me.pTotal.Controls.Add(Me.ckbComprobante)
        Me.pTotal.Enabled = False
        Me.pTotal.Location = New System.Drawing.Point(414, 228)
        Me.pTotal.Name = "pTotal"
        Me.pTotal.Size = New System.Drawing.Size(920, 297)
        Me.pTotal.TabIndex = 197
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToOrderColumns = True
        Me.dgvDetalle.AllowUserToResizeColumns = False
        Me.dgvDetalle.AllowUserToResizeRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5})
        Me.dgvDetalle.Location = New System.Drawing.Point(45, 15)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(666, 246)
        Me.dgvDetalle.TabIndex = 10
        '
        'Column1
        '
        Me.Column1.HeaderText = "Codigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column2.HeaderText = "Descripcion"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Cantidad"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Costo"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Precio"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(761, 140)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(56, 13)
        Me.Label20.TabIndex = 126
        Me.Label20.Text = "Sub-Total:"
        '
        'txtSubtotal
        '
        Me.txtSubtotal.Location = New System.Drawing.Point(823, 137)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.Size = New System.Drawing.Size(78, 20)
        Me.txtSubtotal.TabIndex = 127
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(737, 166)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(80, 13)
        Me.Label19.TabIndex = 128
        Me.Label19.Text = "Total Impuesto:"
        '
        'txtTiva
        '
        Me.txtTiva.Location = New System.Drawing.Point(823, 163)
        Me.txtTiva.Name = "txtTiva"
        Me.txtTiva.Size = New System.Drawing.Size(78, 20)
        Me.txtTiva.TabIndex = 129
        '
        'txtTcosto
        '
        Me.txtTcosto.Location = New System.Drawing.Point(823, 15)
        Me.txtTcosto.Name = "txtTcosto"
        Me.txtTcosto.Size = New System.Drawing.Size(78, 20)
        Me.txtTcosto.TabIndex = 121
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(769, 192)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(48, 13)
        Me.Label18.TabIndex = 130
        Me.Label18.Text = "Total IR:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(753, 18)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(64, 13)
        Me.Label15.TabIndex = 120
        Me.Label15.Text = "Total Costo:"
        '
        'txtTir
        '
        Me.txtTir.Location = New System.Drawing.Point(823, 189)
        Me.txtTir.Name = "txtTir"
        Me.txtTir.Size = New System.Drawing.Size(78, 20)
        Me.txtTir.TabIndex = 131
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(752, 44)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(65, 13)
        Me.Label16.TabIndex = 122
        Me.Label16.Text = "Total Venta:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(765, 218)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(52, 13)
        Me.Label21.TabIndex = 132
        Me.Label21.Text = "Total IMI:"
        '
        'txtTventa
        '
        Me.txtTventa.Location = New System.Drawing.Point(823, 41)
        Me.txtTventa.Name = "txtTventa"
        Me.txtTventa.Size = New System.Drawing.Size(78, 20)
        Me.txtTventa.TabIndex = 123
        '
        'txtTimi
        '
        Me.txtTimi.Location = New System.Drawing.Point(823, 215)
        Me.txtTimi.Name = "txtTimi"
        Me.txtTimi.Size = New System.Drawing.Size(78, 20)
        Me.txtTimi.TabIndex = 133
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(728, 70)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(89, 13)
        Me.Label17.TabIndex = 124
        Me.Label17.Text = "Total Descuento:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(757, 244)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(60, 13)
        Me.Label22.TabIndex = 134
        Me.Label22.Text = "Total Neto:"
        '
        'ckbEstado
        '
        Me.ckbEstado.AutoSize = True
        Me.ckbEstado.Location = New System.Drawing.Point(287, 274)
        Me.ckbEstado.Name = "ckbEstado"
        Me.ckbEstado.Size = New System.Drawing.Size(110, 17)
        Me.ckbEstado.TabIndex = 187
        Me.ckbEstado.Text = "Estado de factura"
        Me.ckbEstado.UseVisualStyleBackColor = True
        '
        'txtTneto
        '
        Me.txtTneto.Location = New System.Drawing.Point(823, 241)
        Me.txtTneto.Name = "txtTneto"
        Me.txtTneto.Size = New System.Drawing.Size(78, 20)
        Me.txtTneto.TabIndex = 135
        '
        'txtTdescuento
        '
        Me.txtTdescuento.Location = New System.Drawing.Point(823, 67)
        Me.txtTdescuento.Name = "txtTdescuento"
        Me.txtTdescuento.Size = New System.Drawing.Size(78, 20)
        Me.txtTdescuento.TabIndex = 125
        '
        'ckbAnulado
        '
        Me.ckbAnulado.AutoSize = True
        Me.ckbAnulado.Location = New System.Drawing.Point(45, 274)
        Me.ckbAnulado.Name = "ckbAnulado"
        Me.ckbAnulado.Size = New System.Drawing.Size(65, 17)
        Me.ckbAnulado.TabIndex = 184
        Me.ckbAnulado.Text = "Anulada"
        Me.ckbAnulado.UseVisualStyleBackColor = True
        '
        'ckbAprobacion
        '
        Me.ckbAprobacion.AutoSize = True
        Me.ckbAprobacion.Location = New System.Drawing.Point(211, 274)
        Me.ckbAprobacion.Name = "ckbAprobacion"
        Me.ckbAprobacion.Size = New System.Drawing.Size(80, 17)
        Me.ckbAprobacion.TabIndex = 186
        Me.ckbAprobacion.Text = "Aprobacion"
        Me.ckbAprobacion.UseVisualStyleBackColor = True
        '
        'ckbComprobante
        '
        Me.ckbComprobante.AutoSize = True
        Me.ckbComprobante.Location = New System.Drawing.Point(116, 274)
        Me.ckbComprobante.Name = "ckbComprobante"
        Me.ckbComprobante.Size = New System.Drawing.Size(89, 17)
        Me.ckbComprobante.TabIndex = 185
        Me.ckbComprobante.Text = "Comprobante"
        Me.ckbComprobante.UseVisualStyleBackColor = True
        '
        'pPago
        '
        Me.pPago.Controls.Add(Me.Label68)
        Me.pPago.Controls.Add(Me.Label66)
        Me.pPago.Controls.Add(Me.txtObservacion)
        Me.pPago.Controls.Add(Me.Label65)
        Me.pPago.Controls.Add(Me.Label13)
        Me.pPago.Controls.Add(Me.Label41)
        Me.pPago.Controls.Add(Me.txtSaldo)
        Me.pPago.Controls.Add(Me.Label40)
        Me.pPago.Controls.Add(Me.dtpTurnoTransaccion)
        Me.pPago.Controls.Add(Me.cbModalidad)
        Me.pPago.Controls.Add(Me.ckbIVA)
        Me.pPago.Controls.Add(Me.ckbIR)
        Me.pPago.Controls.Add(Me.ckbIMI)
        Me.pPago.Controls.Add(Me.ckbFinanciado)
        Me.pPago.Controls.Add(Me.ckbPlazoMensual)
        Me.pPago.Controls.Add(Me.Label31)
        Me.pPago.Enabled = False
        Me.pPago.Location = New System.Drawing.Point(4, 149)
        Me.pPago.Name = "pPago"
        Me.pPago.Size = New System.Drawing.Size(1331, 60)
        Me.pPago.TabIndex = 196
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.Color.Transparent
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label68.Location = New System.Drawing.Point(652, 0)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(17, 25)
        Me.Label68.TabIndex = 193
        Me.Label68.Text = "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.BackColor = System.Drawing.Color.Transparent
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label66.Location = New System.Drawing.Point(451, 0)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(17, 25)
        Me.Label66.TabIndex = 192
        Me.Label66.Text = "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(953, 5)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(360, 47)
        Me.txtObservacion.TabIndex = 12
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.Color.Transparent
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label65.Location = New System.Drawing.Point(293, 0)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(17, 25)
        Me.Label65.TabIndex = 191
        Me.Label65.Text = "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(866, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(81, 13)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Observaciones:"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(679, 9)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(64, 13)
        Me.Label41.TabIndex = 68
        Me.Label41.Text = "Total Saldo:"
        '
        'txtSaldo
        '
        Me.txtSaldo.Location = New System.Drawing.Point(750, 4)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(78, 20)
        Me.txtSaldo.TabIndex = 69
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(643, 32)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(100, 13)
        Me.Label40.TabIndex = 70
        Me.Label40.Text = "Turno Transaccion:"
        '
        'dtpTurnoTransaccion
        '
        Me.dtpTurnoTransaccion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTurnoTransaccion.Location = New System.Drawing.Point(749, 30)
        Me.dtpTurnoTransaccion.Name = "dtpTurnoTransaccion"
        Me.dtpTurnoTransaccion.Size = New System.Drawing.Size(79, 20)
        Me.dtpTurnoTransaccion.TabIndex = 76
        '
        'cbModalidad
        '
        Me.cbModalidad.EditValue = "SEMANAL"
        Me.cbModalidad.Location = New System.Drawing.Point(118, 4)
        Me.cbModalidad.Name = "cbModalidad"
        Me.cbModalidad.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbModalidad.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbModalidad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbModalidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbModalidad.Properties.Items.AddRange(New Object() {"SEMANAL", "QUINCENAL", "MENSUAL"})
        Me.cbModalidad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbModalidad.Size = New System.Drawing.Size(158, 20)
        Me.cbModalidad.TabIndex = 145
        Me.cbModalidad.TabStop = False
        '
        'ckbIVA
        '
        Me.ckbIVA.AutoSize = True
        Me.ckbIVA.Location = New System.Drawing.Point(318, 6)
        Me.ckbIVA.Name = "ckbIVA"
        Me.ckbIVA.Size = New System.Drawing.Size(43, 17)
        Me.ckbIVA.TabIndex = 117
        Me.ckbIVA.Text = "IVA"
        Me.ckbIVA.UseVisualStyleBackColor = True
        '
        'ckbIR
        '
        Me.ckbIR.AutoSize = True
        Me.ckbIR.Location = New System.Drawing.Point(367, 6)
        Me.ckbIR.Name = "ckbIR"
        Me.ckbIR.Size = New System.Drawing.Size(37, 17)
        Me.ckbIR.TabIndex = 118
        Me.ckbIR.Text = "IR"
        Me.ckbIR.UseVisualStyleBackColor = True
        '
        'ckbIMI
        '
        Me.ckbIMI.AutoSize = True
        Me.ckbIMI.Location = New System.Drawing.Point(410, 6)
        Me.ckbIMI.Name = "ckbIMI"
        Me.ckbIMI.Size = New System.Drawing.Size(41, 17)
        Me.ckbIMI.TabIndex = 119
        Me.ckbIMI.Text = "IMI"
        Me.ckbIMI.UseVisualStyleBackColor = True
        '
        'ckbFinanciado
        '
        Me.ckbFinanciado.AutoSize = True
        Me.ckbFinanciado.Location = New System.Drawing.Point(472, 6)
        Me.ckbFinanciado.Name = "ckbFinanciado"
        Me.ckbFinanciado.Size = New System.Drawing.Size(78, 17)
        Me.ckbFinanciado.TabIndex = 136
        Me.ckbFinanciado.Text = "Financiada"
        Me.ckbFinanciado.UseVisualStyleBackColor = True
        '
        'ckbPlazoMensual
        '
        Me.ckbPlazoMensual.AutoSize = True
        Me.ckbPlazoMensual.Location = New System.Drawing.Point(556, 6)
        Me.ckbPlazoMensual.Name = "ckbPlazoMensual"
        Me.ckbPlazoMensual.Size = New System.Drawing.Size(95, 17)
        Me.ckbPlazoMensual.TabIndex = 153
        Me.ckbPlazoMensual.Text = "Plazo Mensual"
        Me.ckbPlazoMensual.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(10, 8)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(102, 13)
        Me.Label31.TabIndex = 53
        Me.Label31.Text = "Modalidad de Pago:"
        '
        'pDetalle
        '
        Me.pDetalle.Controls.Add(Me.txtCodigoCliente)
        Me.pDetalle.Controls.Add(Me.Label11)
        Me.pDetalle.Controls.Add(Me.txtVendedor)
        Me.pDetalle.Controls.Add(Me.Label5)
        Me.pDetalle.Controls.Add(Me.txtColector)
        Me.pDetalle.Controls.Add(Me.Label8)
        Me.pDetalle.Controls.Add(Me.Label6)
        Me.pDetalle.Controls.Add(Me.txtCliente)
        Me.pDetalle.Enabled = False
        Me.pDetalle.Location = New System.Drawing.Point(4, 91)
        Me.pDetalle.Name = "pDetalle"
        Me.pDetalle.Size = New System.Drawing.Size(1331, 39)
        Me.pDetalle.TabIndex = 195
        '
        'txtCodigoCliente
        '
        Me.txtCodigoCliente.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigoCliente.Location = New System.Drawing.Point(79, 10)
        Me.txtCodigoCliente.Name = "txtCodigoCliente"
        Me.txtCodigoCliente.ReadOnly = True
        Me.txtCodigoCliente.Size = New System.Drawing.Size(72, 20)
        Me.txtCodigoCliente.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(584, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 13)
        Me.Label11.TabIndex = 101
        Me.Label11.Text = "Vendedor:"
        '
        'txtVendedor
        '
        Me.txtVendedor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtVendedor.Location = New System.Drawing.Point(643, 10)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.ReadOnly = True
        Me.txtVendedor.Size = New System.Drawing.Size(277, 20)
        Me.txtVendedor.TabIndex = 102
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(981, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 103
        Me.Label5.Text = "Colector:"
        '
        'txtColector
        '
        Me.txtColector.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtColector.Location = New System.Drawing.Point(1036, 10)
        Me.txtColector.Name = "txtColector"
        Me.txtColector.ReadOnly = True
        Me.txtColector.Size = New System.Drawing.Size(277, 20)
        Me.txtColector.TabIndex = 104
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 13)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Cod. Cliente:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(194, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Nombre:"
        '
        'txtCliente
        '
        Me.txtCliente.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCliente.Location = New System.Drawing.Point(247, 10)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.ReadOnly = True
        Me.txtCliente.Size = New System.Drawing.Size(277, 20)
        Me.txtCliente.TabIndex = 5
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.lbSerie)
        Me.pMaster.Controls.Add(Me.cbCaja)
        Me.pMaster.Controls.Add(Me.lbFactura)
        Me.pMaster.Controls.Add(Me.cbEstado)
        Me.pMaster.Controls.Add(Me.Label53)
        Me.pMaster.Controls.Add(Me.cbTipo)
        Me.pMaster.Controls.Add(Me.txtTasaCambio)
        Me.pMaster.Controls.Add(Me.Label14)
        Me.pMaster.Controls.Add(Me.dtpFechaF)
        Me.pMaster.Controls.Add(Me.Label4)
        Me.pMaster.Controls.Add(Me.dtpFechaI)
        Me.pMaster.Controls.Add(Me.Label51)
        Me.pMaster.Controls.Add(Me.Label50)
        Me.pMaster.Controls.Add(Me.txtSolicitud)
        Me.pMaster.Controls.Add(Me.Label49)
        Me.pMaster.Controls.Add(Me.txtSucursal)
        Me.pMaster.Controls.Add(Me.Label10)
        Me.pMaster.Controls.Add(Me.Label9)
        Me.pMaster.Controls.Add(Me.txtRemision)
        Me.pMaster.Controls.Add(Me.Label2)
        Me.pMaster.Controls.Add(Me.txtSupervisor)
        Me.pMaster.Controls.Add(Me.Label52)
        Me.pMaster.Enabled = False
        Me.pMaster.Location = New System.Drawing.Point(4, 13)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(1330, 61)
        Me.pMaster.TabIndex = 194
        '
        'lbSerie
        '
        Me.lbSerie.BackColor = System.Drawing.SystemColors.Control
        Me.lbSerie.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lbSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbSerie.Location = New System.Drawing.Point(1244, 33)
        Me.lbSerie.Name = "lbSerie"
        Me.lbSerie.Size = New System.Drawing.Size(67, 19)
        Me.lbSerie.TabIndex = 145
        Me.lbSerie.Text = "A"
        Me.lbSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cbCaja
        '
        Me.cbCaja.EditValue = ""
        Me.cbCaja.Location = New System.Drawing.Point(397, 40)
        Me.cbCaja.Name = "cbCaja"
        Me.cbCaja.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbCaja.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbCaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCaja.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbCaja.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCaja.Size = New System.Drawing.Size(100, 20)
        Me.cbCaja.TabIndex = 144
        Me.cbCaja.TabStop = False
        '
        'lbFactura
        '
        Me.lbFactura.AutoSize = True
        Me.lbFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbFactura.ForeColor = System.Drawing.Color.Red
        Me.lbFactura.Location = New System.Drawing.Point(1247, 12)
        Me.lbFactura.Name = "lbFactura"
        Me.lbFactura.Size = New System.Drawing.Size(72, 25)
        Me.lbFactura.TabIndex = 142
        Me.lbFactura.Text = "00000"
        '
        'cbEstado
        '
        Me.cbEstado.EditValue = ""
        Me.cbEstado.Enabled = False
        Me.cbEstado.Location = New System.Drawing.Point(1096, 40)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbEstado.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbEstado.Properties.AppearanceDisabled.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cbEstado.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbEstado.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEstado.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbEstado.Properties.Items.AddRange(New Object() {"RESERVADA", "ACTIVA", "CANCELADA", "DEVOLUCION", "ANULADA", "-----------------"})
        Me.cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbEstado.Size = New System.Drawing.Size(126, 20)
        Me.cbEstado.TabIndex = 141
        Me.cbEstado.TabStop = False
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(1047, 43)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(43, 13)
        Me.Label53.TabIndex = 140
        Me.Label53.Text = "Estado:"
        '
        'cbTipo
        '
        Me.cbTipo.EditValue = "CREDITO"
        Me.cbTipo.Location = New System.Drawing.Point(1096, 15)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbTipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTipo.Properties.Items.AddRange(New Object() {"CREDITO", "CONTADO", "CREDITO/CONTADO"})
        Me.cbTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTipo.Size = New System.Drawing.Size(126, 20)
        Me.cbTipo.TabIndex = 139
        Me.cbTipo.TabStop = False
        '
        'txtTasaCambio
        '
        Me.txtTasaCambio.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtTasaCambio.Location = New System.Drawing.Point(592, 40)
        Me.txtTasaCambio.Name = "txtTasaCambio"
        Me.txtTasaCambio.ReadOnly = True
        Me.txtTasaCambio.Size = New System.Drawing.Size(83, 20)
        Me.txtTasaCambio.TabIndex = 138
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(503, 43)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(83, 13)
        Me.Label14.TabIndex = 137
        Me.Label14.Text = "Tipo de cambio:"
        '
        'dtpFechaF
        '
        Me.dtpFechaF.Location = New System.Drawing.Point(806, 40)
        Me.dtpFechaF.Name = "dtpFechaF"
        Me.dtpFechaF.Size = New System.Drawing.Size(184, 20)
        Me.dtpFechaF.TabIndex = 134
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(732, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 133
        Me.Label4.Text = "Vencimiento:"
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Location = New System.Drawing.Point(806, 15)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(184, 20)
        Me.dtpFechaI.TabIndex = 132
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(723, 18)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(79, 13)
        Me.Label51.TabIndex = 131
        Me.Label51.Text = "Fecha Factura:"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(346, 43)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(45, 13)
        Me.Label50.TabIndex = 129
        Me.Label50.Text = "N. Caja:"
        '
        'txtSolicitud
        '
        Me.txtSolicitud.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSolicitud.Location = New System.Drawing.Point(243, 15)
        Me.txtSolicitud.Name = "txtSolicitud"
        Me.txtSolicitud.ReadOnly = True
        Me.txtSolicitud.Size = New System.Drawing.Size(52, 20)
        Me.txtSolicitud.TabIndex = 128
        Me.txtSolicitud.TabStop = False
        Me.txtSolicitud.Text = "00000"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(179, 18)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(58, 13)
        Me.Label49.TabIndex = 127
        Me.Label49.Text = "N. Pagare:"
        '
        'txtSucursal
        '
        Me.txtSucursal.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSucursal.Location = New System.Drawing.Point(78, 40)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.ReadOnly = True
        Me.txtSucursal.Size = New System.Drawing.Size(217, 20)
        Me.txtSucursal.TabIndex = 126
        Me.txtSucursal.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(21, 43)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(51, 13)
        Me.Label10.TabIndex = 125
        Me.Label10.Text = "Sucursal:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(1028, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 13)
        Me.Label9.TabIndex = 122
        Me.Label9.Text = "Tipo Venta:"
        '
        'txtRemision
        '
        Me.txtRemision.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtRemision.Location = New System.Drawing.Point(78, 15)
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.ReadOnly = True
        Me.txtRemision.Size = New System.Drawing.Size(72, 20)
        Me.txtRemision.TabIndex = 120
        Me.txtRemision.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "Remision:"
        '
        'txtSupervisor
        '
        Me.txtSupervisor.Location = New System.Drawing.Point(398, 15)
        Me.txtSupervisor.Name = "txtSupervisor"
        Me.txtSupervisor.Size = New System.Drawing.Size(277, 20)
        Me.txtSupervisor.TabIndex = 100
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(332, 18)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(60, 13)
        Me.Label52.TabIndex = 99
        Me.Label52.Text = "Supervisor:"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(583, 77)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(122, 13)
        Me.Label62.TabIndex = 114
        Me.Label62.Text = "DATOS DE SOLICITUD"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(18, 77)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(92, 13)
        Me.Label55.TabIndex = 110
        Me.Label55.Text = "DATOS CLIENTE"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(18, 0)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(97, 13)
        Me.Label54.TabIndex = 109
        Me.Label54.Text = "DATOS FACTURA"
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(-3, -32)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(1338, 42)
        Me.pmasterDeco.TabIndex = 107
        Me.pmasterDeco.Text = "__________________________________________________________________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(18, 133)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(108, 13)
        Me.Label56.TabIndex = 113
        Me.Label56.Text = "DETALLE FACTURA"
        '
        'ppagoDeco
        '
        Me.ppagoDeco.AutoSize = True
        Me.ppagoDeco.BackColor = System.Drawing.Color.Transparent
        Me.ppagoDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ppagoDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.ppagoDeco.Location = New System.Drawing.Point(-3, 101)
        Me.ppagoDeco.Name = "ppagoDeco"
        Me.ppagoDeco.Size = New System.Drawing.Size(1338, 42)
        Me.ppagoDeco.TabIndex = 112
        Me.ppagoDeco.Text = "__________________________________________________________________" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.ppagoDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(436, 212)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(119, 13)
        Me.Label63.TabIndex = 189
        Me.Label63.Text = "ARTICULOS DETALLE"
        '
        'pdetalleDeco
        '
        Me.pdetalleDeco.AutoSize = True
        Me.pdetalleDeco.BackColor = System.Drawing.Color.Transparent
        Me.pdetalleDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pdetalleDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pdetalleDeco.Location = New System.Drawing.Point(-3, 46)
        Me.pdetalleDeco.Name = "pdetalleDeco"
        Me.pdetalleDeco.Size = New System.Drawing.Size(1338, 42)
        Me.pdetalleDeco.TabIndex = 108
        Me.pdetalleDeco.Text = "__________________________________________________________________" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.pdetalleDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ptotalDeco
        '
        Me.ptotalDeco.AutoSize = True
        Me.ptotalDeco.BackColor = System.Drawing.Color.Transparent
        Me.ptotalDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ptotalDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.ptotalDeco.Location = New System.Drawing.Point(416, 181)
        Me.ptotalDeco.Name = "ptotalDeco"
        Me.ptotalDeco.Size = New System.Drawing.Size(918, 42)
        Me.ptotalDeco.TabIndex = 188
        Me.ptotalDeco.Text = "_____________________________________________" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.ptotalDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmFacturacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1339, 581)
        Me.Controls.Add(Me.pMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "FrmFacturacion"
        Me.pMain.ResumeLayout(False)
        Me.pMain.PerformLayout()
        Me.TabPrincipal.ResumeLayout(False)
        Me.tabCliente.ResumeLayout(False)
        Me.tabCliente.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cbCtipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCliente3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCbarrio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCmunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCdepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCliente2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProductos.ResumeLayout(False)
        Me.tabProductos.PerformLayout()
        CType(Me.dgvProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabFinanciamiento.ResumeLayout(False)
        Me.tabFinanciamiento.PerformLayout()
        Me.tabAbono.ResumeLayout(False)
        Me.tabAbono.PerformLayout()
        Me.tabMora.ResumeLayout(False)
        Me.tabMora.PerformLayout()
        Me.tabPago.ResumeLayout(False)
        Me.tabPago.PerformLayout()
        Me.pTotal.ResumeLayout(False)
        Me.pTotal.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pPago.ResumeLayout(False)
        Me.pPago.PerformLayout()
        CType(Me.cbModalidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pDetalle.ResumeLayout(False)
        Me.pDetalle.PerformLayout()
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        CType(Me.cbCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtColector As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtVendedor As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSupervisor As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents TabPrincipal As System.Windows.Forms.TabControl
    Friend WithEvents tabCliente As System.Windows.Forms.TabPage
    Friend WithEvents tabProductos As System.Windows.Forms.TabPage
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents pdetalleDeco As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents dgvCliente2 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvProducto As System.Windows.Forms.DataGridView
    Friend WithEvents TextBox47 As System.Windows.Forms.TextBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents TextBox48 As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents ppagoDeco As System.Windows.Forms.Label
    Friend WithEvents ckbEstado As System.Windows.Forms.CheckBox
    Friend WithEvents ckbAprobacion As System.Windows.Forms.CheckBox
    Friend WithEvents ckbComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents ckbAnulado As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox43 As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents TextBox42 As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents TextBox41 As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents TextBox40 As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents TextBox34 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents TextBox35 As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TextBox36 As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents TextBox38 As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents TextBox39 As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents TextBox33 As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TextBox30 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents TextBox31 As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents TextBox32 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TextBox29 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents ckbPlazoMensual As System.Windows.Forms.CheckBox
    Friend WithEvents TextBox28 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TextBox27 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents ckbFinanciado As System.Windows.Forms.CheckBox
    Friend WithEvents txtTneto As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtTimi As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtTir As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtTiva As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtTdescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtTventa As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtTcosto As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ckbIMI As System.Windows.Forms.CheckBox
    Friend WithEvents ckbIR As System.Windows.Forms.CheckBox
    Friend WithEvents ckbIVA As System.Windows.Forms.CheckBox
    Friend WithEvents tabDimar As System.Windows.Forms.TabPage
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents ptotalDeco As System.Windows.Forms.Label
    Friend WithEvents tabFinanciamiento As System.Windows.Forms.TabPage
    Friend WithEvents tabAbono As System.Windows.Forms.TabPage
    Friend WithEvents tabMora As System.Windows.Forms.TabPage
    Friend WithEvents tabPago As System.Windows.Forms.TabPage
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents txtTasaCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaF As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtSolicitud As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtRemision As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbEstado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents cbTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lbFactura As System.Windows.Forms.Label
    Friend WithEvents cbCaja As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbModalidad As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lbSerie As System.Windows.Forms.TextBox
    Friend WithEvents pDetalle As System.Windows.Forms.Panel
    Friend WithEvents pPago As System.Windows.Forms.Panel
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents pTotal As System.Windows.Forms.Panel
    Friend WithEvents dgvCliente As System.Windows.Forms.DataGridView
    Friend WithEvents lbCremision As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpTurnoTransaccion As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbCcoincidencia As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCcorreo2 As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents txtCcorreo1 As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents txtCtelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents txtCtelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCcedula As System.Windows.Forms.TextBox
    Friend WithEvents lbCcedula As System.Windows.Forms.Label
    Friend WithEvents txtCnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents txtCtelefono3 As System.Windows.Forms.TextBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents txtCcorreo3 As System.Windows.Forms.TextBox
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents txtCdirecion3 As System.Windows.Forms.TextBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents dgvCliente3 As System.Windows.Forms.DataGridView
    Friend WithEvents cbCbarrio As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents cbCmunicipio As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents cbCdepartamento As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents dtpCfechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents txtCdireccion2 As System.Windows.Forms.TextBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents txtCdireccion1 As System.Windows.Forms.TextBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents btnCaceptar As System.Windows.Forms.Button
    Friend WithEvents btnCcancelar As System.Windows.Forms.Button
    Friend WithEvents cbCtipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtCcolector As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
