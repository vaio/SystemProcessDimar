﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
Public Class FrmFactura
    '----Print-----
    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    Dim misValue As Object = System.Reflection.Missing.Value
    '----DB--------
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Dim tabla As Integer = 0
    Dim Tab As Integer = 0
    Dim check As Boolean = True
    Dim CodBodega As Integer = 0
    '----CALCULOS
    Dim cantidad As Decimal = 0
    Dim costo As Decimal = 0
    Dim precio As Decimal = 0
    Dim iva As Decimal = 0
    Dim IR As Decimal = 0
    Dim IMI As Decimal = 0
    Dim totalImpuesto As Decimal = 0
    Dim Subtotal As Decimal = 0
    Dim totalNeto As Decimal = 0
    Dim precio_Lista As Decimal = 0
    Dim Gastos_Colocacion As Decimal = 0
    Dim Precio_Factura As Decimal = 0
    Dim MontoFinanciar As Decimal = 0
    Dim ntpagar As Decimal = 0
    Dim maxDescuento As Decimal = 0.0
    '-----------
    Dim ncueta As Integer = 0
    Dim Prima As Decimal = 0
    Dim cuota As Decimal = 0
    Dim descuento As Decimal = 0
    Dim tdescuento As Decimal = 0
    Dim Financiamiento As Decimal = 0
    Dim plazo As Integer = 0
    Dim interes As Decimal = 0
    Dim modalidad As Integer = 0
    '----------
    Dim TaPagar As Decimal = 0
    Public edicion As Integer = 0

    Private Sub FrmFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        CodFactura()
        Carga_DatosGenerales()
        cbModenda.SelectedIndex = 0
        ckbIVA.Checked = True
        cbtipo.SelectedIndex = -1
        cbModenda.SelectedIndex = -1
        cbvtipo.SelectedIndex = -1

    End Sub

    Sub CodFactura()
        txtFactura.Text = ""
        txtSerie.Text = ""
        Dim dt As DataTable = New DataTable()
        sqlstring = "select	right('00000'+ltrim( rtrim(isnull(max(cast( FACT.Id_Factura as int))+1,1) ) ),5) as 'Factura' ,isnull(max(FACT.Id_Serie),'A') as 'Serie',char(ASCII(isnull(max(FACT.Id_Serie),'A'))+1) as 'Siguiente Serie' ,(select SUC.Nombre from Configuracion.Sucursales SUC where SUC.Key_Sucursal=" & My.Settings.Sucursal & ") as 'Sucursal' from FACTURACION.Facturas as FACT"
        cmd = New SqlCommand(sqlstring, con)

        Try

            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            con.Close()
            txtFactura.Text = dt.Rows(0).Item("Factura")
            txtSerie.Text = dt.Rows(0).Item("Serie")
            txtSucursal.Text = dt.Rows(0).Item("Sucursal")

        Catch ex As Exception
            con.Close()
        End Try
    End Sub
    Sub Carga_DatosGenerales()
        Dim dt As DataTable = New DataTable()
        cbCaja.Properties.Items.Clear()
        sqlstring = "select Codigo_Caja as Caja, ISNULL((select Tasa_Oficial from Tbl_TasasCambio where Fecha=@fecha),'0.0') as 'Tasa',(select Val1 from Configuracion.Parametros where Id_Parametro=1) as 'Descuento' from Cajas where Id_Sucursal='" & My.Settings.Sucursal & "' and Activo = 1"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Text))
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                cbCaja.Properties.Items.Add(row0(0))
                txtTasaCambio.Text = String.Format("{0:n2}", row0(1))
                maxDescuento = String.Format("{0:n2}", row0(2))
                'maxDescuento = row0(2)
            Next
            con.Close()
            dt.Rows.Clear()
            cbCaja.SelectedIndex = 0
            If txtTasaCambio.Text = "0.00" Then
                FrmTasaCambio.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub txtRemision_TextChanged(sender As Object, e As EventArgs) Handles txtRemision.TextChanged
        Dim dt As DataTable = New DataTable()
        Dim codigo As String = ""
        Dim fecha As Date
        If Nuevo = 1 Then
            'txtCcodigo.Text = ""
            'txtCcedula.Text = ""

            If txtRemision.Text = "" Then Exit Sub
            sqlstring = "select * from vw_FacturaCargaInicial where Remision='" + txtRemision.Text + "' and Cliente='" + txtCnombre.Text + " " + txtCapellido.Text + "'"
            cmd = New SqlCommand(sqlstring, con)
            Try
                con.Open()
                adapter = New SqlDataAdapter(cmd)
                adapter.Fill(dt)
                For Each row0 In dt.Rows
                    txtGarantia.Text = row0("Garantia")
                    txtSolicitud.Text = row0("Solicitud")
                    cbtipo.SelectedIndex = row0("Tipo") - 1
                    cbvtipo.SelectedIndex = row0("Tipo2") - 1
                    txtSupervisor.Text = row0("Supervisor")
                    txtvehiculo.Text = row0("Vehiculo")
                    txtRuta.Text = row0("Ruta")
                    '----------
                    txtVendedor.Text = row0("Vendedor")
                    fecha = row0("Fecha")
                    If row0("Id_Cliente") <> "-" Then
                        codigo = Microsoft.VisualBasic.Right("00000000" & row0(9), 8)
                    Else
                        Try
                            Dim space1 As Integer = row0(10).indexof(" ")
                            Dim space2 As Integer = row0(10).indexof(" ", space1 + 1)
                            txtCnombre.Text = row0(10).substring(0, space2)
                            txtCapellido.Text = row0(10).substring(space2 + 1)
                        Catch ex As Exception
                            txtCnombre.Text = row0(10)
                        End Try
                    End If
                    '---------
                Next
                dt.Rows.Clear()
                con.Close()
                dtpFechaI.Value = fecha
                txtCcodigo.Text = codigo
                Carga_Detalle()
            Catch ex As Exception
                MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
                con.Close()
            End Try
        ElseIf Nuevo = 0 Then
        End If
    End Sub
    Sub Carga_Detalle()
        Dim dt As DataTable = New DataTable()
        dgvDetalle.Rows.Clear()
        sqlstring = "select distinct  SFD.Id_Remision as 'Remision', SFD.Id_Factura_Solicitud as 'Id_Solicitud', SFD.Id_Solicitud as 'Pagare', SFD.Garantia, SFD.Id_Producto as 'Codigo', PRD.Nombre_Producto as 'Producto', Cantidad, Costo, PD.Precio as 'Precio C', (PD.Precio / tc.Tasa_Oficial) as 'Precio D', PF.Precio as 'Precio Facturacion' from [Tbl_Factura_Solicitud.Detalle] SFD inner join Productos PRD on PRD.Codigo_Producto=SFD.Id_Producto inner join Tbl_Precio P on P.Id_Producto=SFD.Id_Producto inner join Tbl_Precio_Detalle PD on PD.Id_Precio=p.Id_Precio and PD.Tipo=@tipo inner join Tbl_Precio_Detalle PF on PF.Id_Precio=p.Id_Precio and PF.Tipo=3 inner join Tbl_TasasCambio TC on TC.Fecha=@fecha" & _
                    " where SFD.Id_Remision=@remision and SFD.Id_Solicitud=@solicitud "
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            With cmd.Parameters
                .AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Value))
                .AddWithValue("@remision", txtRemision.Text)
                .AddWithValue("@tipo", cbtipo.SelectedIndex + 1)
                '.AddWithValue("@cliente", txtCnombre.Text + " " + txtCapellido.Text)
                .AddWithValue("@solicitud", txtSolicitud.Text)
            End With
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows

                dgvDetalle.Rows.Add(row0("Codigo"), row0("Producto"), row0("Cantidad"), row0("Costo"), row0("Precio C"), row0("Precio D"), row0("Pagare"), row0("Garantia"), row0("Precio Facturacion"), row0("Id_Solicitud"))

            Next
            con.Close()
            dt.Rows.Clear()
            cbModenda.SelectedIndex = 0
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub Carga_Detalle_Existente()
        Dim tasa As Decimal = txtTasaCambio.Text
        Dim dt As DataTable = New DataTable()
        dgvDetalle.Rows.Clear()
        sqlstring = "select distinct Factura_Detalle.Id_Producto as 'Codigo', Productos.Nombre_Producto as 'Producto', Cantidad, Costo, Precio, Id_Pagare as 'Pagare', Garantia, Precio_Factura from Factura_Detalle inner join Productos on Productos.Codigo_Producto=Factura_Detalle.Id_Producto where Id_Factura=@factura and Id_Serie=@serie and Id_Sucursal=(select Key_Sucursal from Configuracion.Sucursales where Configuracion.Sucursales.Nombre= @sucursal)"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            With cmd.Parameters
                .AddWithValue("factura", txtFactura.Text)
                .AddWithValue("serie", txtSerie.Text)
                .AddWithValue("sucursal", txtSucursal.Text)
            End With
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                dgvDetalle.Rows.Add(row0(0), row0(1), row0(2), row0(3), String.Format("{0:n2}", row0(4)), String.Format("{0:n2}", (row0(4) * tasa)), row0(5), row0(6), row0(7))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub FrmFactura_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None
    End Sub
    Private Sub cbEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbEstado.SelectedIndexChanged
        Estado()
    End Sub
    Sub Estado()
        pMaster.Enabled = False
        Pcliente.Enabled = False
        Pdetalle.Enabled = False
        Ppago.Enabled = False
        frmPrincipal.bbiGuardar.Enabled = False
        frmPrincipal.bbiCancelar.Enabled = False
        Select Case cbEstado.SelectedIndex
            Case 0 'RESERVADA
                pMaster.Enabled = True
                frmPrincipal.bbiGuardar.Enabled = True
                frmPrincipal.bbiCancelar.Enabled = True
            Case 1 'EMITIDA
                Pcliente.Enabled = True
                If cbvtipo.SelectedIndex > 0 Then
                    Ppago.Enabled = False
                ElseIf txtCcodigo.Text <> "" Then
                    Ppago.Enabled = True
                End If
                If txtCcodigo.Text <> "" Then
                    Pdetalle.Enabled = True
                End If
                frmPrincipal.bbiGuardar.Enabled = True
                frmPrincipal.bbiCancelar.Enabled = True
            Case 2 'IMPRESA
            Case 3 'ANULADA
        End Select
        If cbEstado.SelectedIndex > 1 Then
            cbEstado.ForeColor = Color.Red
        Else
            cbEstado.ForeColor = Color.Black
        End If
    End Sub
    Sub Guardar()

        Select Case cbEstado.SelectedIndex + 1
            Case 1 'guardado inicial
                cbEstado.SelectedIndex = cbEstado.SelectedIndex + 1
                edicion = 1
                Try
                    con.Open()
                    Dim cmd As New SqlCommand("SP_Factura", con)
                    cmd.CommandType = CommandType.StoredProcedure
                    With cmd.Parameters
                        .AddWithValue("@edicion", edicion)

                        .AddWithValue("@sucursal", My.Settings.Sucursal)
                        .AddWithValue("@serie", txtSerie.Text)
                        .AddWithValue("@factura", txtFactura.Text)
                        .AddWithValue("@remision", txtRemision.Text)
                        .AddWithValue("@cartera", txtCcartera.Text)
                        .AddWithValue("@tipo", cbtipo.SelectedIndex + 1)
                        .AddWithValue("@caja", cbCaja.Text)

                        If txtSolicitud.Text <> "" Then
                            .AddWithValue("@solicitud", txtSolicitud.Text)
                        End If

                        .AddWithValue("@estado", cbEstado.SelectedIndex + 1)
                        .AddWithValue("@usuario", frmPrincipal.LblNombreUsuario.Text)
                        .AddWithValue("@vendedor", txtVendedor.Text)

                        If cbModenda.SelectedIndex = 0 Then
                            .AddWithValue("@Cordoba", 1)
                        Else
                            .AddWithValue("@Cordoba", 0)
                        End If

                        .AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Text))

                        If txtCcedula.Text <> "" Then
                            .AddWithValue("@ccliente", txtCcodigo.Text)
                        End If

                        .AddWithValue("@ncliente", txtCnombre.Text + " " + txtCapellido.Text)

                        If txtObservacion.Text <> "" Then
                            .AddWithValue("@observacion", txtObservacion.Text)
                        End If

                        '.AddWithValue("@barrio", 2)
                        If luBarrio.EditValue <> "" Then .AddWithValue("@barrio", luBarrio.EditValue)

                        If txtcdireccion.Text <> "" Then
                            .AddWithValue("@direccion", txtcdireccion.Text)
                        End If

                        .AddWithValue("@tcambio", txtTasaCambio.Text)

                    End With
                    cmd.ExecuteReader()
                    con.Close()
                    'Guardar_Detalle()
                    MsgBox("Factura Guardada")
                Catch ex As Exception
                    MsgBox("Error: " + ex.Message)
                    con.Close()
                End Try
            Case 2 To 3  'Actualizar
                If txtCcodigo.Text <> "" Then
                    edicion = 3
                    Try
                        con.Open()
                        Dim cmd As New SqlCommand("SP_Factura", con)
                        cmd.CommandType = CommandType.StoredProcedure
                        With cmd.Parameters
                            .AddWithValue("@edicion", edicion)
                            .AddWithValue("@sucursal", My.Settings.Sucursal)
                            .AddWithValue("@codigo", txtFactura.Text)
                            .AddWithValue("@remision", txtRemision.Text)
                            .AddWithValue("@serie", txtSerie.Text)
                            .AddWithValue("@tipo", cbtipo.SelectedIndex + 1)
                            .AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Text))
                            .AddWithValue("@ncliente", txtCnombre.Text + " " + txtCapellido.Text)
                            .AddWithValue("@caja", cbCaja.Text)
                            .AddWithValue("@supervisor", txtSupervisor.Text)
                            .AddWithValue("@vendedor", txtVendedor.Text)
                            .AddWithValue("@estado", cbEstado.SelectedIndex + 1)
                            .AddWithValue("@tcambio", txtTasaCambio.Text)
                            .AddWithValue("@usuario", frmPrincipal.LblNombreUsuario.Text)
                            '------
                            If txtSolicitud.Text <> "" Then
                                .AddWithValue("@solicitud", txtSolicitud.Text)
                            End If
                            If txtCcedula.Text <> "" Then
                                .AddWithValue("@ccliente", txtCcodigo.Text)
                            End If
                            If txtCcartera.Text <> "" And Not txtCcartera.Text = "CUENTA NUEVA" Then
                                .AddWithValue("@colector", txtCcartera.Text)
                            End If
                            If txtObservacion.Text <> "" Then
                                .AddWithValue("@observacion", txtObservacion.Text)
                            End If
                            If txtcdireccion.Text <> "" Then
                                .AddWithValue("@direccion", txtcdireccion.Text)
                            End If

                            .AddWithValue("@iva", ckbIVA.Checked)
                            .AddWithValue("@ir", ckbIR.Checked)
                            .AddWithValue("@imi", ckbIMI.Checked)
                            .AddWithValue("@tcosto", txtTcosto.Text)
                            .AddWithValue("@subtotal", txtSubtotal.Text)
                            .AddWithValue("@tiva", txtIVA.Text)
                            .AddWithValue("@tir", txtIR.Text)
                            .AddWithValue("@timi", txtIMI.Text)
                            .AddWithValue("@tneto", txtTneto.Text)
                            .AddWithValue("@aPagar", txtTapagar.Text)
                            .AddWithValue("@colocacion", txtTcolocacion.Text)
                            .AddWithValue("@descuento", txtDescuento.Text)
                            .AddWithValue("@tdescuento", txtTdescuento.Text)
                            .AddWithValue("@precioDetalle", txtTprecio.Text)
                            .AddWithValue("@netoapagar", txtntpagar.Text)
                            If cbvtipo.SelectedIndex = 0 Then
                                .AddWithValue("@financiado", 1)
                                .AddWithValue("@tfinanciado", txtFinancimiento.Text)
                            Else
                                .AddWithValue("@tfinanciado", txtFinancimiento.Text)
                            End If
                            .AddWithValue("@interes", txtInteres.Text)
                        End With
                        cmd.ExecuteReader()
                        con.Close()
                        MsgBox("Factura Actualizada")
                    Catch ex As Exception
                        MsgBox("Error: " + ex.Message)
                        con.Close()
                    End Try
                Else
                    MsgBox("Ingrese el cliente")
                End If
        End Select
        Estado()
    End Sub
    Sub Guardar_Detalle()
        edicion = 2
        For i = 0 To dgvDetalle.Rows.Count - 1
            Try
                con.Open()
                Dim cmd As New SqlCommand("SP_Factura", con)
                cmd.CommandType = CommandType.StoredProcedure
                With cmd.Parameters
                    .AddWithValue("@edicion", edicion)
                    .AddWithValue("@sucursal", My.Settings.Sucursal)
                    .AddWithValue("@codigo", txtFactura.Text)
                    .AddWithValue("@serie", txtSerie.Text)
                    .AddWithValue("@remision", txtRemision.Text)
                    .AddWithValue("@tipo", cbtipo.SelectedIndex + 1)
                    .AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Text))
                    .AddWithValue("@ncliente", txtCnombre.Text + " " + txtCapellido.Text)
                    .AddWithValue("@caja", cbCaja.Text)
                    .AddWithValue("@supervisor", txtSupervisor.Text)
                    .AddWithValue("@vendedor", txtVendedor.Text)
                    .AddWithValue("@estado", cbEstado.SelectedIndex + 1)
                    .AddWithValue("@usuario", frmPrincipal.LblNombreUsuario.Text)

                    .AddWithValue("@producto", dgvDetalle.Rows(i).Cells(0).Value)
                    .AddWithValue("@cantidad", dgvDetalle.Rows(i).Cells(2).Value)
                    .AddWithValue("@costo", dgvDetalle.Rows(i).Cells(3).Value)
                    .AddWithValue("@precio", dgvDetalle.Rows(i).Cells(4).Value)
                    .AddWithValue("@pf", dgvDetalle.Rows(i).Cells(8).Value)
                    If dgvDetalle.Rows(i).Cells(6).Value <> "" Then
                        .AddWithValue("@pagare", dgvDetalle.Rows(i).Cells(6).Value)
                    End If
                    .AddWithValue("@garantia", dgvDetalle.Rows(i).Cells(7).Value)
                    .AddWithValue("@solicitud1", dgvDetalle.Rows(i).Cells(9).Value)
                End With
                cmd.ExecuteReader()
                con.Close()
            Catch ex As Exception
                con.Close()
                MsgBox("Error: " + ex.Message)
            End Try
        Next
    End Sub
    Sub Cargar(ByVal factura As String, serie As String)
        Dim dt As DataTable = New DataTable()
        Dim fecha As String = ""
        Dim codigo As String = ""
        Dim financiado As Boolean = False
        cbtipo.SelectedIndex = -1
        txtCcodigo.Text = ""
        sqlstring = "select * from vw_FacturaCargaF where Factura='" + factura + "' and Serie='" + serie + "'"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtSucursal.Text = row0("Sucursal")
                txtFactura.Text = row0("Factura")
                txtSerie.Text = row0("Serie")
                cbvtipo.SelectedIndex = row0("Tipo") '- 1
                txtRemision.Text = row0("Remision")
                fecha = row0("Fecha")
                cbCaja.SelectedItem = row0("Caja")
                '---
                txtTasaCambio.Text = row0("Tasa de Cambio")
                '---
                txtSupervisor.Text = row0("Supervisor")
                txtvehiculo.Text = row0("Vehiculo")
                txtRuta.Text = row0("Ruta")
                cbEstado.SelectedIndex = row0("Estado") - 1
                txtVendedor.Text = row0("Vendedor")
                If row0("Codigo Cliente") <> "0" Then
                    codigo = row0("Codigo Cliente")
                Else
                    Try
                        Dim space1 As Integer = row0("Cliente").indexof(" ")
                        Dim space2 As Integer = row0("Cliente").indexof(" ", space1 + 1)
                        txtCnombre.Text = row0("Cliente").substring(0, space2)
                        txtCapellido.Text = row0("Cliente").substring(space2 + 1)
                    Catch ex As Exception
                        txtCnombre.Text = row0("Cliente")
                        txtCapellido.Text = ""
                    End Try
                    txtCcedula.Text = ""
                    txtCcodigo.Text = ""
                End If
                If row0("Colector") <> "0" Then
                    txtCcartera.Text = row0("Colector")
                Else
                    txtCcartera.Text = ""
                End If
                If row0("Observacion") <> "0" Then
                    txtObservacion.Text = row0("Observacion")
                Else
                    txtObservacion.Text = ""
                End If
                If row0("Direccion") <> "" Then
                    txtcdireccion.Text = row0("Direccion")
                Else
                    txtcdireccion.Text = ""
                End If
                If row0("Contabilizada") <> "0" Then
                    lbEndosado.Visible = True
                Else
                    lbEndosado.Visible = False
                End If
                If row0("Verificado") <> "0" Then
                    lbVerificado.Visible = True
                Else
                    lbVerificado.Visible = False
                End If
                'txtSolicitud.Text = row0("Solicitud")
                If row0("Solicitud") <> "0" Then txtSolicitud.Text = row0("Solicitud")
                If row0("Financiada") <> "0" Then
                    financiado = True
                    'pendiente plan de pago
                End If
                If row0("Fecha vencimiento") <> "0" Then
                    'pendiente fecha final
                End If
                If row0("Impuesto") <> "0" Then
                    ckbIVA.Checked = True
                Else
                    ckbIVA.Checked = False
                End If
                If row0("Expr1") <> "0" Then
                    ckbIR.Checked = True
                Else
                    ckbIR.Checked = False
                End If
                If row0("Expr2") <> "0" Then
                    ckbIMI.Checked = True
                Else
                    ckbIMI.Checked = False
                End If
                txtDescuento.Text = row0("Descuento")
                txtInteres.Text = row0("Interes")
                If row0("Cordoba") = True Then
                    cbModenda.SelectedIndex = 0
                Else
                    cbModenda.SelectedIndex = 1
                End If


            Next
            dt.Rows.Clear()
            con.Close()
            txtCcodigo.Text = codigo
            dtpFechaI.Value = fecha
            Carga_Detalle_Existente()
        Catch ex As Exception
            MsgBox("Error al cargar datos de factura existente." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub rbMonedaCheckedChanged(sender As Object, e As EventArgs) Handles cbModenda.SelectedIndexChanged
        If cbModenda.SelectedIndex = 0 Then
            dgvDetalle.Columns("ColumnCordoba").Visible = True
            dgvDetalle.Columns("ColumnDolar").Visible = False
        Else
            dgvDetalle.Columns("ColumnCordoba").Visible = False
            dgvDetalle.Columns("ColumnDolar").Visible = True
        End If
        Factura_Calculo()
    End Sub
    Private Sub dgvDetalle_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvDetalle.RowsAdded
        Factura_Calculo()
    End Sub
    Sub Factura_Calculo()
        If txtRemision.Text <> "" And dgvDetalle.Rows.Count > 0 Then
            Try
                costo = 0
                Subtotal = 0
                iva = 0
                totalNeto = 0
                IR = 0
                IMI = 0
                TaPagar = 0
                Gastos_Colocacion = 0
                Precio_Factura = 0
                precio_Lista = 0
                Financiamiento = 0
                descuento = 0
                '----
                cantidad = 0    '???
                '-----
                For i = 0 To dgvDetalle.RowCount - 1
                    cantidad = dgvDetalle.Rows(i).Cells(2).Value
                    costo += dgvDetalle.Rows(i).Cells(3).Value
                    If cbModenda.SelectedIndex = 0 Then
                        precio_Lista += dgvDetalle.Rows(i).Cells(4).Value
                    Else
                        precio_Lista += dgvDetalle.Rows(i).Cells(5).Value
                    End If
                    Precio_Factura += dgvDetalle.Rows(i).Cells(8).Value
                Next
                descuento = txtDescuento.Text
                If descuento > maxDescuento Then
                    MsgBox("El maximo porcentaje de descuento es: " & maxDescuento & "%")
                    txtDescuento.Text = maxDescuento
                    txtDescuento.SelectAll()
                End If
                tdescuento = (precio_Lista * descuento) / 100
                precio_Lista -= tdescuento
                Subtotal = Precio_Factura
                If ckbIVA.Checked = True Then
                    iva = Precio_Factura * 0.15
                    totalNeto = iva + Subtotal
                Else
                    totalNeto = Subtotal
                End If
                TaPagar = totalNeto
                Gastos_Colocacion = (precio_Lista - Precio_Factura) - iva
                'Gastos_Colocacion -= tdescuento
                If ckbIR.Checked = True Then
                    IR = Precio_Factura * 0.02
                    TaPagar -= IR
                    Gastos_Colocacion += IR
                End If
                If ckbIMI.Checked = True Then
                    IMI = Precio_Factura * 0.01
                    TaPagar -= IMI
                    Gastos_Colocacion += IMI
                End If
                interes = txtInteres.Text
                plazo = txtPlazo.Text
                Prima = txtPrima.Text
                ntpagar = TaPagar
                If Ppago.Enabled = True And plazo > 0 Then
                    interes = txtInteres.Text
                    plazo = txtPlazo.Text
                    Prima = txtPrima.Text
                    MontoFinanciar = ntpagar - Prima
                    Financiamiento = (MontoFinanciar * (interes * plazo)) / 100
                    ntpagar += MontoFinanciar + Financiamiento
                    cuota = ntpagar / plazo
                    Plan_Pago()
                End If
            Catch ex As Exception
            End Try
            txtTcosto.Text = String.Format("{0:n2}", costo)
            txtSubtotal.Text = String.Format("{0:n2}", Subtotal)
            txtTneto.Text = String.Format("{0:n2}", totalNeto)
            txtTapagar.Text = String.Format("{0:n2}", TaPagar)
            txtTcolocacion.Text = String.Format("{0:n2}", Gastos_Colocacion)
            txtTprecio.Text = String.Format("{0:n2}", precio_Lista)
            txtTdescuento.Text = String.Format("{0:n2}", tdescuento)
            txtMontoFinanciar.Text = String.Format("{0:n2}", MontoFinanciar)
            txtFinancimiento.Text = String.Format("{0:n2}", Financiamiento)
            txtCuota.Text = String.Format("{0:n2}", cuota)
            txtIVA.Text = String.Format("{0:n2}", iva)
            txtIR.Text = String.Format("{0:n2}", IR)
            txtIMI.Text = String.Format("{0:n2}", IMI)
            txtntpagar.Text = String.Format("{0:n2}", ntpagar)
        End If
    End Sub
    Sub Plan_Pago()
        dgvPago.Rows.Clear()
        Select Case cbModalidad.SelectedIndex
            Case 0 'semanal
                ncueta = 4 * plazo
            Case 1 'quincenal
                ncueta = 2 * plazo
            Case 2 'mensual
                ncueta = 1 * plazo
        End Select
        Try
            Dim fechaP As Date = dtpPrima.Value
            Dim fechaC As Date = dtpCuota.Value
            Dim fechaC2 As Date = dtpCuota2.Value
            Dim pcuota As Decimal = 0
            Dim pprincipal As Decimal = 0
            Dim pinteres As Decimal = 0
            Dim ppendiente As Decimal = ntpagar
            Dim bandera As Boolean = False
            For i = 0 To ncueta
                pprincipal = MontoFinanciar / ncueta
                pinteres = Financiamiento / ncueta
                pcuota = pprincipal + pinteres
                If Prima > 0 And i = 0 Then
                    ppendiente -= Prima
                    dgvPago.Rows.Add(i, String.Format("{0:dd/MM/yy}", fechaP), String.Format("{0:n2}", Prima), 0, 0, String.Format("{0:n2}", ppendiente))
                Else
                    ppendiente -= pcuota
                    Select Case cbModalidad.SelectedIndex
                        Case 0 'semanal
                            dgvPago.Rows.Add(i, String.Format("{0:dd/MM/yy}", fechaC), String.Format("{0:n2}", pcuota), String.Format("{0:n2}", pprincipal), String.Format("{0:n2}", pinteres), String.Format("{0:n2}", ppendiente))
                            fechaC = fechaC.AddDays(7)
                        Case 1 'quincenal
                            If bandera = False Then
                                dgvPago.Rows.Add(i, String.Format("{0:dd/MM/yy}", fechaC), String.Format("{0:n2}", pcuota), String.Format("{0:n2}", pprincipal), String.Format("{0:n2}", pinteres), String.Format("{0:n2}", ppendiente))
                                bandera = True
                            Else
                                dgvPago.Rows.Add(i, String.Format("{0:dd/MM/yy}", fechaC2), String.Format("{0:n2}", pcuota), String.Format("{0:n2}", pprincipal), String.Format("{0:n2}", pinteres), String.Format("{0:n2}", ppendiente))
                                bandera = False
                            End If
                        Case 2 'mensual
                            dgvPago.Rows.Add(i, String.Format("{0:dd/MM/yy}", fechaC), String.Format("{0:n2}", pcuota), String.Format("{0:n2}", pprincipal), String.Format("{0:n2}", pinteres), String.Format("{0:n2}", ppendiente))
                            fechaC = fechaC.AddMonths(1)
                    End Select
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Paneles_Enable_Changed(sender As Object, e As EventArgs) Handles Ppago.EnabledChanged, pMaster.EnabledChanged, Pcliente.EnabledChanged, Pdetalle.EnabledChanged
        dgvDetalle.ClearSelection()
        If pMaster.Enabled = True Then
            pmasterDeco.ForeColor = Color.DodgerBlue
            frmPrincipal.bbiGuardar.Enabled = True
            lbObservacion.Visible = True
        Else
            pmasterDeco.ForeColor = SystemColors.AppWorkspace
            frmPrincipal.bbiGuardar.Enabled = False
            lbObservacion.Visible = False
        End If
        If Pcliente.Enabled = True Then
            pClienteDeco.ForeColor = Color.DodgerBlue
        Else
            pClienteDeco.ForeColor = SystemColors.AppWorkspace
        End If
        If Pdetalle.Enabled = True Then
            PdetalleDeco.ForeColor = Color.DodgerBlue
            Label35.ForeColor = Color.DodgerBlue
        Else
            PdetalleDeco.ForeColor = SystemColors.AppWorkspace
            Label35.ForeColor = SystemColors.AppWorkspace
        End If
        If Ppago.Enabled = True Then
            pPagoDeco.ForeColor = Color.DodgerBlue
        Else
            pPagoDeco.ForeColor = SystemColors.AppWorkspace
        End If
    End Sub
    Private Sub dtpFechaI_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaI.ValueChanged
        Dim dt As DataTable = New DataTable()
        sqlstring = "select Tasa_Oficial from Tbl_TasasCambio where Fecha=@fecha"
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(dtpFechaI.Text))
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtTasaCambio.Text = row0(0)
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub txtCnombre_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles txtCnombre.MouseDoubleClick, txtCcodigo.MouseDoubleClick, txtCcedula.MouseDoubleClick, txtCapellido.MouseDoubleClick
        txtCnombre.SelectionStart = txtCnombre.Text.Length

        With frmEditCliente
            If txtCcodigo.Text <> "" Then
                .edicion = 2
                .txtcodigo.Text = txtCcodigo.Text
            Else
                .edicion = 1
                .txtnombre.Text = txtCnombre.Text
                .txtapellido.Text = txtCapellido.Text
                .ckbActivo.Checked = True
            End If
            frmEditCliente.ShowDialog()
        End With
    End Sub
    Private Sub txtCcodigo_TextChanged(sender As Object, e As EventArgs) Handles txtCcodigo.TextChanged
        Dim dt As DataTable = New DataTable()
        txtCnombre.Text = ""
        txtCcedula.Text = ""
        txtCapellido.Text = ""
        sqlstring = "select Cedula, Nombre, Apellido, Telefono1, Id_Tipo, Direccion1 from Clientes where Codigo=@codigo"
        con.Close()
        Try
            con.Open()
            cmd = New SqlCommand(sqlstring, con)
            With cmd.Parameters
                .AddWithValue("@codigo", txtCcodigo.Text)
            End With
            cmd.ExecuteNonQuery()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                txtCcedula.Text = row0("Cedula")
                txtCnombre.Text = row0("Nombre")
                txtCapellido.Text = row0("Apellido")
                txtctelefono.Text = row0("Telefono1")
                txtcdireccion.Text = row0("Direccion1")
                cbtipo.SelectedIndex = row0("Id_Tipo") - 1
            Next
            dt.Rows.Clear()
            con.Close()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
        End Try
        con.Close()
    End Sub
    Private Sub cbvtipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbvtipo.SelectedIndexChanged
        If cbvtipo.SelectedIndex > 0 Then
            Ppago.Enabled = False
            Ppago.Visible = False
        Else
            Ppago.Enabled = True
            Ppago.Visible = True
        End If
    End Sub

    Private Sub txtInteres_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrima.KeyPress, txtInteres.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not (e.KeyChar = ".")
    End Sub

    Private Sub txtFactura_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtFactura.KeyPress
        e.Handled = Not IsNumeric(e.KeyChar) And Not Char.IsControl(e.KeyChar)
    End Sub
    Private Sub txtPrima_TextChanged(sender As Object, e As EventArgs) Handles txtPrima.TextChanged, txtPlazo.TextChanged, txtInteres.TextChanged, dtpPrima.ValueChanged, dtpCuota.ValueChanged, txtDescuento.TextChanged, cbModalidad.TextChanged, ckbIVA.CheckedChanged, ckbIR.CheckedChanged, ckbIMI.CheckedChanged
        Dim FechaI As Date = dtpFechaI.Value
        Dim FechaP As Date = dtpPrima.Value
        Dim FechaC As Date = dtpCuota.Value
        If txtDescuento.Text = "" Then txtDescuento.Text = 0

        If cbModalidad.SelectedIndex = 1 Then
            dtpCuota2.Enabled = True
        Else
            dtpCuota2.Enabled = False
        End If
        If FechaP > (FechaI.AddDays(16)) Then
            MsgBox("La fecha de PRIMA no puede ser 15 dias despues de la fecha de FACTURACION")
            dtpPrima.Value = dtpFechaI.Value
        ElseIf FechaC > (FechaI.AddDays(31)) Then
            MsgBox("La fecha de CUOTA no puede ser 30 dias despues de la fecha de FACTURACION")
            dtpCuota.Value = dtpFechaI.Value
        Else
            Factura_Calculo()
        End If
    End Sub

    Private Sub pMain_Resize(sender As Object, e As EventArgs) Handles pMain.Resize
        pMain.VerticalScroll.Value = 0
    End Sub
    Private Sub txtObservacion_TextChanged(sender As Object, e As EventArgs) Handles txtObservacion.TextChanged
        If txtObservacion.Text.Length > 0 Then
            lbObservacion.Visible = False
        Else
            lbObservacion.Visible = True
        End If
    End Sub
    Sub Print1()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Remision.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Remision, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Remision.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Facturacion")
        xlWorkSheet.Cells(5, 2) = txtCnombre.Text + " " + txtCapellido.Text
        xlWorkSheet.Cells(6, 2) = txtcdireccion.Text
        If cbvtipo.SelectedIndex = 0 Then
            xlWorkSheet.Cells(7, 5) = "X" 'Credito
            xlWorkSheet.Cells(7, 3) = ""
        Else
            xlWorkSheet.Cells(7, 3) = "X" 'Credito/Contado
            xlWorkSheet.Cells(7, 5) = ""
        End If
        xlWorkSheet.Cells(5, 8) = dtpFechaI.Value.Day
        xlWorkSheet.Cells(5, 9) = dtpFechaI.Value.Month
        xlWorkSheet.Cells(5, 10) = dtpFechaI.Value.Year
        xlWorkSheet.Cells(6, 9) = txtctelefono.text
        If txtPlazo.Text = "" Or txtPlazo.Text = 0 Then
            xlWorkSheet.Cells(7, 7) = "--"
        Else
            xlWorkSheet.Cells(7, 7) = txtPlazo.Text
        End If
        xlWorkSheet.Cells(14, 10) = txtSubtotal.Text
        xlWorkSheet.Cells(15, 10) = txtIVA.Text
        If txtPrima.Text <> "" And Not txtPrima.Text = 0 Then
            xlWorkSheet.Cells(16, 10) = txtPrima.Text
        Else
            xlWorkSheet.Cells(16, 10) = "--"
        End If
        If txtFinancimiento.Text <> "" And Not txtFinancimiento.Text = 0 Then
            xlWorkSheet.Cells(17, 10) = txtFinancimiento.Text
        Else
            xlWorkSheet.Cells(17, 10) = "--"
        End If
        xlWorkSheet.Cells(18, 10) = txtntpagar.Text

        Dim i As Integer = 1
        Dim f As Integer = 9
        Do While f <= 13
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            f = f + 1
        Loop

        f = 9
        Do While i <= dgvDetalle.Rows.Count
            xlWorkSheet.Cells(f, 1) = dgvDetalle.Rows((i - 1)).Cells(2).Value
            xlWorkSheet.Cells(f, 2) = dgvDetalle.Rows((i - 1)).Cells(0).Value
            xlWorkSheet.Cells(f, 3) = dgvDetalle.Rows((i - 1)).Cells(1).Value
            xlWorkSheet.Cells(f, 9) = dgvDetalle.Rows((i - 1)).Cells(8).Value
            'Select Case cbModenda.SelectedIndex
            '    Case 0 'cordoba
            '        xlWorkSheet.Cells(f, 9) = dgvDetalle.Rows((i - 1)).Cells(4).Value
            '    Case 1  'dolar
            '        xlWorkSheet.Cells(f, 9) = dgvDetalle.Rows((i - 1)).Cells(5).Value
            'End Select
            xlWorkSheet.Cells(f, 10) = (dgvDetalle.Rows((i - 1)).Cells(2).Value) * (dgvDetalle.Rows((i - 1)).Cells(8).Value)

            i = i + 1
            f = f + 1
            If f < 14 Then
                xlWorkSheet.Cells(f, 3) = "======ULTIMA LINA======="
            End If
        Loop
        xlWorkBook.Application.DisplayAlerts = False
        Try
            xlWorkBook.Save()
        Catch ex As Exception
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End Try
        '-----------------PRINT EXCEL--------------------------------
        xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        cbEstado.SelectedIndex = 2
        Guardar()
        '------------------------------------------------------------
        xlWorkBook.Close()
        xlApp.Quit()
        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)
    End Sub
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
        For Each Process As Process In xlp
            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
                Process.Kill()
                Exit For
            End If
        Next
    End Sub

    Private Sub txtDescuento_MouseClick(sender As Object, e As MouseEventArgs) Handles txtDescuento.MouseClick
        txtDescuento.SelectAll()
    End Sub
End Class