﻿Imports System.Data.SqlClient
Public Class FrmVerificacion_Cliente
    Dim strsql As String = ""
    Dim id_barrio As Integer
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim adapter As SqlDataAdapter
    Public edition As Integer
    Private Sub FrmVerificacion_Cliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Height = 312
        cargar_ruta()
        If nTipoEdic = 2 Then
            edition = 1
            cargaEdit(CodigoEntidad)
        End If
    End Sub
    Sub cargaEdit(ByVal codigo As String)
        Try
            strsql = "select Id_Cliente, Nombre, Apellido, Cedula, Tbl_TransUnion.Id_Ruta, Observacion, Fecha, Estado, Dimar, Inficsa, Tbl_TransUnion.Id_Barrio, Tbl_Barrios.Nombre_Barrio  from Tbl_TransUnion LEFT join Almacenes on Almacenes.Codigo_Bodega=Tbl_TransUnion.Id_Ruta left join Tbl_Barrios on Tbl_Barrios.Id_Barrio=Tbl_TransUnion.Id_Barrio where Id_Cliente='" + codigo + "'"
            Dim tblDatos As DataTable = SQL(strsql, "tblDatosProdServ", My.Settings.SolIndustrialCNX).Tables(0)
            If tblDatos.Rows.Count <> 0 Then
                txtcodigo.Text = tblDatos.Rows(0).Item("Id_Cliente")
                txtNombre.Text = tblDatos.Rows(0).Item("Nombre")
                txtApellido.Text = tblDatos.Rows(0).Item("Apellido")
                txtCedula.Text = tblDatos.Rows(0).Item("Cedula")
                luRuta.EditValue = tblDatos.Rows(0).Item("Id_Ruta")
                '------
                txtObservacion.Text = tblDatos.Rows(0).Item("Observacion")
                dtpFecha.Text = tblDatos.Rows(0).Item("Fecha")
                cbEstado.SelectedIndex = tblDatos.Rows(0).Item("Estado")
                ckDimar.Checked = tblDatos.Rows(0).Item("Dimar")
                ckInficsa.Checked = tblDatos.Rows(0).Item("Inficsa")
                id_barrio = tblDatos.Rows(0).Item("Id_Barrio")
                txtBarrio.Text = tblDatos.Rows(0).Item("Nombre_Barrio")
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub cargar_ruta()
        tblRuta.DataSource = SQL("select Nombre_Bodega as Nombre, Codigo_Bodega as Codigo from Almacenes where Nombre_Bodega LIKE 'RUTA%'", "tblRuta", My.Settings.SolIndustrialCNX).Tables(0)
        luRuta.Properties.DataSource = tblRuta
        luRuta.Properties.DisplayMember = "Nombre"
        luRuta.Properties.ValueMember = "Codigo"
        luRuta.Properties.PopulateColumns()
        luRuta.Properties.Columns("Codigo").Visible = False
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim CatalogoProv As New clsCatalogos(My.Settings.SolIndustrialCNX)
        Dim Resum As String = ""
        Resum = CatalogoProv.EditarTransUnion(edition, txtcodigo.Text, txtNombre.Text, txtApellido.Text, txtCedula.Text, luRuta.EditValue, txtObservacion.Text, dtpFecha.Text, cbEstado.SelectedIndex, ckDimar.Checked, ckInficsa.Checked, id_barrio)
        If Resum = "OK" Then
            frmPrincipal.CargarDatos()
            Me.Close()
        End If
    End Sub
    Sub Borrar()
        edition = 2
        Dim CatalogoProv As New clsCatalogos(My.Settings.SolIndustrialCNX)
        Dim Resum As String = ""
        Resum = CatalogoProv.EditarTransUnion(edition, CodigoEntidad, "", "", "", 0, "", Today, 0, 1, 1, 1)
        If Resum = "OK" Then
            frmPrincipal.CargarDatos()
            Me.Close()
        End If
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
    Private Sub txtCedula_TextChanged(sender As Object, e As EventArgs) Handles txtCedula.TextChanged
        If txtCedula.TextLength > 13 Then
            GridControl1.DataSource = vbNull
            GridView1.Columns.Clear()
            strsql = "SELECT * FROM vw_transunion where Cedula='" + txtCedula.Text + "'"
            Dim tabledatos As DataTable = SQL(strsql, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
            GridControl1.DataSource = tabledatos

            GridView1.BestFitColumns()

            If GridView1.RowCount > 0 Then
                Me.Height = 606
                lbGrid.Visible = True
                pGrid.Visible = True
                GridControl1.Visible = True
            Else
                GridControl1.DataSource = vbNull
                GridView1.Columns.Clear()
                strsql = "select Codigo, Cedula, Nombre, Apellido, Tipo, Clasificacion from vwVerClientes where Cedula='" + txtCedula.Text + "'"
                tabledatos = SQL(strsql, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
                GridControl1.DataSource = tabledatos
                GridView1.BestFitColumns()
                If GridView1.RowCount > 0 Then

                    Me.Height = 606
                    lbGrid.Visible = True
                    pGrid.Visible = True
                    GridControl1.Visible = True
                Else
                    Me.Height = 312
                    lbGrid.Visible = False
                    pGrid.Visible = False
                    GridControl1.Visible = False

                End If
            End If
        End If
    End Sub
    Private Sub GridControl1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles GridControl1.MouseDoubleClick
        Try
            txtNombre.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Nombre").ToString()
            txtApellido.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Apellido").ToString()
            txtObservacion.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Tipo").ToString
        Catch ex As Exception
        End Try

        Try
            txtBarrio.Text = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Barrio").ToString
            id_barrio = GridView1.GetRowCellValue(GridView1.GetSelectedRows(0), "Id_Barrio").ToString
        Catch ex As Exception
        End Try

        Me.Height = 312
        lbGrid.Visible = False
        pGrid.Visible = False
        GridControl1.Visible = False
    End Sub

    Private Sub txtBarrio_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles txtBarrio.MouseDoubleClick

        If lbGrid.Visible = False Then
            GridControl1.DataSource = vbNull
            GridView1.Columns.Clear()
            strsql = "select Tbl_Barrios.Id_Barrio,Tbl_Barrios.Nombre_Barrio as 'Barrio', Tbl_Zonas_Distritos.Nombre_Zona_Distrito as 'Distrito', Tbl_Municipio.Nombre_Municipio as 'Municipio',catalogo.Departamento.Nombre as 'Departamento' from Catalogo.Departamento right join Tbl_Municipio on Tbl_Municipio.Id_Departamento=Catalogo.Departamento.Id_Departamento  right join Tbl_Zonas_Distritos on Tbl_Zonas_Distritos.Id_Municipio=Tbl_Municipio.Id_Municipio right join Tbl_Barrios on Tbl_Barrios.Id_Zona_Distrito=Tbl_Zonas_Distritos.Id_Zona_Distrito"
            Dim tabledatos As DataTable = SQL(strsql, "tbldocumentos", My.Settings.SolIndustrialCNX).Tables(0)
            GridControl1.DataSource = tabledatos
            GridView1.Columns("Id_Barrio").Visible = False
            GridView1.BestFitColumns()

            Me.Height = 606
            lbGrid.Visible = True
            pGrid.Visible = True
            GridControl1.Visible = True

        Else
            Me.Height = 312
            lbGrid.Visible = False
            pGrid.Visible = False
            GridControl1.Visible = False

        End If
        

    End Sub
End Class
