﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEndoseFactura
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEndoseFactura))
        Me.pMain = New System.Windows.Forms.Panel()
        Me.lbCodigo = New System.Windows.Forms.Label()
        Me.lbFact = New System.Windows.Forms.Label()
        Me.lbObservacion2 = New System.Windows.Forms.Label()
        Me.txtObservacion2 = New System.Windows.Forms.TextBox()
        Me.btnMarcar = New System.Windows.Forms.Button()
        Me.pdeco = New System.Windows.Forms.Panel()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaF = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbObservacion = New System.Windows.Forms.Label()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtnFactura = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.lbMaster = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.nbgRetrive = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nbiDetalle = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiPendiente = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiEndosada = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItem1 = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiAplicar = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbgCancelar = New DevExpress.XtraNavBar.NavBarItem()
        Me.pMain.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMaster.SuspendLayout()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.lbCodigo)
        Me.pMain.Controls.Add(Me.lbFact)
        Me.pMain.Controls.Add(Me.lbObservacion2)
        Me.pMain.Controls.Add(Me.txtObservacion2)
        Me.pMain.Controls.Add(Me.btnMarcar)
        Me.pMain.Controls.Add(Me.pdeco)
        Me.pMain.Controls.Add(Me.dtpFechaI)
        Me.pMain.Controls.Add(Me.dtpFechaF)
        Me.pMain.Controls.Add(Me.Label2)
        Me.pMain.Controls.Add(Me.btnAplicar)
        Me.pMain.Controls.Add(Me.GridControl1)
        Me.pMain.Controls.Add(Me.pMaster)
        Me.pMain.Controls.Add(Me.NavBarControl1)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1234, 785)
        Me.pMain.TabIndex = 0
        '
        'lbCodigo
        '
        Me.lbCodigo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbCodigo.AutoSize = True
        Me.lbCodigo.Location = New System.Drawing.Point(905, 42)
        Me.lbCodigo.Name = "lbCodigo"
        Me.lbCodigo.Size = New System.Drawing.Size(37, 13)
        Me.lbCodigo.TabIndex = 215
        Me.lbCodigo.Text = "00000"
        Me.lbCodigo.Visible = False
        '
        'lbFact
        '
        Me.lbFact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbFact.AutoSize = True
        Me.lbFact.Location = New System.Drawing.Point(874, 42)
        Me.lbFact.Name = "lbFact"
        Me.lbFact.Size = New System.Drawing.Size(31, 13)
        Me.lbFact.TabIndex = 214
        Me.lbFact.Text = "Fact:"
        Me.lbFact.Visible = False
        '
        'lbObservacion2
        '
        Me.lbObservacion2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbObservacion2.AutoSize = True
        Me.lbObservacion2.Location = New System.Drawing.Point(874, 29)
        Me.lbObservacion2.Name = "lbObservacion2"
        Me.lbObservacion2.Size = New System.Drawing.Size(70, 13)
        Me.lbObservacion2.TabIndex = 212
        Me.lbObservacion2.Text = "Observacion:"
        Me.lbObservacion2.Visible = False
        '
        'txtObservacion2
        '
        Me.txtObservacion2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtObservacion2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion2.Location = New System.Drawing.Point(950, 26)
        Me.txtObservacion2.Multiline = True
        Me.txtObservacion2.Name = "txtObservacion2"
        Me.txtObservacion2.Size = New System.Drawing.Size(270, 46)
        Me.txtObservacion2.TabIndex = 211
        Me.txtObservacion2.Visible = False
        '
        'btnMarcar
        '
        Me.btnMarcar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMarcar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnMarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMarcar.Location = New System.Drawing.Point(1146, 26)
        Me.btnMarcar.Name = "btnMarcar"
        Me.btnMarcar.Size = New System.Drawing.Size(76, 46)
        Me.btnMarcar.TabIndex = 213
        Me.btnMarcar.Text = "Marcar Todo"
        Me.btnMarcar.UseVisualStyleBackColor = False
        '
        'pdeco
        '
        Me.pdeco.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.pdeco.Location = New System.Drawing.Point(750, 53)
        Me.pdeco.Name = "pdeco"
        Me.pdeco.Size = New System.Drawing.Size(5, 18)
        Me.pdeco.TabIndex = 205
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaI.Location = New System.Drawing.Point(663, 52)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(90, 20)
        Me.dtpFechaI.TabIndex = 2
        '
        'dtpFechaF
        '
        Me.dtpFechaF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaF.Location = New System.Drawing.Point(753, 52)
        Me.dtpFechaF.Name = "dtpFechaF"
        Me.dtpFechaF.Size = New System.Drawing.Size(90, 20)
        Me.dtpFechaF.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(617, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 203
        Me.Label2.Text = "Rango:"
        '
        'btnAplicar
        '
        Me.btnAplicar.BackColor = System.Drawing.Color.Orange
        Me.btnAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAplicar.Location = New System.Drawing.Point(9, 96)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(107, 23)
        Me.btnAplicar.TabIndex = 212
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = False
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(124, 91)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1110, 694)
        Me.GridControl1.TabIndex = 175
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1, Me.AdvBandedGridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.GroupPanelText = "Facturas"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsView.AllowHtmlDrawGroups = False
        Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.PaintStyleName = "UltraFlat"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.AdvBandedGridView1.GridControl = Me.GridControl1
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Name = "GridBand1"
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.lbEstado)
        Me.pMaster.Controls.Add(Me.dtpFecha)
        Me.pMaster.Controls.Add(Me.Label3)
        Me.pMaster.Controls.Add(Me.lbObservacion)
        Me.pMaster.Controls.Add(Me.txtObservacion)
        Me.pMaster.Controls.Add(Me.Label6)
        Me.pMaster.Controls.Add(Me.txtCodigo)
        Me.pMaster.Controls.Add(Me.Label5)
        Me.pMaster.Controls.Add(Me.txtnFactura)
        Me.pMaster.Controls.Add(Me.Label4)
        Me.pMaster.Controls.Add(Me.txtMonto)
        Me.pMaster.Controls.Add(Me.Label1)
        Me.pMaster.Controls.Add(Me.txtConcepto)
        Me.pMaster.Controls.Add(Me.lbMaster)
        Me.pMaster.Controls.Add(Me.pmasterDeco)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Top
        Me.pMaster.Enabled = False
        Me.pMaster.Location = New System.Drawing.Point(124, 0)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(1110, 91)
        Me.pMaster.TabIndex = 177
        '
        'lbEstado
        '
        Me.lbEstado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEstado.Location = New System.Drawing.Point(926, 6)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(16, 16)
        Me.lbEstado.TabIndex = 210
        Me.lbEstado.Text = "--"
        Me.lbEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpFecha
        '
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(76, 52)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(90, 20)
        Me.dtpFecha.TabIndex = 206
        Me.dtpFecha.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(30, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 207
        Me.Label3.Text = "Fecha:"
        '
        'lbObservacion
        '
        Me.lbObservacion.AutoSize = True
        Me.lbObservacion.Location = New System.Drawing.Point(656, 29)
        Me.lbObservacion.Name = "lbObservacion"
        Me.lbObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lbObservacion.TabIndex = 200
        Me.lbObservacion.Text = "Observacion:"
        '
        'txtObservacion
        '
        Me.txtObservacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Location = New System.Drawing.Point(732, 26)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(270, 46)
        Me.txtObservacion.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(30, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Codigo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCodigo.Location = New System.Drawing.Point(76, 26)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(90, 20)
        Me.txtCodigo.TabIndex = 10
        Me.txtCodigo.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(348, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "N Fact:"
        '
        'txtnFactura
        '
        Me.txtnFactura.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtnFactura.Location = New System.Drawing.Point(396, 52)
        Me.txtnFactura.Name = "txtnFactura"
        Me.txtnFactura.ReadOnly = True
        Me.txtnFactura.Size = New System.Drawing.Size(77, 20)
        Me.txtnFactura.TabIndex = 8
        Me.txtnFactura.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(208, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Monto:"
        '
        'txtMonto
        '
        Me.txtMonto.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtMonto.Location = New System.Drawing.Point(254, 52)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.ReadOnly = True
        Me.txtMonto.Size = New System.Drawing.Size(77, 20)
        Me.txtMonto.TabIndex = 6
        Me.txtMonto.Text = "0.00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(192, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Concepto:"
        '
        'txtConcepto
        '
        Me.txtConcepto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConcepto.Location = New System.Drawing.Point(254, 26)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(375, 20)
        Me.txtConcepto.TabIndex = 1
        '
        'lbMaster
        '
        Me.lbMaster.AutoSize = True
        Me.lbMaster.Location = New System.Drawing.Point(26, 6)
        Me.lbMaster.Name = "lbMaster"
        Me.lbMaster.Size = New System.Drawing.Size(100, 13)
        Me.lbMaster.TabIndex = 198
        Me.lbMaster.Text = "MASTER ENDOSE"
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(3, -26)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(2678, 42)
        Me.pmasterDeco.TabIndex = 197
        Me.pmasterDeco.Text = "_________________________________________________________________________________" & _
    "____________________________________________________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.nbgRetrive
        Me.NavBarControl1.Appearance.Background.BackColor = System.Drawing.Color.LightSlateGray
        Me.NavBarControl1.Appearance.Background.Options.UseBackColor = True
        Me.NavBarControl1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.NavBarControl1.ContentButtonHint = Nothing
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.GroupBackgroundImage = CType(resources.GetObject("NavBarControl1.GroupBackgroundImage"), System.Drawing.Image)
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.nbgRetrive})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NavBarItem1, Me.nbiAplicar, Me.nbiDetalle, Me.nbiEndosada, Me.nbiPendiente, Me.nbgCancelar})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 0)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.OptionsNavPane.ExpandedWidth = 124
        Me.NavBarControl1.Size = New System.Drawing.Size(124, 785)
        Me.NavBarControl1.TabIndex = 167
        Me.NavBarControl1.Text = "NavBarControl1"
        Me.NavBarControl1.View = New DevExpress.XtraNavBar.ViewInfo.AdvExplorerBarViewInfoRegistrator()
        '
        'nbgRetrive
        '
        Me.nbgRetrive.Appearance.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.nbgRetrive.Appearance.BackColor2 = System.Drawing.SystemColors.ButtonFace
        Me.nbgRetrive.Appearance.Options.UseBackColor = True
        Me.nbgRetrive.AppearanceHotTracked.BackColor = System.Drawing.Color.White
        Me.nbgRetrive.AppearanceHotTracked.Options.UseBackColor = True
        Me.nbgRetrive.Caption = "Mostrar"
        Me.nbgRetrive.Expanded = True
        Me.nbgRetrive.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText
        Me.nbgRetrive.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiDetalle), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiPendiente), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiEndosada)})
        Me.nbgRetrive.Name = "nbgRetrive"
        '
        'nbiDetalle
        '
        Me.nbiDetalle.Caption = "Detalle"
        Me.nbiDetalle.Name = "nbiDetalle"
        '
        'nbiPendiente
        '
        Me.nbiPendiente.Caption = "Pendientes"
        Me.nbiPendiente.Name = "nbiPendiente"
        '
        'nbiEndosada
        '
        Me.nbiEndosada.Caption = "Endosadas"
        Me.nbiEndosada.Name = "nbiEndosada"
        '
        'NavBarItem1
        '
        Me.NavBarItem1.Caption = "NavBarItem1"
        Me.NavBarItem1.Name = "NavBarItem1"
        '
        'nbiAplicar
        '
        Me.nbiAplicar.Caption = "Aplicar"
        Me.nbiAplicar.Name = "nbiAplicar"
        '
        'nbgCancelar
        '
        Me.nbgCancelar.Caption = "Cancelar"
        Me.nbgCancelar.Name = "nbgCancelar"
        '
        'frmEndoseFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1234, 785)
        Me.Controls.Add(Me.pMain)
        Me.Name = "frmEndoseFactura"
        Me.Text = "Endose Facturas"
        Me.pMain.ResumeLayout(False)
        Me.pMain.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents nbgRetrive As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nbiDetalle As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiPendiente As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiEndosada As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiAplicar As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbgCancelar As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItem1 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtnFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents lbMaster As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents lbObservacion As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pdeco As System.Windows.Forms.Panel
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaF As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents btnMarcar As System.Windows.Forms.Button
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lbObservacion2 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion2 As System.Windows.Forms.TextBox
    Friend WithEvents lbCodigo As System.Windows.Forms.Label
    Friend WithEvents lbFact As System.Windows.Forms.Label
End Class
