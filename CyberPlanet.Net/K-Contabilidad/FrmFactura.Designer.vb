﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFactura
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.Pdetalle = New System.Windows.Forms.Panel()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.PdetalleDeco = New System.Windows.Forms.Label()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.gbTotal = New System.Windows.Forms.GroupBox()
        Me.txtTdescuento = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtntpagar = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtTprecio = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtTcosto = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtTapagar = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTcolocacion = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.txtTneto = New System.Windows.Forms.TextBox()
        Me.ckbIVA = New System.Windows.Forms.CheckBox()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.txtFinancimiento = New System.Windows.Forms.TextBox()
        Me.ckbIR = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ckbIMI = New System.Windows.Forms.CheckBox()
        Me.txtIMI = New System.Windows.Forms.TextBox()
        Me.txtIR = New System.Windows.Forms.TextBox()
        Me.txtSubtotal = New System.Windows.Forms.TextBox()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Ppago = New System.Windows.Forms.Panel()
        Me.dgvPago = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnPprint = New System.Windows.Forms.Button()
        Me.txtPago = New System.Windows.Forms.TextBox()
        Me.btnPmodificar = New System.Windows.Forms.Button()
        Me.dtpCuota2 = New System.Windows.Forms.DateTimePicker()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.dtpCuota = New System.Windows.Forms.DateTimePicker()
        Me.txtPlazo = New System.Windows.Forms.TextBox()
        Me.txtMontoFinanciar = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbModalidad = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtPrima = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpPrima = New System.Windows.Forms.DateTimePicker()
        Me.txtInteres = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtCuota = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.pPagoDeco = New System.Windows.Forms.Label()
        Me.Pcliente = New System.Windows.Forms.Panel()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtcdireccion = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtCnombre = New System.Windows.Forms.TextBox()
        Me.txtCapellido = New System.Windows.Forms.TextBox()
        Me.cbtipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDescuento = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtCcodigo = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtCcedula = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCcartera = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.pClienteDeco = New System.Windows.Forms.Label()
        Me.pMaster = New System.Windows.Forms.Panel()
        Me.lbEndosado = New System.Windows.Forms.Label()
        Me.lbVerificado = New System.Windows.Forms.Label()
        Me.lbObservacion = New System.Windows.Forms.Label()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cbModenda = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cbEstado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtFactura = New System.Windows.Forms.TextBox()
        Me.txtRemision = New System.Windows.Forms.TextBox()
        Me.cbvtipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txtGarantia = New System.Windows.Forms.TextBox()
        Me.txtSupervisor = New System.Windows.Forms.TextBox()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.txtSolicitud = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtvehiculo = New System.Windows.Forms.TextBox()
        Me.txtRuta = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTasaCambio = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.cbCaja = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.pmasterDeco = New System.Windows.Forms.Label()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnCordoba = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDolar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtctelefono = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.luBarrio = New DevExpress.XtraEditors.LookUpEdit()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.pMain.SuspendLayout()
        Me.Pdetalle.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbTotal.SuspendLayout()
        Me.Ppago.SuspendLayout()
        CType(Me.dgvPago, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cbModalidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pcliente.SuspendLayout()
        CType(Me.cbtipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMaster.SuspendLayout()
        CType(Me.cbModenda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbvtipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.luBarrio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.Pdetalle)
        Me.pMain.Controls.Add(Me.Ppago)
        Me.pMain.Controls.Add(Me.Pcliente)
        Me.pMain.Controls.Add(Me.pMaster)
        Me.pMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pMain.Location = New System.Drawing.Point(0, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(1235, 586)
        Me.pMain.TabIndex = 0
        '
        'Pdetalle
        '
        Me.Pdetalle.Controls.Add(Me.Label89)
        Me.Pdetalle.Controls.Add(Me.PdetalleDeco)
        Me.Pdetalle.Controls.Add(Me.dgvDetalle)
        Me.Pdetalle.Controls.Add(Me.gbTotal)
        Me.Pdetalle.Dock = System.Windows.Forms.DockStyle.Top
        Me.Pdetalle.Enabled = False
        Me.Pdetalle.Location = New System.Drawing.Point(0, 413)
        Me.Pdetalle.Name = "Pdetalle"
        Me.Pdetalle.Size = New System.Drawing.Size(1235, 171)
        Me.Pdetalle.TabIndex = 252
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(27, 5)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(108, 13)
        Me.Label89.TabIndex = 203
        Me.Label89.Text = "DETALLE FACTURA"
        '
        'PdetalleDeco
        '
        Me.PdetalleDeco.AutoSize = True
        Me.PdetalleDeco.BackColor = System.Drawing.Color.Transparent
        Me.PdetalleDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PdetalleDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.PdetalleDeco.Location = New System.Drawing.Point(6, -27)
        Me.PdetalleDeco.Name = "PdetalleDeco"
        Me.PdetalleDeco.Size = New System.Drawing.Size(1298, 42)
        Me.PdetalleDeco.TabIndex = 202
        Me.PdetalleDeco.Text = "________________________________________________________________"
        Me.PdetalleDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.AllowUserToResizeColumns = False
        Me.dgvDetalle.AllowUserToResizeRows = False
        Me.dgvDetalle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalle.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.ColumnCordoba, Me.ColumnDolar, Me.Column5, Me.Column11, Me.Column12, Me.Column13})
        Me.dgvDetalle.Location = New System.Drawing.Point(11, 21)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(723, 147)
        Me.dgvDetalle.TabIndex = 204
        Me.dgvDetalle.TabStop = False
        '
        'gbTotal
        '
        Me.gbTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbTotal.Controls.Add(Me.txtTdescuento)
        Me.gbTotal.Controls.Add(Me.Label31)
        Me.gbTotal.Controls.Add(Me.txtntpagar)
        Me.gbTotal.Controls.Add(Me.Label30)
        Me.gbTotal.Controls.Add(Me.txtTprecio)
        Me.gbTotal.Controls.Add(Me.Label28)
        Me.gbTotal.Controls.Add(Me.txtTcosto)
        Me.gbTotal.Controls.Add(Me.Label27)
        Me.gbTotal.Controls.Add(Me.txtTapagar)
        Me.gbTotal.Controls.Add(Me.Label12)
        Me.gbTotal.Controls.Add(Me.txtTcolocacion)
        Me.gbTotal.Controls.Add(Me.Label10)
        Me.gbTotal.Controls.Add(Me.txtIVA)
        Me.gbTotal.Controls.Add(Me.txtTneto)
        Me.gbTotal.Controls.Add(Me.ckbIVA)
        Me.gbTotal.Controls.Add(Me.Label99)
        Me.gbTotal.Controls.Add(Me.txtFinancimiento)
        Me.gbTotal.Controls.Add(Me.ckbIR)
        Me.gbTotal.Controls.Add(Me.Label11)
        Me.gbTotal.Controls.Add(Me.ckbIMI)
        Me.gbTotal.Controls.Add(Me.txtIMI)
        Me.gbTotal.Controls.Add(Me.txtIR)
        Me.gbTotal.Controls.Add(Me.txtSubtotal)
        Me.gbTotal.Controls.Add(Me.Label92)
        Me.gbTotal.Controls.Add(Me.Label35)
        Me.gbTotal.Location = New System.Drawing.Point(728, 16)
        Me.gbTotal.Name = "gbTotal"
        Me.gbTotal.Size = New System.Drawing.Size(495, 153)
        Me.gbTotal.TabIndex = 239
        Me.gbTotal.TabStop = False
        '
        'txtTdescuento
        '
        Me.txtTdescuento.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTdescuento.BackColor = System.Drawing.Color.SeaShell
        Me.txtTdescuento.Location = New System.Drawing.Point(242, 95)
        Me.txtTdescuento.Name = "txtTdescuento"
        Me.txtTdescuento.ReadOnly = True
        Me.txtTdescuento.Size = New System.Drawing.Size(64, 20)
        Me.txtTdescuento.TabIndex = 256
        Me.txtTdescuento.TabStop = False
        Me.txtTdescuento.Text = "0.00"
        Me.txtTdescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label31
        '
        Me.Label31.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(173, 98)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(62, 13)
        Me.Label31.TabIndex = 255
        Me.Label31.Text = "Descuento:"
        '
        'txtntpagar
        '
        Me.txtntpagar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtntpagar.BackColor = System.Drawing.Color.SeaShell
        Me.txtntpagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtntpagar.Location = New System.Drawing.Point(414, 95)
        Me.txtntpagar.Name = "txtntpagar"
        Me.txtntpagar.ReadOnly = True
        Me.txtntpagar.Size = New System.Drawing.Size(64, 20)
        Me.txtntpagar.TabIndex = 252
        Me.txtntpagar.TabStop = False
        Me.txtntpagar.Text = "0.00"
        Me.txtntpagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label30
        '
        Me.Label30.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(335, 98)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(73, 13)
        Me.Label30.TabIndex = 251
        Me.Label30.Text = "Neto a Pagar:"
        '
        'txtTprecio
        '
        Me.txtTprecio.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTprecio.BackColor = System.Drawing.Color.SeaShell
        Me.txtTprecio.Location = New System.Drawing.Point(414, 19)
        Me.txtTprecio.Name = "txtTprecio"
        Me.txtTprecio.ReadOnly = True
        Me.txtTprecio.Size = New System.Drawing.Size(64, 20)
        Me.txtTprecio.TabIndex = 250
        Me.txtTprecio.TabStop = False
        Me.txtTprecio.Text = "0.00"
        Me.txtTprecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(332, 22)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(76, 13)
        Me.Label28.TabIndex = 249
        Me.Label28.Text = "Precio Detalle:"
        '
        'txtTcosto
        '
        Me.txtTcosto.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTcosto.BackColor = System.Drawing.Color.SeaShell
        Me.txtTcosto.Location = New System.Drawing.Point(83, 17)
        Me.txtTcosto.Name = "txtTcosto"
        Me.txtTcosto.ReadOnly = True
        Me.txtTcosto.Size = New System.Drawing.Size(64, 20)
        Me.txtTcosto.TabIndex = 248
        Me.txtTcosto.TabStop = False
        Me.txtTcosto.Text = "0.00"
        Me.txtTcosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label27
        '
        Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(40, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(37, 13)
        Me.Label27.TabIndex = 247
        Me.Label27.Text = "Costo:"
        '
        'txtTapagar
        '
        Me.txtTapagar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTapagar.BackColor = System.Drawing.Color.SeaShell
        Me.txtTapagar.Location = New System.Drawing.Point(242, 43)
        Me.txtTapagar.Name = "txtTapagar"
        Me.txtTapagar.ReadOnly = True
        Me.txtTapagar.Size = New System.Drawing.Size(64, 20)
        Me.txtTapagar.TabIndex = 246
        Me.txtTapagar.TabStop = False
        Me.txtTapagar.Text = "0.00"
        Me.txtTapagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(162, 46)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(74, 13)
        Me.Label12.TabIndex = 245
        Me.Label12.Text = "Total a Pagar:"
        '
        'txtTcolocacion
        '
        Me.txtTcolocacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTcolocacion.BackColor = System.Drawing.Color.SeaShell
        Me.txtTcolocacion.Location = New System.Drawing.Point(242, 69)
        Me.txtTcolocacion.Name = "txtTcolocacion"
        Me.txtTcolocacion.ReadOnly = True
        Me.txtTcolocacion.Size = New System.Drawing.Size(64, 20)
        Me.txtTcolocacion.TabIndex = 244
        Me.txtTcolocacion.TabStop = False
        Me.txtTcolocacion.Text = "0.00"
        Me.txtTcolocacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(173, 72)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 13)
        Me.Label10.TabIndex = 243
        Me.Label10.Text = "Colocacion:"
        '
        'txtIVA
        '
        Me.txtIVA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtIVA.BackColor = System.Drawing.Color.SeaShell
        Me.txtIVA.Location = New System.Drawing.Point(83, 69)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.ReadOnly = True
        Me.txtIVA.Size = New System.Drawing.Size(64, 20)
        Me.txtIVA.TabIndex = 231
        Me.txtIVA.TabStop = False
        Me.txtIVA.Text = "0.00"
        Me.txtIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTneto
        '
        Me.txtTneto.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTneto.BackColor = System.Drawing.Color.SeaShell
        Me.txtTneto.Location = New System.Drawing.Point(83, 95)
        Me.txtTneto.Name = "txtTneto"
        Me.txtTneto.ReadOnly = True
        Me.txtTneto.Size = New System.Drawing.Size(64, 20)
        Me.txtTneto.TabIndex = 225
        Me.txtTneto.TabStop = False
        Me.txtTneto.Text = "0.00"
        Me.txtTneto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ckbIVA
        '
        Me.ckbIVA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckbIVA.AutoSize = True
        Me.ckbIVA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbIVA.Checked = True
        Me.ckbIVA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckbIVA.Location = New System.Drawing.Point(34, 71)
        Me.ckbIVA.Name = "ckbIVA"
        Me.ckbIVA.Size = New System.Drawing.Size(43, 17)
        Me.ckbIVA.TabIndex = 206
        Me.ckbIVA.TabStop = False
        Me.ckbIVA.Text = "IVA"
        Me.ckbIVA.UseVisualStyleBackColor = True
        '
        'Label99
        '
        Me.Label99.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(16, 98)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(60, 13)
        Me.Label99.TabIndex = 224
        Me.Label99.Text = "Total Neto:"
        '
        'txtFinancimiento
        '
        Me.txtFinancimiento.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFinancimiento.BackColor = System.Drawing.Color.SeaShell
        Me.txtFinancimiento.Location = New System.Drawing.Point(414, 45)
        Me.txtFinancimiento.Name = "txtFinancimiento"
        Me.txtFinancimiento.ReadOnly = True
        Me.txtFinancimiento.Size = New System.Drawing.Size(64, 20)
        Me.txtFinancimiento.TabIndex = 170
        Me.txtFinancimiento.TabStop = False
        Me.txtFinancimiento.Text = "0.00"
        Me.txtFinancimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ckbIR
        '
        Me.ckbIR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckbIR.AutoSize = True
        Me.ckbIR.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbIR.Location = New System.Drawing.Point(40, 123)
        Me.ckbIR.Name = "ckbIR"
        Me.ckbIR.Size = New System.Drawing.Size(37, 17)
        Me.ckbIR.TabIndex = 207
        Me.ckbIR.TabStop = False
        Me.ckbIR.Text = "IR"
        Me.ckbIR.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(327, 48)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 169
        Me.Label11.Text = "Financiamiento:"
        '
        'ckbIMI
        '
        Me.ckbIMI.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckbIMI.AutoSize = True
        Me.ckbIMI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ckbIMI.Location = New System.Drawing.Point(195, 19)
        Me.ckbIMI.Name = "ckbIMI"
        Me.ckbIMI.Size = New System.Drawing.Size(41, 17)
        Me.ckbIMI.TabIndex = 208
        Me.ckbIMI.TabStop = False
        Me.ckbIMI.Text = "IMI"
        Me.ckbIMI.UseVisualStyleBackColor = True
        '
        'txtIMI
        '
        Me.txtIMI.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtIMI.BackColor = System.Drawing.Color.SeaShell
        Me.txtIMI.Location = New System.Drawing.Point(242, 17)
        Me.txtIMI.Name = "txtIMI"
        Me.txtIMI.ReadOnly = True
        Me.txtIMI.Size = New System.Drawing.Size(64, 20)
        Me.txtIMI.TabIndex = 233
        Me.txtIMI.TabStop = False
        Me.txtIMI.Text = "0.00"
        Me.txtIMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIR
        '
        Me.txtIR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtIR.BackColor = System.Drawing.Color.SeaShell
        Me.txtIR.Location = New System.Drawing.Point(83, 121)
        Me.txtIR.Name = "txtIR"
        Me.txtIR.ReadOnly = True
        Me.txtIR.Size = New System.Drawing.Size(64, 20)
        Me.txtIR.TabIndex = 232
        Me.txtIR.TabStop = False
        Me.txtIR.Text = "0.00"
        Me.txtIR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSubtotal
        '
        Me.txtSubtotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSubtotal.BackColor = System.Drawing.Color.SeaShell
        Me.txtSubtotal.Location = New System.Drawing.Point(83, 43)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.ReadOnly = True
        Me.txtSubtotal.Size = New System.Drawing.Size(64, 20)
        Me.txtSubtotal.TabIndex = 217
        Me.txtSubtotal.TabStop = False
        Me.txtSubtotal.Text = "0.00"
        Me.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label92
        '
        Me.Label92.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(20, 46)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(56, 13)
        Me.Label92.TabIndex = 216
        Me.Label92.Text = "Sub-Total:"
        '
        'Label35
        '
        Me.Label35.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.SystemColors.Control
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.Label35.Location = New System.Drawing.Point(325, 61)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(162, 20)
        Me.Label35.TabIndex = 200
        Me.Label35.Text = "_________________"
        '
        'Ppago
        '
        Me.Ppago.Controls.Add(Me.dgvPago)
        Me.Ppago.Controls.Add(Me.GroupBox2)
        Me.Ppago.Controls.Add(Me.Label8)
        Me.Ppago.Controls.Add(Me.pPagoDeco)
        Me.Ppago.Dock = System.Windows.Forms.DockStyle.Top
        Me.Ppago.Enabled = False
        Me.Ppago.Location = New System.Drawing.Point(0, 177)
        Me.Ppago.Name = "Ppago"
        Me.Ppago.Size = New System.Drawing.Size(1235, 236)
        Me.Ppago.TabIndex = 199
        '
        'dgvPago
        '
        Me.dgvPago.AllowUserToAddRows = False
        Me.dgvPago.AllowUserToDeleteRows = False
        Me.dgvPago.AllowUserToResizeColumns = False
        Me.dgvPago.AllowUserToResizeRows = False
        Me.dgvPago.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvPago.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPago.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Column7, Me.Column6, Me.Column9, Me.Column10, Me.Column8})
        Me.dgvPago.Location = New System.Drawing.Point(11, 21)
        Me.dgvPago.Name = "dgvPago"
        Me.dgvPago.ReadOnly = True
        Me.dgvPago.RowHeadersVisible = False
        Me.dgvPago.Size = New System.Drawing.Size(723, 212)
        Me.dgvPago.TabIndex = 241
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "#"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 35
        '
        'Column7
        '
        Me.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column7.HeaderText = "Fecha"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "Cuota"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 80
        '
        'Column9
        '
        Me.Column9.HeaderText = "Principal"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 80
        '
        'Column10
        '
        Me.Column10.HeaderText = "Interes"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 80
        '
        'Column8
        '
        Me.Column8.HeaderText = "Pendiente"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 80
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnPprint)
        Me.GroupBox2.Controls.Add(Me.txtPago)
        Me.GroupBox2.Controls.Add(Me.btnPmodificar)
        Me.GroupBox2.Controls.Add(Me.dtpCuota2)
        Me.GroupBox2.Controls.Add(Me.Label37)
        Me.GroupBox2.Controls.Add(Me.dtpCuota)
        Me.GroupBox2.Controls.Add(Me.txtPlazo)
        Me.GroupBox2.Controls.Add(Me.txtMontoFinanciar)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cbModalidad)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.txtPrima)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.dtpPrima)
        Me.GroupBox2.Controls.Add(Me.txtInteres)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.txtCuota)
        Me.GroupBox2.Controls.Add(Me.Label38)
        Me.GroupBox2.Location = New System.Drawing.Point(728, 15)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(495, 212)
        Me.GroupBox2.TabIndex = 242
        Me.GroupBox2.TabStop = False
        '
        'btnPprint
        '
        Me.btnPprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPprint.Location = New System.Drawing.Point(40, 146)
        Me.btnPprint.Name = "btnPprint"
        Me.btnPprint.Size = New System.Drawing.Size(75, 23)
        Me.btnPprint.TabIndex = 18
        Me.btnPprint.Text = "Imprimir"
        Me.btnPprint.UseVisualStyleBackColor = True
        Me.btnPprint.Visible = False
        '
        'txtPago
        '
        Me.txtPago.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtPago.BackColor = System.Drawing.SystemColors.Control
        Me.txtPago.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPago.Location = New System.Drawing.Point(283, 29)
        Me.txtPago.Name = "txtPago"
        Me.txtPago.ReadOnly = True
        Me.txtPago.Size = New System.Drawing.Size(195, 15)
        Me.txtPago.TabIndex = 261
        Me.txtPago.TabStop = False
        Me.txtPago.Text = "00000000"
        Me.txtPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnPmodificar
        '
        Me.btnPmodificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPmodificar.Location = New System.Drawing.Point(39, 175)
        Me.btnPmodificar.Name = "btnPmodificar"
        Me.btnPmodificar.Size = New System.Drawing.Size(75, 23)
        Me.btnPmodificar.TabIndex = 19
        Me.btnPmodificar.Text = "Modificar"
        Me.btnPmodificar.UseVisualStyleBackColor = True
        Me.btnPmodificar.Visible = False
        '
        'dtpCuota2
        '
        Me.dtpCuota2.CustomFormat = "dddd  dd/MM/yy"
        Me.dtpCuota2.Enabled = False
        Me.dtpCuota2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCuota2.Location = New System.Drawing.Point(126, 106)
        Me.dtpCuota2.Name = "dtpCuota2"
        Me.dtpCuota2.Size = New System.Drawing.Size(150, 20)
        Me.dtpCuota2.TabIndex = 14
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(36, 112)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(84, 13)
        Me.Label37.TabIndex = 258
        Me.Label37.Text = "Segunda Cuota:"
        '
        'dtpCuota
        '
        Me.dtpCuota.CustomFormat = "dddd  dd/MM/yy"
        Me.dtpCuota.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCuota.Location = New System.Drawing.Point(126, 80)
        Me.dtpCuota.Name = "dtpCuota"
        Me.dtpCuota.Size = New System.Drawing.Size(150, 20)
        Me.dtpCuota.TabIndex = 13
        '
        'txtPlazo
        '
        Me.txtPlazo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPlazo.Location = New System.Drawing.Point(414, 106)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(64, 20)
        Me.txtPlazo.TabIndex = 16
        Me.txtPlazo.Text = "0"
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMontoFinanciar
        '
        Me.txtMontoFinanciar.BackColor = System.Drawing.Color.SeaShell
        Me.txtMontoFinanciar.Location = New System.Drawing.Point(405, 168)
        Me.txtMontoFinanciar.Name = "txtMontoFinanciar"
        Me.txtMontoFinanciar.ReadOnly = True
        Me.txtMontoFinanciar.Size = New System.Drawing.Size(73, 20)
        Me.txtMontoFinanciar.TabIndex = 172
        Me.txtMontoFinanciar.TabStop = False
        Me.txtMontoFinanciar.Text = "0.00"
        Me.txtMontoFinanciar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(304, 170)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 13)
        Me.Label1.TabIndex = 171
        Me.Label1.Text = "Monto a Financiar:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(325, 83)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 159
        Me.Label6.Text = "Monto de Prima:"
        '
        'cbModalidad
        '
        Me.cbModalidad.EditValue = "QUINCENAL"
        Me.cbModalidad.Location = New System.Drawing.Point(126, 28)
        Me.cbModalidad.Name = "cbModalidad"
        Me.cbModalidad.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbModalidad.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbModalidad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbModalidad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbModalidad.Properties.Items.AddRange(New Object() {"SEMANAL", "QUINCENAL", "MENSUAL"})
        Me.cbModalidad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbModalidad.Size = New System.Drawing.Size(150, 20)
        Me.cbModalidad.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(61, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 161
        Me.Label5.Text = "Modalidad:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(372, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 13)
        Me.Label3.TabIndex = 157
        Me.Label3.Text = "Plazo:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(44, 86)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(76, 13)
        Me.Label29.TabIndex = 179
        Me.Label29.Text = "Primera Cuota:"
        '
        'txtPrima
        '
        Me.txtPrima.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtPrima.Location = New System.Drawing.Point(415, 80)
        Me.txtPrima.Name = "txtPrima"
        Me.txtPrima.Size = New System.Drawing.Size(63, 20)
        Me.txtPrima.TabIndex = 15
        Me.txtPrima.Text = "0.00"
        Me.txtPrima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(459, 56)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(20, 16)
        Me.Label16.TabIndex = 177
        Me.Label16.Text = "%"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(51, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 13)
        Me.Label7.TabIndex = 150
        Me.Label7.Text = "Fecha Prima:"
        '
        'dtpPrima
        '
        Me.dtpPrima.CustomFormat = "dddd  dd/MM/yy"
        Me.dtpPrima.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPrima.Location = New System.Drawing.Point(126, 54)
        Me.dtpPrima.Name = "dtpPrima"
        Me.dtpPrima.Size = New System.Drawing.Size(150, 20)
        Me.dtpPrima.TabIndex = 12
        '
        'txtInteres
        '
        Me.txtInteres.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtInteres.BackColor = System.Drawing.SystemColors.Control
        Me.txtInteres.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtInteres.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInteres.Location = New System.Drawing.Point(415, 55)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.ReadOnly = True
        Me.txtInteres.Size = New System.Drawing.Size(43, 15)
        Me.txtInteres.TabIndex = 176
        Me.txtInteres.TabStop = False
        Me.txtInteres.Text = "5.5"
        Me.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(361, 146)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(38, 13)
        Me.Label13.TabIndex = 173
        Me.Label13.Text = "Cuota:"
        '
        'txtCuota
        '
        Me.txtCuota.BackColor = System.Drawing.Color.SeaShell
        Me.txtCuota.Location = New System.Drawing.Point(405, 142)
        Me.txtCuota.Name = "txtCuota"
        Me.txtCuota.ReadOnly = True
        Me.txtCuota.Size = New System.Drawing.Size(73, 20)
        Me.txtCuota.TabIndex = 174
        Me.txtCuota.TabStop = False
        Me.txtCuota.Text = "0.00"
        Me.txtCuota.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.SystemColors.Control
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.Label38.Location = New System.Drawing.Point(35, 117)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(450, 20)
        Me.Label38.TabIndex = 260
        Me.Label38.Text = "_________________________________________________"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(27, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(122, 13)
        Me.Label8.TabIndex = 201
        Me.Label8.Text = "MODALIDAD DE PAGO"
        '
        'pPagoDeco
        '
        Me.pPagoDeco.AutoSize = True
        Me.pPagoDeco.BackColor = System.Drawing.Color.Transparent
        Me.pPagoDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pPagoDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pPagoDeco.Location = New System.Drawing.Point(6, -27)
        Me.pPagoDeco.Name = "pPagoDeco"
        Me.pPagoDeco.Size = New System.Drawing.Size(1298, 42)
        Me.pPagoDeco.TabIndex = 247
        Me.pPagoDeco.Text = "________________________________________________________________"
        Me.pPagoDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Pcliente
        '
        Me.Pcliente.Controls.Add(Me.Label15)
        Me.Pcliente.Controls.Add(Me.luBarrio)
        Me.Pcliente.Controls.Add(Me.Label41)
        Me.Pcliente.Controls.Add(Me.TextBox1)
        Me.Pcliente.Controls.Add(Me.txtcdireccion)
        Me.Pcliente.Controls.Add(Me.Label40)
        Me.Pcliente.Controls.Add(Me.txtctelefono)
        Me.Pcliente.Controls.Add(Me.Label39)
        Me.Pcliente.Controls.Add(Me.Label33)
        Me.Pcliente.Controls.Add(Me.Label25)
        Me.Pcliente.Controls.Add(Me.txtCnombre)
        Me.Pcliente.Controls.Add(Me.txtCapellido)
        Me.Pcliente.Controls.Add(Me.cbtipo)
        Me.Pcliente.Controls.Add(Me.Label9)
        Me.Pcliente.Controls.Add(Me.txtDescuento)
        Me.Pcliente.Controls.Add(Me.Label32)
        Me.Pcliente.Controls.Add(Me.txtCcodigo)
        Me.Pcliente.Controls.Add(Me.Label21)
        Me.Pcliente.Controls.Add(Me.Label22)
        Me.Pcliente.Controls.Add(Me.txtCcedula)
        Me.Pcliente.Controls.Add(Me.Label20)
        Me.Pcliente.Controls.Add(Me.txtCcartera)
        Me.Pcliente.Controls.Add(Me.Label18)
        Me.Pcliente.Controls.Add(Me.Label26)
        Me.Pcliente.Controls.Add(Me.pClienteDeco)
        Me.Pcliente.Dock = System.Windows.Forms.DockStyle.Top
        Me.Pcliente.Enabled = False
        Me.Pcliente.Location = New System.Drawing.Point(0, 104)
        Me.Pcliente.Name = "Pcliente"
        Me.Pcliente.Size = New System.Drawing.Size(1235, 73)
        Me.Pcliente.TabIndex = 198
        '
        'Label41
        '
        Me.Label41.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(701, 27)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(21, 13)
        Me.Label41.TabIndex = 205
        Me.Label41.Text = "PV"
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TextBox1.Location = New System.Drawing.Point(625, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(75, 20)
        Me.TextBox1.TabIndex = 9
        Me.TextBox1.Text = "0"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcdireccion
        '
        Me.txtcdireccion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtcdireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcdireccion.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtcdireccion.Location = New System.Drawing.Point(813, 50)
        Me.txtcdireccion.Name = "txtcdireccion"
        Me.txtcdireccion.Size = New System.Drawing.Size(393, 20)
        Me.txtcdireccion.TabIndex = 10
        '
        'Label40
        '
        Me.Label40.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(784, 53)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(23, 13)
        Me.Label40.TabIndex = 203
        Me.Label40.Text = "Dir:"
        '
        'Label33
        '
        Me.Label33.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(604, 27)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(15, 13)
        Me.Label33.TabIndex = 181
        Me.Label33.Text = "%"
        '
        'Label25
        '
        Me.Label25.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(41, 27)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(29, 13)
        Me.Label25.TabIndex = 168
        Me.Label25.Text = "Cod:"
        '
        'txtCnombre
        '
        Me.txtCnombre.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCnombre.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCnombre.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtCnombre.Location = New System.Drawing.Point(272, 24)
        Me.txtCnombre.Name = "txtCnombre"
        Me.txtCnombre.ReadOnly = True
        Me.txtCnombre.Size = New System.Drawing.Size(195, 20)
        Me.txtCnombre.TabIndex = 7
        '
        'txtCapellido
        '
        Me.txtCapellido.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCapellido.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCapellido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCapellido.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtCapellido.Location = New System.Drawing.Point(272, 50)
        Me.txtCapellido.Name = "txtCapellido"
        Me.txtCapellido.ReadOnly = True
        Me.txtCapellido.Size = New System.Drawing.Size(195, 20)
        Me.txtCapellido.TabIndex = 158
        Me.txtCapellido.TabStop = False
        '
        'cbtipo
        '
        Me.cbtipo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbtipo.EditValue = "EMPLEADO"
        Me.cbtipo.Enabled = False
        Me.cbtipo.Location = New System.Drawing.Point(813, 24)
        Me.cbtipo.Name = "cbtipo"
        Me.cbtipo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbtipo.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbtipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbtipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbtipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbtipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbtipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbtipo.Properties.Items.AddRange(New Object() {"DETALLE", "MAYORISTA", "EMPLEADO"})
        Me.cbtipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbtipo.Size = New System.Drawing.Size(104, 20)
        Me.cbtipo.TabIndex = 153
        Me.cbtipo.TabStop = False
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(743, 27)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 13)
        Me.Label9.TabIndex = 122
        Me.Label9.Text = "Tipo Cliente:"
        '
        'txtDescuento
        '
        Me.txtDescuento.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtDescuento.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtDescuento.Location = New System.Drawing.Point(563, 24)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(40, 20)
        Me.txtDescuento.TabIndex = 8
        Me.txtDescuento.Text = "0"
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label32
        '
        Me.Label32.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(525, 27)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(35, 13)
        Me.Label32.TabIndex = 181
        Me.Label32.Text = "Desc."
        '
        'txtCcodigo
        '
        Me.txtCcodigo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCcodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcodigo.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtCcodigo.Location = New System.Drawing.Point(76, 24)
        Me.txtCcodigo.Name = "txtCcodigo"
        Me.txtCcodigo.ReadOnly = True
        Me.txtCcodigo.Size = New System.Drawing.Size(94, 20)
        Me.txtCcodigo.TabIndex = 163
        Me.txtCcodigo.TabStop = False
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(27, 53)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(43, 13)
        Me.Label21.TabIndex = 159
        Me.Label21.Text = "Cedula:"
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(216, 53)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(52, 13)
        Me.Label22.TabIndex = 157
        Me.Label22.Text = "Apeliidos:"
        '
        'txtCcedula
        '
        Me.txtCcedula.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCcedula.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcedula.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtCcedula.Location = New System.Drawing.Point(76, 50)
        Me.txtCcedula.Name = "txtCcedula"
        Me.txtCcedula.ReadOnly = True
        Me.txtCcedula.Size = New System.Drawing.Size(94, 20)
        Me.txtCcedula.TabIndex = 160
        Me.txtCcedula.TabStop = False
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(221, 27)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(47, 13)
        Me.Label20.TabIndex = 161
        Me.Label20.Text = "Nombre:"
        '
        'txtCcartera
        '
        Me.txtCcartera.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCcartera.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtCcartera.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCcartera.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtCcartera.Location = New System.Drawing.Point(1144, 24)
        Me.txtCcartera.Name = "txtCcartera"
        Me.txtCcartera.ReadOnly = True
        Me.txtCcartera.Size = New System.Drawing.Size(63, 20)
        Me.txtCcartera.TabIndex = 151
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(1094, 27)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(44, 13)
        Me.Label18.TabIndex = 150
        Me.Label18.Text = "Cartera:"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(27, 5)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(110, 13)
        Me.Label26.TabIndex = 200
        Me.Label26.Text = "DATOS DE CLIENTE"
        '
        'pClienteDeco
        '
        Me.pClienteDeco.AutoSize = True
        Me.pClienteDeco.BackColor = System.Drawing.Color.Transparent
        Me.pClienteDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pClienteDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pClienteDeco.Location = New System.Drawing.Point(6, -27)
        Me.pClienteDeco.Name = "pClienteDeco"
        Me.pClienteDeco.Size = New System.Drawing.Size(1298, 42)
        Me.pClienteDeco.TabIndex = 199
        Me.pClienteDeco.Text = "________________________________________________________________"
        Me.pClienteDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pMaster
        '
        Me.pMaster.Controls.Add(Me.lbEndosado)
        Me.pMaster.Controls.Add(Me.lbVerificado)
        Me.pMaster.Controls.Add(Me.lbObservacion)
        Me.pMaster.Controls.Add(Me.txtObservacion)
        Me.pMaster.Controls.Add(Me.Label34)
        Me.pMaster.Controls.Add(Me.cbModenda)
        Me.pMaster.Controls.Add(Me.txtVendedor)
        Me.pMaster.Controls.Add(Me.Label17)
        Me.pMaster.Controls.Add(Me.cbEstado)
        Me.pMaster.Controls.Add(Me.Label53)
        Me.pMaster.Controls.Add(Me.txtSerie)
        Me.pMaster.Controls.Add(Me.txtSucursal)
        Me.pMaster.Controls.Add(Me.Label23)
        Me.pMaster.Controls.Add(Me.Label50)
        Me.pMaster.Controls.Add(Me.txtFactura)
        Me.pMaster.Controls.Add(Me.txtRemision)
        Me.pMaster.Controls.Add(Me.cbvtipo)
        Me.pMaster.Controls.Add(Me.Label95)
        Me.pMaster.Controls.Add(Me.Label2)
        Me.pMaster.Controls.Add(Me.Label51)
        Me.pMaster.Controls.Add(Me.txtGarantia)
        Me.pMaster.Controls.Add(Me.txtSupervisor)
        Me.pMaster.Controls.Add(Me.dtpFechaI)
        Me.pMaster.Controls.Add(Me.txtSolicitud)
        Me.pMaster.Controls.Add(Me.Label52)
        Me.pMaster.Controls.Add(Me.Label49)
        Me.pMaster.Controls.Add(Me.Label14)
        Me.pMaster.Controls.Add(Me.txtvehiculo)
        Me.pMaster.Controls.Add(Me.txtRuta)
        Me.pMaster.Controls.Add(Me.Label4)
        Me.pMaster.Controls.Add(Me.txtTasaCambio)
        Me.pMaster.Controls.Add(Me.Label54)
        Me.pMaster.Controls.Add(Me.Label24)
        Me.pMaster.Controls.Add(Me.cbCaja)
        Me.pMaster.Controls.Add(Me.Label19)
        Me.pMaster.Controls.Add(Me.pmasterDeco)
        Me.pMaster.Dock = System.Windows.Forms.DockStyle.Top
        Me.pMaster.Enabled = False
        Me.pMaster.Location = New System.Drawing.Point(0, 0)
        Me.pMaster.Name = "pMaster"
        Me.pMaster.Size = New System.Drawing.Size(1235, 104)
        Me.pMaster.TabIndex = 197
        '
        'lbEndosado
        '
        Me.lbEndosado.AutoSize = True
        Me.lbEndosado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEndosado.ForeColor = System.Drawing.Color.Blue
        Me.lbEndosado.Location = New System.Drawing.Point(1050, 13)
        Me.lbEndosado.Name = "lbEndosado"
        Me.lbEndosado.Size = New System.Drawing.Size(79, 16)
        Me.lbEndosado.TabIndex = 200
        Me.lbEndosado.Text = "Endosado"
        Me.lbEndosado.Visible = False
        '
        'lbVerificado
        '
        Me.lbVerificado.AutoSize = True
        Me.lbVerificado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbVerificado.ForeColor = System.Drawing.Color.Red
        Me.lbVerificado.Location = New System.Drawing.Point(1140, 13)
        Me.lbVerificado.Name = "lbVerificado"
        Me.lbVerificado.Size = New System.Drawing.Size(79, 16)
        Me.lbVerificado.TabIndex = 201
        Me.lbVerificado.Text = "Verificado"
        Me.lbVerificado.Visible = False
        '
        'lbObservacion
        '
        Me.lbObservacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbObservacion.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbObservacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbObservacion.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.lbObservacion.Location = New System.Drawing.Point(1019, 68)
        Me.lbObservacion.Name = "lbObservacion"
        Me.lbObservacion.Size = New System.Drawing.Size(121, 20)
        Me.lbObservacion.TabIndex = 199
        Me.lbObservacion.Text = "**Observacion**"
        '
        'txtObservacion
        '
        Me.txtObservacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Location = New System.Drawing.Point(956, 56)
        Me.txtObservacion.MaxLength = 250
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(250, 45)
        Me.txtObservacion.TabIndex = 169
        '
        'Label34
        '
        Me.Label34.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(329, 57)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(49, 13)
        Me.Label34.TabIndex = 198
        Me.Label34.Text = "Moneda:"
        '
        'cbModenda
        '
        Me.cbModenda.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbModenda.EditValue = "CORDOBA"
        Me.cbModenda.Location = New System.Drawing.Point(384, 54)
        Me.cbModenda.Name = "cbModenda"
        Me.cbModenda.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbModenda.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbModenda.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbModenda.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbModenda.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbModenda.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbModenda.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbModenda.Properties.Items.AddRange(New Object() {"CORDOBA", "DOLAR"})
        Me.cbModenda.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbModenda.Size = New System.Drawing.Size(83, 20)
        Me.cbModenda.TabIndex = 5
        '
        'txtVendedor
        '
        Me.txtVendedor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtVendedor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtVendedor.Location = New System.Drawing.Point(709, 54)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.ReadOnly = True
        Me.txtVendedor.Size = New System.Drawing.Size(208, 20)
        Me.txtVendedor.TabIndex = 149
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(647, 56)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 13)
        Me.Label17.TabIndex = 148
        Me.Label17.Text = "Vendedor:"
        '
        'cbEstado
        '
        Me.cbEstado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbEstado.Enabled = False
        Me.cbEstado.Location = New System.Drawing.Point(384, 80)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.cbEstado.Properties.Appearance.Options.UseForeColor = True
        Me.cbEstado.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbEstado.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbEstado.Properties.AppearanceDisabled.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbEstado.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.cbEstado.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbEstado.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEstado.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbEstado.Properties.Items.AddRange(New Object() {"RESERVADA", "EMITIDA", "IMPRESA", "ANULADA"})
        Me.cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbEstado.Size = New System.Drawing.Size(206, 20)
        Me.cbEstado.TabIndex = 141
        Me.cbEstado.TabStop = False
        '
        'Label53
        '
        Me.Label53.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(335, 83)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(43, 13)
        Me.Label53.TabIndex = 140
        Me.Label53.Text = "Estado:"
        '
        'txtSerie
        '
        Me.txtSerie.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSerie.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Location = New System.Drawing.Point(132, 54)
        Me.txtSerie.MaxLength = 1
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(20, 20)
        Me.txtSerie.TabIndex = 2
        Me.txtSerie.TabStop = False
        Me.txtSerie.Text = "A"
        Me.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSucursal
        '
        Me.txtSucursal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSucursal.BackColor = System.Drawing.SystemColors.Control
        Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSucursal.Location = New System.Drawing.Point(1011, 29)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.ReadOnly = True
        Me.txtSucursal.Size = New System.Drawing.Size(195, 15)
        Me.txtSucursal.TabIndex = 126
        Me.txtSucursal.TabStop = False
        Me.txtSucursal.Text = "ASD"
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(824, 84)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(33, 13)
        Me.Label23.TabIndex = 164
        Me.Label23.Text = "Ruta:"
        '
        'Label50
        '
        Me.Label50.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(481, 31)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(45, 13)
        Me.Label50.TabIndex = 129
        Me.Label50.Text = "N. Caja:"
        '
        'txtFactura
        '
        Me.txtFactura.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtFactura.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtFactura.Location = New System.Drawing.Point(76, 54)
        Me.txtFactura.MaxLength = 8
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.Size = New System.Drawing.Size(56, 20)
        Me.txtFactura.TabIndex = 1
        Me.txtFactura.TabStop = False
        Me.txtFactura.Text = "00000000"
        '
        'txtRemision
        '
        Me.txtRemision.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtRemision.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtRemision.Location = New System.Drawing.Point(76, 28)
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.ReadOnly = True
        Me.txtRemision.Size = New System.Drawing.Size(76, 20)
        Me.txtRemision.TabIndex = 120
        Me.txtRemision.TabStop = False
        '
        'cbvtipo
        '
        Me.cbvtipo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbvtipo.EditValue = "CONTADO"
        Me.cbvtipo.Location = New System.Drawing.Point(384, 28)
        Me.cbvtipo.Name = "cbvtipo"
        Me.cbvtipo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbvtipo.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbvtipo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbvtipo.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbvtipo.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbvtipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbvtipo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbvtipo.Properties.Items.AddRange(New Object() {"CREDITO", "CONTADO", "CREDITO/CONTADO"})
        Me.cbvtipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbvtipo.Size = New System.Drawing.Size(83, 20)
        Me.cbvtipo.TabIndex = 4
        '
        'Label95
        '
        Me.Label95.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(24, 57)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(46, 13)
        Me.Label95.TabIndex = 168
        Me.Label95.Text = "Factura:"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "Remision:"
        '
        'Label51
        '
        Me.Label51.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(30, 83)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(40, 13)
        Me.Label51.TabIndex = 131
        Me.Label51.Text = "Fecha:"
        '
        'txtGarantia
        '
        Me.txtGarantia.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtGarantia.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtGarantia.Location = New System.Drawing.Point(225, 28)
        Me.txtGarantia.Name = "txtGarantia"
        Me.txtGarantia.ReadOnly = True
        Me.txtGarantia.Size = New System.Drawing.Size(46, 20)
        Me.txtGarantia.TabIndex = 155
        Me.txtGarantia.TabStop = False
        Me.txtGarantia.Text = "00000"
        '
        'txtSupervisor
        '
        Me.txtSupervisor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSupervisor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSupervisor.Location = New System.Drawing.Point(709, 28)
        Me.txtSupervisor.Name = "txtSupervisor"
        Me.txtSupervisor.ReadOnly = True
        Me.txtSupervisor.Size = New System.Drawing.Size(208, 20)
        Me.txtSupervisor.TabIndex = 100
        '
        'dtpFechaI
        '
        Me.dtpFechaI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpFechaI.CustomFormat = "dd/MMMM/yyyy"
        Me.dtpFechaI.Location = New System.Drawing.Point(76, 80)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(195, 20)
        Me.dtpFechaI.TabIndex = 3
        '
        'txtSolicitud
        '
        Me.txtSolicitud.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSolicitud.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtSolicitud.Location = New System.Drawing.Point(225, 54)
        Me.txtSolicitud.Name = "txtSolicitud"
        Me.txtSolicitud.ReadOnly = True
        Me.txtSolicitud.Size = New System.Drawing.Size(46, 20)
        Me.txtSolicitud.TabIndex = 128
        Me.txtSolicitud.TabStop = False
        Me.txtSolicitud.Text = "00000"
        '
        'Label52
        '
        Me.Label52.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(643, 31)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(60, 13)
        Me.Label52.TabIndex = 99
        Me.Label52.Text = "Supervisor:"
        '
        'Label49
        '
        Me.Label49.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(175, 57)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(44, 13)
        Me.Label49.TabIndex = 127
        Me.Label49.Text = "Pagare:"
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(481, 57)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 137
        Me.Label14.Text = "Cambio:"
        '
        'txtvehiculo
        '
        Me.txtvehiculo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtvehiculo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtvehiculo.Location = New System.Drawing.Point(709, 81)
        Me.txtvehiculo.Name = "txtvehiculo"
        Me.txtvehiculo.ReadOnly = True
        Me.txtvehiculo.Size = New System.Drawing.Size(96, 20)
        Me.txtvehiculo.TabIndex = 167
        '
        'txtRuta
        '
        Me.txtRuta.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtRuta.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtRuta.Location = New System.Drawing.Point(863, 81)
        Me.txtRuta.Name = "txtRuta"
        Me.txtRuta.ReadOnly = True
        Me.txtRuta.Size = New System.Drawing.Size(54, 20)
        Me.txtRuta.TabIndex = 165
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(169, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 154
        Me.Label4.Text = "Garantia:"
        '
        'txtTasaCambio
        '
        Me.txtTasaCambio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtTasaCambio.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtTasaCambio.Location = New System.Drawing.Point(532, 54)
        Me.txtTasaCambio.Name = "txtTasaCambio"
        Me.txtTasaCambio.ReadOnly = True
        Me.txtTasaCambio.Size = New System.Drawing.Size(60, 20)
        Me.txtTasaCambio.TabIndex = 138
        Me.txtTasaCambio.TabStop = False
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(27, 13)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(105, 13)
        Me.Label54.TabIndex = 196
        Me.Label54.Text = "MASTER FACTURA"
        '
        'Label24
        '
        Me.Label24.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(652, 84)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(51, 13)
        Me.Label24.TabIndex = 166
        Me.Label24.Text = "Vehiculo:"
        '
        'cbCaja
        '
        Me.cbCaja.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbCaja.Location = New System.Drawing.Point(532, 28)
        Me.cbCaja.Name = "cbCaja"
        Me.cbCaja.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.cbCaja.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White
        Me.cbCaja.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbCaja.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.cbCaja.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.cbCaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCaja.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbCaja.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCaja.Size = New System.Drawing.Size(60, 20)
        Me.cbCaja.TabIndex = 6
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(318, 31)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 13)
        Me.Label19.TabIndex = 152
        Me.Label19.Text = "Tipo Venta:"
        '
        'pmasterDeco
        '
        Me.pmasterDeco.AutoSize = True
        Me.pmasterDeco.BackColor = System.Drawing.Color.Transparent
        Me.pmasterDeco.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pmasterDeco.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.pmasterDeco.Location = New System.Drawing.Point(4, -19)
        Me.pmasterDeco.Name = "pmasterDeco"
        Me.pmasterDeco.Size = New System.Drawing.Size(1298, 42)
        Me.pmasterDeco.TabIndex = 195
        Me.pmasterDeco.Text = "________________________________________________________________"
        Me.pmasterDeco.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Column1
        '
        Me.Column1.HeaderText = "Codigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 80
        '
        'Column2
        '
        Me.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column2.HeaderText = "Articulo"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column3.HeaderText = "Cantidad"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 80
        '
        'Column4
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column4.HeaderText = "Costo"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'ColumnCordoba
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.ColumnCordoba.DefaultCellStyle = DataGridViewCellStyle3
        Me.ColumnCordoba.HeaderText = "C$"
        Me.ColumnCordoba.Name = "ColumnCordoba"
        Me.ColumnCordoba.ReadOnly = True
        Me.ColumnCordoba.Width = 80
        '
        'ColumnDolar
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.ColumnDolar.DefaultCellStyle = DataGridViewCellStyle4
        Me.ColumnDolar.HeaderText = "USD"
        Me.ColumnDolar.Name = "ColumnDolar"
        Me.ColumnDolar.ReadOnly = True
        Me.ColumnDolar.Visible = False
        Me.ColumnDolar.Width = 80
        '
        'Column5
        '
        Me.Column5.HeaderText = "Pagare"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Visible = False
        '
        'Column11
        '
        Me.Column11.HeaderText = "Garantia"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.HeaderText = "Precio Facturacion"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        '
        'Column13
        '
        Me.Column13.HeaderText = "Id_Solicitud_Facturacion"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Visible = False
        '
        'txtctelefono
        '
        Me.txtctelefono.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtctelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtctelefono.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtctelefono.Location = New System.Drawing.Point(563, 50)
        Me.txtctelefono.Name = "txtctelefono"
        Me.txtctelefono.Size = New System.Drawing.Size(159, 20)
        Me.txtctelefono.TabIndex = 202
        '
        'Label39
        '
        Me.Label39.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(507, 53)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(52, 13)
        Me.Label39.TabIndex = 201
        Me.Label39.Text = "Telefono:"
        '
        'luBarrio
        '
        Me.luBarrio.Location = New System.Drawing.Point(1006, 24)
        Me.luBarrio.Name = "luBarrio"
        Me.luBarrio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.luBarrio.Size = New System.Drawing.Size(82, 20)
        Me.luBarrio.TabIndex = 206
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(953, 27)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(44, 13)
        Me.Label15.TabIndex = 207
        Me.Label15.Text = "Cartera:"
        '
        'FrmFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1235, 586)
        Me.Controls.Add(Me.pMain)
        Me.Name = "FrmFactura"
        Me.pMain.ResumeLayout(False)
        Me.Pdetalle.ResumeLayout(False)
        Me.Pdetalle.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbTotal.ResumeLayout(False)
        Me.gbTotal.PerformLayout()
        Me.Ppago.ResumeLayout(False)
        Me.Ppago.PerformLayout()
        CType(Me.dgvPago, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cbModalidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pcliente.ResumeLayout(False)
        Me.Pcliente.PerformLayout()
        CType(Me.cbtipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMaster.ResumeLayout(False)
        Me.pMaster.PerformLayout()
        CType(Me.cbModenda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbvtipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.luBarrio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents Pcliente As System.Windows.Forms.Panel
    Friend WithEvents pMaster As System.Windows.Forms.Panel
    Friend WithEvents txtGarantia As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCcartera As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtVendedor As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cbCaja As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbEstado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents cbvtipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtTasaCambio As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtRemision As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSupervisor As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents pmasterDeco As System.Windows.Forms.Label
    Friend WithEvents txtvehiculo As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtRuta As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cbtipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtSolicitud As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtCcodigo As System.Windows.Forms.TextBox
    Friend WithEvents txtCcedula As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtCnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtCapellido As System.Windows.Forms.TextBox
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents PdetalleDeco As System.Windows.Forms.Label
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents txtSerie As System.Windows.Forms.TextBox
    Friend WithEvents txtFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents pClienteDeco As System.Windows.Forms.Label
    Friend WithEvents gbTotal As System.Windows.Forms.GroupBox
    Friend WithEvents txtTdescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtntpagar As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtTprecio As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtTcosto As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtTapagar As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTcolocacion As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents txtTneto As System.Windows.Forms.TextBox
    Friend WithEvents ckbIVA As System.Windows.Forms.CheckBox
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents txtFinancimiento As System.Windows.Forms.TextBox
    Friend WithEvents ckbIR As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ckbIMI As System.Windows.Forms.CheckBox
    Friend WithEvents txtIMI As System.Windows.Forms.TextBox
    Friend WithEvents txtIR As System.Windows.Forms.TextBox
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Pdetalle As System.Windows.Forms.Panel
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents cbModenda As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lbObservacion As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Ppago As System.Windows.Forms.Panel
    Friend WithEvents dgvPago As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnPmodificar As System.Windows.Forms.Button
    Friend WithEvents dtpCuota2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents dtpCuota As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtPlazo As System.Windows.Forms.TextBox
    Friend WithEvents txtMontoFinanciar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPrima As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbModalidad As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents dtpPrima As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtInteres As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtCuota As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents pPagoDeco As System.Windows.Forms.Label
    Friend WithEvents txtcdireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents txtPago As System.Windows.Forms.TextBox
    Friend WithEvents btnPprint As System.Windows.Forms.Button
    Friend WithEvents lbEndosado As System.Windows.Forms.Label
    Friend WithEvents lbVerificado As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnCordoba As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDolar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtctelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents luBarrio As DevExpress.XtraEditors.LookUpEdit
End Class
