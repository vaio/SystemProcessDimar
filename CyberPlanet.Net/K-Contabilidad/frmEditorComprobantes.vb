﻿Public Class frmEditorComprobantes

    Private Sub frmEditorComprobantes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            LimpiarCampos()
            CargarTipoComprob()
            CargarDetalleComprob(CInt(IIf(txtNumComprob.Text = "", 0, txtNumComprob.Text)), luTipoComprob.EditValue, dtpFechaComprob.Value)
            DesactivarCampos(True)
            luTipoComprob.Focus()
    End Sub

    Private Sub frmEditorComprobantes_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave
        NumCatalogo = 0
    End Sub

    Sub LimpiarCampos()
          luTipoComprob.ItemIndex = 1
          txtNumComprob.Text = ""
          dtpFechaComprob.Value = Now.Date
          txtMonto.Text = Format(0, "###,###,###.00")
          txtConceptoComprob.Text = ""
    End Sub

    Sub CargarTipoComprob()
        Tbl_TipoComprobBS.DataSource = SQL("Select Id_TipoComprobante,Tipo_Comprobante from Tbl_TipoComprobante order by Id_TipoComprobante", "tblTipoComprob", My.Settings.SolIndustrialCNX).Tables(0)
        luTipoComprob.Properties.DataSource = Tbl_TipoComprobBS
        luTipoComprob.Properties.DisplayMember = "Tipo_Comprobante"
        luTipoComprob.Properties.ValueMember = "Id_TipoComprobante"
        luTipoComprob.ItemIndex = 1
    End Sub

    Sub DesactivarCampos(ByVal bIsActivo As Boolean)
          luTipoComprob.Enabled = Not bIsActivo
          txtNumComprob.ReadOnly = bIsActivo
          dtpFechaComprob.Enabled = Not bIsActivo
          'txtMonto.ReadOnly = bIsActivo
          txtConceptoComprob.ReadOnly = bIsActivo
    End Sub

    Sub CargarDetalleComprob(ByVal nNumComprob As Integer, ByVal nTipoComprob As Integer, ByVal dFecha As Date)
        Dim strSqlComprob As String
        strSqlComprob = "Select * from Tbl_MasterComprobantes where Num_Comprobante = " & nNumComprob & " and Id_TipoComprobante = " & nTipoComprob & " and Fecha = '" & Format(dFecha, "yyyy/MM/dd") & "'"
        Dim tblComprob As DataTable = SQL(strSqlComprob, "tblComprob", My.Settings.SolIndustrialCNX).Tables(0)

        If tblComprob.Rows.Count > 0 Then
              luTipoComprob.EditValue = tblComprob.Rows(0).Item(1)
              txtNumComprob.Text = tblComprob.Rows(0).Item(0)
              dtpFechaComprob.Value = tblComprob.Rows(0).Item(2)
              txtMonto.Text = tblComprob.Rows(0).Item(7)
              txtConceptoComprob.Text = tblComprob.Rows(0).Item(6)

              Dim StrsqlDetComprob As String = "Select [Cuenta Contable],Nombre_Cuenta,Id_CentroCosto,[Centro Costo],Tasa_Oficial,Debito,Credito from vw_DetalleComprobante where "

              StrsqlDetComprob = StrsqlDetComprob & " Num_Comprobante = " & nNumComprob & " and "
              StrsqlDetComprob = StrsqlDetComprob & " Id_TipoComprobante = " & nTipoComprob & " "
              StrsqlDetComprob = StrsqlDetComprob & " and Fecha = '" & Format(dFecha, "yyyy/MM/dd") & "'"
              'StrsqlDetDoc = StrsqlDetDoc & " order by Documento desc"

              Tbl_DetalleComprobBS.DataSource = SQL(StrsqlDetComprob, "tblDetalleComprob", My.Settings.SolIndustrialCNX).Tables(0)
              'EncabezadosDetDoc() 
              If Tbl_DetalleComprobBS.Count <= 0 Then
                  btnAgregar.Enabled = True
                  btnModificar.Enabled = False
                  btnEliminar.Enabled = False
              Else
                  btnAgregar.Enabled = True
                  btnModificar.Enabled = True
                  btnEliminar.Enabled = True
              End If
        Else

            Dim StrsqlDetComprob As String = "Select [Cuenta Contable],Nombre_Cuenta,Id_CentroCosto,[Centro Costo],Tasa_Oficial,Debito,Credito from vw_DetalleComprobante where Num_Comprobante = 0 "
            Tbl_DetalleComprobBS.DataSource = SQL(StrsqlDetComprob, "tblDetalleComprob", My.Settings.SolIndustrialCNX).Tables(0)

            btnAgregar.Enabled = False
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
          frmPrincipal.bbiNuevo.Enabled = bEstado
          frmPrincipal.bbiModificar.Enabled = bEstado
          frmPrincipal.bbiEliminar.Enabled = bEstado
          frmPrincipal.bbiGuardar.Enabled = Not bEstado
          frmPrincipal.bbiCancelar.Enabled = Not bEstado
          frmPrincipal.bbiBuscar.Enabled = bEstado
          frmPrincipal.bbiExportar.Enabled = bEstado
    End Sub

    Function SiguienteComprob()
          Dim strsqlSigComprob As String
          SiguienteComprob = 0
          strsqlSigComprob = "Select max(Num_Comprobante)+1 as maximo from Tbl_MasterComprobantes where Id_TipoComprobante = " & luTipoComprob.EditValue 'IIf(luTipoComprob.EditValue = "", 0, luTipoComprob.EditValue)
          Dim dtCatalogoCuenta As DataTable = SQL(strsqlSigComprob, "tblSigCuenta", My.Settings.SolIndustrialCNX).Tables(0)
          SiguienteComprob = dtCatalogoCuenta.Rows(0).Item("maximo")
    End Function

    Function Nuevo()
          PermitirBotonesEdicion(False)
          DesactivarCampos(False)
          LimpiarCampos()
          txtNumComprob.Text = IIf(IsDBNull(SiguienteComprob()), 1, SiguienteComprob())
          luTipoComprob.Focus()
    End Function

    Function Modificar()
          PermitirBotonesEdicion(False)
          luTipoComprob.Focus()
    End Function

    Function Eliminar()
          DesactivarCampos(True)
          PermitirBotonesEdicion(False)
    End Function

    Function Guardar()
          PermitirBotonesEdicion(True)
          DesactivarCampos(True)
          GuardarComprobante()
          nTipoEdic = 0
    End Function

    Function Cancelar()
          LimpiarCampos()
          nTipoEdic = 0
          DesactivarCampos(True)
          PermitirBotonesEdicion(True)
    End Function

    Function Buscar()
          frmBusquedaDoc.ShowDialog()
    End Function

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Sub GuardarComprobante()
        Dim Resum As String = ""
        Dim MasterComprob As New clsContabilidad(My.Settings.SolIndustrialCNX)

        Try
            Resum = MasterComprob.EditarMasterComprobDiario(CInt(txtNumComprob.Text), luTipoComprob.EditValue, dtpFechaComprob.Value, txtConceptoComprob.Text.ToUpper, CDbl(txtMonto.Text), 0, 0, 0, nTipoEdic)
            If Resum = "OK" Then
                If nTipoEdic = 3 Then
                    LimpiarCampos()
                End If
                'MsgBox("La operación solicitada se realizó de forma exitosa", MsgBoxStyle.Information, "CyberPlanet.Net")
                CargarDetalleComprob(txtNumComprob.Text, luTipoComprob.EditValue, dtpFechaComprob.Value)
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        '    Cerrar()
            If NumCatalogo_Ant = 0 Then
            End If
        End Try
    End Sub

    'Sub CargarEdicionComprobante()
    '      LimpiarCampos()

    '      If TblCatalogoBindingSource.Count > 0 Then
    '            Dim registro As DataRowView = TblCatalogoBindingSource.Current
    '            cmbNivel.Text = registro.Item("Nivel")
    '            If cmbNivel.Text = 1 Then
    '                  txtCuentaEdit.Text = registro.Item("Cuenta")
    '            ElseIf cmbNivel.Text = 2 Then
    '                  txtCuentaEdit.Text = registro.Item("SCuenta")
    '            ElseIf cmbNivel.Text = 3 Then
    '                  txtCuentaEdit.Text = registro.Item("SSCuenta")
    '            ElseIf cmbNivel.Text = 4 Then
    '                  txtCuentaEdit.Text = registro.Item("SSSCuenta")
    '            ElseIf cmbNivel.Text = 5 Then
    '                  txtCuentaEdit.Text = registro.Item("SSSSCuenta")
    '            ElseIf cmbNivel.Text = 6 Then
    '                  txtCuentaEdit.Text = registro.Item("SSSSSCuenta")
    '            End If
    '            txtNombreCuenta.Text = registro.Item("Nombre_Cuenta")
    '            txtDescripcionCuenta.Text = ""
    '            cmbTipoCuenta.SelectedIndex = (registro.Item("Tipo_Cuenta") - 1)
    '            cmbTipoSaldo.SelectedIndex = (registro.Item("Tipo_Saldo") - 1)
    '            chkEsDetalle.Checked = registro.Item("IsDetalle")
    '            chkEsDeshabilitada.Checked = False
    '      End If
    'End Sub

    Private Sub luTipoComprob_EditValueChanged(sender As Object, e As EventArgs) Handles luTipoComprob.EditValueChanged
        If nTipoEdic = 1 Then
          txtNumComprob.Text = IIf(IsDBNull(SiguienteComprob()), 1, SiguienteComprob())
        End If
    End Sub

    Private Sub txtDiferencia_Leave(sender As Object, e As EventArgs) Handles txtDiferencia.Leave
      luTipoComprob.Focus()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
          nTipoEdicDet = 1
          frmDetalleComprob.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
          nTipoEdicDet = 2
          frmDetalleComprob.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
            nTipoEdicDet = 3
          frmDetalleComprob.ShowDialog()
    End Sub

End Class
