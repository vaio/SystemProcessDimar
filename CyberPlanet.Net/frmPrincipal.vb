Imports DevExpress.Data.Filtering
Imports DevExpress.XtraGrid.Columns
Imports System.Globalization
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Threading
Imports SIGMA.Servicio
Imports System.Xml
'--------------------------------------------------------------
Public Class frmPrincipal
    Dim strsql As String = ""
    Dim adapter As SqlDataAdapter
    'Dim ShowResume As Boolean = True
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
#Region "Procedimientos del Formulario"
    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        Me.bbiNuevo.Enabled = bEstado
        Me.bbiModificar.Enabled = bEstado
        Me.bbiEliminar.Enabled = bEstado
        Me.bbiGuardar.Enabled = Not bEstado
        Me.bbiCancelar.Enabled = Not bEstado
        Me.bbiBuscar.Enabled = bEstado
        Me.bbiExportar.Enabled = bEstado
    End Sub
    Private Sub frmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        nbcNotificacion.OptionsNavPane.NavPaneState = DevExpress.XtraNavBar.NavPaneState.Expanded
        nbcNotificacion.OptionsNavPane.NavPaneState = DevExpress.XtraNavBar.NavPaneState.Collapsed
        Me.BackgroundImage = My.Resources.logo
        'CargarMesAnioDia()
        'btnShowResume.Image = Image.FromFile("C:\Worksystem\btn_On_Lit.png")
        'ShowResume = True
        'CargarLogo()
        '-------ojo
        Tasa_Cambio()
        LblNombreUsuario.Text = UserFullName
        lblSucursal.Text = SucursalName

        'Timer1.Enabled = True
        'SaludoalUsuario()
        Dim newCulture As CultureInfo = DirectCast(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
        newCulture.DateTimeFormat.DateSeparator = "/"
        Thread.CurrentThread.CurrentCulture = newCulture
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        If IO.Directory.Exists(sPath) Then
            'MsgBox("Existe")
            IO.Directory.Delete(sPath, True)
        Else
            'MsgBox("No existe")
        End If
        'Tasa_Cambio()
        'Me.TopMost = True 'no toma en cuenta la taskbar al ajustar su tama�o
    End Sub


    Sub Tasa_Cambio()

        Dim fecha As Date = Today
        Dim tasa As Decimal = 0
        Dim actualizada As Boolean = False

        TSProceso.Visible = False
        tsslbProceso.Visible = False
        TSProgressBar1.Visible = False

        'If fecha.Day > 20 Then
        If fecha.Day > 0 Then


            TSProceso.Visible = True
            tsslbProceso.Visible = True
            tsslbProceso.Text = "Consultando T/C BANCO CENTRAL"


            'fecha = fecha.AddMonths(1)

            Dim dt As DataTable = New DataTable()

            strsql = "select Tasa_Oficial from Tbl_TasasCambio where Fecha='" + Convert.ToDateTime(fecha) + "'"
            cmd = New SqlCommand(strsql, con)
            Try

                TSProceso.Visible = True
                tsslbProceso.Visible = True
                tsslbProceso.Text = "Consultando T/C BANCO CENTRAL"

                con.Open()
                adapter = New SqlDataAdapter(cmd)
                adapter.Fill(dt)

                For Each row0 In dt.Rows

                    actualizada = True

                Next

                con.Close()
                dt.Rows.Clear()

            Catch ex As Exception

                MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)

                con.Close()

            End Try



            'nuevo-----------------------------------------------
            'If actualizada = False Then

            '    Try

            '        Dim objserv As New Servicio.Tipo_Cambio_BCNSoapClient


            '        tasa = objserv.RecuperaTC_Dia(fecha.Year, fecha.Month, 1)

            '        If tasa = 0 Then

            '            TSProceso.Visible = True
            '            tsslbProceso.Visible = True
            '            tsslbProceso.Text = "BANCO CENTRAL: T/C " + fecha + " no disponible"
            '            Exit Sub
            '        End If

            '    Catch ex As Exception
            '        MsgBox(ex.Message)
            '    End Try

            'End If
            'fin nuevo------------------------------------------------------------



            'strsql = "select Tasa_Oficial from Tbl_TasasCambio where Fecha='" + Convert.ToDateTime(fecha) + "'"
            'cmd = New SqlCommand(strsql, con)
            'Try
            '    con.Open()
            '    adapter = New SqlDataAdapter(cmd)
            '    adapter.Fill(dt)
            '    For Each row0 In dt.Rows
            '        actualizada = True
            '    Next
            '    con.Close()
            '    dt.Rows.Clear()
            'Catch ex As Exception
            '    MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            '    con.Close()
            'End Try






            If actualizada = False Then
                Select Case MsgBox("Tasa de Cambio desactualizada." & vbNewLine & "Actualizar tasa de cambio", MsgBoxStyle.YesNo, "")
                    Case MsgBoxResult.Yes
                        TSProceso.Visible = True
                        tsslbProceso.Visible = True
                        TSProgressBar1.Visible = True
                        tsslbProceso.Text = "ACTUALIZANDO T/C"
                        For i = 1 To Date.DaysInMonth(fecha.Year, fecha.Month)
                            fecha = New Date(fecha.Year, fecha.Month, i)
                            Try
                                '------------------------
                                Dim objServ As New Servicio.Tipo_Cambio_BCNSoapClient
                                tasa = objServ.RecuperaTC_Dia(fecha.Year, fecha.Month, i)
                                If tasa = 0 Then MsgBox("Tasa")
                                con.Open()
                                Dim cmd As New SqlCommand("SP_Tasa", con)
                                cmd.CommandType = CommandType.StoredProcedure
                                With cmd.Parameters
                                    .AddWithValue("@fecha", fecha)
                                    .AddWithValue("@tasa", tasa)
                                End With
                                cmd.ExecuteReader()
                                con.Close()
                            Catch ex As Exception
                                TSProgressBar1.Value = 0
                                MsgBox("Error: " + ex.Message)
                                con.Close()
                                Exit For
                                '-----------------------------------------------
                            End Try
                            TSProgressBar1.Value = (i * 100) / Date.DaysInMonth(fecha.Year, fecha.Month)
                        Next
                        TSProgressBar1.Value = 0
                        TSProceso.Visible = False
                        tsslbProceso.Visible = False
                        MsgBox("Proceso Finalizado")
                End Select
            End If
            TSProceso.Visible = False
            tsslbProceso.Visible = False
            TSProgressBar1.Visible = False
        End If
    End Sub

    Sub CargarLogo()
        'peLogoCompany.Left = Me.Width - peLogoCompany.Width - 15
        'pbBanner1.Top = Panel3.Height - 8
        'pbBanner1.Width = Panel3.Width
        'pbBanner1.Left = Panel3.Width - pbBanner1.Width
    End Sub

    Sub SaludoalUsuario()
        Dim voz As Object = CreateObject("SAPI.SpVoice")
        Dim tHora As DateTime = Now.ToShortTimeString
        'If tHora>

        If Now.Hour >= 5 And Now.Hour < 12 Then
            voz.Speak("BUENOS DIAS")
        ElseIf Now.Hour >= 12 And Now.Hour < 19 Then
            voz.Speak("BUENAS TARDES")
        ElseIf Now.Hour >= 19 And Now.Hour < 24 Then
            voz.Speak("Buenos Noches,, ")
        ElseIf Now.Hour >= 1 And Now.Hour < 5 Then
            voz.Speak("Buenos Noches,, ")
        End If

        voz.Speak("Bienveh needo ah Sif WorkSystem")

        'voz.Speak("Gracias por su veesee ta ,,  ")
        'voz.Speak("Su tee empoh ah feena lee zahdo ,,  ")
        'voz.Speak("Bienveh needo ah CiberPlahnet")
        'voz.Speak("Bienveh needo ah CiberPlahnet")
    End Sub

    Sub CargarTitulo()
        If NumCatalogo = 1 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Almacenes"
        ElseIf NumCatalogo = 2 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Marcas"
        ElseIf NumCatalogo = 3 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Modelos"
        ElseIf NumCatalogo = 4 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Lineas de Productos"
        ElseIf NumCatalogo = 5 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Categorias"
        ElseIf NumCatalogo = 6 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Proveedor"
        ElseIf NumCatalogo = 7 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Unidades de Medida"
        ElseIf NumCatalogo = 8 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Presentaci�n"
        ElseIf NumCatalogo = 9 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Producto / Servicios"
        ElseIf NumCatalogo = 10 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Departamentos"
        ElseIf NumCatalogo = 11 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Municipios"
        ElseIf NumCatalogo = 12 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Zonas/Distritos"
        ElseIf NumCatalogo = 13 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Barrios"
        ElseIf NumCatalogo = 14 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Clientes"
        ElseIf NumCatalogo = 15 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Suministros"
        ElseIf NumCatalogo = 16 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Activos Fijos"
        ElseIf NumCatalogo = 17 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Colaboradores"
        ElseIf NumCatalogo = 18 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Tipos de Gastos"
        ElseIf NumCatalogo = 19 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Terminales"
        ElseIf NumCatalogo = 20 Then
            frmCatalogo.LblTitulo.Text = "Catalogo de Recetas"
        ElseIf NumCatalogo = 25 Then
            frmCatalogo.LblTitulo.Text = "Entradas de Inventario"
        ElseIf NumCatalogo = 26 Then
            frmCatalogo.LblTitulo.Text = "Salidas de Inventario"
        ElseIf NumCatalogo = 28 Then
            frmCatalogo.LblTitulo.Text = "Solicitud de Pagos"
            'ElseIf NumCatalogo = 29 Then
            '    frmCatalogo.LblTitulo.Text = "Costos Indirectos de Fabricaci�n"
        ElseIf NumCatalogo = 34 Then
            frmCatalogo.LblTitulo.Text = "Verificacion de Clientes"
        End If
    End Sub

    Sub CargarDatos()
        If NumCatalogo = 1 Then
            strsql = "Select * from vwVerAlmacenes"
        ElseIf NumCatalogo = 2 Then
            strsql = "Select * from vwVerMarcas"
        ElseIf NumCatalogo = 3 Then
            strsql = "Select * from vwVerModelos"
        ElseIf NumCatalogo = 4 Then
            strsql = "Select * from vwVerLineasProd"
        ElseIf NumCatalogo = 5 Then
            strsql = "Select * from vwVerCategorias"
        ElseIf NumCatalogo = 6 Then
            strsql = "Select * from vwVerProveedores"
        ElseIf NumCatalogo = 7 Then
            strsql = "Select * from vwVerUnidMed"
        ElseIf NumCatalogo = 8 Then
            strsql = "Select * from vwVerPresent"
        ElseIf NumCatalogo = 9 Then
            strsql = "select * from viewPrueba"
        ElseIf NumCatalogo = 10 Then
            strsql = "select catalogo.Departamento.Id_Departamento As Codigo,catalogo.Departamento.Nombre as 'Departamento' from Catalogo.Departamento "
        ElseIf NumCatalogo = 11 Then
            strsql = "select Tbl_Barrios.Nombre_Barrio as 'Barrio', Tbl_Zonas_Distritos.Nombre_Zona_Distrito as 'Distrito', Tbl_Municipio.Nombre_Municipio,catalogo.Departamento.Nombre as 'Departamento' from Catalogo.Departamento right join Tbl_Municipio on Tbl_Municipio.Id_Departamento=Catalogo.Departamento.Id_Departamento  right join Tbl_Zonas_Distritos on Tbl_Zonas_Distritos.Id_Municipio=Tbl_Municipio.Id_Municipio right join Tbl_Barrios on Tbl_Barrios.Id_Zona_Distrito=Tbl_Zonas_Distritos.Id_Zona_Distrito"
        ElseIf NumCatalogo = 12 Then
            strsql = "select Tbl_Barrios.Nombre_Barrio as 'Barrio', Tbl_Zonas_Distritos.Nombre_Zona_Distrito as 'Distrito', Tbl_Municipio.Nombre_Municipio,catalogo.Departamento.Nombre as 'Departamento' from Catalogo.Departamento right join Tbl_Municipio on Tbl_Municipio.Id_Departamento=Catalogo.Departamento.Id_Departamento  right join Tbl_Zonas_Distritos on Tbl_Zonas_Distritos.Id_Municipio=Tbl_Municipio.Id_Municipio right join Tbl_Barrios on Tbl_Barrios.Id_Zona_Distrito=Tbl_Zonas_Distritos.Id_Zona_Distrito"
        ElseIf NumCatalogo = 13 Then
            strsql = "select Tbl_Barrios.Nombre_Barrio as 'Barrio', Tbl_Zonas_Distritos.Nombre_Zona_Distrito as 'Distrito', Tbl_Municipio.Nombre_Municipio,catalogo.Departamento.Nombre as 'Departamento' from Catalogo.Departamento right join Tbl_Municipio on Tbl_Municipio.Id_Departamento=Catalogo.Departamento.Id_Departamento  right join Tbl_Zonas_Distritos on Tbl_Zonas_Distritos.Id_Municipio=Tbl_Municipio.Id_Municipio right join Tbl_Barrios on Tbl_Barrios.Id_Zona_Distrito=Tbl_Zonas_Distritos.Id_Zona_Distrito"
        ElseIf NumCatalogo = 14 Then
            strsql = "select Codigo, Cedula, Nombre+' '+Apellido as 'Nombre', [Tel Personal 1], [Direccion Domiciliar 1], [Correo Personal 1], Departamento, Municipio, Distrito, Barrio, Tipo,  Activo from vwVerClientes"
        ElseIf NumCatalogo = 15 Then
            strsql = "Select * from vwSuministros"
        ElseIf NumCatalogo = 16 Then
            strsql = "Select * from vwActivosFijos"
        ElseIf NumCatalogo = 17 Then
            strsql = "Select * from vwColaboradores"
        ElseIf NumCatalogo = 18 Then
            strsql = "Select * from vwTipoOperacion"
        ElseIf NumCatalogo = 19 Then
            strsql = "Select * from vw_Terminales"
        ElseIf NumCatalogo = 20 Then
            strsql = "Select * from vwRecetas"
        ElseIf NumCatalogo = 25 Then
            strsql = "Select * from vwVerEntradasInventario"
        ElseIf NumCatalogo = 26 Then
            strsql = "Select * from vwSalidasInventario"
        ElseIf NumCatalogo = 28 Then
            strsql = "Select * from vwVerPagos order by Codigo"
        ElseIf NumCatalogo = 34 Then
            strsql = "select * from vw_TransUnion_Catalogo where Fecha=(select cast(GETDATE() as date) )"
            'strsql = "select CR.Id_Cliente as 'Codigo' , cast(cr.Fecha as date) as 'Fecha', CAST(CR.Fecha AS time) AS 'Hora'  , cr.Nombre , cr.Apellido , cr.Cedula , isnull(ALM.Nombre_Bodega, 'PENDIENTE') as 'Ruta' , BAR.Nombre_Barrio as 'Barrio' , cr.Observacion , (case when cr.Dimar=1 then 'SI' when cr.Dimar=0 then '' end) as 'Dimar' , (case when cr.Inficsa=1 then 'SI' when cr.Inficsa=0 then '' end) as 'Inficsa' , (case when Estado=0 then 'PENDIENTE' when Estado=1 then 'NO APROBADO' when Estado=2 then 'APROBADO' end) as 'Estado' from Tbl_TransUnion CR left join Almacenes ALM on ALM.Codigo_Bodega=cr.Id_Ruta left join Tbl_Barrios BAR on BAR.Id_Barrio=CR.Id_Barrio where cast(cr.Fecha as date)=(select cast(getdate() as date))"
            'strsql = "select CR.Id_Cliente as 'Codigo' , cast(cr.Fecha as date) as 'Fecha', CAST(CR.Fecha AS time) AS 'Hora'  , cr.Nombre , cr.Apellido , cr.Cedula , isnull(ALM.Nombre_Bodega, 'PENDIENTE') as 'Ruta' , BAR.Nombre_Barrio as 'Barrio' , cr.Observacion , (case when cr.Dimar=1 then 'SI' when cr.Dimar=0 then '' end) as 'Dimar' , (case when cr.Inficsa=1 then 'SI' when cr.Inficsa=0 then '' end) as 'Inficsa' , (case when Estado=0 then 'PENDIENTE' when Estado=1 then 'NO APROBADO' when Estado=2 then 'APROBADO' end) as 'Estado' from Tbl_TransUnion CR left join Almacenes ALM on ALM.Codigo_Bodega=cr.Id_Ruta left join Tbl_Barrios BAR on BAR.Id_Barrio=CR.Id_Barrio"
        End If
        frmCatalogo.GridView1.BestFitColumns()
        Dim criterio As String = frmCatalogo.GridView1.ActiveFilterString
        'pendiente restablecer text de filtro aplicado

        Dim tblDatosCatalogo As DataTable = Nothing
        frmCatalogo.GridView1.Columns.Clear()
        frmCatalogo.TblCatalogosBS.DataSource = SQL(strsql, "tblDatosCatalogo", My.Settings.SolIndustrialCNX).Tables(0)

        frmCatalogo.GridView1.ActiveFilterString = criterio

        If NumCatalogo = 9 Then
            frmCatalogo.encabezadoProducto()
        ElseIf NumCatalogo = 16 Then
            frmCatalogo.EncabezadosDetDoc()
        ElseIf NumCatalogo = 28 Then

            With frmCatalogo.GridView1


                .Columns("Vigente").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("Vigente").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

                .Columns("1-30").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("1-30").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

                .Columns("31-60").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("31-60").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

                .Columns("61-90").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("61-90").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

                .Columns("91-120").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("91-120").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

                .Columns("121 - -").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("121 - -").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

            End With
        End If

    End Sub

    'Sub CargarResumenTrans()
    '    Dim strsqlResume As String
    '    'strsqlResume = "SELECT Tipo_Transaccion,count(*) as 'N�', (SUM(Monto)*Signo)  AS Total FROM dbo.Tbl_Transacciones WHERE (Anulado = 0) and "
    '    strsqlResume = "SELECT case when Tipo_Transaccion=1 then 'Venta de Contado' else case when Tipo_Transaccion=2 then 'Venta de Credito' else case when Tipo_Transaccion=3 then 'Compra de Contado' else case when Tipo_Transaccion=4 then 'Compra de Credito' else case when Tipo_Transaccion=5 then 'Pago de Gastos Operacionales' else case when Tipo_Transaccion=6 then 'Recuperacion de CxC' else case when Tipo_Transaccion=7 then 'Pago de Deudas' else case when Tipo_Transaccion=8 then 'Compra de Suministros' else 'Compra de Activos Fijos' end end end end end end end end  as 'Operacion' ,count(*) as 'N�', (SUM(Monto)*Signo)  AS Total FROM dbo.Tbl_Transacciones WHERE (Anulado = 0) and "
    '    If cmbTipoVista.SelectedIndex = 0 Then
    '        strsqlResume = strsqlResume & " Fecha = '" & Format(dtpDias.Value, "yyyy/MM/dd") & "' "
    '    ElseIf cmbTipoVista.SelectedIndex = 1 Then
    '        strsqlResume = strsqlResume & " month(Fecha) = " & (cmbMeses.SelectedIndex + 1) & " and year(Fecha) = " & CInt(cmbAnio.Text) & " "
    '    End If
    '    strsqlResume = strsqlResume & " GROUP BY Tipo_Transaccion, Signo "
    '    Me.GridView1.Columns.Clear()
    '    Me.Tbl_ResumenTransBS.DataSource = SQL(strsqlResume, "tblResumenTrans", My.Settings.SolIndustrialCNX).Tables(0)
    '    EncabezadosResumenTrans()
    'End Sub

    Sub MostrarCatalogos()
        frmCatalogo.MdiParent = Me
        frmCatalogo.WindowState = FormWindowState.Maximized
        frmCatalogo.Show()
        bbiExportar.Enabled = True
    End Sub

    'Para generar el grafico
    Sub CargarContabilidad()
        'Dim StrsqlConta As String = ""
        'StrsqlConta = "SELECT CASE WHEN Tbl_Transacciones.Tipo_Transaccion = 1 OR Tbl_Transacciones.Tipo_Transaccion = 6 THEN 'INGRESOS' ELSE 'EGRESOS' END AS Tipo, (SUM(Tbl_Transacciones.Monto) * Tbl_Transacciones.Signo) AS Montos "
        'StrsqlConta = StrsqlConta & " FROM Tbl_Transacciones INNER JOIN Tbl_Proveedor ON Tbl_Transacciones.Codigo_Proveedor = Tbl_ProveedoMaster_Facturasr.Codigo_Proveedor INNER JOIN Tbl_Clientes ON Tbl_Transacciones.Id_Cliente = Tbl_Clientes.Id_Cliente "
        'StrsqlConta = StrsqlConta & " WHERE (Tbl_Transacciones.Anulado = 0) and "
        'If cmbTipoVista.SelectedIndex = 0 Then
        '    StrsqlConta = StrsqlConta & " Fecha = '" & Format(dtpDias.Value, "yyyy/MM/dd") & "' "
        'ElseIf cmbTipoVista.SelectedIndex = 1 Then
        '    StrsqlConta = StrsqlConta & " month(Fecha) = " & (cmbMeses.SelectedIndex + 1) & " and year(Fecha) = " & CInt(cmbAnio.Text) & " "
        'End If
        'StrsqlConta = StrsqlConta & " GROUP BY CASE WHEN Tbl_Transacciones.Tipo_Transaccion = 1 OR Tbl_Transacciones.Tipo_Transaccion = 6 THEN 'INGRESOS' ELSE 'EGRESOS' END, Tbl_Transacciones.Signo "
        'StrsqlConta = StrsqlConta & " ORDER BY Tipo DESC "

        'StrsqlConta = "SELECT Tipo_Venta AS Sucursal, CASE WHEN month(fecha) = 1 THEN SUM(SubTotal_Venta) ELSE 0 END AS Enero, CASE WHEN month(fecha) = 2 THEN SUM(SubTotal_Venta) "
        'StrsqlConta = StrsqlConta & " ELSE 0 END AS Febrero, CASE WHEN month(fecha) = 3 THEN SUM(SubTotal_Venta) ELSE 0 END AS Marzo, CASE WHEN month(fecha) = 4 THEN SUM(SubTotal_Venta) ELSE 0 END AS Abril, "
        'StrsqlConta = StrsqlConta & " CASE WHEN month(fecha) = 5 THEN SUM(SubTotal_Venta) ELSE 0 END AS Mayo, CASE WHEN month(fecha) = 6 THEN SUM(SubTotal_Venta) ELSE 0 END AS Junio, CASE WHEN month(fecha) "
        'StrsqlConta = StrsqlConta & " = 7 THEN SUM(SubTotal_Venta) ELSE 0 END AS Julio, CASE WHEN month(fecha) = 8 THEN SUM(SubTotal_Venta) ELSE 0 END AS Agosto, CASE WHEN month(fecha) = 9 THEN SUM(SubTotal_Venta) "
        'StrsqlConta = StrsqlConta & " ELSE 0 END AS Septiembre, CASE WHEN month(fecha) = 10 THEN SUM(SubTotal_Venta) ELSE 0 END AS Octubre, CASE WHEN month(fecha) = 11 THEN SUM(SubTotal_Venta) "
        'StrsqlConta = StrsqlConta & " ELSE 0 END AS Noviembre, CASE WHEN month(fecha) = 12 THEN SUM(SubTotal_Venta) ELSE 0 END AS Diciembre"
        'StrsqlConta = StrsqlConta & " FROM dbo.Facturas "
        ''StrsqlConta = StrsqlConta & " WHERE (Tbl_Transacciones.Anulada = 0) and "
        'If cmbTipoVista.SelectedIndex = 0 Then
        '    StrsqlConta = StrsqlConta & " where Fecha = '" & Format(dtpDias.Value, "yyyy/MM/dd") & "' "
        'ElseIf cmbTipoVista.SelectedIndex = 1 Then
        '    StrsqlConta = StrsqlConta & " where month(Fecha) = " & (cmbMeses.SelectedIndex + 1) & " and year(Fecha) = " & CInt(cmbAnio.Text) & " "
        'End If
        'StrsqlConta = StrsqlConta & " GROUP BY Tipo_Venta ,month(fecha) "
        'StrsqlConta = StrsqlConta & " ORDER BY Tipo_Venta,month(fecha) "


        'frmTransacciones.GridView1.Columns.Clear()
        'Me.Tbl_ResumenContaBS.DataSource = SQL(StrsqlConta, "tblDatosConta", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    'Private Sub cmbTipoVista_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If cmbTipoVista.SelectedIndex = 0 Then
    '        dtpDias.Visible = True
    '        cmbMeses.Visible = False
    '        cmbAnio.Visible = False
    '    Else
    '        dtpDias.Visible = False
    '        cmbMeses.Visible = True
    '        cmbAnio.Visible = True
    '    End If
    '    CargarContabilidad()
    'End Sub

    'Private Sub cmbMeses_SelectedValueChanged(sender As Object, e As EventArgs)
    '    CargarContabilidad()
    '    frmDetalleFactura.CargarDataDocumentos()
    '    'frmTransacciones.CargarTransacciones()
    'End Sub

    'Private Sub cmbAnio_SelectedValueChanged(sender As Object, e As EventArgs)
    '    CargarContabilidad()
    '    frmDetalleFactura.CargarDataDocumentos()
    '    'frmTransacciones.CargarTransacciones()
    'End Sub

    'Private Sub dtpDias_ValueChanged(sender As Object, e As EventArgs)
    '    CargarContabilidad()
    '    frmDetalleFactura.CargarDataDocumentos()
    '    'frmTransacciones.CargarTransacciones()
    'End Sub

#End Region

#Region "Controles del Formulario para Funciones de Catalogos."

    Private Sub bbiActualizar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiActualizar.ItemClick
        If NumCatalogo = 0 Then
            MigrarActualizacionDatos()
        ElseIf NumCatalogo = 50 Then
            frmCatalogoCuentas.Actualizar()
        End If
    End Sub

    Sub MigrarActualizacionDatos()
        Dim Resum As String = ""
        Dim MigrarUpdate As New clsCatalogos(My.Settings.SolIndustrialCNX)

        Try
            Resum = MigrarUpdate.MigrarActualizacionDatos(1)
            If Resum = "OK" Then
                MsgBox("La operaci�n solicitada se realiz� de forma exitosa", MsgBoxStyle.Information, "SIIF-WorkSystem")
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally

        End Try
    End Sub

    Private Sub bbiBodegas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiBodegas.ItemClick

        NumCatalogo = 1
        MostrarResumen(False)
        CargarTitulo()
        CargarDatos()
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        'Ribbon.SelectedPage = RibbonControl.Pages(0) 'Mostrar la pesta�a "Principal"

    End Sub

    Private Sub bbiMarcas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiMarcas.ItemClick
        NumCatalogo = 2
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiModelos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiModelos.ItemClick
        NumCatalogo = 3
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiDepartamentos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiDepartamentos.ItemClick
        NumCatalogo = 4
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiCategorias_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCategorias.ItemClick
        NumCatalogo = 5
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiProveedor_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiProveedor.ItemClick
        NumCatalogo = 6
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiUnidMed_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiUnidMed.ItemClick
        NumCatalogo = 7
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiPresentacion_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiPresentacion.ItemClick
        NumCatalogo = 8
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiProductos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiProductos.ItemClick
        NumCatalogo = 9
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiDepart_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiDepart.ItemClick
        NumCatalogo = 10
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiMunic_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiMunic.ItemClick
        NumCatalogo = 11
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiZonDist_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiZonDist.ItemClick
        NumCatalogo = 12
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiBarrios_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiBarrios.ItemClick
        NumCatalogo = 13
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub
    Private Sub bbiCliente_ItemClick_1(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCliente.ItemClick
        NumCatalogo = 14
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiSuministros_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiSuministros.ItemClick
        NumCatalogo = 15
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiActivosFijos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiActivosFijos.ItemClick
        NumCatalogo = 16
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiTipoGastos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiTipoGastos.ItemClick
        NumCatalogo = 18
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiTerminales_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiTerminales.ItemClick
        NumCatalogo = 19
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

#End Region

#Region "Controles del Formulario para Funciones de Ingresos."

    Private Sub bbiVentaProdServ_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiFacturacion.ItemClick
        Try
            FrmFactura.Dispose()
            FrmFactura.MdiParent = Me
            FrmFactura.WindowState = FormWindowState.Maximized
            FrmFactura.Show()
            NumCatalogo = 33
            RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub AbrirEditorIngresos(ByVal TipTras As Integer)
        MsgBox("Cambio")
        MostrarRegistrosTransacciones()
        TipoTransaccion = TipTras
        frmTransacciones.AbrirEditorTransacciones()
    End Sub

    Sub MostrarRegistrosTransacciones()
        'CargarMesAnioDia()
    End Sub


#End Region

#Region "Controles del Formulario para Funciones de Egresos."

    Private Sub bbiCompraProd_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCompraProd.ItemClick
        TipoDocumento = 4
        MostrarVentanaDocumentos()
    End Sub

    Private Sub bbiCompraRecargas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCompraRecargas.ItemClick
        AbrirEditorIngresos(10)
    End Sub

    'Private Sub bbiCompraLocales_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCompraLocales.ItemClick
    '    NumCatalogo = 20
    '    MostrarResumen(False)
    '    frmMasterCompraLocal.MdiParent = Me
    '    frmMasterCompraLocal.WindowState = FormWindowState.Maximized
    '    frmMasterCompraLocal.Show()
    '    bbiExportar.Enabled = True
    '  'AbrirEditorIngresos(11)
    'End Sub

    Private Sub bbiCompraActiv_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiDevolProv.ItemClick
        AbrirEditorIngresos(12)
    End Sub

    Private Sub bbiRegGastos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiRegGastos.ItemClick
        AbrirEditorIngresos(13)
    End Sub

#End Region

#Region "Controles del Formulario para Funciones de CyberCafe"

    'Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
    '    StatusBar.Items.Item(6).Text = Now.Date
    '    StatusBar.Items.Item(8).Text = Now.ToShortTimeString
    'End Sub

#End Region

    Private Sub bbiEntradas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiEntradas.ItemClick
        TipoMovimiento = 4
        NumCatalogo = 25
        MostrarResumen(False)
        frmEntradaSalida.Dispose()
        frmEntradaSalida.MdiParent = Me
        frmEntradaSalida.WindowState = FormWindowState.Maximized
        'frmEntradaSalida.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmProyeccion.mainPanel.Size.Width / 2, 0)
        'frmEntradaSalida.mainPanel.Anchor = AnchorStyles.None
        frmEntradaSalida.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiSalidas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiSalidas.ItemClick
        TipoMovimiento = 5
        NumCatalogo = 26
        MostrarResumen(False)
        frmEntradaSalida.Dispose()
        frmEntradaSalida.MdiParent = Me
        frmEntradaSalida.WindowState = FormWindowState.Maximized
        'frmEntradaSalida.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmProyeccion.mainPanel.Size.Width / 2, 0)
        'frmEntradaSalida.mainPanel.Anchor = AnchorStyles.None
        frmEntradaSalida.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiTransferencia_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiTransferencia.ItemClick
        'TipoDocumento = 5
        'MostrarVentanaDocumentos()
        NumCatalogo = 39
        MostrarResumen(False)
        frmTraslados.Dispose()
        frmTraslados.MdiParent = Me
        frmTraslados.WindowState = FormWindowState.Maximized
        'frmEntradaSalida.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmProyeccion.mainPanel.Size.Width / 2, 0)
        'frmEntradaSalida.mainPanel.Anchor = AnchorStyles.None
        frmTraslados.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Sub MostrarVentanaDocumentos()
        frmDetalleFactura.Dispose()
        frmDetalleFactura.MdiParent = Me
        frmDetalleFactura.WindowState = FormWindowState.Maximized
        frmDetalleFactura.Show()
    End Sub

    Private Sub bbiCatalogo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCatalogo.ItemClick
        frmCatalogoCuentas.Dispose()
        frmCatalogoCuentas.MdiParent = Me
        frmCatalogoCuentas.WindowState = FormWindowState.Maximized
        frmCatalogoCuentas.Show()
    End Sub

    Private Sub bbiHacerCierre_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiHacerCierre.ItemClick
        MostrarRegistrosTransacciones()
        frmTransacciones.btnRealizarCierre.Visible = True
        frmTransacciones.ChkMostrarCierre.Visible = True
    End Sub

    Private Sub btnShowResume_Click(sender As Object, e As EventArgs)
        'If ShowResume = True Then
        'btnShowResume.Image = Image.FromFile("C:\Worksystem\btn_Off_Lit.png")
        'ShowResume = False
        'Panel1.Visible = False
        'Panel3.Visible = False
        'ElseIf ShowResume = False Then
        ''btnShowResume.Image = Image.FromFile("C:\Worksystem\btn_On_Lit.png")
        'ShowResume = True
        'Panel1.Visible = True
        'Panel3.Visible = True
        'End If
    End Sub

    Sub MostrarResumen(ByVal VerResumen As Boolean)
        If VerResumen Then
            'btnShowResume.Image = Image.FromFile("C:\Worksystem\btn_On_Lit.png")
        Else
            'btnShowResume.Image = Image.FromFile("C:\Worksystem\btn_Off_Lit.png")
        End If
    End Sub

    Private Sub bbiArqueo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiArqueo.ItemClick
        'FrmCierreTransacciones.Dispose()
        FrmCierreTransacciones.ShowDialog()
    End Sub


    Private Sub bbiProgramPagos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiProgramPagos.ItemClick
        NumCatalogo = 21
        MostrarResumen(False)
        frmProgramPagos.Dispose()
        frmProgramPagos.MdiParent = Me
        frmProgramPagos.WindowState = FormWindowState.Maximized
        frmProgramPagos.Show()
    End Sub

    Private Sub bbiNuevo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiNuevo.ItemClick
        nTipoEdic = 1

        If NumCatalogo >= 1 And NumCatalogo <= 20 Then
            frmCatalogo.AbrirEditorCatalogos()
        ElseIf NumCatalogo = 21 Then

            ' frmMasterCompraLocal.Nuevo()
            '------------------------ bob paso por aqui  -------------------------------
            compra_local.nuevo()
            '----------------------------------------------------------------------------

        ElseIf NumCatalogo = 22 Then

            '   frmMasterCompraForanea.Nuevo()

            compra_foranea.nuevo()

        ElseIf NumCatalogo = 23 Then
            frmOrdenProd.nuevo()
        ElseIf NumCatalogo = 24 Then
            Dim result As Integer = MessageBox.Show("Si continua perdera los datos mostrados." + Chr(13) + Chr(13) + "Desea Continuar?", "Nueva Kardex", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                frmKardexInv.nuevo()
            End If
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            frmEntradaSalida.Nuevo()
        ElseIf NumCatalogo = 27 Then
            frmProyeccion.nuevo()
        ElseIf NumCatalogo = 28 Then
            frmCatalogo.AbrirEditorCatalogos()
        ElseIf NumCatalogo = 30 Then
            frmAdminCIF.nuevo()
        ElseIf NumCatalogo = 32 Then
            FrmRemision.Nuevo()
        ElseIf NumCatalogo = 33 Then
            Nuevo = 1
            frmBusquedaDoc.Show()
        ElseIf NumCatalogo = 34 Then
            FrmVerificacion_Cliente.Show()
            FrmVerificacion_Cliente.edition = 0
        ElseIf NumCatalogo = 37 Then
            frmVehiculo.Vnuevo()
        ElseIf NumCatalogo = 38 Then
            frmOrdenPedido.Nuevo()
        ElseIf NumCatalogo = 39 Then
            frmTraslados.Nuevo()
        ElseIf NumCatalogo = 40 Or NumCatalogo = 41 Then
            frmEndoseFactura.Nuevo()
        End If

        Select Case NumCatalogo
            Case 44 'PRESTAMOS
                frmPrestamos.Nuevo()
        End Select
    End Sub

    Private Sub bbiModificar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiModificar.ItemClick
        nTipoEdic = 2
        If NumCatalogo >= 1 And NumCatalogo <= 20 Then
            frmCatalogo.AbrirEditorCatalogos()
        ElseIf NumCatalogo = 21 Then
            ' frmMasterCompraLocal.Modificar()
            '--------------- bob estuvo aqui -------------------
            compra_local.Modificar()
            '---------------------------------------------------
        ElseIf NumCatalogo = 22 Then
            '  frmMasterCompraForanea.Modificar()

            compra_foranea.modificar()

        ElseIf NumCatalogo = 23 Then
            frmOrdenProd.Modificar()
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            frmEntradaSalida.Modificar()
        ElseIf NumCatalogo = 27 Then
            frmProyeccion.Modificar()
        ElseIf NumCatalogo = 50 Then
            frmCatalogoCuentas.Modificar()
        ElseIf NumCatalogo = 32 Then
            FrmRemision.Busqueda()
        ElseIf NumCatalogo = 33 Then
            FrmFactura.Estado()
        ElseIf NumCatalogo = 34 Then
            FrmVerificacion_Cliente.Show()
            FrmVerificacion_Cliente.edition = 1
        ElseIf NumCatalogo = 37 Then 'Vehiculo
            frmVehiculo.Veditar()
        ElseIf NumCatalogo = 38 Then
            frmOrdenPedido.Modificar()
        ElseIf NumCatalogo = 40 Or NumCatalogo = 41 Then
            frmEndoseFactura.Modificar()
        End If

    End Sub

    Private Sub bbiEliminar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiEliminar.ItemClick
        nTipoEdic = 3
        If NumCatalogo >= 1 And NumCatalogo <= 20 Then
            frmCatalogo.AbrirEditorCatalogos()
        ElseIf NumCatalogo = 21 Then
            frmMasterCompraLocal.Eliminar()
        ElseIf NumCatalogo = 22 Then
            '  frmMasterCompraForanea.Eliminar()

            compra_foranea.eliminar()

        ElseIf NumCatalogo = 32 Then
            FrmRemision.RemisionBorrar()
        ElseIf NumCatalogo = 33 Then
            'FrmFacturacion.Borrar()
        ElseIf NumCatalogo = 34 Then
            FrmVerificacion_Cliente.Borrar()
        ElseIf NumCatalogo = 37 Then 'Vehiculo
            frmVehiculo.Vborrar()
        End If
    End Sub

    Private Sub bbiExportar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiExportar.ItemClick
        Dim PS As DevExpress.XtraPrinting.PrintingSystem = New DevExpress.XtraPrinting.PrintingSystem
        Dim PCL As DevExpress.XtraPrinting.PrintableComponentLink = New DevExpress.XtraPrinting.PrintableComponentLink(PS)
        If NumCatalogo = 32 Then
            Rremision.Dispose()
            Rremision.MdiParent = Me
            Rremision.WindowState = FormWindowState.Maximized
            Rremision.Show()
            RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        ElseIf NumCatalogo = 23 Then
            RordenProduccion.Dispose()
            RordenProduccion.MdiParent = Me
            RordenProduccion.WindowState = FormWindowState.Maximized
            RordenProduccion.Show()
            RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        ElseIf NumCatalogo = 24 Then
            With frmKardexInv
                If .grdKardex.MainView.RowCount > 0 Then
                    .Tentrada = 0
                    .Tsalida = 0
                    .Tsaldo = 0
                    .Tpromedio = 0
                    .print()
                Else
                    MsgBox("No existen registros que imprimir")
                End If
            End With
        ElseIf NumCatalogo = 25 Then
            RordenProduccion.Dispose()
            RordenProduccion.MdiParent = Me
            RordenProduccion.WindowState = FormWindowState.Maximized
            RordenProduccion.Show()
            RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        ElseIf NumCatalogo = 26 Then
            RordenProduccion.Dispose()
            RordenProduccion.MdiParent = Me
            RordenProduccion.WindowState = FormWindowState.Maximized
            RordenProduccion.Show()
            RibbonControl1.SelectedPage = RibbonControl1.Pages(0)

        ElseIf NumCatalogo = 27 Then

            PCL.Component = frmProyeccion.grdDetProyeccion
            PS.PreviewFormEx.Text = frmProyeccion.grvDetProyeccion.GroupPanelText
            PS.Document.Name = "rptPedido"
            PS.ExportOptions.Email.RecipientName = "jojo"
            PCL.RtfReportHeader = frmProyeccion.grvDetProyeccion.GroupPanelText
            PCL.RtfReportHeader = "Reporte de Pedido de Materiales"
            PCL.RtfReportFooter = Date.Now.ToString
            PCL.Margins.Left = 50 ' = 1/2 inch
            PCL.Margins.Right = 50
            PCL.Margins.Top = 50
            PCL.Margins.Bottom = 50
            PCL.Landscape = True
            PCL.PaperKind = System.Drawing.Printing.PaperKind.LetterRotated
            PCL.EnablePageDialog = True
            PCL.CreateDocument()
            PS.PreviewFormEx.MdiParent = Me
            PS.PreviewFormEx.Show()

        ElseIf NumCatalogo = 37 Then

            PCL.Component = frmVehiculo.GridControl1
            PS.PreviewFormEx.Text = frmVehiculo.GridView1.GroupPanelText
            PS.Document.Name = "Reporte Vehiculo"
            'PS.ExportOptions.Email.RecipientName = "jojo"
            PCL.RtfReportHeader = frmVehiculo.GridView1.GroupPanelText
            PCL.RtfReportFooter = Date.Now.ToString
            PCL.Margins.Left = 50
            PCL.Margins.Right = 50
            PCL.Margins.Top = 50
            PCL.Margins.Bottom = 50
            PCL.Landscape = True
            PCL.PaperKind = System.Drawing.Printing.PaperKind.LetterRotated
            PCL.EnablePageDialog = True
            PCL.CreateDocument()
            PS.PreviewFormEx.MdiParent = Me
            PS.PreviewFormEx.Show()
            '--------------------------------------
        ElseIf NumCatalogo = 38 Then
            PCL.Component = frmOrdenPedido.grdDetalle
            PS.PreviewFormEx.Text = "Orden de Materiales"
            PS.Document.Name = "Orden de Materiales"
            PS.ExportOptions.Email.RecipientName = Remitente
            PS.ExportOptions.Email.Subject = "Orden de Materiales"
            PCL.RtfReportHeader = "Orden de materiales"
            PCL.RtfReportFooter = Date.Now.ToString
            PCL.Margins.Left = 50 ' = 1/2 inch
            PCL.Margins.Right = 50
            PCL.Margins.Top = 50
            PCL.Margins.Bottom = 50
            PCL.Landscape = True
            PCL.PaperKind = System.Drawing.Printing.PaperKind.LetterRotated
            PCL.EnablePageDialog = True
            PCL.CreateDocument()
            PS.PreviewFormEx.MdiParent = Me
            PS.PreviewFormEx.Show()
        Else
            'Dim PS As DevExpress.XtraPrinting.PrintingSystem = New DevExpress.XtraPrinting.PrintingSystem
            'Dim PCL As DevExpress.XtraPrinting.PrintableComponentLink = New DevExpress.XtraPrinting.PrintableComponentLink(PS)
            If NumCatalogo >= 1 And NumCatalogo <= 20 Then
                PCL.Component = frmCatalogo.GridControl1
                PS.PreviewFormEx.Text = frmCatalogo.LblTitulo.Text
                PS.Document.Name = frmCatalogo.LblTitulo.Text
            ElseIf NumCatalogo = 27 Then
                PCL.Component = frmProyeccion.grdDetProyeccion
                PS.PreviewFormEx.Text = "Reporte de Solicitud de Pedido"
                PS.Document.Name = "Reporte de Solicitud de Pedido"
                Dim Title As String = ""
                Title = (Chr(13) + Chr(9) + Chr(9) + Chr(9) + Chr(9) + Chr(9) + ("Reporte de Solicitud de Pedido").ToUpper + Chr(13)) & _
                        (Chr(9) + Chr(9) + Chr(9) + Chr(9) + Chr(9) + Chr(9) + Date.Now.ToString + Chr(13))

                PCL.RtfReportHeader = Title
            End If
            PCL.Margins.Left = 50 ' = 1/2 inch
            PCL.Margins.Right = 50
            PCL.Margins.Top = 50
            PCL.Margins.Bottom = 50
            PCL.Landscape = True
            PCL.PaperKind = System.Drawing.Printing.PaperKind.LetterRotated
            PCL.EnablePageDialog = True
            PCL.CreateDocument()

            PS.PreviewFormEx.MdiParent = Me
            PS.PreviewFormEx.Show()
        End If
    End Sub

    Private Sub bbiCancelar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCancelar.ItemClick
        nTipoEdic = 0
        If NumCatalogo >= 1 And NumCatalogo <= 20 Then
            frmCatalogo.AbrirEditorCatalogos()
        ElseIf NumCatalogo = 21 Then

            'frmMasterCompraLocal.Cancelar()

            compra_local.Cancelar()

        ElseIf NumCatalogo = 22 Then

            '  frmMasterCompraForanea.Cancelar()

            compra_foranea.cancelar()


        ElseIf NumCatalogo = 23 Then
            frmOrdenProd.Cancelar()
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            frmEntradaSalida.Cancelar()
        ElseIf NumCatalogo = 27 Then
            frmProyeccion.Cancelar()
        ElseIf NumCatalogo = 30 Then
            'frmAdminCIF.cancelar()
        ElseIf NumCatalogo = 32 Then
            FrmRemision.Nuevo()
            FrmRemision.pmaster.Enabled = False
        ElseIf NumCatalogo = 33 Then
            '  FrmFacturacion.Nuevo()
        ElseIf NumCatalogo = 34 Then
            CargarDatos()
        ElseIf NumCatalogo = 38 Then
            frmOrdenPedido.Cancelar()
        ElseIf NumCatalogo = 39 Then
            frmTraslados.Cancelar()
        ElseIf NumCatalogo = 40 Then
            frmEndoseFactura.Cancelar()
        End If
    End Sub
    Private Sub bbiGuardar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiGuardar.ItemClick
        If NumCatalogo >= 1 And NumCatalogo <= 20 Then
            frmCatalogo.AbrirEditorCatalogos()
        ElseIf NumCatalogo = 21 Then

            ' frmMasterCompraLocal.GuardarMasterDoc()

            compra_local.guardar()

        ElseIf NumCatalogo = 22 Then

            compra_foranea.guardar()

        ElseIf NumCatalogo = 23 Then
            frmOrdenProd.GuardarMasterDoc()
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            frmEntradaSalida.GuardarMasterDoc()
        ElseIf NumCatalogo = 27 Then
            frmProyeccion.GuardarMasterDoc()
        ElseIf NumCatalogo = 30 Then
            frmAdminCIF.guardarMasterDoc()
        ElseIf NumCatalogo = 32 Then
            With FrmRemision
                .edicion = 1
                .Guardar()
            End With
        ElseIf NumCatalogo = 33 Then
            FrmFactura.Guardar()
        ElseIf NumCatalogo = 38 Then
            frmOrdenPedido.GuardarMasterDoc()
        ElseIf NumCatalogo = 39 Then
            frmTraslados.GuardarMasterDoc()
        ElseIf NumCatalogo = 40 Then
            frmEndoseFactura.Guardar()
        ElseIf NumCatalogo = 41 Then
            frmEndoseFactura.GuardarB()
        End If

        Select Case NumCatalogo
            Case 44
                'frmPrestamos.Guardar()
        End Select
        'If Application.OpenForms().OfType(Of FrmRemision).Any Then
        '    Select Case FrmRemision.tabPrincipal.SelectedIndex
        '        Case 0 'REMISION
        '            FrmRemision.RemisionGuardar()
        '        Case 1 'VENTAS
        '        Case 2 'ENTRADA A BODEGA
        '        Case 3 'DEVOLUCIONES
        '        Case 4 'RECIBOS
        '    End Select
        'End If
    End Sub

    Private Sub bbiBuscar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiBuscar.ItemClick
        If NumCatalogo = 21 Then
            frmMasterCompraLocal.Buscar()
        ElseIf NumCatalogo = 22 Then
            'frmBusquedaDoc.Text = "Buscar Compra Foranea"
            'frmBusquedaDoc.ShowDialog()
            compra_foranea.historial()

        ElseIf NumCatalogo = 23 Then
            frmBusquedaDoc.Text = "Buscar Orden de Producci�n"
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 24 Then
            frmBusquedaDoc.Text = "Buscar Producto"
            'frmBusquedaDoc.Size = New Size(648, 448)
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            frmBusquedaDoc.Text = "Buscar Producto"
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 27 Then
            frmBusquedaDoc.Text = "Buscar Proyecci�n"
            'frmBusquedaDoc.Size = New Size(600, 324)
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 30 Then
            frmBusquedaDoc.Text = "Busqueda de Costos Registrados"
            frmBusquedaDoc.Size = New Size(500, 324)
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 32 Then 'REMISION
            frmBusquedaDoc.ShowDialog()
            frmBusquedaDoc.Text = "Busqueda de Ordenes de Remisiones"
        ElseIf NumCatalogo = 33 Then 'FACTURA
            Nuevo = 0
            frmBusquedaDoc.ShowDialog()
            frmBusquedaDoc.Text = "Busqueda de Facturas Registradas"
        ElseIf NumCatalogo = 34 Then 'Verificacion'
            frmBusquedaDoc.ShowDialog()
            frmBusquedaDoc.Text = "Busqueda de Clientes Verificados"
        ElseIf NumCatalogo = 38 Then
            frmBusquedaDoc.Text = "Busqueda de Ordenes de Pedidos"
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 39 Then
            frmBusquedaDoc.Text = "Busqueda de Ordenes de Traslados"
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 40 Then
            frmBusquedaDoc.Text = "Busqueda de Endoses"
            frmBusquedaDoc.ShowDialog()
        ElseIf NumCatalogo = 41 Then
            frmBusquedaDoc.Text = "Busqueda de Verificacion"
            frmBusquedaDoc.ShowDialog()
        End If
    End Sub

    Private Sub bbiCerrar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCerrar.ItemClick
        If NumCatalogo >= 1 And NumCatalogo <= 20 Then
            NumCatalogo = 0
            frmCatalogo.Cerrar()
            PermitirBotonesEdicion(True)
        ElseIf NumCatalogo = 23 Then
            If Application.OpenForms().OfType(Of RordenProduccion).Any Then
                NumCatalogo = 0
                RordenProduccion.Close()
            Else
                NumCatalogo = 0
                frmOrdenProd.Cerrar()
                PermitirBotonesEdicion(True)
            End If
        ElseIf NumCatalogo = 21 Then

            NumCatalogo = 0

            compra_local.Close()
            PermitirBotonesEdicion(True)

        ElseIf NumCatalogo = 22 Then
            NumCatalogo = 0

            compra_foranea.Close()

            PermitirBotonesEdicion(True)
        ElseIf NumCatalogo = 24 Then
            NumCatalogo = 0
            frmKardexInv.Cerrar()
            PermitirBotonesEdicion(True)
        ElseIf NumCatalogo = 25 Or NumCatalogo = 26 Then
            NumCatalogo = 0
            frmEntradaSalida.Cerrar()
            PermitirBotonesEdicion(True)
        ElseIf NumCatalogo = 27 Then
            NumCatalogo = 0
            frmProyeccion.Cerrar()
            PermitirBotonesEdicion(True)
        ElseIf NumCatalogo = 28 Then
            NumCatalogo = 0
            frmCatalogo.Cerrar()
            PermitirBotonesEdicion(True)
        ElseIf NumCatalogo = 29 Then
            NumCatalogo = 0
            frmCierreOrden.Cerrar()
        ElseIf NumCatalogo = 30 Then
            NumCatalogo = 0
            frmAdminCIF.Cerrar()
        ElseIf NumCatalogo = 31 Then
            NumCatalogo = 0
            'frmReparaciones_no_use.Cerrar()
        ElseIf NumCatalogo = 32 Then 'pendiente cerrar reportes

            NumCatalogo = 0
            FrmRemision.Close()
        ElseIf NumCatalogo = 33 Then
            NumCatalogo = 0
            FrmFactura.Close()
        ElseIf NumCatalogo = 34 Then
            NumCatalogo = 0
            frmCatalogo.Close()
        ElseIf NumCatalogo = 35 Then
            NumCatalogo = 0
            frmMaestroSeguridad.Close()
        ElseIf NumCatalogo = 36 Then
            NumCatalogo = 0
            FrmRInventario.Close()
        ElseIf NumCatalogo = 37 Then
            NumCatalogo = 0
            frmVehiculo.Close()
        ElseIf NumCatalogo = 38 Then
            NumCatalogo = 0
            frmOrdenPedido.Close()
        ElseIf NumCatalogo = 39 Then
            NumCatalogo = 0
            frmTraslados.Close()
        ElseIf NumCatalogo = 40 Or NumCatalogo = 41 Then
            NumCatalogo = 0
            frmEndoseFactura.Close()
        Else
            'NumCatalogo = 0
            If MsgBox("Desea realmente abandonar la aplicaci�n?", MsgBoxStyle.YesNo, "Informaci�n") = MsgBoxResult.Yes Then
                Application.ExitThread()
                'Application.Exit()
                'Me.Dispose()
                'Me.Close()
                'frmSingIn.Close()
            End If
        End If
    End Sub
    Private Sub bbiOrdenesPago_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiOrdenesPago.ItemClick
        NumCatalogo = 22
        MostrarResumen(False)
        frmMasterCompraForanea.MdiParent = Me
        frmMasterCompraForanea.WindowState = FormWindowState.Maximized
        frmMasterCompraForanea.Show()
        bbiExportar.Enabled = True
    End Sub

    Private Sub bbiEditCatalogo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiEditCatalogo.ItemClick
        NumCatalogo = 50
        MostrarResumen(False)
        frmCatalogoCuentas.MdiParent = Me
        frmCatalogoCuentas.WindowState = FormWindowState.Maximized
        frmCatalogoCuentas.Show()
        bbiExportar.Enabled = True
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiEditComprob_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiEditComprob.ItemClick
        NumCatalogo = 51
        MostrarResumen(False)
        frmEditorComprobantes.MdiParent = Me
        frmEditorComprobantes.WindowState = FormWindowState.Maximized
        frmEditorComprobantes.Show()
        bbiExportar.Enabled = True
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiReciboManual_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiReciboManual.ItemClick
        frmRecibosAnticipos.ShowDialog()
    End Sub

    Private Sub bbiAplicarAnticipo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiAplicarAnticipo.ItemClick
        frmAplicacionAnticipos.ShowDialog()
    End Sub

    Private Sub bbiExpedEmpleado_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiExpedEmpleado.ItemClick
        NumCatalogo = 17
        'MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub
    Private Sub BarButtonItem36_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub btnRecetasProduccion_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnRecetasProduccion.ItemClick
        NumCatalogo = 20
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub btnOrdenProd_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOrdenProd.ItemClick
        NumCatalogo = 23
        MostrarResumen(False)
        frmOrdenProd.Dispose()
        frmOrdenProd.MdiParent = Me
        frmOrdenProd.WindowState = FormWindowState.Maximized
        'frmOrdenProd.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmOrdenProd.mainPanel.Size.Width / 2, 0)
        'frmOrdenProd.mainPanel.Anchor = AnchorStyles.None
        frmOrdenProd.Show()
        bbiExportar.Enabled = True
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiControlKardex_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiControlKardex.ItemClick
        NumCatalogo = 24
        MostrarResumen(False)
        frmKardexInv.Dispose()
        frmKardexInv.MdiParent = Me
        frmKardexInv.WindowState = FormWindowState.Maximized
        frmKardexInv.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmKardexInv.mainPanel.Size.Width / 2, 0)
        frmKardexInv.mainPanel.Anchor = AnchorStyles.None
        frmKardexInv.Show()
        bbiNuevo.Enabled = False
        bbiModificar.Enabled = False
        bbiEliminar.Enabled = False
        bbiGuardar.Enabled = False
        bbiCancelar.Enabled = False
        bbiBuscar.Enabled = True
        bbiExportar.Enabled = True
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiCompraLocales_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCompraLocales.ItemClick

        NumCatalogo = 21
        MostrarResumen(False)
        'frmMasterCompraLocal.MdiParent = Me
        'frmMasterCompraLocal.WindowState = FormWindowState.Maximized
        'frmMasterCompraLocal.Show()

        '------------------------------- aqui tambien estuvo bob xD -----------------------------------------------

        compra_local.MdiParent = Me
        compra_local.WindowState = FormWindowState.Maximized
        compra_local.pTop.Enabled = False
        compra_local.Show()

        '----------------------------------------------------------------------------------------------------------

        bbiExportar.Enabled = False
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)

    End Sub

    Private Sub bbiComprasForaneas_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiComprasForaneas.ItemClick
        NumCatalogo = 22
        MostrarResumen(False)
        'frmMasterCompraForanea.MdiParent = Me
        'frmMasterCompraForanea.WindowState = FormWindowState.Maximized
        'frmMasterCompraForanea.Show()
        compra_foranea.MdiParent = Me
        compra_foranea.WindowState = FormWindowState.Maximized
        compra_foranea.Show()
        bbiExportar.Enabled = False
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiProyeccion_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiProyeccion.ItemClick
        NumCatalogo = 27
        MostrarResumen(False)
        frmProyeccion.Dispose()
        frmProyeccion.MdiParent = Me
        frmProyeccion.WindowState = FormWindowState.Maximized
        frmProyeccion.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmProyeccion.mainPanel.Size.Width / 2, 0)
        frmProyeccion.mainPanel.Anchor = AnchorStyles.None
        frmProyeccion.Show()
        bbiExportar.Enabled = True
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiOrdenPago_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiOrdenPago.ItemClick
        NumCatalogo = 28
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiCif_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCOrden.ItemClick
        NumCatalogo = 29
        MostrarResumen(False)
        frmCierreOrden.Dispose()
        frmCierreOrden.MdiParent = Me
        frmCierreOrden.WindowState = FormWindowState.Maximized
        'frmCierreOrden.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmCierreOrden.mainPanel.Size.Width / 2, 0)
        'frmCierreOrden.mainPanel.Anchor = AnchorStyles.None
        frmCierreOrden.Show()
        bbiExportar.Enabled = False
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub BarButtonItem34_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem34.ItemClick
        NumCatalogo = 30
        MostrarResumen(False)
        frmAdminCIF.Dispose()
        frmAdminCIF.MdiParent = Me
        frmAdminCIF.WindowState = FormWindowState.Maximized
        'frmAdminCIF.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmAdminCIF.mainPanel.Size.Width / 2, 0)
        'frmAdminCIF.mainPanel.Anchor = AnchorStyles.None
        frmAdminCIF.Show()
        bbiExportar.Enabled = False
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub BarButtonItem32_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem32.ItemClick
        NumCatalogo = 31
        MostrarResumen(False)
        'frmReparaciones_no_use.Dispose()
        'frmReparaciones_no_use.MdiParent = Me
        'frmReparaciones_no_use.WindowState = FormWindowState.Maximized
        'frmReparaciones_no_use.mainPanel.Location = New Point(Me.ClientSize.Width / 2.4 - frmReparaciones_no_use.mainPanel.Size.Width / 2, 0)
        'frmReparaciones_no_use.mainPanel.Anchor = AnchorStyles.None
        'frmReparaciones_no_use.Show()
        bbiExportar.Enabled = False
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub btnRemision_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnRemision.ItemClick
        FrmRemision.Dispose()
        FrmRemision.MdiParent = Me
        FrmRemision.WindowState = FormWindowState.Maximized
        FrmRemision.Show()
        NumCatalogo = 32
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        bbiExportar.Enabled = True
    End Sub
    Private Sub bbiFcliente_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiFcliente.ItemClick
        NumCatalogo = 34
        MostrarResumen(False)
        If CatalogoVisible = False Then
            MostrarCatalogos()
        End If
        CargarTitulo()
        CargarDatos()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        bbiCancelar.Enabled = True
    End Sub

    Private Sub frmPrincipal_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If MsgBox("Desea realmente abandonar la aplicaci�n?", MsgBoxStyle.YesNo, "Informaci�n") = MsgBoxResult.Yes Then
            Application.ExitThread()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub bbiUsuarios_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiUsuarios.ItemClick
        NumCatalogo = 35
        MostrarResumen(False)
        frmMaestroSeguridad.MdiParent = Me
        frmMaestroSeguridad.WindowState = FormWindowState.Maximized
        frmMaestroSeguridad.Show()
        bbiExportar.Enabled = False
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiCaja_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        FrmCaja.ShowDialog()
    End Sub

    Private Sub bbiCaja_ItemClick_1(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiCaja.ItemClick
        FrmCaja.ShowDialog()
    End Sub

    Private Sub bbiTasaCambio_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiTasaCambio.ItemClick
        FrmTasaCambio.ShowDialog()
    End Sub

    Private Sub bbiRinventario_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiRinventario.ItemClick
        FrmRInventario.Dispose()
        FrmRInventario.MdiParent = Me
        FrmRInventario.WindowState = FormWindowState.Maximized
        FrmRInventario.Show()
        NumCatalogo = 36
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiVehiculo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiVehiculo.ItemClick
        NumCatalogo = 37
        frmVehiculo.Dispose()
        frmVehiculo.MdiParent = Me
        frmVehiculo.WindowState = FormWindowState.Maximized
        frmVehiculo.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiPedidosProv_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiPedidosProv.ItemClick
        NumCatalogo = 38
        MostrarResumen(False)
        frmOrdenPedido.MdiParent = Me
        frmOrdenPedido.WindowState = FormWindowState.Maximized
        frmOrdenPedido.Show()
        bbiExportar.Enabled = True
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub
    Private Sub bbiImprimir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiImprimir.ItemClick
        Dim PS As DevExpress.XtraPrinting.PrintingSystem = New DevExpress.XtraPrinting.PrintingSystem
        Dim PCL As DevExpress.XtraPrinting.PrintableComponentLink = New DevExpress.XtraPrinting.PrintableComponentLink(PS)
        Select Case NumCatalogo
            Case 1 To 9 ' CATALOGO BODEGA, MARCAS, MODELOS, LINEAS, CATEGORIA, PROVEEDORES, UNIDADES, PRESENTACION, PRODUCTO
                PS.PreviewFormEx.Text = frmCatalogo.Text
                PCL.Component = frmCatalogo.GridControl1
                PS.Document.Name = frmCatalogo.Text
                PCL.RtfReportHeader = frmCatalogo.Text
                PS.PreviewFormEx.Show()
            Case 32 'REMISION
                FrmRemision.Print2()
            Case 33 'Factura
                FrmFactura.Print1()

            Case 34 'CENTRAL RIESGO

                PS.PreviewFormEx.Text = frmCatalogo.Text
                PCL.Component = frmCatalogo.GridControl1

                PS.Document.Name = frmCatalogo.Text

                PCL.RtfReportHeader = frmCatalogo.Text

                'PS.PreviewFormEx.Show()


                'With PS.PreviewFormEx

                PS.PreviewFormEx.Dispose()
                PS.PreviewFormEx.MdiParent = Me
                PS.PreviewFormEx.WindowState = FormWindowState.Maximized
                PS.PreviewFormEx.Show()
                Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None

                'End With


                'FrmRemision.Dispose()
                'FrmRemision.MdiParent = Me
                'FrmRemision.WindowState = FormWindowState.Maximized
                'FrmRemision.Show()


                '                MsgBox(PS.Document.Name)
            Case 21

                Dim x As New reporte_moviemiento_almacen.compra_local(nSucursal, compra_local.txtNumDoc.Text)

                x.visualizar()

                Exit Sub

            Case 22

                compra_foranea.reporte_vista_previa()

                Exit Sub

        End Select
        'PCL.Component = frmVehiculo.GridControl1
        'PS.PreviewFormEx.Text = frmVehiculo.GridView1.GroupPanelText
        'PS.Document.Name = "Reporte Vehiculo"
        'PS.ExportOptions.Email.RecipientName = "jojo"
        'PCL.RtfReportHeader = frmVehiculo.GridView1.GroupPanelText


        PCL.RtfReportFooter = Date.Now.ToString
        PCL.Margins.Left = 50
        PCL.Margins.Right = 50
        PCL.Margins.Top = 50
        PCL.Margins.Bottom = 50
        PCL.Landscape = True
        PCL.PaperKind = System.Drawing.Printing.PaperKind.LetterRotated
        PCL.EnablePageDialog = True
        PCL.CreateDocument()
        'PS.PreviewFormEx.MdiParent = Me
        'PS.PreviewFormEx.WindowState = FormWindowState.Maximized
        'PS.PreviewFormEx.Show()
    End Sub
    Private Sub bbiFendose_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiFendose.ItemClick
        NumCatalogo = 40
        frmEndoseFactura.Dispose()
        frmEndoseFactura.MdiParent = Me
        frmEndoseFactura.WindowState = FormWindowState.Maximized
        frmEndoseFactura.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub
    Private Sub bbiFverificacion_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiFverificacion.ItemClick
        NumCatalogo = 41
        frmEndoseFactura.Dispose()
        frmEndoseFactura.MdiParent = Me
        frmEndoseFactura.WindowState = FormWindowState.Maximized
        frmEndoseFactura.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub BarButtonItem35_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnGarantia.ItemClick
        NumCatalogo = 42
        frmReparaciones.Dispose()
        frmReparaciones.MdiParent = Me
        frmReparaciones.WindowState = FormWindowState.Maximized
        frmReparaciones.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub frmPrincipal_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.BackgroundImage = My.Resources.logo
    End Sub

    Private Sub bbiGenerarPlanilla_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiGenerarPlanilla.ItemClick
        NumCatalogo = 43
        frmPlanilla.Dispose()
        frmPlanilla.MdiParent = Me
        frmPlanilla.WindowState = FormWindowState.Maximized
        frmPlanilla.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiPrestamosPers_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiPrestamosPers.ItemClick
        NumCatalogo = 44
        frmPrestamos.Dispose()
        frmPrestamos.MdiParent = Me
        frmPrestamos.WindowState = FormWindowState.Maximized
        frmPrestamos.Show()
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
    End Sub

    Private Sub bbiKit_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiKit.ItemClick
        FrmKit.Dispose()
        FrmKit.MdiParent = Me
        FrmKit.WindowState = FormWindowState.Maximized
        FrmKit.Show()
        'NumCatalogo = 32
        RibbonControl1.SelectedPage = RibbonControl1.Pages(0)
        'bbiExportar.Enabled = True
    End Sub

End Class