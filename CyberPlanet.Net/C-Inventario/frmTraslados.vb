﻿Imports System.Xml
Imports System.Threading
Imports System.Text.RegularExpressions
'DEVEXPRESS STYLES
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmTraslados

    Public IdEstado As Integer
    Public ResumMaster As String = ""
    Public ResumMDetalle As String = ""
    Public ResumEntrada As String = ""
    Public ResumSalida As String = ""
    Public ResumEntradaDeta As String = ""
    Public ResumSalidaDeta As String = ""
    Public CodMaxEntrada As String = ""
    Public CodMaxSalida As String = ""
    Public Resum As String = ""
    Public Aplicado As Boolean = False
    Public Referencia As String = ""

    Dim C_existencias As Color = Color.Coral

    'Private Bodega As New 


    Public Sub CargarDatos(ByVal Codigo As String)
        Dim strsql As String = ""
        Try

            strsql = "SELECT CodigoSucursal, Codigo_Traslado As Codigo, Bodega_Origen, Bodega_Destino, Comentario, Fecha, Aplicado, Activo, isnull(Referencia,'') as 'Referencia' " & _
                     "FROM Orden_Traslado WHERE CodigoSucursal = " & My.Settings.Sucursal & " and Codigo_Traslado = '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            If tblDatos.Rows.Count <> 0 Then

                txtCodigo.Text = tblDatos.Rows(0).Item("Codigo")
                cmbBdOrigen.SelectedValue = tblDatos.Rows(0).Item("Bodega_Origen")
                cmbBdDestino.SelectedValue = tblDatos.Rows(0).Item("Bodega_Destino")
                txtComentario.Text = tblDatos.Rows(0).Item("Comentario")
                deRegistrado.Value = tblDatos.Rows(0).Item("Fecha")
                Aplicado = tblDatos.Rows(0).Item("Aplicado")
                ckIsActivo.Checked = tblDatos.Rows(0).Item("Activo")
                Referencia = tblDatos.Rows(0).Item("Referencia")

                If Aplicado = True Then
                    txtEstado.Text = "APLICADO"
                    btnAplicar.Enabled = False
                Else
                    txtEstado.Text = "SIN APLICAR"
                    btnAplicar.Enabled = False
                End If

                DesactivarCampos(True)
                CargarDetalle(txtCodigo.Text)
            Else
                LimpiarCampos()
                btnAgregar.Enabled = False
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
            End If
            If UserPerfil = "0002" Then btnAplicar.Enabled = False
            'If Referencia <> "" Then Panel1.Enabled = False : lbEstado.Visible = True : lbReferencia.Text = Referencia : lbReferencia.Visible = True Else Panel1.Enabled = True : lbEstado.Visible = False : lbReferencia.Text = Referencia : lbReferencia.Visible = False
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CargarDetalle(ByVal Codigo As String)
        Dim strsql As String = ""

        Try
            If Aplicado = True Then
                strsql = "SELECT OrdDet.Codigo_Producto AS 'Codigo', Productos.Nombre_Producto AS 'Producto', Und.Descripcion_Unidad As 'Medida', OrdDet.Cantidad, dbo.getCostoProducto(OrdDet.Codigo_Producto) AS 'Costo Unitario', (dbo.getCostoProducto(OrdDet.Codigo_Producto) * OrdDet.Cantidad) AS 'Costo Total' " & _
                     "FROM Orden_Traslado_Detalle AS OrdDet INNER JOIN Productos ON OrdDet.Codigo_Producto = Productos.Codigo_Producto INNER JOIN Unidades_Medida Und ON Productos.Codigo_UnidMedida = Und.Codigo_UnidadMed " & _
                     "WHERE OrdDet.CodigoSucursal = " & My.Settings.Sucursal & " and OrdDet.Codigo_Traslado = '" & Codigo & "'"
            Else
                'NO TOMABA EN CUENTA SI EXISTIA INVENTARIO PARA REALIZAR EL TRASLADO      

                strsql = "SELECT OTD.Codigo_Producto as 'Codigo', PRD.Nombre_Producto as 'Producto', MDD.Descripcion_Unidad as 'Medida', isnull(INV.UndTotal_Existencia,0) as 'Existencias', OTD.Cantidad as 'Cantidad', OTD.Costo_Unitario 'Costo Unitario', OTD.Costo_Total as 'Costo Total' FROM Orden_Traslado_Detalle OTD inner join Inventario INV on INV.Codigo_Producto=OTD.Codigo_Producto and INV.Codigo_Bodega= '" & cmbBdOrigen.SelectedValue & "' inner join Productos PRD on PRD.Codigo_Producto=OTD.Codigo_Producto inner join Unidades_Medida MDD on MDD.Codigo_UnidadMed=PRD.Codigo_UnidMedida " & _
                    "where OTD.CodigoSucursal='" & My.Settings.Sucursal & "' and OTD.Codigo_Traslado='" + Codigo + "'"

                'strsql = "SELECT OrdDet.Codigo_Producto AS 'Codigo', Productos.Nombre_Producto AS 'Producto', Und.Descripcion_Unidad As 'Medida', OrdDet.Cantidad, OrdDet.Costo_Unitario AS 'Costo Unitario', OrdDet.Costo_Total AS 'Costo Total' " & _
                '     "FROM Orden_Traslado_Detalle AS OrdDet INNER JOIN Productos ON OrdDet.Codigo_Producto = Productos.Codigo_Producto INNER JOIN Unidades_Medida Und ON Productos.Codigo_UnidMedida = Und.Codigo_UnidadMed " & _
                '     "WHERE OrdDet.CodigoSucursal = " & My.Settings.Sucursal & " and OrdDet.Codigo_Traslado = '" & Codigo & "'"
            End If

            grdDetalle.DataSource = SQL(strsql, "tblDatosDetalle", My.Settings.SolIndustrialCNX).Tables(0)
            With grvDetalle

                .Columns("Costo Unitario").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                .Columns("Costo Unitario").DisplayFormat.FormatString = Format("C4")

                .Columns("Costo Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                .Columns("Costo Total").DisplayFormat.FormatString = Format("C4")
                .Columns("Costo Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                .Columns("Costo Total").SummaryItem.DisplayFormat = "C$ {0:###,###,##0.0000}"

            End With

            grvDetalle.OptionsView.ShowFooter = True

            If grvDetalle.RowCount <= 0 Then
                btnAgregar.Enabled = True
                btnModificar.Enabled = False
                btnEliminar.Enabled = False
            Else
                btnAgregar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
            End If

            If UserPerfil = "0002" Then btnAplicar.Enabled = False
        Catch ex As Exception
        End Try
    End Sub

    Sub PermitirBotonesEdicion(ByVal bEstado As Boolean)
        frmPrincipal.bbiNuevo.Enabled = bEstado
        frmPrincipal.bbiModificar.Enabled = bEstado
        frmPrincipal.bbiEliminar.Enabled = bEstado
        frmPrincipal.bbiGuardar.Enabled = Not bEstado
        frmPrincipal.bbiCancelar.Enabled = Not bEstado
        frmPrincipal.bbiBuscar.Enabled = bEstado
        frmPrincipal.bbiExportar.Enabled = bEstado

    End Sub

    Private Sub DesactivarCampos(ByVal bIsActivo As Boolean)
        deRegistrado.Enabled = Not bIsActivo
        txtComentario.Enabled = Not bIsActivo
        cmbBdOrigen.Enabled = Not bIsActivo
        cmbBdDestino.Enabled = Not bIsActivo
        'ckIsActivo.Enabled = Not bIsActivo
    End Sub

    Public Sub Nuevo()
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)
        nTipoEdic = 1
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        LimpiarCampos()
        btnAgregar.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Public Sub Modificar()
        nTipoEdic = 2
        PermitirBotonesEdicion(False)
        DesactivarCampos(False)
        cmbBdOrigen.Enabled = False
    End Sub

    Public Sub Eliminar()
        nTipoEdic = 3
        DesactivarCampos(True)
        PermitirBotonesEdicion(False)
    End Sub

    Function Cancelar()
        nTipoEdic = 1
        PermitirBotonesEdicion(True)
        DesactivarCampos(True)
    End Function

    Sub LimpiarCampos()
        txtCodigo.Text = Microsoft.VisualBasic.Right("00000000" & RegistroMaximo(), 8)
        txtComentario.Text = ""
        deRegistrado.Value = Date.Now
        cmbBdOrigen.SelectedIndex = 0
        cmbBdDestino.SelectedIndex = 0
        ckIsActivo.Checked = True
        txtEstado.Text = "SIN APLICAR"
        'btnAplicar.Enabled = False
        grdDetalle.DataSource = Nothing
        'grvDetalle.Columns.Clear()
    End Sub

    Private Sub frmEntradaSalida_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NumCatalogo = 39
        DesactivarCampos(True)
        cargarBodegaOrigen()
        cargarBodegaDestino()
        LimpiarCampos()
        lblExist.BackColor = C_existencias



    End Sub

    Public Sub cargarBodegaOrigen()
        cmbBdOrigen.DataSource = SQL("SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes order by Codigo_Bodega", "tblBodega", My.Settings.SolIndustrialCNX).Tables(0)
        cmbBdOrigen.DisplayMember = "Nombre_Bodega"
        cmbBdOrigen.ValueMember = "Codigo_Bodega"
    End Sub

    Public Sub cargarBodegaDestino()
        cmbBdDestino.DataSource = SQL("SELECT Codigo_Bodega,Nombre_Bodega FROM Almacenes order by Codigo_Bodega", "tblBodega", My.Settings.SolIndustrialCNX).Tables(0)
        cmbBdDestino.DisplayMember = "Nombre_Bodega"
        cmbBdDestino.ValueMember = "Codigo_Bodega"
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Public Sub GuardarMasterDoc()
        Dim Resum As String = ""
        Dim Produccion As New clsProduccion(My.Settings.SolIndustrialCNX)
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        Try

            Dim result As Integer = MessageBox.Show("Desea realizar la acción solicitada?", "Confirmación de Acción", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then

                Resum = Inventario.EditarTraslado(My.Settings.Sucursal, txtCodigo.Text, cmbBdOrigen.SelectedValue, cmbBdDestino.SelectedValue, txtComentario.Text.ToUpper, deRegistrado.Value, False, ckIsActivo.EditValue, nTipoEdic, Nothing)

                If Resum = "OK" Then
                    MsgBox("Proceso realizado correctamente.", MsgBoxStyle.Information, "CyberPlanet.Net")
                    CargarDatos(txtCodigo.Text)
                    Cancelar()
                Else
                    MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "Información")
                End If
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try
    End Sub

    Public Sub NumerosyDecimal(ByVal CajaTexto As Windows.Forms.TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "," And Not CajaTexto.Text.IndexOf(",") Then
            e.Handled = True
        ElseIf e.KeyChar = "," Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub frmRecetas_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Dim strsql As String = "Select * from vwRecetas"
        Dim tblDatosCatalogo As DataTable = Nothing
        frmCatalogo.GridView1.Columns.Clear()
        frmCatalogo.TblCatalogosBS.DataSource = SQL(strsql, "tblDatosCatalogo", My.Settings.SolIndustrialCNX).Tables(0)
    End Sub

    Private Sub cmdAddProv_Click(sender As Object, e As EventArgs)
        nTipoEdic_Ant = nTipoEdic
        NumCatalogo_Ant = NumCatalogo
        nTipoEdic = 1
        NumCatalogo = 9
        frmCatalogo.AbrirEditorCatalogos()
        nTipoEdic = nTipoEdic_Ant
        NumCatalogo = NumCatalogo_Ant
        nTipoEdic_Ant = 0
        NumCatalogo_Ant = 0
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdicDet = 1
        frmAdicional.Text = "Material de Traslado"
        frmAdicional.ShowDialog()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        nTipoEdicDet = 2
        frmAdicional.Text = "Material de Traslado"
        frmAdicional.ShowDialog()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim Resum As String = ""
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        nTipoEdicDet = 3

        Try
            Dim result As Integer = MessageBox.Show("Desea eliminar el registro seleccionado?", "Confirmación de Eliminación", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Dim CodigoProducto As String = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Codigo").ToString()
                Dim Cantidad As Integer = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Cantidad")
                Dim Costo As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Unitario")
                Dim CostoTotal As Double = grvDetalle.GetRowCellValue(grvDetalle.GetSelectedRows(0), "Costo Total")

                Resum = Inventario.EditarTrasladoDeta(My.Settings.Sucursal, txtCodigo.Text, CodigoProducto, Cantidad, Costo, nTipoEdicDet)

                If Resum = "OK" Then
                    CargarDetalle(txtCodigo.Text)
                End If

            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally
        End Try

    End Sub

    Private Sub frmEntradaSalida_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 39
    End Sub

    Private Sub frmEntradaSalida_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub

    Private Sub txtDocumento_KeyPress(sender As Object, e As KeyPressEventArgs)
        'NumerosyDecimal(txtDocumento.text, e)
    End Sub

    Private Sub grdDetalle_DataSourceChanged(sender As Object, e As EventArgs) Handles grdDetalle.DataSourceChanged
        If grvDetalle.RowCount >= 0 And Aplicado = False Then
            btnAplicar.Enabled = True
            Panel1.Enabled = True
        Else
            btnAplicar.Enabled = False
            Panel1.Enabled = False
        End If
    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click
        Dim Resum As String = ""
        Dim Validar As Integer = 0
        Dim Codigo, Cantidad, Costo As String
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

            Try
                Dim result As Integer = MessageBox.Show("Desea aplicar el trasado de materiales?", "Confirmación de Traslado", MessageBoxButtons.YesNo)
                If result = DialogResult.Yes Then

                    CodMaxSalida = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(cmbBdOrigen.SelectedValue, 5), 8)
                    ResumSalida = Inventario.EditarMovimientoAlmacen(CodMaxSalida, My.Settings.Sucursal, txtCodigo.Text, 3047, ("SALIDA DE BODEGA DE " + cmbBdOrigen.Text), 0, Date.Now, 5, False, 1, cmbBdOrigen.SelectedValue)

                    CodMaxEntrada = Microsoft.VisualBasic.Right("00000000" & Inventario.RegMaxMovimiento(cmbBdDestino.SelectedValue, 4), 8)
                    ResumEntrada = Inventario.EditarMovimientoAlmacen(CodMaxEntrada, My.Settings.Sucursal, txtCodigo.Text, 3047, ("ENTRADA A BODEGA DE " + cmbBdDestino.Text), 0, Date.Now, 4, False, 1, cmbBdDestino.SelectedValue)

                    For i As Integer = 0 To grvDetalle.DataRowCount - 1
                        Codigo = grvDetalle.GetRowCellValue(i, "Codigo")
                        Cantidad = grvDetalle.GetRowCellValue(i, "Cantidad")
                        Costo = (grvDetalle.GetRowCellValue(i, "Costo Unitario"))

                        ResumSalidaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxSalida, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", cmbBdOrigen.SelectedValue, False, 1, 5)
                        ResumEntradaDeta = Inventario.EditarMovimientoAlmacenDetalle(CodMaxEntrada, My.Settings.Sucursal, Codigo, Cantidad, Costo, "", cmbBdDestino.SelectedValue, False, 1, 4)

                        If ResumSalidaDeta = "OK" And ResumEntradaDeta = "OK" Then
                            Resum = Inventario.EditarTrasladoDeta(My.Settings.Sucursal, txtCodigo.Text, Codigo, CDbl(Cantidad), (CDbl(Costo) / CDbl(Cantidad)), 2)
                            Validar = Validar + 1
                        End If

                    Next i

                    If Validar = grvDetalle.DataRowCount Then
                        Resum = Inventario.EditarTraslado(My.Settings.Sucursal, txtCodigo.Text, cmbBdOrigen.SelectedValue, cmbBdDestino.SelectedValue, txtComentario.Text.ToUpper, deRegistrado.Value, True, ckIsActivo.EditValue, 2, Referencia)
                        MsgBox("Proceso realizado correctamente.", MsgBoxStyle.Information, "Mensaje de Información")
                        CargarDatos(txtCodigo.Text)
                    Else
                        MsgBox("Ocurrio un problema al realizar la acción, favor comunicarse con el Dpto. de Informatica.", MsgBoxStyle.Critical, "Error de Sistema")
                    End If

                End If
            Catch ex As Exception
                Call MsgBox("Error: " + ex.Message)
            Finally
            End Try

    End Sub


    Private Sub grvDetProyeccion_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles grvDetalle.RowStyle

        Dim view As ColumnView = DirectCast(sender, ColumnView)

        If view.IsValidRowHandle(e.RowHandle) Then

            Dim Existencia As Decimal = view.GetRowCellValue(e.RowHandle, "Existencias")
            Dim Requerido As Decimal = view.GetRowCellValue(e.RowHandle, "Cantidad")
            Dim Result As Decimal = Existencia - Requerido

            If Result < 0 Then e.Appearance.BackColor = C_existencias : btnAplicar.Enabled = False


        End If
    End Sub
End Class