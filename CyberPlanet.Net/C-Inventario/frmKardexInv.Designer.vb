﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKardexInv
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmKardexInv))
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.deInicio = New DevExpress.XtraEditors.DateEdit()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.cmbBodega = New System.Windows.Forms.ComboBox()
        Me.deFin = New DevExpress.XtraEditors.DateEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.lblNombreProd = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblMaximo = New System.Windows.Forms.Label()
        Me.lblMinimo = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.grdKardex = New DevExpress.XtraGrid.GridControl()
        Me.grvKardex = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.mainPanel.SuspendLayout()
        CType(Me.deInicio.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deInicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdKardex, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvKardex, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.deInicio)
        Me.mainPanel.Controls.Add(Me.lblFechaIngreso)
        Me.mainPanel.Controls.Add(Me.cmbBodega)
        Me.mainPanel.Controls.Add(Me.deFin)
        Me.mainPanel.Controls.Add(Me.Label6)
        Me.mainPanel.Controls.Add(Me.Label3)
        Me.mainPanel.Controls.Add(Me.lblProveedor)
        Me.mainPanel.Controls.Add(Me.lblCodigo)
        Me.mainPanel.Controls.Add(Me.lblNombreProd)
        Me.mainPanel.Controls.Add(Me.Label5)
        Me.mainPanel.Controls.Add(Me.GroupBox2)
        Me.mainPanel.Controls.Add(Me.grdKardex)
        Me.mainPanel.Controls.Add(Me.btnGenerar)
        Me.mainPanel.Controls.Add(Me.Label1)
        Me.mainPanel.Location = New System.Drawing.Point(0, 0)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1160, 510)
        Me.mainPanel.TabIndex = 0
        '
        'deInicio
        '
        Me.deInicio.EditValue = Nothing
        Me.deInicio.Location = New System.Drawing.Point(671, 10)
        Me.deInicio.Name = "deInicio"
        Me.deInicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deInicio.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deInicio.Size = New System.Drawing.Size(140, 20)
        Me.deInicio.TabIndex = 64
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.Location = New System.Drawing.Point(602, 13)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(74, 16)
        Me.lblFechaIngreso.TabIndex = 65
        Me.lblFechaIngreso.Text = "Fecha Inicio:"
        '
        'cmbBodega
        '
        Me.cmbBodega.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBodega.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBodega.FormattingEnabled = True
        Me.cmbBodega.Location = New System.Drawing.Point(671, 54)
        Me.cmbBodega.Name = "cmbBodega"
        Me.cmbBodega.Size = New System.Drawing.Size(221, 21)
        Me.cmbBodega.TabIndex = 83
        '
        'deFin
        '
        Me.deFin.EditValue = Nothing
        Me.deFin.Location = New System.Drawing.Point(888, 10)
        Me.deFin.Name = "deFin"
        Me.deFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFin.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFin.Size = New System.Drawing.Size(140, 20)
        Me.deFin.TabIndex = 69
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(602, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 82
        Me.Label6.Text = "Bodega:"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(830, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 16)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Fecha Fin:"
        '
        'lblProveedor
        '
        Me.lblProveedor.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblProveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProveedor.Location = New System.Drawing.Point(63, 52)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(228, 20)
        Me.lblProveedor.TabIndex = 81
        Me.lblProveedor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCodigo
        '
        Me.lblCodigo.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblCodigo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCodigo.Location = New System.Drawing.Point(63, 9)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(100, 20)
        Me.lblCodigo.TabIndex = 78
        Me.lblCodigo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNombreProd
        '
        Me.lblNombreProd.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblNombreProd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNombreProd.Location = New System.Drawing.Point(168, 9)
        Me.lblNombreProd.Name = "lblNombreProd"
        Me.lblNombreProd.Size = New System.Drawing.Size(417, 20)
        Me.lblNombreProd.TabIndex = 77
        Me.lblNombreProd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 74
        Me.Label5.Text = "Proveedor:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblMaximo)
        Me.GroupBox2.Controls.Add(Me.lblMinimo)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(312, 35)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(273, 46)
        Me.GroupBox2.TabIndex = 73
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Existencias del Producto"
        '
        'lblMaximo
        '
        Me.lblMaximo.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblMaximo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMaximo.Location = New System.Drawing.Point(185, 18)
        Me.lblMaximo.Name = "lblMaximo"
        Me.lblMaximo.Size = New System.Drawing.Size(70, 20)
        Me.lblMaximo.TabIndex = 80
        Me.lblMaximo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMinimo
        '
        Me.lblMinimo.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblMinimo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMinimo.Location = New System.Drawing.Point(57, 19)
        Me.lblMinimo.Name = "lblMinimo"
        Me.lblMinimo.Size = New System.Drawing.Size(70, 20)
        Me.lblMinimo.TabIndex = 79
        Me.lblMinimo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(133, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Maxima:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "Minima:"
        '
        'grdKardex
        '
        Me.grdKardex.Location = New System.Drawing.Point(0, 89)
        Me.grdKardex.MainView = Me.grvKardex
        Me.grdKardex.Name = "grdKardex"
        Me.grdKardex.Size = New System.Drawing.Size(1160, 403)
        Me.grdKardex.TabIndex = 72
        Me.grdKardex.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvKardex})
        '
        'grvKardex
        '
        Me.grvKardex.GridControl = Me.grdKardex
        Me.grvKardex.Name = "grvKardex"
        Me.grvKardex.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvKardex.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvKardex.OptionsBehavior.Editable = False
        Me.grvKardex.OptionsCustomization.AllowSort = False
        Me.grvKardex.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvKardex.OptionsView.AllowHtmlDrawGroups = False
        Me.grvKardex.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvKardex.OptionsView.ShowFooter = True
        Me.grvKardex.OptionsView.ShowGroupPanel = False
        '
        'btnGenerar
        '
        Me.btnGenerar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnGenerar.Image = CType(resources.GetObject("btnGenerar.Image"), System.Drawing.Image)
        Me.btnGenerar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerar.Location = New System.Drawing.Point(918, 49)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(110, 27)
        Me.btnGenerar.TabIndex = 71
        Me.btnGenerar.Text = "&Generar Kardex"
        Me.btnGenerar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Producto:"
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'frmKardexInv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1161, 494)
        Me.Controls.Add(Me.mainPanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmKardexInv"
        Me.Text = "Kardex de Inventario"
        Me.mainPanel.ResumeLayout(False)
        Me.mainPanel.PerformLayout()
        CType(Me.deInicio.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deInicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.grdKardex, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvKardex, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents deInicio As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents grdKardex As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvKardex As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents deFin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblNombreProd As System.Windows.Forms.Label
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents lblMinimo As System.Windows.Forms.Label
    Friend WithEvents lblMaximo As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents cmbBodega As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
End Class
