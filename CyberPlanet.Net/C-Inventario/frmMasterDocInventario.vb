﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmMasterDocInventario

Private Sub frmMasterDocInventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
	deFechaDoc.Value = Now.Date
	CargarConceptos()
	CargarBodegas()
	If nTipoEdicDoc = 1 Then
			Dim strNum As String = "0000000000" & SiguienteDocumentoInv()
			Dim nlong As Integer = Len(strNum)
			txtNumDoc.Text = strNum.Substring((nlong - 10), 10)
			lblSerie.Text = SerieDocumentoInv()
	ElseIf nTipoEdicDoc = 2 Then
			CargarDatos()
	ElseIf nTipoEdicDoc = 3 Then
			CargarDatos()
			Panel2.Enabled = False
	End If
	CargarDetalleDoc()
End Sub

Sub CargarDatos()
        Dim sNumDoc As String = ""
        Dim sSerie As String = ""

    If frmDetalleFactura.TblBSDocumentos.Count > 0 Then
      Dim registro As DataRowView = frmDetalleFactura.TblBSDocumentos.Current
      sNumDoc = registro.Item("N°")
      sSerie = registro.Item("Serie")
    End If

        Dim strSql As String = ""
    If TipoDocumento = 1 Then       'Entrada
      strSql = "Select * from vw_Entradas where "
    ElseIf TipoDocumento = 2 Then   'Salida
      strSql = "Select * from vw_Salidas where "
    ElseIf TipoDocumento = 5 Then   'Transferencias
      strSql = "Select * from vw_Transferencias where "
    End If
    strSql = strSql & " [N°]= '" & sNumDoc & "' and Serie = '" & sSerie & "'"


        Dim dtTblDoc As DataTable = SQL(strSql, "tblNumDoc", My.Settings.SolIndustrialCNX).Tables(0)
        If dtTblDoc.Rows.Count > 0 Then
            txtNumDoc.Text = dtTblDoc.Rows(0).Item(0)
            lblSerie.Text = dtTblDoc.Rows(0).Item(1)
            deFechaDoc.Text = dtTblDoc.Rows(0).Item(2)
            luConceptoOperacion.Text = dtTblDoc.Rows(0).Item(3)
            txtDescripcion.Text = dtTblDoc.Rows(0).Item(4)
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Cerrar()
    End Sub

    Public Sub Cerrar()
        Me.Dispose()
        Close()
    End Sub

    Public Sub CargarBodegas()
        Dim strSqlBodegas As String
        strSqlBodegas = "Select Id_Bodega, Nombre_Bodega from Tbl_Bodega order by Id_Bodega"
        If TipoDocumento = 1 Then
            Label3.Visible = True
            luBodegaOrigen.Visible = True
            Label6.Visible = False
            luBodegaDestino.Visible = False
        ElseIf TipoDocumento = 2 Then
            Label3.Visible = True
            luBodegaOrigen.Visible = True
            Label6.Visible = False
            luBodegaDestino.Visible = False
        ElseIf TipoDocumento = 5 Then
            Label3.Visible = True
            luBodegaOrigen.Visible = True
            Label6.Visible = True
            luBodegaDestino.Visible = True
        End If
        TblBodegasBindingSource.DataSource = SQL(strSqlBodegas, "tblBodegas", My.Settings.SolIndustrialCNX).Tables(0)
        luBodegaOrigen.Properties.DataSource = TblBodegasBindingSource
        luBodegaOrigen.Properties.DisplayMember = "Nombre_Bodega"
        luBodegaOrigen.Properties.ValueMember = "Id_Bodega"
        luBodegaOrigen.ItemIndex = 0

        luBodegaDestino.Properties.DataSource = TblBodegasBindingSource
        luBodegaDestino.Properties.DisplayMember = "Nombre_Bodega"
        luBodegaDestino.Properties.ValueMember = "Id_Bodega"
        luBodegaDestino.ItemIndex = 0

    End Sub

    Public Sub CargarConceptos()
        Dim strSqlTipConcep As String = ""
        If TipoDocumento = 1 Then
            strSqlTipConcep = "Select CodigoCateg, Descripcion from Tbl_ConceptosMovimientos where Tipo=1 order by CodigoCateg"
        ElseIf TipoDocumento = 2 Then
            strSqlTipConcep = "Select CodigoCateg, Descripcion from Tbl_ConceptosMovimientos where Tipo=2 order by CodigoCateg"
        Else
            Label2.Visible = False
            luConceptoOperacion.Visible = False
        End If
        If luConceptoOperacion.Visible Then
            TblConceptosBindingSource.DataSource = SQL(strSqlTipConcep, "tblConceptos", My.Settings.SolIndustrialCNX).Tables(0)
            luConceptoOperacion.Properties.DataSource = TblConceptosBindingSource
            luConceptoOperacion.Properties.DisplayMember = "Descripcion"
            luConceptoOperacion.Properties.ValueMember = "CodigoCateg"
            luConceptoOperacion.ItemIndex = 0
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim Resum As String = ""
        Dim MasterDoc As New clsDocumentos(My.Settings.SolIndustrialCNX)

        Try
            Resum = MasterDoc.EditarMasterDoc(nSucursal, txtNumDoc.Text, lblSerie.Text, TipoDocumento, CDate(deFechaDoc.Text), txtDescripcion.Text, TipoDocumento, nTipoEdicDoc)
            If Resum = "OK" Then
                btnAgregar.Enabled = True
                btnModificar.Enabled = True
                btnEliminar.Enabled = True
                frmDetalleFactura.CargarDataDocumentos()
            Else
                MsgBox("Ha ocurrido un error en la aplicacion de esta tarea, intentelo de nuevo o consulte con el Administrador del sistema.", MsgBoxStyle.Information, "SIGCA")
            End If

        Catch ex As Exception
            Call MsgBox("Error: " + ex.Message)
        Finally

        End Try
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        nTipoEdicDet = 1
        frmDetalleProducto.ShowDialog()
    End Sub

    Public Sub CargarDetalleDoc()
        Dim StrsqlDetDoc As String = ""
        If TipoDocumento = 1 Then               'Entrada
            StrsqlDetDoc = "Select * from vw_DetalleEntrada where "
        ElseIf TipoDocumento = 2 Then       'Salida
            StrsqlDetDoc = "Select * from vw_DetalleSalida where "
        ElseIf TipoDocumento = 3 Then       'Ventas
            StrsqlDetDoc = "Select * from vw_DetalleVenta where "
        ElseIf TipoDocumento = 4 Then       'Compras
            StrsqlDetDoc = "Select * from vw_DetalleCompras where "
        ElseIf TipoDocumento = 5 Then       'Transferencias
            StrsqlDetDoc = "Select * from vw_DetalleTransferencia where "
        End If

        StrsqlDetDoc = StrsqlDetDoc & " Documento = '" & txtNumDoc.Text & "'"
        If TipoDocumento = 1 Or TipoDocumento = 2 Or TipoDocumento = 5 Then
            StrsqlDetDoc = StrsqlDetDoc & " and Serie = '" & lblSerie.Text & "'"
        ElseIf TipoDocumento = 3 Then
            StrsqlDetDoc = StrsqlDetDoc & " and Serie_Factura = '" & lblSerie.Text & "'"
        End If
        'StrsqlDetDoc = StrsqlDetDoc & " order by Documento desc"
        TblDetalleDocBindingSource.DataSource = SQL(StrsqlDetDoc, "tblDatosTransacciones", My.Settings.SolIndustrialCNX).Tables(0)
        If TipoDocumento = 3 Then
            EncabezadosDetDocVenta()
        Else
            EncabezadosDetDoc()
        End If
        If TblDetalleDocBindingSource.Count = 0 Then
            btnAgregar.Enabled = True
            btnModificar.Enabled = False
            btnEliminar.Enabled = False
        Else
            btnAgregar.Enabled = True
            btnModificar.Enabled = True
            btnEliminar.Enabled = True
        End If
    End Sub

	Sub EncabezadosDetDoc()
			GridView1.Columns.Clear()
			GridView1.OptionsView.ShowFooter = True

			Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
			gc0.Name = "Id_Producto"
			gc0.Caption = "Id_Producto"
			gc0.FieldName = "Id_Producto"
			gc0.Width = 20
			GridView1.Columns.Add(gc0)
			gc0.Visible = True
			gc0.VisibleIndex = 0
			GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

			Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
			gc1.Name = "Nombre_Producto"
			gc1.Caption = "Nombre_Producto"
			gc1.FieldName = "Nombre_Producto"
			gc1.Width = 60
			GridView1.Columns.Add(gc1)
			gc1.Visible = True
			gc1.VisibleIndex = 1

			Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
			gc2.Name = "Cantidad"
			gc2.Caption = "Cantidad"
			gc2.FieldName = "Cantidad"
			gc2.Width = 10
			GridView1.Columns.Add(gc2)
			gc2.Visible = True
			gc2.VisibleIndex = 2

			Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
			gc3.Name = "Costo_Unitario"
			gc3.Caption = "Costo_Unitario"
			gc3.FieldName = "Costo_Unitario"
			gc3.DisplayFormat.FormatString = "{0:$0.00}"
			gc3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
			gc3.Width = 10
			GridView1.Columns.Add(gc3)
			gc3.Visible = True
			gc3.VisibleIndex = 3

			Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
			gc4.Name = "SubTotal"
			gc4.Caption = "SubTotal"
			gc4.FieldName = "SubTotal"
			gc4.DisplayFormat.FormatString = "{0:$0.00}"
			gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
			gc4.Width = 10
			GridView1.Columns.Add(gc4)
			gc4.Visible = True
			gc4.VisibleIndex = 4
			GridView1.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
			GridView1.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"
	End Sub

	Sub EncabezadosDetDocVenta()
			GridView1.Columns.Clear()
			GridView1.OptionsView.ShowFooter = True

			Dim gc0 As New DevExpress.XtraGrid.Columns.GridColumn
			gc0.Name = "Id_Producto"
			gc0.Caption = "Id_Producto"
			gc0.FieldName = "Id_Producto"
			gc0.Width = 20
			GridView1.Columns.Add(gc0)
			gc0.Visible = True
			gc0.VisibleIndex = 0
			GridView1.Columns(0).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count

			Dim gc1 As New DevExpress.XtraGrid.Columns.GridColumn
			gc1.Name = "Nombre_Producto"
			gc1.Caption = "Nombre_Producto"
			gc1.FieldName = "Nombre_Producto"
			gc1.Width = 60
			GridView1.Columns.Add(gc1)
			gc1.Visible = True
			gc1.VisibleIndex = 1

			Dim gc2 As New DevExpress.XtraGrid.Columns.GridColumn
			gc2.Name = "Cantidad"
			gc2.Caption = "Cantidad"
			gc2.FieldName = "Cantidad"
			gc2.Width = 10
			GridView1.Columns.Add(gc2)
			gc2.Visible = True
			gc2.VisibleIndex = 2

			Dim gc3 As New DevExpress.XtraGrid.Columns.GridColumn
			gc3.Name = "Costo_Unitario"
			gc3.Caption = "Costo_Unitario"
			gc3.FieldName = "Costo_Unitario"
			gc3.DisplayFormat.FormatString = "{0:$0.00}"
			gc3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
			gc3.Width = 10
			GridView1.Columns.Add(gc3)
			gc3.Visible = True
			gc3.VisibleIndex = 3

			Dim gc4 As New DevExpress.XtraGrid.Columns.GridColumn
			gc4.Name = "Precio_Venta"
			gc4.Caption = "Precio_Venta"
			gc4.FieldName = "Precio_Venta"
			gc4.DisplayFormat.FormatString = "{0:$0.00}"
			gc4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
			gc4.Width = 10
			GridView1.Columns.Add(gc4)
			gc4.Visible = True
			gc4.VisibleIndex = 4
			GridView1.Columns(4).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
			GridView1.Columns(4).SummaryItem.DisplayFormat = "{0:$0.00}"

			Dim gc5 As New DevExpress.XtraGrid.Columns.GridColumn
			gc5.Name = "SubTotal_Costo"
			gc5.Caption = "SubTotal_Costo"
			gc5.FieldName = "SubTotal_Costo"
			gc5.DisplayFormat.FormatString = "{0:$0.00}"
			gc5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
			gc5.Width = 10
			GridView1.Columns.Add(gc5)
			gc5.Visible = True
			gc5.VisibleIndex = 5

			Dim gc6 As New DevExpress.XtraGrid.Columns.GridColumn
			gc6.Name = "SubTotal_Venta"
			gc6.Caption = "SubTotal_Venta"
			gc6.FieldName = "SubTotal_Venta"
			gc6.DisplayFormat.FormatString = "{0:$0.00}"
			gc6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
			gc6.Width = 10
			GridView1.Columns.Add(gc6)
			gc6.Visible = True
			gc6.VisibleIndex = 6
			GridView1.Columns(6).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
			GridView1.Columns(6).SummaryItem.DisplayFormat = "{0:$0.00}"

	End Sub


Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
		nTipoEdicDet = 2
End Sub

Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
		nTipoEdicDet = 3
End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick

    End Sub
End Class