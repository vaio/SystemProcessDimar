﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTraslados
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTraslados))
        Me.gpbEncabezado = New System.Windows.Forms.GroupBox()
        Me.lbReferencia = New System.Windows.Forms.Label()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtComentario = New System.Windows.Forms.TextBox()
        Me.cmbBdDestino = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbBdOrigen = New System.Windows.Forms.ComboBox()
        Me.txtBodega = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.deRegistrado = New System.Windows.Forms.DateTimePicker()
        Me.ckIsActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.grbDetalleProd = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.grdDetalle = New DevExpress.XtraGrid.GridControl()
        Me.grvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.mainPanel = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblExist = New System.Windows.Forms.Label()
        Me.gpbEncabezado.SuspendLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbDetalleProd.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mainPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbEncabezado
        '
        Me.gpbEncabezado.Controls.Add(Me.lbReferencia)
        Me.gpbEncabezado.Controls.Add(Me.lbEstado)
        Me.gpbEncabezado.Controls.Add(Me.txtEstado)
        Me.gpbEncabezado.Controls.Add(Me.Label1)
        Me.gpbEncabezado.Controls.Add(Me.txtComentario)
        Me.gpbEncabezado.Controls.Add(Me.cmbBdDestino)
        Me.gpbEncabezado.Controls.Add(Me.Label2)
        Me.gpbEncabezado.Controls.Add(Me.cmbBdOrigen)
        Me.gpbEncabezado.Controls.Add(Me.txtBodega)
        Me.gpbEncabezado.Controls.Add(Me.txtCodigo)
        Me.gpbEncabezado.Controls.Add(Me.lblCodigo)
        Me.gpbEncabezado.Controls.Add(Me.deRegistrado)
        Me.gpbEncabezado.Controls.Add(Me.ckIsActivo)
        Me.gpbEncabezado.Controls.Add(Me.Label6)
        Me.gpbEncabezado.Controls.Add(Me.Label4)
        Me.gpbEncabezado.Location = New System.Drawing.Point(3, 3)
        Me.gpbEncabezado.Name = "gpbEncabezado"
        Me.gpbEncabezado.Size = New System.Drawing.Size(1006, 88)
        Me.gpbEncabezado.TabIndex = 47
        Me.gpbEncabezado.TabStop = False
        Me.gpbEncabezado.Text = "Encabezado - Datos Generales"
        '
        'lbReferencia
        '
        Me.lbReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbReferencia.ForeColor = System.Drawing.SystemColors.Desktop
        Me.lbReferencia.Location = New System.Drawing.Point(903, -3)
        Me.lbReferencia.Name = "lbReferencia"
        Me.lbReferencia.Size = New System.Drawing.Size(93, 20)
        Me.lbReferencia.TabIndex = 87
        Me.lbReferencia.Text = "00000001"
        Me.lbReferencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbReferencia.Visible = False
        '
        'lbEstado
        '
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbEstado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbEstado.Location = New System.Drawing.Point(817, -3)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(91, 20)
        Me.lbEstado.TabIndex = 86
        Me.lbEstado.Text = "Referencia:"
        Me.lbEstado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbEstado.Visible = False
        '
        'txtEstado
        '
        Me.txtEstado.BackColor = System.Drawing.Color.White
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstado.Enabled = False
        Me.txtEstado.Location = New System.Drawing.Point(821, 51)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(175, 20)
        Me.txtEstado.TabIndex = 81
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(776, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 80
        Me.Label1.Text = "Estado:"
        '
        'txtComentario
        '
        Me.txtComentario.BackColor = System.Drawing.Color.White
        Me.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComentario.Location = New System.Drawing.Point(73, 51)
        Me.txtComentario.Name = "txtComentario"
        Me.txtComentario.Size = New System.Drawing.Size(693, 20)
        Me.txtComentario.TabIndex = 79
        '
        'cmbBdDestino
        '
        Me.cmbBdDestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBdDestino.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBdDestino.BackColor = System.Drawing.Color.White
        Me.cmbBdDestino.FormattingEnabled = True
        Me.cmbBdDestino.Location = New System.Drawing.Point(587, 20)
        Me.cmbBdDestino.Name = "cmbBdDestino"
        Me.cmbBdDestino.Size = New System.Drawing.Size(179, 21)
        Me.cmbBdDestino.TabIndex = 78
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(495, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Bodega Destino:"
        '
        'cmbBdOrigen
        '
        Me.cmbBdOrigen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBdOrigen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBdOrigen.BackColor = System.Drawing.Color.White
        Me.cmbBdOrigen.FormattingEnabled = True
        Me.cmbBdOrigen.Location = New System.Drawing.Point(304, 19)
        Me.cmbBdOrigen.Name = "cmbBdOrigen"
        Me.cmbBdOrigen.Size = New System.Drawing.Size(179, 21)
        Me.cmbBdOrigen.TabIndex = 74
        '
        'txtBodega
        '
        Me.txtBodega.AutoSize = True
        Me.txtBodega.Location = New System.Drawing.Point(212, 22)
        Me.txtBodega.Name = "txtBodega"
        Me.txtBodega.Size = New System.Drawing.Size(81, 13)
        Me.txtBodega.TabIndex = 73
        Me.txtBodega.Text = "Bodega Origen:"
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.White
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(73, 21)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(117, 20)
        Me.txtCodigo.TabIndex = 68
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(8, 24)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 69
        Me.lblCodigo.Text = "Codigo:"
        '
        'deRegistrado
        '
        Me.deRegistrado.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.deRegistrado.Location = New System.Drawing.Point(822, 21)
        Me.deRegistrado.Name = "deRegistrado"
        Me.deRegistrado.Size = New System.Drawing.Size(98, 20)
        Me.deRegistrado.TabIndex = 57
        '
        'ckIsActivo
        '
        Me.ckIsActivo.Enabled = False
        Me.ckIsActivo.Location = New System.Drawing.Point(944, 21)
        Me.ckIsActivo.Name = "ckIsActivo"
        Me.ckIsActivo.Properties.Caption = "Activo"
        Me.ckIsActivo.Size = New System.Drawing.Size(52, 18)
        Me.ckIsActivo.TabIndex = 35
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(776, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "Fecha:"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 17)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Comentario:"
        '
        'grbDetalleProd
        '
        Me.grbDetalleProd.Controls.Add(Me.Panel3)
        Me.grbDetalleProd.Controls.Add(Me.grdDetalle)
        Me.grbDetalleProd.Location = New System.Drawing.Point(3, 120)
        Me.grbDetalleProd.Name = "grbDetalleProd"
        Me.grbDetalleProd.Size = New System.Drawing.Size(1009, 382)
        Me.grbDetalleProd.TabIndex = 48
        Me.grbDetalleProd.TabStop = False
        Me.grbDetalleProd.Text = "Detalle del Traslado"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnAplicar)
        Me.Panel3.Controls.Add(Me.Panel1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 327)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1003, 52)
        Me.Panel3.TabIndex = 57
        '
        'btnAplicar
        '
        Me.btnAplicar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnAplicar.Enabled = False
        Me.btnAplicar.Image = CType(resources.GetObject("btnAplicar.Image"), System.Drawing.Image)
        Me.btnAplicar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAplicar.Location = New System.Drawing.Point(879, 5)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(114, 40)
        Me.btnAplicar.TabIndex = 4
        Me.btnAplicar.Text = "Aplicar Traslados"
        Me.btnAplicar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Controls.Add(Me.btnModificar)
        Me.Panel1.Controls.Add(Me.btnAgregar)
        Me.Panel1.Location = New System.Drawing.Point(1, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(284, 50)
        Me.Panel1.TabIndex = 1
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(190, 5)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(83, 40)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Enabled = False
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(101, 5)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(83, 40)
        Me.btnModificar.TabIndex = 1
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Enabled = False
        Me.btnAgregar.Image = CType(resources.GetObject("btnAgregar.Image"), System.Drawing.Image)
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(11, 5)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(83, 40)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'grdDetalle
        '
        Me.grdDetalle.Location = New System.Drawing.Point(15, 19)
        Me.grdDetalle.MainView = Me.grvDetalle
        Me.grdDetalle.Name = "grdDetalle"
        Me.grdDetalle.Size = New System.Drawing.Size(981, 293)
        Me.grdDetalle.TabIndex = 52
        Me.grdDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvDetalle, Me.GridView1})
        '
        'grvDetalle
        '
        Me.grvDetalle.GridControl = Me.grdDetalle
        Me.grvDetalle.Name = "grvDetalle"
        Me.grvDetalle.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalle.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.grvDetalle.OptionsBehavior.Editable = False
        Me.grvDetalle.OptionsBehavior.ReadOnly = True
        Me.grvDetalle.OptionsCustomization.AllowColumnMoving = False
        Me.grvDetalle.OptionsCustomization.AllowColumnResizing = False
        Me.grvDetalle.OptionsCustomization.AllowGroup = False
        Me.grvDetalle.OptionsMenu.EnableGroupPanelMenu = False
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.grvDetalle.OptionsSelection.EnableAppearanceFocusedRow = False
        Me.grvDetalle.OptionsView.AllowHtmlDrawGroups = False
        Me.grvDetalle.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.grvDetalle.OptionsView.ShowFooter = True
        Me.grvDetalle.OptionsView.ShowGroupPanel = False
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.grdDetalle
        Me.GridView1.Name = "GridView1"
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.Label10)
        Me.mainPanel.Controls.Add(Me.Label11)
        Me.mainPanel.Controls.Add(Me.Label15)
        Me.mainPanel.Controls.Add(Me.lblExist)
        Me.mainPanel.Controls.Add(Me.grbDetalleProd)
        Me.mainPanel.Controls.Add(Me.gpbEncabezado)
        Me.mainPanel.Location = New System.Drawing.Point(-2, -2)
        Me.mainPanel.Name = "mainPanel"
        Me.mainPanel.Size = New System.Drawing.Size(1012, 505)
        Me.mainPanel.TabIndex = 48
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(812, 104)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(82, 13)
        Me.Label10.TabIndex = 94
        Me.Label10.Text = "Con Existencias"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Location = New System.Drawing.Point(798, 104)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(14, 14)
        Me.Label11.TabIndex = 93
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(926, 104)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(73, 13)
        Me.Label15.TabIndex = 92
        Me.Label15.Text = "Sin Existencia"
        '
        'lblExist
        '
        Me.lblExist.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.lblExist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblExist.Location = New System.Drawing.Point(912, 104)
        Me.lblExist.Name = "lblExist"
        Me.lblExist.Size = New System.Drawing.Size(14, 14)
        Me.lblExist.TabIndex = 91
        '
        'frmTraslados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1009, 500)
        Me.Controls.Add(Me.mainPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTraslados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entradas/Salidas de Inventario"
        Me.gpbEncabezado.ResumeLayout(False)
        Me.gpbEncabezado.PerformLayout()
        CType(Me.ckIsActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbDetalleProd.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.grdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mainPanel.ResumeLayout(False)
        Me.mainPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gpbEncabezado As System.Windows.Forms.GroupBox
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ckIsActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grbDetalleProd As System.Windows.Forms.GroupBox
    Friend WithEvents grdDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents mainPanel As System.Windows.Forms.Panel
    Friend WithEvents cmbBdOrigen As System.Windows.Forms.ComboBox
    Friend WithEvents txtBodega As System.Windows.Forms.Label
    Friend WithEvents deRegistrado As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents cmbBdDestino As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
    Friend WithEvents txtComentario As System.Windows.Forms.TextBox
    Friend WithEvents txtEstado As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lbReferencia As System.Windows.Forms.Label
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblExist As System.Windows.Forms.Label
End Class
