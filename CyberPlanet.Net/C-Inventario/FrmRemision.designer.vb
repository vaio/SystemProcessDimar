﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRemision
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRemision))
        Me.tabPrincipal = New System.Windows.Forms.TabControl()
        Me.tabRemsion = New System.Windows.Forms.TabPage()
        Me.Pcomando = New System.Windows.Forms.Panel()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgvRemisiones = New System.Windows.Forms.DataGridView()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lbcodigo = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtExistencia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.dgvRemision = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabVentas = New System.Windows.Forms.TabPage()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtvExistencia = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtvCantidad = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtvCliente = New System.Windows.Forms.TextBox()
        Me.dgvVenta2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnVguardar = New System.Windows.Forms.Button()
        Me.btnVcancelar = New System.Windows.Forms.Button()
        Me.btnVagregar = New System.Windows.Forms.Button()
        Me.btnVborrar = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.dgvVentas = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvVenta = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnVmas = New System.Windows.Forms.Button()
        Me.btnVmenos = New System.Windows.Forms.Button()
        Me.tabEntrada = New System.Windows.Forms.TabPage()
        Me.btnEmas = New System.Windows.Forms.Button()
        Me.btnEmenos = New System.Windows.Forms.Button()
        Me.dgvEntrada2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvEntrada = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvEntradas = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnEguardar = New System.Windows.Forms.Button()
        Me.btnEcancelar = New System.Windows.Forms.Button()
        Me.btnEagregar = New System.Windows.Forms.Button()
        Me.btnEborrar = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lbeCodigo = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txteExistencia = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txteCantidad = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txteProducto = New System.Windows.Forms.TextBox()
        Me.Devolucion = New System.Windows.Forms.TabPage()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.DataGridView5 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.DataGridView6 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabRecepcionRecibo = New System.Windows.Forms.TabPage()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.DataGridView7 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.DataGridView8 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pPrincipal = New System.Windows.Forms.Panel()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtdestino = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtruta = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtvehiculo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtsupervisor = New System.Windows.Forms.TextBox()
        Me.pDeco = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Psupervisor = New System.Windows.Forms.Panel()
        Me.dgvSupervisor = New System.Windows.Forms.DataGridView()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lbTipo = New System.Windows.Forms.Label()
        Me.pVehiculo = New System.Windows.Forms.Panel()
        Me.dgvVehiculo = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pRuta = New System.Windows.Forms.Panel()
        Me.dgvRuta = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pProducto = New System.Windows.Forms.Panel()
        Me.dgvProducto = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pMain = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lbRemision = New System.Windows.Forms.Label()
        Me.tabPrincipal.SuspendLayout()
        Me.tabRemsion.SuspendLayout()
        Me.Pcomando.SuspendLayout()
        CType(Me.dgvRemisiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabVentas.SuspendLayout()
        CType(Me.dgvVenta2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabEntrada.SuspendLayout()
        CType(Me.dgvEntrada2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Devolucion.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabRecepcionRecibo.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pPrincipal.SuspendLayout()
        Me.pDeco.SuspendLayout()
        Me.Psupervisor.SuspendLayout()
        CType(Me.dgvSupervisor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pVehiculo.SuspendLayout()
        CType(Me.dgvVehiculo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pRuta.SuspendLayout()
        CType(Me.dgvRuta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pProducto.SuspendLayout()
        CType(Me.dgvProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabPrincipal
        '
        Me.tabPrincipal.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.tabPrincipal.Controls.Add(Me.tabRemsion)
        Me.tabPrincipal.Controls.Add(Me.tabVentas)
        Me.tabPrincipal.Controls.Add(Me.tabEntrada)
        Me.tabPrincipal.Controls.Add(Me.Devolucion)
        Me.tabPrincipal.Controls.Add(Me.tabRecepcionRecibo)
        Me.tabPrincipal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabPrincipal.Location = New System.Drawing.Point(0, 0)
        Me.tabPrincipal.Name = "tabPrincipal"
        Me.tabPrincipal.SelectedIndex = 0
        Me.tabPrincipal.Size = New System.Drawing.Size(874, 502)
        Me.tabPrincipal.TabIndex = 1
        Me.tabPrincipal.TabStop = False
        '
        'tabRemsion
        '
        Me.tabRemsion.Controls.Add(Me.Pcomando)
        Me.tabRemsion.Controls.Add(Me.Label10)
        Me.tabRemsion.Controls.Add(Me.dgvRemisiones)
        Me.tabRemsion.Controls.Add(Me.lbcodigo)
        Me.tabRemsion.Controls.Add(Me.Label8)
        Me.tabRemsion.Controls.Add(Me.txtExistencia)
        Me.tabRemsion.Controls.Add(Me.Label6)
        Me.tabRemsion.Controls.Add(Me.txtCantidad)
        Me.tabRemsion.Controls.Add(Me.Label3)
        Me.tabRemsion.Controls.Add(Me.txtProducto)
        Me.tabRemsion.Controls.Add(Me.dgvRemision)
        Me.tabRemsion.Location = New System.Drawing.Point(4, 25)
        Me.tabRemsion.Name = "tabRemsion"
        Me.tabRemsion.Padding = New System.Windows.Forms.Padding(3)
        Me.tabRemsion.Size = New System.Drawing.Size(866, 473)
        Me.tabRemsion.TabIndex = 0
        Me.tabRemsion.Text = "Remsion de Mercaderia"
        Me.tabRemsion.UseVisualStyleBackColor = True
        '
        'Pcomando
        '
        Me.Pcomando.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Pcomando.Controls.Add(Me.btnGuardar)
        Me.Pcomando.Controls.Add(Me.btnCancelar)
        Me.Pcomando.Controls.Add(Me.btnAgregar)
        Me.Pcomando.Controls.Add(Me.btnBorrar)
        Me.Pcomando.Location = New System.Drawing.Point(512, 137)
        Me.Pcomando.Name = "Pcomando"
        Me.Pcomando.Size = New System.Drawing.Size(358, 39)
        Me.Pcomando.TabIndex = 10
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnGuardar.Location = New System.Drawing.Point(226, 8)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(119, 23)
        Me.btnGuardar.TabIndex = 10
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Location = New System.Drawing.Point(154, 8)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(66, 23)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnAgregar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnAgregar.Location = New System.Drawing.Point(9, 8)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(66, 23)
        Me.btnAgregar.TabIndex = 7
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'btnBorrar
        '
        Me.btnBorrar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBorrar.Location = New System.Drawing.Point(82, 8)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(66, 23)
        Me.btnBorrar.TabIndex = 8
        Me.btnBorrar.Text = "Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(-11, 178)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(919, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = resources.GetString("Label10.Text")
        '
        'dgvRemisiones
        '
        Me.dgvRemisiones.AllowUserToAddRows = False
        Me.dgvRemisiones.AllowUserToDeleteRows = False
        Me.dgvRemisiones.AllowUserToResizeColumns = False
        Me.dgvRemisiones.AllowUserToResizeRows = False
        Me.dgvRemisiones.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvRemisiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRemisiones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column14, Me.Column15, Me.Column16, Me.Column17, Me.Column18, Me.Column19, Me.Column20})
        Me.dgvRemisiones.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvRemisiones.Location = New System.Drawing.Point(3, 206)
        Me.dgvRemisiones.Name = "dgvRemisiones"
        Me.dgvRemisiones.ReadOnly = True
        Me.dgvRemisiones.RowHeadersVisible = False
        Me.dgvRemisiones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRemisiones.Size = New System.Drawing.Size(860, 264)
        Me.dgvRemisiones.TabIndex = 10
        '
        'Column14
        '
        Me.Column14.HeaderText = "Codigo"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Width = 80
        '
        'Column15
        '
        Me.Column15.HeaderText = "Fecha"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Width = 80
        '
        'Column16
        '
        Me.Column16.HeaderText = "Supervisor"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        Me.Column16.Width = 150
        '
        'Column17
        '
        Me.Column17.HeaderText = "Vehiculo"
        Me.Column17.Name = "Column17"
        Me.Column17.ReadOnly = True
        Me.Column17.Width = 150
        '
        'Column18
        '
        Me.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column18.HeaderText = "Ruta"
        Me.Column18.Name = "Column18"
        Me.Column18.ReadOnly = True
        '
        'Column19
        '
        Me.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column19.HeaderText = "Sucursal"
        Me.Column19.Name = "Column19"
        Me.Column19.ReadOnly = True
        '
        'Column20
        '
        Me.Column20.HeaderText = "Pendiente"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        Me.Column20.Width = 60
        '
        'lbcodigo
        '
        Me.lbcodigo.AutoSize = True
        Me.lbcodigo.Location = New System.Drawing.Point(15, 163)
        Me.lbcodigo.Name = "lbcodigo"
        Me.lbcodigo.Size = New System.Drawing.Size(39, 13)
        Me.lbcodigo.TabIndex = 9
        Me.lbcodigo.Text = "codigo"
        Me.lbcodigo.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(415, 150)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(24, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "De:"
        '
        'txtExistencia
        '
        Me.txtExistencia.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtExistencia.Location = New System.Drawing.Point(445, 147)
        Me.txtExistencia.Name = "txtExistencia"
        Me.txtExistencia.ReadOnly = True
        Me.txtExistencia.Size = New System.Drawing.Size(54, 20)
        Me.txtExistencia.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(294, 150)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Cantidad:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(352, 147)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(54, 20)
        Me.txtCantidad.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 150)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Producto:"
        '
        'txtProducto
        '
        Me.txtProducto.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtProducto.Location = New System.Drawing.Point(73, 147)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(206, 20)
        Me.txtProducto.TabIndex = 3
        '
        'dgvRemision
        '
        Me.dgvRemision.AllowUserToAddRows = False
        Me.dgvRemision.AllowUserToDeleteRows = False
        Me.dgvRemision.AllowUserToResizeColumns = False
        Me.dgvRemision.AllowUserToResizeRows = False
        Me.dgvRemision.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvRemision.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRemision.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3})
        Me.dgvRemision.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvRemision.Location = New System.Drawing.Point(3, 3)
        Me.dgvRemision.MultiSelect = False
        Me.dgvRemision.Name = "dgvRemision"
        Me.dgvRemision.ReadOnly = True
        Me.dgvRemision.RowHeadersVisible = False
        Me.dgvRemision.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRemision.Size = New System.Drawing.Size(860, 128)
        Me.dgvRemision.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "Codigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 80
        '
        'Column2
        '
        Me.Column2.HeaderText = "Cant"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 80
        '
        'Column3
        '
        Me.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column3.HeaderText = "Descripcion"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'tabVentas
        '
        Me.tabVentas.Controls.Add(Me.Label17)
        Me.tabVentas.Controls.Add(Me.txtvExistencia)
        Me.tabVentas.Controls.Add(Me.Label18)
        Me.tabVentas.Controls.Add(Me.txtvCantidad)
        Me.tabVentas.Controls.Add(Me.Label19)
        Me.tabVentas.Controls.Add(Me.txtvCliente)
        Me.tabVentas.Controls.Add(Me.dgvVenta2)
        Me.tabVentas.Controls.Add(Me.Panel2)
        Me.tabVentas.Controls.Add(Me.Label16)
        Me.tabVentas.Controls.Add(Me.dgvVentas)
        Me.tabVentas.Controls.Add(Me.dgvVenta)
        Me.tabVentas.Controls.Add(Me.btnVmas)
        Me.tabVentas.Controls.Add(Me.btnVmenos)
        Me.tabVentas.Location = New System.Drawing.Point(4, 25)
        Me.tabVentas.Name = "tabVentas"
        Me.tabVentas.Padding = New System.Windows.Forms.Padding(3)
        Me.tabVentas.Size = New System.Drawing.Size(866, 473)
        Me.tabVentas.TabIndex = 2
        Me.tabVentas.Text = "Ventas"
        Me.tabVentas.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(415, 150)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(24, 13)
        Me.Label17.TabIndex = 43
        Me.Label17.Text = "De:"
        '
        'txtvExistencia
        '
        Me.txtvExistencia.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtvExistencia.Location = New System.Drawing.Point(445, 147)
        Me.txtvExistencia.Name = "txtvExistencia"
        Me.txtvExistencia.ReadOnly = True
        Me.txtvExistencia.Size = New System.Drawing.Size(54, 20)
        Me.txtvExistencia.TabIndex = 42
        Me.txtvExistencia.TabStop = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(294, 150)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 41
        Me.Label18.Text = "Cantidad:"
        '
        'txtvCantidad
        '
        Me.txtvCantidad.Location = New System.Drawing.Point(352, 147)
        Me.txtvCantidad.Name = "txtvCantidad"
        Me.txtvCantidad.Size = New System.Drawing.Size(54, 20)
        Me.txtvCantidad.TabIndex = 9
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(16, 150)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 13)
        Me.Label19.TabIndex = 39
        Me.Label19.Text = "Cliente:"
        '
        'txtvCliente
        '
        Me.txtvCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtvCliente.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtvCliente.Location = New System.Drawing.Point(64, 147)
        Me.txtvCliente.Name = "txtvCliente"
        Me.txtvCliente.Size = New System.Drawing.Size(215, 20)
        Me.txtvCliente.TabIndex = 8
        '
        'dgvVenta2
        '
        Me.dgvVenta2.AllowUserToAddRows = False
        Me.dgvVenta2.AllowUserToDeleteRows = False
        Me.dgvVenta2.AllowUserToResizeColumns = False
        Me.dgvVenta2.AllowUserToResizeRows = False
        Me.dgvVenta2.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvVenta2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVenta2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn47, Me.Column21})
        Me.dgvVenta2.Location = New System.Drawing.Point(468, 3)
        Me.dgvVenta2.MultiSelect = False
        Me.dgvVenta2.Name = "dgvVenta2"
        Me.dgvVenta2.ReadOnly = True
        Me.dgvVenta2.RowHeadersVisible = False
        Me.dgvVenta2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVenta2.Size = New System.Drawing.Size(395, 128)
        Me.dgvVenta2.TabIndex = 34
        Me.dgvVenta2.TabStop = False
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        Me.DataGridViewTextBoxColumn42.ReadOnly = True
        Me.DataGridViewTextBoxColumn42.Width = 80
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.HeaderText = "Venta"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        Me.DataGridViewTextBoxColumn44.ReadOnly = True
        Me.DataGridViewTextBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn44.Width = 80
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn45.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        Me.DataGridViewTextBoxColumn45.ReadOnly = True
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn47.HeaderText = "Cliente"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        Me.DataGridViewTextBoxColumn47.ReadOnly = True
        '
        'Column21
        '
        Me.Column21.HeaderText = "Id_Detalle"
        Me.Column21.Name = "Column21"
        Me.Column21.ReadOnly = True
        Me.Column21.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel2.Controls.Add(Me.btnVguardar)
        Me.Panel2.Controls.Add(Me.btnVcancelar)
        Me.Panel2.Controls.Add(Me.btnVagregar)
        Me.Panel2.Controls.Add(Me.btnVborrar)
        Me.Panel2.Location = New System.Drawing.Point(512, 137)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(358, 39)
        Me.Panel2.TabIndex = 31
        '
        'btnVguardar
        '
        Me.btnVguardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnVguardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnVguardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVguardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnVguardar.Location = New System.Drawing.Point(226, 8)
        Me.btnVguardar.Name = "btnVguardar"
        Me.btnVguardar.Size = New System.Drawing.Size(119, 23)
        Me.btnVguardar.TabIndex = 13
        Me.btnVguardar.Text = "Guardar"
        Me.btnVguardar.UseVisualStyleBackColor = False
        '
        'btnVcancelar
        '
        Me.btnVcancelar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnVcancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVcancelar.Location = New System.Drawing.Point(154, 8)
        Me.btnVcancelar.Name = "btnVcancelar"
        Me.btnVcancelar.Size = New System.Drawing.Size(66, 23)
        Me.btnVcancelar.TabIndex = 12
        Me.btnVcancelar.Text = "Cancelar"
        Me.btnVcancelar.UseVisualStyleBackColor = False
        '
        'btnVagregar
        '
        Me.btnVagregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnVagregar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnVagregar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnVagregar.Location = New System.Drawing.Point(9, 8)
        Me.btnVagregar.Name = "btnVagregar"
        Me.btnVagregar.Size = New System.Drawing.Size(66, 23)
        Me.btnVagregar.TabIndex = 10
        Me.btnVagregar.Text = "Agregar"
        Me.btnVagregar.UseVisualStyleBackColor = False
        '
        'btnVborrar
        '
        Me.btnVborrar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnVborrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVborrar.Location = New System.Drawing.Point(82, 8)
        Me.btnVborrar.Name = "btnVborrar"
        Me.btnVborrar.Size = New System.Drawing.Size(66, 23)
        Me.btnVborrar.TabIndex = 11
        Me.btnVborrar.Text = "Borrar"
        Me.btnVborrar.UseVisualStyleBackColor = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(-52, 171)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(919, 13)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = resources.GetString("Label16.Text")
        '
        'dgvVentas
        '
        Me.dgvVentas.AllowUserToAddRows = False
        Me.dgvVentas.AllowUserToDeleteRows = False
        Me.dgvVentas.AllowUserToResizeColumns = False
        Me.dgvVentas.AllowUserToResizeRows = False
        Me.dgvVentas.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVentas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20})
        Me.dgvVentas.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvVentas.Location = New System.Drawing.Point(3, 206)
        Me.dgvVentas.Name = "dgvVentas"
        Me.dgvVentas.ReadOnly = True
        Me.dgvVentas.RowHeadersVisible = False
        Me.dgvVentas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVentas.Size = New System.Drawing.Size(860, 264)
        Me.dgvVentas.TabIndex = 32
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 80
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Width = 80
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Supervisor"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Width = 150
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Vehiculo"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Width = 150
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn19.HeaderText = "Ruta"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn20.HeaderText = "Sucursal"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        '
        'dgvVenta
        '
        Me.dgvVenta.AllowUserToAddRows = False
        Me.dgvVenta.AllowUserToDeleteRows = False
        Me.dgvVenta.AllowUserToResizeColumns = False
        Me.dgvVenta.AllowUserToResizeRows = False
        Me.dgvVenta.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVenta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.Column22})
        Me.dgvVenta.Location = New System.Drawing.Point(3, 3)
        Me.dgvVenta.MultiSelect = False
        Me.dgvVenta.Name = "dgvVenta"
        Me.dgvVenta.ReadOnly = True
        Me.dgvVenta.RowHeadersVisible = False
        Me.dgvVenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVenta.Size = New System.Drawing.Size(395, 128)
        Me.dgvVenta.TabIndex = 23
        Me.dgvVenta.TabStop = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Width = 80
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "Cant"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Width = 80
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn23.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        '
        'Column22
        '
        Me.Column22.HeaderText = "Id_Detalle"
        Me.Column22.Name = "Column22"
        Me.Column22.ReadOnly = True
        Me.Column22.Visible = False
        '
        'btnVmas
        '
        Me.btnVmas.BackgroundImage = Global.CyberPlanet.Net.My.Resources.Resources.if_Border_Arrow_Right_1063909
        Me.btnVmas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnVmas.Enabled = False
        Me.btnVmas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnVmas.FlatAppearance.BorderSize = 0
        Me.btnVmas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVmas.Location = New System.Drawing.Point(415, 18)
        Me.btnVmas.Name = "btnVmas"
        Me.btnVmas.Size = New System.Drawing.Size(40, 40)
        Me.btnVmas.TabIndex = 37
        Me.btnVmas.UseVisualStyleBackColor = True
        Me.btnVmas.Visible = False
        '
        'btnVmenos
        '
        Me.btnVmenos.BackgroundImage = Global.CyberPlanet.Net.My.Resources.Resources.if_Border_Arrow_Left_1063910
        Me.btnVmenos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnVmenos.Enabled = False
        Me.btnVmenos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnVmenos.FlatAppearance.BorderSize = 0
        Me.btnVmenos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVmenos.Location = New System.Drawing.Point(415, 75)
        Me.btnVmenos.Name = "btnVmenos"
        Me.btnVmenos.Size = New System.Drawing.Size(40, 40)
        Me.btnVmenos.TabIndex = 36
        Me.btnVmenos.UseVisualStyleBackColor = True
        Me.btnVmenos.Visible = False
        '
        'tabEntrada
        '
        Me.tabEntrada.Controls.Add(Me.btnEmas)
        Me.tabEntrada.Controls.Add(Me.btnEmenos)
        Me.tabEntrada.Controls.Add(Me.dgvEntrada2)
        Me.tabEntrada.Controls.Add(Me.dgvEntrada)
        Me.tabEntrada.Controls.Add(Me.dgvEntradas)
        Me.tabEntrada.Controls.Add(Me.Panel1)
        Me.tabEntrada.Controls.Add(Me.Label11)
        Me.tabEntrada.Controls.Add(Me.lbeCodigo)
        Me.tabEntrada.Controls.Add(Me.Label13)
        Me.tabEntrada.Controls.Add(Me.txteExistencia)
        Me.tabEntrada.Controls.Add(Me.Label14)
        Me.tabEntrada.Controls.Add(Me.txteCantidad)
        Me.tabEntrada.Controls.Add(Me.Label15)
        Me.tabEntrada.Controls.Add(Me.txteProducto)
        Me.tabEntrada.Location = New System.Drawing.Point(4, 25)
        Me.tabEntrada.Name = "tabEntrada"
        Me.tabEntrada.Padding = New System.Windows.Forms.Padding(3)
        Me.tabEntrada.Size = New System.Drawing.Size(866, 473)
        Me.tabEntrada.TabIndex = 1
        Me.tabEntrada.Text = "Entrada de Bodega"
        Me.tabEntrada.UseVisualStyleBackColor = True
        '
        'btnEmas
        '
        Me.btnEmas.BackgroundImage = Global.CyberPlanet.Net.My.Resources.Resources.if_Border_Arrow_Right_1063909
        Me.btnEmas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnEmas.Enabled = False
        Me.btnEmas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnEmas.FlatAppearance.BorderSize = 0
        Me.btnEmas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmas.Location = New System.Drawing.Point(415, 18)
        Me.btnEmas.Name = "btnEmas"
        Me.btnEmas.Size = New System.Drawing.Size(40, 40)
        Me.btnEmas.TabIndex = 41
        Me.btnEmas.UseVisualStyleBackColor = True
        Me.btnEmas.Visible = False
        '
        'btnEmenos
        '
        Me.btnEmenos.BackgroundImage = Global.CyberPlanet.Net.My.Resources.Resources.if_Border_Arrow_Left_1063910
        Me.btnEmenos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnEmenos.Enabled = False
        Me.btnEmenos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnEmenos.FlatAppearance.BorderSize = 0
        Me.btnEmenos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmenos.Location = New System.Drawing.Point(415, 75)
        Me.btnEmenos.Name = "btnEmenos"
        Me.btnEmenos.Size = New System.Drawing.Size(40, 40)
        Me.btnEmenos.TabIndex = 40
        Me.btnEmenos.UseVisualStyleBackColor = True
        Me.btnEmenos.Visible = False
        '
        'dgvEntrada2
        '
        Me.dgvEntrada2.AllowUserToAddRows = False
        Me.dgvEntrada2.AllowUserToDeleteRows = False
        Me.dgvEntrada2.AllowUserToResizeColumns = False
        Me.dgvEntrada2.AllowUserToResizeRows = False
        Me.dgvEntrada2.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvEntrada2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEntrada2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn10})
        Me.dgvEntrada2.Location = New System.Drawing.Point(468, 3)
        Me.dgvEntrada2.MultiSelect = False
        Me.dgvEntrada2.Name = "dgvEntrada2"
        Me.dgvEntrada2.ReadOnly = True
        Me.dgvEntrada2.RowHeadersVisible = False
        Me.dgvEntrada2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEntrada2.Size = New System.Drawing.Size(395, 128)
        Me.dgvEntrada2.TabIndex = 39
        Me.dgvEntrada2.TabStop = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 80
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Bodega"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgvEntrada
        '
        Me.dgvEntrada.AllowUserToAddRows = False
        Me.dgvEntrada.AllowUserToDeleteRows = False
        Me.dgvEntrada.AllowUserToResizeColumns = False
        Me.dgvEntrada.AllowUserToResizeRows = False
        Me.dgvEntrada.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvEntrada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEntrada.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14})
        Me.dgvEntrada.Location = New System.Drawing.Point(3, 3)
        Me.dgvEntrada.MultiSelect = False
        Me.dgvEntrada.Name = "dgvEntrada"
        Me.dgvEntrada.ReadOnly = True
        Me.dgvEntrada.RowHeadersVisible = False
        Me.dgvEntrada.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEntrada.Size = New System.Drawing.Size(395, 128)
        Me.dgvEntrada.TabIndex = 38
        Me.dgvEntrada.TabStop = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 80
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Cant"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 80
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn13.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Id_Detalle"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'dgvEntradas
        '
        Me.dgvEntradas.AllowUserToAddRows = False
        Me.dgvEntradas.AllowUserToDeleteRows = False
        Me.dgvEntradas.AllowUserToResizeColumns = False
        Me.dgvEntradas.AllowUserToResizeRows = False
        Me.dgvEntradas.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvEntradas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEntradas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn43, Me.Column24, Me.DataGridViewTextBoxColumn48, Me.DataGridViewTextBoxColumn50})
        Me.dgvEntradas.Location = New System.Drawing.Point(3, 206)
        Me.dgvEntradas.Name = "dgvEntradas"
        Me.dgvEntradas.ReadOnly = True
        Me.dgvEntradas.RowHeadersVisible = False
        Me.dgvEntradas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEntradas.Size = New System.Drawing.Size(860, 264)
        Me.dgvEntradas.TabIndex = 13
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.HeaderText = "Sucursal"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        Me.DataGridViewTextBoxColumn43.ReadOnly = True
        Me.DataGridViewTextBoxColumn43.Width = 80
        '
        'Column24
        '
        Me.Column24.HeaderText = "Bodega"
        Me.Column24.Name = "Column24"
        Me.Column24.ReadOnly = True
        Me.Column24.Width = 80
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn48.HeaderText = "Vehiculo"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        Me.DataGridViewTextBoxColumn48.ReadOnly = True
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        Me.DataGridViewTextBoxColumn50.ReadOnly = True
        Me.DataGridViewTextBoxColumn50.Width = 80
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.Controls.Add(Me.btnEguardar)
        Me.Panel1.Controls.Add(Me.btnEcancelar)
        Me.Panel1.Controls.Add(Me.btnEagregar)
        Me.Panel1.Controls.Add(Me.btnEborrar)
        Me.Panel1.Location = New System.Drawing.Point(512, 137)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(358, 39)
        Me.Panel1.TabIndex = 20
        '
        'btnEguardar
        '
        Me.btnEguardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEguardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEguardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEguardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnEguardar.Location = New System.Drawing.Point(226, 8)
        Me.btnEguardar.Name = "btnEguardar"
        Me.btnEguardar.Size = New System.Drawing.Size(119, 23)
        Me.btnEguardar.TabIndex = 10
        Me.btnEguardar.Text = "Guardar"
        Me.btnEguardar.UseVisualStyleBackColor = False
        '
        'btnEcancelar
        '
        Me.btnEcancelar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnEcancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEcancelar.Location = New System.Drawing.Point(154, 8)
        Me.btnEcancelar.Name = "btnEcancelar"
        Me.btnEcancelar.Size = New System.Drawing.Size(66, 23)
        Me.btnEcancelar.TabIndex = 9
        Me.btnEcancelar.Text = "Cancelar"
        Me.btnEcancelar.UseVisualStyleBackColor = False
        '
        'btnEagregar
        '
        Me.btnEagregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEagregar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEagregar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnEagregar.Location = New System.Drawing.Point(9, 8)
        Me.btnEagregar.Name = "btnEagregar"
        Me.btnEagregar.Size = New System.Drawing.Size(66, 23)
        Me.btnEagregar.TabIndex = 7
        Me.btnEagregar.Text = "Agregar"
        Me.btnEagregar.UseVisualStyleBackColor = False
        '
        'btnEborrar
        '
        Me.btnEborrar.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnEborrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEborrar.Location = New System.Drawing.Point(82, 8)
        Me.btnEborrar.Name = "btnEborrar"
        Me.btnEborrar.Size = New System.Drawing.Size(66, 23)
        Me.btnEborrar.TabIndex = 8
        Me.btnEborrar.Text = "Borrar"
        Me.btnEborrar.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(-26, 178)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(919, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = resources.GetString("Label11.Text")
        '
        'lbeCodigo
        '
        Me.lbeCodigo.AutoSize = True
        Me.lbeCodigo.Location = New System.Drawing.Point(15, 163)
        Me.lbeCodigo.Name = "lbeCodigo"
        Me.lbeCodigo.Size = New System.Drawing.Size(39, 13)
        Me.lbeCodigo.TabIndex = 19
        Me.lbeCodigo.Text = "codigo"
        Me.lbeCodigo.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(415, 150)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(24, 13)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "De:"
        '
        'txteExistencia
        '
        Me.txteExistencia.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txteExistencia.Location = New System.Drawing.Point(445, 147)
        Me.txteExistencia.Name = "txteExistencia"
        Me.txteExistencia.ReadOnly = True
        Me.txteExistencia.Size = New System.Drawing.Size(54, 20)
        Me.txteExistencia.TabIndex = 17
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(294, 150)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(52, 13)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Cantidad:"
        '
        'txteCantidad
        '
        Me.txteCantidad.Location = New System.Drawing.Point(352, 147)
        Me.txteCantidad.Name = "txteCantidad"
        Me.txteCantidad.Size = New System.Drawing.Size(54, 20)
        Me.txteCantidad.TabIndex = 15
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(15, 150)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(53, 13)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Producto:"
        '
        'txteProducto
        '
        Me.txteProducto.BackColor = System.Drawing.SystemColors.Window
        Me.txteProducto.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txteProducto.Location = New System.Drawing.Point(73, 147)
        Me.txteProducto.Name = "txteProducto"
        Me.txteProducto.ReadOnly = True
        Me.txteProducto.Size = New System.Drawing.Size(206, 20)
        Me.txteProducto.TabIndex = 13
        '
        'Devolucion
        '
        Me.Devolucion.Controls.Add(Me.Panel3)
        Me.Devolucion.Controls.Add(Me.Label21)
        Me.Devolucion.Controls.Add(Me.DataGridView5)
        Me.Devolucion.Controls.Add(Me.Label22)
        Me.Devolucion.Controls.Add(Me.Label23)
        Me.Devolucion.Controls.Add(Me.TextBox7)
        Me.Devolucion.Controls.Add(Me.Label24)
        Me.Devolucion.Controls.Add(Me.TextBox8)
        Me.Devolucion.Controls.Add(Me.Label25)
        Me.Devolucion.Controls.Add(Me.TextBox9)
        Me.Devolucion.Controls.Add(Me.DataGridView6)
        Me.Devolucion.Location = New System.Drawing.Point(4, 25)
        Me.Devolucion.Name = "Devolucion"
        Me.Devolucion.Padding = New System.Windows.Forms.Padding(3)
        Me.Devolucion.Size = New System.Drawing.Size(866, 473)
        Me.Devolucion.TabIndex = 3
        Me.Devolucion.Text = "Devolucion de Producto"
        Me.Devolucion.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel3.Controls.Add(Me.Button9)
        Me.Panel3.Controls.Add(Me.Button10)
        Me.Panel3.Controls.Add(Me.Button11)
        Me.Panel3.Controls.Add(Me.Button12)
        Me.Panel3.Location = New System.Drawing.Point(512, 137)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(358, 39)
        Me.Panel3.TabIndex = 31
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button9.Location = New System.Drawing.Point(226, 8)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(119, 23)
        Me.Button9.TabIndex = 10
        Me.Button9.Text = "Guardar"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Location = New System.Drawing.Point(154, 8)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(66, 23)
        Me.Button10.TabIndex = 9
        Me.Button10.Text = "Cancelar"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.DarkOrange
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button11.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button11.Location = New System.Drawing.Point(9, 8)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(66, 23)
        Me.Button11.TabIndex = 7
        Me.Button11.Text = "Agregar"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Location = New System.Drawing.Point(82, 8)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(66, 23)
        Me.Button12.TabIndex = 8
        Me.Button12.Text = "Borrar"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(-26, 178)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(919, 13)
        Me.Label21.TabIndex = 33
        Me.Label21.Text = resources.GetString("Label21.Text")
        '
        'DataGridView5
        '
        Me.DataGridView5.AllowUserToAddRows = False
        Me.DataGridView5.AllowUserToDeleteRows = False
        Me.DataGridView5.AllowUserToResizeColumns = False
        Me.DataGridView5.AllowUserToResizeRows = False
        Me.DataGridView5.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29})
        Me.DataGridView5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DataGridView5.Location = New System.Drawing.Point(3, 206)
        Me.DataGridView5.Name = "DataGridView5"
        Me.DataGridView5.ReadOnly = True
        Me.DataGridView5.RowHeadersVisible = False
        Me.DataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView5.Size = New System.Drawing.Size(860, 264)
        Me.DataGridView5.TabIndex = 32
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Width = 80
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Width = 80
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "Supervisor"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.Width = 150
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "Vehiculo"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.Width = 150
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn28.HeaderText = "Ruta"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn29.HeaderText = "Sucursal"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(15, 163)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(39, 13)
        Me.Label22.TabIndex = 30
        Me.Label22.Text = "codigo"
        Me.Label22.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(415, 150)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(24, 13)
        Me.Label23.TabIndex = 29
        Me.Label23.Text = "De:"
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TextBox7.Location = New System.Drawing.Point(445, 147)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(54, 20)
        Me.TextBox7.TabIndex = 28
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(294, 150)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(52, 13)
        Me.Label24.TabIndex = 27
        Me.Label24.Text = "Cantidad:"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(352, 147)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(54, 20)
        Me.TextBox8.TabIndex = 26
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(15, 150)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(53, 13)
        Me.Label25.TabIndex = 25
        Me.Label25.Text = "Producto:"
        '
        'TextBox9
        '
        Me.TextBox9.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.TextBox9.Location = New System.Drawing.Point(73, 147)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(206, 20)
        Me.TextBox9.TabIndex = 24
        '
        'DataGridView6
        '
        Me.DataGridView6.AllowUserToAddRows = False
        Me.DataGridView6.AllowUserToDeleteRows = False
        Me.DataGridView6.AllowUserToResizeColumns = False
        Me.DataGridView6.AllowUserToResizeRows = False
        Me.DataGridView6.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView6.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32})
        Me.DataGridView6.Dock = System.Windows.Forms.DockStyle.Top
        Me.DataGridView6.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView6.MultiSelect = False
        Me.DataGridView6.Name = "DataGridView6"
        Me.DataGridView6.ReadOnly = True
        Me.DataGridView6.RowHeadersVisible = False
        Me.DataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView6.Size = New System.Drawing.Size(860, 128)
        Me.DataGridView6.TabIndex = 23
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.Width = 80
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "Cant"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.Width = 80
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn32.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        '
        'tabRecepcionRecibo
        '
        Me.tabRecepcionRecibo.Controls.Add(Me.Panel4)
        Me.tabRecepcionRecibo.Controls.Add(Me.Label26)
        Me.tabRecepcionRecibo.Controls.Add(Me.DataGridView7)
        Me.tabRecepcionRecibo.Controls.Add(Me.Label27)
        Me.tabRecepcionRecibo.Controls.Add(Me.Label28)
        Me.tabRecepcionRecibo.Controls.Add(Me.TextBox10)
        Me.tabRecepcionRecibo.Controls.Add(Me.Label29)
        Me.tabRecepcionRecibo.Controls.Add(Me.TextBox11)
        Me.tabRecepcionRecibo.Controls.Add(Me.Label30)
        Me.tabRecepcionRecibo.Controls.Add(Me.TextBox12)
        Me.tabRecepcionRecibo.Controls.Add(Me.DataGridView8)
        Me.tabRecepcionRecibo.Location = New System.Drawing.Point(4, 25)
        Me.tabRecepcionRecibo.Name = "tabRecepcionRecibo"
        Me.tabRecepcionRecibo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabRecepcionRecibo.Size = New System.Drawing.Size(866, 473)
        Me.tabRecepcionRecibo.TabIndex = 4
        Me.tabRecepcionRecibo.Text = "Recepcion de Recibos"
        Me.tabRecepcionRecibo.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel4.Controls.Add(Me.Button13)
        Me.Panel4.Controls.Add(Me.Button14)
        Me.Panel4.Controls.Add(Me.Button15)
        Me.Panel4.Controls.Add(Me.Button16)
        Me.Panel4.Location = New System.Drawing.Point(512, 137)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(358, 39)
        Me.Panel4.TabIndex = 31
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkOrange
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button13.Location = New System.Drawing.Point(226, 8)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(119, 23)
        Me.Button13.TabIndex = 10
        Me.Button13.Text = "Guardar"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Location = New System.Drawing.Point(154, 8)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(66, 23)
        Me.Button14.TabIndex = 9
        Me.Button14.Text = "Cancelar"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.Color.DarkOrange
        Me.Button15.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button15.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button15.Location = New System.Drawing.Point(9, 8)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(66, 23)
        Me.Button15.TabIndex = 7
        Me.Button15.Text = "Agregar"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button16.Location = New System.Drawing.Point(82, 8)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(66, 23)
        Me.Button16.TabIndex = 8
        Me.Button16.Text = "Borrar"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(-26, 178)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(919, 13)
        Me.Label26.TabIndex = 33
        Me.Label26.Text = resources.GetString("Label26.Text")
        '
        'DataGridView7
        '
        Me.DataGridView7.AllowUserToAddRows = False
        Me.DataGridView7.AllowUserToDeleteRows = False
        Me.DataGridView7.AllowUserToResizeColumns = False
        Me.DataGridView7.AllowUserToResizeRows = False
        Me.DataGridView7.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView7.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38})
        Me.DataGridView7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DataGridView7.Location = New System.Drawing.Point(3, 206)
        Me.DataGridView7.Name = "DataGridView7"
        Me.DataGridView7.ReadOnly = True
        Me.DataGridView7.RowHeadersVisible = False
        Me.DataGridView7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView7.Size = New System.Drawing.Size(860, 264)
        Me.DataGridView7.TabIndex = 32
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.Width = 80
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.Width = 80
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "Supervisor"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.ReadOnly = True
        Me.DataGridViewTextBoxColumn35.Width = 150
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "Vehiculo"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        Me.DataGridViewTextBoxColumn36.ReadOnly = True
        Me.DataGridViewTextBoxColumn36.Width = 150
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn37.HeaderText = "Ruta"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.ReadOnly = True
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn38.HeaderText = "Sucursal"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        Me.DataGridViewTextBoxColumn38.ReadOnly = True
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(15, 163)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(39, 13)
        Me.Label27.TabIndex = 30
        Me.Label27.Text = "codigo"
        Me.Label27.Visible = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(415, 150)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(24, 13)
        Me.Label28.TabIndex = 29
        Me.Label28.Text = "De:"
        '
        'TextBox10
        '
        Me.TextBox10.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.TextBox10.Location = New System.Drawing.Point(445, 147)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(54, 20)
        Me.TextBox10.TabIndex = 28
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(294, 150)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(52, 13)
        Me.Label29.TabIndex = 27
        Me.Label29.Text = "Cantidad:"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(352, 147)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(54, 20)
        Me.TextBox11.TabIndex = 26
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(15, 150)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(53, 13)
        Me.Label30.TabIndex = 25
        Me.Label30.Text = "Producto:"
        '
        'TextBox12
        '
        Me.TextBox12.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.TextBox12.Location = New System.Drawing.Point(73, 147)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(206, 20)
        Me.TextBox12.TabIndex = 24
        '
        'DataGridView8
        '
        Me.DataGridView8.AllowUserToAddRows = False
        Me.DataGridView8.AllowUserToDeleteRows = False
        Me.DataGridView8.AllowUserToResizeColumns = False
        Me.DataGridView8.AllowUserToResizeRows = False
        Me.DataGridView8.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView8.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41})
        Me.DataGridView8.Dock = System.Windows.Forms.DockStyle.Top
        Me.DataGridView8.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView8.MultiSelect = False
        Me.DataGridView8.Name = "DataGridView8"
        Me.DataGridView8.ReadOnly = True
        Me.DataGridView8.RowHeadersVisible = False
        Me.DataGridView8.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView8.Size = New System.Drawing.Size(860, 128)
        Me.DataGridView8.TabIndex = 23
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        Me.DataGridViewTextBoxColumn39.ReadOnly = True
        Me.DataGridViewTextBoxColumn39.Width = 80
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.HeaderText = "Cant"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        Me.DataGridViewTextBoxColumn40.ReadOnly = True
        Me.DataGridViewTextBoxColumn40.Width = 80
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn41.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        Me.DataGridViewTextBoxColumn41.ReadOnly = True
        '
        'pPrincipal
        '
        Me.pPrincipal.Controls.Add(Me.tabPrincipal)
        Me.pPrincipal.Location = New System.Drawing.Point(3, 41)
        Me.pPrincipal.Name = "pPrincipal"
        Me.pPrincipal.Size = New System.Drawing.Size(874, 502)
        Me.pPrincipal.TabIndex = 2
        '
        'dtpFecha
        '
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(761, 6)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(103, 20)
        Me.dtpFecha.TabIndex = 18
        Me.dtpFecha.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(557, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Destino:"
        '
        'txtdestino
        '
        Me.txtdestino.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtdestino.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtdestino.Location = New System.Drawing.Point(609, 6)
        Me.txtdestino.Name = "txtdestino"
        Me.txtdestino.ReadOnly = True
        Me.txtdestino.Size = New System.Drawing.Size(85, 20)
        Me.txtdestino.TabIndex = 16
        Me.txtdestino.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(415, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Ruta:"
        '
        'txtruta
        '
        Me.txtruta.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtruta.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtruta.Location = New System.Drawing.Point(454, 6)
        Me.txtruta.Name = "txtruta"
        Me.txtruta.ReadOnly = True
        Me.txtruta.Size = New System.Drawing.Size(85, 20)
        Me.txtruta.TabIndex = 14
        Me.txtruta.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(260, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Vehiculo:"
        '
        'txtvehiculo
        '
        Me.txtvehiculo.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtvehiculo.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtvehiculo.Location = New System.Drawing.Point(317, 6)
        Me.txtvehiculo.Name = "txtvehiculo"
        Me.txtvehiculo.ReadOnly = True
        Me.txtvehiculo.Size = New System.Drawing.Size(85, 20)
        Me.txtvehiculo.TabIndex = 12
        Me.txtvehiculo.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Supervisor:"
        '
        'txtsupervisor
        '
        Me.txtsupervisor.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtsupervisor.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtsupervisor.Location = New System.Drawing.Point(77, 6)
        Me.txtsupervisor.Name = "txtsupervisor"
        Me.txtsupervisor.ReadOnly = True
        Me.txtsupervisor.Size = New System.Drawing.Size(172, 20)
        Me.txtsupervisor.TabIndex = 10
        Me.txtsupervisor.TabStop = False
        '
        'pDeco
        '
        Me.pDeco.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.pDeco.Controls.Add(Me.Label7)
        Me.pDeco.Controls.Add(Me.txtsupervisor)
        Me.pDeco.Controls.Add(Me.dtpFecha)
        Me.pDeco.Controls.Add(Me.Label1)
        Me.pDeco.Controls.Add(Me.txtvehiculo)
        Me.pDeco.Controls.Add(Me.Label5)
        Me.pDeco.Controls.Add(Me.Label2)
        Me.pDeco.Controls.Add(Me.txtdestino)
        Me.pDeco.Controls.Add(Me.txtruta)
        Me.pDeco.Controls.Add(Me.Label4)
        Me.pDeco.Location = New System.Drawing.Point(5, 3)
        Me.pDeco.Name = "pDeco"
        Me.pDeco.Size = New System.Drawing.Size(869, 32)
        Me.pDeco.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(715, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Fecha:"
        '
        'Psupervisor
        '
        Me.Psupervisor.Controls.Add(Me.dgvSupervisor)
        Me.Psupervisor.Location = New System.Drawing.Point(3, 260)
        Me.Psupervisor.Name = "Psupervisor"
        Me.Psupervisor.Size = New System.Drawing.Size(874, 280)
        Me.Psupervisor.TabIndex = 3
        '
        'dgvSupervisor
        '
        Me.dgvSupervisor.AllowUserToAddRows = False
        Me.dgvSupervisor.AllowUserToDeleteRows = False
        Me.dgvSupervisor.AllowUserToResizeColumns = False
        Me.dgvSupervisor.AllowUserToResizeRows = False
        Me.dgvSupervisor.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvSupervisor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSupervisor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Column5, Me.Column6})
        Me.dgvSupervisor.Location = New System.Drawing.Point(7, 12)
        Me.dgvSupervisor.MultiSelect = False
        Me.dgvSupervisor.Name = "dgvSupervisor"
        Me.dgvSupervisor.ReadOnly = True
        Me.dgvSupervisor.RowHeadersVisible = False
        Me.dgvSupervisor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSupervisor.Size = New System.Drawing.Size(860, 207)
        Me.dgvSupervisor.TabIndex = 1
        '
        'Column4
        '
        Me.Column4.HeaderText = "Codigo"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Column5
        '
        Me.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column5.HeaderText = "Nombre"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "Telefono"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 120
        '
        'lbTipo
        '
        Me.lbTipo.AutoSize = True
        Me.lbTipo.BackColor = System.Drawing.Color.DarkOrange
        Me.lbTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTipo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lbTipo.Location = New System.Drawing.Point(10, 479)
        Me.lbTipo.Name = "lbTipo"
        Me.lbTipo.Size = New System.Drawing.Size(116, 20)
        Me.lbTipo.TabIndex = 8
        Me.lbTipo.Text = "SUPERVISOR"
        Me.lbTipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pVehiculo
        '
        Me.pVehiculo.Controls.Add(Me.dgvVehiculo)
        Me.pVehiculo.Location = New System.Drawing.Point(3, 260)
        Me.pVehiculo.Name = "pVehiculo"
        Me.pVehiculo.Size = New System.Drawing.Size(874, 280)
        Me.pVehiculo.TabIndex = 4
        '
        'dgvVehiculo
        '
        Me.dgvVehiculo.AllowUserToAddRows = False
        Me.dgvVehiculo.AllowUserToDeleteRows = False
        Me.dgvVehiculo.AllowUserToResizeColumns = False
        Me.dgvVehiculo.AllowUserToResizeRows = False
        Me.dgvVehiculo.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvVehiculo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVehiculo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column23})
        Me.dgvVehiculo.Location = New System.Drawing.Point(7, 12)
        Me.dgvVehiculo.Name = "dgvVehiculo"
        Me.dgvVehiculo.ReadOnly = True
        Me.dgvVehiculo.RowHeadersVisible = False
        Me.dgvVehiculo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVehiculo.Size = New System.Drawing.Size(860, 207)
        Me.dgvVehiculo.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'Column7
        '
        Me.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column7.HeaderText = "Marca"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Column8
        '
        Me.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column8.HeaderText = "Modelo"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column9.HeaderText = "Placa"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        '
        'Column10
        '
        Me.Column10.HeaderText = "Odometro"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        '
        'Column23
        '
        Me.Column23.HeaderText = "Ruta"
        Me.Column23.Name = "Column23"
        Me.Column23.ReadOnly = True
        '
        'pRuta
        '
        Me.pRuta.Controls.Add(Me.dgvRuta)
        Me.pRuta.Location = New System.Drawing.Point(3, 260)
        Me.pRuta.Name = "pRuta"
        Me.pRuta.Size = New System.Drawing.Size(874, 279)
        Me.pRuta.TabIndex = 11
        '
        'dgvRuta
        '
        Me.dgvRuta.AllowUserToAddRows = False
        Me.dgvRuta.AllowUserToDeleteRows = False
        Me.dgvRuta.AllowUserToResizeColumns = False
        Me.dgvRuta.AllowUserToResizeRows = False
        Me.dgvRuta.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvRuta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRuta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.Column11, Me.Column12})
        Me.dgvRuta.Location = New System.Drawing.Point(7, 12)
        Me.dgvRuta.Name = "dgvRuta"
        Me.dgvRuta.ReadOnly = True
        Me.dgvRuta.RowHeadersVisible = False
        Me.dgvRuta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRuta.Size = New System.Drawing.Size(860, 207)
        Me.dgvRuta.TabIndex = 1
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'Column11
        '
        Me.Column11.HeaderText = "Nombre"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 200
        '
        'Column12
        '
        Me.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column12.HeaderText = "Detalle"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        '
        'pProducto
        '
        Me.pProducto.Controls.Add(Me.dgvProducto)
        Me.pProducto.Location = New System.Drawing.Point(3, 260)
        Me.pProducto.Name = "pProducto"
        Me.pProducto.Size = New System.Drawing.Size(874, 280)
        Me.pProducto.TabIndex = 12
        '
        'dgvProducto
        '
        Me.dgvProducto.AllowUserToAddRows = False
        Me.dgvProducto.AllowUserToDeleteRows = False
        Me.dgvProducto.AllowUserToResizeColumns = False
        Me.dgvProducto.AllowUserToResizeRows = False
        Me.dgvProducto.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProducto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.Column13})
        Me.dgvProducto.Location = New System.Drawing.Point(7, 12)
        Me.dgvProducto.Name = "dgvProducto"
        Me.dgvProducto.ReadOnly = True
        Me.dgvProducto.RowHeadersVisible = False
        Me.dgvProducto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProducto.Size = New System.Drawing.Size(860, 207)
        Me.dgvProducto.TabIndex = 1
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Codigo"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Nombre"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Existencia"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'Column13
        '
        Me.Column13.HeaderText = "ExistenciaMIN"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        '
        'pMain
        '
        Me.pMain.Controls.Add(Me.Label9)
        Me.pMain.Controls.Add(Me.lbRemision)
        Me.pMain.Controls.Add(Me.pPrincipal)
        Me.pMain.Controls.Add(Me.Psupervisor)
        Me.pMain.Controls.Add(Me.pProducto)
        Me.pMain.Controls.Add(Me.pVehiculo)
        Me.pMain.Controls.Add(Me.pRuta)
        Me.pMain.Controls.Add(Me.pDeco)
        Me.pMain.Controls.Add(Me.lbTipo)
        Me.pMain.Location = New System.Drawing.Point(23, 0)
        Me.pMain.Name = "pMain"
        Me.pMain.Size = New System.Drawing.Size(891, 546)
        Me.pMain.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label9.Location = New System.Drawing.Point(748, 38)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 25)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "- No."
        '
        'lbRemision
        '
        Me.lbRemision.AutoSize = True
        Me.lbRemision.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbRemision.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lbRemision.Location = New System.Drawing.Point(802, 38)
        Me.lbRemision.Name = "lbRemision"
        Me.lbRemision.Size = New System.Drawing.Size(72, 25)
        Me.lbRemision.TabIndex = 3
        Me.lbRemision.Text = "30266"
        '
        'FrmRemision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(932, 551)
        Me.Controls.Add(Me.pMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmRemision"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.tabPrincipal.ResumeLayout(False)
        Me.tabRemsion.ResumeLayout(False)
        Me.tabRemsion.PerformLayout()
        Me.Pcomando.ResumeLayout(False)
        CType(Me.dgvRemisiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRemision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabVentas.ResumeLayout(False)
        Me.tabVentas.PerformLayout()
        CType(Me.dgvVenta2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabEntrada.ResumeLayout(False)
        Me.tabEntrada.PerformLayout()
        CType(Me.dgvEntrada2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Devolucion.ResumeLayout(False)
        Me.Devolucion.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabRecepcionRecibo.ResumeLayout(False)
        Me.tabRecepcionRecibo.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pPrincipal.ResumeLayout(False)
        Me.pDeco.ResumeLayout(False)
        Me.pDeco.PerformLayout()
        Me.Psupervisor.ResumeLayout(False)
        CType(Me.dgvSupervisor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pVehiculo.ResumeLayout(False)
        CType(Me.dgvVehiculo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pRuta.ResumeLayout(False)
        CType(Me.dgvRuta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pProducto.ResumeLayout(False)
        CType(Me.dgvProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pMain.ResumeLayout(False)
        Me.pMain.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabPrincipal As System.Windows.Forms.TabControl
    Friend WithEvents tabRemsion As System.Windows.Forms.TabPage
    Friend WithEvents tabEntrada As System.Windows.Forms.TabPage
    Friend WithEvents tabVentas As System.Windows.Forms.TabPage
    Friend WithEvents Devolucion As System.Windows.Forms.TabPage
    Friend WithEvents tabRecepcionRecibo As System.Windows.Forms.TabPage
    Friend WithEvents pPrincipal As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents dgvRemision As System.Windows.Forms.DataGridView
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtdestino As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtruta As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtvehiculo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtsupervisor As System.Windows.Forms.TextBox
    Friend WithEvents pDeco As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Psupervisor As System.Windows.Forms.Panel
    Friend WithEvents dgvSupervisor As System.Windows.Forms.DataGridView
    Friend WithEvents btnBorrar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Pcomando As System.Windows.Forms.Panel
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lbTipo As System.Windows.Forms.Label
    Friend WithEvents pVehiculo As System.Windows.Forms.Panel
    Friend WithEvents dgvVehiculo As System.Windows.Forms.DataGridView
    Friend WithEvents pRuta As System.Windows.Forms.Panel
    Friend WithEvents dgvRuta As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pProducto As System.Windows.Forms.Panel
    Friend WithEvents dgvProducto As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtExistencia As System.Windows.Forms.TextBox
    Friend WithEvents lbcodigo As System.Windows.Forms.Label
    Friend WithEvents dgvRemisiones As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnEguardar As System.Windows.Forms.Button
    Friend WithEvents btnEcancelar As System.Windows.Forms.Button
    Friend WithEvents btnEagregar As System.Windows.Forms.Button
    Friend WithEvents btnEborrar As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lbeCodigo As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txteExistencia As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txteCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txteProducto As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnVguardar As System.Windows.Forms.Button
    Friend WithEvents btnVcancelar As System.Windows.Forms.Button
    Friend WithEvents btnVagregar As System.Windows.Forms.Button
    Friend WithEvents btnVborrar As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents dgvVentas As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvVenta As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents DataGridView5 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView6 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents DataGridView7 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView8 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvVenta2 As System.Windows.Forms.DataGridView
    Friend WithEvents btnVmas As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtvExistencia As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtvCantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtvCliente As System.Windows.Forms.TextBox
    Friend WithEvents btnVmenos As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvEntradas As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEmas As System.Windows.Forms.Button
    Friend WithEvents btnEmenos As System.Windows.Forms.Button
    Friend WithEvents dgvEntrada2 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvEntrada As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pMain As System.Windows.Forms.Panel
    Friend WithEvents lbRemision As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
End Class
