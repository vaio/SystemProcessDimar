﻿Imports System.Xml
Imports System.Text.RegularExpressions
Imports Excel = Microsoft.Office.Interop.Excel
Public Class frmKardexInv
    '----Print-----
    Dim xlApp As Microsoft.Office.Interop.Excel.Application
    Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
    Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
    Dim misValue As Object = System.Reflection.Missing.Value
    Public Tentrada As Decimal = 0
    Public Tsalida As Decimal = 0
    Public Tsaldo As Decimal = 0
    Public Tpromedio As Decimal = 0

    Public Sub Mensaje()
        MsgBox("Este producto no tiene movimientos que mostrar.", MsgBoxStyle.Information, "Información")
    End Sub

    Public Sub CargarDatos(ByVal Codigo As String)

        Dim strsql As String = ""

        Try
            strsql = "SELECT PRD.Codigo_Producto, PRD.Nombre_Producto, Tbl_Proveedor.Nombre_Proveedor, PRD.Existencia_Minima, PRD.Existencia_Maxima " & _
                     "FROM Productos PRD INNER JOIN Tbl_Proveedor ON PRD.Codigo_Proveedor = Tbl_Proveedor.Codigo_Proveedor where PRD.Codigo_Producto = '" & Codigo & "'"

            Dim tblDatos As DataTable = SQL(strsql, "tblDatos", My.Settings.SolIndustrialCNX).Tables(0)

            lblCodigo.Text = tblDatos.Rows(0).Item(0)
            lblNombreProd.Text = tblDatos.Rows(0).Item(1)
            lblProveedor.Text = tblDatos.Rows(0).Item(2)
            lblMinimo.Text = tblDatos.Rows(0).Item(3)
            lblMaximo.Text = tblDatos.Rows(0).Item(4)

            deInicio.Enabled = True
            deFin.Enabled = True
            cmbBodega.Enabled = True
            btnGenerar.Enabled = True

            Bodega(Codigo)

        Catch ex As Exception
        End Try
    End Sub

    Public Sub Bodega(ByVal Codigo As String)
        Dim strsql As String = ""
        Dim valor As String = ""

        strsql = " SELECT distinct Alm.Codigo_Bodega, Alm.Nombre_Bodega FROM Almacenes Alm INNER JOIN Inventario_SaldoInicial Inv "
        strsql = strsql & " ON Alm.Codigo_Sucursal = Inv.Codigo_Sucursal and Alm.Codigo_Bodega = Inv.Codigo_Bodega "
        strsql = strsql & " WHERE Inv.Codigo_Sucursal = 1 and Inv.Codigo_Producto = '" & Codigo & "' "

        Dim Data As DataTable = SQL(strsql, "tblData", My.Settings.SolIndustrialCNX).Tables(0)

        cmbBodega.DisplayMember = "Nombre_Bodega"
        cmbBodega.ValueMember = "Codigo_Bodega"
        cmbBodega.DataSource = Data

        If Data.Rows.Count > 0 Then
            btnGenerar.Enabled = True
        Else
            btnGenerar.Enabled = False
            cmbBodega.Text = ""
            grdKardex.DataSource = Nothing
        End If
    End Sub

    Public Sub cargarKardex(ByVal Codigo As String)
        Dim strsql As String = ""
        Dim Inventario As New clsInventario(My.Settings.SolIndustrialCNX)

        grdKardex.DataSource = Nothing
       
        strsql = "exec [dbo].[SP_GET_Kardex_Inventario] '" & Codigo & "'," & My.Settings.Sucursal & "," & cmbBodega.SelectedValue.ToString & ",'" & deInicio.DateTime.Date & "','" & deFin.DateTime.Date & "'"

        grdKardex.DataSource = SQL(strsql, "tblDatosRecetaDetalle", My.Settings.SolIndustrialCNX).Tables(0)
        grvKardex.Columns("No.").Width = 30
        grvKardex.Columns("Fecha").Width = 70
        grvKardex.Columns("No. Movimiento").Width = 90
        grvKardex.Columns("Información del Movimiento").Width = 300
        grvKardex.Columns("Entrada").Width = 80
        grvKardex.Columns("Salida").Width = 80
        grvKardex.Columns("Saldo").Width = 80
        grvKardex.Columns("Costo Entrada").Width = 100
        grvKardex.Columns("Costo Entrada").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns("Costo Entrada").DisplayFormat.FormatString = "{0:C$ #,##0.0000}"
        grvKardex.Columns("Costo Salida").Width = 100
        grvKardex.Columns("Costo Salida").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns("Costo Salida").DisplayFormat.FormatString = "{0:C$ #,##0.0000}"
        grvKardex.Columns("Costo Saldo").Width = 100
        grvKardex.Columns("Costo Saldo").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns("Costo Saldo").DisplayFormat.FormatString = "{0:C$ #,##0.0000}"
        grvKardex.Columns("Costo Promedio").Width = 85
        grvKardex.Columns("Costo Promedio").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        grvKardex.Columns("Costo Promedio").DisplayFormat.FormatString = "{0:C$ #,##0.0000}"
    End Sub

    Public Sub nuevo()
        lblCodigo.Text = ""
        lblNombreProd.Text = ""

        Dim Fecha As Date = Date.Now
        Fecha = DateSerial(Year(Fecha), Month(Fecha) + 0, 1)
        deInicio.DateTime = Fecha

        deFin.DateTime = Date.Now
        grdKardex.DataSource = Nothing
    End Sub

    Public Sub Cerrar()
        nTipoEdic = 0
        nTipoEdicDet = 0
        Me.Dispose()
        Close()
    End Sub

    Private Sub frmKardexInv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Fecha As Date = Date.Now
        Fecha = DateSerial(Year(Fecha), Month(Fecha) + 0, 1)
        deInicio.DateTime = Fecha
        deFin.DateTime = Date.Now

        deInicio.Enabled = False
        deFin.Enabled = False
        cmbBodega.Enabled = False
        btnGenerar.Enabled = False
    End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        cargarKardex(lblCodigo.Text)
    End Sub

    Private Sub frmKardexInv_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        NumCatalogo = 24
    End Sub

    Private Sub cmbBodega_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbBodega.SelectedValueChanged
        grdKardex.DataSource = Nothing
    End Sub

    Sub print()
        Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        Dim Fpath As String = sPath & "\Reportes.xlsx"
        IO.Directory.CreateDirectory(sPath)
        If My.Computer.FileSystem.FileExists(Fpath) = False Then
            My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reportes, True)
        End If
        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reportes.xlsx")
        xlWorkSheet = xlWorkBook.Sheets("Kardex")
        xlWorkSheet.Cells(2, 11) = "BODEGA: " + cmbBodega.Text
        xlWorkSheet.Cells(3, 11) = "FECHA INICIO: " + deInicio.Text
        xlWorkSheet.Cells(4, 11) = "FECHA FIN: " + deFin.Text
        xlWorkSheet.Cells(3, 1) = "DIMAR S.A. (CENTRAL)"
        xlWorkSheet.Cells(4, 1) = "PRODUCTO: " + lblCodigo.Text + " " + lblNombreProd.Text
        xlWorkSheet.Cells(5, 1) = "PROVEEDOR: " + lblProveedor.Text
        xlWorkSheet.Cells(6, 4) = lblMinimo.Text
        xlWorkSheet.Cells(6, 6) = lblMaximo.Text
        Dim i As Integer = 1
        Dim f As Integer = 9
        Do While f <= 33
            xlWorkSheet.Cells(f, 1) = ""
            xlWorkSheet.Cells(f, 2) = ""
            xlWorkSheet.Cells(f, 3) = ""
            xlWorkSheet.Cells(f, 4) = ""
            xlWorkSheet.Cells(f, 5) = ""
            xlWorkSheet.Cells(f, 6) = ""
            xlWorkSheet.Cells(f, 7) = ""
            xlWorkSheet.Cells(f, 8) = ""
            xlWorkSheet.Cells(f, 9) = ""
            xlWorkSheet.Cells(f, 10) = ""
            xlWorkSheet.Cells(f, 11) = ""
            f = f + 1
        Loop
        f = 9
        Do While i <= grdKardex.MainView.RowCount
            xlWorkSheet.Cells(f, 1) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(0))
            xlWorkSheet.Cells(f, 2) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(1))
            xlWorkSheet.Cells(f, 3) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(2))
            xlWorkSheet.Cells(f, 4) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(3))
            xlWorkSheet.Cells(f, 5) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(4))
            xlWorkSheet.Cells(f, 6) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(5))
            xlWorkSheet.Cells(f, 7) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(6))
            xlWorkSheet.Cells(f, 8) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(7))
            xlWorkSheet.Cells(f, 9) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(8))
            xlWorkSheet.Cells(f, 10) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(9))
            xlWorkSheet.Cells(f, 11) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(10))
            Tentrada += grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(7))
            Tsalida += grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(8))
            Tsaldo += grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(9))
            Tpromedio = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(10))
            i = i + 1
            f = f + 1
        Loop
        xlWorkSheet.Cells(34, 8) = Tentrada
        xlWorkSheet.Cells(34, 9) = Tsalida
        xlWorkSheet.Cells(34, 10) = Tsaldo
        xlWorkSheet.Cells(34, 11) = Tpromedio
        xlWorkBook.Application.DisplayAlerts = False
        Try
            xlWorkBook.Save()
        Catch ex As Exception
            MsgBox(ex.Message)
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
            '-------------------abajo
            'xlApp.Quit()
            'releaseObject(xlApp)
            'releaseObject(xlWorkBook)
            'releaseObject(xlWorkSheet)
        End Try
        'PENDIENTE
        'xlApp.Visible = True
        'xlWorkSheet.PrintPreview()
        'xlApp.Visible = False
        '-----------------PRINT EXCEL--------------------------------
        xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        'xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        '------------------------------------------------------------
        xlWorkBook.Close()
        xlApp.Quit()
        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)
        '---------
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)

        'Dim sPath As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Housing")
        'Dim Fpath As String = sPath & "\Reportes.xlsx"
        'IO.Directory.CreateDirectory(sPath)
        'If My.Computer.FileSystem.FileExists(Fpath) = False Then
        '    My.Computer.FileSystem.WriteAllBytes(Fpath, My.Resources.Reportes, True)
        'End If
        'xlApp = New Microsoft.Office.Interop.Excel.Application
        'xlWorkBook = xlApp.Workbooks.Open(sPath + "\" + "Reportes.xlsx")
        'xlWorkSheet = xlWorkBook.Sheets("Kardex")
        'xlWorkSheet.Cells(2, 11) = "BODEGA: " + cmbBodega.Text
        'xlWorkSheet.Cells(3, 11) = "FECHA INICIO: " + deInicio.Text
        'xlWorkSheet.Cells(4, 11) = "FECHA FIN: " + deFin.Text
        'xlWorkSheet.Cells(3, 1) = "DIMAR S.A. (CENTRAL)"
        'xlWorkSheet.Cells(4, 1) = "PRODUCTO: " + lblCodigo.Text + " " + lblNombreProd.Text
        'xlWorkSheet.Cells(5, 1) = "PROVEEDOR: " + lblProveedor.Text
        'xlWorkSheet.Cells(6, 4) = lblMinimo.Text
        'xlWorkSheet.Cells(6, 6) = lblMaximo.Text
        'Dim i As Integer = 1
        'Dim f As Integer = 9
        'Do While f <= 33
        '    xlWorkSheet.Cells(f, 1) = ""
        '    xlWorkSheet.Cells(f, 2) = ""
        '    xlWorkSheet.Cells(f, 3) = ""
        '    xlWorkSheet.Cells(f, 4) = ""
        '    xlWorkSheet.Cells(f, 5) = ""
        '    xlWorkSheet.Cells(f, 6) = ""
        '    xlWorkSheet.Cells(f, 7) = ""
        '    xlWorkSheet.Cells(f, 8) = ""
        '    xlWorkSheet.Cells(f, 9) = ""
        '    xlWorkSheet.Cells(f, 10) = ""
        '    xlWorkSheet.Cells(f, 11) = ""
        '    f = f + 1
        'Loop
        'f = 9
        'Do While i <= grdKardex.MainView.RowCount
        '    xlWorkSheet.Cells(f, 1) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(0))
        '    xlWorkSheet.Cells(f, 2) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(1))
        '    xlWorkSheet.Cells(f, 3) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(2))
        '    xlWorkSheet.Cells(f, 4) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(3))
        '    xlWorkSheet.Cells(f, 5) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(4))
        '    xlWorkSheet.Cells(f, 6) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(5))
        '    xlWorkSheet.Cells(f, 7) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(6))
        '    xlWorkSheet.Cells(f, 8) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(7))
        '    xlWorkSheet.Cells(f, 9) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(8))
        '    xlWorkSheet.Cells(f, 10) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(9))
        '    xlWorkSheet.Cells(f, 11) = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(10))
        '    Tentrada += grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(7))
        '    Tsalida += grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(8))
        '    Tsaldo += grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(9))
        '    Tpromedio = grvKardex.GetRowCellValue(i - 1, grvKardex.Columns(10))
        '    i = i + 1
        '    f = f + 1
        'Loop
        'xlWorkSheet.Cells(34, 8) = Tentrada
        'xlWorkSheet.Cells(34, 9) = Tsalida
        'xlWorkSheet.Cells(34, 10) = Tsaldo
        'xlWorkSheet.Cells(34, 11) = Tpromedio
        'xlWorkBook.Application.DisplayAlerts = False
        'Try
        '    xlWorkBook.Save()
        'Catch ex As Exception
        '    xlWorkBook.Close()
        '    xlApp.Quit()
        '    releaseObject(xlApp)
        '    releaseObject(xlWorkBook)
        '    releaseObject(xlWorkSheet)
        'End Try

        ''-----------------PRINT EXCEL--------------------------------
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True)
        ''xlWorkSheet.PrintOut(From:=1, To:=1, Copies:=1, Collate:=True, ActivePrinter:=My.Settings.Impresora)
        ''------------------------------------------------------------
        'xlWorkBook.Close()
        'xlApp.Quit()
        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

'    Private Sub End_Excel_App(datestart As Date, dateEnd As Date)
'        Dim xlp() As Process = Process.GetProcessesByName("EXCEL")
'        For Each Process As Process In xlp
'            If Process.StartTime >= datestart And Process.StartTime <= dateEnd Then
'                Process.Kill()
'                Exit For
'            End If
'        Next
'    End Sub

    Private Sub frmKardexInv_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.mainPanel.Location = New Point(Me.ClientSize.Width / 2 - Me.mainPanel.Size.Width / 2, Me.ClientSize.Height / 2 - Me.mainPanel.Size.Height / 2)
        Me.mainPanel.Anchor = AnchorStyles.None
    End Sub
End Class