﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class FrmRemision
    Dim con As SqlConnection = New SqlConnection(My.Settings.SolIndustrialCNX)
    Dim cmd As New SqlCommand
    Dim sqlstring As String
    Dim adapter As SqlDataAdapter
    Dim tabla As Integer = 0
    Dim Tab As Integer = 0
    Dim check As Boolean = True
    Dim codigoMovimiento As String = ""
    Dim codigoMovimiento2 As String = ""
    Private Sub FrmRemision_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Retrive()
        pPrincipal.BringToFront()
    End Sub
    Private Sub Retrive()
        dgv_Supervisor()
        dgv_Vehiculo()
        dgv_Producto()
        dgv_Actualizar()
    End Sub
    Sub dgv_Actualizar()
        dgv_Remisiones()
        dgv_Ventas()
        dgv_Entradas()
    End Sub
    Sub dgv_Remisiones()
        Dim dt As DataTable = New DataTable()
        dgvRemisiones.Rows.Clear()
        sqlstring = "select top 12 Id_Remision, Tbl_Remision.Fecha,Supervisores.Nombre_Supervisor, Tbl_Remision.Id_Vehiculo, Tbl_Vehiculo.Nombre_Ruta, Configuracion.Sucursales.Nombre, Estado from Tbl_Remision inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Supervisores on Supervisores.Codigo_Supervisor=Tbl_Remision.Id_Supervisor order by Id_Remision desc"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                populateRemisiones(row0(0), row0(1), row0(2), row0(3), row0(4), row0(5), row0(6))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
        If dgvRemisiones.Rows.Count > 0 Then
            lbRemision.Text = dgvRemisiones.Rows(0).Cells(0).Value + 1
        Else
            lbRemision.Text = 1
        End If
    End Sub
    Sub dgv_Ventas()
        Dim dt As DataTable = New DataTable()
        dgvVentas.Rows.Clear()
        sqlstring = "select Id_Remision, Tbl_Remision.Fecha,Supervisores.Nombre_Supervisor, Tbl_Remision.Id_Vehiculo, Tbl_Vehiculo.Nombre_Ruta, Configuracion.Sucursales.Nombre from Tbl_Remision inner join Tbl_Vehiculo on Tbl_Vehiculo.Id_Vehiculo=Tbl_Remision.Id_Vehiculo inner join Configuracion.Sucursales on Configuracion.Sucursales.Key_Sucursal=Tbl_Remision.Id_Sucursal inner join Supervisores on Supervisores.Codigo_Supervisor=Tbl_Remision.Id_Supervisor where Estado=1 order by Id_Remision desc"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                populateVentas(row0(0), row0(1), row0(2), row0(3), row0(4), row0(5))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub dgv_Entradas()
        Dim dt As DataTable = New DataTable()
        dgvEntradas.Rows.Clear()
        sqlstring = "select Almacenes.Codigo_Sucursal, Almacenes.Codigo_Bodega, Nombre_Bodega, Inventario.UndTotal_Existencia from Almacenes inner join Inventario on Inventario.Codigo_Bodega=Almacenes.Codigo_Bodega where Almacenes.Nombre_Bodega LIKE 'VEHICULO %' and Inventario.UndTotal_Existencia > 0"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                populateEntradas(row0(0), row0(1), row0(2), row0(3))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub dgv_Supervisor()
        Dim dt As DataTable = New DataTable()
        dgvSupervisor.Rows.Clear()
        sqlstring = "select Codigo_Supervisor, Nombre_Supervisor,isnull(Telefono,'-') from Supervisores"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                populate_supervisor(row0(0), row0(1), row0(2))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub dgv_Vehiculo()
        Dim dt As DataTable = New DataTable()
        dgvVehiculo.Rows.Clear()
        sqlstring = "Select Id_Vehiculo, Marca, Modelo, Placa, Odo_Final, Nombre_Ruta from Tbl_Vehiculo"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row1 In dt.Rows
                populate_vehiculo(row1(0), row1(1), row1(2), row1(3), row1(4), row1(5))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub dgv_Producto()
        Dim dt As DataTable = New DataTable()
        dgvProducto.Rows.Clear()
        sqlstring = "select Productos.Codigo_Producto, Nombre_Producto, Inventario.UndTotal_Existencia,Existencia_Minima from Productos inner join Inventario on Inventario.Codigo_Producto=Productos.Codigo_Producto where Id_ProductosTipo=2 and Inventario.Codigo_Bodega=4 and Inventario.UndTotal_Existencia > Productos.Existencia_Minima and Productos.SeFactura=1"
        cmd = New SqlCommand(sqlstring, con)
        Try
            con.Open()
            adapter = New SqlDataAdapter(cmd)
            adapter.Fill(dt)
            For Each row0 In dt.Rows
                populate_Producto(row0(0), row0(1), row0(2), row0(3))
            Next
            con.Close()
            dt.Rows.Clear()
        Catch ex As Exception
            MsgBox("Error al mostrar datos." & vbNewLine & "Contactar administrador de sistema" & vbNewLine & "Error: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Sub populate_supervisor(Id As String, Nombre As String, Telefono As String)
        Dim row0 As String() = New String() {Id, Nombre, Telefono}
        dgvSupervisor.Rows.Add(row0)
    End Sub
    Sub populate_vehiculo(id As String, Marca As String, Modelo As String, Placa As String, Odo As Decimal, Ruta As String)
        Dim row1 As String() = New String() {id, Marca, Modelo, Placa, Odo, Ruta}
        dgvVehiculo.Rows.Add(row1)
    End Sub
    Sub populate_Ruta(Id As String, Nombre As String, Detalle As String)
        Dim row0 As String() = New String() {Id, Nombre, Detalle}
        dgvRuta.Rows.Add(row0)
    End Sub
    Sub populate_Producto(Id As String, Nombre As String, Existencia As Decimal, Minimo As Decimal)
        Dim row0 As String() = New String() {Id, Nombre, Existencia, Minimo}
        dgvProducto.Rows.Add(row0)
    End Sub
    Sub populateRemisiones(id As String, Fecha As Date, Supervisor As String, Vehiculo As String, Ruta As String, Sucursal As String, Estado As String)
        Dim row0 As String() = New String() {id, Fecha, Supervisor, Vehiculo, Ruta, Sucursal, Estado}
        dgvRemisiones.Rows.Add(row0)
    End Sub
    Sub populateVentas(id As String, Fecha As Date, Supervisor As String, Vehiculo As String, Ruta As String, Sucursal As String)
        Dim row0 As String() = New String() {id, Fecha, Supervisor, Vehiculo, Ruta, Sucursal}
        dgvVentas.Rows.Add(row0)
    End Sub
    Sub populateEntradas(Sucursal As Integer, Codigo As String, Nombre As String, Cantidad As Decimal)
        Dim row0 As String() = New String() {Sucursal, Codigo, Nombre, Cantidad}
        dgvEntradas.Rows.Add(row0)
    End Sub
    Private Sub txtsupervisor_MouseClick(sender As Object, e As MouseEventArgs) Handles txtsupervisor.MouseClick
        If tabPrincipal.SelectedIndex = 0 Or tabPrincipal.SelectedIndex = 2 Then
            Psupervisor.BringToFront()
            lbTipo.Text = "SUPERVISOR"
            lbTipo.BringToFront()
            tabla = 1
        End If
    End Sub
    Private Sub txtvehiculo_MouseClick(sender As Object, e As MouseEventArgs) Handles txtvehiculo.MouseClick
        If tabPrincipal.SelectedIndex = 0 Then
            pVehiculo.BringToFront()
            lbTipo.Text = "VEHICULO"
            lbTipo.BringToFront()
            tabla = 2
        End If
    End Sub
    Private Sub txtProducto_MouseClick(sender As Object, e As MouseEventArgs) Handles txtProducto.MouseClick, txteProducto.MouseClick
        If tabPrincipal.SelectedIndex = 0 Or tabPrincipal.SelectedIndex = 2 Then
            pProducto.BringToFront()
            lbTipo.Text = "PRODUCTO"
            lbTipo.BringToFront()
            tabla = 5
        End If
    End Sub

    Private Sub dgvSupervisor_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvSupervisor.CellMouseClick
        txtsupervisor.Text = dgvSupervisor.Item(1, dgvSupervisor.CurrentRow.Index).Value
    End Sub
    Private Sub dgvVehiculo_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVehiculo.CellMouseClick
        txtvehiculo.Text = dgvVehiculo.Item(0, dgvVehiculo.CurrentRow.Index).Value
        txtruta.Text = dgvVehiculo.Item(5, dgvVehiculo.CurrentRow.Index).Value
        Vehiculo_dgvRemision()
    End Sub
    Private Sub Vehiculo_dgvRemision()
        dgvRemision.Rows.Clear()
        sqlstring = "select Inventario.Codigo_Producto as Codigo, Inventario.UndTotal_Existencia as Cantidad, Productos.Nombre_Producto as Producto from Inventario inner join Productos on Productos.Codigo_Producto=Inventario.Codigo_Producto where Inventario.Codigo_Bodega=(Select Codigo_Bodega from Almacenes where Nombre_Bodega='VEHICULO " + txtvehiculo.Text + "')"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                dgvRemision.Rows.Add(lectura("Codigo"), lectura("Cantidad"), lectura("Producto"))
            End While
            con.Close()
        Catch ex As Exception
            MsgBox("Error al mostrar items existentes en vehiculo" + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub dgvRuta_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRuta.CellMouseClick
        txtruta.Text = dgvRuta.Item(1, dgvRuta.CurrentRow.Index).Value
    End Sub
    Private Sub dgvProducto_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvProducto.CellMouseClick
        Select Case tabPrincipal.SelectedIndex
            Case 0 'Remsion
                txtProducto.Text = dgvProducto.Item(1, dgvProducto.CurrentRow.Index).Value
                txtExistencia.Text = (dgvProducto.Item(2, dgvProducto.CurrentRow.Index).Value - dgvProducto.Item(3, dgvProducto.CurrentRow.Index).Value)
                lbcodigo.Text = dgvProducto.Item(0, dgvProducto.CurrentRow.Index).Value
            Case 2 'Entrada
                txteProducto.Text = dgvProducto.Item(1, dgvProducto.CurrentRow.Index).Value
                txteExistencia.Text = (dgvProducto.Item(2, dgvProducto.CurrentRow.Index).Value - dgvProducto.Item(3, dgvProducto.CurrentRow.Index).Value)
                lbecodigo.Text = dgvProducto.Item(0, dgvProducto.CurrentRow.Index).Value
        End Select
        
    End Sub
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If txtProducto.Text <> "" And txtCantidad.Text <> "" Then
            Dim cantidad As Decimal = txtCantidad.Text
            Dim existencia As Decimal = txtExistencia.Text
            If cantidad <= existencia And txtCantidad.Text > 0 Then
                dgvRemision.Rows.Add(lbcodigo.Text, txtCantidad.Text, txtProducto.Text)
                txtProducto.Text = ""
                txtCantidad.Text = ""
                txtExistencia.Text = ""
            Else
                MsgBox("Existencias insuficientes")
            End If
        Else
            MsgBox("Datos Faltantes")
        End If
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If dgvRemision.Rows.Count > 0 Then
            Remision_Guardar()
        Else
            MsgBox("No se ha seleccionado articulos")
        End If
    End Sub
    Private Sub Remision_Guardar()
        sqlstring = "insert into Tbl_Remision(Id_Remision, Id_Sucursal, Fecha, Id_Supervisor, Id_Vehiculo, Estado) Values ('" + lbRemision.Text + "', '" & My.Settings.Sucursal & "', '" + dtpFecha.Value + "',(select Codigo_Supervisor from Supervisores where Nombre_Supervisor ='" + txtsupervisor.Text + "'),'" + txtvehiculo.Text + "',1)"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            MsgBox("Registro Guardado")
            Remision_Guardar_Detalle()
        Catch ex As Exception
            MsgBox("Error :" + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Remision_Guardar_Detalle()
        Try
            For i = 0 To dgvRemision.Rows.Count - 1
                sqlstring = "Insert into [Tbl_Remision.Detalle] (Id_Sucursal, Id_Remision, Id_Producto,Cantidad, Cant_vendida) values ('" & My.Settings.Sucursal & "', '" + lbRemision.Text + "','" + dgvRemision.Rows(i).Cells(0).Value.ToString + "', '" & dgvRemision.Rows(i).Cells(1).Value & "', 0)"
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            Next
            Remision_Movimiento_Salida()
        Catch ex As Exception
            MsgBox("Error Detalle Remision: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Remision_Movimiento_Salida()
        Movimiento_Datos()
        Try
            sqlstring = "insert into Movimientos_Almacenes (Codigo_Movimiento, Tipo_Movimiento, CodigoSucursal, Codigo_Documento, Tipo_Documento, Comentario, Total_Neto, Fecha, Anulada) values ('" + codigoMovimiento2 + "',5, '" & My.Settings.Sucursal & "', '" + lbRemision.Text + "',3040, 'SALIDA DE BODEGA PRODUCTOS TERMINADOS',0,'" + dtpFecha.Value + "',0 )"
            cmd = New SqlCommand(sqlstring, con)
            cmd.CommandType = CommandType.Text
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            Remision_Movimiento_SalidaDetalle()
        Catch ex As Exception
            MsgBox("Error Movimiento master: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Remision_Movimiento_SalidaDetalle()
        Try
            For i = 0 To dgvRemision.Rows.Count - 1
                sqlstring = "insert into Movimientos_Almaneces_Detalle(Codigo_Movimiento, Bodega, Id_Moviemiento_Detalle, Id_Sucursal, Codigo_Producto, Cantidad, Precio_Unitario, Anulada, Codigo_Factura) values('" + codigoMovimiento2 + "',4, (select ((SELECT COUNT(*) FROM Movimientos_Almaneces_Detalle where Codigo_Movimiento='" + codigoMovimiento2 + "')+1)), '" & My.Settings.Sucursal & "', '" + dgvRemision.Rows(i).Cells(0).Value.ToString + "', '" & dgvRemision.Rows(i).Cells(1).Value & "', (select Costo_Promedio from Productos where Codigo_Producto='" + dgvRemision.Rows(i).Cells(0).Value.ToString + "'), 0, '')"
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            Next
            Movimiento_Update()
            Remision_Movimiento_Entrada()
        Catch ex As Exception
            con.Close()
            MsgBox("Error:  Movimiento Detalle Salida" + ex.Message)
        End Try
    End Sub
    Private Sub Remision_Movimiento_Entrada()
        Movimiento_Datos()
        Try
            sqlstring = "insert into Movimientos_Almacenes (Codigo_Movimiento, Tipo_Movimiento, CodigoSucursal, Codigo_Documento, Tipo_Documento, Comentario, Total_Neto, Fecha, Anulada) values ('" + codigoMovimiento2 + "',4, '" & My.Settings.Sucursal & "', '" + lbRemision.Text + "',3040, 'ENTRADA DE BODEGA RUTA N. " + txtvehiculo.Text + "',0,'" + dtpFecha.Value + "',0 )"
            cmd = New SqlCommand(sqlstring, con)
            cmd.CommandType = CommandType.Text
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            Remision_Movimiento_EntradaDetalle()
        Catch ex As Exception
            MsgBox("Error Movimiento master: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Remision_Movimiento_EntradaDetalle()
        Try
            For i = 0 To dgvRemision.Rows.Count - 1
                sqlstring = "insert into Movimientos_Almaneces_Detalle(Codigo_Movimiento, Bodega, Id_Moviemiento_Detalle, Id_Sucursal, Codigo_Producto, Cantidad, Precio_Unitario, Anulada, Codigo_Factura) values('" + codigoMovimiento2 + "',(select Codigo_Bodega from Almacenes where Nombre_Bodega='VEHICULO " + txtvehiculo.Text + "'), (select ((SELECT COUNT(*) FROM Movimientos_Almaneces_Detalle where Codigo_Movimiento='" + codigoMovimiento2 + "')+1)), '" & My.Settings.Sucursal & "', '" + dgvRemision.Rows(i).Cells(0).Value.ToString + "', '" & dgvRemision.Rows(i).Cells(1).Value & "', (select Costo_Promedio from Productos where Codigo_Producto='" + dgvRemision.Rows(i).Cells(0).Value.ToString + "'), 0,'')"
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            Next
            Movimiento_Update()
            dgv_Actualizar()
        Catch ex As Exception
            con.Close()
            MsgBox("Error:  Movimiento Detalle Entrada" + ex.Message)
        End Try
    End Sub
    Private Sub Movimiento_Update()
        sqlstring = "Update Movimientos_Almacenes set [Total_Neto]=(select sum(Cantidad*Precio_Unitario) from Movimientos_Almaneces_Detalle where Codigo_Movimiento='" + codigoMovimiento2 + "') where Codigo_Movimiento='" + codigoMovimiento2 + "'"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            MsgBox("Error al actualizar neto de movimiento")
        End Try
    End Sub
    Sub Movimiento_Datos()
        sqlstring = "select top 1 Codigo_Movimiento from Movimientos_Almacenes order by Codigo_Movimiento desc"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                codigoMovimiento = lectura("Codigo_Movimiento")
            End While
            con.Close()
        Catch ex As Exception
            MsgBox("Codigo movimiento: " + ex.Message)
            con.Close()
        End Try

        Dim N As Integer = Val(codigoMovimiento)
        N += 1
        Select Case N.ToString.Length
            Case 1
                codigoMovimiento2 = ("00000" + N.ToString)
            Case 2
                codigoMovimiento2 = ("0000" + N.ToString)
            Case 3
                codigoMovimiento2 = ("000" + N.ToString)
            Case 4
                codigoMovimiento2 = ("00" + N.ToString)
            Case 5
                codigoMovimiento2 = ("0" + N.ToString)
            Case 6
                codigoMovimiento2 = (N.ToString)
        End Select
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Remision_Borrar()
    End Sub
    Private Sub Remision_Borrar()
        dgvRemision.Rows.Clear()
        txtProducto.Text = ""
        txtsupervisor.Text = ""
        txtruta.Text = ""
        txtCantidad.Text = ""
        txtExistencia.Text = ""
        txtvehiculo.Text = ""
        pPrincipal.BringToFront()
    End Sub
    Private Sub btnVagregar_Click(sender As Object, e As EventArgs) Handles btnVagregar.Click
        Dim cantidad
        Dim cantidad2
        Try
            If btnVagregar.Text = "Agregar" Then
                cantidad = dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(1).Value
                cantidad2 = txtvCantidad.Text
                If txtvCliente.Text <> "" And txtvCantidad.Text <> "" And txtvExistencia.Text <> "" Then
                    If cantidad >= cantidad2 And Not (cantidad = 0 Or cantidad2 = 0) Then
                        dgvVenta2.Rows.Add(dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(0).Value.ToString, txtvCantidad.Text, dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(2).Value.ToString, txtvCliente.Text, dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(3).Value)
                        dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(1).Value = cantidad - cantidad2
                        dgvVenta.ClearSelection()
                        txtvCliente.Text = ""
                        txtvCantidad.Text = ""
                        txtvExistencia.Text = ""
                        dgvVenta2.Sort(dgvVenta2.Columns(3), System.ComponentModel.ListSortDirection.Descending)
                    End If
                End If
            Else
                For i = 0 To dgvVenta.Rows.Count - 1
                    If dgvVenta.Rows(i).Cells(0).Value = dgvVenta2.Rows(dgvVenta2.CurrentRow.Index).Cells(0).Value Then
                        cantidad = dgvVenta.Rows(i).Cells(1).Value
                        cantidad2 = dgvVenta2.Rows(dgvVenta2.CurrentRow.Index).Cells(1).Value
                        dgvVenta.Rows(i).Cells(1).Value = cantidad + cantidad2
                        dgvVenta2.Rows.Remove(dgvVenta2.CurrentRow)
                    End If
                    dgvVenta2.ClearSelection()
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub tabPrincipal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabPrincipal.SelectedIndexChanged
        Select Case tabPrincipal.SelectedIndex
            Case 0 'REMISION
                dgv_Remisiones()

            Case 1 'VENTAS
                pPrincipal.BringToFront()
            Case 2 'ENTRADA BODEGA

        End Select
    End Sub
    Private Sub btnVguardar_Click(sender As Object, e As EventArgs) Handles btnVguardar.Click
        Dim bandera As Boolean = True
        Dim count As Integer = 0
        If dgvVenta2.Rows.Count > 0 Then
            For i = 0 To dgvVenta2.Rows.Count - 1
                count = 0
                For j = i To dgvVenta2.Rows.Count - 1
                    If dgvVenta2.Rows(i).Cells(3).Value.ToString = dgvVenta2.Rows(j).Cells(3).Value.ToString Then
                        count = count + 1
                        If count = 1 Then
                            Try
                                sqlstring = "Insert into Tbl_Factura_Solicitud(Id_Sucursal,Id_Remision,Cliente,Fecha) VALUES ('" & My.Settings.Sucursal & "','" + lbRemision.Text + "', '" + dgvVenta2.Rows(i).Cells(3).Value.ToString + "', '" + dtpFecha.Value + "' )"
                                cmd = New SqlCommand(sqlstring, con)
                                cmd.CommandType = CommandType.Text
                                con.Open()
                                cmd.ExecuteNonQuery()
                                con.Close()
                                '============
                                For x = 0 To dgvVenta2.Rows.Count - 1
                                    If dgvVenta2.Rows(i).Cells(3).Value.ToString = dgvVenta2.Rows(x).Cells(3).Value.ToString Then
                                        Try
                                            sqlstring = "Insert into [Tbl_Factura_Solicitud.Detalle](Id_Remision, Id_Solicitud, Id_Sucursal, Id_Producto, Cantidad) VALUES ('" + lbRemision.Text + "', (select top 1 Id_Solicitud From Tbl_Factura_Solicitud where Id_Sucursal='" & My.Settings.Sucursal & "' and Id_Remision='" + lbRemision.Text + "' order by Id_Solicitud desc), '" & My.Settings.Sucursal & "', '" & dgvVenta2.Rows(x).Cells(0).Value & "','" & dgvVenta2.Rows(x).Cells(1).Value & "' )"
                                            cmd = New SqlCommand(sqlstring, con)
                                            cmd.CommandType = CommandType.Text
                                            con.Open()
                                            cmd.ExecuteNonQuery()
                                            con.Close()
                                        Catch ex As Exception
                                            bandera = False
                                            MsgBox("Error: " + ex.Message)
                                            con.Close()
                                        End Try
                                    End If
                                Next
                                '============
                            Catch ex As Exception
                                bandera = False
                                MsgBox("Error: " + ex.Message)
                                con.Close()
                            End Try
                        End If
                        i = j
                    End If
                Next
            Next
            If bandera = True Then
                MsgBox("Solicitud Ingresada")
                Venta_Movimiento_Salida()
            End If
        End If
    End Sub
    Private Sub Venta_Movimiento_Salida()
        Movimiento_Datos()
        Try
            sqlstring = "insert into Movimientos_Almacenes (Codigo_Movimiento, Tipo_Movimiento, CodigoSucursal, Codigo_Documento, Tipo_Documento, Comentario, Total_Neto, Fecha, Anulada) values ('" + codigoMovimiento2 + "',5, '" & My.Settings.Sucursal & "', '" + lbRemision.Text + "',3040, 'SALIDA A VENTA DE RUTA N. " + txtvehiculo.Text + "',0,'" + dtpFecha.Value + "',0 )"
            cmd = New SqlCommand(sqlstring, con)
            cmd.CommandType = CommandType.Text
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            Venta_Movimiento_SalidaDetalle()
        Catch ex As Exception
            MsgBox("Error Movimiento master: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Venta_Movimiento_SalidaDetalle()
        Try
            For i = 0 To dgvVenta2.Rows.Count - 1
                sqlstring = "insert into Movimientos_Almaneces_Detalle(Codigo_Movimiento, Bodega, Id_Moviemiento_Detalle, Id_Sucursal, Codigo_Producto, Cantidad, Precio_Unitario, Anulada, Codigo_Factura) values('" + codigoMovimiento2 + "',(select Codigo_Bodega from Almacenes where Nombre_Bodega= 'VEHICULO " + txtvehiculo.Text + "'), (select ((SELECT COUNT(*) FROM Movimientos_Almaneces_Detalle where Codigo_Movimiento='" + codigoMovimiento2 + "')+1)), '" & My.Settings.Sucursal & "', '" + dgvVenta2.Rows(i).Cells(0).Value.ToString + "', '" & dgvVenta2.Rows(i).Cells(1).Value & "', (select Costo_Promedio from Productos where Codigo_Producto='" + dgvVenta2.Rows(i).Cells(0).Value.ToString + "'), 0,'')"
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            Next
            Movimiento_Update()
        Catch ex As Exception
            con.Close()
            MsgBox("Error:  Movimiento Detalle" + ex.Message)
        End Try
    End Sub
    Private Sub dgvEntradas_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvEntradas.CellMouseClick
        txtvehiculo.Text = (dgvEntradas.Rows(dgvEntradas.CurrentRow.Index).Cells(2).Value.ToString).Substring(9)
        txtruta.Text = "RUTA " + (dgvEntradas.Rows(dgvEntradas.CurrentRow.Index).Cells(2).Value.ToString).Substring(9)
        dgvEntrada.Rows.Clear()
        sqlstring = "select Inventario.Codigo_Producto as Codigo, UndTotal_Existencia as Cantidad, Productos.Nombre_Producto as producto from Inventario inner join Productos on Productos.Codigo_Producto=Inventario.Codigo_Producto where Inventario.Codigo_Bodega=(select Codigo_Bodega from Almacenes where Nombre_Bodega='VEHICULO " + txtvehiculo.Text + "')"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                dgvEntrada.Rows.Add(lectura("Codigo"), lectura("Cantidad"), lectura("Producto"))
            End While
            con.Close()
        Catch ex As Exception
            con.Close()
            MsgBox("Error al mostrar datos Vehiculo. " + ex.Message)
        End Try
    End Sub

    Private Sub dgvVentas_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVentas.CellMouseClick
        'AL SELECCIONAR UN REGISTRO PENDIENTE O CLICK EN BOTON AGREGAR?
        lbRemision.Text = dgvVentas.Rows(dgvVentas.CurrentRow.Index).Cells(0).Value.ToString
        dtpFecha.Value = dgvVentas.Rows(dgvVentas.CurrentRow.Index).Cells(1).Value
        txtsupervisor.Text = dgvVentas.Rows(dgvVentas.CurrentRow.Index).Cells(2).Value.ToString
        txtvehiculo.Text = dgvVentas.Rows(dgvVentas.CurrentRow.Index).Cells(3).Value.ToString
        txtruta.Text = dgvVentas.Rows(dgvVentas.CurrentRow.Index).Cells(4).Value
        dgvVenta.Rows.Clear()
        dgvVenta2.Rows.Clear()
        sqlstring = "select Id_Producto, [Tbl_Remision.Detalle].Cantidad as Cantidad, [Tbl_Remision.Detalle].Id_remsion_Detalle as Detalle, [Tbl_Remision.Detalle].Cant_vendida as Vendido, Productos.Nombre_Producto as Producto from [Tbl_Remision.Detalle] inner join Productos on Productos.Codigo_Producto=[Tbl_Remision.Detalle].Id_Producto where [Tbl_Remision.Detalle].Id_Remision='" + dgvVentas.Rows(dgvVentas.CurrentRow.Index).Cells(0).Value.ToString + "'"
        cmd = New SqlCommand(sqlstring, con)
        cmd.CommandType = CommandType.Text
        Try
            con.Open()
            Dim lectura As SqlDataReader = cmd.ExecuteReader
            While lectura.Read
                dgvVenta.Rows.Add(lectura("Id_Producto"), lectura("Cantidad"), lectura("Producto"), lectura("Detalle"))
            End While
            con.Close()
        Catch ex As Exception
            MsgBox("erro al actualizar inventario." & vbNewLine & "" + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub dgvEntrada_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvEntrada.CellMouseClick
        Try
            txteCantidad.Text = 0
            txteExistencia.Text = dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value
            txteProducto.Text = dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(2).Value.ToString
            btnEmas.Visible = True
            btnEmenos.Visible = False
            btnEagregar.Text = "Agregar"
            dgvEntrada2.ClearSelection()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvEntrada2_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvEntrada2.CellMouseClick
        Try
            btnEmas.Visible = False
            btnEmenos.Visible = True
            btnEagregar.Text = "Quitar"
            dgvEntrada.ClearSelection()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvVenta_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVenta.CellMouseClick
        Try
            txtvCantidad.Text = 0
            txtvExistencia.Text = dgvVenta.Rows(dgvVenta.CurrentRow.Index).Cells(1).Value
            btnVmas.Visible = True
            btnVmenos.Visible = False
            btnVagregar.Text = "Agregar"
            dgvVenta2.ClearSelection()
            txtvCliente.Focus()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub dgvVenta2_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVenta2.CellMouseClick
        Try
            btnVmas.Visible = False
            btnVmenos.Visible = True
            btnVagregar.Text = "Quitar"
            dgvVenta.ClearSelection()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnEagregar_Click(sender As Object, e As EventArgs) Handles btnEagregar.Click
        Dim cantidad
        Dim cantidad2
        Try
            If btnEagregar.Text = "Agregar" Then
                cantidad = dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value
                cantidad2 = txteCantidad.Text
                If txteProducto.Text <> "" And txteCantidad.Text <> "" And txteExistencia.Text <> "" Then
                    If cantidad >= cantidad2 And Not (cantidad = 0 Or cantidad2 = 0) Then
                        dgvEntrada2.Rows.Add(dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(0).Value.ToString, txteCantidad.Text, txteProducto.Text, "PRODUCTOS TERMINADOS")
                        dgvEntrada.Rows(dgvEntrada.CurrentRow.Index).Cells(1).Value = cantidad - cantidad2
                        dgvEntrada.ClearSelection()
                        txteProducto.Text = ""
                        txteCantidad.Text = ""
                        txteExistencia.Text = ""
                        'dgvEntrada2.Sort(dgvEntrada2.Columns(3), System.ComponentModel.ListSortDirection.Descending)
                    End If
                End If
            Else
                For i = 0 To dgvEntrada.Rows.Count - 1
                    If dgvEntrada.Rows(i).Cells(0).Value = dgvEntrada2.Rows(dgvEntrada2.CurrentRow.Index).Cells(0).Value Then
                        cantidad = dgvEntrada.Rows(i).Cells(1).Value
                        cantidad2 = dgvEntrada2.Rows(dgvEntrada2.CurrentRow.Index).Cells(1).Value
                        dgvEntrada.Rows(i).Cells(1).Value = cantidad + cantidad2
                        dgvEntrada2.Rows.Remove(dgvEntrada2.CurrentRow)
                    End If
                    dgvEntrada2.ClearSelection()
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnEguardar_Click(sender As Object, e As EventArgs) Handles btnEguardar.Click
        Entrada_Movimiento_Salida()
    End Sub
    Private Sub Entrada_Movimiento_Salida()
        Movimiento_Datos()
        Try
            sqlstring = "insert into Movimientos_Almacenes (Codigo_Movimiento, Tipo_Movimiento, CodigoSucursal, Codigo_Documento, Tipo_Documento, Comentario, Total_Neto, Fecha, Anulada) values ('" + codigoMovimiento2 + "',5, '" & My.Settings.Sucursal & "', '" + lbRemision.Text + "',3040, 'SALIDA DE BODEGA RUTA N. " + txtvehiculo.Text + "',0,'" + dtpFecha.Value + "',0 )"
            cmd = New SqlCommand(sqlstring, con)
            cmd.CommandType = CommandType.Text
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            Entrada_Movimiento_SalidaDetalle()
        Catch ex As Exception
            MsgBox("Error Movimiento master: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Entrada_Movimiento_SalidaDetalle()
        Try
            For i = 0 To dgvEntrada2.Rows.Count - 1
                sqlstring = "insert into Movimientos_Almaneces_Detalle(Codigo_Movimiento, Bodega, Id_Moviemiento_Detalle, Id_Sucursal, Codigo_Producto, Cantidad, Precio_Unitario, Anulada, Codigo_Factura) values('" + codigoMovimiento2 + "',(select Codigo_Bodega from Almacenes where Nombre_Bodega='VEHICULO " + txtvehiculo.Text + "'), (select ((SELECT COUNT(*) FROM Movimientos_Almaneces_Detalle where Codigo_Movimiento='" + codigoMovimiento2 + "')+1)), '" & My.Settings.Sucursal & "', '" + dgvEntrada2.Rows(i).Cells(0).Value.ToString + "', '" & dgvEntrada2.Rows(i).Cells(1).Value & "', (select Costo_Promedio from Productos where Codigo_Producto='" + dgvEntrada2.Rows(i).Cells(0).Value.ToString + "'), 0, '')"
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            Next
            Movimiento_Update()
            MsgBox("Salida Detalle")
            Entrada_Movimiento_Entrada()
        Catch ex As Exception
            con.Close()
            MsgBox("Error:  Movimiento Detalle Salida" + ex.Message)
        End Try
    End Sub
    Private Sub Entrada_Movimiento_Entrada()
        Movimiento_Datos()
        Try
            sqlstring = "insert into Movimientos_Almacenes (Codigo_Movimiento, Tipo_Movimiento, CodigoSucursal, Codigo_Documento, Tipo_Documento, Comentario, Total_Neto, Fecha, Anulada) values ('" + codigoMovimiento2 + "',4, '" & My.Settings.Sucursal & "', '" + lbRemision.Text + "',3040, 'ENTRADA A BODEGA PRODUCTOS TERMINADOS',0,'" + dtpFecha.Value + "',0 )"
            cmd = New SqlCommand(sqlstring, con)
            cmd.CommandType = CommandType.Text
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            Entrada_Movimiento_EntradaDetalle()
        Catch ex As Exception
            MsgBox("Error Movimiento master: " + ex.Message)
            con.Close()
        End Try
    End Sub
    Private Sub Entrada_Movimiento_EntradaDetalle()
        Try
            For i = 0 To dgvEntrada2.Rows.Count - 1
                sqlstring = "insert into Movimientos_Almaneces_Detalle(Codigo_Movimiento, Bodega, Id_Moviemiento_Detalle, Id_Sucursal, Codigo_Producto, Cantidad, Precio_Unitario, Anulada, Codigo_Factura) values('" + codigoMovimiento2 + "',(select Codigo_Bodega from Almacenes where Nombre_Bodega='VEHICULO " + txtvehiculo.Text + "'), (select ((SELECT COUNT(*) FROM Movimientos_Almaneces_Detalle where Codigo_Movimiento='" + codigoMovimiento2 + "')+1)), '" & My.Settings.Sucursal & "', '" + dgvEntrada2.Rows(i).Cells(0).Value.ToString + "', '" & dgvEntrada2.Rows(i).Cells(1).Value & "', (select Costo_Promedio from Productos where Codigo_Producto='" + dgvEntrada2.Rows(i).Cells(0).Value.ToString + "'), 0,'')"
                cmd = New SqlCommand(sqlstring, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            Next
            Movimiento_Update()
            MsgBox("Entrada Detalle")
            'dgv_Actualizar()
        Catch ex As Exception
            con.Close()
            MsgBox("Error:  Movimiento Detalle Entrada" + ex.Message)
        End Try
    End Sub
    Private Sub FrmRemision_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Me.pMain.Location = New Point(Me.ClientSize.Width / 2 - Me.pMain.Size.Width / 2, Me.ClientSize.Height / 2 - Me.pMain.Size.Height / 2)
        Me.pMain.Anchor = AnchorStyles.None
    End Sub
End Class